#! /usr/bin/env python
# -*- coding: utf-8 -*-

import portfolio

CONFIGS = [
    [
        1,
        [
            "--heuristic",
            "hFF=ff(cost_type=1)",
            "--search",
            "eager(alt([single(sum([g(),weight(hFF,10)])),single(sum([g(),weight(hFF,10)]),pref_only=true)],boost=3025),preferred=[hFF],reopen_closed=true,pathmax=false,lookahead=true,la_greedy=true,la_repair=false,cost_type=1,bound=BOUND)"
        ]
    ],
    [
        49,
        [
            "--landmarks",
            "lmg=lm_rhw(reasonable_orders=true,only_causal_landmarks=false,disjunctive_landmarks=true,conjunctive_landmarks=true,no_orders=false,lm_cost_type=2,cost_type=1)",
            "--heuristic",
            "hLM,hFF=lm_ff_syn(lmg,admissible=false)",
            "--search",
            "lazy(alt([single(sum([g(),weight(hLM,10)])),single(sum([g(),weight(hLM,10)]),pref_only=true),single(sum([g(),weight(hFF,10)])),single(sum([g(),weight(hFF,10)]),pref_only=true)],boost=2000),preferred=[hLM,hFF],reopen_closed=false,cost_type=1,bound=BOUND)"
        ]
    ],
    [
        92,
        [
            "--heuristic",
            "hFF=ff(cost_type=1)",
            "--heuristic",
            "hCg=cg(cost_type=1)",
            "--heuristic",
            "hBlind=blind()",
            "--search",
            "eager(alt([single(sum([g(),weight(hBlind,2)])),single(sum([g(),weight(hFF,2)])),single(sum([g(),weight(hCg,2)]))],boost=4480),preferred=[],reopen_closed=true,pathmax=true,lookahead=true,la_greedy=true,la_repair=true,cost_type=0,bound=BOUND)"
        ]
    ],
    [
        60,
        [
            "--landmarks",
            "lmg=lm_merged([lm_rhw(),lm_hm(m=1)],reasonable_orders=false,only_causal_landmarks=false,disjunctive_landmarks=true,conjunctive_landmarks=true,no_orders=true,cost_type=2)",
            "--heuristic",
            "hLM=lmcount(lmg,admissible=false,pref=false,cost_type=2)",
            "--search",
            "lazy(single(hLM),preferred=[],reopen_closed=false,cost_type=0,bound=BOUND)"
        ]
    ],
    [
        60,
        [
            "--heuristic",
            "hAdd=add(cost_type=2)",
            "--search",
            "lazy(alt([single(sum([g(),weight(hAdd,10)])),single(sum([g(),weight(hAdd,10)]),pref_only=true)],boost=2000),preferred=[hAdd],reopen_closed=false,cost_type=1,bound=BOUND)"
        ]
    ]
]

if __name__ == '__main__':
    portfolio.run(CONFIGS, optimal=False)
