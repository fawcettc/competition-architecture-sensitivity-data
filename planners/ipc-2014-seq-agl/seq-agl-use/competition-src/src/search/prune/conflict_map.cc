/*
 * conflict_map.cc
 *
 *  Created on: Jan 9, 2014
 *      Author: ThinkPad
 */

#include "conflict_map.h"

ConflictMap::ConflictMap() {
	// TODO Auto-generated constructor stub

}

ConflictMap::~ConflictMap() {
	// TODO Auto-generated destructor stub
}

void ConflictMap::initialize() {

	cout << "Initializing ConflictMap..." << endl;

	int prop_id = 0;
	propositions.resize(g_variable_domain.size());
	for (size_t var = 0; var < g_variable_domain.size(); var++) {
		for (int value = 0; value < g_variable_domain[var]; value++)
			propositions[var].push_back(prop_id++);
	}

	ops_add_props.resize(prop_id);
	ops_del_props.resize(prop_id);
	for(size_t i = 0; i < g_operators.size(); i++) {

		Operator& op = g_operators[i];
		for(size_t j = 0; j < op.get_pre_post().size(); j++) {

			const PrePost& pre_post = op.get_pre_post()[j];
			int prop = propositions[pre_post.var][pre_post.post];
			ops_add_props[prop].push_back(op.get_index());

			if(pre_post.pre == -1) {

				for(int k = 0; k < g_variable_domain[pre_post.var]; k++) {

					if(pre_post.pre == k)
						continue;

					prop = propositions[pre_post.var][k];
					ops_del_props[prop].push_back(op.get_index());
				}
				continue;
			}
			prop = propositions[pre_post.var][pre_post.pre];
			ops_del_props[prop].push_back(op.get_index());
		}
	}

	ops_add_by_ops.resize(g_operators.size());
	ops_del_by_ops.resize(g_operators.size());
	for(size_t i = 0; i < g_operators.size(); i++) {

//		cout << "operator index: " << i << endl;
		Operator& op = g_operators[i];
		for(size_t j = 0; j < op.get_prevail().size(); j++) {

			const Prevail& prevail = op.get_prevail()[j];
			int prop = propositions[prevail.var][prevail.prev];

			for(size_t k = 0; k < ops_add_props[prop].size(); k++) {

				ops_add_by_ops[ops_add_props[prop][k]].push_back(op.get_index());
			}
			for(size_t k = 0; k < ops_del_props[prop].size(); k++) {

				ops_del_by_ops[ops_del_props[prop][k]].push_back(op.get_index());
			}
		}
		for(size_t j = 0; j < op.get_pre_post().size(); j++) {

			const PrePost& pre_post = op.get_pre_post()[j];
			if(pre_post.pre == -1)
				continue;

			int prop = propositions[pre_post.var][pre_post.pre];
			for(size_t k = 0; k < ops_add_props[prop].size(); k++) {

				ops_add_by_ops[ops_add_props[prop][k]].push_back(op.get_index());
			}
			for(size_t k = 0; k < ops_del_props[prop].size(); k++) {

				ops_del_by_ops[ops_del_props[prop][k]].push_back(op.get_index());
			}
		}
	}
	cout << "ConflictMap was initialized." << endl;
}

bool ConflictMap::has_common_member(vector<int>& vec1, vector<int>& vec2) {

	size_t i = 0;
	size_t j = 0;
	while(vec1.size() != i && vec2.size() != j) {

		if(vec1[i] == vec2[j])
			return true;
		if(vec1[i] > vec2[j])
			j++;
		else
			i++;
	}
	return false;
}

bool ConflictMap::has_conflict(int op1, int op2) {

	if(op1 == op2)
		return true;
	if(op1 > op2)
		swap(op1, op2);
	assert(op1 < op2);
	pair<int, int> pair1 = make_pair<int, int> (op1, op2);
	map<pair<int, int>, bool>::iterator iter = op_conflict_map.find(pair1);
	if(iter != op_conflict_map.end())
		return iter->second;
	bool result = false;

	if(has_conflict(&g_operators[op1], &g_operators[op2])) {

		result = true;
	}
//	else if(has_common_member(ops_add_by_ops[op1], ops_del_by_ops[op2]) ||
//			has_common_member(ops_add_by_ops[op2], ops_del_by_ops[op1])) {
//
//		result = true;
//	}

	op_conflict_map[pair1] = result;
	return result;
}


bool ConflictMap::has_conflict_pre(const Operator* op, int var, int val) {

	for(int i = 0; i < op->get_pre_post().size(); i++) {

		const PrePost& pre_post = op->get_pre_post()[i];
		int var1 = pre_post.var;
		int val1 = pre_post.pre;
		if(val1 == -1)
			continue;
		if(var1 == var && val1 != val) {

			return true;
		}
	}
	for(int i = 0; i < op->get_prevail().size(); i++) {

		const Prevail& prevail = op->get_prevail()[i];
		int var1 = prevail.var;
		int val1 = prevail.prev;
		if(var1 == var && val1 != val) {

			return true;
		}
	}
	return false;
}


bool ConflictMap::has_conflict_pre_post(const Operator* op, int var, int val) {

	for(int i = 0; i < op->get_pre_post().size(); i++) {

		const PrePost& pre_post = op->get_pre_post()[i];
		int var1 = pre_post.var;
		int val1 = pre_post.post;
		if(var1 == var && val1 != val) {
			return true;
		}
		val1 = pre_post.pre;
		if(val1 == -1)
			continue;
		if(var1 == var && val1 != val) {

			return true;
		}
	}
	for(int i = 0; i < op->get_prevail().size(); i++) {

		const Prevail& prevail = op->get_prevail()[i];
		int var1 = prevail.var;
		int val1 = prevail.prev;
		if(var1 == var && val1 != val) {

			return true;
		}
	}
	return false;
}

bool ConflictMap::has_conflict(const Operator* op1, const Operator* op2) {


	for(int i = 0; i < op1->get_pre_post().size(); i++) {

		const PrePost& pre_post = op1->get_pre_post()[i];
		int var = pre_post.var;
		int val = pre_post.post;
		if(has_conflict_pre_post(op2, var, val))
			return true;
	}
	for(int i = 0; i < op2->get_pre_post().size(); i++) {

		const PrePost& pre_post = op2->get_pre_post()[i];
		int var = pre_post.var;
		int val = pre_post.post;
		if(has_conflict_pre(op1, var, val))
			return true;
	}
	return false;
}

ConflictMap* conflict_map;
