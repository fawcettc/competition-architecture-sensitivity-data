/*
 * conflict_map.h
 *
 *  Created on: Jan 9, 2014
 *      Author: ThinkPad
 */

#ifndef CONFLICT_MAP_H_
#define CONFLICT_MAP_H_
#include "../globals.h"
#include "../operator.h"
#include <map>

using namespace std;

class ConflictMap {

private:
	vector<vector<int> > propositions;
	vector<vector<int> > ops_add_props;
	vector<vector<int> > ops_del_props;
	vector<vector<int> > ops_add_by_ops;
	vector<vector<int> > ops_del_by_ops;
	bool has_common_member(vector<int>& vec1, vector<int>& vec2);
	map<pair<int, int>, bool > op_conflict_map;


	bool has_conflict_pre(const Operator* op, int var, int val);
	bool has_conflict_pre_post(const Operator* op, int var, int val);
	bool has_conflict(const Operator* op1, const Operator* op2);

public:
	ConflictMap();
	virtual ~ConflictMap();
	void initialize();
	bool has_conflict(int op1, int op2);
};

extern ConflictMap* conflict_map;

#endif /* CONFLICT_MAP_H_ */
