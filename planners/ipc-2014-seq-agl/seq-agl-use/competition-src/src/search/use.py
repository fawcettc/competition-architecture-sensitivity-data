#! /usr/bin/env python
# -*- coding: utf-8 -*-

import seq_sat_portfolio
	 
CONFIGS = [
	(289, ["--restart-factor", "5",
	"--initial-threshold", "1000",
    "--heuristic", "hlm1,hff1=lm_ff_syn(lm_rhw(reasonable_orders=true,lm_cost_type=1,cost_type=1))",
	"--search", "iterated(lazy_greedy(hff1,hlm1,preferred=(hff1,hlm1),cost_type=1),repeat_last=true,continue_on_fail=true)"]),
	]

FINAL_CONFIG = ["--exit"]

seq_sat_portfolio.run(configs=CONFIGS, final_config=FINAL_CONFIG, timeout=290)
