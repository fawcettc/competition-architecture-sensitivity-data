/*
 * GoalState.h
 *
 *  Created on: Nov 14, 2013
 *      Author: Reza
 */

#ifndef GOALSTATE_H_
#define GOALSTATE_H_

#include <iostream>
#include <fstream>
#include "../data/globals.h"
#include "../data/operator.h"
#include "InconsistentMap.h"
using namespace std;


class GoalState {
public:
	int* vars;

	int* get_vars() const {

		return vars;
	}
	GoalState(const vector<pair<int, int> >& goal) {

		//		vars.resize(g_variable_domain.size());
		vars = new int[g_variable_domain.size()];
		fill_n(vars, g_variable_domain.size(), -1);
		//		for(size_t i = 0; i < g_variable_domain.size(); i++)
		//			vars[i] = -1;


		for(size_t i = 0; i < goal.size(); i++)
			vars[goal[i].first] = goal[i].second;

	}

    int operator[](int index) const {
        return vars[index];
    }

	GoalState(const GoalState* other, const Operator* op) {

		//		vars.resize(g_variable_domain.size());
		vars = new int[g_variable_domain.size()];
		//		vars = malloc(len * sizeof(int));
		memcpy(vars, other->vars, g_variable_domain.size() * sizeof(int));
		//		for(size_t i = 0; i < g_variable_domain.size(); i++)
		//			vars[i] = other->vars[i];

		for(size_t i = 0; i < op->pre_post.size(); i++) {

			if(vars[op->pre_post[i].var] != -1)
				if(op->pre_post[i].post != vars[op->pre_post[i].var])
					cout << "EYBAL" << endl;
			vars[op->pre_post[i].var] = op->pre_post[i].pre;
		}

		for(size_t i = 0; i < op->prevail.size(); i++) {

			vars[op->prevail[i].var] = op->prevail[i].prev;
		}

	}

	GoalState(const GoalState* other) {

		//		vars.resize(g_variable_domain.size());
		vars = new int[g_variable_domain.size()];
		//		vars = malloc(len * sizeof(int));
		memcpy(vars, other->vars, g_variable_domain.size() * sizeof(int));
		//		for(size_t i = 0; i < g_variable_domain.size(); i++)
		//			vars[i] = other->vars[i];
	}


	bool operator==(const GoalState &other) const {

		for(size_t i = 0; i < g_variable_domain.size(); i++) {

			//			if(this->vars[i] == -1)
			//				continue;
			if(this->vars[i] != other.vars[i])
				return false;
		}
		return true;
	}

	bool operator<(const GoalState &other) const {

	    int size = g_variable_domain.size();
	    return ::lexicographical_compare(vars, vars + size,
	                                     other.vars, other.vars + size);
	}

	size_t hash() const {
		//		cout << "begin hash";
		// hash function adapted from Python's hash function for tuples.
		size_t hash_value = 0x345678;
		//		size_t mult = 1007;
		size_t mult = 1000003;
		for (int i = g_variable_domain.size() - 1; i >= 0; i--) {
			int temp = vars[i];
			if(temp == -1)
				temp = g_variable_domain[i];

			hash_value = (hash_value ^ temp) * mult;
			mult += 82520 + i + i;
			//				mult += 4630 + i + i;
		}
		hash_value += 97531;
		//		hash_value += 9847;
		//		cout << "end hash: " << hash_value << endl;
		return hash_value;
	}

	void dump() const {
	    // We cast the values to int since we'd get bad output otherwise
	    // if state_var_t == char.
	    for (int i = 0; i < g_variable_domain.size(); i++)
	    	if(vars[i] != -1)
				cout << "  " << g_variable_name[i] << ": "
					 << static_cast<int>(vars[i]) << ", ";
	    cout << endl;
	}

	void dump(InconsistentMap* iMap) const {
	    // We cast the values to int since we'd get bad output otherwise
	    // if state_var_t == char.
	    for (int i = 0; i < g_variable_domain.size(); i++)
	    	if(vars[i] != -1)
				cout << "(" << iMap->toString(i, vars[i]) << ")";
	    cout << endl;
	}

	virtual ~GoalState();

};


struct GoalStateHash {
	size_t operator()(const GoalState* p) const {
		return p->hash();
	}
};

struct GoalStateEqual {
	bool operator()(const GoalState* c1, const GoalState* c2) const {
		return *c1 == *c2;
	}
};

#endif /* GOALSTATE_H_ */
