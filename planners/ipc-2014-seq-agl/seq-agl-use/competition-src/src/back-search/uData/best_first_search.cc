#include "best_first_search.h"

#include "../data/globals.h"
#include "heuristic.h"
#include "../data/successor_generator.h"
#include "standard_scalar_open_list.h"
#include "GoalState.h"
#include "additive_heuristic.h"

#include <cassert>
using namespace std;

OpenListInfo::OpenListInfo(Heuristic *heur, bool only_pref)
	: open(new BucketOpenList<OpenListEntry>(heur, false)) {
    heuristic = heur;
    only_preferred_operators = only_pref;
    priority = 0;

}

BestFirstSearchEngine::BestFirstSearchEngine()
    : SearchEngine(), current_state(*(new GoalState(g_goal))) {
    generated_states = 0;
    current_predecessor = 0;
    current_operator = 0;
    back_successor_generator = new BackSuccessorGenerator(g_goal, g_initial_state);
    back_successor_generator->initialize();
    temp = 0;
}

BestFirstSearchEngine::~BestFirstSearchEngine() {
}

void BestFirstSearchEngine::add_heuristic(Heuristic *heuristic,
                                          bool use_estimates,
                                          bool use_preferred_operators) {
    assert(use_estimates || use_preferred_operators);
    heuristics.push_back(heuristic);
    best_heuristic_values.push_back(-1);
    if (use_estimates) {
        open_lists.push_back(OpenListInfo(heuristic, false));
        open_lists.push_back(OpenListInfo(heuristic, true));
    }
    if (use_preferred_operators)
        preferred_operator_heuristics.push_back(heuristic);
}

void BestFirstSearchEngine::initialize() {
    cout << "Conducting best first search." << endl;
    assert(!open_lists.empty());
}

void BestFirstSearchEngine::statistics() const {
    cout << "Expanded " << closed_list.size() << " state(s)." << endl;
    cout << "Generated " << generated_states << " state(s)." << endl;
}

int BestFirstSearchEngine::step() {
    // Invariants:
    // - current_state is the next state for which we want to compute the heuristic.
    // - current_predecessor is a permanent pointer to the predecessor of that state.
    // - current_operator is the operator which leads to current_state from predecessor.

    if (!closed_list.contains(current_state)) {
        const GoalState *parent_ptr = closed_list.insert(
            current_state, current_predecessor, current_operator);
//        if(current_operator != NULL)
//        	current_operator->get_name();
//        current_state.dump();
        for (int i = 0; i < heuristics.size(); i++) {
            if (current_predecessor != NULL)
                heuristics[i]->reach_state(*current_predecessor, *current_operator, *parent_ptr);
            heuristics[i]->evaluate(current_state);
            if(heuristics[i]->get_value() == 0) {
            	cout << "Goal State: ";
            	current_state.dump(back_successor_generator->getInconsitentMap());
            	cout << endl << "Initial State: ";
            	g_initial_state->dump(back_successor_generator->getInconsitentMap());
            	cout << endl;
            	cout << ":D:D:D" << endl;
            }
        }
        if (!is_dead_end()) {
            if (check_goal()) {

                return SOLVED;
            }
            if (check_progress()) {
                report_progress();
                reward_progress();
            }
            generate_successors(parent_ptr);
        }
    }
    return fetch_next_state();
}

bool BestFirstSearchEngine::is_dead_end() {
    // If a reliable heuristic reports a dead end, we trust it.
    // Otherwise, all heuristics must agree on dead-end-ness.
    int dead_end_counter = 0;
    for (int i = 0; i < heuristics.size(); i++) {
        if (heuristics[i]->is_dead_end()) {
            if (heuristics[i]->dead_ends_are_reliable())
                return true;
            else
                dead_end_counter++;
        }
    }
    return dead_end_counter == heuristics.size();
}

bool BestFirstSearchEngine::check_goal() {
    // Any heuristic reports 0 iff this is a goal state, so we can
    // pick an arbitrary one. (only if there are no action costs)
    Heuristic *heur = open_lists[0].heuristic;
    if (!heur->is_dead_end() && heur->get_heuristic() == 0) {
        // We actually need this silly !heur->is_dead_end() check because
        // this state *might* be considered a non-dead end by the
        // overall search even though heur considers it a dead end
        // (e.g. if heur is the CG heuristic, but the FF heuristic is
        // also computed and doesn't consider this state a dead end.
        // If heur considers the state a dead end, it cannot be a goal
        // state (heur will not be *that* stupid). We may not call
        // get_heuristic() in such cases because it will barf.
        if (g_use_metric) {
            bool is_goal = test_initial(current_state);
            if (!is_goal)
                return false;
        }

        cout << "Solution found!" << endl;
        Plan plan;
        closed_list.trace_path(current_state, plan);
        std::reverse(plan.begin(),plan.end());
        set_plan(plan);
        return true;
    } else {
        return false;
    }
}

bool BestFirstSearchEngine::check_progress() {
    bool progress = false;
    for (int i = 0; i < heuristics.size(); i++) {
        if (heuristics[i]->is_dead_end())
            continue;
        int h = heuristics[i]->get_heuristic();
        int &best_h = best_heuristic_values[i];
        if (best_h == -1 || h < best_h) {
            best_h = h;
            progress = true;
        }
    }
    return progress;
}

void BestFirstSearchEngine::report_progress() {
    cout << "Best heuristic value: ";
    for (int i = 0; i < heuristics.size(); i++) {
        cout << best_heuristic_values[i];
        if (i != heuristics.size() - 1)
            cout << "/";
    }
    cout << " [expanded " << closed_list.size() << " state(s)][generated " << generated_states << "]" << endl;
    current_state.dump(back_successor_generator->getInconsitentMap());
}

void BestFirstSearchEngine::reward_progress() {
    // Boost the "preferred operator" open lists somewhat whenever
    // progress is made. This used to be used in multi-heuristic mode
    // only, but it is also useful in single-heuristic mode, at least
    // in Schedule.
    //
    // TODO: Test the impact of this, and find a better way of rewarding
    // successful exploration. For example, reward only the open queue
    // from which the good state was extracted and/or the open queues
    // for the heuristic for which a new best value was found.

    for (int i = 0; i < open_lists.size(); i++)
        if (open_lists[i].only_preferred_operators)
            open_lists[i].priority -= 1000;
}

void BestFirstSearchEngine::generate_successors(const GoalState *parent_ptr) {
    vector<const Operator *> all_operators;

//    temp++;
//    if(temp % 100 == 0) {
//
//    	cout << ">>>>>>>>>>>>>>>>" << temp << endl;
//    }

//    g_successor_generator->generate_applicable_ops(current_state, all_operators);
//    if(temp == 100) {
//
//    	while(true) {
//
//    		back_successor_generator->get_applicable_operators1(*parent_ptr, all_operators);
//    		temp++;
//			if(temp % 1000 == 0) {
//
//    		    	cout << ":D >>>>>>>>>>>>>>>>" << temp << endl;
//			}
//    	}
//    }
    back_successor_generator->get_applicable_operators(*parent_ptr, all_operators);

//    for(int i = 0; i < all_operators.size(); i++) {
//
//    	cout << all_operators[i]->get_name() << endl;
//    }

    vector<const Operator *> preferred_operators;
    for (int i = 0; i < preferred_operator_heuristics.size(); i++) {
        Heuristic *heur = preferred_operator_heuristics[i];
        if (!heur->is_dead_end())
            heur->get_preferred_operators(preferred_operators);
    }

    for (int i = 0; i < open_lists.size(); i++) {
        Heuristic *heur = open_lists[i].heuristic;
        if (!heur->is_dead_end()) {
            OpenList<OpenListEntry> *open = open_lists[i].open;
            open->evaluate(0, false); // TODO: handle preferredness in open list
            if(!open_lists[i].only_preferred_operators) {

            	for (int j = 0; j < all_operators.size(); j++) {
            		GoalState* g = new GoalState(parent_ptr, all_operators[j]);
            		open_lists[i].heuristic->reach_state(*parent_ptr, *all_operators[j], *g);
            		open_lists[i].heuristic->evaluate(*g);
            		open->evaluate(0, false);
					open->insert(make_pair(parent_ptr, all_operators[j]));
					delete g->vars;
					delete g;
				}
            } else {

//            	vector<int> pred;
//            	pred.resize(g_variable_domain.size(), -1);
//
//
//				for(int j = 0; j < all_operators.size(); j++) {
//
//					const Operator& op = *all_operators[j];
//					for(int l = 0; l < op.pre_post.size(); l++) {
//
//						int var = op.pre_post[l].var;
//						int value = op.pre_post[l].post;
//						if(parent_ptr->vars[var] == -1)
//							continue;
//						if(pred[var] == -1) {
//
//							pred[var] = back_successor_generator->goh[j];
//							continue;
//						}
//						int old_op = pred[var];
//						if(AdditiveHeuristic::op_ss[old_op] > AdditiveHeuristic::op_ss[back_successor_generator->goh[j]])
//							pred[var] = back_successor_generator->goh[j];
//					}
//				}
//
//				for(int j = 0; j < all_operators.size(); j++) {
//
//					for(int k = 0; k < g_variable_domain.size(); k++) {
//
//						if(pred[k] == -1)
//							continue;
//						if(pred[k] == back_successor_generator->goh[j]) {
//
//		                	open_lists[i].heuristic->evaluate(GoalState(parent_ptr, all_operators[j]));
//		            		open->evaluate(0, false);
//							open->insert(make_pair(parent_ptr, all_operators[j]));
//							temp++;
//							break;
//						}
//					}
//				}

//                for (int j = 0; j < preferred_operators.size(); j++) {
//					if(std::find(all_operators.begin(), all_operators.end(), preferred_operators[j]) != all_operators.end()) {
//	                	open_lists[i].heuristic->evaluate(GoalState(parent_ptr, preferred_operators[j]));
//	            		open->evaluate(0, false);
//						open->insert(make_pair(parent_ptr, preferred_operators[j]));
//						temp++;
//					}
//                }
           }
        }
    }

//    cout << "\tP: " << temp << ", BP: " << preferred_operators.size() << ", ALL: " << all_operators.size() << endl;
    generated_states += all_operators.size();
}

int BestFirstSearchEngine::fetch_next_state() {
    OpenListInfo *open_info = select_open_queue();
    if (!open_info) {
        cout << "Completely explored state space -- no solution!" << endl;
        return FAILED;
    }

    OpenListEntry next = open_info->open->remove_min();
    open_info->priority++;

    current_predecessor = next.first;
    current_operator = next.second;
    current_state = GoalState(current_predecessor, current_operator);

    return IN_PROGRESS;
}

OpenListInfo *BestFirstSearchEngine::select_open_queue() {
    OpenListInfo *best = 0;
    for (size_t i = 0; i < open_lists.size(); i++)
        if (!open_lists[i].open->empty() &&
            (best == 0 || open_lists[i].priority < best->priority))
            best = &open_lists[i];
    return best;
}
