/*
 * GoalStateInfo.h
 *
 *  Created on: Nov 22, 2013
 *      Author: Reza
 */

#ifndef GOALSTATEINFO_H_
#define GOALSTATEINFO_H_
#include "GoalState.h"

class GoalStateInfo {
public:
	GoalStateInfo(GoalState* parent, int op_index);
	virtual ~GoalStateInfo();

	GoalState* parent;
	int op_index;
};

#endif /* GOALSTATEINFO_H_ */
