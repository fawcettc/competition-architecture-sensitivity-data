#include "ff_heuristic.h"

#include "../data/globals.h"
#include "../data/operator.h"
#include "../data/state.h"

#include <cassert>
#include <vector>
using namespace std;

#include <ext/hash_map>
using namespace __gnu_cxx;


// construction and destruction
FFHeuristic::FFHeuristic()
    : AdditiveHeuristic() {
//    AdditiveHeuristic::initialize();
    relaxed_plan.resize(g_operators.size(), false);
}

FFHeuristic::~FFHeuristic() {
}

// initialization
void FFHeuristic::initialize() {
    cout << "Initializing FF heuristic..." << endl;
    AdditiveHeuristic::initialize();
    relaxed_plan.resize(g_operators.size(), false);
}

void FFHeuristic::mark_preferred_operators_and_relaxed_plan(Proposition *goal) {
    if (!goal->marked) { // Only consider each subgoal once.
        goal->marked = true;
        UnaryOperator *unary_op = goal->reached_by;
        if (unary_op) { // We have not yet chained back to a start node.
            for (int i = 0; i < unary_op->precondition.size(); i++)
                mark_preferred_operators_and_relaxed_plan(unary_op->precondition[i]);
            int operator_no = unary_op->operator_no;
            if (operator_no != -1) {
                // This is not an axiom.
                relaxed_plan[operator_no] = true;

//                if (unary_op->cost == unary_op->base_cost) {
//                    // This test is implied by the next but cheaper,
//                    // so we perform it to save work.
//                    // If we had no 0-cost operators and axioms to worry
//                    // about, it would also imply applicability.
//                    const Operator *op = &g_operators[operator_no];
//                    if (op->is_applicable(state))
//                        set_preferred(op);
//                }
            }
        }
    }
}

int FFHeuristic::compute_heuristic(const GoalState &state) {
    int h_add = compute_add_and_ff(state);
//    cout << "DDDDDDDDDDDD " << h_add << endl;
    if (h_add == DEAD_END)
        return h_add;

    for (int var = 0; var < propositions.size(); var++) {
        for (int value = 0; value < propositions[var].size(); value++) {
            Proposition &prop = propositions[var][value];
            prop.marked = false;
        }
    }
    // Collecting the relaxed plan also sets the preferred operators.
    for(int i = 0; i < g_variable_domain.size(); i++) {

    	if(state.vars[i] == -1)
    		continue;
    	Proposition& prop = propositions[i][state.vars[i]];
    	mark_preferred_operators_and_relaxed_plan(&prop);

    	UnaryOperator *unary_op = prop.reached_by;
    	if(unary_op == 0)
    		continue;

    	int operator_no = unary_op->operator_no;
    	if (operator_no != -1) {

    		const Operator *op = &g_operators[operator_no];
    		//			if (op->is_applicable(state))
    		set_preferred(op);
    	}
    }
//    for (int i = 0; i < goal_propositions.size(); i++)
//        mark_preferred_operators_and_relaxed_plan(state);

    int h_ff = 0;
    for (int op_no = 0; op_no < relaxed_plan.size(); op_no++) {
        if (relaxed_plan[op_no]) {
            relaxed_plan[op_no] = false; // Clean up for next computation.
//            h_ff += get_adjusted_cost(g_operators[op_no]);
            h_ff += 1;
        }
    }
    return h_ff;
}
