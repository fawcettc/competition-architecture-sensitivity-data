/*
 * BackSearchEngine.h
 *
 *  Created on: Nov 18, 2013
 *      Author: R_Sadraei
 */

#ifndef BACKSEARCHENGINE_H_
#define BACKSEARCHENGINE_H_

#include "../data/globals.h"
#include "../data/operator.h"
#include "GoalState.h"
#include "PropOperator.h"
#include "GoalStateInfo.h"
#include "../data/state.h"
#include "MyOpenList.h"
#include <ext/hash_map>
#include <ext/hash_set>
#include <string>
#include "heuristic.h"
#include "InconsistentMap.h"
#include <set>

typedef __gnu_cxx::hash_set<int> h_set;
typedef __gnu_cxx::hash_set<GoalState> g_set;
typedef __gnu_cxx::hash_map<GoalState * , GoalStateInfo* ,  GoalStateHash, GoalStateEqual>  h_map;

class BackSuccessorGenerator {

	vector<bool> bv;
	vector<int> index;

	vector<Operator*> unconditioned_operators;
	vector<PropOperator*> prop_operators;
	vector<vector<int> > precondition_of;
	vector<vector<int> > add_by;
	vector<vector<int> > has_conflict;
	vector<vector<int> > propositions;
	int propositions_num;
	int evaluated;
	InconsistentMap* iMap;


	vector<pair<int, int> > final_goal;
	State *initial_state;

	void build_unconditioned_operators(Operator &op);
	void build_unconditioned_operators();
	void build_prop_operators();
	void build_propositions();
	void build_meta_data();
	bool contains(vector<int>& vec, int var, int val);
	GoalState* apply(const GoalState* goalState, int op_index);


public:

	bool is_bad(int var, int val) {

		if(var == 5 && val == 3)
			return true;
		if(var == 6 && val == 1)
			return true;
		if(var == 6 && val == 4)
			return true;
		return false;
	}

	bool check_op(Operator* op) {

		vector<Prevail> pre_vector = op->get_prevail();
		for(int i = 0; i < pre_vector.size(); i++) {

			if(is_bad(pre_vector[i].var, pre_vector[i].prev))
				return false;
		}

		vector<PrePost> prepost_vector = op->get_pre_post();

		for(int i = 0; i < prepost_vector.size(); i++) {

			if(is_bad(prepost_vector[i].var, prepost_vector[i].pre))
				return false;
		}
		return true;
	}
	InconsistentMap* getInconsitentMap() {

		return iMap;
	}
	vector<int> goh;
	void get_applicable_operators(const GoalState& goalState, vector<const Operator *>& all_operators);
	void get_applicable_operators1(const GoalState& goalState, vector<const Operator *>& all_operators);
	BackSuccessorGenerator(vector<pair<int, int> > final_goal, State *initial_state);
	virtual ~BackSuccessorGenerator();
	void check_all();
	void initialize();
};

#endif /* BACKSEARCHENGINE_H_ */
