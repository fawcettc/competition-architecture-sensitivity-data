/*
 * Util.h
 *
 *  Created on: Nov 25, 2013
 *      Author: R_Sadraei
 */

#ifndef UTIL_H_
#define UTIL_H_
#include "../data/globals.h"
#include "BackSuccessorGenerator.h"
#include "search_engine.h"
#include "../uData/InconsistentMap.h"

class Util {
public:
	Util();
	virtual ~Util();

	static bool contains(vector<int>& vec, int prop) {

		for(size_t i = 0; i < vec.size(); i++)
			if(vec[i] == prop)
				return true;
		return false;

	}

	static State* apply(const State* goalState, const Operator* op) {

		for(size_t i = 0; i < op->prevail.size(); i++)
			if(goalState->get_buffer()[op->prevail[i].var] != op->prevail[i].prev)
				cout << "VAY" << endl;
		for(size_t i = 0; i < op->pre_post.size(); i++)
			if(op->pre_post[i].pre != -1)
				if(op->pre_post[i].pre != goalState->get_buffer()[op->pre_post[i].var])
					cout << "VAY1" << endl;
		State* result = new State(*goalState, *op);
		return result;

	}

	static bool check_plan(State* initial_state, vector<pair<int, int> >& goal, SearchEngine::Plan plan, InconsistentMap* iMap) {

		State* state = initial_state;
		for(size_t i = 0; i < plan.size(); i++) {

			state = apply(state, plan[i]);
			plan[i]->dump();
		}

		state->dump(iMap);

		for(size_t i = 0; i < goal.size(); i++) {

			if(goal[i].second != state->get_buffer()[goal[i].first])
				return false;
		}
		return true;
	}


};

#endif /* UTIL_H_ */
