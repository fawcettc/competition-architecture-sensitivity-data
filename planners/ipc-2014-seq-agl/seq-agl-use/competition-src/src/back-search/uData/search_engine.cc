#include <cassert>
#include <iostream>
#include <limits>
using namespace std;

#include "../data/globals.h"
#include "search_engine.h"
#include "../data/timer.h"

SearchEngine::SearchEngine() {
}

SearchEngine::~SearchEngine() {
}

void SearchEngine::statistics() const {
}

bool SearchEngine::found_solution() const {
    return solved;
}

const SearchEngine::Plan &SearchEngine::get_plan() const {
    assert(solved);
    return plan;
}

void SearchEngine::set_plan(const Plan &p) {
    solved = true;
    plan = p;
}

void SearchEngine::search() {
    initialize();
    Timer timer;
    while (step() == IN_PROGRESS)
        ;
    cout << "Actual search time: " << timer
         << " [t=" << g_timer << "]" << endl;
}

bool SearchEngine::check_goal_and_set_plan(const GoalState &state) {
    if (test_initial(state)) {
        cout << "Solution found!" << endl;
        Plan plan;
        set_plan(plan);
        return true;
    }
    return false;
}

void SearchEngine::save_plan_if_necessary() const {
    if (found_solution())
        save_plan(get_plan());
}

int SearchEngine::get_adjusted_cost(const Operator &op) const {
    return get_adjusted_action_cost(op, cost_type);
}


SearchEngineOptions::SearchEngineOptions()
    : cost_type(NORMAL),
      bound(numeric_limits<int>::max()) {
}

SearchEngineOptions::~SearchEngineOptions() {
}
