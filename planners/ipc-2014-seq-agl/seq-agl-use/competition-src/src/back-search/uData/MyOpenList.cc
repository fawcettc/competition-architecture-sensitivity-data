#ifdef MYOPENLIST_H_
/*
 * MyOpenList.cpp
 *
 *  Created on: Nov 18, 2013
 *      Author: R_Sadraei
 */

//#include "MyOpenList.h"
#include <limits>

using namespace std;

template<class Entry>
MyOpenList<Entry>::MyOpenList() {

    lowest_bucket = numeric_limits<int>::max();
    size = 0;
}

template<class Entry>
MyOpenList<Entry>::~MyOpenList() {
	// TODO Auto-generated destructor stub
}

template<class Entry>
void MyOpenList<Entry>::insert(int key, const Entry& entry) {


	if (key >= buckets.size())
		buckets.resize(key + 1);
	if (key < lowest_bucket)
		lowest_bucket = key;
	buckets[key].push_back(entry);
	size++;
}

template<class Entry>
pair<int, Entry&> MyOpenList<Entry>::remove_min() {

    while (buckets[lowest_bucket].empty())
        lowest_bucket++;
    size--;

    Entry& result = buckets[lowest_bucket].front();
    buckets[lowest_bucket].pop_front();
    return make_pair<int, Entry&>(lowest_bucket, result);
}


template<class Entry>
bool MyOpenList<Entry>::empty() const {
    return size == 0;
}

template<class Entry>
void MyOpenList<Entry>::clear() {
    buckets.clear();
    lowest_bucket = numeric_limits<int>::max();
    size = 0;
}
#endif


