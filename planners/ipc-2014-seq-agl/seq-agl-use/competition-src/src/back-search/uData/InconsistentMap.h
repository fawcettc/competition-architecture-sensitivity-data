/*
 * InconsistentMap.h
 *
 *  Created on: Nov 26, 2013
 *      Author: Reza
 */

#ifndef INCONSISTENTMAP_H_
#define INCONSISTENTMAP_H_

#include "../data/globals.h"
#include "../lm/landmarks_types.h"
#include <vector>
#include <set>
#include <map>
#include <ext/hash_map>
#include <list>
#include <ext/hash_set>
#include <iostream>
#include <fstream>
#include <assert.h>
using namespace __gnu_cxx;

struct Pddl_proposition {
    string predicate;
    vector<string> arguments;
    string to_string() const {
        string output = predicate;
        for (unsigned int i = 0; i < arguments.size(); i++) {
            output += " ";
            output += arguments[i];
        }
        return output;
    }
};

class InconsistentMap {
public:
	InconsistentMap();
	virtual ~InconsistentMap();
    vector<vector<set<pair<int, int> > > > inconsistent_facts;
    hash_map<pair<int, int>, Pddl_proposition, hash_int_pair> pddl_propositions;
    map<string, int> pddl_proposition_indeces;
    bool external_inconsistencies_read;

	void read_external_inconsistencies() {


		g_variable_domain_pure.resize(g_variable_domain.size());
		for(int i = 0; i < g_variable_domain.size(); i++)
			g_variable_domain_pure[i] = g_variable_domain[i];

	    /* Read inconsistencies that were found by translator from separate file. Note: this
	     is somewhat cumbersome, but avoids substantial changes to translator and predecessor
	     output structure. Translator finds groups of facts such that exactly one of those facts
	     is true at any point in time. Hence, all facts within a group are mutually exclusive.
	     */
	    cout << "Reading invariants from file..." << endl;
	    ifstream myfile("all.groups");
	    if (myfile.is_open()) {
	        ifstream &in = myfile;
	        check_magic(in, "begin_groups");
	        int no_groups;
	        in >> no_groups;
	        // Number of variables is unknown, as preprocessing may throw out variables,
	        // whereas our input comes directly from the translator. Thus we build an index
	        // that maps for each variable from the number used by the translator (in var name)
	        // to the number used in the preprocessor output for that variable.
	        hash_map<int, int> variable_index;
	        for (int i = 0; i < g_variable_name.size(); i++) {
	            string number = g_variable_name[i].substr(3);
	            int number2 = atoi(number.c_str());
	            variable_index.insert(make_pair(number2, i));
	        }
	        for (int j = 0; j < g_variable_name.size(); j++) {
	            inconsistent_facts.push_back(vector<set<pair<int, int> > > ());
	            for (int k = 0; k < g_variable_domain[j]; k++)
	                inconsistent_facts[j].push_back(set<pair<int, int> > ());
	        }
	        for (int i = 0; i < no_groups; i++) {
	            check_magic(in, "group");
	            int no_facts;
	            in >> no_facts;
	            vector<pair<int, int> > invariant_group;
	            for (int j = 0; j < no_facts; j++) {
	                int var, val, no_args;
	                in >> var >> val;
	                string predicate, endline;
	                in >> predicate >> no_args;
	                vector<string> args;
	                for (int k = 0; k < no_args; k++) {
	                    string arg;
	                    in >> arg;
	                    args.push_back(arg);
	                }
	                getline(in, endline);
	                // Variable may not be in index if it has been discarded by preprocessor
	                if (variable_index.find(var) != variable_index.end()) {
	                    pair<int, int> var_val_pair = make_pair(
	                        variable_index.find(var)->second, val);
	                    invariant_group.push_back(var_val_pair);
	                    // Save fact with predicate name (needed for disj. LMs / 1-step lookahead)
	                    Pddl_proposition prop;
	                    prop.predicate = predicate;
	                    prop.arguments = args;
	                    pddl_propositions.insert(make_pair(var_val_pair, prop));
	                    if (pddl_proposition_indeces.find(predicate)
	                        == pddl_proposition_indeces.end()) {
	                        pddl_proposition_indeces.insert(make_pair(predicate,
	                                                                  pddl_proposition_indeces.size()));
	                    }
	                }
	            }

	            for (int j = 0; j < invariant_group.size(); j++) {
	                for (int k = 0; k < invariant_group.size(); k++) {
	                    if (j == k)
	                        continue;
	                    inconsistent_facts[invariant_group[j].first][invariant_group[j].second].insert(
	                        make_pair(invariant_group[k].first, invariant_group[k].second));
	                }
	            }
	            if(invariant_group.size() == 1) {

                    inconsistent_facts[invariant_group[0].first][0].insert(make_pair(invariant_group[0].first, 1));
                    inconsistent_facts[invariant_group[0].first][1].insert(make_pair(invariant_group[0].first, 0));
                    g_variable_domain_pure[invariant_group[0].first] = 1;
	            }


	        }

	        check_magic(in, "end_groups");
	        myfile.close();
	        external_inconsistencies_read = true;
	        cout << "done" << endl;
	    } else {
	        cout << "Unable to open invariants file!" << endl;
	        exit(1);
	    }


	}

	bool inconsistent(const pair<int, int> &a, const pair<
	                                             int, int> &b) const {

        assert(a.first != b.first || a.second != b.second);
	    if (a.first == b.first && a.second != b.second)
	        return true;
	    set<pair<int, int> > s = inconsistent_facts[a.first][a.second];
	    if (s.find(b) != s.end())
	        return true;
	    return false;
	}

	set<pair<int, int> > getInconsistentSet(const pair<int, int> &a) {

		return inconsistent_facts[a.first][a.second];
	}

	string toString(int var, int val) {

		return pddl_propositions[make_pair(var, val)].to_string();
	}
};

#endif /* INCONSISTENTMAP_H_ */
