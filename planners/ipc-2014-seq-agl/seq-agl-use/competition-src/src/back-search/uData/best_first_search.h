#ifndef BEST_FIRST_SEARCH_H
#define BEST_FIRST_SEARCH_H

#include <vector>

#include "closed_list.h"
#include "../data/open_list.h"
#include "GoalState.h"
#include "search_engine.h"
#include "../data/open_list_buckets.h"
#include "BackSuccessorGenerator.h"

class Heuristic;
class Operator;

typedef pair<const GoalState *, const Operator *> OpenListEntry;

struct OpenListInfo {
    OpenListInfo(Heuristic *heur, bool only_pref);
    Heuristic *heuristic;
    bool only_preferred_operators;
    OpenList<OpenListEntry> *open;
    int priority; // low value indicates high priority
};

class BestFirstSearchEngine : public SearchEngine {
	int temp;
	BackSuccessorGenerator* back_successor_generator;
    std::vector<Heuristic *> heuristics;
    std::vector<Heuristic *> preferred_operator_heuristics;
    std::vector<OpenListInfo> open_lists;
    ClosedList<GoalState, const Operator *> closed_list;

    enum {FAILED, SOLVED, IN_PROGRESS};
    typedef std::vector<const Operator *> Plan;


    std::vector<int> best_heuristic_values;
    int generated_states;

    GoalState current_state;
    const GoalState *current_predecessor;
    const Operator *current_operator;

    bool is_dead_end();
    bool check_goal();
    bool check_progress();
    void report_progress();
    void reward_progress();
    void generate_successors(const GoalState *parent_ptr);
    int fetch_next_state();
    OpenListInfo *select_open_queue();
public:
    virtual void initialize();
    virtual int step();
    BestFirstSearchEngine();
    ~BestFirstSearchEngine();
    virtual void add_heuristic(Heuristic *heuristic, bool use_estimates,
                               bool use_preferred_operators);
    virtual void statistics() const;
    InconsistentMap* get_inconsistent_map() {

    	return back_successor_generator->getInconsitentMap();
    }
};

#endif
