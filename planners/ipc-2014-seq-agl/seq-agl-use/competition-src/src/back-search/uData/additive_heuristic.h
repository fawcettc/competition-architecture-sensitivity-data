#ifndef ADDITIVE_HEURISTIC_H
#define ADDITIVE_HEURISTIC_H

#include "../uData/priority_queue.h"
#include "relaxation_heuristic.h"
#include <cassert>

class AdditiveHeuristic : public RelaxationHeuristic {
    AdaptiveQueue<Proposition *> queue;
    typedef __gnu_cxx::hash_set<int> h_set;
    h_set goal_vars;

    void setup_exploration_queue();
    void setup_exploration_queue_state(const State &state);
    void relaxed_exploration();
    void mark_preferred_operators(const GoalState &state);

    void enqueue_if_necessary(Proposition *prop, int cost, UnaryOperator *op) {
        assert(cost >= 0);
        if (prop->cost == -1 || prop->cost > cost) {
            prop->cost = cost;
            prop->reached_by = op;
            queue.push(cost, prop);
        }
        assert(prop->cost != -1 && prop->cost <= cost);
    }
protected:
    virtual void initialize();
    virtual int compute_heuristic(const GoalState &state);

    // Common part of h^add and h^ff computation.
    int compute_add_and_ff(const GoalState &state);
public:
    AdditiveHeuristic();
    ~AdditiveHeuristic();
    static vector<int> op_ss;

};

#endif
