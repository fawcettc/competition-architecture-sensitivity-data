/*
 * MyOpenList.h
 *
 *  Created on: Nov 18, 2013
 *      Author: R_Sadraei
 */

#ifndef MYOPENLIST_H_
#define MYOPENLIST_H_
#include <deque>
#include <utility>
#include <vector>

template<class Entry>
class MyOpenList {

    typedef std::deque<Entry> Bucket;
    std::vector<Bucket> buckets;
    int lowest_bucket;
    int size;

public:
	MyOpenList();
	~MyOpenList();
    void insert(int key, const Entry& entry);
    bool empty() const;
    void clear();
    pair<int, Entry&> remove_min();

    int get_size() {

    	return size;
    }
};

#include "MyOpenList.cc"

#endif /* MYOPENLIST_H_ */
