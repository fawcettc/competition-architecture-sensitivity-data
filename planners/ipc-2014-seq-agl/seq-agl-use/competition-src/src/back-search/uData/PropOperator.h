/*
 * PropOperator.h
 *
 *  Created on: Nov 18, 2013
 *      Author: R_Sadraei
 */

#ifndef PROPOPERATOR_H_
#define PROPOPERATOR_H_

#include "../data/globals.h"

class PropOperator {

public:

	int operator_index;
	vector<int> addlist;
	vector<int> deletelist;
	vector<int> preconditions;

	PropOperator(int operator_index);

	virtual ~PropOperator();
};

#endif /* PROPOPERATOR_H_ */
