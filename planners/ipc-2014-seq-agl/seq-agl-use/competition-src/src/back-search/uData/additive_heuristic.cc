#include "additive_heuristic.h"

#include "../data/operator.h"
#include "../data/state.h"

#include <cassert>
#include <vector>
using namespace std;

vector<int> AdditiveHeuristic::op_ss;

// construction and destruction
AdditiveHeuristic::AdditiveHeuristic()
    : RelaxationHeuristic() {
	initialize();
	for(int i = 0; i < g_goal.size(); i++) {

		goal_vars.insert(g_goal[i].first);
	}
}

AdditiveHeuristic::~AdditiveHeuristic() {
}

// initialization
void AdditiveHeuristic::initialize() {
    cout << "Initializing additive heuristic..." << endl;
    RelaxationHeuristic::initialize();
    setup_exploration_queue();
    setup_exploration_queue_state(*g_initial_state);
    relaxed_exploration();
}

// heuristic computation
void AdditiveHeuristic::setup_exploration_queue() {
    queue.clear();

    for (int var = 0; var < propositions.size(); var++) {
        for (int value = 0; value < propositions[var].size(); value++) {
            Proposition &prop = propositions[var][value];
            prop.cost = -1;
            prop.marked = false;
        }
    }

    // Deal with operators and axioms without preconditions.
    for (int i = 0; i < unary_operators.size(); i++) {
        UnaryOperator &op = unary_operators[i];
        op.unsatisfied_preconditions = op.precondition.size();
//        assert(op.base_cost != 0);
        op.cost = op.base_cost; // will be increased by precondition costs

        if (op.unsatisfied_preconditions == 0)
            enqueue_if_necessary(op.effect, op.base_cost, &op);
    }
}

void AdditiveHeuristic::setup_exploration_queue_state(const State &state) {
    for (int var = 0; var < propositions.size(); var++) {
        Proposition *init_prop = &propositions[var][state[var]];
        enqueue_if_necessary(init_prop, 0, 0);
    }
}

void AdditiveHeuristic::relaxed_exploration() {
//    int unsolved_goals = goal_propositions.size();
    while (!queue.empty()) {
        pair<int, Proposition *> top_pair = queue.pop();
        int distance = top_pair.first;
        Proposition *prop = top_pair.second;
        int prop_cost = prop->cost;
        assert(prop_cost <= distance);
        if (prop_cost < distance)
            continue;
//        if (prop->is_goal && --unsolved_goals == 0)
//            return;
        const vector<UnaryOperator *> &triggered_operators = prop->precondition_of;
        for (int i = 0; i < triggered_operators.size(); i++) {
            UnaryOperator *unary_op = triggered_operators[i];
            unary_op->unsatisfied_preconditions--;
            unary_op->cost += prop_cost;
            assert(unary_op->unsatisfied_preconditions >= 0);
            if (unary_op->unsatisfied_preconditions == 0)
                enqueue_if_necessary(unary_op->effect, unary_op->cost, unary_op);
        }
    }

	for(int i = 0; i < propositions.size(); i++) {

		assert(propositions[i][(*g_initial_state)[i]].cost == 0);
	}

	int temp = 0;
	for(int i = 0; i < propositions.size(); i++) {
		for(int j = 0; j < propositions[i].size(); j++) {


			if(propositions[i][j].cost == 0)
				temp++;
		}
	}


	op_ss.resize(g_operators.size(), 0);
	for(int i = 0; i < unary_operators.size(); i++) {

		UnaryOperator& op = unary_operators[i];
		int op_index = op.operator_no;
		int op_cost = op.cost;
		if(op_ss[op_index] < op_cost)
			op_ss[op_index] = op_cost;
	}
	cout << propositions.size() << " @@@@@@@@@@@@@@@@@@@@@@@@@@@@ " << temp << endl;

//	assert(temp == propositions.size());

}

void AdditiveHeuristic::mark_preferred_operators(const GoalState &state) {
//    if (!goal->marked) { // Only consider each subgoal once.
//        goal->marked = true;
//        UnaryOperator *unary_op = goal->reached_by;
//        if (unary_op) { // We have not yet chained back to a start node.
//            for (int i = 0; i < unary_op->precondition.size(); i++)
//                mark_preferred_operators(state, unary_op->precondition[i]);
//            int operator_no = unary_op->operator_no;
//            if (unary_op->cost == unary_op->base_cost && operator_no != -1) {
//                // Necessary condition for this being a preferred
//                // operator, which we use as a quick test before the
//                // more expensive applicability test.
//                // If we had no 0-cost operators and axioms to worry
//                // about, this would also be a sufficient condition.
//                const Operator *op = &g_operators[operator_no];
//                if (op->is_applicable(state))
//                    set_preferred(op);
//            }
//        }
//    }

	for(int i = 0; i < propositions.size(); i++) {

		if(state.vars[i] == -1)
			continue;
		Proposition& prop = propositions[i][state.vars[i]];

		UnaryOperator *unary_op = prop.reached_by;
		if(unary_op == 0)
			continue;

		int operator_no = unary_op->operator_no;
		if (operator_no != -1) {

			const Operator *op = &g_operators[operator_no];
//			if (op->is_applicable(state))
			set_preferred(op);
		}
	}
}

int AdditiveHeuristic::compute_add_and_ff(const GoalState &state) {

    int total_cost = 0;
	for(int i = 0; i < propositions.size(); i++) {

		if(state.vars[i] == -1)
			continue;
		Proposition& prop = propositions[i][state.vars[i]];
		if(prop.cost == -1)
			return DEAD_END;
		int d = 1;
		if(goal_vars.count(i) != 0)
			d = 1;
		total_cost += d * prop.cost;
	}
	if(total_cost == 0) {

		for(int i = 0; i < propositions.size(); i++) {

			if(state.vars[i] == -1)
				continue;
//			Proposition& prop = propositions[i][state.vars[i]];
			assert((*g_initial_state)[i] == state.vars[i]);
//			if(prop.cost == -1)
//				return DEAD_END;
//			total_cost += prop.cost;
		}

	}
    return total_cost;
}

int AdditiveHeuristic::compute_heuristic(const GoalState &state) {
    int h = compute_add_and_ff(state);
    if (h != DEAD_END) {
//        for (int i = 0; i < goal_propositions.size(); i++)
            mark_preferred_operators(state);
    }
    return h;
}
