#ifndef SEARCH_ENGINE_H
#define SEARCH_ENGINE_H

#include <vector>

class Heuristic;

#include "../data/operator.h"
#include "../data/operator_cost.h"
#include "GoalState.h"

struct SearchEngineOptions {
    int cost_type;
    int bound;

    SearchEngineOptions();
    ~SearchEngineOptions();

};

class SearchEngine {
public:
    typedef std::vector<const Operator *> Plan;
private:
    bool solved;
    Plan plan;
protected:
    int bound;
    OperatorCost cost_type;

    enum {FAILED, SOLVED, IN_PROGRESS};
    virtual void initialize() {}
    virtual int step() = 0;

    void set_plan(const Plan &plan);
    bool check_goal_and_set_plan(const GoalState &state);
    int get_adjusted_cost(const Operator &op) const;
public:
    SearchEngine();
    virtual ~SearchEngine();
    virtual void statistics() const;
    virtual void heuristic_statistics() const {}
    virtual void save_plan_if_necessary() const;
    bool found_solution() const;
    const Plan &get_plan() const;
    void search();
    void set_bound(int b) {bound = b; }
    int get_bound() {return bound; }
};

#endif
