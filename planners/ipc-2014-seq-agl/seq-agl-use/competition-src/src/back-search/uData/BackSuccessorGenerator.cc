/*
 * BackSearchEngine.cpp
 *
 *  Created on: Nov 18, 2013
 *      Author: R_Sadraei
 */

#include "BackSuccessorGenerator.h"

BackSuccessorGenerator::BackSuccessorGenerator(vector<pair<int, int> > final_goal, State *initial_state) {

	this->initial_state = initial_state;
	this->final_goal = final_goal;
	propositions_num = 0;
	evaluated = 0;
	iMap = new InconsistentMap();
	iMap->read_external_inconsistencies();
	bv.resize(g_operators.size(), false);
}

BackSuccessorGenerator::~BackSuccessorGenerator() {
	// TODO Auto-generated destructor stub

}

GoalState* BackSuccessorGenerator::apply(const GoalState* goalState, const int op_index) {

	Operator* op = unconditioned_operators[op_index];
	GoalState* result = new GoalState(goalState, op);
	return result;
}

void BackSuccessorGenerator::get_applicable_operators(const GoalState& goalState, vector<const Operator *>& all_operators) {

	goh.clear();
	h_set seen;

	for(size_t i = 0; i < g_variable_domain.size(); i++) {

		if(goalState.vars[i] == -1)
			continue;

		int prop = propositions[i][goalState.vars[i]];
		for(size_t j = 0; j < add_by[prop].size(); j++) {

			int op_index = add_by[prop][j];
			seen.insert(op_index);

		}
	}



	for(size_t i = 0; i < g_variable_domain.size(); i++) {

		if(goalState.vars[i] == -1)
			continue;

		int prop = propositions[i][goalState.vars[i]];
		for(size_t j = 0; j < has_conflict[prop].size(); j++) {

			int op_index = has_conflict[prop][j];
			seen.erase(op_index);
		}
	}

	h_set::iterator it = seen.begin();
	for(;it != seen.end(); ++it) {

		int ttt = *it;
		Operator &op = *unconditioned_operators[ttt];
		//TODO: Reza
		for(size_t i = 0; i < op.pre_post.size(); i++) {

			if(goalState.vars[op.pre_post[i].var] != -1)
				assert(op.pre_post[i].post == goalState.vars[op.pre_post[i].var]);

		}


//		if(check_op(&g_operators[ttt])) {
			all_operators.push_back(&g_operators[ttt]);
			goh.push_back(ttt);
//		}
	}
}

void BackSuccessorGenerator::get_applicable_operators1(const GoalState& goalState, vector<const Operator *>& all_operators) {

	h_set seen;


	for(size_t i = 0; i < g_variable_domain.size(); i++) {

		if(goalState.vars[i] == -1)
			continue;

		int prop = propositions[i][goalState.vars[i]];
		for(size_t j = 0; j < add_by[prop].size(); j++) {

			int op_index = add_by[prop][j];
//			seen.insert(op_index);
			if(!bv[op_index]) {

				index.push_back(op_index);
				bv[op_index] == true;
			}
		}
	}

	for(size_t i = 0; i < g_variable_domain.size(); i++) {

		if(goalState.vars[i] == -1)
			continue;

		int prop = propositions[i][goalState.vars[i]];
		for(size_t j = 0; j < has_conflict[prop].size(); j++) {

			int op_index = has_conflict[prop][j];
//			seen.erase(op_index);
			bv[op_index] = false;
		}
	}


//	h_set::iterator it = seen.begin();
//	for(;it != seen.end(); ++it) {
//
//		//TODO: Reza
//		all_operators.push_back(&g_operators[*it]);
//	}

//	for(int i = 0; i < index.size(); i++) {
//
//		if(bv[index[i]]) {
//			all_operators.push_back(&g_operators[index[i]]);
//			bv[index[i]] = false;
//		}
//	}
	index.clear();
}

void BackSuccessorGenerator::build_meta_data() {

	precondition_of.resize(propositions_num);
	add_by.resize(propositions_num);

	for(size_t i = 0; i < prop_operators.size(); i++) {

		PropOperator* op = prop_operators[i];
		for(size_t j = 0; j < op->preconditions.size(); j++) {

			precondition_of[op->preconditions[j]].push_back(i);
		}
		for(size_t j = 0; j < op->addlist.size(); j++) {

			add_by[op->addlist[j]].push_back(i);
		}
	}

	has_conflict.resize(propositions_num);

	for(size_t i = 0; i < unconditioned_operators.size(); i++) {

		Operator* op = unconditioned_operators[i];
		for(size_t j = 0; j < op->prevail.size(); j++) {

			int var = op->prevail[j].var;
			int val = op->prevail[j].prev;
			set<pair<int, int> > pSet = iMap->getInconsistentSet(make_pair(var, val));

			set<pair<int, int> >::iterator it = pSet.begin();

			for (; it != pSet.end(); ++it) {

				has_conflict[propositions[(*it).first][(*it).second]].push_back(i);
			}
		}
		for(size_t j = 0; j < op->pre_post.size(); j++) {

			int var = op->pre_post[j].var;
			int val = op->pre_post[j].post;
			set<pair<int, int> > pSet = iMap->getInconsistentSet(make_pair(var, val));

			set<pair<int, int> >::iterator it = pSet.begin();

			for (; it != pSet.end(); ++it) {

				has_conflict[propositions[(*it).first][(*it).second]].push_back(i);
			}
		}
	}
}

void BackSuccessorGenerator::build_prop_operators() {

	for(size_t i = 0; i < unconditioned_operators.size(); i++) {

		Operator* op = unconditioned_operators[i];
		PropOperator* prop_operator = new PropOperator(i);
		for(size_t j = 0; j < op->prevail.size(); j++) {

			int prop = propositions[op->prevail[j].var][op->prevail[j].prev];
			prop_operator->preconditions.push_back(prop);
		}

		for(size_t j = 0; j < op->pre_post.size(); j++) {

			if(op->pre_post[j].pre != -1) {

				int prop = propositions[op->pre_post[j].var][op->pre_post[j].pre];
				prop_operator->preconditions.push_back(prop);
				prop_operator->deletelist.push_back(prop);
			}

			int prop = propositions[op->pre_post[j].var][op->pre_post[j].post];
			prop_operator->addlist.push_back(prop);
		}
		prop_operators.push_back(prop_operator);
	}

	assert(prop_operators.size() == g_operators.size());
	assert(g_axioms.size() == 0);
}

void BackSuccessorGenerator::build_propositions() {

	propositions_num = 0;
	propositions.resize(g_variable_domain.size());
	for (size_t var = 0; var < g_variable_domain.size(); var++) {
		for (int value = 0; value < g_variable_domain[var]; value++) {

			propositions[var].push_back(propositions_num++);
		}
	}
}

void BackSuccessorGenerator::build_unconditioned_operators() {

	for(size_t i = 0; i < g_operators.size(); i++)
		build_unconditioned_operators(g_operators[i]);
}

void BackSuccessorGenerator::build_unconditioned_operators(Operator &op) {

	const vector<PrePost> &pre_post = op.get_pre_post();
	vector<PrePost> conditioned_preposts;
	vector<PrePost> unconditioned_preposts;

	for (size_t i = 0; i < pre_post.size(); i++) {
		if(pre_post[i].cond.size() != 0)
			conditioned_preposts.push_back(pre_post[i]);
		else
			unconditioned_preposts.push_back(pre_post[i]);
	}

	for (size_t i = 0; i < conditioned_preposts.size(); i++) {

		vector<PrePost> pre_posts;
		vector<Prevail> prevails;
		for(size_t j = 0; j < op.prevail.size(); j++) {

			prevails.push_back(* new Prevail(op.prevail[j]));
		}

		for(size_t j = 0; j < unconditioned_preposts.size(); j++) {

			PrePost* pre_post = new PrePost(unconditioned_preposts[j]);
			pre_posts.push_back(*pre_post);
		}

		PrePost* pre_post = new PrePost(conditioned_preposts[i]);
		pre_posts.push_back(*pre_post);

		Operator* new_op = new Operator(op.get_name(), false, op.get_cost());
		new_op->prevail.insert(new_op->prevail.end(), prevails.begin(), prevails.end());
		new_op->pre_post.insert(new_op->pre_post.end(), pre_posts.begin(), pre_posts.end());
		unconditioned_operators.push_back(new_op);
	}

	vector<PrePost> pre_posts;
	vector<Prevail> prevails;
	for(size_t j = 0; j < op.prevail.size(); j++) {

		prevails.push_back(* new Prevail(op.prevail[j]));
	}

	for(size_t j = 0; j < unconditioned_preposts.size(); j++) {

		PrePost* pre_post = new PrePost(unconditioned_preposts[j]);
		pre_posts.push_back(*pre_post);
	}

	Operator* new_op = new Operator(op.get_name(), false, op.get_cost());
	new_op->prevail.insert(new_op->prevail.end(), prevails.begin(), prevails.end());
	new_op->pre_post.insert(new_op->pre_post.end(), pre_posts.begin(), pre_posts.end());
	unconditioned_operators.push_back(new_op);
}

void BackSuccessorGenerator::check_all() {

	for(size_t i = 0; i < g_operators.size(); i++) {

		Operator op = g_operators[i];
		PropOperator* pop = prop_operators[i];

		for(size_t j = 0; j < op.pre_post.size(); j++) {

			if(op.pre_post[j].pre != -1)
				assert (contains(pop->preconditions, op.pre_post[j].var, op.pre_post[j].pre));

			assert (contains(pop->addlist, op.pre_post[j].var, op.pre_post[j].post));

			if(op.pre_post[j].pre != -1)
				assert (contains(pop->deletelist, op.pre_post[j].var, op.pre_post[j].pre));

		}

		for(size_t j = 0; j < op.prevail.size(); j++)
			assert (contains(pop->preconditions, op.prevail[j].var, op.prevail[j].prev));

	}
}

bool BackSuccessorGenerator::contains(vector<int>& vec, int var, int val) {

	for(size_t i = 0; i < vec.size(); i++)
		if(vec[i] == propositions[var][val])
			return true;
	return false;

}

void BackSuccessorGenerator::initialize() {

	build_unconditioned_operators();
	build_propositions();
	build_prop_operators();
	build_meta_data();
}


