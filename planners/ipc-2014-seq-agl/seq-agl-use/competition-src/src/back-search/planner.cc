#include <iostream>
#include <fstream>
#include "data/globals.h"
#include "data/operator.h"
#include "uData/GoalState.h"
#include "uData/BackSuccessorGenerator.h"
#include <ext/hash_map>
#include <ext/hash_set>
#include <string>
#include "uData/Util.h"
#include "data/timer.h"
#include "uData/InconsistentMap.h"
#include "uData/best_first_search.h"
#include "uData/additive_heuristic.h"
#include "uData/search_engine.h"
#include "uData/ff_heuristic.h"
#include "lm/landmarks_graph.h"
#include "lm/landmarks_graph_rpg_sasp.h"
#include "lm/exploration.h"
#include "lm/landmark_count_heuristic.h"
using namespace std;

vector<Operator> unconditioned_operators;
vector<vector<int> > precondition_of;
vector<vector<int> > propositions;

int main(int argc, const char **argv) {

	if(exists_test("temp.t"))
		return 0;
		
	bool poly_time_method = false;
	istream &in = cin;
	in >> poly_time_method;
	if (poly_time_method) {
		cout << "Poly-time method not implemented in this branch." << endl;
		cout << "Starting normal solver." << endl;
	}

	read_everything(in);

	LandmarksGraph::LandmarkGraphOptions *lo = new LandmarksGraph::LandmarkGraphOptions();
	lo->reasonable_orders = true;
	lo->only_causal_landmarks = false;
	lo->disjunctive_landmarks = true;
	lo->conjunctive_landmarks = false;
	lo->reasonable_orders = true;
	LandmarksGraph *graph = new LandmarksGraphNew(*lo, new Exploration());
	LandmarksGraph::build_lm_graph(graph);

	LandmarkCountHeuristic* lm_count_heu = new LandmarkCountHeuristic(*graph, false, false, false, false);

	BestFirstSearchEngine* search_engine = new BestFirstSearchEngine();
	g_initial_state->dump(search_engine->get_inconsistent_map());

	//	search_engine->add_heuristic(new GoalCountHeuristic(), true, false);
	search_engine->add_heuristic(new AdditiveHeuristic(), true, false);
	search_engine->add_heuristic(lm_count_heu, true, false);
	//	search_engine->add_heuristic(new FFHeuristic(), true, true);


	for (int i = 1; i < argc; ++i) {
		string arg = string(argv[i]);
		if (arg.compare("--random-seed") == 0) {
			++i;
			srand(atoi(argv[i]));
			cout << "random seed " << argv[i] << endl;
		} else if (arg.compare("--plan-file") == 0) {
			++i;
			g_plan_filename = argv[i];
		} else {
			cerr << "unknown option " << arg << endl << endl;
			exit(1);
		}
	}

	Timer search_timer;
	search_engine->search();
	search_timer.stop();
	g_timer.stop();
	search_engine->save_plan_if_necessary();
	search_engine->statistics();
	search_engine->heuristic_statistics();
    cout << "Search time: " << search_timer << endl;
    cout << "Total time: " << g_timer << endl;

    return search_engine->found_solution() ? 0 : 1;
}
