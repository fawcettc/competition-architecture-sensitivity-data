#ifndef LANDMARKS_LANDMARK_STATUS_MANAGER_H
#define LANDMARKS_LANDMARK_STATUS_MANAGER_H

#include "landmarks_graph.h"
#include "../uData/GoalState.h"
#include "state_proxy.h"

class LandmarkStatusManager {
private:
    hash_map<GoalStateProxy, vector<bool> > reached_lms;

    bool do_intersection;
    LandmarksGraph &lm_graph;

    bool landmark_is_leaf(const LandmarkNode &node, const vector<bool> &reached) const;
//    bool check_lost_landmark_children_needed_again(const LandmarkNode &node) const;
    bool check_lost_landmark_parents_needed_again(const LandmarkNode &node) const;
public:
    LandmarkStatusManager(LandmarksGraph &graph);
    virtual ~LandmarkStatusManager();

    void clear_reached();
    vector<bool> &get_reached_landmarks(const GoalState &state);

    bool update_lm_status(const GoalState &state);

    void set_landmarks_for_initial_state();
    bool update_reached_lms(const GoalState &parent_state, const Operator &op, const GoalState &state);
};

#endif
