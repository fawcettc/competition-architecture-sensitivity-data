#!/usr/bin/env bash

BASEDIR="$(dirname "$0")"
MEMLIMIT=$4

#translate pddl to sas
python $BASEDIR/translate/translate.py $1 $2

#output.sas is the sasfile, solve
java -Xmx${MEMLIMIT}m -XX:MetaspaceSize=64m -XX:MaxMetaspaceSize=128m -jar $BASEDIR/freelunch.jar output.sas $3
