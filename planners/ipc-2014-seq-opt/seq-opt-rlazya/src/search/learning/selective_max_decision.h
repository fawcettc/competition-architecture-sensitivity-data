#ifndef LEARNING_SELECTIVE_MAX_DECISION_H
#define LEARNING_SELECTIVE_MAX_DECISION_H

#include "../heuristic.h"
#include "../globals.h"
#include "../search_space.h"
#include "../timer.h"
#include "classifier.h"
#include "feature_extractor.h"
#include "state_space_sample.h"
#include <math.h>

class SelectiveMaxDecision {
    vector<Heuristic *> heuristics;
    int *hvalue;
    bool *computed;
    int num_always_calc;

    //statistics
    int num_evals;
    int correct0_classifications;
    int incorrect0_classifications;
    int correct1_classifications;
    int incorrect1_classifications;

    int training_set_size;
    int num_learn_from_no_confidence;
    double *total_computation_time;
    double *avg_time;
    ExactTimer total_classification_time;
    ExactTimer total_training_time;

    int *num_evaluated;
    int *num_winner;
    int *num_only_winner;
    double branching_factor;
    int num_pairs;
    bool test_mode;
    bool perfect_mode;
    bool zero_threshold;

    int goal_depth_estimate;
    int goal_cost_estimate;
    Heuristic *goal_depth_heuristic;


    //int current_min_training_set;

    // parameters
    int min_training_set;
    double conf_threshold;
    bool uniform_sampling;
    bool random_selection;
    double alpha;
    bool retime_heuristics;

    int num_heuristics;

    // classification
    state_space_sample_t state_space_sample_type;

    classifier_t classifier_type;
    Classifier **classifiers;

    FeatureExtractor *feature_extractor;
    FeatureExtractorFactory feature_extractor_types;

    int *expensive;
    int *cheap;
    double *threshold;
    double *dist;
    double *heuristic_conf;
    int num_classes;

    bool VOI_mode;
    //double C;
public:
    SelectiveMaxDecision(bool VOI_mode_);
    virtual ~SelectiveMaxDecision();
    void add_heuristic(Heuristic *h) {
        heuristics.push_back(h);
        num_heuristics++;
    }

    void train();
    int classify(const State &state);
    void learn(const State &state, int h1_value, int h2_value);

    //int voi_decision(const State &state, int g, int h);
    int voi_decision(const State &s,
            int ihc_expanded_after_all_heuristics,
            const Heuristic &h1, const Heuristic &h2);
};

#endif
