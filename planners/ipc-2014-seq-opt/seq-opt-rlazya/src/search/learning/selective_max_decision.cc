#include "selective_max_decision.h"

#include "selective_max_heuristic.h"
#include "../successor_generator.h"
#include "maximum_heuristic.h"
#include "naive_bayes_classifier.h"
#include "state_vars_feature_extractor.h"
#include "composite_feature_extractor.h"
#include "AODE.h"
#include "probe_state_space_sample.h"
#include "PDB_state_space_sample.h"
#include "../option_parser.h"
#include "../plugin.h"
#include "../ff_heuristic.h"

#include <cassert>
#include <limits>



SelectiveMaxDecision::SelectiveMaxDecision(bool VOI_mode_):
    VOI_mode(VOI_mode_)
{
    goal_depth_estimate = 0;
    goal_cost_estimate = 0;

    num_heuristics = 0;
    min_training_set = 100;
    conf_threshold = 0.6;
    uniform_sampling = false;
    random_selection = false;
    alpha = 1.0;
    classifier_type = NB;
    state_space_sample_type = Probe;
    retime_heuristics = false;

    // statistics
    num_evals = 0;
    correct0_classifications = 0;
    incorrect0_classifications = 0;
    correct1_classifications = 0;
    incorrect1_classifications = 0;
    training_set_size = 0;
    num_learn_from_no_confidence = 0;
    // timing
    total_classification_time.reset();
    total_classification_time.stop();
    total_training_time.reset();
    total_training_time.stop();


    //test_mode = false;
    //perfect_mode = g_test_mode;

    test_mode = false;    //g_test_mode;
    perfect_mode = false;
    zero_threshold = false;

    goal_depth_heuristic = 0;
}

SelectiveMaxDecision::~SelectiveMaxDecision() {
    for (int i = 0; i < num_pairs; i++) {
        delete classifiers[i];
    }
    delete total_computation_time;
    delete classifiers;
    delete expensive;
    delete cheap;
    delete threshold;
    delete heuristic_conf;
    delete dist;
    delete goal_depth_heuristic;
}

void SelectiveMaxDecision::train() {
    if (VOI_mode) {
        return;
    }

    cout << "Beginning Training" << endl;
    total_computation_time = new double[num_heuristics];


    num_classes = 2;
    num_pairs = (num_heuristics * (num_heuristics - 1)) / 2;
    classifiers = new Classifier *[num_pairs];
    expensive = new int[num_pairs];
    cheap = new int[num_pairs];
    threshold = new double[num_pairs];
    heuristic_conf = new double[num_heuristics];
    dist = new double[num_classes];

    feature_extractor = feature_extractor_types.create();
    cout << "Number of features: " << feature_extractor->get_num_features() << endl;

    for (int i = 0; i < num_pairs; i++) {
        switch (classifier_type) {
        case NB:
            classifiers[i] = new NBClassifier();
            break;
        case AODE:
            classifiers[i] = new AODEClassifier();
            break;
        default:
            cerr << "Unknown classifier type" << endl;
            exit(353);
        }
        classifiers[i]->set_feature_extractor(feature_extractor);
        classifiers[i]->buildClassifier(num_classes);
        threshold[i] = 0;
    }


    total_training_time.reset();

    MaxHeuristic max(Heuristic::default_options());
    for (int i = 0; i < num_heuristics; i++) {
        max.add_heuristic(heuristics[i]);
    }

    // initialize all heuristics

    max.evaluate(*g_initial_state);
    int h0 = max.get_heuristic();
    goal_cost_estimate = 2 * h0;

    if (g_use_metric) {
        Options opts = Heuristic::default_options();
        opts.set<int>("cost_type", 1);
        goal_depth_heuristic = new FFHeuristic(opts);
        goal_depth_heuristic->evaluate(*g_initial_state);
        goal_depth_estimate = 2 * goal_depth_heuristic->get_heuristic();
    } else {
        goal_depth_estimate = goal_cost_estimate;
    }

    cout << "goal depth estimate: " << goal_depth_estimate << endl;

    if (min_training_set == 0) {
        min_training_set = 5 * goal_depth_estimate;
    }

    //cout << "Min Training Set Size: " << min_training_set << endl;


    cout << "Starting Training Example Collection" << endl;
    StateSpaceSample *sample;

    switch (state_space_sample_type) {
    case Probe:
        sample = new ProbeStateSpaceSample(goal_depth_estimate,
                                           2 * min_training_set / goal_depth_estimate,
                                           min_training_set);
        for (int i = 0; i < heuristics.size(); i++) {
            sample->add_heuristic(heuristics[i]);
        }
        break;
    /*
    case ProbAStar:
            sample = new ProbAStarSample(&max, min_training_set);
            break;
    */
    case PDB:
        sample = new PDBStateSpaceSample(goal_depth_estimate,
                                         min_training_set,
                                         min_training_set);
        for (int i = 0; i < heuristics.size(); i++) {
            sample->add_heuristic(heuristics[i]);
        }
        break;
    default:
        cerr << "Unknown state space sample" << endl;
        exit(931);
    }

    sample->collect();

    sample_t &training_set = sample->get_samp();
    training_set_size = training_set.size();
    branching_factor = sample->get_branching_factor();

    cout << "Training Example Collection Finished" << endl;
    cout << "Branching Factor: " << branching_factor << endl;
    cout << "Training Set Size: " << training_set_size << endl;

    // Get Heuristic Evaluation Times and Values

    //cout << "Collecting Timing Data" << endl;
    ExactTimer retimer;
    for (int i = 0; i < num_heuristics; i++) {
        total_computation_time[i] = sample->get_computation_time(i) + 1;
        if (retime_heuristics) {
            total_computation_time[i] = 0;
            double before = retimer();
            sample_t::const_iterator it;
            for (it = training_set.begin(); it != training_set.end(); it++) {
                const State s = (*it).first;
                heuristics[i]->evaluate(s);
            }
            double after = retimer();
            total_computation_time[i] = after - before + 1;
        }
        cout << "Time " << i << " - " << total_computation_time[i] << endl;
    }
    //cout << "Timing Data Collected" << endl;


    cout << "Beginning Training Classifier" << endl;
    // Calculate Thresholds
    int classifier_index = 0;
    cout << "Thresholds" << endl;
    for (int i = 0; i < num_heuristics; i++) {
        for (int j = i + 1; j < num_heuristics; j++) {
            cout << "Training pair: " << i << ", " << j << endl;
            if (total_computation_time[i] > total_computation_time[j]) {
                expensive[classifier_index] = i;
                cheap[classifier_index] = j;
            } else {
                expensive[classifier_index] = j;
                cheap[classifier_index] = i;
            }

            double thr = alpha *
                         log((double)total_computation_time[expensive[classifier_index]] /
                             (double)total_computation_time[cheap[classifier_index]]) /
                         log(branching_factor);

            if (zero_threshold) {
                thr = 0;
            }

            threshold[classifier_index] = thr;

            cout << expensive[classifier_index] /*heuristics[expensive[classifier_index]]->get_name()*/ <<
            " <=> " << expensive[classifier_index] /*heuristics[expensive[classifier_index]]->get_name()*/ << " - " <<
            cheap[classifier_index] /*heuristics[cheap[classifier_index]]->get_name()*/ << " > " << thr << endl;
            //training_set.reset_iterator();

            sample_t::const_iterator it;
            //while (training_set.has_more_states()) {
            for (it = training_set.begin(); it != training_set.end(); it++) {
                //const State s = training_set.get_next_state();
                const State s = (*it).first;
                int value_expensive = training_set[s][expensive[classifier_index]];
                int value_cheap = training_set[s][cheap[classifier_index]];

                int tag = 0;
                int diff = value_expensive - value_cheap;
                if (diff > threshold[classifier_index]) {
                    tag = 1;
                }
                classifiers[classifier_index]->addExample(&s, tag);
            }
            classifier_index++;
        }
    }

    cout << "Finished Training Classifier" << endl;

    //training_set.resize(0);
    delete sample;


    total_training_time.stop();

    // reset statistics and heuristics after training
    for (int i = 0; i < num_heuristics; i++) {
        heuristics[i]->reset();
    }

    //cout << "Freed Memory" << endl;
}

int SelectiveMaxDecision::classify(const State &state) {
    total_classification_time.resume();
    double max_confidence = 0.0;
    int best_heuristics = -1;

    for (int i = 0; i < num_heuristics; i++) {
        heuristic_conf[i] = 0.0;
    }

    for (int classifier_index = 0; classifier_index < num_pairs; classifier_index++) {
        classifiers[classifier_index]->distributionForInstance(&state, dist);
        // get confidence for h_i better than h_j and for h_j better than h_i
        double confidence_expensive = dist[1];
        double confidence_cheap = dist[0];

        heuristic_conf[expensive[classifier_index]] += confidence_expensive;
        heuristic_conf[cheap[classifier_index]] += confidence_cheap;
    }


    for (int i = 0; i < num_heuristics; i++) {
        //cout << i << " " << heuristic_conf[i]  << endl;
        if (heuristic_conf[i] > max_confidence) {
            max_confidence = heuristic_conf[i];
            best_heuristics = i;
        }
    }

    total_classification_time.stop();

    if (max_confidence > (conf_threshold * (num_heuristics - 1))) {
        // if there is a decision over the confidence threshold, use it
        return best_heuristics;
    }
    num_learn_from_no_confidence++;
    return -1;
}

void SelectiveMaxDecision::learn(const State &state, int h1_value, int h2_value) {
    assert(num_heuristics == 2);

    int diff = h2_value - h1_value;
    int tag = 0;
    if (diff > threshold[0]) {
        tag = 1;
    }

    training_set_size++;
    classifiers[0]->addExample(&state, tag);
}


//int SelectiveMaxDecision::voi_decision(const State &s, int g, int h) {
//    if (h == 0)
//        return 0;
//
//    int h_steps = h;
//    if (g_use_metric) {
//        goal_depth_heuristic->evaluate(s);
//        h_steps = goal_depth_heuristic->get_heuristic();
//    }
//
//    double left_side = branching_factor * h_steps * exp(- (goal_cost_estimate - g) / h);
//
//    double right_side = total_computation_time[1] /
//            (total_computation_time[0] + (alpha * total_computation_time[1]));
//
//    //cout << left_side << "  " << right_side << "    -   " << (left_side > right_side) << endl;
//    if (left_side > right_side) {
//        return 1;
//    } else {
//        return 0;
//    }
//}


int SelectiveMaxDecision::voi_decision(const State &s,
        int ihc_expanded_after_all_heuristics,
        const Heuristic &h1, const Heuristic &h2) {

    const int SIMULATED_EXAMPLES = 1000;
    const double SIMULATED_P_HELPFUL = 0.5;

    if ((h1.get_num_evaluations() < 1) || (h2.get_num_evaluations() < 1))
        return 1;



    double tr = h2.get_avg_computation_time() / h1.get_avg_computation_time();
    double p_helpful_obs = 1.0 - (1.0 * ihc_expanded_after_all_heuristics / h2.get_num_evaluations());

    double p_helpful = ((p_helpful_obs * h2.get_num_evaluations()) + (SIMULATED_P_HELPFUL * SIMULATED_EXAMPLES)) / (SIMULATED_EXAMPLES + h2.get_num_evaluations());

    double C = tr / (p_helpful * (1 + tr));

//    cout << "tr = " << tr << "\tp_helpful =  " << p_helpful << "\tVOI threshold = " << C <<
//            "\t\t " << ihc_expanded_after_all_heuristics << " / " << h2.get_num_evaluations() << endl;

    if (C <= 1)
        return 1;

    vector<const Operator *> ops;
    g_successor_generator->generate_applicable_ops(s, ops);
    int num_successors = ops.size();

    if (num_successors > C) {
        return 1;
    } else {
        return 0;
    }

}
