#include <cassert>

#include "stored_heuristic_value_feature_extractor.h"
#include "../globals.h"
#include "../state.h"

StoredHeuristicValueFeatureExtractor::StoredHeuristicValueFeatureExtractor(
        SearchSpace &search_space_,
        int feature_domain_size_):
    search_space(search_space_),
    feature_domain_size(feature_domain_size_)
{
}

StoredHeuristicValueFeatureExtractor::~StoredHeuristicValueFeatureExtractor() {
}

int StoredHeuristicValueFeatureExtractor::get_num_features() {
    return 1;
}

int StoredHeuristicValueFeatureExtractor::get_feature_domain_size(int ) {
    return feature_domain_size;
}

void StoredHeuristicValueFeatureExtractor::extract_features(const void *obj,
                                                vector<int> &features) {
    const State &state = *((const State *)obj);

    if (search_space.get_node(state).is_dead_end()) {
        features.push_back(feature_domain_size - 1);
    } else {
        int h = search_space.get_node(state).get_h();
        if (h >= feature_domain_size) {
            features.push_back(feature_domain_size - 1);
        } else {
            features.push_back(h);
        }
    }
}
