#ifndef LEARNING_SETTABLE_FEATURE_EXTRACTOR_H
#define LEARNING_SETTABLE_FEATURE_EXTRACTOR_H

#include "feature_extractor.h"
#include "../search_space.h"

class StoredHeuristicValueFeatureExtractor : public FeatureExtractor {
    SearchSpace &search_space;
    int feature_domain_size;
public:
    StoredHeuristicValueFeatureExtractor(
            SearchSpace &search_space_,
            int feature_domain_size_);
    virtual ~StoredHeuristicValueFeatureExtractor();

    virtual int get_num_features();
    virtual int get_feature_domain_size(int feature);
    virtual void extract_features(const void *obj, vector<int> &features);
};

#endif
