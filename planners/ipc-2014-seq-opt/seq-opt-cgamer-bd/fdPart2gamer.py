#!/usr/bin/python

import sys

f = open(sys.argv[1], "r")
counter = 1
add = True
for l in f.readlines():
    if l.strip() == "":
        print ("[]")
        add = True
    else:
        if add: 
            print ("[]")
            add = False
            
        if l.startswith("<none of those>") or l.startswith("NegatedAtom "):
            print ("(none-of-these-" + str(counter) + ")")
            counter += 1
        else:
            print (l.replace("()", ")").replace("(", ".").replace(", ", ".").replace("Atom ", "(").replace("\n", ""))

f.close()
