#!/bin/bash

rm  -f "./action_costs_from_domain_file.txt" 
rm  -f "./action_costs_from_problem_file.txt" 

rm  -f "./domain_ready.txt" 
rm  -f "./domain_wce.txt" 
rm  -f "./domain_without_action_costs.txt" 
rm  -f "./facts_wce.txt" 
rm  -f "./problem_ready.txt" 
rm  -f "./problem_without_action_costs.txt" 
rm  -f "./parser_log.txt" 
