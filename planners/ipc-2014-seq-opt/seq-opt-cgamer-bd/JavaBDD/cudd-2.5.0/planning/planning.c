#include "planning.h"

DdNode * cuddBddAndAllRecur(DdManager * manager, int iniNode, int numNodes)
{
  printf("Recur call %d %d \n", iniNode, numNodes);
  statLine(manager);
  DdNode * one = DD_ONE(manager);
  // printf("Node one: %p\n", one);

  DdNode * f, *F, *g, *G, *t, *e, *r;
  DdNode * ft, *fe;

  int i, j;
  int index = -1;
  int pos = 0;

  /* Filter repeated nodes */
  for(i = 0; i < numNodes; i++){
    f = nodes[iniNode + i];
    F = Cudd_Regular(f);
    //printf("Node %d: %p %p\n", iniNode + i, f, F);

    if (F == one) { //f is zero or one
      if (f == one){ //f is one: skip it 
	//printf("Constant 1\n");
	continue;
      }else{
	printf("Return constant 0\n");
	
	return(f); //Return zero
      }
    }
    //printf("Node index %d\n", F->index);
    // printf("Check repeated nodes\n");
    //Insert in the right position (between 0 and pos) 
    int repeated = 0;
    for(j = 0; j < pos; j++){
      g = nodes[iniNode + j];
      G = Cudd_Regular(g);
      if (F == G) {
	if (f == g){
	  repeated = 1;
	  break;
	}else{
	  printf("Return constant 0: Oposite BDDs\n");
	  return(Cudd_Not(one));
	}
      }
    }

    if(repeated) continue;
    // printf("Updating index\n");
    if(index == -1){
      index = manager->perm[F->index];	
    }else{      
      //printf("Here %d\n", F->index);
      //printf("Here2 %d\n", manager->perm[F->index]);
      index = (index <= manager->perm[F->index] ? index : manager->perm[F->index]);
    }
    //printf("Updated index %d\n", index);

    nodes[iniNode + pos++] = f;
  }

  // printf("We have %d nodes\n", pos);
  /* Terminal cases. */
  if(pos == 0){
    printf("Return one: Empty clause\n");
    return one;
  }else if (pos == 1){
    printf("Return base bdd: Only clause\n");
    return nodes[iniNode];
  }/*else if(pos == 2){
    return cuddBddAndRecur(manager, nodes[iniNode], nodes[iniNode + 1]);
    }*/

  //NodesT start in iniNode + 2*pos, nodesE start in iniNode + pos
  int iniT = iniNode + 2*pos;
  int iniE = iniNode + pos;
  for(i = 0; i < pos; i++){
    f = nodes[iniNode + i];
    F = Cudd_Regular(f);

    if(index == manager->perm[F->index]){
      ft = cuddT(F);
      fe = cuddE(F);
      if (Cudd_IsComplement(f)) {
	ft = Cudd_Not(ft);
	fe = Cudd_Not(fe);
      }
      nodes [iniT + i] = ft;
      nodes [iniE + i] = fe;
    }else{
      nodes [iniT + i] = f;
      nodes [iniE + i] = f;
    }
  }

  t = cuddBddAndAllRecur(manager, iniT, pos);
  if (t == NULL) return(NULL);
  cuddRef(t);

  e = cuddBddAndAllRecur(manager, iniE, pos);
  if (e == NULL) {
    Cudd_IterDerefBdd(manager, t);
    return(NULL);
  }
  cuddRef(e);

  if (t == e) {
    printf("Return node t=e\n");
    r = t;
  } else {
    printf("Return new node\n");
    if (Cudd_IsComplement(t)) {
      r = cuddUniqueInter(manager,(int)index,Cudd_Not(t),Cudd_Not(e));
      if (r == NULL) {
	Cudd_IterDerefBdd(manager, t);
	Cudd_IterDerefBdd(manager, e);
	return(NULL);
      }
      r = Cudd_Not(r);
    } else {
      r = cuddUniqueInter(manager,(int)index,t,e);
      if (r == NULL) {
	Cudd_IterDerefBdd(manager, t);
	Cudd_IterDerefBdd(manager, e);
	return(NULL);
      }
    }
  }
  cuddDeref(e);
  cuddDeref(t);

  //printf("Returned node  ");
  //  Cudd_PrintDebug(manager, r, 0, 3);
  //printf("\n\n");

  return(r);
}





DdNode * cuddBddAndAllRecurTimeout(DdManager * manager, int iniNode, int numNodes, long maxTime)
{
  statLine(manager);
  DdNode * one = DD_ONE(manager);

  DdNode * f, *F, *g, *G, *t, *e, *r;
  DdNode * ft, *fe;

  int i, j;
  int index = -1;
  int pos = 0;
  if(util_cpu_time() > maxTime){
    printf("Timeout reached\n");
    return NULL;
  }

  /* Filter repeated nodes */
  for(i = 0; i < numNodes; i++){
    f = nodes[iniNode + i];
    F = Cudd_Regular(f);
    if (F == one) { //f is zero or one
      if (f == one){ //f is one: skip it 
	continue;
      }else{
	return(f); //Return zero
      }
    }

    //Insert in the right position (between 0 and pos) 
    int repeated = 0;
    for(j = 0; j < pos; j++){
      g = nodes[iniNode + j];
      G = Cudd_Regular(g);
      if (F == G) {
	if (f == g){
	  repeated = 1;
	  break;
	}else{
	  return(Cudd_Not(one));
	}
      }
    }

    if(repeated) continue;
      
    if(index == -1){
      index = manager->perm[F->index];	
    }else{   
      index = (index <= manager->perm[F->index] ? index : manager->perm[F->index]);
    }
    nodes[iniNode + pos++] = f;
  }

  /* Terminal cases. */
  if(pos == 0){
    return one;
  }else if (pos == 1){
    return nodes[iniNode];
  }else if(pos == 2){
    return cuddBddAndRecur(manager, nodes[iniNode], nodes[iniNode + 1]);
  }

  //NodesT start in iniNode + 2*pos, nodesE start in iniNode + pos
  int iniT = iniNode + 2*pos;
  int iniE = iniNode + pos;
  for(i = 0; i < pos; i++){
    f = nodes[iniNode + i];
    F = Cudd_Regular(f);

    if(index == manager->perm[F->index]){
      ft = cuddT(F);
      fe = cuddE(F);
      if (Cudd_IsComplement(f)) {
	ft = Cudd_Not(ft);
	fe = Cudd_Not(fe);
      }
      nodes [iniT + i] = ft;
      nodes [iniE + i] = fe;
    }else{
      nodes [iniT + i] = f;
      nodes [iniE + i] = f;
    }
  }

  t = cuddBddAndAllRecur(manager, iniT, pos);
  if (t == NULL) return(NULL);
  cuddRef(t);

  e = cuddBddAndAllRecur(manager, iniE, pos);
  if (e == NULL) {
    Cudd_IterDerefBdd(manager, t);
    return(NULL);
  }
  cuddRef(e);

  if (t == e) {
    r = t;
  } else {
    if (Cudd_IsComplement(t)) {
      r = cuddUniqueInter(manager,(int)index,Cudd_Not(t),Cudd_Not(e));
      if (r == NULL) {
	Cudd_IterDerefBdd(manager, t);
	Cudd_IterDerefBdd(manager, e);
	return(NULL);
      }
      r = Cudd_Not(r);
    } else {
      r = cuddUniqueInter(manager,(int)index,t,e);
      if (r == NULL) {
	Cudd_IterDerefBdd(manager, t);
	Cudd_IterDerefBdd(manager, e);
	return(NULL);
      }
    }
  }
  cuddDeref(e);
  cuddDeref(t);

  return(r);
}



DdNode *
Cudd_bddOrAllTimeout(DdManager * dd,
		     int numBdds, DdNode ** bdds,
		     long timeout){
  DdNode *res;

  nodes = malloc (sizeof(DdNode*)*numBdds*1000);

  int i;
  for(i = 0; i <numBdds; i++){
    nodes [i] = Cudd_Not(bdds[i]);
  }

  do {
    dd->reordered = 0;
    res = cuddBddAndAllRecurTimeout(dd, 0, numBdds, util_cpu_time() + timeout);
  } while (dd->reordered == 1);
  res = Cudd_NotCond(res,res != NULL);
  free(nodes);
  return(res);

} /* end of Cudd_bddOr */



DdNode *
Cudd_bddOrAll(DdManager * dd,
	      int numBdds, DdNode ** bdds){
  DdNode *res;

  int i;
  //  printf("Done malloc for array: %d \n", sizeof(DdNode*)*numBdds*1000 );
  nodes = malloc (sizeof(DdNode*)*numBdds*1000);
  for(i = 0; i <numBdds; i++){
    nodes [i] = Cudd_Not(bdds[i]);
    //printf("Copy and negate %d: %p %p\n", i, bdds[i], nodes[i]);
  }

  do {
    dd->reordered = 0;
    res = cuddBddAndAllRecur(dd, 0, numBdds);
  } while (dd->reordered == 1);
  res = Cudd_NotCond(res,res != NULL);
  free(nodes);
  return(res);

} /* end of Cudd_bddOr */



DdNode *
Cudd_bddAndAllTimeout(DdManager * dd,
		      int numBdds, DdNode ** bdds,
		      long timeout){
  DdNode *res;

  int i;
  for(i = 0; i <numBdds; i++){
    nodes [i] = bdds[i];
  }

  do {
    dd->reordered = 0;
    res = cuddBddAndAllRecurTimeout(dd, 0, numBdds, util_cpu_time() + timeout);
  } while (dd->reordered == 1);
  return(res);
}



DdNode *
Cudd_bddAndAll(DdManager * dd,
	       int numBdds, DdNode ** bdds){
  DdNode *res;

  int i;
  for(i = 0; i <numBdds; i++){
    nodes [i] = bdds[i];
  }

  do {
    dd->reordered = 0;
    res = cuddBddAndAllRecur(dd, 0, numBdds);
  } while (dd->reordered == 1);
  return(res);
}
