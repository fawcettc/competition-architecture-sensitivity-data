#ifndef PLANNING_H
#define PLANNING_H

#include "util.h"
#include "cuddInt.h"
#include "cudd.h"


DdNode ** nodes;

DdNode *
cuddBddAndAllRecur(DdManager * manager,
		   int iniNode, int numNodes);

DdNode *
cuddBddAndAllRecurTimeout(DdManager * manager,
			  int iniNode, int numNodes, 
			  long timeout);


DdNode *
Cudd_bddOrAll(DdManager * dd,int numBdds,
	      DdNode ** bdds);
DdNode *
Cudd_bddOrAllTimeout(DdManager * dd,int numBdds,
		     DdNode ** bdds, long timeout);

DdNode *
Cudd_bddAndAll(DdManager * dd,int numBdds,
	      DdNode ** bdds);
DdNode *
Cudd_bddAndAllTimeout(DdManager * dd,int numBdds,
		     DdNode ** bdds, long timeout);




#endif
