/* 
 * Created on 7. April 2010, 15:24
 */
#include <cstdio>
#include <cmath>
#include <iostream>
#include <fstream>
#include <map>
#include <list>
#include <string>
#include <sstream>
#include <vector>

#include "cudd.h"
#include "util.h"
#include "dddmp.h"

#include "c_ranking.h"
#include "c_split.h"

using namespace std;

static DdManager *manager;
static tree_levels levs;             
static DdLocalCache *satCountCache;
static boolean * assignment;

void printConn4State(boolean * state){
  for (int i = 0; i < levs.num_variables; i++) {
    if (i%10==0) cout << "#";
    cout << (int)(state[i]);
  }
  cout << endl;
}
/*
void printStates(DdNode * bdd){
  int num_states = satCount(manager, &levs, satCountCache, bdd);
  for(int sol = 0; sol < num_states; sol++){
    unrank(manager, &levs, satCountCache, bdd, sol, assignment);	  
    cout << rank(manager, &levs, satCountCache, bdd, assignment) << ": ";
    printConn4State(assignment);
  }
}
*/
void printCall() {
    cerr << "call: cuddtest <gamename>" << endl;
    exit(1);
}

inline int log2_(int value) {
    return ceil(log2(value));
}

int readPartition(string gameName, list<list<string> >& partitions) {
    int numberOfVariables = 0;
    list<string> partitionInput;
    string line;
    string partitionFileName(gameName);
    partitionFileName += "Part.gdl";
    ifstream partitionFile(partitionFileName.c_str());
    while (!partitionFile.eof()) {
        getline(partitionFile, line);
        partitionInput.push_back(line);
    }
    list<string> stringList;
    partitions.push_back(stringList);
    while (partitionInput.begin() != partitionInput.end()) {
        line = partitionInput.front();
        partitionInput.pop_front();
        if (line == "") {
            if (partitions.back().size() > 0) {
                list<string> newStringList;
                partitions.push_back(newStringList);
            }
            continue;
        }
        int index;
        while ((index = line.find(" ")) > 0) {
            partitions.back().push_back(line.substr(0, index));
            line = line.substr(index + 1);
        }
        partitions.back().push_back(line);
    }
    if (partitions.back().size() == 0)
        partitions.pop_back();
    list<list<string> >::iterator partIt = partitions.begin();
    while (partIt != partitions.end()) {
        numberOfVariables += log2_((*partIt).size());
        partIt++;
    }
        numberOfVariables *= 2;
    return numberOfVariables;
}


/*
 * Reads BDD files, print number of states and nodes and plot them.
 */
int main(int argc, char** argv) {
    if (argc < 2) {
        cerr << "Error: too few arguments." << endl;
        printCall();
    }
    string gameName(argv[1]);
    list<list<string> > partitions;
    int numberOfVariables = readPartition(gameName, partitions);

    manager = Cudd_Init(numberOfVariables, 0, CUDD_UNIQUE_SLOTS, CUDD_CACHE_SLOTS, 0);
    vector <DdNode *> variables = vector <DdNode *>(0);
    for (int i = 0; i < numberOfVariables; i++) {
      variables.push_back(Cudd_bddIthVar(manager,i));
      Cudd_Ref(variables.back());
    }
    
    init_tree_levels_parity(manager, &levs, false);

    for(int i = 0; i < levs.num_variables; i++){
      printf("(%d %d)", levs.level_var[i], levs.var_at_level[i]);
    }
    assignment = (boolean *)malloc (levs.num_variables*sizeof(boolean));

    int counter = 0;
    FILE* filepointer;
    string frontFileName(argv[1]);
    frontFileName += "/layer";
    while (counter < 15) {
      satCountCache = cuddLocalCacheInit(manager, 1, 127, 0);

      stringstream numberStream;
        numberStream << counter++;
        string numberString;
        numberStream >> numberString;
        string bddFileName(frontFileName);
        bddFileName += numberString;
	bddFileName += "_reaches_50_50";
        if ((filepointer = fopen(bddFileName.c_str(), "r")) == NULL){
            break;
	}
	
        cout << bddFileName << ": " <<endl;

	// char* fileName = (char*) malloc(sizeof(char) * (bddFileName.size() + 1));
	//        sprintf(fileName, "%s\0", bddFileName.c_str());
	//Dddmp_cuddBddStore(manager, NULL, variables[0].getNode(), NULL, NULL, DDDMP_MODE_BINARY, DDDMP_VARDEFAULT, fileName, NULL);

	//read bdd file of layer i
	DdNode * bdd =  Dddmp_cuddBddLoad(manager, DDDMP_VAR_MATCHIDS, NULL, NULL, NULL, DDDMP_MODE_BINARY, NULL, filepointer);

        fclose(filepointer);

        cout << (long) Cudd_CountMinterm(manager,bdd, numberOfVariables / 2) <<
	  " states using " <<  Cudd_DagSize(bdd) - 1 << " nodes." << "My sat count: " <<
	  satCount(manager, &levs, satCountCache, bdd) << " states" << endl;

	//Cudd_CheckKeys(manager);
	Cudd_DebugCheck(manager);

	int numFolds = 8;
	
	DdNode ** sp = (DdNode **) malloc (numFolds*sizeof(DdNode*));

	//printStates(manager, &levs, satCountCache, bdd);
	numFolds = split_uniform(manager, &levs, satCountCache, bdd, numFolds, sp);       
	for(int i = 0; i < numFolds; i++){
	    Cudd_Ref(sp[i]);
	}
	//CuddHeapProfile(manager);
        Cudd_CheckKeys(manager);
	Cudd_DebugCheck(manager);

	cout << "Divided into " << numFolds << " parts" << endl;
	for(int i = 0; i < numFolds; i++){

	  cout << "Part " << i << " has " << satCount(manager, &levs, satCountCache, sp[i]) << " states and " <<  Cudd_DagSize(sp[i]) << " nodes" << endl;
	  
		  //printStates(manager, &levs, satCountCache, sp[i]);
	  Cudd_RecursiveDeref(manager, sp[i]); //Free memory of the bdd
	}		
	free(sp);

	//Print dot file
	/*	DdNode *Dds[] = {root.getNode()};
        char* dot_fileName = (char*) malloc(sizeof(char) * (15));
        sprintf(dot_fileName, "./dot/%d.dot\0", counter-1);
	FILE* fp = fopen(dot_fileName, "w");
	int result = Cudd_DumpDot(manager, 1, Dds, NULL, NULL, fp);
	fclose(fp);
	free (dot_fileName);*/
	cuddLocalCacheQuit(satCountCache);
	Cudd_RecursiveDeref(manager, bdd);
	Cudd_CheckZeroRef(manager);
    }

    /* FILE* fp;
    cout << endl;
    fp = fopen("info", "w");
    Cudd_PrintInfo(manager, fp);
    fclose(fp);
    ifstream partitionFile("info");
    while (!partitionFile.eof()) {
        string line;
        getline(partitionFile, line);
        cout << line << endl;
    }
    partitionFile.close();
    remove("info");*/
    
    free (assignment);

    printf("Check 0 ref \n");Cudd_CheckZeroRef(manager);
    Cudd_Quit(manager);
    return (EXIT_SUCCESS);
}

