#include "c_split.h"
#include "c_ranking.h"


int split_uniform (DdManager * manager, tree_levels *levs, DdLocalCache * satCountCache, DdNode * bdd, int numFolders, DdNode ** res){
  boolean * assignment = (boolean *) malloc (levs->num_variables*sizeof(boolean));
  DdNode * bigbdd = bdd;
  long sc = satCount(manager, levs, satCountCache, bdd);

  //  printf("Num folders %d, sc %ld\n", numFolders, sc);
  if(numFolders > sc){
    numFolders = sc;
  }

  int index = 0;
  pair_nodes aux;

  long numStatesPartition = sc/numFolders;

  int i;
  for(i = 0; i < numFolders -1; i++){
    
    unrank(manager, levs, satCountCache, bigbdd, numStatesPartition-1, assignment);
    //    printf("Dividing by: "); printState(levs->num_variables, assignment);
    split(manager, levs, satCountCache, bigbdd, assignment, &aux);
    Cudd_Ref(aux[0]); Cudd_Ref(aux[1]);
    //    printf("Result left: \n"); printStates(manager, levs, satCountCache, aux[0]);
    //    printf("Result right: \n"); printStates(manager, levs, satCountCache, aux[1]);
    res[index++] = aux[0];    //Get the left part to the result vector
    bigbdd = aux[1];          //The second part is the rest
  }
  free(assignment);
  res[index++] = bigbdd; //Put the remaining bdd in the result vector
  return index;
}

void split(DdManager * manager, tree_levels *levs, DdLocalCache * satCountCache, DdNode * bdd, boolean *partition_vector, pair_nodes * result){
  return splitRek(manager, levs, satCountCache, bdd, 0, partition_vector, result);
}

//Recursive step of the split into two procedure. 
//It is neccesary to use the current level in the tree, because we need to call this procedure for each level, and not just for each node in the path.
void splitRek(DdManager * manager, tree_levels *levs, DdLocalCache * satCountCache, DdNode * node, int level, boolean * assignment, pair_nodes *result){
  //Auxiliar variables to store the result
  DdNode * rLeft, * rRight;

  DdNode * N = Cudd_Regular(node);

  long l = get_level(levs, N);

  DdNode * zero = Cudd_ReadLogicZero(manager);

  Cudd_Ref(zero);
  if(level < l){ //If we are on a deleted node (both arcs were pointing to the same)
    Cudd_Ref(node);
    splitRek(manager, levs, satCountCache, node,level+1, assignment, result);
    if(assignment[l]){
      //Left = Node(node, split(node).first)
      rLeft = create_node(manager, levs->var_at_level[level], node, (*result)[0]);
      //Right = Node(zero, split(node).second)
      rRight = create_node(manager, levs->var_at_level[level], zero, (*result)[1]);     
    }else{
      //Left = Node(split(node).first, zero)
      rLeft = create_node(manager, levs->var_at_level[level], (*result)[0], zero);
      //Right = Node(split(node).second, node)
      rRight = create_node(manager, levs->var_at_level[level], (*result)[1], node);
    }    
  }else if(cuddIsConstant(N)) {
    assert (cuddV (N) == 1); //assignment must be valid
    Cudd_Ref(node);
    //The leaf node is always assigned to the left part
    rLeft = node;
    //The right part is always zero in the leaf
    rRight = zero; 
  }else{
    DdNode * nodeT, *nodeE, *Nt, *Ne;
    nodeT = cuddT(N);
    nodeE = cuddE(N);
    if (Cudd_IsComplement(node)) { //If the node was complemented by the ongoing arc
      nodeT = Cudd_Not(nodeT); nodeE = Cudd_Not(nodeE); //Apply a not operation over both sons
    }
    Nt = Cudd_Regular(nodeT);
    Ne = Cudd_Regular(nodeE);
    
    long j = get_level(levs, Ne);
    long k = get_level(levs, Nt);

    if(assignment[l]){
      //Recursion over then node to determine both then nodes
      //      Cudd_Ref(nodeT);
      splitRek(manager, levs, satCountCache, nodeT, level+1, assignment, result);

      //Assign else node to rLeft else
      rLeft = create_node(manager, N->index, (*result)[0], nodeE);
      //Assign constant 0 to Right else
      rRight = create_node(manager, N->index, (*result)[1], zero); 
    }else{
      //      cuddRef(nodeE);
      //Recursion over else node to determine both else nodes.
      splitRek(manager, levs, satCountCache, nodeE, level+1, assignment, result);

      //Assign then node to rRight then
      rRight = create_node(manager, N->index, nodeT, (*result)[1]); 
      //Assign constant 0 to rLeft then
      rLeft = create_node(manager, N->index, zero, (*result)[0]);
    }
  }

  (*result) [0] = rLeft; 
  (*result) [1] = rRight;
}


DdNode * create_node(DdManager * manager, int variable, DdNode * t, DdNode * e){
  if(t == e){ //If both arcs are the same
    return t; //just return one of them
  }else{
    //Making a new node
    DdNode * res;
    if(Cudd_IsComplement(t)){ //The then arc cannot be complemented
      //Generate tmp with negated arcs
      DdNode * tmp = cuddUniqueInter(manager, variable, Cudd_Not(t), Cudd_Not(e));
      Cudd_Ref(tmp);
      //The result is the negated tmp node
      res = Cudd_Not(tmp);
      //Cudd_Ref(res); //Reference the node
      //Cudd_Deref(tmp);
    }else{
      res = cuddUniqueInter(manager, variable, t, e);
      Cudd_Ref(res); //Reference the node
    }

    return res;
  }
}



void split_top_variable(DdManager * manager, DdNode * bdd, DdNode ** res){
  split_variable(manager, bdd, Cudd_Regular(bdd)->index, res);
}

void split_variable(DdManager * manager, DdNode * bdd, int variable, DdNode ** res){
  DdNode * select_variables = Cudd_bddIthVar(manager, variable);
  Cudd_Ref(select_variables);
  res [0] = Cudd_bddAnd(manager, bdd, select_variables);
  Cudd_Ref(res[0]);

  res [1] = Cudd_bddAnd(manager, bdd, Cudd_Not(select_variables));
  Cudd_Ref(res[1]);
  Cudd_RecursiveDeref(manager, select_variables);
}




/*
 * DdNode ** bdd is a pointer to a BDD array of pointers
 * Returns the number of splitted BDDs
 */
int split_layer(DdManager * manager, DdNode ** bdd, int layer, DdNode *** res){
  
}



/*
int split_top_variables (DdManager * manager, tree_levels *levs, DdLocalCache * satCountCache, DdNode * bdd, int numFolders, DdNode ** res){
  boolean * assignment = (boolean *) malloc (levs->num_variables*sizeof(boolean));
  DdNode * bigbdd = bdd;
  long sc = satCount(manager, levs, satCountCache, bdd);

  printf("Num folders %d, sc %ld\n", numFolders, sc);
  if(numFolders > sc){
    numFolders = sc;
  }

  int index = 0;
  pair_nodes aux;

  int numStatesPartition = sc/numFolders;

  int i;
  for(i = 0; i < numFolders -1; i++){
    
    unrank(manager, levs, satCountCache, bigbdd, numStatesPartition-1, assignment);
    //    printf("Dividing by: "); printState(levs->num_variables, assignment);
    split(manager, levs, satCountCache, bigbdd, assignment, &aux);
    Cudd_Ref(aux[0]); Cudd_Ref(aux[1]);
    //    printf("Result left: \n"); printStates(manager, levs, satCountCache, aux[0]);
    //    printf("Result right: \n"); printStates(manager, levs, satCountCache, aux[1]);
    res[index++] = aux[0];    //Get the left part to the result vector
    bigbdd = aux[1];          //The second part is the rest
  }
  free(assignment);
  res[index++] = bigbdd; //Put the remaining bdd in the result vector
  return index;
}
*/
