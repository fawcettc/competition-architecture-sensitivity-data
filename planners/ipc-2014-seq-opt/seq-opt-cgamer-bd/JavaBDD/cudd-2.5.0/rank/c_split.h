#ifndef SPLIT_H
#define SPLIT_H

#include "c_ranking.h"
#include "util.h"
#include "cuddInt.h"
#include "cudd.h"


typedef DdNode * pair_nodes [2];
/*
 * The split class offers functionality to split a BDD structure into several,
 */
  
//Returns an array of nodes with numFolders elements.
int split_uniform (DdManager * manager, tree_levels *levs, DdLocalCache * satCountCache, DdNode * bdd, int numFolders, DdNode ** res);

//Returns a pair with the two roots.
void split (DdManager * manager, tree_levels *levs, DdLocalCache * satCountCache, DdNode * bdd, boolean * partition_vector, pair_nodes *result);

//Returns a pair of DdNodes
void splitRek(DdManager * manager, tree_levels *levs, DdLocalCache * satCountCache, DdNode * node, int level, boolean * assignment, pair_nodes *result);

DdNode * create_node(DdManager * manager, int variable, DdNode * t, DdNode * e);

void split_variable(DdManager * manager, DdNode * bdd, int variable, DdNode ** res);


void split_top_variable(DdManager * manaher, DdNode * bdd, DdNode ** res);

#endif
