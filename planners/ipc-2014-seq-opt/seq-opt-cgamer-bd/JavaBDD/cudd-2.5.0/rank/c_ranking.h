#ifndef RANKING_H
#define RANKING_H

#include "util.h"
#include "cuddInt.h"
#include "cudd.h"

/*
 * Ranking functions for the BDD structure
 */
static int satCountRecovered = 0;
static int satCountInserted = 0;

typedef struct {
  int * level_var;        //Level of the tree for each variable index
  int * var_at_level;     //Variable of the problem at the level i
  int level_const;        //Level of the tree in which are the constants
  int num_variables;      //Number of used variables
} tree_levels;

typedef unsigned char boolean;

void printState(int len, boolean * state);

void printStates(DdManager * manager, tree_levels *levs, DdLocalCache * satCountCache, 
		 DdNode * bdd);


//Initialize tree levels to the default value: all the variables are relevant
void init_tree_levels_all (DdManager * manager, tree_levels * lev);
//Initialize tree levels to the parity condition: odd or even
void init_tree_levels_parity (DdManager * manager, tree_levels * lev, boolean odd);
//Initialize tree levels with a particular set of relevant variables
void init_tree_levels_variables (DdManager * manager, tree_levels * lev,
		       int num_rel_variables, int * relevant_variables);

int get_level(tree_levels * lev, DdNode * node);

//Should receive a non-regular node (needs to know if it is complemented for the constant case)
long satCount(DdManager * manager, tree_levels *levs, DdLocalCache * satCountCache, DdNode * node);

 //Returns the ranking of the permutation.
long rank(DdManager * manager, tree_levels *levs, DdLocalCache * satCountCache, DdNode * root_node, boolean * assignment);
void unrank(DdManager * manager, tree_levels *levs, DdLocalCache * satCountCache, DdNode * root_node, long ranking, boolean * assignment);

//Auxiliar functions for satcount, rank and unrank
long satCountAux(DdManager * manager, tree_levels *levs, DdLocalCache * satCountCache, DdNode * node);
long rankAux(DdManager * manager, tree_levels *levs, DdLocalCache * satCountCache, DdNode * node, boolean * assignment);

#endif
