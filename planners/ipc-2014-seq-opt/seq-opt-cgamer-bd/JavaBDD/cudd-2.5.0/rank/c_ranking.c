#include "c_ranking.h" 

void printState(int len, boolean * state){
  int i;
  for (i = 0; i < len; i++) {
    if (i%10==0) printf("#");
    printf("%d", (int)(state[i]));
  }
  printf("\n");
}

void printStates(DdManager * manager, tree_levels *levs, DdLocalCache * satCountCache,
		 DdNode * bdd){
  boolean * assignment = (boolean *)malloc (levs->num_variables*sizeof(boolean));
  long num_states = satCount(manager, levs, satCountCache, bdd);
  int sol;
  for(sol = 0; sol < num_states; sol++){
    unrank(manager, levs, satCountCache, bdd, sol, assignment);	  
    printf("Rank %ld: ", rank(manager, levs, satCountCache, bdd, assignment));
    printState(levs->num_variables, assignment);
  }
  free(assignment);
}

void unrank(DdManager * manager, tree_levels *levs, DdLocalCache * satCountCache,  DdNode * root_node, long ranking, boolean * assignment){
  int i,j,k, l;
  long d;
  DdNode *node, *nodeT, *nodeE, *N, *Nt, *Ne;

  i = get_level(levs, root_node);

  d = ranking / satCount(manager, levs, satCountCache, root_node);

  //Assign variables up to the root with the binary value of the ranking
  for (j=0;j<i;j++) {
    assignment[j] = (d / (1L << (i-j-1))) % 2;
  }
  node = root_node;
  
  while (!cuddIsConstant((N = Cudd_Regular(node)))) {
    ranking = ranking % satCount(manager, levs, satCountCache,node);
    //    cout << "Ranking: " << ranking;
    //Get the Then and Else sons
    //    N = Cudd_Regular(node);
    nodeT = cuddT(N);
    nodeE = cuddE(N);
    if (Cudd_IsComplement(node)) { //If the node was complemented by the ongoing arc
      //if(!cuddIsConstant(node)) cout << "complemented" << endl;
      nodeT = Cudd_Not(nodeT); nodeE = Cudd_Not(nodeE); //Apply a not operation over both sons
    }
    Nt = Cudd_Regular(nodeT);
    Ne = Cudd_Regular(nodeE);
        
    j = get_level(levs, Ne);
    k = get_level(levs, Nt);
    //    cout << "  Node level: " << i << " Else level " << j << " Then level " << k << endl;
    //    cout << "Sat count else: " << satCount(manager, level, satCountCache,nodeE) << " then " << satCount(manager, level, satCountCache,nodeT) << endl;
    //If the ranking value is on the half corresponding to the else node
    if (ranking < ((1L << (j-i-1)) * satCount(manager, levs, satCountCache,nodeE))) {
      //      cout << "   Else at level " << j << endl;
      assignment[i] = 0; //Put a zero
      d = ranking / satCount(manager, levs, satCountCache,nodeE);
      //If there is a level gap, put the binary value of the ranking
      for (l=i+1;l<j;l++) {
        assignment[l] = (d / (1L << (j-l-1))) % 2;
      }
      node = nodeE;
      i = j;
    }else {
      //The ranking value is on the half corresponding the then value
      assignment[i] = 1;
      //      cout << "   Then at level " << k << endl;
      ranking = ranking - ((1L << (j-i-1)) * satCount(manager, levs, satCountCache,nodeE));
      d = ranking / satCount(manager, levs, satCountCache,nodeT);
      for (l=i+1;l<k;l++) {
        assignment[l] = (d / (1L << (k-l-1))) % 2;
      }
      node = nodeT;
      i = k;
    }
  }
  //    cout << " cuddV(N): " << cuddV(N) << endl;
  //    assert(cuddV(N) == 1);
}

//Precomputes the sat count for each node, storing it in the table. Called from constructor.
/*void Ranking::precomputeSatCount (){ //long bdd::markSats(long n) {
  long csr = satCountAux(root_node);
  long i = root_node->index;
  long satcount_tree = (1L << i) * csr;
  }*/

//Should receive a non-regular node (needs to know if it is complemented for the constant case)
long satCount(DdManager * manager, tree_levels *levs, DdLocalCache * satCountCache, DdNode * node){
  DdNode * res, *Nt, *Ne, *N;
  long i, j, k, cst, cse, satcount;
  
  N = Cudd_Regular(node); // Obtain the real reference to the node
  if (cuddIsConstant(N)) { //Base case, return 0 or 1
    if (node == manager->background ||
	node == Cudd_Not(manager->one)) {
      //printf("Base case: returned 0\n");
      return 0L; 
    } else {
      //printf("Base case: returned 1\n");
      return 1L;
    }
  }

  //Try to recover the value
  if ((res = cuddLocalCacheLookup(satCountCache, &node)) != NULL) {
    satcount = cuddV(res);
    //printf("reco %d %p\n", satCountRecovered, node);
    //    satCountRecovered++; printf("reco %d %p\n", satCountRecovered, node);
    return(satcount);
  }

  //printf("intermediate node\n");
  //Get the Then and Else sons
  Nt = cuddT(N);
  Ne = cuddE(N); 
  if (Cudd_IsComplement(node)) { //If the node was complemented by the ongoing arc
    Nt = Cudd_Not(Nt); Ne = Cudd_Not(Ne); //Apply a not operation over both sons
  }

  i = get_level(levs, N);
  j = get_level(levs, Nt);
  k = get_level(levs, Ne);
  cst = satCount(manager, levs, satCountCache, Nt);  /* recursion then-successor */
  cse = satCount(manager, levs, satCountCache, Ne);  /* recursion else-successor */
  satcount = (1L << (j-i-1)) * cst + (1L << (k-i-1)) * cse;

  /*printf("cst %d cse %d index %d level: %d Tindex %d Tlevel: %d  Eindex %d index Elevel: %d node %p is %d\n",
	 cst, cse, N->index, i, (cuddIsConstant(Nt) ? -25 : Cudd_Regular(Nt)->index),
	 j, (cuddIsConstant(Ne) ? -25 : Cudd_Regular(Ne)->index), k, node, satcount);*/
  res = cuddUniqueConst(satCountCache->manager,satcount);
  /*Cudd_Ref(node);*/ Cudd_Ref(res);

  //printf("ins %d %p\n", satCountInserted, node);
  cuddLocalCacheInsert(satCountCache,&node,res);
  //  cuddLocalCacheProfile(satCountCache);
  //  satCountInserted++; 


  return satcount;
}


 // Performs a ranking rec step of an assignment in the bdd.
long rank(DdManager * manager, tree_levels *levs, DdLocalCache * satCountCache, DdNode * root_node, boolean * assignment){
  long d, i;
  int j;
  i = levs->level_var[Cudd_Regular(root_node)->index];
  d = 0;
  for (j=0;j<i;j++) {
    d += assignment[j] ? (1L << j) : 0;
  }
  return d*satCount(manager, levs, satCountCache, root_node) +
    rankAux(manager, levs, satCountCache, root_node, assignment) - 1;  
}


long rankAux(DdManager * manager, tree_levels *levs, DdLocalCache * satCountCache, DdNode * node, boolean * assignment){
  DdNode *N, *Ne, *Nt;
  boolean value;
  long i, j, k, r0, r1, d0, d1;
  int l;
  N = Cudd_Regular(node); // Obtain the real reference to the node
  if(Cudd_IsConstant(node)){
    value = cuddV(Cudd_Regular(N));
    if(Cudd_IsComplement(node)){
      value = !value;
    }    
    assert(value);
    return 1;
  }

  //Get the Then and Else sons
  Nt = cuddT(N);
  Ne = cuddE(N); 
  if (Cudd_IsComplement(node)) { //If the node was complemented by the ongoing arc
    Nt = Cudd_Not(Nt); Ne = Cudd_Not(Ne); //Apply a not operation over both sons
  }

  i = get_level(levs, N);
  j = get_level(levs, Ne);
  k = get_level(levs, Nt);

  if (assignment[i] == 0) {
    r0 = rankAux(manager, levs, satCountCache, Ne,assignment);
    d0 = 0;
    for (l=i+1;l<j;l++) {
      d0 += assignment[l] ? (1L << l-i-1) : 0;
    }
    return d0 * satCount(manager, levs, satCountCache,Ne) + r0;
  }
  else {
    r1 = rankAux(manager, levs, satCountCache, Nt,assignment);
    d1 = 0;
    for (l=i+1;l<k;l++) {
      d1 += assignment[l] ? (1L << l-i-1) : 0;
    }
    return
      (1L << (j-i-1)) * satCount(manager, levs, satCountCache,Ne) +
      d1 * satCount(manager, levs, satCountCache,Nt) + r1;
  }
}


void init_tree_levels (DdManager * manager, tree_levels * lev){   
  int i;
  int num_variables = Cudd_ReadSize(manager);
  //printf("init tree levels: %d\n", num_variables);
  lev->num_variables = 0;
  lev->level_var = (int *)malloc (num_variables*sizeof(int));
  lev->var_at_level =(int *)malloc (num_variables*sizeof(int));
    
  //Initialize level of variables and constants
  lev->level_const = 0; //level_constant is just one more than the last variable levelasd
  for(i = 0; i < num_variables; i++){
      lev->var_at_level[lev->level_const] = i;
      lev->level_var[i] = lev->level_const++;
      lev->num_variables ++;
  }
}


void init_tree_levels_parity (DdManager * manager, tree_levels * lev, boolean odd){   
  int i;
  int num_variables = Cudd_ReadSize(manager);
  //printf("init tree levels: %d\n", num_variables);
  lev->num_variables = 0;
  lev->level_var = (int *)malloc (num_variables*sizeof(int));
  lev->var_at_level =(int *)malloc (num_variables*sizeof(int));
    
  //Initialize level of variables and constants
  lev->level_const = 0; //level_constant is just one more than the last variable levelasd
  for(i = 0; i < num_variables; i++){
    if(i%2 == odd){ //Only count odd variables (even variables are for the image computation)
      lev->var_at_level[lev->level_const] = i;
      lev->level_var[i] = lev->level_const++;
      lev->num_variables ++;
    }else{
      lev->level_var[i] = -1;
    }
  }
}


void init_tree_levels_variables (DdManager * manager, tree_levels * lev,
		       int num_rel_variables, int * relevant_variables){   
  int i;
  int num_variables = Cudd_ReadSize(manager);
  //printf("init tree levels: %d\n", num_variables);
  lev->num_variables = num_rel_variables;
  lev->level_var = (int *)malloc (num_variables*sizeof(int));
  lev->var_at_level =(int *)malloc (num_variables*sizeof(int));
    
  //Initialize level of variables and constants
  lev->level_const = 0; //level_constant is just one more than the last variable levelasd
  for(i = 0; i < num_variables; i++){
    lev->level_var[i] = -1;
    lev->var_at_level[i] = -1;
  }
  for(i = 0; i < num_rel_variables; i++){
      lev->var_at_level[lev->level_const] = relevant_variables[i];
      lev->level_var[relevant_variables[i]] = lev->level_const++;
  }
}


int get_level(tree_levels * lev, DdNode * node){
  // printf("Variable: %d\n", Cudd_Regular(node)->index);
  //  printf("Is constant: %d\n", cuddIsConstant(Cudd_Regular(node)));
  return cuddIsConstant(Cudd_Regular(node)) ? lev->level_const :
    lev->level_var[Cudd_Regular(node)->index];
}

