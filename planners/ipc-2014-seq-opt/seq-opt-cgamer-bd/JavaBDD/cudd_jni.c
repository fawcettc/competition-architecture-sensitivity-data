#include <jni.h>
#include <stdlib.h>
#include <stdint.h>

#include "util.h"
#include "cudd.h"
#include "cuddInt.h"
#include "cudd_jni.h"
#include "dddmpInt.h"


#include "c_split.h" 
#include "c_ranking.h" 
#include "planning.h"
/*
** When casting from `int' to a pointer type, you should
** first cast to `intptr_cast_type'.  This is a type
** that is (a) the same size as a pointer, on most platforms,
** to avoid compiler warnings about casts from pointer to int of
** different size; and (b) guaranteed to be at least as big as
** `int'.
*/
#ifndef __STDC_VERSION__
#define __STDC_VERSION__ 199901
#endif
#if __STDC_VERSION__ >= 199901
#include <inttypes.h>
#if INTPTR_MAX >= INT_MAX
typedef intptr_t intptr_cast_type;
#else /* no intptr_t, or intptr_t smaller than `int' */
typedef intmax_t intptr_cast_type;
#endif
#else
#include <stddef.h>
#include <limits.h>
#if PTRDIFF_MAX >= INT_MAX
typedef ptrdiff_t intptr_cast_type;
#else
typedef int intptr_cast_type;
#endif
#endif

static DdManager *manager;
static tree_levels levs;              // For the ranking and splitting procedures
static DdLocalCache *satCountCache;      // For the ranking and splitting procedures
static jlong bdd_one, bdd_zero;

#define INVALID_BDD 0L


static DdNode **  readNodeArray(JNIEnv * env, jlongArray bdds_array, int num_bdds){
  DdNode ** bdds = malloc(sizeof(DdNode *)*num_bdds);
  intptr_cast_type * bdd_pts = (intptr_cast_type*)((*env)->GetPrimitiveArrayCritical(env, bdds_array, NULL));
  int i;
  for( i =0; i < num_bdds; i++){
    
    bdds[i] = (DdNode *)(bdd_pts[i]);
    //      printf("BDD %d %p %lld\n", i, bdds[i], bdd_pts[i]);
  }

  (*env)->ReleasePrimitiveArrayCritical(env, bdds_array, bdd_pts, JNI_ABORT);
  return bdds;
}

static void die(JNIEnv *env, char* msg) {
  jclass cls;
  cls = (*env)->FindClass(env, "java/lang/InternalError");
  if (cls != NULL) {
    (*env)->ThrowNew(env, cls, msg);
  }
  (*env)->DeleteLocalRef(env, cls);
}

/**** START OF NATIVE METHOD IMPLEMENTATIONS ****/

/*
 * Class:     net_sf_javabdd_CUDDFactory
 * Method:    registerNatives
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_net_sf_javabdd_CUDDFactory_registerNatives
(JNIEnv *env, jclass cl) {
}

typedef struct CuddPairing {
  DdNode** table;
  struct CuddPairing *next;
} CuddPairing;

static CuddPairing *pair_list;

/*
 * Class:     net_sf_javabdd_CUDDFactory
 * Method:    initialize0
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_net_sf_javabdd_CUDDFactory_initialize0
(JNIEnv *env, jclass cl, jint numSlots, jint cacheSize, jint numvars) {
  jfieldID one_fid;
  jfieldID zero_fid;
    
  if (manager != NULL) {
    die(env, "init called twice!");
    return;
  }
    
  //manager = Cudd_Init(nodenum, 0, numSlots, cachesize, 0);
  manager = Cudd_Init(numvars, 0, numSlots, cacheSize, 0);
  if (manager == NULL) {
    die(env, "unable to initialize CUDD");
    return;
  }
    
  // we cannot use ReadZero because it returns the arithmetic zero,
  // which is different than logical zero.
  bdd_one  = (jlong) (intptr_cast_type) Cudd_ReadOne(manager);
  bdd_zero = (jlong) (intptr_cast_type) Cudd_Not(Cudd_ReadOne(manager));
    
  Cudd_Ref((DdNode *)(intptr_cast_type) bdd_one);
  Cudd_Ref((DdNode *)(intptr_cast_type) bdd_zero);
    
  pair_list = NULL;
    
  one_fid = (*env)->GetStaticFieldID(env, cl, "one", "J");
  zero_fid = (*env)->GetStaticFieldID(env, cl, "zero", "J");
    
  if (!one_fid || !zero_fid) {
    die(env, "cannot find members: version mismatch?");
    return;
  }
  (*env)->SetStaticLongField(env, cl, one_fid, bdd_one);
  (*env)->SetStaticLongField(env, cl, zero_fid, bdd_zero);

  //    printf("Initialize structures for ranking and splitting\n");
  //Initialize structures for the ranking and splitting.
  satCountCache = cuddLocalCacheInit(manager, 1, 127, 0);
}

/*
 * Class:     net_sf_javabdd_CUDDFactory
 * Method:    isInitialized0
 * Signature: ()Z
 */
JNIEXPORT jboolean JNICALL Java_net_sf_javabdd_CUDDFactory_isInitialized0
(JNIEnv *env, jclass cl) {
  return manager != NULL;
}

/*
 * Class:     net_sf_javabdd_CUDDFactory
 * Method:    done0
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_net_sf_javabdd_CUDDFactory_done0
(JNIEnv *env, jclass cl) {
  int bdds;
  DdManager* m;
  int varnum = Cudd_ReadSize(manager);
    
  while (pair_list) {
    CuddPairing *p;
    int n;
    for (n=0 ; n<varnum ; n++) {
      Cudd_RecursiveDeref(manager, pair_list->table[n]);
    }
    free(pair_list->table);
    p = pair_list->next;
    free(pair_list);
    pair_list = p;
  }
    
  Cudd_Deref((DdNode *)(intptr_cast_type) bdd_one);
  Cudd_Deref((DdNode *)(intptr_cast_type) bdd_zero);
    
  fprintf(stdout, "Garbage collections: %d  Time spent: %ldms\n",
	  Cudd_ReadGarbageCollections(manager), Cudd_ReadGarbageCollectionTime(manager));
    
  bdds = Cudd_CheckZeroRef(manager);
  if (bdds > 0) fprintf(stdout, "Note: %d BDDs still in memory when terminating\n", bdds);
  m = manager;
  manager = NULL; // race condition with delRef
  cuddLocalCacheQuit(satCountCache);
  Cudd_Quit(m);
}

/*
 * Class:     net_sf_javabdd_CUDDFactory
 * Method:    varNum0
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_net_sf_javabdd_CUDDFactory_varNum0
(JNIEnv *env, jclass cl) {
  return Cudd_ReadSize(manager);
}

/*
 * Class:     net_sf_javabdd_CUDDFactory
 * Method:    setVarNum0
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_net_sf_javabdd_CUDDFactory_setVarNum0
(JNIEnv *env, jclass cl, jint x) {
  jint old = Cudd_ReadSize(manager);
  CuddPairing *p;
  if (x < 1 || x < old || x > CUDD_MAXINDEX) {
    jclass cls = (*env)->FindClass(env, "java/lang/IllegalArgumentException");
    (*env)->ThrowNew(env, cls, "invalid number of variables");
    (*env)->DeleteLocalRef(env, cls);
    return 0;
  }
  p = pair_list;
  while (p) {
    int n;
    DdNode** t = (DdNode**) malloc(sizeof(DdNode*)*x);
    if (t == NULL) return 0;
    memcpy(t, p->table, sizeof(DdNode*)*old);
    for (n=old ; n<x ; n++) {
      int var = n;
      //int var = Cudd_ReadInvPerm(manager, n); // level2var
      t[n] = Cudd_bddIthVar(manager, var);
      Cudd_Ref(t[n]);
    }
    free(p->table);
    p->table = t;
    p = p->next;
  }
  while (Cudd_ReadSize(manager) < x) {
    Cudd_bddNewVar(manager);
  }
  return old;
}

/*
 * Class:     net_sf_javabdd_CUDDFactory
 * Method:    ithVar0
 * Signature: (I)J
 */
JNIEXPORT jlong JNICALL Java_net_sf_javabdd_CUDDFactory_ithVar0
(JNIEnv *env, jclass cl, jint i) {
  DdNode* d;
  jlong result;
  if (i >= CUDD_MAXINDEX - 1) return INVALID_BDD;
  d = Cudd_bddIthVar(manager, i);
  result = (jlong) (intptr_cast_type) d;
  return result;
}

/*
 * Class:     net_sf_javabdd_CUDDFactory
 * Method:    load0
 * Signature: (Ljava/lang/String;)J
 */
JNIEXPORT jlong JNICALL Java_net_sf_javabdd_CUDDFactory_load0
(JNIEnv *env, jclass cl, jstring fname) {
  char* str;
  str = (char*) (*env)->GetStringUTFChars(env, fname, NULL);
  if (str == NULL) return -1;
  DdNode* rootp = Dddmp_cuddBddLoad(manager, DDDMP_VAR_MATCHIDS, NULL, NULL, NULL, DDDMP_MODE_BINARY, str, NULL);
  (*env)->ReleaseStringUTFChars(env, fname, str);
  return (jlong) rootp;
}

/*
 * Class:     net_sf_javabdd_CUDDFactory
 * Method:    save0
 * Signature: (Ljava/lang/String;J)V
 */
JNIEXPORT void JNICALL Java_net_sf_javabdd_CUDDFactory_save0
(JNIEnv *env, jclass cl, jstring fname, jlong root) {
  DdNode* rootp = (DdNode*) (intptr_cast_type) root;
  char* str;
  str = (char*) (*env)->GetStringUTFChars(env, fname, NULL);
  if (str == NULL) return;
  Dddmp_cuddBddStore(manager, NULL, rootp, NULL, NULL, DDDMP_MODE_BINARY, DDDMP_VARDEFAULT, str, NULL);
  (*env)->ReleaseStringUTFChars(env, fname, str);
}

/*
 * Class:     net_sf_javabdd_CUDDFactory
 * Method:    level2Var0
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_net_sf_javabdd_CUDDFactory_level2Var0
(JNIEnv *env, jclass cl, jint level) {
  //return manager->invperm[level];
  return (jint) Cudd_ReadInvPerm(manager, level);
}

/*
 * Class:     net_sf_javabdd_CUDDFactory
 * Method:    var2Level0
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_net_sf_javabdd_CUDDFactory_var2Level0
(JNIEnv *env, jclass cl, jint v) {
  //return (jint) cuddI(manager, v);
  return (jint) Cudd_ReadPerm(manager, v);
}

/*
 * Class:     net_sf_javabdd_CUDDFactory
 * Method:    setVarOrder0
 * Signature: ([I)V
 */
JNIEXPORT void JNICALL Java_net_sf_javabdd_CUDDFactory_setVarOrder0
(JNIEnv *env, jclass cl, jintArray arr) {
  int *a;
  int varnum = Cudd_ReadSize(manager);
  jint size = (*env)->GetArrayLength(env, arr);
  if (size != varnum) {
    jclass cls = (*env)->FindClass(env, "java/lang/IllegalArgumentException");
    (*env)->ThrowNew(env, cls, "array size != number of vars");
    (*env)->DeleteLocalRef(env, cls);
    return;
  }
  a = (int*) (*env)->GetPrimitiveArrayCritical(env, arr, NULL);
  if (a == NULL) return;
  Cudd_ShuffleHeap(manager, a);
  (*env)->ReleasePrimitiveArrayCritical(env, arr, a, JNI_ABORT);
}

/*
 * Class:     net_sf_javabdd_CUDDFactory
 * Method:    getAllocNum0
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_net_sf_javabdd_CUDDFactory_getAllocNum0
(JNIEnv *env, jclass cl) {
  return Cudd_ReadPeakNodeCount(manager);
}

/*
 * Class:     net_sf_javabdd_CUDDFactory
 * Method:    getNodeNum0
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_net_sf_javabdd_CUDDFactory_getNodeNum0
(JNIEnv *env, jclass cl) {
  return Cudd_ReadNodeCount(manager);
}

/*
 * Class:     net_sf_javabdd_CUDDFactory
 * Method:    getCacheSize0
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_net_sf_javabdd_CUDDFactory_getCacheSize0
(JNIEnv *env, jclass cl) {
  return Cudd_ReadCacheSlots(manager);
}

/* class net_sf_javabdd_CUDDFactory_CUDDBDD */

/*
 * Class:     net_sf_javabdd_CUDDFactory_CUDDBDD
 * Method:    var0
 * Signature: (J)I
 */
JNIEXPORT jint JNICALL Java_net_sf_javabdd_CUDDFactory_00024CUDDBDD_var0
(JNIEnv *env, jclass cl, jlong b) {
  DdNode* d;
  d = (DdNode*) (intptr_cast_type) b;
  return Cudd_Regular(d)->index;
}

/*
 * Class:     net_sf_javabdd_CUDDFactory_CUDDBDD
 * Method:    high0
 * Signature: (J)J
 */
JNIEXPORT jlong JNICALL Java_net_sf_javabdd_CUDDFactory_00024CUDDBDD_high0
(JNIEnv *env, jclass cl, jlong b) {
  DdNode* d;
  DdNode* res;
  jlong result;
  d = (DdNode*) (intptr_cast_type) b;
    
  // TODO: check if d is a constant.
  res = Cudd_T(d);
  res = Cudd_NotCond(res, Cudd_IsComplement(d));
    
  result = (jlong) (intptr_cast_type) res;
  return result;
}

/*
 * Class:     net_sf_javabdd_CUDDFactory_CUDDBDD
 * Method:    low0
 * Signature: (J)J
 */
JNIEXPORT jlong JNICALL Java_net_sf_javabdd_CUDDFactory_00024CUDDBDD_low0
(JNIEnv *env, jclass cl, jlong b) {
  DdNode* d;
  DdNode* res;
  jlong result;
  d = (DdNode*) (intptr_cast_type) b;
    
  // TODO: check if d is a constant.
  res = Cudd_E(d);
  res = Cudd_NotCond(res, Cudd_IsComplement(d));
    
  result = (jlong) (intptr_cast_type) res;
  return result;
}

/*
 * Class:     net_sf_javabdd_CUDDFactory_CUDDBDD
 * Method:    not0
 * Signature: (J)J
 */
JNIEXPORT jlong JNICALL Java_net_sf_javabdd_CUDDFactory_00024CUDDBDD_not0
(JNIEnv *env, jclass cl, jlong b) {
  DdNode* d;
  jlong result;
  d = (DdNode*) (intptr_cast_type) b;
  d = Cudd_Not(d);
  result = (jlong) (intptr_cast_type) d;
  return result;
}

/*
 * Class:     net_sf_javabdd_CUDDFactory_CUDDBDD
 * Method:    ite0
 * Signature: (JJJ)J
 */
JNIEXPORT jlong JNICALL Java_net_sf_javabdd_CUDDFactory_00024CUDDBDD_ite0
(JNIEnv *env, jclass cl, jlong a, jlong b, jlong c) {
  DdNode* d;
  DdNode* e;
  DdNode* f;
  DdNode* g;
  jlong result;
  d = (DdNode*) (intptr_cast_type) a;
  e = (DdNode*) (intptr_cast_type) b;
  f = (DdNode*) (intptr_cast_type) c;
  g = Cudd_bddIte(manager, d, e, f);
  result = (jlong) (intptr_cast_type) g;
  return result;
}

/*
 * Class:     net_sf_javabdd_CUDDFactory_CUDDBDD
 * Method:    relprod0
 * Signature: (JJJ)J
 */
JNIEXPORT jlong JNICALL Java_net_sf_javabdd_CUDDFactory_00024CUDDBDD_relprod0
(JNIEnv *env, jclass cl, jlong a, jlong b, jlong c) {
  DdNode* d;
  DdNode* e;
  DdNode* f;
  DdNode* g;
  jlong result;
  d = (DdNode*) (intptr_cast_type) a;
  e = (DdNode*) (intptr_cast_type) b;
  f = (DdNode*) (intptr_cast_type) c;
  g = Cudd_bddAndAbstract(manager, d, e, f);
  result = (jlong) (intptr_cast_type) g;
  return result;
}

/*
 * Class:     net_sf_javabdd_CUDDFactory_CUDDBDD
 * Method:    relprod_timeout0
 * Signature: (JJJJ)J
 */
JNIEXPORT jlong JNICALL Java_net_sf_javabdd_CUDDFactory_00024CUDDBDD_relprod_1timeout0
(JNIEnv *env, jclass cl, jlong a, jlong b, jlong c, jlong to, jlong maxN) {
  DdNode* d;
  DdNode* e;
  DdNode* f;
  DdNode* g;
  long timeout = (long)to;
  long maxNodes = (long)maxN;
  jlong result;
  d = (DdNode*) (intptr_cast_type) a;
  e = (DdNode*) (intptr_cast_type) b;
  f = (DdNode*) (intptr_cast_type) c;

  if (timeout > 0){
    Cudd_SetTimeLimit(manager, timeout);
    Cudd_ResetStartTime(manager);
  }

  //    g = Cudd_bddAndAbstractTimeout(manager, d, e, f, timeout);
  if (maxNodes > 0){
    g = Cudd_bddAndAbstractLimit(manager, d, e, f, maxNodes);
  }else{
    g = Cudd_bddAndAbstract(manager, d, e, f);
  }

  if(timeout > 0){
    Cudd_UnsetTimeLimit(manager);
  }

  if(g == NULL){
    return INVALID_BDD;
  }
  result = (jlong) (intptr_cast_type) g;

    
  return result;
}


/*
 * Class:     net_sf_javabdd_CUDDFactory_CUDDBDD
 * Method:    compose0
 * Signature: (JJI)J
 */
JNIEXPORT jlong JNICALL Java_net_sf_javabdd_CUDDFactory_00024CUDDBDD_compose0
(JNIEnv *env, jclass cl, jlong a, jlong b, jint i) {
  DdNode* d;
  DdNode* e;
  DdNode* f;
  jlong result;
  d = (DdNode*) (intptr_cast_type) a;
  e = (DdNode*) (intptr_cast_type) b;
  f = Cudd_bddCompose(manager, d, e, i);
  result = (jlong) (intptr_cast_type) f;
  return result;
}

/*
 * Class:     net_sf_javabdd_CUDDFactory_CUDDBDD
 * Method:    exist0
 * Signature: (JJ)J
 */
JNIEXPORT jlong JNICALL Java_net_sf_javabdd_CUDDFactory_00024CUDDBDD_exist0
(JNIEnv *env, jclass cl, jlong a, jlong b) {
  DdNode* d;
  DdNode* e;
  DdNode* f;
  jlong result;
  d = (DdNode*) (intptr_cast_type) a;
  e = (DdNode*) (intptr_cast_type) b;
  f = Cudd_bddExistAbstract(manager, d, e);
  result = (jlong) (intptr_cast_type) f;
  return result;
}

/*
 * Class:     net_sf_javabdd_CUDDFactory_CUDDBDD
 * Method:    forAll0
 * Signature: (JJ)J
 */
JNIEXPORT jlong JNICALL Java_net_sf_javabdd_CUDDFactory_00024CUDDBDD_forAll0
(JNIEnv *env, jclass cl, jlong a, jlong b) {
  DdNode* d;
  DdNode* e;
  DdNode* f;
  jlong result;
  d = (DdNode*) (intptr_cast_type) a;
  e = (DdNode*) (intptr_cast_type) b;
  f = Cudd_bddUnivAbstract(manager, d, e);
  result = (jlong) (intptr_cast_type) f;
  return result;
}

/*
 * Class:     net_sf_javabdd_CUDDFactory_CUDDBDD
 * Method:    restrict0
 * Signature: (JJ)J
 */
JNIEXPORT jlong JNICALL Java_net_sf_javabdd_CUDDFactory_00024CUDDBDD_restrict0
(JNIEnv *env, jclass cl, jlong a, jlong b) {
  DdNode* d;
  DdNode* e;
  DdNode* f;
  jlong result;
  d = (DdNode*) (intptr_cast_type) a;
  e = (DdNode*) (intptr_cast_type) b;
  f = Cudd_bddRestrict(manager, d, e);
  result = (jlong) (intptr_cast_type) f;
  return result;
}

JNIEXPORT jlong JNICALL Java_net_sf_javabdd_CUDDFactory_00024CUDDBDD_restrict_1lim0
(JNIEnv *env, jclass cl, jlong a, jlong b, jlong t_out) {
  DdNode* d;
  DdNode* e;
  DdNode* f;
  jlong result;
  long timeout = (long)t_out;
  if (timeout > 0){
    Cudd_SetTimeLimit(manager, timeout);
    Cudd_ResetStartTime(manager);
  }

  d = (DdNode*) (intptr_cast_type) a;
  e = (DdNode*) (intptr_cast_type) b;
  f = Cudd_bddRestrict(manager, d, e);


  if(timeout > 0){
    Cudd_UnsetTimeLimit(manager);
  }

  if(f == NULL){
    //printf("Return invalid\n");
    return INVALID_BDD;
  }
  result = (jlong) (intptr_cast_type) f;
  return result;
}


JNIEXPORT jlong JNICALL Java_net_sf_javabdd_CUDDFactory_00024CUDDBDD_constrain_1lim0
(JNIEnv *env, jclass cl, jlong a, jlong b, jlong t_out) {
  DdNode* d;
  DdNode* e;
  DdNode* f;
  jlong result;
  long timeout = (long)t_out;
  if (timeout > 0){
    Cudd_SetTimeLimit(manager, timeout);
    Cudd_ResetStartTime(manager);
  }


  d = (DdNode*) (intptr_cast_type) a;
  e = (DdNode*) (intptr_cast_type) b;
  f = Cudd_bddConstrain(manager, d, e);


  if(timeout > 0){
    Cudd_UnsetTimeLimit(manager);
  }

  if(f == NULL){
    //printf("Return invalid\n");
    return INVALID_BDD;
  }
  result = (jlong) (intptr_cast_type) f;
  return result;
}
JNIEXPORT jlong JNICALL Java_net_sf_javabdd_CUDDFactory_00024CUDDBDD_nonPollutingAnd_1lim0
(JNIEnv * env, jclass cl, jlong a, jlong b, jlong t_out){
  DdNode* d;
  DdNode* e;
  DdNode* f;
  jlong result;
  long timeout = (long)t_out;
  if (timeout > 0){
    Cudd_SetTimeLimit(manager, timeout);
    Cudd_ResetStartTime(manager);
  }

  d = (DdNode*) (intptr_cast_type) a;
  e = (DdNode*) (intptr_cast_type) b;
  f = Cudd_bddNPAnd(manager, d, e);


  if(timeout > 0){
    Cudd_UnsetTimeLimit(manager);
  }

  if(f == NULL){
    //printf("Return invalid\n");
    return INVALID_BDD;
  }

  result = (jlong) (intptr_cast_type) f;
  return result;
}


/*
 * Class:     net_sf_javabdd_CUDDFactory_CUDDBDD
 * Method:    liCompaction0
 * Signature: (JJ)J
 */
JNIEXPORT jlong JNICALL Java_net_sf_javabdd_CUDDFactory_00024CUDDBDD_liCompaction_1lim0
(JNIEnv * env, jclass cl, jlong a, jlong b, jlong t_out){
  DdNode* d;
  DdNode* e;
  DdNode* f;
  jlong result;
  long timeout = (long)t_out;

  if (timeout > 0){
    Cudd_SetTimeLimit(manager, timeout);
    Cudd_ResetStartTime(manager);
  }

  d = (DdNode*) (intptr_cast_type) a;
  e = (DdNode*) (intptr_cast_type) b;
  f = Cudd_bddLICompaction(manager, d, e);

  if(timeout > 0){
    Cudd_UnsetTimeLimit(manager);
  }

  if(f == NULL){
    //printf("Return invalid\n");
    return INVALID_BDD;
  }

  result = (jlong) (intptr_cast_type) f;

  return result;
}



/*
 * Class:     net_sf_javabdd_CUDDFactory_CUDDBDD
 * Method:    support0
 * Signature: (J)J
 */
JNIEXPORT jlong JNICALL Java_net_sf_javabdd_CUDDFactory_00024CUDDBDD_support0
(JNIEnv *env, jclass cl, jlong a) {
  DdNode *d;
  DdNode *e;
  jlong result;
  d = (DdNode*) (intptr_cast_type) a;
  e = Cudd_Support(manager, d);
  result = (jlong) (intptr_cast_type) e;
  return result;
}


/*
 * Class:     net_sf_javabdd_CUDDFactory_CUDDBDD
 * Method:    apply0
 * Signature: (JJI)J
 */
JNIEXPORT jlong JNICALL Java_net_sf_javabdd_CUDDFactory_00024CUDDBDD_apply0
(JNIEnv *env, jclass cl, jlong a, jlong b, jint oper) {
  DdNode* d;
  DdNode* e;
  DdNode* f;
  jlong result;

  d = (DdNode*) (intptr_cast_type) a;
  e = (DdNode*) (intptr_cast_type) b;
  switch (oper) {
  case 0: /* and */
    f = Cudd_bddAnd(manager, d, e);
    break;
  case 1: /* xor */
    f = Cudd_bddXor(manager, d, e);
    break;
  case 2: /* or */
    f = Cudd_bddOr(manager, d, e);
    break;
  case 3: /* nand */
    f = Cudd_bddNand(manager, d, e);
    break;
  case 4: /* nor */
    f = Cudd_bddNor(manager, d, e);
    break;
  case 5: /* imp */
    d = Cudd_Not(d);
    Cudd_Ref(d);
    f = Cudd_bddOr(manager, d, e);
    Cudd_RecursiveDeref(manager, d);
    break;
  case 6: /* biimp */
    f = Cudd_bddXnor(manager, d, e);
    break;
  case 7: /* diff */
    e = Cudd_Not(e);
    Cudd_Ref(e);
    f = Cudd_bddAnd(manager, d, e);
    Cudd_RecursiveDeref(manager, e);
    break;
  case 8: /* less */
    d = Cudd_Not(d);
    Cudd_Ref(d);
    f = Cudd_bddAnd(manager, d, e);
    Cudd_RecursiveDeref(manager, d);
    break;
  case 9: /* invimp */
    e = Cudd_Not(e);
    Cudd_Ref(e);
    f = Cudd_bddOr(manager, d, e);
    Cudd_RecursiveDeref(manager, e);
    break;
  default:
    die(env, "operation not supported");
    return INVALID_BDD;
  }
  result = (jlong) (intptr_cast_type) f;
  return result;
}

JNIEXPORT jlong JNICALL Java_net_sf_javabdd_CUDDFactory_00024CUDDBDD_apply_1maxsize0
(JNIEnv * env, jclass cl, jlong a, jlong b, jint oper, jlong max_size){

  DdNode * d;
  DdNode* e;
  DdNode* f;
  jlong result;
  long maxSize = (long)max_size;
  //    printf("Apply MaxSize %ld: \n", maxSize);

  d = (DdNode*) (intptr_cast_type) a;
  e = (DdNode*) (intptr_cast_type) b;
  switch (oper) {
  case 0: /* and */
    f = Cudd_bddAndLimit(manager, d, e, maxSize);
    break;
    //case 1: /* xor */
    //	  f = Cudd_bddXorLimit(manager, d, e, maxSize);
    // break;
  case 2: /* or */
    f = Cudd_bddOrLimit(manager, d, e, maxSize);
    break;
    /*case 3: // nand 
      f = Cudd_bddNandLimit(manager, d, e, maxSize);
      break;*/
    /*    case 4: // nor 
	  f = Cudd_bddNorLimit(manager, d, e, maxSize);
	  break;*/
  case 5: /* imp */
    d = Cudd_Not(d);
    Cudd_Ref(d);
    f = Cudd_bddOrLimit(manager, d, e, maxSize);
    Cudd_RecursiveDeref(manager, d);
    break;
  case 6: /* biimp */
    f = Cudd_bddXnorLimit(manager, d, e, maxSize);
    break;
  case 7: /* diff */
    e = Cudd_Not(e);
    Cudd_Ref(e);
    f = Cudd_bddAndLimit(manager, d, e, maxSize);
    Cudd_RecursiveDeref(manager, e);
    break;
  case 8: /* less */
    d = Cudd_Not(d);
    Cudd_Ref(d);
    f = Cudd_bddAndLimit(manager, d, e, maxSize);
    Cudd_RecursiveDeref(manager, d);
    break;
  case 9: /* invimp */
    e = Cudd_Not(e);
    Cudd_Ref(e);
    f = Cudd_bddOrLimit(manager, d, e, maxSize);
    Cudd_RecursiveDeref(manager, e);
    break;
  default:
    die(env, "operation not supported");
    return INVALID_BDD;
  }
  if(f == NULL){
    //printf("Return invalid\n");
    return INVALID_BDD;
  }
  result = (jlong) (intptr_cast_type) f;
  return result;
}

/*
 * Class:     net_sf_javabdd_CUDDFactory_CUDDBDD
 * Method:    apply_timeout0
 * Signature: (JJIJ)J
 */
JNIEXPORT jlong JNICALL Java_net_sf_javabdd_CUDDFactory_00024CUDDBDD_apply_1timeout0
(JNIEnv *env, jclass cl, jlong a, jlong b, jint oper, jlong t_out, jlong maxN) {
  DdNode* d;
  DdNode* e;
  DdNode* f;
  jlong result;
  long timeout = (long)t_out;
  long maxNodes = (long)maxN;

  d = (DdNode*) (intptr_cast_type) a;
  e = (DdNode*) (intptr_cast_type) b;

  if (timeout > 0){
    Cudd_SetTimeLimit(manager, timeout);
    Cudd_ResetStartTime(manager);
  }

  //    g = Cudd_bddAndAbstractTimeout(manager, d, e, f, timeout);
  if (maxNodes > 0){
    switch (oper) {
    case 0: /* and */
      f = Cudd_bddAndLimit(manager, d, e, maxNodes);
      break;
    case 1: /* xor */
      f = Cudd_bddXorLimit(manager, d, e, maxNodes);
      break;
    case 2: /* or */
      f = Cudd_bddOrLimit(manager, d, e, maxNodes);
      break;
    case 3: /* nand */
      f = Cudd_bddNandLimit(manager, d, e, maxNodes);
      break;
    case 4: /* nor */
      f = Cudd_bddNorLimit(manager, d, e, maxNodes);
      break;
    case 5: /* imp */
      d = Cudd_Not(d);
      Cudd_Ref(d);
      f = Cudd_bddOrLimit(manager, d, e, maxNodes);
      Cudd_RecursiveDeref(manager, d);
      break;
    case 6: /* biimp */
      f = Cudd_bddXnorLimit(manager, d, e, maxNodes);
      break;
    case 7: /* diff */
      e = Cudd_Not(e);
      Cudd_Ref(e);
      f = Cudd_bddAndLimit(manager, d, e, maxNodes);
      Cudd_RecursiveDeref(manager, e);
      break;
    case 8: /* less */
      d = Cudd_Not(d);
      Cudd_Ref(d);
      f = Cudd_bddAndLimit(manager, d, e, maxNodes);
      Cudd_RecursiveDeref(manager, d);
      break;
    case 9: /* invimp */
      e = Cudd_Not(e);
      Cudd_Ref(e);
      f = Cudd_bddOrLimit(manager, d, e, maxNodes);
      Cudd_RecursiveDeref(manager, e);
      break;
    default:
      die(env, "operation not supported");
      return INVALID_BDD;
    }
  }else{
    switch (oper) {
    case 0: /* and */
      f = Cudd_bddAnd(manager, d, e);
      break;
    case 1: /* xor */
      f = Cudd_bddXor(manager, d, e);
      break;
    case 2: /* or */
      f = Cudd_bddOr(manager, d, e);
      break;
    case 3: /* nand */
      f = Cudd_bddNand(manager, d, e);
      break;
    case 4: /* nor */
      f = Cudd_bddNor(manager, d, e);
      break;
    case 5: /* imp */
      d = Cudd_Not(d);
      Cudd_Ref(d);
      f = Cudd_bddOr(manager, d, e);
      Cudd_RecursiveDeref(manager, d);
      break;
    case 6: /* biimp */
      f = Cudd_bddXnor(manager, d, e);
      break;
    case 7: /* diff */
      e = Cudd_Not(e);
      Cudd_Ref(e);
      f = Cudd_bddAnd(manager, d, e);
      Cudd_RecursiveDeref(manager, e);
      break;
    case 8: /* less */
      d = Cudd_Not(d);
      Cudd_Ref(d);
      f = Cudd_bddAnd(manager, d, e);
      Cudd_RecursiveDeref(manager, d);
      break;
    case 9: /* invimp */
      e = Cudd_Not(e);
      Cudd_Ref(e);
      f = Cudd_bddOr(manager, d, e);
      Cudd_RecursiveDeref(manager, e);
      break;
    default:
      die(env, "operation not supported");
      return INVALID_BDD;
    }
  }

  if(timeout > 0){
    Cudd_UnsetTimeLimit(manager);
  }

  if(f == NULL){
    //printf("Return invalid\n");
    return INVALID_BDD;
  }

  result = (jlong) (intptr_cast_type) f;
  return result;
}
/*
 * Class:     net_sf_javabdd_CUDDFactory_CUDDBDD
 * Method:    apply_all0
 * Signature: ([JI)J
 */
JNIEXPORT jlong JNICALL Java_net_sf_javabdd_CUDDFactory_00024CUDDBDD_apply_1all0
(JNIEnv * env, jclass cl, jlongArray bdds_array, jint oper ){
  DdNode* f;
  jlong result;
  //    printf("Size of long %d and size of DdNode* %d\n", sizeof(long), sizeof(DdNode * ));

  int num_bdds = (*env)->GetArrayLength(env, bdds_array);
  DdNode ** bdds = readNodeArray(env, bdds_array, num_bdds);

  switch (oper) {
  case 0: /* and */
    f = Cudd_bddAndAll(manager, num_bdds, bdds);
    break;
    //        case 1: /* xor */
    // f = Cudd_bddXorTimeout(manager, d, e, timeout);
    // break;
  case 2: /* or */
    f = Cudd_bddOrAll(manager, num_bdds, bdds);
    break;
    //        case 3: /* nand */
    //            f = Cudd_bddNandTimeout(manager, d, e, timeout);
    //            break;
    //        case 4: /* nor */
    // f = Cudd_bddNorTimeout(manager, d, e, timeout);
    // break;
    // case 5: /* imp */
    // d = Cudd_Not(d);
    // Cudd_Ref(d);
    // f = Cudd_bddOrTimeout(manager, d, e, timeout);
    // Cudd_RecursiveDeref(manager, d);
    // break;
    //        case 6: /* biimp */
    /*     f = Cudd_bddXnorTimeout(manager, d, e, timeout); */
    /*     break; */
    /* case 7: /\* diff *\/ */
    /*     e = Cudd_Not(e); */
    /*     Cudd_Ref(e); */
    /*     f = Cudd_bddAndTimeout(manager, d, e, timeout); */
    /*     Cudd_RecursiveDeref(manager, e); */
    /*     break; */
    /* case 8: /\* less *\/ */
    /*     d = Cudd_Not(d); */
    /*     Cudd_Ref(d); */
    /*     f = Cudd_bddAndTimeout(manager, d, e, timeout); */
    /*     Cudd_RecursiveDeref(manager, d); */
    /*     break; */
    /* case 9: /\* invimp *\/ */
    /*     e = Cudd_Not(e); */
    /*     Cudd_Ref(e); */
    /*     f = Cudd_bddOrTimeout(manager, d, e, timeout); */
    /*     Cudd_RecursiveDeref(manager, e); */
    /*     break; */
  default:
    die(env, "operation not supported");
    free(bdds);
    return INVALID_BDD;
  }
  free(bdds);
    
  if(f == NULL){
    //printf("Return invalid\n");
    return INVALID_BDD;
  }
  result = (jlong) (intptr_cast_type) f;
  return result;
}

/*
 * Class:     net_sf_javabdd_CUDDFactory_CUDDBDD
 * Method:    apply_all_timeout0
 * Signature: ([JIJ)J
 */
JNIEXPORT jlong JNICALL Java_net_sf_javabdd_CUDDFactory_00024CUDDBDD_apply_1all_1timeout0
(JNIEnv *env, jclass cl, jlongArray bdds_array, jint oper, jlong t_out, jlong maxN) {
  DdNode* f;
  jlong result;
  long timeout = (long)t_out;
  long maxNodes = (long)maxN;

  int num_bdds = (*env)->GetArrayLength(env, bdds_array);
  DdNode ** bdds = readNodeArray(env, bdds_array, num_bdds);

  switch (oper) {
  case 0: /* and */
    f = Cudd_bddAndAllTimeout(manager, num_bdds, bdds, timeout);
    break;
    //        case 1: /* xor */
    // f = Cudd_bddXorTimeout(manager, d, e, timeout);
    // break;
  case 2: /* or */
    f = Cudd_bddOrAllTimeout(manager, num_bdds, bdds, timeout);
    break;
    //        case 3: /* nand */
    //            f = Cudd_bddNandTimeout(manager, d, e, timeout);
    //            break;
    //        case 4: /* nor */
    // f = Cudd_bddNorTimeout(manager, d, e, timeout);
    // break;
    // case 5: /* imp */
    // d = Cudd_Not(d);
    // Cudd_Ref(d);
    // f = Cudd_bddOrTimeout(manager, d, e, timeout);
    // Cudd_RecursiveDeref(manager, d);
    // break;
    //        case 6: /* biimp */
    /*     f = Cudd_bddXnorTimeout(manager, d, e, timeout); */
    /*     break; */
    /* case 7: /\* diff *\/ */
    /*     e = Cudd_Not(e); */
    /*     Cudd_Ref(e); */
    /*     f = Cudd_bddAndTimeout(manager, d, e, timeout); */
    /*     Cudd_RecursiveDeref(manager, e); */
    /*     break; */
    /* case 8: /\* less *\/ */
    /*     d = Cudd_Not(d); */
    /*     Cudd_Ref(d); */
    /*     f = Cudd_bddAndTimeout(manager, d, e, timeout); */
    /*     Cudd_RecursiveDeref(manager, d); */
    /*     break; */
    /* case 9: /\* invimp *\/ */
    /*     e = Cudd_Not(e); */
    /*     Cudd_Ref(e); */
    /*     f = Cudd_bddOrTimeout(manager, d, e, timeout); */
    /*     Cudd_RecursiveDeref(manager, e); */
    /*     break; */
  default:
    free(bdds);
    die(env, "operation not supported");
    return INVALID_BDD;
  }
  free(bdds);
  if(f == NULL){
    //printf("Return invalid\n");
    return INVALID_BDD;
  }
  result = (jlong) (intptr_cast_type) f;
  return result;
}

static DdNode* satone_rec(DdNode* f) {
  DdNode* zero = (DdNode*)(intptr_cast_type)bdd_zero;
  DdNode* one = (DdNode*)(intptr_cast_type)bdd_one;
  DdNode* F = Cudd_Regular(f);
  DdNode* high;
  DdNode* low;
  DdNode* r;
  unsigned int index;
    
  if (F == zero ||
      F == one) {
    return f;
  }
    
  index = F->index;
  high = cuddT(F);
  low = cuddE(F);
  if (Cudd_IsComplement(f)) {
    high = Cudd_Not(high);
    low = Cudd_Not(low);
  }
  if (low == (DdNode*)(intptr_cast_type)bdd_zero) {
    DdNode* res = satone_rec(high);
    if (res == NULL) {
      return NULL;
    }
    cuddRef(res);
    if (Cudd_IsComplement(res)) {
      r = cuddUniqueInter(manager, (int)index, Cudd_Not(res), one);
      if (r == NULL) {
	Cudd_IterDerefBdd(manager, res);
	return NULL;
      }
      r = Cudd_Not(r);
    } else {
      r = cuddUniqueInter(manager, (int)index, res, zero);
      if (r == NULL) {
	Cudd_IterDerefBdd(manager, res);
	return NULL;
      }
    }
    cuddDeref(res);
  } else {
    DdNode* res = satone_rec(low);
    if (res == NULL) return NULL;
    cuddRef(res);
    r = cuddUniqueInter(manager, (int)index, one, Cudd_Not(res));
    if (r == NULL) {
      Cudd_IterDerefBdd(manager, res);
      return NULL;
    }
    r = Cudd_Not(r);
    cuddDeref(res);
  }
    
  return r;
}

/*
 * Class:     net_sf_javabdd_CUDDFactory_CUDDBDD
 * Method:    satOne0
 * Signature: (J)J
 */
JNIEXPORT jlong JNICALL Java_net_sf_javabdd_CUDDFactory_00024CUDDBDD_satOne0
(JNIEnv *env, jclass cl, jlong a) {
  DdNode* d;
  DdNode* res;
  jlong result;
    
  d = (DdNode*) (intptr_cast_type) a;
    
  do {
    manager->reordered = 0;
    res = satone_rec(d);
  } while (manager->reordered == 1);
    
  result = (jlong) (intptr_cast_type) res;
  return result;
}





JNIEXPORT jlongArray JNICALL Java_net_sf_javabdd_CUDDFactory_00024CUDDBDD_split_1states_1max_1folds
(JNIEnv * env, jclass cl, jint split_type, jlong node, jlong num_states, jint max_folds, jintArray relevant_variables){

  DdNode* res;
  long numStates = num_states;
  DdNode* d = (DdNode*) (intptr_cast_type) node;

  int num_rel_variables = (*env)->GetArrayLength(env, relevant_variables);
  int * rel_variables = (int*)((*env)->GetPrimitiveArrayCritical(env, relevant_variables, NULL));

  int numFolds; 
  DdNode ** sp;

  if(split_type == net_sf_javabdd_CUDDFactory_SPLIT_STATES_TYPE_LEX){
    init_tree_levels_variables(manager, &levs, num_rel_variables, rel_variables);
    long sc = satCount(manager, &levs, satCountCache, d);

    //  printf("Entering in uniform split states sc %ld max_states %ld\n", sc, numStates);

    numFolds = (sc + numStates -1)/numStates;
    if(numFolds <= 0) numFolds = 1;
    else if(numFolds > max_folds) numFolds = max_folds;
    sp = (DdNode **) malloc (numFolds*sizeof(DdNode*));
  
    if(numFolds > 1){
      //    printf("Sat count: %ld Start uniform split in %d folds\n", sc, numFolds);
      numFolds = split_uniform(manager, &levs,  satCountCache, d, numFolds, sp);
      //    printf("Splitted in %d folds\n", numFolds); 
      int i;
      for(i = 0; i < numFolds; i++) Cudd_Ref(sp[i]);
      //Cudd_RecursiveDeref(manager, d); //Dereference the big bdd
    }else{
      sp[0] = d;
    }

  }else if(split_type == net_sf_javabdd_CUDDFactory_SPLIT_STATES_TYPE_CUDD){
    DdNode* aux_bdd = d;
    DdNode * res_split_1, * res_split_2; 

    DdNode ** node_rel_variables = (DdNode **) malloc (num_rel_variables*sizeof(DdNode*)); 
    int i;
    for(i = 0; i < num_rel_variables; i++){
      //    printf("%d ", rel_variables[i]);
      node_rel_variables [i] = Cudd_bddIthVar(manager, rel_variables[i]);
      Cudd_Ref(node_rel_variables[i]);
    }

    int varcount = num_rel_variables;
    double sc = Cudd_CountMinterm(manager, d, varcount);


    //  printf("Entering in uniform split states sc %lf max_states %ld\n", sc, numStates);
    numFolds = ((long)sc + numStates -1)/numStates;

    if(numFolds <= 0) numFolds = 1;
    else if(numFolds > max_folds) numFolds = max_folds;

    int numFoldsAux = numFolds;
    sp = (DdNode **) malloc (numFolds*sizeof(DdNode*));
  
    while(numFoldsAux > 1){
      printf ("num folds %d  num nodes %d sat count %ld\n", numFoldsAux, Cudd_DagSize(aux_bdd) -1, (long)Cudd_CountMinterm(manager, aux_bdd, varcount));
      res_split_1 = Cudd_SplitSet(manager,aux_bdd, node_rel_variables,
				  num_rel_variables, numStates);
      Cudd_Ref(res_split_1);

    
      printf("Res split 1 num nodes %d  sat count %ld\n", Cudd_DagSize(res_split_1) -1, (long)Cudd_CountMinterm(manager, res_split_1, varcount));

      res_split_2 = Cudd_bddAnd(manager,aux_bdd, Cudd_Not(res_split_1));
      Cudd_Ref(res_split_2);
      printf("Res split 2 num nodes %d sat count %ld\n", Cudd_DagSize(res_split_2) -1, (long)Cudd_CountMinterm(manager, res_split_2, varcount));


      if(aux_bdd != d){
	Cudd_RecursiveDeref(manager, aux_bdd); //Dereference the auxiliary bdd
      }
      sp [numFoldsAux-1] = res_split_1;
      aux_bdd = res_split_2;
      numFoldsAux --;
    }
    //  printf("Finished partitioning\n");
  
    sp[0] = aux_bdd;

    for(i = 0; i < num_rel_variables; i++){
      Cudd_RecursiveDeref(manager, node_rel_variables[i]);
    }
    free(node_rel_variables);  
  }

  (*env)->ReleasePrimitiveArrayCritical(env, relevant_variables, rel_variables, JNI_ABORT);


  jlongArray result=(*env)->NewLongArray(env, numFolds);
  (*env)->SetLongArrayRegion(env, result, 0, numFolds, (jlong *)sp);
  free(sp);


  return result;

}


/*
 * Class:     net_sf_javabdd_CUDDFactory_CUDDBDD
 * Method:    split_states
 * Signature: (IJJ[I)[J
 */
JNIEXPORT jlongArray JNICALL Java_net_sf_javabdd_CUDDFactory_00024CUDDBDD_split_1states
(JNIEnv * env, jclass cl, jint split_type, jlong node, jlong num_states, jintArray relevant_variables){

  DdNode* res;
  long numStates = num_states;
  DdNode* d = (DdNode*) (intptr_cast_type) node;

  int num_rel_variables = (*env)->GetArrayLength(env, relevant_variables);
  int * rel_variables = (int*)((*env)->GetPrimitiveArrayCritical(env, relevant_variables, NULL));

  int numFolds; 
  DdNode ** sp;

  if(split_type == net_sf_javabdd_CUDDFactory_SPLIT_STATES_TYPE_LEX){
    init_tree_levels_variables(manager, &levs, num_rel_variables, rel_variables);
    long sc = satCount(manager, &levs, satCountCache, d);

    //  printf("Entering in uniform split states sc %ld max_states %ld\n", sc, numStates);

    numFolds = (sc + numStates -1)/numStates;
    if(numFolds <= 0) numFolds = 1;
    sp = (DdNode **) malloc (numFolds*sizeof(DdNode*));
  
    if(numFolds > 1){
      //    printf("Sat count: %ld Start uniform split in %d folds\n", sc, numFolds);
      numFolds = split_uniform(manager, &levs,  satCountCache, d, numFolds, sp);
      //    printf("Splitted in %d folds\n", numFolds); 
      int i;
      for(i = 0; i < numFolds; i++) Cudd_Ref(sp[i]);
      //Cudd_RecursiveDeref(manager, d); //Dereference the big bdd
    }else{
      sp[0] = d;
    }

  }else if(split_type == net_sf_javabdd_CUDDFactory_SPLIT_STATES_TYPE_CUDD){
    DdNode* aux_bdd = d;
    DdNode * res_split_1, * res_split_2; 

    DdNode ** node_rel_variables = (DdNode **) malloc (num_rel_variables*sizeof(DdNode*)); 
    int i;
    for(i = 0; i < num_rel_variables; i++){
      //    printf("%d ", rel_variables[i]);
      node_rel_variables [i] = Cudd_bddIthVar(manager, rel_variables[i]);
      Cudd_Ref(node_rel_variables[i]);
    }

    int varcount = num_rel_variables;
    double sc = Cudd_CountMinterm(manager, d, varcount);


    //  printf("Entering in uniform split states sc %lf max_states %ld\n", sc, numStates);
    numFolds = ((long)sc + numStates -1)/numStates;

    if(numFolds <= 0) numFolds = 1;
    int numFoldsAux = numFolds;
    sp = (DdNode **) malloc (numFolds*sizeof(DdNode*));
  
    while(numFoldsAux > 1){
      printf ("num folds %d  num nodes %d sat count %ld\n", numFoldsAux, Cudd_DagSize(aux_bdd) -1, (long)Cudd_CountMinterm(manager, aux_bdd, varcount));
      res_split_1 = Cudd_SplitSet(manager,aux_bdd, node_rel_variables,
				  num_rel_variables, numStates);
      Cudd_Ref(res_split_1);

    
      printf("Res split 1 num nodes %d  sat count %ld\n", Cudd_DagSize(res_split_1) -1, (long)Cudd_CountMinterm(manager, res_split_1, varcount));

      res_split_2 = Cudd_bddAnd(manager,aux_bdd, Cudd_Not(res_split_1));
      Cudd_Ref(res_split_2);
      printf("Res split 2 num nodes %d sat count %ld\n", Cudd_DagSize(res_split_2) -1, (long)Cudd_CountMinterm(manager, res_split_2, varcount));


      if(aux_bdd != d){
	Cudd_RecursiveDeref(manager, aux_bdd); //Dereference the auxiliary bdd
      }
      sp [numFoldsAux-1] = res_split_1;
      aux_bdd = res_split_2;
      numFoldsAux --;
    }
    //  printf("Finished partitioning\n");
  
    sp[0] = aux_bdd;

    for(i = 0; i < num_rel_variables; i++){
      Cudd_RecursiveDeref(manager, node_rel_variables[i]);
    }
    free(node_rel_variables);  
  }

  (*env)->ReleasePrimitiveArrayCritical(env, relevant_variables, rel_variables, JNI_ABORT);


  jlongArray result=(*env)->NewLongArray(env, numFolds);
  (*env)->SetLongArrayRegion(env, result, 0, numFolds, (jlong *)sp);
  free(sp);


  return result;
}

/*
 * Class:     net_sf_javabdd_CUDDFactory_CUDDBDD
 * Method:    split_states_folds
 * Signature: (IJI[I)[J
 */
JNIEXPORT jlongArray JNICALL Java_net_sf_javabdd_CUDDFactory_00024CUDDBDD_split_1states_1folds
(JNIEnv *env, jclass cl, jint split_type, jlong node, jint num_folds, jintArray relevant_variables){

  DdNode* d = (DdNode*) (intptr_cast_type) node;

  int num_rel_variables = (*env)->GetArrayLength(env, relevant_variables);
  int * rel_variables = (int*)((*env)->GetPrimitiveArrayCritical(env, relevant_variables, NULL));

  DdNode ** sp;
  int numFolds = num_folds; 
  if(split_type == net_sf_javabdd_CUDDFactory_SPLIT_STATES_TYPE_LEX){
    init_tree_levels_variables(manager, &levs, num_rel_variables, rel_variables);

    long sc = satCount(manager, &levs, satCountCache, d);
    
    if(numFolds > sc) numFolds = sc;
    if(numFolds <= 0) numFolds = 1;

    sp = (DdNode **) malloc (numFolds*sizeof(DdNode*));

    if(numFolds > 1){
      //printf("Sat count: %ld Start uniform split in %d folds\n", sc, numFolds);
      numFolds = split_uniform(manager, &levs,  satCountCache, d, numFolds, sp);
      // printf("Splitted in %d folds\n", numFolds); 
      int i;
      for(i = 0; i < numFolds; i++) Cudd_Ref(sp[i]);
      //Cudd_RecursiveDeref(manager, d); //Dereference the big bdd
    }else{
      sp[0] = d;
    }

  }else if(split_type == net_sf_javabdd_CUDDFactory_SPLIT_STATES_TYPE_CUDD){
    DdNode* aux_bdd = d;
    DdNode * res_split_1, * res_split_2; 

    DdNode ** node_rel_variables = (DdNode **) malloc (num_rel_variables*sizeof(DdNode*)); 
    int i;
    for(i = 0; i < num_rel_variables; i++){
      node_rel_variables [i] = Cudd_bddIthVar(manager, rel_variables[i]);
      Cudd_Ref(node_rel_variables[i]);
    }

    int varcount = num_rel_variables;
    double sc = Cudd_CountMinterm(manager, d, varcount);

    if(numFolds > sc) numFolds = sc;
    if(numFolds <= 0) numFolds = 1;
    int numFoldsAux = numFolds;
    int numStates = sc/numFolds;
    sp = (DdNode **) malloc (numFolds*sizeof(DdNode*));
    while(numFoldsAux > 1){
      res_split_1 = Cudd_SplitSet(manager,aux_bdd, node_rel_variables,
				  num_rel_variables, numStates);
      Cudd_Ref(res_split_1);
      
      res_split_2 = Cudd_bddAnd(manager,aux_bdd, Cudd_Not(res_split_1));
      //    res_split_2 = Cudd_bddRestrict(manager,aux_bdd, Cudd_Not(res_split_1));
      Cudd_Ref(res_split_2);

      //    printf("Restrict Part 1: %d, Part 2: %d\n", Cudd_DagSize(res_split_1), Cudd_DagSize(res_split_2));
      printf("And Part 1: %d, Part 2: %d\n", Cudd_DagSize(res_split_1), Cudd_DagSize(res_split_2));
    
      if(aux_bdd != d){
	Cudd_RecursiveDeref(manager, aux_bdd); //Dereference the auxiliary bdd
      }
      sp [numFoldsAux-1] = res_split_1;
      aux_bdd = res_split_2;
      numFoldsAux --;
    }
    
    sp[0] = aux_bdd;
    for(i = 0; i < num_rel_variables; i++){
      Cudd_RecursiveDeref(manager, node_rel_variables[i]);
    }
    free(node_rel_variables);
  }
  (*env)->ReleasePrimitiveArrayCritical(env, relevant_variables, rel_variables, JNI_ABORT);
 
  jlongArray result=(*env)->NewLongArray(env, numFolds);
  (*env)->SetLongArrayRegion(env, result, 0, numFolds, (jlong *)sp);
  free(sp);
  return result;  
}

/*
 * Class:     net_sf_javabdd_CUDDFactory_CUDDBDD
 * Method:    split_nodes
 * Signature: (IJJ[I)[J
 */
JNIEXPORT jlongArray JNICALL Java_net_sf_javabdd_CUDDFactory_00024CUDDBDD_split_1nodes
(JNIEnv * env, jclass cl, jint split_type, jlong node, jlong num_nodes, jintArray relevant_variables){
  DdNode* res;
  DdNode* d = (DdNode*) (intptr_cast_type) node;
  long numNodes = num_nodes;
  long nodeCount = Cudd_DagSize(d)-1;

   
  int num_rel_variables = (*env)->GetArrayLength(env, relevant_variables);
  int * rel_variables = (int*)((*env)->GetPrimitiveArrayCritical(env, relevant_variables, NULL));

  int numFolds = nodeCount/numNodes; 
  if(nodeCount < 100) numFolds = 1;
  if(numFolds <= 0) numFolds = 1;
  int numFoldsAux = numFolds;
  
  DdNode ** sp = (DdNode **) malloc (numFolds*sizeof(DdNode*));
  DdNode *aux_bdd = d;
  DdNode * res_split_1, * res_split_2;

  while(numFoldsAux > 1){  
    if(split_type == net_sf_javabdd_CUDDFactory_SPLIT_NODES_TYPE_COMPRESS){
      res_split_1 = Cudd_SubsetCompress(manager,aux_bdd, num_rel_variables, numNodes);
    }else if(split_type == net_sf_javabdd_CUDDFactory_SPLIT_NODES_TYPE_HBRANCH){
      res_split_1 = Cudd_SubsetHeavyBranch(manager,aux_bdd, num_rel_variables, numNodes);
    }else if(split_type == net_sf_javabdd_CUDDFactory_SPLIT_NODES_TYPE_SPATH){
      res_split_1 = Cudd_SubsetShortPaths(manager,aux_bdd, num_rel_variables, numNodes, 0);
    }
    Cudd_Ref(res_split_1);
    res_split_2 = Cudd_bddAnd(manager,aux_bdd, Cudd_Not(res_split_1));
    printf("APPLYING AND\n");
    //res_split_2 = Cudd_bddRestrict(manager,aux_bdd, Cudd_Not(res_split_1));
    Cudd_Ref(res_split_2);

    printf("Part 1: %d, Part 2: %d\n", Cudd_DagSize(res_split_1), Cudd_DagSize(res_split_2));

    if(aux_bdd != d){
      Cudd_RecursiveDeref(manager, aux_bdd); //Dereference the auxiliary bdd
    }
    sp [numFoldsAux-1] = res_split_1;
    aux_bdd = res_split_2;
    numFoldsAux --;
  }
  
  sp[0] = aux_bdd;

 
  (*env)->ReleasePrimitiveArrayCritical(env, relevant_variables, rel_variables, JNI_ABORT);

  jlongArray result=(*env)->NewLongArray(env, numFolds);
  (*env)->SetLongArrayRegion(env, result, 0, numFolds, (jlong *)sp);
  free(sp);

  return result;
}

/*
 * Class:     net_sf_javabdd_CUDDFactory_CUDDBDD
 * Method:    split_nodes_folds
 * Signature: (IJJ[I)[J
 */
JNIEXPORT jlongArray JNICALL Java_net_sf_javabdd_CUDDFactory_00024CUDDBDD_split_1nodes_1folds
(JNIEnv * env, jclass cl, jint split_type, jlong node, jlong num_folds, jintArray relevant_variables){
  DdNode* res;
  DdNode* d = (DdNode*) (intptr_cast_type) node;

  long nodeCount = Cudd_DagSize(d)-1;
   
  int num_rel_variables = (*env)->GetArrayLength(env, relevant_variables);
  int * rel_variables = (int*)((*env)->GetPrimitiveArrayCritical(env, relevant_variables, NULL));

  int numFolds = num_folds; 
  
  if(numFolds <= 0) numFolds = 1;
  int numNodes = nodeCount/numFolds;
  if(numNodes < 100){
    numFolds = 1;
  }
  int numFoldsAux = numFolds;

  
  DdNode ** sp = (DdNode **) malloc (numFolds*sizeof(DdNode*));
  DdNode *aux_bdd = d;
  DdNode * res_split_1, * res_split_2;

  while(numFoldsAux > 1){  
    if(split_type == net_sf_javabdd_CUDDFactory_SPLIT_NODES_TYPE_COMPRESS){
      res_split_1 = Cudd_SubsetCompress(manager,aux_bdd, num_rel_variables, numNodes);
    }else if(split_type == net_sf_javabdd_CUDDFactory_SPLIT_NODES_TYPE_HBRANCH){
      res_split_1 = Cudd_SubsetHeavyBranch(manager,aux_bdd, num_rel_variables, numNodes);
    }else if(split_type == net_sf_javabdd_CUDDFactory_SPLIT_NODES_TYPE_SPATH){
      res_split_1 = Cudd_SubsetShortPaths(manager,aux_bdd, num_rel_variables, numNodes, 0);
    }
    Cudd_Ref(res_split_1);
    res_split_2 = Cudd_bddAnd(manager,aux_bdd, Cudd_Not(res_split_1));
    Cudd_Ref(res_split_2);

    printf("Num nodes %d And Part 1: %d, Part 2: %d\n", numNodes, Cudd_DagSize(res_split_1), Cudd_DagSize(res_split_2));


    if(aux_bdd != d){
      Cudd_RecursiveDeref(manager, aux_bdd); //Dereference the auxiliary bdd
    }
    sp [numFoldsAux-1] = res_split_1;
    aux_bdd = res_split_2;
    numFoldsAux --;
  }
  
  sp[0] = aux_bdd;

 
  (*env)->ReleasePrimitiveArrayCritical(env, relevant_variables, rel_variables, JNI_ABORT);

  jlongArray result=(*env)->NewLongArray(env, numFolds);
  (*env)->SetLongArrayRegion(env, result, 0, numFolds, (jlong *)sp);
  free(sp);

  return result;
}

/*
 * Class:     net_sf_javabdd_CUDDFactory_CUDDBDD
 * Method:    split_mask
 * Signature: (IJ[I[I)[J
 */
JNIEXPORT jlongArray JNICALL Java_net_sf_javabdd_CUDDFactory_00024CUDDBDD_split_1mask
(JNIEnv * env, jclass cl, jint split_type, jlong node, jintArray relevant_variables, jintArray mask_variables){
  DdNode* res;
  DdNode* d = (DdNode*) (intptr_cast_type) node;

  int num_rel_variables = (*env)->GetArrayLength(env, relevant_variables);
  int * rel_variables = (int*)((*env)->GetPrimitiveArrayCritical(env, relevant_variables, NULL));
  DdNode ** node_rel_variables = (DdNode **) malloc (num_rel_variables*sizeof(DdNode*)); 
  int i;
  for(i = 0; i < num_rel_variables; i++){
    //    printf("%d ", rel_variables[i]);
    node_rel_variables [i] = Cudd_bddIthVar(manager, rel_variables[i]);
    Cudd_Ref(node_rel_variables[i]);
  }

  int mvars = (*env)->GetArrayLength(env, mask_variables);
  int * maskVars = (int*)((*env)->GetPrimitiveArrayCritical(env, mask_variables, NULL));
  DdNode ** nodeMaskVars = (DdNode **) malloc (mvars*sizeof(DdNode*)); 
  for(i = 0; i < mvars; i++){
    nodeMaskVars [i] = Cudd_bddIthVar(manager, maskVars[i]);
    Cudd_Ref(nodeMaskVars[i]);
  }


  int numFolds = 2;
  DdNode ** sp =  (DdNode **) malloc (numFolds*sizeof(DdNode*));

  DdNode * split1, *split2;

  if(split_type == net_sf_javabdd_CUDDFactory_SPLIT_MASK_TYPE_CUDD){
    split1 = Cudd_SubsetWithMaskVars(manager, d, node_rel_variables, num_rel_variables, nodeMaskVars, mvars);
  }
  Cudd_Ref(split1);

  split2 = Cudd_bddAnd(manager,d, Cudd_Not(split1));
  Cudd_Ref(split2);

  sp[0] = split1;
  sp[1] = split2;

  for(i = 0; i < num_rel_variables; i++){
    Cudd_RecursiveDeref(manager, node_rel_variables[i]);
  }
  free(node_rel_variables);

  for(i = 0; i < mvars; i++){
    Cudd_RecursiveDeref(manager, nodeMaskVars[i]);
  }
  free(nodeMaskVars);


  (*env)->ReleasePrimitiveArrayCritical(env, relevant_variables, rel_variables, JNI_ABORT);
  (*env)->ReleasePrimitiveArrayCritical(env, mask_variables, maskVars, JNI_ABORT);

  jlongArray result=(*env)->NewLongArray(env, numFolds);
  (*env)->SetLongArrayRegion(env, result, 0, numFolds, (jlong *)*sp);
  free(sp);

  return result;  

}

/*
 * Class:     net_sf_javabdd_CUDDFactory_CUDDBDD
 * Method:    split_2
 * Signature: (IJ[I)[J
 */
JNIEXPORT jlongArray JNICALL Java_net_sf_javabdd_CUDDFactory_00024CUDDBDD_split_12
(JNIEnv * env, jclass cl, jint split_type, jlong node, jintArray relevant_variables){
  DdNode* res;
  DdNode* d = (DdNode*) (intptr_cast_type) node;

  int num_rel_variables = (*env)->GetArrayLength(env, relevant_variables);
  int * rel_variables = (int*)((*env)->GetPrimitiveArrayCritical(env, relevant_variables, NULL));

  (*env)->ReleasePrimitiveArrayCritical(env, relevant_variables, rel_variables, JNI_ABORT);

  int numFolds = 2; 
  DdNode *** sp = (DdNode ***) malloc(sizeof(DdNode **));
  
  double sc = Cudd_CountMinterm(manager, d, num_rel_variables);

  if(split_type == net_sf_javabdd_CUDDFactory_SPLIT_TYPE_DISJDECOMP){
    numFolds = Cudd_bddVarDisjDecomp(manager, d, sp);
  }else if(split_type == net_sf_javabdd_CUDDFactory_SPLIT_TYPE_GENDISJDECOMP){
    numFolds = Cudd_bddGenDisjDecomp(manager, d, sp);
  }else if(split_type == net_sf_javabdd_CUDDFactory_SPLIT_TYPE_ITERDISJDECOMP){
    DdNode * aux;

    printf("TOTAL size %d states %f \n", Cudd_DagSize(d), Cudd_CountMinterm(manager, d, num_rel_variables));

    DdNode * node_var = Cudd_bddIthVar(manager, rel_variables[1]);
    Cudd_Ref(node_var);


    aux = Cudd_bddAnd(manager, d, node_var);
    Cudd_Ref(aux);
    printf("nt size %d states %f \n", Cudd_DagSize(aux), Cudd_CountMinterm(manager, aux, num_rel_variables));
    DdNode * node_true = Cudd_bddExistAbstract(manager, aux, node_var);
    Cudd_Ref(node_true);
    Cudd_RecursiveDeref(manager, aux);

    aux = Cudd_bddAnd(manager, d, Cudd_Not(node_var));
    Cudd_Ref(aux);
    printf("nf size %d states %f \n", Cudd_DagSize(aux), Cudd_CountMinterm(manager, aux, num_rel_variables));
    DdNode * node_false = Cudd_bddExistAbstract(manager, aux, node_var);
    Cudd_Ref(node_false);
    Cudd_RecursiveDeref(manager, aux);

    //aux = Cudd_bddAndAbstract(manager, node_true, node_false, node_var);
    aux = Cudd_bddAnd(manager, node_true, node_false);
    Cudd_Ref(aux);

    printf("AUX size %d states %f \n", Cudd_DagSize(aux), Cudd_CountMinterm(manager, aux, num_rel_variables));

    DdNode * node_both = Cudd_bddAnd(manager, aux, d);
    Cudd_Ref(node_both); 
    Cudd_RecursiveDeref(manager, aux);
    
    aux = Cudd_bddAnd(manager, node_true, Cudd_Not(node_both));
    Cudd_Ref(aux);
    DdNode * node_true_restricted = Cudd_bddAnd(manager, aux, d);
    Cudd_Ref(node_true_restricted);
    Cudd_RecursiveDeref(manager, aux);

    aux = Cudd_bddAnd(manager, node_false, Cudd_Not(node_both));
    Cudd_Ref(aux);
    DdNode * node_false_restricted = Cudd_bddAnd(manager, aux, d);
    Cudd_Ref(node_false_restricted);
    Cudd_RecursiveDeref(manager, aux);



    printf("true size %d states %f false size %d states %f \n ",
	   Cudd_DagSize(node_true), Cudd_CountMinterm(manager, node_true, num_rel_variables),
	   Cudd_DagSize(node_false), Cudd_CountMinterm(manager, node_false, num_rel_variables));

    Cudd_RecursiveDeref(manager, node_true);
    Cudd_RecursiveDeref(manager, node_false);
    Cudd_RecursiveDeref(manager, node_var);


    numFolds = 3;

    *sp = (DdNode **)malloc(sizeof(DdNode *)*3);

    (*sp)[0] = node_false_restricted;
    (*sp)[1] = node_true_restricted;
    (*sp)[2] = node_both;
    
    int varcount = num_rel_variables;
    printf("SPfalse size %d states %f SPtrue size %d states %f SPboth size %d states %f\n ",
	   Cudd_DagSize(node_false_restricted), Cudd_CountMinterm(manager, node_false_restricted, varcount),
	   Cudd_DagSize(node_true_restricted), Cudd_CountMinterm(manager, node_true_restricted, varcount),
	   Cudd_DagSize(node_both), Cudd_CountMinterm(manager, node_both, varcount));


  }/*else if(split_type == net_sf_javabdd_CUDDFactory_SPLIT_TYPE_ITERDISJDECOMP){
     numFolds = Cudd_bddIterDisjDecomp(manager, d, sp);
     }*/

  jlongArray result=(*env)->NewLongArray(env, numFolds);
  (*env)->SetLongArrayRegion(env, result, 0, numFolds, (jlong *)*sp);
  free(*sp);

  return result;  

}





/*
 * Class:     net_sf_javabdd_CUDDFactory_CUDDBDD
 * Method:    split_nodes
 * Signature: (IJJ[I)[J
 */
/*JNIEXPORT jlongArray JNICALL Java_net_sf_javabdd_CUDDFactory_00024CUDDBDD_split_1nodes
  (JNIEnv * env, jclass cl, jint split_type, jlong nodes_thresh, jlong node, jintArray relevant_variables){
  DdNode* res;
  long nunmNodes = nodes;
  DdNode * original_bdd = (DdNode*) (intptr_cast_type) node;
  DdNode* aux_bdd = original_bdd;
  DdNode * res_split_1, * res_split_2; 



  int num_rel_variables = (*env)->GetArrayLength(env, relevant_variables);
  int * rel_variables = (int*)((*env)->GetPrimitiveArrayCritical(env, relevant_variables, NULL));


  (*env)->ReleasePrimitiveArrayCritical(env, relevant_variables, rel_variables, JNI_ABORT);
  int varcount = num_rel_variables;
  double sc = Cudd_CountMinterm(manager, original_bdd, varcount);


  //  printf("Entering in uniform split states sc %lf max_states %ld\n", sc, numStates);

  int numFolds = ((long)sc + numStates -1)/numStates;

  if(numFolds <= 0) numFolds = 1;
  int numFoldsAux = numFolds;
  DdNode ** sp = (DdNode **) malloc (numFolds*sizeof(DdNode*));
  
  while(numFoldsAux > 1){
  //    printf ("num folds %d  num nodes %d sat count %ld\n", numFoldsAux, Cudd_DagSize(aux_bdd) -1, (long)Cudd_CountMinterm(manager, aux_bdd, varcount));
  res_split_1 = Cudd_SubsetCompress(manager,aux_bdd, num_rel_variables, numStates);
  Cudd_Ref(res_split_1);

    
  //    printf("Res split 1 num nodes %d  sat count %ld\n", Cudd_DagSize(res_split_1) -1, (long)Cudd_CountMinterm(manager, res_split_1, varcount));

  res_split_2 = Cudd_bddAnd(manager,aux_bdd, Cudd_Not(res_split_1));
  Cudd_Ref(res_split_2);
  //    printf("Res split 2 num nodes %d sat count %ld\n", Cudd_DagSize(res_split_2) -1, (long)Cudd_CountMinterm(manager, res_split_2, varcount));


  if(aux_bdd != original_bdd){
  //      printf("NOT CALLED \n");
  Cudd_RecursiveDeref(manager, aux_bdd); //Dereference the auxiliary bdd
  }
  sp [numFoldsAux-1] = res_split_1;
  aux_bdd = res_split_2;
  numFoldsAux --;
  }
  //  printf("Finished partitioning\n");
  
  sp[0] = aux_bdd;
  
  jlongArray result=(*env)->NewLongArray(env, numFolds);
  (*env)->SetLongArrayRegion(env, result, 0, numFolds, (jlong *)sp);
  free(sp);


  return result;
  }


  /*
  * Class:     net_sf_javabdd_CUDDFactory_CUDDBDD
  * Method:    uniform_split_states0
  * Signature: (JJ)[J
  */
/*JNIEXPORT jlongArray JNICALL Java_net_sf_javabdd_CUDDFactory_00024CUDDBDD_uniform_1split_1states0
  (JNIEnv * env, jclass cl, jlong node, jlong states, jintArray relevant_variables){
  //  if(1 > 0) return Java_net_sf_javabdd_CUDDFactory_00024CUDDBDD_uniform_1split_1states0123(env, cl, node, folds, relevant_variables);

  DdNode* res;
  long numStates = states;
  DdNode* d = (DdNode*) (intptr_cast_type) node;

  int num_rel_variables = (*env)->GetArrayLength(env, relevant_variables);
  int * rel_variables = (int*)((*env)->GetPrimitiveArrayCritical(env, relevant_variables, NULL));

  init_tree_levels_variables(manager, &levs, num_rel_variables, rel_variables);
  long sc = satCount(manager, &levs, satCountCache, d);
  (*env)->ReleasePrimitiveArrayCritical(env, relevant_variables, rel_variables, JNI_ABORT);

  //  printf("Entering in uniform split states sc %ld max_states %ld\n", sc, numStates);

  int numFolds = (sc + numStates -1)/numStates;
  if(numFolds <= 0) numFolds = 1;
  DdNode ** sp = (DdNode **) malloc (numFolds*sizeof(DdNode*));
  
  if(numFolds > 1){
  //    printf("Sat count: %ld Start uniform split in %d folds\n", sc, numFolds);
  numFolds = split_uniform(manager, &levs,  satCountCache, d, numFolds, sp);
  //    printf("Splitted in %d folds\n", numFolds); 
  int i;
  for(i = 0; i < numFolds; i++) Cudd_Ref(sp[i]);
  //Cudd_RecursiveDeref(manager, d); //Dereference the big bdd
  }else{
  sp[0] = d;
  }

  jlongArray result=(*env)->NewLongArray(env, numFolds);
  (*env)->SetLongArrayRegion(env, result, 0, numFolds, (jlong *)sp);
  free(sp);

  return result;
  }

  /*
  * Class:     net_sf_javabdd_CUDDFactory_CUDDBDD
  * Method:    uniform_split_folds0
  * Signature: (JI)[J
  */
/*JNIEXPORT jlongArray JNICALL Java_net_sf_javabdd_CUDDFactory_00024CUDDBDD_uniform_1split_1folds0
  (JNIEnv * env, jclass cl, jlong node, jint folds, jintArray relevant_variables){
  //if(1 > 0)
  //  return Java_net_sf_javabdd_CUDDFactory_00024CUDDBDD_uniform_1split_1states0123(env, cl, node, folds, relevant_variables);
  DdNode* res;
  DdNode* d = (DdNode*) (intptr_cast_type) node;

  int num_rel_variables = (*env)->GetArrayLength(env, relevant_variables);
  int * rel_variables = (int*)((*env)->GetPrimitiveArrayCritical(env, relevant_variables, NULL));
  init_tree_levels_variables(manager, &levs, num_rel_variables, rel_variables);
  (*env)->ReleasePrimitiveArrayCritical(env, relevant_variables, rel_variables, JNI_ABORT);

  long sc = satCount(manager, &levs, satCountCache, d);

  int numFolds = folds;
  if(numFolds > sc) numFolds = sc;
  if(numFolds <= 0) numFolds = 1;
  DdNode ** sp = (DdNode **) malloc (numFolds*sizeof(DdNode*));
  
  if(numFolds > 1){
  //    printf("Sat count: %ld Start uniform split in %d folds\n", sc, numFolds);
  numFolds = split_uniform(manager, &levs,  satCountCache, d, numFolds, sp);
  //    printf("Splitted in %d folds\n", numFolds); 
  int i;
  for(i = 0; i < numFolds; i++) Cudd_Ref(sp[i]);
  //Cudd_RecursiveDeref(manager, d); //Dereference the big bdd
  }else{
  sp[0] = d;
  }
  jlongArray result=(*env)->NewLongArray(env, numFolds);
  (*env)->SetLongArrayRegion(env, result, 0, numFolds, (jlong *)sp);
  free(sp);

  return result;  
  }


  /*
  * Class:     net_sf_javabdd_CUDDFactory_CUDDBDD
  * Method:    uniform_split_states0
  * Signature: (JJ)[J
  */
/*JNIEXPORT jlongArray JNICALL Java_net_sf_javabdd_CUDDFactory_00024CUDDBDD_uniform_1split_1states0
  (JNIEnv * env, jclass cl, jlong node, jlong states, jintArray relevant_variables){
  DdNode* res;
  long numStates = states;
  DdNode * original_bdd = (DdNode*) (intptr_cast_type) node;
  DdNode* aux_bdd = original_bdd;
  DdNode * res_split_1, * res_split_2; 



  int num_rel_variables = (*env)->GetArrayLength(env, relevant_variables);
  int * rel_variables = (int*)((*env)->GetPrimitiveArrayCritical(env, relevant_variables, NULL));
  int varcount = num_rel_variables;
  double sc = Cudd_CountMinterm(manager, original_bdd, varcount);


  DdNode ** node_rel_variables = (DdNode **) malloc (num_rel_variables*sizeof(DdNode*)); 
  int i;
  for(i = 0; i < num_rel_variables; i++){
  //    printf("%d ", rel_variables[i]);
  node_rel_variables [i] = Cudd_bddIthVar(manager, rel_variables[i]);
  Cudd_Ref(node_rel_variables[i]);
  }
  //  printf("\n");
  (*env)->ReleasePrimitiveArrayCritical(env, relevant_variables, rel_variables, JNI_ABORT);


  //  printf("Entering in uniform split states sc %lf max_states %ld\n", sc, numStates);

  int numFolds = ((long)sc + numStates -1)/numStates;

  if(numFolds <= 0) numFolds = 1;
  int numFoldsAux = numFolds;
  DdNode ** sp = (DdNode **) malloc (numFolds*sizeof(DdNode*));
  
  while(numFoldsAux > 1){
  //    printf ("num folds %d  num nodes %d sat count %ld\n", numFoldsAux, Cudd_DagSize(aux_bdd) -1, (long)Cudd_CountMinterm(manager, aux_bdd, varcount));
  res_split_1 = Cudd_SplitSet(manager,aux_bdd, node_rel_variables,
  num_rel_variables, numStates);
  Cudd_Ref(res_split_1);

    
  //    printf("Res split 1 num nodes %d  sat count %ld\n", Cudd_DagSize(res_split_1) -1, (long)Cudd_CountMinterm(manager, res_split_1, varcount));

  res_split_2 = Cudd_bddAnd(manager,aux_bdd, Cudd_Not(res_split_1));
  Cudd_Ref(res_split_2);
  //    printf("Res split 2 num nodes %d sat count %ld\n", Cudd_DagSize(res_split_2) -1, (long)Cudd_CountMinterm(manager, res_split_2, varcount));


  if(aux_bdd != original_bdd){
  //      printf("NOT CALLED \n");
  Cudd_RecursiveDeref(manager, aux_bdd); //Dereference the auxiliary bdd
  }
  sp [numFoldsAux-1] = res_split_1;
  aux_bdd = res_split_2;
  numFoldsAux --;
  }
  //  printf("Finished partitioning\n");
  
  sp[0] = aux_bdd;
  
  jlongArray result=(*env)->NewLongArray(env, numFolds);
  (*env)->SetLongArrayRegion(env, result, 0, numFolds, (jlong *)sp);
  free(sp);
  free(node_rel_variables);

  return result;
  }*/


/*JNIEXPORT jlongArray JNICALL Java_net_sf_javabdd_CUDDFactory_00024CUDDBDD_uniform_1split_1states0
  (JNIEnv * env, jclass cl, jlong node, jlong states, jintArray relevant_variables){
  DdNode* res;
  long numStates = states;
  DdNode * original_bdd = (DdNode*) (intptr_cast_type) node;
  DdNode* aux_bdd = original_bdd;
  DdNode * res_split_1, * res_split_2; 



  int num_rel_variables = (*env)->GetArrayLength(env, relevant_variables);
  int * rel_variables = (int*)((*env)->GetPrimitiveArrayCritical(env, relevant_variables, NULL));


  (*env)->ReleasePrimitiveArrayCritical(env, relevant_variables, rel_variables, JNI_ABORT);
  int varcount = num_rel_variables;
  double sc = Cudd_CountMinterm(manager, original_bdd, varcount);


  //  printf("Entering in uniform split states sc %lf max_states %ld\n", sc, numStates);

  int numFolds = ((long)sc + numStates -1)/numStates;

  if(numFolds <= 0) numFolds = 1;
  int numFoldsAux = numFolds;
  DdNode ** sp = (DdNode **) malloc (numFolds*sizeof(DdNode*));
  
  while(numFoldsAux > 1){
  //    printf ("num folds %d  num nodes %d sat count %ld\n", numFoldsAux, Cudd_DagSize(aux_bdd) -1, (long)Cudd_CountMinterm(manager, aux_bdd, varcount));
  res_split_1 = Cudd_SubsetCompress(manager,aux_bdd, num_rel_variables, numStates);
  Cudd_Ref(res_split_1);

    
  //    printf("Res split 1 num nodes %d  sat count %ld\n", Cudd_DagSize(res_split_1) -1, (long)Cudd_CountMinterm(manager, res_split_1, varcount));

  res_split_2 = Cudd_bddAnd(manager,aux_bdd, Cudd_Not(res_split_1));
  Cudd_Ref(res_split_2);
  //    printf("Res split 2 num nodes %d sat count %ld\n", Cudd_DagSize(res_split_2) -1, (long)Cudd_CountMinterm(manager, res_split_2, varcount));


  if(aux_bdd != original_bdd){
  //      printf("NOT CALLED \n");
  Cudd_RecursiveDeref(manager, aux_bdd); //Dereference the auxiliary bdd
  }
  sp [numFoldsAux-1] = res_split_1;
  aux_bdd = res_split_2;
  numFoldsAux --;
  }
  //  printf("Finished partitioning\n");
  
  sp[0] = aux_bdd;
  
  jlongArray result=(*env)->NewLongArray(env, numFolds);
  (*env)->SetLongArrayRegion(env, result, 0, numFolds, (jlong *)sp);
  free(sp);


  return result;
  }
*/


/*
 * Class:     net_sf_javabdd_CUDDFactory_CUDDBDD
 * Method:    nodeCount0
 * Signature: (J)I
 */
JNIEXPORT jint JNICALL Java_net_sf_javabdd_CUDDFactory_00024CUDDBDD_nodeCount0
(JNIEnv *env, jclass cl, jlong a) {
  DdNode* d;
  d = (DdNode*) (intptr_cast_type) a;
  return Cudd_DagSize(d)-1;
}


/*
 * Class:     net_sf_javabdd_CUDDFactory
 * Method:    nodeCount0
 * Signature: ([J)I
 */
JNIEXPORT jint JNICALL Java_net_sf_javabdd_CUDDFactory_nodeCount1
(JNIEnv * env, jclass cl, jlongArray bdds_array){
  int num_bdds = (*env)->GetArrayLength(env, bdds_array);
  DdNode ** bdds = readNodeArray(env, bdds_array, num_bdds);
    
  int count = Cudd_SharingSize(bdds, num_bdds);
    
  free(bdds);
  return count;
}


/*
 * Class:     net_sf_javabdd_CUDDFactory_CUDDBDD
 * Method:    pathCount0
 * Signature: (J)D
 */
JNIEXPORT jdouble JNICALL Java_net_sf_javabdd_CUDDFactory_00024CUDDBDD_pathCount0
(JNIEnv *env, jclass cl, jlong a) {
  DdNode* d;
  d = (DdNode*) (intptr_cast_type) a;
  return Cudd_CountPathsToNonZero(d);
}

/*
 * Class:     net_sf_javabdd_CUDDFactory_CUDDBDD
 * Method:    satCount0
 * Signature: (J)D
 */
JNIEXPORT jdouble JNICALL Java_net_sf_javabdd_CUDDFactory_00024CUDDBDD_satCount0
(JNIEnv *env, jclass cl, jlong a) {
  DdNode* d;
  int varcount = Cudd_ReadSize(manager);
  d = (DdNode*) (intptr_cast_type) a;
  return Cudd_CountMinterm(manager, d, varcount);
}

/*
 * Class:     net_sf_javabdd_CUDDFactory_CUDDBDD
 * Method:    addRef
 * Signature: (J)V
 */
JNIEXPORT void JNICALL Java_net_sf_javabdd_CUDDFactory_00024CUDDBDD_addRef
(JNIEnv *env, jclass cl, jlong a) {
  DdNode* d;
  d = (DdNode*) (intptr_cast_type) a;
  Cudd_Ref(d);
}

/*
 * Class:     net_sf_javabdd_CUDDFactory_CUDDBDD
 * Method:    delRef
 * Signature: (J)V
 */
JNIEXPORT void JNICALL Java_net_sf_javabdd_CUDDFactory_00024CUDDBDD_delRef
(JNIEnv *env, jclass cl, jlong a) {
  DdNode* d;
  if (manager == NULL) return;
  d = (DdNode*) (intptr_cast_type) a;
  if (d != INVALID_BDD)
    Cudd_IterDerefBdd(manager, d);
}

/*
 * Class:     net_sf_javabdd_CUDDFactory_CUDDBDD
 * Method:    veccompose0
 * Signature: (JJ)J
 */
JNIEXPORT jlong JNICALL Java_net_sf_javabdd_CUDDFactory_00024CUDDBDD_veccompose0
(JNIEnv *env, jclass cl, jlong a, jlong b) {
  DdNode* d;
  CuddPairing* e;
  DdNode* f;
  jlong result;
  d = (DdNode*) (intptr_cast_type) a;
  e = (CuddPairing*) (intptr_cast_type) b;
  f = Cudd_bddVectorCompose(manager, d, e->table);
  result = (jlong) (intptr_cast_type) f;
  return result;
}

/*
 * Class:     net_sf_javabdd_CUDDFactory_CUDDBDD
 * Method:    replace0
 * Signature: (JJ)J
 */
JNIEXPORT jlong JNICALL Java_net_sf_javabdd_CUDDFactory_00024CUDDBDD_replace0
(JNIEnv *env, jclass cl, jlong a, jlong b) {
  DdNode* d;
  CuddPairing* e;
  DdNode* f;
  jlong result;
  int n;
  int *arr;
  int varnum = Cudd_ReadSize(manager);
  arr = (int*) malloc(sizeof(int)*varnum);
  if (arr == NULL) return INVALID_BDD;
  d = (DdNode*) (intptr_cast_type) a;
  e = (CuddPairing*) (intptr_cast_type) b;
  for (n=0; n<varnum; ++n) {
    DdNode* node = e->table[n];
    int var = Cudd_Regular(node)->index;
    int level = var;
    //int level = Cudd_ReadPerm(manager, var);
    arr[n] = level;
  }
  f = Cudd_bddPermute(manager, d, arr);
  free(arr);
  result = (jlong) (intptr_cast_type) f;
  return result;
}

/* class net_sf_javabdd_CUDDFactory_CUDDBDDPairing */

/*
 * Class:     net_sf_javabdd_CUDDFactory_CUDDBDDPairing
 * Method:    alloc
 * Signature: ()J
 */
JNIEXPORT jlong JNICALL Java_net_sf_javabdd_CUDDFactory_00024CUDDBDDPairing_alloc
(JNIEnv *env, jclass cl) {
  int n;
  int varnum = Cudd_ReadSize(manager);
  CuddPairing* r = (CuddPairing*) malloc(sizeof(CuddPairing));
  if (r == NULL) return 0;
  r->table = (DdNode**) malloc(sizeof(DdNode*)*varnum);
  if (r->table == NULL) {
    free(r);
    return 0;
  }
  for (n=0 ; n<varnum ; n++) {
    int var = n;
    //int var = Cudd_ReadInvPerm(manager, n); // level2var
    r->table[n] = Cudd_bddIthVar(manager, var);
    Cudd_Ref(r->table[n]);
  }
  r->next = pair_list;
  pair_list = r;
  return (jlong) (intptr_cast_type) r;
}

/*
 * Class:     net_sf_javabdd_CUDDFactory_CUDDBDDPairing
 * Method:    set0
 * Signature: (JII)V
 */
JNIEXPORT void JNICALL Java_net_sf_javabdd_CUDDFactory_00024CUDDBDDPairing_set0
(JNIEnv *env, jclass cl, jlong p, jint var, jint b) {
  CuddPairing* r = (CuddPairing*) (intptr_cast_type) p;
  int level1 = var;
  //int level1 = Cudd_ReadPerm(manager, var);
  //int level2 = Cudd_ReadPerm(manager, b);
  Cudd_RecursiveDeref(manager, r->table[level1]);
  r->table[level1] = Cudd_bddIthVar(manager, b);
  Cudd_Ref(r->table[level1]);
}

/*
 * Class:     net_sf_javabdd_CUDDFactory_CUDDBDDPairing
 * Method:    set2
 * Signature: (JIJ)V
 */
JNIEXPORT void JNICALL Java_net_sf_javabdd_CUDDFactory_00024CUDDBDDPairing_set2
(JNIEnv *env, jclass cl, jlong p, jint var, jlong b) {
  CuddPairing* r = (CuddPairing*) (intptr_cast_type) p;
  DdNode *d = (DdNode*) (intptr_cast_type) b;
  int level1 = var;
  //int level1 = Cudd_ReadPerm(manager, var);
  Cudd_RecursiveDeref(manager, r->table[level1]);
  r->table[level1] = d;
  Cudd_Ref(r->table[level1]);
}

/*
 * Class:     net_sf_javabdd_CUDDFactory_CUDDBDDPairing
 * Method:    reset0
 * Signature: (J)V
 */
JNIEXPORT void JNICALL Java_net_sf_javabdd_CUDDFactory_00024CUDDBDDPairing_reset0
(JNIEnv *env, jclass cl, jlong p) {
  int n;
  CuddPairing* r = (CuddPairing*) (intptr_cast_type) p;
  int varnum = Cudd_ReadSize(manager);
  for (n=0 ; n<varnum ; n++) {
    int var;
    Cudd_RecursiveDeref(manager, r->table[n]);
    var = n;
    //int var = Cudd_ReadInvPerm(manager, n); // level2var
    r->table[n] = Cudd_bddIthVar(manager, var);
    Cudd_Ref(r->table[n]);
  }
}

/*
 * Class:     net_sf_javabdd_CUDDFactory_CUDDBDDPairing
 * Method:    free0
 * Signature: (J)V
 */
JNIEXPORT void JNICALL Java_net_sf_javabdd_CUDDFactory_00024CUDDBDDPairing_free0
(JNIEnv *env, jclass cl, jlong p) {
  int n;
  CuddPairing* r = (CuddPairing*) (intptr_cast_type) p;
  CuddPairing** ptr;
  int varnum = Cudd_ReadSize(manager);
  for (n=0 ; n<varnum ; n++) {
    Cudd_RecursiveDeref(manager, r->table[n]);
  }
  ptr = &pair_list;
  while (*ptr != r)
    ptr = &(*ptr)->next;
  *ptr = r->next;
  free(r->table);
  free(r);
}
