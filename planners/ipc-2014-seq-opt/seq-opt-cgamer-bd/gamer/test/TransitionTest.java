import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import junit.framework.Assert;
import net.sf.javabdd.BDD;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import pddl2bdd.parser.gamer.GroundedPDDLParser;
import pddl2bdd.varOrder.VarOrderingOpt;
import pddl2bdd.varOrder.VariableOrdering;
import pddl2bdd.problem.VariablePartitioning;
import pddl2bdd.transition.TManagerBuilder;
import pddl2bdd.transition.TransitionManager;
import org.junit.Test;
import pddl2bdd.deprecated.TListBuilder;
import pddl2bdd.problem.Problem;
import pddl2bdd.transition.TTreeBuilder;  
import pddl2bdd.util.Log;

/**
 *
  * @author Peter Kissmann and Alvaro Torralba
 */
public class TransitionTest {

    VariablePartitioning variables;
    TManagerBuilder tBuilderTree, tBuilderList;
    TransitionManager tListFw, tListBw, tTreeFw, tTreeBw;

    static Problem problem;
    
    BDD init, goal;

    public TransitionTest() {
        VariableOrdering varOrder = new VarOrderingOpt();
        variables = new VariablePartitioning(problem, null);
        String ttParams = "bdd no_tree";
        ArrayList<String> paramsTT = new ArrayList<String>(Arrays.asList(ttParams.split(" ")));
        

        tBuilderList = new TListBuilder(problem,
                variables, new ArrayList<String>());
        tBuilderList.init(problem);
        tBuilderTree = new TTreeBuilder(problem,
                variables, paramsTT, TTreeBuilder.EdeleteType.NONE);
        tBuilderTree.init(problem);
        this.tListFw = tBuilderList.getTransitionManager(true);
        this.tListBw = tBuilderList.getTransitionManager(false);

        this.tTreeFw = tBuilderTree.getTransitionManager(true);
        this.tTreeBw = tBuilderTree.getTransitionManager(false);

        this.init = variables.createInitialState(problem, new ArrayList<Integer>());
        BDD trueGoal = variables.createGoal(problem, new ArrayList<Integer>());
        this.goal = trueGoal.replace(variables.getSp2s());
        trueGoal.free();
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
        String library = "cudd";
        problem = GroundedPDDLParser.parse("orig-1nPar.gdl");
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {

        this.init.free();
        this.goal.free();
        
        this.tListBw.free();
        this.tListFw.free();
        this.tTreeBw.free();
        this.tTreeFw.free();

        this.tBuilderList.cleanup();
        this.tBuilderTree.cleanup();

        this.variables.cleanup();
    }

   /* @Test
    public void testDisjunction(){
        ArrayList<BDD> bddsFw = new ArrayList<BDD>();
        
        ArrayList<String> opsList = new ArrayList<String>();
        this.tListFw.applicableOperators(this.init, opsList);
        for(String op : opsList){
            bddsFw.add(tListFw.applyAction(init, op));
        }

        BDD[] array = bddsFw.toArray(new BDD[bddsFw.size()]);
        BDD resIte = TransitionTree.merge_disj_ite(array, Long.MAX_VALUE);
        Log.getLog().log("Res Ite: " + resIte);

        bddsFw.clear();
        for(String op : opsList){
            bddsFw.add(tListFw.applyAction(init, op));
        }
        array = bddsFw.toArray(new BDD[bddsFw.size()]);
        BDD resAll = TransitionTree.merge_disj_all(array, Long.MAX_VALUE);
        Log.getLog().log("Res All: " + resAll);


        Assert.assertEquals(resIte, resAll);

    }*/

    /*@Test
    public void stepFW() {
        Log.getLog().log("Step FW");
        step(this.tListFw, this.tTreeFw, this.init);
    }*/
 
    @Test
    public void stepBW() {
        Log.getLog().log("Step BW");
        step(this.tListBw, this.tTreeBw, this.goal);
    }

    public void step(TransitionManager tList, TransitionManager tTree, BDD from){
        ArrayList<String> opsList = new ArrayList<String>();
        tList.applicableOperators(from, opsList);
        Log.getLog().log("Applicable operators list: " + opsList);
        
        ArrayList<String> opsTree = new ArrayList<String>();
        tTree.applicableOperators(from, opsTree);
        Log.getLog().log("Applicable operators tree: " + opsTree);

        Assert.assertEquals(opsList, opsTree);
        

        for(String op : opsTree){
            BDD bddList = tList.applyAction(from, op);
            Log.getLog().log("Applied action list: " + op);
            BDD bddTree = tTree.applyAction(from, op);
            Log.getLog().log("Applied action tree: " + op);

            Assert.assertEquals(bddList, bddTree);
            bddList.free();
            bddTree.free();
        }

        
         HashMap<Integer,BDD> resList = tList.testImage(from.id(), null,
                 System.currentTimeMillis() + 10000, 0);
            Log.getLog().log("Applied step list: " + resList);

         HashMap<Integer,BDD> resTree = tTree.testImage(from.id(), null,
                 Long.MAX_VALUE, 0);
         Log.getLog().log("Applied step tree: " + resTree);

//         Assert.assertEquals(resList.keySet(), resTree.keySet());
         Assert.assertEquals(resList, resTree);

         for(BDD bdd : resTree.values()){
             bdd.free();
         }
         for(BDD bdd : resList.values()){
             bdd.free();
         }

    }

}