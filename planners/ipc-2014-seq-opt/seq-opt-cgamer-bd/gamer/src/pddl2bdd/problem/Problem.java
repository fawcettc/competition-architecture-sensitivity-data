/*
 * Gamer, a tool for finding optimal plans
 * Copyright (C) 2007-2012 by Peter Kissmann
 * modified by Alvaro Torralba (2012-2013)
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */
package pddl2bdd.problem;

import pddl2bdd.parser.gamer.logic.Predicate;
import pddl2bdd.parser.gamer.logic.Expression;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import pddl2bdd.util.Log;

/**
 *
 * @author  Peter Kissmann and Alvaro Torralba
 */
public class Problem {

    private boolean isUniformCost;
    private List<Action> actions;
    private List<String> initialState;
    private Expression goalDescription;
    private List<List<String>> partitioning;
    private Set<String> partitioningNames;
    private Set<String> unreachableFacts;
    private Set<String> staticFacts;
    private Mutexes mutexFW, mutexBW;

    public Problem() {
        this.isUniformCost = false;
        this.actions = new ArrayList< Action>();
        this.initialState = new ArrayList< String>();
        this.partitioning = new ArrayList< List< String>>();
        this.partitioningNames = new HashSet<String>();
        this.mutexFW = new Mutexes(true);
        this.mutexBW = new Mutexes(false);
        this.unreachableFacts = new HashSet<String>();
        this.staticFacts = new HashSet<String>();
    }
    
    public void readGamerVariablesFile(File gamerFile){
        try {
            BufferedReader br = new BufferedReader(new FileReader(gamerFile));
            String line;
            while ((line = br.readLine()) != null) {
                line = line.replace("(", "").replace(")", "").trim();
                if(!line.equals("[]") && !line.startsWith("none-of-these") &&
                        !this.partitioningNames.contains(line)){
                    if(initialState.contains(line)){
                        this.staticFacts.add(line);
                    }else{
                        this.unreachableFacts.add(line);
                    }
                }
            }
            br.close();
        } catch (FileNotFoundException ex) {
            Log.getLog().log(Level.SEVERE, "ERROR: Static/Unreachable Facts FILE NOT FOUND: " + gamerFile.getAbsolutePath());
            System.exit(-1);
        } catch (IOException ex) {
            Log.getLog().log(Level.SEVERE, "ERROR: Static/Unreachable Facts FILE NOT READ");
            System.exit(-1);
        }
        
        Log.getLog().log(Level.INFO,"Static Facts: " + this.staticFacts);
        Log.getLog().log(Level.INFO,"Unreachable Facts: " + this.unreachableFacts);
        Log.getLog().log(Level.INFO,"Initial state: " + this.initialState);
    }
    
    /*public void readStaticFile(File factsFile, boolean reachable){
        try {
            BufferedReader br = new BufferedReader(new FileReader(factsFile));
            String line;
            while ((line = br.readLine()) != null) {
                line = line.trim();
                if(!line.equals("")){
                    if(reachable){
                        this.staticFacts.add(line);
                    }else{
                        this.unreachableFacts.add(line);
                    }
                }
            }
            br.close();
        } catch (FileNotFoundException ex) {
            Log.getLog().log(Level.SEVERE, "ERROR: Static/Unreachable Facts FILE NOT FOUND: " + factsFile.getAbsolutePath());
            System.exit(-1);
        } catch (IOException ex) {
            Log.getLog().log(Level.SEVERE, "ERROR: Static/Unreachable Facts FILE NOT READ");
            System.exit(-1);
        }
    }*/
    
    /*public void setFDVariables (File variablesFile){
        Log.getLog().log("Reading FD variables file");
        //1) Read file and put it into fdVariables
         Set<Set<String>> fdVariables = new HashSet<Set<String>>();
                   
        try {
            BufferedReader br = new BufferedReader(new FileReader(variablesFile));
            String line;
            Set<String> group = new HashSet<String>();
            while ((line = br.readLine()) != null) {
                line = line.trim();
                if(line.equals("")){
                    fdVariables.add(group);
                    group = new HashSet<String>();
                }else{
                    group.add(line);
                }
            }
            br.close();
        } catch (FileNotFoundException ex) {
            Log.getLog().log("ERROR: FD Variables FILE NOT FOUND");
            System.exit(-1);
        } catch (IOException ex) {
            Log.getLog().log("ERROR: FD Variables FILE NOT READ");
            System.exit(-1);
        }
        
        //2) Modify partitioning if necessary
        for(Set<String> var : partitioning){
        
        }
    }*/

    public void readMutexFile(File mutexFile, boolean useInvariants) {
        Log.getLog().log("Reading mutex file");
        //1) Read file
        try {
            BufferedReader br = new BufferedReader(new FileReader(mutexFile));
            Set<Set<String>> mutexGroupsFW = new HashSet<Set<String>>();
            Set<Set<String>> invariantGroupsFW = new HashSet<Set<String>>();
            Set<Set<String>> mutexGroupsBW = new HashSet<Set<String>>();
            Set<Set<String>> invariantGroupsBW = new HashSet<Set<String>>();
            String line;
            Set<String> group = null;
            while ((line = br.readLine()) != null) {
                line = line.trim();
                if(line.equals("")) continue;
                if (line.startsWith("begin_")) {
                    group = new HashSet<String>();
                } else if (line.startsWith("end_")) {
                    if(line.equals("end_variable_group")){
                        invariantGroupsFW.add(group);
                    }else if (line.equals("end_mutex_group_fw")){
                        mutexGroupsFW.add(group);
                    }else if (line.equals("end_mutex_group_bw")){
                        mutexGroupsBW.add(group);
                    }else if(line.equals("end_exactly_one_group_fw")){
                        invariantGroupsFW.add(group);
                    }else if(line.equals("end_exactly_one_group_bw")){
                        invariantGroupsBW.add(group);
                    }else{
                        Log.getLog().log(Level.SEVERE, "Error: unkwown invariant group?? " + line);
                        System.exit(-1);
                    }
                } else {
                    group.add(line);
                }
            }
            br.close();
            
            //Mutex and invariant groups from fw analysis go to prune bw search
            for(Set<String> mut : mutexGroupsFW){
                this.mutexBW.addMutex(mut);
            }
            for(Set<String> inv : invariantGroupsFW){
                this.mutexBW.addInvariant(inv);
            }
            
            //Mutex and invariant groups from bw analysis go to prune fw search
            for(Set<String> inv : invariantGroupsBW){
                this.mutexFW.addInvariant(inv);
            }
            for(Set<String> mut : mutexGroupsBW){
                this.mutexFW.addMutex(mut);
            }
        } catch (FileNotFoundException ex) {
            Log.getLog().log(Level.SEVERE, "ERROR: MUTEX FILE NOT FOUND: " + mutexFile.getAbsolutePath());
            System.exit(-1);
        } catch (IOException ex) {
            Log.getLog().log(Level.SEVERE, "ERROR: MUTEX FILE NOT READ");
            System.exit(-1);
        }
    }

    public int getVariable(String fluent) {
        for (int i = 0; i < this.partitioning.size(); i++) {
            if (this.partitioning.get(i).contains(fluent)) {
                return i;
            }
        }
        Log.getLog().log("Mutex fluent not found in partitioning: " + fluent);
        return -1;
    }

    /*public void addStaticFluents(File staticFluentsFile, boolean reachable) {
        ArrayList<String> stFluents = new ArrayList<String>();
        try {
            BufferedReader br = new BufferedReader(new FileReader(staticFluentsFile));
            String line;

            while ((line = br.readLine()) != null) {
                line = line.trim();
                if (line.equals("")) {
                    continue;
                }
                line = line.replace(" ", ".");
                stFluents.add(line);
            }
            br.close();
        } catch (IOException ex) {
            System.err.println("Error while reading static fluents file");
            ex.printStackTrace();
            System.exit(-1);
        }

        for (List<String> var : this.getPartitioning()) {
            for (int i = 0; i < var.size(); i++) {
                if (stFluents.contains(var.get(i))) {
                    String pre = var.remove(i);
                    i--;
                    if (reachable) {
                        this.staticFluents.add(pre);
                    } else {
                        this.notReachableFluents.add(pre);
                    }
                }
            }
        }
    }*/

    public void checkValidActions(File validActionsFile) {
        ArrayList<String> validActions = new ArrayList<String>();
        try {
            BufferedReader br = new BufferedReader(new FileReader(validActionsFile));
            String line;

            while ((line = br.readLine()) != null) {
                line = line.trim();
                if (line.equals("")) {
                    continue;
                }
                line = line.replace(" ", ".");
                validActions.add(line);
            }
            br.close();
        } catch (IOException ex) {
            Log.getLog().log(Level.SEVERE, "Error while reading valid actions file");
            ex.printStackTrace();
            System.exit(-1);
        }

        ArrayList<Action> removedActions = new ArrayList<Action>();
        for (Action acc : this.actions) {
            if (!validActions.contains(acc.getName())) {
                removedActions.add(acc);
            }
        }

        this.actions.removeAll(removedActions);
    }

    public List<Action> getActions() {
        return actions;
    }

    public Expression getGoalDescription() {
        return goalDescription;
    }

    public List<String> getInitialState() {
        return initialState;
    }

    public boolean isIsUniformCost() {
        return isUniformCost;
    }

    public Mutexes getMutexFW() {
        return mutexFW;
    }
    
    public Mutexes getMutexBW() {
        return mutexBW;
    }

    public List<List<String>> getPartitioning() {
        return partitioning;
    }

    public boolean isUniformCost() {
        return this.isUniformCost;
    }

    public void setUniformCost() {
        this.isUniformCost = false;
        if (actions.isEmpty()) {
            // should only happen if goalBDD file is present, so that a PDB has been generated
            return;
        }
        int lastFoundCost = - 1;
        for (Action acc : actions) {
            int cost = acc.getCost();
            if (lastFoundCost == - 1) {
                lastFoundCost = cost;
            } else {
                if (lastFoundCost != cost) {
                    return;
                }
            }
        }
        // if no costs present or all uniform, set costs to 1, in case of A* search
        for (Action acc : actions) {
            acc.setCost(1);
        }
        this.isUniformCost = true;
    }

    public void cleanup() {
        actions.clear();
        actions = null;
        initialState.clear();
        initialState = null;
        goalDescription = null;
        partitioning.clear();
        partitioning = null;
    }

    public void setUniformCost(boolean uniformCost) {
        this.isUniformCost = uniformCost;
    }

    public void addAction(Action action) {
        this.actions.add(action);
    }

    public void addInitialStatePred(Predicate pred) {
        this.initialState.add(pred.getName());
    }

    public void setGoalDescription(Expression expr) {
        this.goalDescription = expr;
    }

    public void addPartitioningGroup(ArrayList<Predicate> group) {
        if (group.size() <= 1) {
            Log.getLog().log(Level.SEVERE, "Partitioning error, variable with less than 2 values: " + group);
            System.exit(-1);
        }
        
        ArrayList<String> partition = new ArrayList<String>();
        for (Predicate predicate : group) {
            String predicateName = predicate.getName();
            partition.add(predicateName);
            this.partitioningNames.add(predicateName);
        }
        this.partitioning.add(partition);
        //System.out.println("Read: " + partition);
    }

    public Set<String> getUnreachableFacts() {
        return unreachableFacts;
    }

    public Set<String> getStaticFacts() {
        return staticFacts;
    }
    
}
