/*
 * Gamer, a tool for finding optimal plans
 * Copyright (C) 2007-2012 by Peter Kissmann
 * modified by Alvaro Torralba (2012-2013)
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */
package pddl2bdd.problem;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import net.sf.javabdd.BDD;
import net.sf.javabdd.BDDVarSet;
import pddl2bdd.util.Log;

/**
 * Represents an NAryVariable.
 *
 * @author Peter Kissmann and Alvaro Torralba
 */
public class NAryVariable {
    private VariablePartitioning variables;

    private int variable, value;
    
    private String name;
    private BDD preBDD;
    private BDD preBDDNegated;
    private BDDVarSet preVars;

    private BDD effBDDNegated;
    private BDD effBDD;
    private BDDVarSet effVars;

    private boolean [] varsRequired;
    private boolean [] varsRequiredNot;

    private Set <Integer> prePositiveVars, preNegativeVars;
    private Set <Integer> effPositiveVars, effNegativeVars;
    
    /**
     * Variables for doing the abstract exist
     */
    //BDDVarSet cube;
   // BDDVarSet cubep;
    
    public NAryVariable(VariablePartitioning variables, String name, int currentVariable,
            int numberOfVars, int value) {
        BDD tmp;
        this.variables = variables;
        this.name = name;
        //Set number of varianble and value
        this.variable = 0;
        for(List<String> partition : variables.getPartitionedVariables()){
            this.value = partition.indexOf(this.name);
            if(this.value >= 0){
                break;
            }
            this.variable++;
        }
        this.prePositiveVars = new HashSet<Integer>();
        this.preNegativeVars = new HashSet<Integer>();
        this.effPositiveVars = new HashSet<Integer>();
        this.effNegativeVars = new HashSet<Integer>();

        this.varsRequired = new boolean [variables.numBinaryVariables()];
        this.varsRequiredNot = new boolean [variables.numBinaryVariables()];
        Arrays.fill(varsRequired, false);
        Arrays.fill(varsRequiredNot, false);

        BDD[][] partVariables = getVariables(currentVariable,
                    numberOfVars, value, variables.getVariables(),
                    variables.getNotVariables());

        if (numberOfVars > 1) {
            this.preBDD = variables.getFactory().one();
            this.effBDD = variables.getFactory().one();
            for (int k = 0; k < numberOfVars; k++) {
                tmp = this.preBDD;
                this.preBDD = tmp.and(partVariables[0][k]);
                if (!tmp.equals(variables.getOneBDD())) {
                    tmp.free();
                }
                tmp = this.effBDD;
                this.effBDD = tmp.and(partVariables[1][k]);
                if (!tmp.equals(variables.getOneBDD())) {
                    tmp.free();
                }
            }
        } else {
            this.preBDD = partVariables[0][0].id();
            this.effBDD = partVariables[1][0].id();
        }
        this.preBDDNegated = this.preBDD.not();
        this.effBDDNegated = this.effBDD.not();


        int [] indexPreVars = new int [numberOfVars];
        int [] indexEffVars = new int [numberOfVars];
        for(int i = 0; i < numberOfVars; i++){
            indexPreVars[i] = (currentVariable + i)*2;
            indexEffVars[i] = (currentVariable + i)*2 +1;
        }
        this.preVars = variables.getFactory().makeSet(indexPreVars);
        this.effVars = variables.getFactory().makeSet(indexEffVars);
    }


     private BDD[][] getVariables(int startingVariable, int numberOfVars,
            int value, BDD [] variables, BDD []not_variables) {
        int remainingValue = value;
        BDD[][] returnVariables = new BDD[2][numberOfVars];
        for (int i = 0; i < numberOfVars; i++) {
            int varPre = (startingVariable + i) * 2;
            int varEff = (startingVariable + i) * 2 + 1;

            if (remainingValue >= Math.pow(2, numberOfVars - i - 1)) {
                returnVariables[0][i] = variables[varPre];
                returnVariables[1][i] = variables[varEff];
                remainingValue = remainingValue
                        - (int) Math.pow(2, numberOfVars - i - 1);
                this.prePositiveVars.add(varPre);
                this.effPositiveVars.add(varEff);
                this.varsRequired [varPre] = true;
                this.varsRequired [varEff] = true;
            } else {
                returnVariables[0][i] = not_variables[varPre];
                returnVariables[1][i] = not_variables[varEff];
                this.preNegativeVars.add(varPre);
                this.effNegativeVars.add(varEff);
                this.varsRequiredNot [varPre] = true;
                this.varsRequiredNot [varEff] = true;
            }
        }
        if (remainingValue != 0) {
            Log.getLog().log(Level.SEVERE, "ERROR: n-ary variable could not be created using binary variables!");
            System.exit(1);
        }
        return returnVariables;
    }

     public void free(){
         if(this.preBDD != null){
             preBDD.free();
             preBDDNegated.free();
         }
         if (this.effBDD != null) {
             effBDD.free();
             effBDDNegated.free();
         }

         this.preVars.free();
         this.effVars.free();
         
         //cube.free();
        // cubep.free();
     }

    public String getName() {
        return name;
    }

    public BDD getPreBDD() {
        return preBDD;
    }

    public BDD getEffBDD() {
        return effBDD;
    }

    public BDD getPreBDDNegated() {
        return preBDDNegated;
    }

    public BDD getEffBDDNegated() {
        return effBDDNegated;
    }

  /*  public BDDVarSet getCube() {
        return cube;
    }

    public BDDVarSet getCubep() {
        return cubep;
    }*/


    /**
     * if !neg returns true in case the NAry variable requires var to be true
     * if neg returns true in case the NAry variable requires var to be false
     *
     * @param var bdd variable that is checked.
     * @param neg if it is requiered to be true or false
     * @return true if the variable is required to have a particular value.
     */
    public boolean hasPrecondition(int var, boolean neg) {
        if(neg){
            return this.varsRequiredNot [var];
        }else{
            return this.varsRequired [var];
        }
    }

    public int getVariable() {
        return variable;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return this.name;
    }

    public ArrayList<NAryVariable> getOthers() {
        return this.variables.getOthers(this.variable, this.name);
    }


    public BDDVarSet getPreVars(){
        return this.preVars;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final NAryVariable other = (NAryVariable) obj;
        if (this.variable != other.variable) {
            return false;
        }
        if (this.value != other.value) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + this.variable;
        hash = 79 * hash + this.value;
        return hash;
    }

    public BDDVarSet getEffVars() {
        return this.effVars;
    }

    public Set<Integer> getPreNegativeVars() {
        return preNegativeVars;
    }

    public Set<Integer> getPrePositiveVars() {
        return prePositiveVars;
    }    
}