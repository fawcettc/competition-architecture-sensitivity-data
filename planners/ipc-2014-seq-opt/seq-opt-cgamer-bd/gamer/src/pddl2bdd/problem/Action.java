/*
 * Gamer, a tool for finding optimal plans
 * Copyright (C) 2007-2012 by Peter Kissmann
 * modified by Alvaro Torralba (2012-2013)
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */
package pddl2bdd.problem;

import java.util.ArrayList;
import java.util.List;
import pddl2bdd.transition.Transition;



public abstract class Action {
    
    protected String name;
    protected int cost;
    protected boolean unused;
    
    public Action() {
        unused = false;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public int getCost() {
        return cost;
    }
    
     public void setUnused(boolean unused) {
        this.unused = unused;
    }

    public boolean getUnused() {
        return unused;
    }
    
    public abstract ArrayList<Transition> getTransitions(VariablePartitioning variables,
            List<Integer> emptyPartitions);

    public abstract void effectVariables(List<Integer> effectIndices,
            List<String> allVars, List<Integer> partitionIndices);

    public abstract void preconditionVariables(List<Integer> effectIndices,
            List<String> allVars, List<Integer> partitionIndices);
    

}
