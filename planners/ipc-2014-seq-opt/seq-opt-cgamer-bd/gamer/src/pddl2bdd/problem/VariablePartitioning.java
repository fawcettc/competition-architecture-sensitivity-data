/*
 * Gamer, a tool for finding optimal plans
 * Copyright (C) 2007-2012 by Peter Kissmann
 * modified by Alvaro Torralba (2012-2013)
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */
package pddl2bdd.problem;

import pddl2bdd.varOrder.VariableOrdering;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import net.sf.javabdd.BDD;
import net.sf.javabdd.BDDFactory;
import net.sf.javabdd.BDDPairing;
import net.sf.javabdd.BDDVarSet;
import pddl2bdd.Parameters;
import pddl2bdd.transition.Assign;
import pddl2bdd.util.Log;
import pddl2bdd.util.Maths;

/**
 * All the information about VariablePartitioning and VariableOrdering
 *
 * @author Peter Kissmann and Alvaro Torralba
 */
public class VariablePartitioning {

    private BDDFactory factory;
    private BDD zeroBDD, oneBDD; //BDDs for 0 and 1
    private List<BDDVarSet> cubeVar, cubeVarp;
    private BDDVarSet cube, cubep; // cube of S and S'
    private BDD cubeBDD, cubepBDD; // BDDs for cube of S and S'
    private BDDPairing s2sp, sp2s; // permutation for S -> S' and S' -> S
    // partition of variables as given by the user
    private List<List<String>> partitionedVariables;
    private Map<String, NAryVariable> nAryVariablesByName;
    private List<NAryVariable> nAryVariables;
    /*private List<String> nAryVariables; // list of all n-ary variables
     // bdds representing the n-ary variables for the current and next state
     private List<BDD> nAryVariablesPreBDDs, nAryVariablesEffBDDs;*/
    private int numberOfVariables; // number of boolean variables
    private BDD[] variables, not_variables; // BDD variables and its negation
    private BDD[] S, Sp; // S (current state) and S' variables (next state)
    private Mutexes mutexFW, mutexBW;

    //Facts that cannot be reached (to read the actions)
    private Set<String> unreachableFacts;
    //Facts that are always true (to read the actions)
    private Set<String> staticFacts;

    public VariablePartitioning(Problem problem,
            Parameters params) {
        this.numberOfVariables = 0;

        for (List<String> group : problem.getPartitioning()) {
            if (group.size() <= 1) {
                System.err.println("Partitioning error, variable with less than 2 values: " + group);
                System.exit(-1);
            }
            numberOfVariables += Maths.log2(group.size());
        }
        numberOfVariables *= 2;
        this.factory = BDDFactory.init("cudd", params.getCuddInitNodes(),
                    params.getCuddInitCacheSize(), numberOfVariables);
        factory.setVarNum(numberOfVariables);

        this.zeroBDD = factory.zero();
        this.oneBDD = factory.one();

        this.mutexFW = problem.getMutexFW();
        this.mutexBW = problem.getMutexBW();

        this.partitionedVariables = params.getVarOrder().getVariableOrdering(problem);
        this.unreachableFacts = problem.getUnreachableFacts();
        this.staticFacts = problem.getStaticFacts();
        // allocate BDD vars
        Log.getLog().log(Level.INFO, "   creating variables ..." + this.partitionedVariables.size() + " => " + this.numberOfVariables / 2);
        variables = new BDD[numberOfVariables];
        not_variables = new BDD[numberOfVariables];
        for (int i = 0; i < numberOfVariables; i++) {
            BDD variable = factory.ithVar(i);
            variables[i] = variable;
            not_variables[i] = variable.not();
        }

        // get current / next state variables
        S = new BDD[numberOfVariables / 2];
        Sp = new BDD[numberOfVariables / 2];
        int[] preVars = new int[numberOfVariables / 2];
        int[] effVars = new int[numberOfVariables / 2];
        for (int i = 0; i < numberOfVariables / 2; i++) {
            S[i] = variables[i * 2];
            preVars[i] = i * 2;
            Sp[i] = variables[i * 2 + 1];
            effVars[i] = i * 2 + 1;
        }

        // S and S' cube:
        cube = factory.makeSet(preVars);
        cubep = factory.makeSet(effVars);
        cubeBDD = cube.toBDD();
        cubepBDD = cubep.toBDD();

        // S -> S' permutation and S' -> S permutation
        s2sp = factory.makePair();
        sp2s = factory.makePair();
        for (int i = 0; i < numberOfVariables / 2; i++) {
            s2sp.set(i * 2, i * 2 + 1);
            sp2s.set(i * 2 + 1, i * 2);
        }

        createNAryVariables();
        Log.getLog().log("   done ...");
    }

    public void initMutex(int maxTimeMutexBDDs, int maxNodesMutexBDDs) {
        this.mutexFW.init(this, maxTimeMutexBDDs, maxNodesMutexBDDs);
        this.mutexBW.init(this, maxTimeMutexBDDs, maxNodesMutexBDDs);
    }

    private void createNAryVariables() {
        int currentVariable = 0;
        nAryVariables = new ArrayList<NAryVariable>();
        nAryVariablesByName = new HashMap<String, NAryVariable>();
        for (int i = 0; i < partitionedVariables.size(); i++) {
            int size = partitionedVariables.get(i).size();
            int numberOfVars = Maths.log2(size);
            for (int j = 0; j < size; j++) {
                NAryVariable var = new NAryVariable(this,
                        partitionedVariables.get(i).get(j),
                        currentVariable, numberOfVars, j);
                nAryVariables.add(var);
                nAryVariablesByName.put(var.getName(), var);
            }
            currentVariable += numberOfVars;
        }
    }

    public void cleanup() {
        cubeBDD.free();
        cubepBDD.free();
        cube.free();
        cubep.free();
        s2sp.reset();
        sp2s.reset();

        this.zeroBDD.free();
        this.oneBDD.free();

        for (NAryVariable nAryVariable : this.nAryVariables) {
            nAryVariable.free();
        }

        for (int i = 0; i < numberOfVariables; i++) {
            variables[i].free();
            not_variables[i].free();
        }

        this.mutexFW.cleanup();
        this.mutexBW.cleanup();
        
        this.factory.done();
    }

    public BDDFactory getFactory() {
        return factory;
    }

    public BDD getOneBDD() {
        return oneBDD;
    }

    public BDD[] getVariables() {
        return variables;
    }

    public BDD[] getNotVariables() {
        return not_variables;
    }

    public boolean isUnreachable(String name){
        return this.unreachableFacts.contains(name);
    }
    
    public boolean isStatic(String name){
        return this.staticFacts.contains(name);
    }
    
    public NAryVariable getNAryVariable(String name) {
        return nAryVariablesByName.get(name);
    }

    public NAryVariable getNoneOfThese(int indexVar) {
        for (String sp : partitionedVariables.get(indexVar)) {
            if (sp.startsWith("none-of-these")) {
                return nAryVariablesByName.get(sp);
            }
        }

        return null;
    }

    public int numSASVariables() {
        return this.partitionedVariables.size();
    }

    public int numBinaryVariables() {
        return this.numberOfVariables;
    }

    public List<String> getValues(int indexVar) {
        return this.partitionedVariables.get(indexVar);
    }

    public List<List<String>> getPartitionedVariables() {
        return partitionedVariables;
    }

    public BDD getBDDVar(int i) {
        return variables[i];
    }

    public BDD getBDDNotVar(int i) {
        return not_variables[i];
    }

    public List<NAryVariable> getnAryVariables() {
        return nAryVariables;
    }
    
/*    public BDDVarSet getCube(List<Integer> partitions) {
        ArrayList<Integer> bVars = new ArrayList<Integer>();
        for(int part : partitions){
            this.numberOfVariables
        }
        int[] binaryVars = new int[numberOfVariables / 2];
        for (int i = 0; i < numberOfVariables / 2; i++) {
            preVars[i] = i * 2;
        }
        return factory.makeSet(binaryVars);
    }*/

    public BDDVarSet getCube() {
        return this.cube;
    }

    public BDDVarSet getCubep() {
        return this.cubep;
    }

    public BDDPairing getS2sp() {
        return this.s2sp;
    }

    public BDDPairing getSp2s() {
        return this.sp2s;
    }

    public BDDPairing getPairing(BDDVarSet variables, boolean forward) {
        BDDPairing res = this.factory.makePair();

        for (int var : variables.toArray()) {
            int varp;
            if (var % 2 == 0) {
                varp = var + 1;
            } else {
                varp = var;
                var = varp - 1;
            }

            if (forward) {
                res.set(varp, var);
            } else {
                res.set(var, varp);
            }
        }
        return res;
    }

    public int numPredicates() {
        int res = 0;
        for (List<String> part : this.partitionedVariables) {
            res += part.size();
        }
        return res;
    }

    public boolean[] getUnusedVarIndices(List<Integer> emptyPartitions) {
        boolean unusedVarIndices[] = new boolean[numPredicates()];

        //TODO: This part of the code may be wrong
        int index = 0;
        int maxIndex = 0;
        for (int foundIndex : emptyPartitions) {
            while (index < foundIndex) {
                maxIndex += this.partitionedVariables.get(index).size();
                index++;
            }
            int size = this.partitionedVariables.get(index).size();
            for (int i = maxIndex; i < maxIndex + size; i++) {
                unusedVarIndices[i] = true;
            }
            maxIndex += size;
            index++;
        }

        return unusedVarIndices;

    }

    /**
     * Initialize initialState taking into account the empty partitions
     */
    public BDD createInitialState(Problem problem, List<Integer> emptyPartitions) {
        List<String> initialVariables = problem.getInitialState();
        BDD init = factory.one();

        for (int i = 0; i < this.partitionedVariables.size(); i++) {
            if (emptyPartitions.contains(i)) {
                continue;
            }
            List<String> partVar = this.partitionedVariables.get(i);
            NAryVariable nAryVar = null;
            for (String value : partVar) {
                if (initialVariables.contains(value)) {
                    nAryVar = this.getNAryVariable(value);
                    break;
                }
            }
            if (nAryVar == null) {
                for (String par : partVar) {
                    if (par.startsWith("none-of-these")) {
                        nAryVar = this.getNAryVariable(par);
                        break;
                    }
                }
            }

            if (nAryVar != null) {
                BDD tmp = init;
                init = nAryVar.getPreBDD().and(init);
                tmp.free();
            } else {
                Log.getLog().log(Level.WARNING, "Error: no variable of group " + i
                        + " though there is no \'none-of-these\'-variable!");
            }
        }
        return init;
    }

    public BDD createGoal(Problem problem, List<Integer> emptyPartitions) {
        BDD goal = problem.getGoalDescription().createBDD(this,
                false, emptyPartitions);
        return goal;
    }

    public BDD getZeroBDD() {
        return zeroBDD;
    }

    /**
     * Gets the list of assignments and returns a different list avoiding
     * negative effects that make no sense
     *
     * @param e
     * @param emptyPartitions
     * @return
     */
    public HashSet<NAryVariable> correctEffect(Set<Assign> e, List<Integer> emptyPartitions) {
        HashSet<NAryVariable> res = new HashSet<NAryVariable>();
        HashSet<Integer> assertedVariables = new HashSet<Integer>();
        HashSet<Integer> negatedVariables = new HashSet<Integer>();

        for (Assign eff : e) {
            if(eff.isUnreachable()){
                Log.getLog().log("Unreachable effect?");
                continue; //return new HashSet<NAryVariable>();
            }else if(eff.isStaticFact()){
                Log.getLog().log("Static effect?");
                continue; 
            }
            int var = eff.getNAryVariable().getVariable();
            if (emptyPartitions.contains(var)) {
                continue;
            }
            if (eff.isNegated()) {
                negatedVariables.add(var);
            } else {
                if (assertedVariables.contains(var)) {
                    Log.getLog().log(Level.SEVERE, "Error: same variable appearing twice in some operator");
                    System.exit(-1);
                }
                assertedVariables.add(var);
                res.add(eff.getNAryVariable());
            }
        }

        for (int negVar : negatedVariables) {
            if (!assertedVariables.contains(negVar)) {
                String nameNoneOfThese = null;
                for (String sp : this.partitionedVariables.get(negVar)) {
                    if (sp.startsWith("none-of-these")) {
                        nameNoneOfThese = sp;
                        break;
                    }
                }
                if (nameNoneOfThese == null) {
                    Log.getLog().log(Level.SEVERE, "Error: negated variable without positive effect nor none-of-these: " + this.partitionedVariables.get(negVar));
                    System.exit(-1);
                }
                res.add(this.getNAryVariable(nameNoneOfThese));
            }
        }
        return res;
    }

    /**
     * Returns all the NAryVariables for the specified variable, except value.
     *
     * @param variable
     * @param value
     * @return
     */
    public ArrayList<NAryVariable> getOthers(int variable, String value) {
        ArrayList<NAryVariable> res = new ArrayList<NAryVariable>();
        for (String name : this.partitionedVariables.get(variable)) {
            if (!name.equals(value)) {
                res.add(this.nAryVariablesByName.get(name));
            }
        }

        return res;
    }

    public Mutexes getMutex(boolean prune_fw) {
        if (prune_fw) {
            return mutexFW;
        } else {
            return mutexBW;
        }
    }

    public List<NAryVariable> getnAryVariables(int variable) {
        ArrayList<NAryVariable> res = new ArrayList<NAryVariable>();
        for (String name : this.partitionedVariables.get(variable)) {
            res.add(this.nAryVariablesByName.get(name));
        }
        return res;
    }
}
