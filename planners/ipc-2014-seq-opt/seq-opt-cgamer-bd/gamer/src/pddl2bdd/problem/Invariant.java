/*
 * Gamer, a tool for finding optimal plans
 * Copyright (C) 2007-2012 by Peter Kissmann
 * modified by Alvaro Torralba (2012-2013)
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */
package pddl2bdd.problem;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.logging.Level;
import net.sf.javabdd.BDD;
import pddl2bdd.util.Log;

/**
 *
 * @author Alvaro Torralba
 */
public class Invariant implements Comparable<Invariant> {

    private VariablePartitioning varsPart;
    private boolean isGroup; //If true, one element of the invariant must be true
    private ArrayList<MutexFluent> mutexFluents;
    private SortedSet<Integer> vars;

    @Override
    public String toString() {
        return (isGroup ? "INVARIANT: " : "MUTEX: ") + this.mutexFluents;
    }

    public Invariant(VariablePartitioning varsPart,
            Set<String> group, boolean isGroup) {
        this.varsPart = varsPart;
        this.isGroup = isGroup;

        //2) Generate BDD
        this.vars = new TreeSet<Integer>();

        this.mutexFluents = new ArrayList<MutexFluent>();
        for (String predicate : group) {
            MutexFluent f = this.parseMutex(predicate);
            mutexFluents.add(f);
            vars.addAll(f.getVariables());
        }
        if (this.vars.size() <= 1) {
            this.isGroup = false;
        }
    }

    public void getBDDsByFirstVar(List<Integer> emptyPartitions, Map<Integer, List<BDD>> res) {
        //Semantic. At most one can be true. 
        for (int i = 0; i < this.mutexFluents.size(); i++) {
            MutexFluent f1 = this.mutexFluents.get(i);
            if (f1.isRelevant(emptyPartitions)) {
                //Log.getLog().log("Skipping: " + f1);
                continue;
            }
            for (int j = i + 1; j < this.mutexFluents.size(); j++) {
                MutexFluent f2 = this.mutexFluents.get(j);
                if (f2.isRelevant(emptyPartitions)) {
                    //  Log.getLog().log("Skipping: " + f2);
                    continue;
                }
                if (f1.getVariables().equals(f2.getVariables())) {
                    //Log.getLog().log("Skipping because " + f1 + " and " + f2 + " refer to the same var");
                    continue;
                }
                BDD both = f1.getBDD().and(f2.getBDD());
                int var = Math.min(f1.getVariables().first(),
                        f2.getVariables().first());

                if (res.containsKey(var)) {
                    BDD tmp = res.get(var).get(0);
                    res.get(var).set(0, tmp.or(both));
                    tmp.free();
                    both.free();
                } else {
                    res.put(var, new ArrayList<BDD>());
                    res.get(var).add(both);
                }
            }
        }
        //Log.getLog().log("Mutex BDDs " + this + ": " + res.size());
    }

    public void getExactlyOneBDDsByFirstVar(List<Integer> emptyPartitions, Map<Integer, List<BDD>> res) {
        if (this.isGroup && !this.isRelevant(emptyPartitions)) {
            //At least one must be true.
            BDD allNot = varsPart.getOneBDD().id();

            int var = Integer.MAX_VALUE;
            for (MutexFluent f : this.mutexFluents) {
                //Log.getLog().log(f.toString());
                var = Math.min(var, f.getVariables().first());
                BDD tmp = allNot;
                BDD notV = f.getBDD().not();
                allNot = allNot.and(notV);
                notV.free();
                tmp.free();
            }

            if (!res.containsKey(var)) {
                res.put(var, new ArrayList<BDD>());
            }
            res.get(var).add(varsPart.getZeroBDD().id());
            res.get(var).add(allNot);
        }
    }

    public boolean valid() {
        return this.vars.size() > 1;
    }

    @Override
    public int compareTo(Invariant o) {
        return this.vars.first().compareTo(o.vars.first());
    }

    public int getFirstVar() {
        return vars.first();
    }

    public BDD getBDDMutex(NAryVariable fluent) {
        return getBDDMutex(fluent, new ArrayList<Integer>());
    }
    
    public BDD getBDDExactlyOne(NAryVariable fluent) {
        return getBDDExactlyOne(fluent, new ArrayList<Integer>());
    }

    public BDD getBDDMutex(NAryVariable fluent, List<Integer> emptyPartitions) {
        BDD res = this.varsPart.getZeroBDD().id();

        //If the invariant has nothing to do with the fluent
        if (this.isRelevant(fluent)) {
            List<MutexFluent> relevant = new ArrayList<MutexFluent>();
            List<MutexFluent> notRelevant = new ArrayList<MutexFluent>();
            for (MutexFluent inv : this.mutexFluents) {
                if (inv.isRelevant(emptyPartitions)) {
                    continue;
                }
                if (inv.isRelevant(fluent)) { //For the rest of elements in the invariant
                    relevant.add(inv);
                } else {
                    notRelevant.add(inv);
                }
            }
            for (MutexFluent relFluent : relevant) {
                for (MutexFluent nonRelFluent : notRelevant) {
                    BDD tmp = res;
                    BDD both = nonRelFluent.getBDD().and(relFluent.getBDD());
                    //Log.getLog().log(nonRelFluent +" is mutex with " + relFluent);
                    res = tmp.or(both);
                    tmp.free();
                    both.free();
                }
            }

            for (int i = 0; i < relevant.size(); i++) {
                BDD bddi = relevant.get(i).getBDD();
                for (int j = i + 1; j < relevant.size(); j++) {
                    BDD bddj = relevant.get(j).getBDD();
                    BDD tmp = res;
                    BDD both = bddi.and(bddj);
                    //Log.getLog().log(relevant.get(i) +" is mutex with " + relevant.get(j));
                    res = tmp.or(both);
                    tmp.free();
                    both.free();
                }
            }
        }
        
        return res;
    }
    
    
    public BDD getBDDExactlyOne(NAryVariable fluent, List<Integer> emptyPartitions) {
        BDD res = this.varsPart.getZeroBDD().id();

        if (this.isGroup && this.isRelevant(fluent) && !this.isRelevant(emptyPartitions)) { //At least one must be true.
            //Log.getLog().log("Invariant of " + this);
            BDD allNot = varsPart.getOneBDD().id();
            for (MutexFluent f : this.mutexFluents) {
                BDD tmp = allNot;
                BDD notV = f.getBDD().not();
                allNot = allNot.and(notV);
                notV.free();
                tmp.free();
            }

            BDD tmp = res;
            res = tmp.or(allNot);
            tmp.free();
            allNot.free();
        }

        //Log.getLog().log("Mutex BDD " + this + ": " + mutexBDD.nodeCount());
        //mutexBDD.printSet();
        return res;
    }

    private MutexFluent parseMutex(String predicate) {
        if (predicate.startsWith("NegatedAtom ")) {
            String[] list = new String[1];
            list[0] = predicate.replace("NegatedAtom ", "Atom ");
            NoneOfThose nf = new NoneOfThose(varsPart, list);
            MutexFluent f = nf.getRepresentativeFluent();
            if (f == null) {
                return nf;
            } else {
                Log.getLog().log(f + " is representative of " + nf);
                nf.cleanup();
                return f;
            }
        } else if (predicate.startsWith("<none of those>")) {
            String[] list = predicate.split(":")[1].split(";");

            NoneOfThose nf = new NoneOfThose(varsPart, list);
            MutexFluent f = nf.getRepresentativeFluent();
            if (f == null) {
                return nf;
            } else {
                Log.getLog().log(f + " is representative of " + nf);
                nf.cleanup();
                return f;
            }
        } else if (predicate.startsWith("Atom")) {
            return new Fluent(varsPart, predicate);
        } else {
            System.err.println("Error: " + predicate + " is not an Atom nor <none of those>");
            System.exit(-1);
        }

        return null;
    }

    public boolean isRelevant(NAryVariable fluent) {
        for (MutexFluent inv : this.mutexFluents) {
            if (inv.isRelevant(fluent)) {
                return true;
            }
        }
        return false;
    }

    public boolean isRelevant(List<Integer> variables) {
        for (MutexFluent inv : this.mutexFluents) {
            if (inv.isRelevant(variables)) {
                return true;
            }
        }
        return false;
    }

    public void cleanup() {
        for (MutexFluent f : this.mutexFluents) {
            f.cleanup();
        }
    }
}

abstract class MutexFluent {

    protected VariablePartitioning varsPart;
    protected BDD bdd;

    public MutexFluent(VariablePartitioning varsPart) {
        this.varsPart = varsPart;
    }

    public void cleanup() {
        if (this.bdd != null) {
            this.bdd.free();
            this.bdd = null;
        }
    }

    abstract boolean isRelevant(NAryVariable nary);

    abstract SortedSet<Integer> getVariables();
    
    public BDD getBDD() {
        if (bdd == null) {
            this.bdd = genBDD();
        }
        return this.bdd;
    }

    abstract BDD genBDD();

    boolean isRelevant(List<Integer> variables) {
        for (int var : variables) {
            if (this.getVariables().contains(var)) {
                return true;
            }
        }
        return false;
    }
}

class Fluent extends MutexFluent {

    NAryVariable nary;

    public Fluent(VariablePartitioning vars, String predicate) {
        super(vars);
        String name = this.parseAtom(predicate);
        this.nary = vars.getNAryVariable(name);
        if (this.nary == null) {
            Log.getLog().log(Level.SEVERE, "Error: unknown nary in mutex fluent: " + name);
            System.exit(-1);

        }
    }

    private String parseAtom(String predicate) {
        return predicate.substring(5, predicate.length()).replace("()", "").replace("(", ".").
                replace(", ", ".").replace(")", "").trim();
    }

    @Override
    public SortedSet<Integer> getVariables() {
        TreeSet<Integer> res = new TreeSet<Integer>();
        res.add(this.nary.getVariable());
        return res;
    }

    @Override
    public BDD genBDD() {
        return this.nary.getPreBDD().id();
    }
    
    @Override
    boolean isRelevant(NAryVariable nary) {
        return this.nary.equals(nary);
    }

    public String getName() {
        return this.nary.getName();
    }

    @Override
    public String toString() {
        return this.getName();
    }
}

class NoneOfThose extends MutexFluent {

    private ArrayList<Fluent> noneOfThose;

    public NoneOfThose(VariablePartitioning vars, String[] predicates) {
        super(vars);
        this.noneOfThose = new ArrayList<Fluent>();
        for (String predicate : predicates) {
            this.noneOfThose.add(new Fluent(vars, predicate));
        }
    }

    public Fluent getRepresentativeFluent() {
        if (this.getVariables().size() == 1) {
            int var = this.getVariables().first();
            List<String> varPart
                    = this.varsPart.getPartitionedVariables().get(var);
            if (this.noneOfThose.size() == varPart.size() - 1) {
                for (String fluent : varPart) {
                    boolean found = false;
                    for (Fluent f : this.noneOfThose) {
                        if (f.getName().equals(fluent)) {
                            found = true;
                            break;
                        }
                    }

                    if (!found) {
                        return new Fluent(varsPart, "Atom " + fluent);
                    }
                }
            }
        }

        Log.getLog().log(Level.SEVERE, "Warning: " + this
                + " does not represent a fluent in our representation");
        return null;
    }

    @Override
    public SortedSet<Integer> getVariables() {
        TreeSet<Integer> res = new TreeSet<Integer>();
        for (Fluent f : noneOfThose) {
            res.addAll(f.getVariables());
        }
        return res;
    }

    @Override
    public BDD genBDD() {
        BDD bdd = this.varsPart.getZeroBDD().id();
        for (Fluent v : noneOfThose) {
            BDD tmp = bdd;
            bdd = tmp.or(v.getBDD());
            tmp.free();
        }
        BDD tmp = bdd;
        bdd = tmp.not();
        tmp.free();

        return bdd;
    }

    @Override
    boolean isRelevant(NAryVariable nary) {
        return this.getVariables().contains(nary.getVariable());
    }

    @Override
    public String toString() {
        String res = "NoneofThose (";
        for (Fluent f : this.noneOfThose) {
            res += f + " ";
        }
        res += ")";
        return res;
    }
}

/*public ArrayList<NAryVariable> getEdeleters(NAryVariable fluent) {
 ArrayList<NAryVariable> res = new ArrayList<NAryVariable>();
 if (this.naryVars.contains(fluent)) {
 for (NAryVariable m : this.naryVars) {
 if (!m.equals(fluent)) {
 res.add(m);
 }
 }
    
 int varNone = -1;
 for (NAryVariable none : this.noneofthose) {
 if (varNone == -1) {
 varNone = none.getVariable();
 } else if (varNone != none.getVariable()) {
 Log.getLog().log("NOOOOO: " + fluent);
 System.exit(-1);
 }
 }
    
 if (varNone != -1) {
 List<String> partitioning = varsPart.getPartitionedVariables().get(varNone);
 if (this.noneofthose.size() == partitioning.size() - 1) {
 for (String s : partitioning) {
 boolean found = false;
 for (NAryVariable none : this.noneofthose) {
 if (none.getName().equals(s)) {
 found = true;
 break;
 }
 }
 if (!found) {
 //Log.getLog().log(fluent.getName() + " is mutex with " + s);
 //Log.getLog().log("According with " + this);
 res.add(varsPart.getNAryVariable(s));
 break;
 }
 }
 } else {
 Log.getLog().log("NOOOOO: " + fluent);
 System.exit(-1);
 }
 }
 }
    
 return res;
 }*/
