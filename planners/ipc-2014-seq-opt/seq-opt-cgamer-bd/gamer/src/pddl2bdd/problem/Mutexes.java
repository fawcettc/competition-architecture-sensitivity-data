/*
 * Gamer, a tool for finding optimal plans
 * Copyright (C) 2007-2012 by Peter Kissmann
 * modified by Alvaro Torralba (2012-2013)
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */
package pddl2bdd.problem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.logging.Level;
import net.sf.javabdd.BDD;
import pddl2bdd.util.Log;

/**
 * Represents state invariants.
 *
 * @author Alvaro Torralba
 */
public class Mutexes {

    VariablePartitioning vars; // Variable partitioning
    int maxNodesMutexBDDs; // Parameter of maxNodes
    long maxTimeMutexBDDs;  // Parameter of maxTime

    /*Name of invariants and mutexes as output by FD*/
    private List<Set<String>> fluentInvariants;
    private List<Set<String>> fluentMutex;

    private List<Invariant> mutexGroups; //Mutex groups in Gamer representation

    private Map<NAryVariable, BDD> fluentBDDsMutex, fluentBDDsExactlyOne; /*Representation by fluent*/

    private List<BDD> mutexBDDs; /*List of mutex BDDs*/

    private boolean fw;

    public Mutexes(boolean fw) {
        this.fluentMutex = new ArrayList<Set<String>>();
        this.fluentInvariants = new ArrayList<Set<String>>();
        this.mutexBDDs = new ArrayList<BDD>();
        this.mutexGroups = new ArrayList<Invariant>();
        this.fluentBDDsMutex = new HashMap<NAryVariable, BDD>();
        this.fluentBDDsExactlyOne = new HashMap<NAryVariable, BDD>();
        this.fw = fw;
    }

    public void cleanup() {
        for (Invariant inv : this.mutexGroups) {
            inv.cleanup();
        }
        if (this.mutexBDDs != null) {
            for (BDD mutexBDD : mutexBDDs) {
                mutexBDD.free();
            }
        }
        this.mutexBDDs = new ArrayList<BDD>();

        if (this.fluentBDDsMutex != null) {
            for (BDD bdd : this.fluentBDDsMutex.values()) {
                bdd.free();
            }
        }
        this.fluentBDDsMutex = new HashMap<NAryVariable, BDD>();
        
        if (this.fluentBDDsExactlyOne != null) {
            for (BDD bdd : this.fluentBDDsExactlyOne.values()) {
                bdd.free();
            }
        }
        this.fluentBDDsExactlyOne = new HashMap<NAryVariable, BDD>();
    }

    public void init(VariablePartitioning vars, long maxTime, int maxNodes) {
        this.vars = vars;
        this.maxTimeMutexBDDs = maxTime;
        this.maxNodesMutexBDDs = maxNodes;

        for (Set<String> fInv : fluentInvariants) {
            Invariant inv = new Invariant(vars, fInv, true);
            if (inv.valid()) {
                this.mutexGroups.add(inv);
            }
        }
        for (Set<String> fMut : fluentMutex) {
            Invariant mut = new Invariant(vars, fMut, false);
            if (mut.valid()) {
                this.mutexGroups.add(mut);
            }
        }
        this.mutexBDDs = computeMutexBDDs(new ArrayList<Integer>());
    }

    private void computeFluentBDDs() {
        Log.getLog().log("Compute Fluent BDDs");
        for (NAryVariable fluent : vars.getnAryVariables()) {
            BDD fBDDMutex = vars.getZeroBDD().id();
            BDD fBDDExactly = vars.getZeroBDD().id();
            for (Invariant inv : this.mutexGroups) {
                BDD tmp = fBDDMutex;
                BDD tmp2 = inv.getBDDMutex(fluent);
                fBDDMutex = fBDDMutex.or(tmp2);
                tmp.free();
                tmp2.free();
                
                tmp = fBDDExactly;
                tmp2 = inv.getBDDExactlyOne(fluent);
                fBDDExactly = fBDDExactly.or(tmp2);
                tmp.free();
                tmp2.free();
            }
            /*if (Log.doExpensiveLog) {
             Log.getLog().log("Fluent Mutex BDD " + fluent + ": " + fBDDMutex.nodeCount() + " "+ fBDDMutex.nodeCount());
             }*/
            this.fluentBDDsExactlyOne.put(fluent, fBDDExactly);
            this.fluentBDDsMutex.put(fluent, fBDDMutex);
           /*if (!fBDD.isZero()) {
                System.out.println(fluent + " " + (this.isFw() ? "fw" : "bw"));
                System.out.println("Mut: " + fBDD.support());
                System.out.println("Var: " + fluent.getPreBDD().support() + " => "
                        + fluent.getEffBDD().support());
            }*/
        }
        Log.getLog().log("Done fluent mutex BDDs.");
    }

    private List<BDD> computeMutexBDDs(List<Integer> emptyPartitions) {
        Log.getLog().log("Compute mutex BDDs");
        SortedMap<Integer, List<BDD>> invBDDs = new TreeMap<Integer, List<BDD>>();
        for (Invariant inv : mutexGroups) {
            inv.getBDDsByFirstVar(emptyPartitions, invBDDs);
            inv.getExactlyOneBDDsByFirstVar(emptyPartitions, invBDDs);
        }
        List<BDD> res = new ArrayList<BDD>();
        for (int key : invBDDs.keySet()) {
            res.addAll(0, invBDDs.get(key));
        }
        if (Log.doExpensiveLog) {
            Log.getLog().log("Done. Mutex BDD " + (fw ? "fw" : "bw") + ":", res);
        }
        res = mergeBDDs(res);
        Log.getLog().log(Level.INFO, "Done. Mutex BDD " + (fw ? "fw" : "bw") + ":", res);
        return res;
    }

    List<BDD> mergeBDDs(List<BDD> bdds) {
        long maxTime = System.currentTimeMillis() + maxTimeMutexBDDs;
        List<BDD> res = new ArrayList<BDD>();
        while (bdds.size() > 1 && System.currentTimeMillis() < maxTime) {
            while (bdds.size() > 1 && bdds.size() % 2 == 1) { //Ensure an even number 
                int last = bdds.size() - 1;
                BDD b1 = bdds.remove(last);
                BDD b2 = bdds.remove(last - 1);
                BDD newb = b1.or(b2, maxTime - System.currentTimeMillis(), maxNodesMutexBDDs);
                if (newb == null) {
                    if (b1.nodeCount() > b2.nodeCount()) {
                        res.add(b1);
                        bdds.add(b2);
                    } else {
                        res.add(b2);
                        bdds.add(b1);
                    }
                } else {
                    if (newb.nodeCount() < maxNodesMutexBDDs) {
                        bdds.add(newb);
                    } else {
                        res.add(newb);
                    }
                    b1.free();
                    b2.free();
                }
            }
            if(bdds.size() <= 1) break;
            List<BDD> aux = new ArrayList<BDD>();
            for (int i = 0; i < bdds.size(); i += 2) {
                BDD b1 = bdds.get(i);
                BDD b2 = bdds.get(i + 1);
                BDD newb = b1.or(b2, maxTime - System.currentTimeMillis(), maxNodesMutexBDDs);
                if (newb == null) {
                    res.add(b1);
                    res.add(b2);
                } else {
                    if (newb.nodeCount() < maxNodesMutexBDDs) {
                        aux.add(newb);
                    } else {
                        res.add(newb);
                    }
                    b1.free();
                    b2.free();
                }
            }
            bdds = aux;
        }

        res.addAll(bdds);
        return res;
    }

    public List<BDD> getMutexBDDs() {
        return mutexBDDs;
    }

    public BDD getMutexBDD(NAryVariable fluent) {
        if (fluentBDDsMutex.isEmpty()) {
            computeFluentBDDs();
        }
        return fluentBDDsMutex.get(fluent);
    }
    
        public BDD getExactlyOneBDD(NAryVariable fluent) {
        if (fluentBDDsExactlyOne.isEmpty()) {
            computeFluentBDDs();
        }
        return fluentBDDsExactlyOne.get(fluent);
    }


    public void addInvariant(Set<String> mutexGroup) {
        this.fluentInvariants.add(mutexGroup);
    }

    public void addMutex(Set<String> mutexGroup) {
        this.fluentMutex.add(mutexGroup);
    }

    public List<BDD> getMutexBDDs(List<Integer> emptyPartitions) {
        List<BDD> res = computeMutexBDDs(emptyPartitions);
        return res;
    }

  /*  public BDD getMutexBDD(NAryVariable fluent, List<Integer> emptyPartitions) {
        if (emptyPartitions.isEmpty()) {
            return getMutexBDD(fluent).id();
        }

        BDD fBDD = vars.getZeroBDD().id();
        for (Invariant inv : this.mutexGroups) {
            BDD tmp = fBDD;
            BDD tmp2 = inv.getBDD(fluent, emptyPartitions);
            fBDD = fBDD.or(tmp2);
            tmp.free();
            tmp2.free();
        }
        /*if (Log.doExpensiveLog) {
         Log.getLog().log("Fluent Mutex BDD " + fluent + ": " + fBDD.nodeCount());
         }
        return fBDD;
    }*/

    public boolean isFw() {
        return fw;
    }
}
