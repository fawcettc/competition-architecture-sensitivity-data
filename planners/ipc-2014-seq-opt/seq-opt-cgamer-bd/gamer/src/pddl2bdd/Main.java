/*
 * Gamer, a tool for finding optimal plans
 * Copyright (C) 2007-2012 by Peter Kissmann
 * modified by Alvaro Torralba (2012-2013)
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */
package pddl2bdd;

import pddl2bdd.parser.gamer.GroundedPDDLParser;
import pddl2bdd.problem.Problem;
import pddl2bdd.problem.VariablePartitioning;
import pddl2bdd.search.Solver;
import pddl2bdd.transition.TManagerBuilder;
import pddl2bdd.util.Log;

public class Main {

    public static void main(String[] args) {
        Parameters params = new Parameters(args);
        Problem problem = GroundedPDDLParser.parse(params.getPartFileName());
        /*if(params.getFDVariablesFile()!= null){
         problem.setFDVariables(params.getFDVariablesFile());
         }*/
        if (params.getValidActionsFile() != null) {
            problem.checkValidActions(params.getValidActionsFile());
        }

        if (params.getMutexFile() != null) {
            Log.getLog().log("Mutex file: " + params.getMutexFile());
            problem.readMutexFile(params.getMutexFile(), params.useInvariants());
        }

        if (params.getGamerFile() != null) {
            Log.getLog().log("Gamer file: " + params.getGamerFile());
            problem.readGamerVariablesFile(params.getGamerFile());
        }

        VariablePartitioning variables = new VariablePartitioning(problem, params);
        variables.initMutex(params.getMaxTimeMutexBDDs(), params.getMaxNodesMutexBDDs());

        TManagerBuilder tBuilder = params.getTransitionManager(variables);
        Solver solver = params.getSolver(variables, tBuilder);

        solver.search();
        tBuilder.cleanup();
        Log.getLog().log("Tbuilder cleaned");
        solver.cleanup();
        Log.getLog().log("Solver cleaned");
        variables.cleanup();
    }
}
