/*
 * Gamer, a tool for finding optimal plans
 * Copyright (C) 2007-2012 by Peter Kissmann
 * modified by Alvaro Torralba (2012-2013)
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */
package pddl2bdd.parser.gamer.logic;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;

import net.sf.javabdd.BDD;
import pddl2bdd.problem.VariablePartitioning;
import pddl2bdd.transition.Assign;
import pddl2bdd.util.Log;

public class NotTerm extends Expression {
    @Override
    public BDD createBDD(VariablePartitioning variables,
            boolean precondition, List<Integer> emptyPartitions) {
        BDD tmp = term.createBDD(variables, precondition, emptyPartitions);
        if (tmp == null)
            return null;
        BDD ret = tmp.not();
        tmp.free();
        return ret;
    }

    @Override
    public Set<Set<Assign>> getAssignments(VariablePartitioning variables) {
        Set<Set<Assign>> res = this.term.getAssignments(variables);
        if(res.size() != 1 || res.iterator().next().size() != 1){
            Log.getLog().log(Level.SEVERE, "Error: NotTerm applied over something that its not a single term: " + res);
            System.exit(-1);
        }
        for (Set <Assign> arr : res){
            for (Assign a : arr){
                a.not();
            }
        }
        return res;
    }

    @Override
    public void classifyEffects(ArrayList<String> addEffects,
            ArrayList<String> deleteEffects, boolean isNegated) {
        term.classifyEffects(addEffects, deleteEffects, !isNegated);
    }
    
    @Override
    public void getAllPredicates(ArrayList<Predicate> allPreds) {
        term.getAllPredicates(allPreds);
    }

    @Override
    public void findDelAdds(ArrayList<String> delEffects,
            ArrayList<String> addEffects, boolean isPositive) {
        term.findDelAdds(delEffects, addEffects, !isPositive);
    }

    @Override
    public boolean eliminateDelEffects(ArrayList<String> delEffectsToEliminate,
            boolean isPositive) {
        return term.eliminateDelEffects(delEffectsToEliminate, !isPositive);
    }

    @Override
    public String toString() {
        String ret = "(not\n" + term.toString() + "\n)";
        return ret;
    }

    public void setTerm(Expression term) {
        this.term = term;
    }

    public Expression getTerm() {
        return term;
    }

    private Expression term;
}
