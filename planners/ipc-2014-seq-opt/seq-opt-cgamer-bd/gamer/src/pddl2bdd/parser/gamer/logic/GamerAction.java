/*
 * Gamer, a tool for finding optimal plans
 * Copyright (C) 2007-2012 by Peter Kissmann
 * modified by Alvaro Torralba (2012-2013)
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */
package pddl2bdd.parser.gamer.logic;

import java.util.ListIterator;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Stack;
import java.util.logging.Level;
import net.sf.javabdd.BDD;
import pddl2bdd.problem.Action;
import pddl2bdd.problem.NAryVariable;

import pddl2bdd.problem.VariablePartitioning;

import pddl2bdd.transition.Assign;
import pddl2bdd.transition.Transition;
import pddl2bdd.util.Log;
import pddl2bdd.util.Maths;

/**
 * @author Peter Kissmann and Alvaro Torralba
 */
public class GamerAction extends Action {

    private Expression precondition;
    private Expression effect;

    public void setPrecondition(Expression precondition) {
        this.precondition = precondition;
    }

    public Expression getPrecondition() {
        return precondition;
    }

    public void setEffect(Expression effect) {
        this.effect = effect;
        ArrayList<String> delEffects = new ArrayList<String>();
        ArrayList<String> addEffects = new ArrayList<String>();
        this.effect.findDelAdds(delEffects, addEffects, true);
        ListIterator<String> delIt = delEffects.listIterator();
        ArrayList<String> delEffectsToEliminate = new ArrayList<String>();
        while (delIt.hasNext()) {
            String predName = delIt.next();
            if (addEffects.contains(predName)) {
                delEffectsToEliminate.add(predName);
            }
        }
        if (delEffectsToEliminate.size() > 0) {
            //System.out.println(this);
            effect.eliminateDelEffects(delEffectsToEliminate, true);
        }
    }

    public Expression getEffect() {
        return effect;
    }

    public String toString() {
        String ret = "action " + name + ":\n";
        ret += "cost: " + cost + "\n";
        ret += "precondition:\n";
        ret += precondition.toString() + "\n";
        ret += "effect:\n";
        ret += effect.toString() + "\n\n";
        return ret;
    }

    /**
     * Returns all possible transitions associated with this operator (there is
     * more than one when we have disjunctive preconditions)
     *
     * @return List of transitions
     */
    @Override
    public ArrayList<Transition> getTransitions(VariablePartitioning variables,
            List<Integer> emptyPartitions) {

        ArrayList<Transition> res = new ArrayList<Transition>();
        Set<Set<Assign>> prec;
        Set<Set<Assign>> eff;

        prec = this.precondition.getAssignments(variables);
        eff = this.effect.getAssignments(variables);

        //Log.getLog().log(Level.SEVERE, "Correct action: " + this.name);
        HashSet<HashSet<NAryVariable>> correctedEffects = new HashSet<HashSet<NAryVariable>>();
        for (Set<Assign> e : eff) {
            HashSet<NAryVariable> corrEff = variables.correctEffect(e, emptyPartitions);
                if(!corrEff.isEmpty()){
            correctedEffects.add(corrEff);
                }
        }

        for (Set<Assign> pre : prec) {
            HashSet<HashSet<NAryVariable>> groundedPreconditions
                    = this.groundPreconditions(pre.iterator(), emptyPartitions);
            for (HashSet<NAryVariable> p : groundedPreconditions) {
                for (HashSet<NAryVariable> e : correctedEffects) {
                    res.add(new Transition(variables, this.name, this.cost, p, e));
                }
            }
        }
        if (res.size() > 1) {
            Log.getLog().log(Level.WARNING, "Warning: Action with more than one transition: " + this);
        }
        if (emptyPartitions.isEmpty() && res.isEmpty()) {
            Log.getLog().log(Level.WARNING, "Warning: Pruned action: " + this.getName());
            //System.exit(-1);
        }
        return res;
    }

    /**
     * Recursive algorithm for getting the grounded preconditions.
     *
     * @param preconditions
     * @return
     */
    private HashSet<HashSet<NAryVariable>> groundPreconditions(Iterator<Assign> preconditions,
            List<Integer> emptyPartitions) {
        HashSet<HashSet<NAryVariable>> res = new HashSet<HashSet<NAryVariable>>();

        if (!preconditions.hasNext()) {
            res.add(new HashSet<NAryVariable>());
        } else {
            Assign assign = preconditions.next();
            while(assign.isStaticFact()){
                if(preconditions.hasNext()){
                    assign = preconditions.next();
                }else{
                    res.add(new HashSet<NAryVariable>());
                    return res;
                }
            }
            if (assign.isUnreachable()) {
                //There are no possibilities
                return new HashSet<HashSet<NAryVariable>>();
            } else {
                ArrayList<NAryVariable> posibilities = assign.getNAryVariables();
                HashSet<HashSet<NAryVariable>> prev = this.groundPreconditions(preconditions, emptyPartitions);
                for (HashSet<NAryVariable> prev_posib : prev) {
                    for (NAryVariable posib : posibilities) {
                        if (!emptyPartitions.contains(posib.getVariable())) {
                            HashSet<NAryVariable> combi = new HashSet<NAryVariable>(prev_posib);
                            combi.add(posib);
                            res.add(combi);
                        } else {
                            res.add(prev_posib);
                        }
                    }
                }
            }
        }
        return res;
    }

    public BDD createBDD(VariablePartitioning variables, List<Integer> emptyPartitions) {
        BDD tmp1;
        BDD tmp2;
        BDD action;

        tmp1 = precondition.createBDD(variables, true, emptyPartitions);
        if (tmp1 == null) {
            tmp1 = variables.getOneBDD().id();
        }
        tmp2 = effect.createBDD(variables, false, emptyPartitions);
        if (tmp2 == null) {
            tmp1.free();
            return null;
        }
        action = tmp1.and(tmp2);
        tmp1.free();
        tmp2.free();

        ArrayList<String> addEffects = new ArrayList<String>();
        ArrayList<String> deleteEffects = new ArrayList<String>();
        effect.classifyEffects(addEffects, deleteEffects, false);

        int partitionCounter = 0;
        int currentVariable = 0;
        Stack<BDD> bddsForConjunction = new Stack<BDD>();
        for (List<String> group : variables.getPartitionedVariables()) {
            int numberOfVars = Maths.log2(group.size());
            boolean oneVariableInserted = false;
            boolean positiveVariableInserted = false;
            for (String pred : group) {
                if (addEffects.contains(pred)) {
                    oneVariableInserted = true;
                    positiveVariableInserted = true;
                    break;
                }
                if (!oneVariableInserted && deleteEffects.contains(pred)) {
                    oneVariableInserted = true;
                }
            }
            if (!oneVariableInserted) {
                for (int i = 0; i < numberOfVars; i++) {
                    tmp1 = variables.getBDDVar((currentVariable + i) * 2);
                    tmp1 = tmp1.biimp(variables.getBDDVar((currentVariable + i) * 2 + 1));
                    /*tmp2 = action;
                     action = tmp1.and(tmp2);
                     tmp1.free();
                     tmp2.free();*/
                    bddsForConjunction.push(tmp1);
                }
            } else if (!positiveVariableInserted) {
                String noneOfTheseName = null;
                for (String sg : group) {
                    if (sg.startsWith("none-ot-these")) {
                        noneOfTheseName = sg;
                        break;
                    }
                }
                if (noneOfTheseName != null) {
                    /*tmp1 = action;
                     action = tmp1.and(variables.getNAryVariable(group.getLast()).getEffBDD());
                     tmp1.free();*/
                    bddsForConjunction.push(variables.getNAryVariable(noneOfTheseName).getEffBDD().id());
                } else {
                    System.err.println("Error: only negated variable of group "
                            + partitionCounter
                            + " in effect though there is no \'none-of-these\'-variable!");
                    System.err.println("in action: " + this);

                    //System.exit(1);
                    //We are here because some operator was pruned in the nPar generation.
                    //Instead of exit we put anything in the effect of the operator)
                    //It does not matter because the preconditions are expected to be contradictory.
                    /*tmp1 = action;
                     action = tmp1.and(variables.getNAryVariable(group.getLast()).getEffBDD());
                     tmp1.free();*/
                    bddsForConjunction.push(variables.getNAryVariable(group.get(0)).getEffBDD().id());
                }
            }
            partitionCounter++;
            currentVariable += numberOfVars;
        }

        while (!bddsForConjunction.empty()) {
            tmp1 = bddsForConjunction.pop();
            tmp2 = action;
            action = tmp1.and(tmp2);
            tmp1.free();
            tmp2.free();
        }
        return action;
    }

    @Override
    public void effectVariables(List<Integer> effectIndices,
            List<String> allVars, List<Integer> partitionIndices) {
        findAllVariables(effectIndices, allVars, partitionIndices, effect);
    }

    @Override
    public void preconditionVariables(List<Integer> preIndices,
            List<String> allVars, List<Integer> partitionIndices) {
        findAllVariables(preIndices, allVars, partitionIndices, precondition);
    }

    private void findAllVariables(List<Integer> allIndices,
            List<String> allVariables, List<Integer> indices,
            Expression formula) {

        if (formula instanceof AndOrTerm) {
            for (Expression term : ((AndOrTerm) formula).getTerms()) {
                findAllVariables(allIndices, allVariables, indices, term);
            }
        } else if (formula instanceof NotTerm) {
            findAllVariables(allIndices, allVariables, indices,
                    ((NotTerm) formula).getTerm());
        } else if (formula instanceof Predicate) {
            if (!((Predicate) formula).getName().equalsIgnoreCase("foo")) {
                int index = allVariables.indexOf(((Predicate) formula).getName());
                int partIndex = -1;

                for (int ind : indices) {
                    if (ind > index) {
                        break;
                    }
                    partIndex++;
                }
                if (partIndex != -1 && !allIndices.contains(partIndex)) {
                    allIndices.add(partIndex);
                }
            }
        } else {
            System.err.println("Error: class " + formula.getClass()
                    + " in findAllVariables!");
            System.err.println("Expression was:");
            System.err.println(formula);
            System.exit(1);
        }
    }
}
