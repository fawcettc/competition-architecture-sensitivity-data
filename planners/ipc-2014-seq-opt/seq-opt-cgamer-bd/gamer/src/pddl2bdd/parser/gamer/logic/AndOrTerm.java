/*
 * Gamer, a tool for finding optimal plans
 * Copyright (C) 2007-2012 by Peter Kissmann
 * modified by Alvaro Torralba (2012-2013)
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */package pddl2bdd.parser.gamer.logic;

import java.util.ListIterator;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import net.sf.javabdd.BDD;
import pddl2bdd.problem.VariablePartitioning;
import pddl2bdd.transition.Assign;

public class AndOrTerm extends Expression {

    @Override
    public BDD createBDD(VariablePartitioning variables, boolean precondition,
            List<Integer> emptyPartitions) {
        if (terms.isEmpty()) {
            if (isAndTerm) {
                return variables.getOneBDD().id();
            } else {
                return variables.getZeroBDD().id();
            }
        }
        if (terms.size() == 1) {
            return terms.get(0).createBDD(variables, precondition, emptyPartitions);
        }
        BDD tmp1;
        BDD tmp2;
        ArrayList<BDD> termBDDs = new ArrayList<BDD>();
        ListIterator<Expression> termsIt = terms.listIterator();
        while (termsIt.hasNext()) {
            tmp1 = termsIt.next().createBDD(variables, precondition, emptyPartitions);
            if (tmp1 != null) {
                termBDDs.add(tmp1);
            }
        }
        if (termBDDs.isEmpty()) {
            return null;
        }
        if (termBDDs.size() == 1) {
            return termBDDs.get(0);
        }

        for (int i = 0; i < termBDDs.size(); i++) {
            tmp1 = termBDDs.get(i);
            i++;
            if (i < termBDDs.size()) {
                tmp2 = termBDDs.get(i);
                if (isAndTerm) {
                    termBDDs.add(tmp1.and(tmp2));
                } else {
                    termBDDs.add(tmp1.or(tmp2));
                }
                tmp1.free();
                tmp2.free();
            }
        }
        return termBDDs.get(termBDDs.size() - 1);
    }

    @Override
    public Set<Set<Assign>> getAssignments(VariablePartitioning variables) {
        Set<Set<Assign>> res = new HashSet<Set<Assign>>();

        if (isAndTerm) { 
            Set<Set<Assign>> resOld = new HashSet<Set<Assign>>();
            resOld.add(new HashSet<Assign>());
            for (Expression t : terms) {
                res = new HashSet<Set<Assign>>();
                Set<Set<Assign>> newTerms = t.getAssignments(variables);
                for (Set<Assign> newTerm : newTerms) {
                    for(Set<Assign> possibility : resOld){
                        newTerm.addAll(possibility);
                        res.add(newTerm);
                    }
                }
                resOld = res;
            }
        } else {
            for (Expression t : terms) {
                res.addAll(t.getAssignments(variables));
            }
        }

        return res;
    }

    @Override
    public void classifyEffects(ArrayList<String> addEffects,
            ArrayList<String> deleteEffects, boolean isNegated) {
        ListIterator<Expression> termsIt = terms.listIterator();
        while (termsIt.hasNext()) {
            termsIt.next().classifyEffects(addEffects, deleteEffects, isNegated);
        }
    }

    @Override
    public void getAllPredicates(ArrayList<Predicate> allPreds) {
        ListIterator<Expression> termIt = terms.listIterator();
        while (termIt.hasNext()) {
            termIt.next().getAllPredicates(allPreds);
        }
    }

    @Override
    public void findDelAdds(ArrayList<String> delEffects,
            ArrayList<String> addEffects, boolean isPositive) {
        ListIterator<Expression> termIt = terms.listIterator();
        while (termIt.hasNext()) {
            termIt.next().findDelAdds(delEffects, addEffects, isPositive);
        }
    }

    @Override
    public boolean eliminateDelEffects(ArrayList<String> delEffectsToEliminate,
            boolean isPositive) {
        ListIterator<Expression> termIt = terms.listIterator();
        while (termIt.hasNext()) {
            if (termIt.next().eliminateDelEffects(delEffectsToEliminate,
                    isPositive)) {
                termIt.remove();
            }
        }
        return false;
    }

    @Override
    public String toString() {
        String ret;
        if (isAndTerm) {
            ret = "(and\n";
        } else {
            ret = "(or\n";
        }
        ListIterator<Expression> termsIt = terms.listIterator();
        while (termsIt.hasNext()) {
            ret += termsIt.next().toString() + "\n";
        }
        ret += ")";
        return ret;
    }

    public void setIsAndTerm(boolean isAndTerm) {
        this.isAndTerm = isAndTerm;
    }

    public boolean getIsAndTerm() {
        return isAndTerm;
    }

    public void setTerms(ArrayList<Expression> terms) {
        this.terms = terms;
    }

    public ArrayList<Expression> getTerms() {
        return terms;
    }
    private boolean isAndTerm;
    private ArrayList<Expression> terms;
}
