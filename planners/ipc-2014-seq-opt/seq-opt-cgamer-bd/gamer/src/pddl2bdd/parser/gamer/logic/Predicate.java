/*
 * Gamer, a tool for finding optimal plans
 * Copyright (C) 2007-2012 by Peter Kissmann
 * modified by Alvaro Torralba (2012-2013)
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */
package pddl2bdd.parser.gamer.logic;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;

import net.sf.javabdd.BDD;
import pddl2bdd.problem.NAryVariable;
import pddl2bdd.transition.Assign;
import pddl2bdd.problem.VariablePartitioning;
import pddl2bdd.util.Log;

public class Predicate extends Expression {

    @Override
    public BDD createBDD(VariablePartitioning variables, boolean precondition, List<Integer> emptyPartitions) {
        if (name.equalsIgnoreCase("foo")) {
            return variables.getOneBDD().id();
        }
        NAryVariable nAryVar = variables.getNAryVariable(name);

        if (nAryVar == null) {
            if(variables.isStatic(name) || variables.isUnreachable(name)){
                return variables.getOneBDD().id();
            }
            Log.getLog().log(Level.WARNING, "Warning, creatingBDD of nary var that does not exist " + name);
            return null;
        }
        
        if (emptyPartitions.contains(nAryVar.getVariable())) {
            return null;
        }

        if (precondition) {
            return nAryVar.getPreBDD().id();
        } else {
            return nAryVar.getEffBDD().id();
        }
    }

    @Override
    public Set<Set<Assign>> getAssignments(VariablePartitioning variables) {
        Set<Set<Assign>> res = new HashSet<Set<Assign>>();
        Set<Assign> res2 = new HashSet<Assign>();
        res.add(res2);
        NAryVariable nAryVar = variables.getNAryVariable(name);
        if (nAryVar == null) {
            if(variables.isUnreachable(name)){
                res2.add(new Assign(true, variables.getZeroBDD(), variables.getOneBDD()));
            }else if (variables.isStatic(name)){
                res2.add(new Assign(false, variables.getZeroBDD(), variables.getOneBDD()));
            }else{
                Log.getLog().log(Level.WARNING, "Warning: nAryVar not found: " + name);
            }
        }else{
            res2.add(new Assign(nAryVar));
        }

        return res;
    }

    @Override
    public void classifyEffects(ArrayList<String> addEffects,
            ArrayList<String> deleteEffects, boolean isNegated) {
        if (!addEffects.contains(name)) {
            if (isNegated && !deleteEffects.contains(name)) {
                deleteEffects.add(name);
            } else {
                addEffects.add(name);
            }
        }
    }

    @Override
    public void getAllPredicates(ArrayList<Predicate> allPreds) {
        if (!allPreds.contains(this)) {
            allPreds.add(this);
        }
    }

    @Override
    public void findDelAdds(ArrayList<String> delEffects,
            ArrayList<String> addEffects, boolean isPositive) {
        if (isPositive) {
            addEffects.add(name);
        } else {
            delEffects.add(name);
        }
    }

    @Override
    public boolean eliminateDelEffects(ArrayList<String> delEffectsToEliminate,
            boolean isPositive) {
        if (!isPositive && delEffectsToEliminate.contains(name)) {
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        String ret = "(" + name + ")";
        return ret;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    private String name;

}
