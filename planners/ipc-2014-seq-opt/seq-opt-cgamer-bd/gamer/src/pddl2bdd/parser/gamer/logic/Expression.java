/*
 * Gamer, a tool for finding optimal plans
 * Copyright (C) 2007-2012 by Peter Kissmann
 * modified by Alvaro Torralba (2012-2013)
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */
package pddl2bdd.parser.gamer.logic;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import net.sf.javabdd.*;
import pddl2bdd.transition.Assign;
import pddl2bdd.problem.VariablePartitioning;

public abstract class Expression {
    public abstract BDD createBDD(VariablePartitioning variables,
            boolean precondition, List <Integer> emptyPartitions);

    public abstract void classifyEffects(ArrayList<String> addEffects,
            ArrayList<String> deleteEffects, boolean isNegated);

    public abstract void getAllPredicates(ArrayList<Predicate> allPreds);

    public abstract void findDelAdds(ArrayList<String> delEffects,
            ArrayList<String> addEffects, boolean isPositive);

    public abstract boolean eliminateDelEffects(
            ArrayList<String> delEffectsToEliminate, boolean isPositive);

    public abstract String toString();
    
    /**
     * Returns the assignments of this expression in disjunctive normal form.
     * (or (and ...) (and ...) ...)
     */
    public abstract Set <Set <Assign>> getAssignments(VariablePartitioning variables);
}
