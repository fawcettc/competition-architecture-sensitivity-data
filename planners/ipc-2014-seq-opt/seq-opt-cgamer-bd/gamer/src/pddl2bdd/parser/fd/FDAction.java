/*
 * Gamer, a tool for finding optimal plans
 * Copyright (C) 2007-2012 by Peter Kissmann
 * modified by Alvaro Torralba (2012-2013)
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */
package pddl2bdd.parser.fd;

import java.util.ArrayList;
import java.util.List;
import pddl2bdd.problem.Action;
import pddl2bdd.problem.VariablePartitioning;
import pddl2bdd.transition.Transition;

/**
 *
 * @author Alvaro Torralba
 */
public class FDAction extends Action{

    @Override
    public ArrayList<Transition> getTransitions(VariablePartitioning variables, List<Integer> emptyPartitions) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void effectVariables(List<Integer> effectIndices, List<String> allVars,
    List<Integer> partitionIndices) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void preconditionVariables(List<Integer> effectIndices, List<String> allVars,
    List<Integer> partitionIndices) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
}
