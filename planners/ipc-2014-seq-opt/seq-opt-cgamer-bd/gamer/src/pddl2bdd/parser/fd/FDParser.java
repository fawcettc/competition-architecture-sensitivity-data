/*
 * Gamer, a tool for finding optimal plans
 * Copyright (C) 2007-2012 by Peter Kissmann
 * modified by Alvaro Torralba (2012-2013)
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */
package pddl2bdd.parser.fd;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import pddl2bdd.parser.gamer.logic.Predicate;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import pddl2bdd.problem.Problem;
import pddl2bdd.util.Log;

/**
 *
 * @author Alvaro Torralba
 */
public class FDParser {

    public Problem parse(File output) {
        Problem problem = new Problem();
        /*try {
            BufferedReader br = new BufferedReader(new FileReader(output));
            String ln;
            while ((ln = br.readLine()) != null) {
                ln = ln.toLowerCase().trim();
                if (ln.equals("")) {
                    continue;
                }else if(ln.equals("begin_operator")){
                    problem.addAction(parseOperator(br));
                }else if (ln.equals("begin_variable")){
                    problem.addPartitioningGroup(parseVariable(br));
                }else if (ln.equals("begin_mutex_group")){
                    problem.getMutex().addInvariant(parseMutexGroup(br));
                }else{
                    while (!br.readLine().startsWith("end_"));
                }
            }
            br.close();
        } catch (IOException e) {
            Log.getLog().log("Error while reading fd output file");
        }*/
        
        return problem;
    }

    private FDAction parseOperator(BufferedReader br) {
        FDAction acc = new FDAction();
        
        return acc;
    }

    private ArrayList<Predicate> parseVariable(BufferedReader br) {
        ArrayList<Predicate> res = new ArrayList<Predicate>();
        
        return res;
    }

    private Set<String> parseMutexGroup(BufferedReader br) {
        HashSet <String> res = new HashSet<String>();
        return res;
                
    }
}
