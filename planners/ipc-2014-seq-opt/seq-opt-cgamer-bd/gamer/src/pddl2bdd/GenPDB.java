package pddl2bdd;

/**
 *
 * @author Alvaro Torralba Arias de Reyna
 */
public class GenPDB {
    /*public static TManagerType ttype = TManagerType.TTree;
    public static ArrayList<String> tManagerParams = new ArrayList<String>();

    public static VarOrderType vOrderType = VarOrderType.Optimize;
    public static ArrayList<String> vOrderParams = new ArrayList<String>();

    public static ArrayList<String> pdbGenParams = new ArrayList<String>();

    public static void parseArgs(String [] args){
        int i = 1;
        while (i < args.length){
            String arg = args[i];
            System.out.println(arg);
            ArrayList<String> params = new ArrayList<String>();
            while(++i < args.length && !args[i].startsWith("-")){
                params.add(args[i]);
            }

            if(arg.equals("-tt")){            //TRANSITION MANAGER OPTIONS
                ttype = TManagerType.TTree;
                tManagerParams = params;
            }else if (arg.equals("-tl")) {
                ttype = TManagerType.TList;
                tManagerParams = params;
            }else if (arg.equals("-vopt")) {  //VARIABLE ORDERING OPTIONS
                vOrderType = VarOrderType.Optimize;
                vOrderParams = params;
            }else if (arg.equals("-vdef")) {
                vOrderType = VarOrderType.Default;
                vOrderParams = params;
            }else if (arg.equals("-vload")) {
                vOrderType = VarOrderType.Load;
                vOrderParams = params;
            }else{
                pdbGenParams.add(arg);
                pdbGenParams.addAll(params);
            }
        }
    }
    
    public static void main(String args []){
            String library = "cudd";
        BDDFactory factory = BDDFactory.init(library, 16000000, 16000000);

        String partFileName = "orig-1nPar.gdl";
        if (args.length > 0) {
            partFileName = args[0];
        }
        parseArgs(args);

        GroundedPDDLParser.parse(partFileName);

        VariableOrdering varOrder = VariableOrdering.getVarOrderAlg(vOrderType, vOrderParams);

        VariablePartitioning variables = new VariablePartitioning(factory, varOrder);

        TManagerBuilder tBuilder = TManagerBuilder.getTransitionManager(factory,
                variables, ttype, tManagerParams);
        
        PDBGeneration pdbGen = new PDBGeneration(variables, tBuilder, pdbGenParams);
    }
    */
}
