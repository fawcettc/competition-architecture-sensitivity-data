/*
 * Gamer, a tool for finding optimal plans
 * Copyright (C) 2007-2012 by Peter Kissmann
 * modified by Alvaro Torralba (2012-2013)
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */
package pddl2bdd;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import pddl2bdd.deprecated.TListBuilder;
import pddl2bdd.parser.gamer.GroundedPDDLParser;
import pddl2bdd.varOrder.VarOrderingOpt;
import pddl2bdd.varOrder.VariableOrdering;
import pddl2bdd.problem.VariablePartitioning;
import pddl2bdd.search.BidirectionalDijkstra;
import pddl2bdd.search.ListAStar;
import pddl2bdd.search.PDBGeneration;
import pddl2bdd.search.PDBHeuristic;
import pddl2bdd.search.Solver;
import pddl2bdd.transition.TManagerBuilder;
import pddl2bdd.transition.TTreeBuilder;
import pddl2bdd.util.Log;

/**
 * Parameter parser. Read parameters
 *
 * @author Peter Kissmann and Alvaro Torralba
 */
public class Parameters {

    public Parameters(String[] args) {
        try {
            /*JCommander jCommander = */
            new JCommander(new Object[]{this, Log.getLog()}, args);
            this.edeletesType = TTreeBuilder.EdeleteType.valueOf(edeletes.toUpperCase());
        } catch (ParameterException e) {
            e.printStackTrace();
            System.err.println("ERROR: " + e.getMessage());
            this.help = true;
        }
        Log.getLog().init();
        if (this.help) {
            new JCommander(this).usage();
            System.exit(0);
        }
    }
    @Parameter(names = "-fM", description = "File with Mutex descriptions")
    private File mutexFile = null;
    @Parameter(names = "-fA", description = "File with valid action names")
    private File validActionsFile = null;
    /*@Parameter(names = "-fV", description = "File with variables")
     private File FDVariablesFile = null;*/
    @Parameter(names = "-fG", description = "File with gamer variables")
    private File gamerFile = null;

    @Parameter(names = "-edel", description = "Use edeletes instead mutex BDDs")
    private String edeletes = "none";
    private TTreeBuilder.EdeleteType edeletesType;
    @Parameter(names = "-noInv", description = "Avoids the use of invariants")
    private boolean discardInvariants = false;

    @Parameter(names = "-nm", description = "Maximum number of nodes for mutex bdds")
    private int maxNodesMutexBDDs = 100000;
    @Parameter(names = "-tm", description = "Maximum time in ms for mutex bdds")
    private int maxTimeMutexBDDs = 60000;

    @Parameter(names = "-v",
            description = "Algorithm to optimize variable ordering."
            + " \"opt\" => optimize ordering; <filename> => read from file",
            converter = VarOrderConverter.class)
    private VariableOrdering varOrder = new VarOrderingOpt();
    @Parameter(names = "-a", variableArity = true,
            description = "<alg> params. <alg> = blind, astar or pdb."
            + " Each algorithm has different params.")
    public List<String> searchAlgParams = new ArrayList<String>();
    @Parameter(names = "-t", variableArity = true, arity = 0,
            description = "<alg> params. <alg> = tl, list, tt or tree.")
    public List<String> tManagerParams = new ArrayList();
    @Parameter(names = "--help")
    private boolean help;
    @Parameter(names = "-P",
            description = "Part filename, default is \"orig-1nPar.gdl\"")
    String partFileName = "orig-1nPar.gdl";

    @Parameter(names = "-cache",
            description = "Cache size to initialize the CUDD manager")
    int cuddInitCacheSize = 16000000;

    @Parameter(names = "-nodes",
            description = "Number of nodes to initialize the CUDD manager")
    int cuddInitNodes = 16000000;

    public VariableOrdering getVarOrder() {
        return varOrder;
    }

    public Solver getSolver(VariablePartitioning variables, TManagerBuilder tBuilder) {
        if (this.searchAlgParams.isEmpty()) {
            return new BidirectionalDijkstra(GroundedPDDLParser.problem, variables, tBuilder, this.searchAlgParams, edeletesType == TTreeBuilder.EdeleteType.NONE);
        }
        String alg = this.searchAlgParams.get(0);
        ArrayList<String> params = new ArrayList<String>(this.searchAlgParams);
        params.remove(0);

        if (alg.equals("blind")) {
            return new BidirectionalDijkstra(GroundedPDDLParser.problem,
                    variables, tBuilder, params,
                    edeletesType == TTreeBuilder.EdeleteType.NONE);
        } else if (alg.equals("astar")) {
            return new ListAStar(GroundedPDDLParser.problem, variables, tBuilder,
                    new PDBHeuristic(variables.getFactory()));
        } else if (alg.equals("pdb")) {
            return new PDBGeneration(GroundedPDDLParser.problem, variables,
                    tBuilder, params, edeletesType == TTreeBuilder.EdeleteType.NONE);
        }

        return new BidirectionalDijkstra(GroundedPDDLParser.problem, variables, tBuilder, params, edeletesType == TTreeBuilder.EdeleteType.NONE);
    }

    public TManagerBuilder getTransitionManager(VariablePartitioning variables) {
        if (tManagerParams.isEmpty()) {
            return new TListBuilder(GroundedPDDLParser.problem, variables, new ArrayList<String>());
        }
        String tMgrName = tManagerParams.get(0);
        ArrayList<String> params = new ArrayList<String>(this.tManagerParams);
        params.remove(0);

        if (tMgrName.equals("tt") || tMgrName.equals("tree")) {
            return new TTreeBuilder(GroundedPDDLParser.problem, variables, params, edeletesType);
        } else {
            return new TListBuilder(GroundedPDDLParser.problem, variables, params);
        }
    }

    public String getPartFileName() {
        return this.partFileName;
    }

    public File getMutexFile() {
        return this.mutexFile;
    }

    public File getGamerFile() {
        return gamerFile;
    }

    public File getValidActionsFile() {
        return validActionsFile;
    }

    public int getMaxNodesMutexBDDs() {
        return maxNodesMutexBDDs;
    }

    public int getMaxTimeMutexBDDs() {
        return maxTimeMutexBDDs;
    }

    public boolean useEdeletes() {
        return edeletesType != TTreeBuilder.EdeleteType.NONE;
    }

    public boolean useMutexBDDs() {
        return edeletesType == TTreeBuilder.EdeleteType.NONE;
    }

    public boolean useInvariants() {
        return !discardInvariants;
    }

    public int getCuddInitCacheSize() {
        return cuddInitCacheSize;
    }

    public int getCuddInitNodes() {
        return cuddInitNodes;
    }
    
    
}
