package pddl2bdd.util;

import java.util.Random;

/**
 *
 * @author alvaro
 */
public class Rnd {
    private static Random rnd = new Random (0);

    public static int nextInt(int size) {
        return rnd.nextInt(size);
    }
}
