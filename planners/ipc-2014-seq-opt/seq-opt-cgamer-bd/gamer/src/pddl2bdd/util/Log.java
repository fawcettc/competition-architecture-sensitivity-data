/*
 * Gamer, a tool for finding optimal plans
 * Copyright (C) 2007-2012 by Peter Kissmann
 * modified by Alvaro Torralba (2012-2013)
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */
package pddl2bdd.util;

import com.beust.jcommander.Parameter;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.logging.ConsoleHandler;
import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import net.sf.javabdd.BDD;

/**
 * Class for perform logging operations. Allows using standard output or file
 * output. Also, it logs BDDs to file.
 *
 * @author Peter Kissmann and Alvaro Torralba
 */
public class Log {
    /*
     * Public static attribute to avoid expensive log computation.
     */

    public static final boolean doExpensiveLog = true;
    private static Log logger = null;
    @Parameter(names = "-logBDD",
            description = "path to log BDDs. If none is found the BDDs are not logged")
    private String pathBDDs = null;

    @Parameter(names = "-log", description = "Decide if log is performed")
    private boolean doLog = false;

    private Logger log;

    public static Log getLog() {
        if (logger == null) {
            logger = new Log();
        }
        return logger;
    }
    private int uniqueId;

    public Log() {
        uniqueId = 0;

    }

    class StdoutConsoleHandler extends ConsoleHandler {
        protected void setOutputStream(OutputStream out) throws SecurityException {
            super.setOutputStream(System.out); // kitten killed here :-(
        }
    }

    public void init() {
        this.log = Logger.getAnonymousLogger();

        StdoutConsoleHandler c = new StdoutConsoleHandler();
        c.setFormatter(new Formatter() {
            @Override
            public String format(LogRecord record) {
                return record.getMessage() + "\n";
            }
        });
        this.log.setUseParentHandlers(false);
        this.log.addHandler(c);

        /*StreamHandler c = new StreamHandler(System.out, new Formatter() {
         @Override
         public String format(LogRecord record) {
         return record.getMessage() + "\n";
         }});*/
        if (doLog) {
            this.log.setLevel(Level.ALL);
            c.setLevel(Level.ALL);
        } else {
            this.log.setLevel(Level.INFO);
            c.setLevel(Level.INFO);
        }

    }

    public void log(String msg) {
        this.log(Level.FINE, msg);
    }

    public void log(String msg, List<BDD> bdds) {
        this.log(Level.FINE, msg, bdds);
    }

    public void log(String msg, int[] array) {
        this.log(Level.FINE, msg, array);
    }

    public void log(Level level, String msg) {
        this.log.log(level, msg);
    }

    public void log(Level level, String msg, List<BDD> bdds) {
        if (bdds.isEmpty()) {
            msg += " 0";
        }
        for (BDD bdd : bdds) {
            msg += " " + bdd.nodeCount();
        }
        log.log(level, msg);
    }

    public void log(Level level, String msg, int[] array) {
        for (int n : array) {
            msg += " " + n;
        }
        log.log(level, msg);
    }

    private void ensureDoNotExist(String filename) {
        File f = new File(filename);
        if (f.exists()) {
            ensureDoNotExist(filename + ".old");
            f.renameTo(new File(filename + ".old"));
        }
    }

    public void logBDD(BDD bdd, String name) {
        if (pathBDDs != null) {

            try {
                File dir = new File(this.pathBDDs);
                if (!dir.isDirectory()) {
                    System.err.append("Error in BDD log: " + this.pathBDDs + " is not a valid path");
                }
                if (!dir.exists()) {
                    dir.mkdirs();
                }
                String filename = pathBDDs + (uniqueId++) + "_" + name + ".bdd";
                ensureDoNotExist(filename);
                bdd.getFactory().save(filename, bdd);
            } catch (IOException ex) {
            }
        }
    }
}
