/*
 * Gamer, a tool for finding optimal plans
 * Copyright (C) 2007-2012 by Peter Kissmann
 * modified by Alvaro Torralba (2012-2013)
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */

package pddl2bdd;

import com.beust.jcommander.IStringConverter;
import java.io.File;
import pddl2bdd.varOrder.VarOrderingDefault;
import pddl2bdd.varOrder.VarOrderingLoad;
import pddl2bdd.varOrder.VarOrderingOpt;
import pddl2bdd.varOrder.VariableOrdering;

/**
 * Read the parameter for variable ordering. 
  * @author Peter Kissmann and Alvaro Torralba
 */
public class VarOrderConverter implements IStringConverter<VariableOrdering> {
    @Override
    public VariableOrdering convert(String value) {
        if (value.startsWith("def")) {
            return new VarOrderingDefault();
        }else if (value.equals("opt")) {
            return new VarOrderingOpt();
        } else if (new File(value).exists()) {
            return new VarOrderingLoad(value);
        }
        return new VarOrderingOpt();
    }
}