/*
 * Gamer, a tool for finding optimal plans
 * Copyright (C) 2007-2012 by Peter Kissmann
 * modified by Alvaro Torralba (2012-2013)
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */
package pddl2bdd.transition;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import net.sf.javabdd.BDD;
import pddl2bdd.problem.NAryVariable;

/**
 * This class allows for disjunctive preconditions over variables. 
 * It allows taking into account invariants over different variables 
 * (these are detected by the problem mutexes)
 * 
 * @author Peter Kissmann and Alvaro Torralba
 */
public class Precondition {
    
    ArrayList <NAryVariable> values;
    BDD preBDD;
    BDD effBDD;

    public Precondition (Precondition other){
        this.values = new ArrayList<NAryVariable>();
        this.values.addAll(other.values);
    }
    
    public Precondition(ArrayList<NAryVariable> values) {
        this.values = values;
    }

    public Precondition(NAryVariable value) {
        this.values = new ArrayList<NAryVariable>();
        this.values.add(value);
    }
    
    public BDD getPreBDD(){
        if(this.values.size() == 1){
            return this.values.get(0).getPreBDD();
        }else if (this.preBDD == null){
            this.preBDD = this.values.get(0).getPreBDD().id();
            for(int i = 1; i < this.values.size(); i++){
                BDD tmp = this.preBDD;
                this.preBDD = tmp.or(this.values.get(i).getPreBDD());
                tmp.free();
            }
        }
        return this.preBDD;
    }
    
    public BDD getEffBDD(){
        if(this.values.size() == 1){
            return this.values.get(0).getEffBDD();
        }else if (this.effBDD == null){
            this.effBDD = this.values.get(0).getEffBDD().id();
            for(int i = 1; i < this.values.size(); i++){
                BDD tmp = this.effBDD;
                this.effBDD = tmp.or(this.values.get(i).getEffBDD());
                tmp.free();
            }
        }
        return this.effBDD;
    }
    
    public void cleanup(){
        if(preBDD != null) preBDD.free();
        if(effBDD != null) effBDD.free();
    }

    /*ArrayList<Precondition> getMutexes(Mutexes mutexes) {
        mutexes.getMutexBDDs();
    }*/

     public Set<Integer> getPrePositiveVars() {
         if(this.values.size () == 1){
            return this.values.get(0).getPrePositiveVars();
         }else{
             return new HashSet<Integer>();
         }
    }

    public Set<Integer> getPreNegativeVars() {
        if(this.values.size () == 1){
            return this.values.get(0).getPreNegativeVars();
         }else{
             return new HashSet<Integer>();
         }
    }
    
    public boolean isSingleValue() {
        return this.values.size() == 1;
    }

    public ArrayList<NAryVariable> getValues() {
        return values;
    }

    @Override
    public String toString() {
        return this.values.toString();
    }
    
    
    
    
}
