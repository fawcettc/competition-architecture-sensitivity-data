/*
 * Gamer, a tool for finding optimal plans
 * Copyright (C) 2007-2012 by Peter Kissmann
 * modified by Alvaro Torralba (2012-2013)
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */
package pddl2bdd.transition;

import java.util.ArrayList;
import net.sf.javabdd.BDD;
import pddl2bdd.problem.NAryVariable;

/**
 * Represents an nAryVariable possibly negated. It may also represent the zero
 * or one literal (unreachable or static facts) in case that asign is null.
 *
 * @author Peter Kissmann and Alvaro Torralba
 */
public class Assign {

    private BDD zeroBDD, oneBDD;
    private NAryVariable asign;
    private boolean negated;

    public Assign(NAryVariable asign) {
        this.asign = asign;
        this.negated = false;
    }

    public Assign(boolean isUnreachable, BDD zeroBDD, BDD oneBDD) {
        this.asign = null;
        this.negated = isUnreachable;
        this.zeroBDD = zeroBDD;
        this.oneBDD = oneBDD;
    }

    public void not() {
        negated = !negated;
    }

    public BDD getPre(BDD bdd) {
        if (this.asign == null) {
            return (this.negated ? zeroBDD : oneBDD);
        }
        if (this.negated) {
            return bdd.and(this.asign.getPreBDDNegated());
        } else {
            return bdd.and(this.asign.getPreBDD());
        }
    }

    BDD getBDD() {
        if (this.asign == null) {
            return (this.negated ? zeroBDD : oneBDD);
        }
        if (this.negated) {
            return asign.getPreBDD().not();
        } else {
            return asign.getPreBDD();
        }
    }

    @Override
    public String toString() {
        if (this.asign == null) {
            return this.negated ? "unreachable" : "static_fact";
        }
        if (negated) {
            return "not " + asign.toString();
        } else {
            return asign.toString();
        }
    }

    public boolean isNegated() {
        return this.negated;
    }

    public ArrayList<NAryVariable> getNAryVariables() {
        if (negated) {
            return this.asign.getOthers();
        } else {
            ArrayList<NAryVariable> res = new ArrayList<NAryVariable>();
            res.add(this.asign);
            return res;
        }
    }

    public NAryVariable getNAryVariable() {
        return this.asign;
    }
    
    public boolean isUnreachable(){
        return this.asign == null && this.negated;
    }
    
    public boolean isStaticFact(){
        return this.asign == null && !this.negated;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Assign) {
            Assign other = (Assign) o;
            if(this.negated != other.negated) {
                return false;
            }
            return this.asign == other.asign ||
                    (this.asign != null && other.asign != null && asign.equals(other.asign));
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + (this.asign != null ? this.asign.hashCode() : 0);
        hash = 67 * hash + (this.negated ? 1 : 0);
        return hash;
    }
}
