/*
 * Gamer, a tool for finding optimal plans
 * Copyright (C) 2007-2012 by Peter Kissmann
 * modified by Alvaro Torralba (2012-2013)
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */
package pddl2bdd.transition;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import net.sf.javabdd.BDDFactory;
import pddl2bdd.problem.Action;
import pddl2bdd.problem.Problem;
import pddl2bdd.problem.VariablePartitioning;
import pddl2bdd.util.Log;

/**
 * 
 * @author Peter Kissmann and Alvaro Torralba
 */
public abstract class TManagerBuilder {
    
    protected BDDFactory factory;
    protected VariablePartitioning variables;

    protected int maxCost; // maximal action-cost
     // list of all possible actions (resp their names)
    protected HashMap<Integer, List<String>> actionNames;
    protected LinkedList<Integer> actionCosts;
    protected boolean zeroCostOperators;

    public BDDFactory getFactory() {
        return this.factory;
    }

    public abstract void cleanup();

    public TransitionManager getReconstructTransitionManager(boolean fw) {
        return new TManagerReconstruct(this, variables, fw);
    }
    
    public static enum TManagerType {TTree, TList};
    
   

    public TManagerBuilder(Problem problem, VariablePartitioning variables){
        this.factory = variables.getFactory();
        this.variables = variables;

        actionNames = new HashMap<Integer, List<String>>();

        actionCosts = new LinkedList<Integer> ();
	Log.getLog().log("   building transition relation ...");
        Log.getLog().log("   number of actions: " +  problem.getActions().size());

	this.maxCost = -1;
	for(Action action : problem.getActions()){
	    int cost = action.getCost();
	    if (!actionCosts.contains(cost)) {
		actionCosts.add(cost);
		actionNames.put(cost, new ArrayList<String>());
	    }
	    if (cost > maxCost)
		maxCost = cost;
	}
	Collections.sort(actionCosts);
        this.zeroCostOperators = false;
	if (actionCosts.get(0) == 0){
            this.zeroCostOperators = true;
	    actionCosts.remove(0);
        }

	for(Action action : problem.getActions()){
	    actionNames.get(action.getCost()).add(action.getName());
	}
    }

    /**
     * Initialize information about the transitions, having into account the
     * abstractions
     *
     * @param emptyPartitions if it is empty then there is no abstraction
     */
    public abstract void init(Problem problem, List<Integer> emptyPartitions,
            String usedActionsFilename);

    public void init(Problem problem){
        this.init(problem, new ArrayList<Integer>(), null);
    }

    public abstract TransitionManager getTransitionManager(boolean forward);



    public int getMaxCost() {
        return maxCost;
    }

    public List<Integer> getActionCosts() {
        return actionCosts;
    }

    public HashMap<Integer, List<String>> getActionNames() {
        return actionNames;
    }

    public boolean hasZeroCostOperators() {
        return this.zeroCostOperators;
    }

    public Iterator<Integer> getDescendingActionCosts() {
        return this.actionCosts.descendingIterator();
    }

    public VariablePartitioning getVariables() {
        return variables;
    }
}
