/*
 * Gamer, a tool for finding optimal plans
 * Copyright (C) 2007-2012 by Peter Kissmann
 * modified by Alvaro Torralba (2012-2013)
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */
package pddl2bdd.transition;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import net.sf.javabdd.BDD;
import net.sf.javabdd.BDDPairing;
import net.sf.javabdd.BDDVarSet;
import pddl2bdd.problem.Mutexes;
import pddl2bdd.problem.NAryVariable;
import pddl2bdd.problem.VariablePartitioning;
import pddl2bdd.util.Log;

/**
 * Class that represents a transition, including preconditions, effects and BDD
 *
 * @author Peter Kissmann and Alvaro Torralba
 */
public class Transition {

    static int transitionsID = 0;
    int id;
    VariablePartitioning variables;  //Var partitioning of the problem
    String name;                     //Action name
    int cost;                        // Action cost
    ArrayList<NAryVariable> prevail; // Fluents in prevail
    ArrayList<PrePost> prepost;      // Modified fluents
    ArrayList<NAryVariable> post;    // Effects without precondition
    BDD effect;                      // 
    BDD invertedEffect;              //
    BDD precondition;                //
    BDD invertedPrecondition;        //
    BDDVarSet effectVars, invertedEffectVars; //The same than existVars and existsBwVars
    BDDVarSet existVars, existBwVars;//Effect variables in fw and bw search
    BDDPairing replaceVars, replaceVarsBw; //Swap variables in fw and bw search
    /*
     * Computing the total BDD for a transition is expensive:
     * we only compute it in demand
     */
    private BDD totalBDD;
    //For negated preconditions. Forces to check the precondition
    private boolean forcePrecondition;
    //For disjointed transitions. If it is true, then we must use totalBDD
    private boolean forceBDDRelprod;
    //Array of the index of binary variables with positive and negative preconditions
    Set<Integer> binaryPosPre, binaryPosPreInv, binaryNegPre, binaryNegPreInv;

    public Transition(Transition t1, Transition t2, long maxTime, int maxTransistionSize) throws MaxSizeException {
        if (t1.cost != t2.cost) {
            Log.getLog().log(Level.SEVERE, "Error: trying to apply disjunction of operators with different cost: " + t1.cost + " " + t2.cost);
            throw new NullPointerException();
        }

        this.id = transitionsID++;
        this.name = t1.name + " " + t2.name;
        this.variables = t1.variables;
        this.cost = t1.cost;
        this.effectVars = t1.effectVars.union(t2.effectVars);
        this.invertedEffectVars = t1.invertedEffectVars.union(t2.invertedEffectVars);

        //Log.getLog().log("Disjunction of " + transitions.size() + " transitions");
        int[] t1Vars = t1.effectVars.toArray();
        Arrays.sort(t1Vars);
        BDD t1TotalBDD = t1.getTotalBDD().id();
        int[] t2Vars = t2.effectVars.toArray();
        Arrays.sort(t2Vars);
        BDD t2TotalBDD = t2.getTotalBDD().id();

        for (int var : this.effectVars.toArray()) {
            if (Arrays.binarySearch(t1Vars, var) < 0) {
                BDD tmp = t1TotalBDD;
                BDD biimpBDD = this.variables.getBDDVar(var).biimp(this.variables.getBDDVar(var + 1));
                t1TotalBDD = t1TotalBDD.and(biimpBDD);
                biimpBDD.free();
                tmp.free();
            }
            if (Arrays.binarySearch(t2Vars, var) < 0) {
                BDD tmp = t2TotalBDD;
                BDD biimpBDD = this.variables.getBDDVar(var).biimp(this.variables.getBDDVar(var + 1));
                t2TotalBDD = t2TotalBDD.and(biimpBDD);
                biimpBDD.free();
                tmp.free();
            }
        }
        long maxNewNodes = maxTransistionSize;// - t2TotalBDD.nodeCount();
        totalBDD = t1TotalBDD.or(t2TotalBDD, maxTime, maxNewNodes);
        t1TotalBDD.free();
        t2TotalBDD.free();
        if (totalBDD == null || this.totalBDD.nodeCount() > maxTransistionSize) {
            this.cleanup();
            throw new MaxSizeException();
        }
        //Log.getLog().log("Unified transitions: " + this.totalBDD.nodeCount() + " max: " + maxTransistionSize);

        this.existVars = t1.existVars.union(t2.existVars);
        this.existBwVars = t1.existBwVars.union(t2.existBwVars);

        this.replaceVars = variables.getPairing(this.existVars, true);
        this.replaceVarsBw = variables.getPairing(this.existBwVars, false);
        this.forceBDDRelprod = true;
        this.forcePrecondition = true;

        this.binaryNegPre = new HashSet<Integer>(t1.binaryNegPre);
        this.binaryNegPre.retainAll(t2.binaryNegPre);
        this.binaryNegPreInv = new HashSet<Integer>(t1.binaryNegPreInv);
        this.binaryNegPreInv.retainAll(t2.binaryNegPreInv);
        this.binaryPosPre = new HashSet<Integer>(t1.binaryPosPre);
        this.binaryPosPre.retainAll(t2.binaryPosPre);
        this.binaryPosPreInv = new HashSet<Integer>(t1.binaryPosPreInv);
        this.binaryPosPreInv.retainAll(t2.binaryPosPreInv);

        this.prevail = new ArrayList<NAryVariable>(t1.prevail);
        this.prevail.addAll(t2.prevail);
        this.prepost = new ArrayList<PrePost>(t1.prepost);
        this.prepost.addAll(t2.prepost);
        this.post = new ArrayList<NAryVariable>(t1.post);
        this.post.addAll(t2.post);
    }

    public Transition(Transition t) {
        this.id = t.id;
        this.variables = t.variables;
        this.name = t.name;
        this.prevail = t.prevail;
        this.prepost = t.prepost;
        this.post = t.post;
        this.cost = t.cost;

        this.effect = t.effect.id();
        this.effectVars = t.effectVars.id();
        this.invertedEffect = t.invertedEffect.id();
        this.invertedEffectVars = t.invertedEffectVars.id();
        this.precondition = t.precondition.id();
        this.invertedPrecondition = t.invertedPrecondition.id();
        this.existVars = t.existVars.id();
        this.existBwVars = t.existBwVars.id();
        this.replaceVars = t.replaceVars;
        this.replaceVarsBw = t.replaceVarsBw;

        this.forcePrecondition = t.forcePrecondition;
        this.forceBDDRelprod = t.forceBDDRelprod;

        if (t.totalBDD != null) {
            this.totalBDD = t.totalBDD.id();
        }
        this.binaryNegPre = new HashSet<Integer>(t.binaryNegPre);
        this.binaryPosPre = new HashSet<Integer>(t.binaryPosPre);
        this.binaryNegPreInv = new HashSet<Integer>(t.binaryNegPreInv);
        this.binaryPosPreInv = new HashSet<Integer>(t.binaryPosPreInv);
    }

    public Transition(VariablePartitioning variables, String name, int cost,
            Set<NAryVariable> p, Set<NAryVariable> e) {
        this.id = transitionsID++;
        this.variables = variables;
        this.name = name;
        this.cost = cost;

        this.prepost = new ArrayList<PrePost>();
        this.prevail = new ArrayList<NAryVariable>();
        this.post = new ArrayList<NAryVariable>();

        this.totalBDD = null;
        this.forcePrecondition = false;
        this.forceBDDRelprod = false;

        HashMap<Integer, NAryVariable> variablesP = new HashMap<Integer, NAryVariable>();

        for (NAryVariable pre : p) {
            variablesP.put(pre.getVariable(), pre);
        }
        for (NAryVariable eff : e) {
            int var = eff.getVariable();
            if (variablesP.containsKey(var)) {
                NAryVariable pre = variablesP.remove(var);
                if (pre.equals(eff)) {
                    this.prevail.add(pre);
                } else {
                    this.prepost.add(new PrePost(pre, eff));
                }
            } else {
                this.post.add(eff);
            }
        }
        for (NAryVariable pre : variablesP.values()) {
            this.prevail.add(pre);
        }

        this.effect = variables.getOneBDD().id();
        this.effectVars = variables.getFactory().emptySet();
        this.invertedEffect = variables.getOneBDD().id();
        this.invertedEffectVars = variables.getFactory().emptySet();
        this.precondition = variables.getOneBDD().id();
        this.invertedPrecondition = variables.getOneBDD().id();
        this.existVars = variables.getFactory().emptySet();
        this.existBwVars = variables.getFactory().emptySet();

        for (NAryVariable eff : post) { //Only effects
            this.effect = and(effect, eff.getPreBDD());
            this.effectVars = union(this.effectVars, eff.getPreVars());
            this.existVars = union(this.existVars, eff.getPreVars());
            this.invertedEffectVars = union(invertedEffectVars, eff.getPreVars());
            this.existBwVars = union(existBwVars, eff.getEffVars());
            //The inverted effect just does not  know the new value, so it does only exists the variables.
            this.invertedPrecondition = and(invertedPrecondition, eff.getPreBDD());
        }

        for (PrePost pp : prepost) { //Variables with precondition and effect: symetric
            this.effect = and(effect, pp.post.getPreBDD());
            this.effectVars = union(effectVars, pp.post.getPreVars());
            this.existVars = union(existVars, pp.post.getPreVars());
            this.invertedEffect = and(invertedEffect, pp.pre.getPreBDD());
            this.invertedEffectVars = union(invertedEffectVars, pp.pre.getPreVars());
            this.existBwVars = union(existBwVars, pp.pre.getEffVars());
            this.precondition = and(precondition, pp.pre.getPreBDD());
            this.invertedPrecondition = and(invertedPrecondition, pp.post.getPreBDD());
        }

        for (NAryVariable pre : prevail) {
            //this.invertedEffect = and(invertedEffect, pre.getPreBDD());
            //this.invertedEffectVars = union(invertedEffectVars, pre.getPreVars());
            this.invertedPrecondition = and(invertedPrecondition, pre.getPreBDD());
            this.precondition = and(precondition, pre.getPreBDD());
        }

        this.replaceVars = variables.getPairing(this.existVars, true);
        this.replaceVarsBw = variables.getPairing(this.existBwVars, false);

        this.binaryNegPre = new HashSet<Integer>();
        this.binaryNegPreInv = new HashSet<Integer>();
        this.binaryPosPre = new HashSet<Integer>();
        this.binaryPosPreInv = new HashSet<Integer>();

        for (NAryVariable pre : this.prevail) {
            //Prevail variables affect both search directions
            this.binaryPosPre.addAll(pre.getPrePositiveVars());
            this.binaryNegPre.addAll(pre.getPreNegativeVars());

            this.binaryPosPreInv.addAll(pre.getPrePositiveVars());
            this.binaryNegPreInv.addAll(pre.getPreNegativeVars());
            /*if (!pre.isSingleValue()) {
             this.forcePrecondition = true;
             }*/
        }

        for (PrePost pp : this.prepost) {
            this.binaryPosPre.addAll(pp.pre.getPrePositiveVars());
            this.binaryNegPre.addAll(pp.pre.getPreNegativeVars());

            this.binaryPosPreInv.addAll(pp.post.getPrePositiveVars());
            this.binaryNegPreInv.addAll(pp.post.getPreNegativeVars());
        }

        for (NAryVariable eff : this.post) {
            this.binaryPosPreInv.addAll(eff.getPrePositiveVars());
            this.binaryNegPreInv.addAll(eff.getPreNegativeVars());
        }
    }

    public void edeletion(Mutexes mutexes, TTreeBuilder.EdeleteType type) {
        switch (type) {
            case PRESERVE:
                this.edeletionPreserve(mutexes);
                break;
            case TOTAL:
                //this.edeletionTotal(mutexes, fw, emptyPartitions);
                break;
            case BDD:
            case BDD_RESTRICT:
            case BDD_NP_AND:
                //this.edeletionBDD(mutexes, fw, emptyPartitions, type);
                break;
        }

    }

    /* private void edeletionBDD(Mutexes mutexes, boolean fw,
     List<Integer> emptyPartitions, TTreeBuilder.EdeleteType type) {
     this.id = transitionsID++; //Now, it is a different transition
     this.getTotalBDDBiimp();
     System.exit(-1);

     for (BDD mutexBDD : mutexes.getMutexBDDs()) {
     BDD tmp = this.totalBDD;
     BDD tmpMutex = mutexBDD.not();
     if(fw){
     BDD aux = tmpMutex;
     tmpMutex = mutexBDD.replace(replaceVars);
     aux.free();
     }
     switch (type) {
     case BDD:
     this.totalBDD = this.totalBDD.and(tmpMutex);
     break;
     case BDD_NP_AND:
     this.totalBDD = this.totalBDD.nonPollutingAnd(tmpMutex, 0);
     break;
     case BDD_RESTRICT:
     this.totalBDD = this.totalBDD.restrict(tmpMutex);
     break;
     }
     tmpMutex.free();
     tmp.free();
     //tmp = this.totalBDD;
     //BDD tmpMutex2 =tmpMutex.replace(variables.getS2sp());
     //this.totalBDD = this.totalBDD.and(tmpMutex2);
     //tmp.free();
     //tmpMutex2.free();
     //tmpMutex.free();
     }

     if (this.totalBDD.isZero()) {
     Log.getLog().log("An operator is invalid with mutexes: " + this.getName());
     System.exit(-1);
     }
     }*/
    /**
     * This edeletion guarantees that no mutex is generated during the pre-image
     * computation, assuming that the input set of states does not have any
     * mutex.
     */
    private void edeletionPreserve(Mutexes mutexes) {
        Log.getLog().log("Adding edeletion preserve to: " + this.getName() + " with mutexes " + (mutexes.isFw() ? "fw" : "bw"));

        this.id = transitionsID++; //Now, it is a different transition
        //Log.getLog().log("Starting edeletion preserve to: " + this);
        ArrayList<BDD> mutexBDDs = new ArrayList<BDD>();

        for (PrePost pp : this.prepost) {
            //In regression, we are making true pp.pre
            //So we must negate everything of these.
            Log.getLog().log("Adding mutexes of pre: " + pp.pre);
            if (!mutexes.isFw()) {
                BDD aux = mutexes.getMutexBDD(pp.pre);
                if (!aux.isZero()) {
                    mutexBDDs.add(aux.id());
                }
                aux = mutexes.getExactlyOneBDD(pp.post);
                if (!aux.isZero()) {
                    mutexBDDs.add(aux.id());
                }
            } else {
                BDD aux = mutexes.getMutexBDD(pp.post);
                BDDPairing pairing = variables.getPairing(this.existVars, false);
                if (!aux.isZero()) {
                    mutexBDDs.add(aux.replace(pairing));
                }
            }
        }

        for (NAryVariable po : this.post) {
            //Log.getLog().log("Post effect over: " + this.variables.getPartitionedVariables().get(variable));
            if (!mutexes.isFw()) {
                //We have a post effect over this variable. 
                //That means that every previous value is possible in regression
                int variable = po.getVariable();
                //for each value of the variable
                for (NAryVariable fluent : this.variables.getnAryVariables(variable)) {
                    //Log.getLog().log("Adding mutexes of: " + fluent); 
                    BDD aux = mutexes.getMutexBDD(fluent);
                    if (!aux.isZero()) {
                        mutexBDDs.add(aux.id());
                    }
                }
                BDD aux = mutexes.getExactlyOneBDD(po);
                if (!aux.isZero()) {
                    mutexBDDs.add(aux.id());
                } 
            } else {
                BDD aux = mutexes.getMutexBDD(po);
                BDDPairing pairing = variables.getPairing(this.existVars, false);
                if (!aux.isZero()) {
                    mutexBDDs.add(aux.replace(pairing));
                }
            }
        }

        BDD before = this.getTotalBDD().id();
        if (this.totalBDD.isZero()) {
            Log.getLog().log(Level.INFO, "Op no applicable before edeletion: " + this);
            before.free();
            return;
        }

        for (BDD bdd : mutexBDDs) {
            BDD tmp = this.totalBDD;
            BDD tmp2 = bdd.not();
            bdd.free(); //Need to free the bdd comming from the replace

            this.totalBDD = tmp.and(tmp2);
            tmp2.free();
            tmp.free();
        }

        if (mutexes.isFw() && !before.equals(this.totalBDD)) {
            Log.getLog().log("Before: " + before.nodeCount() + " After: " + this.totalBDD.nodeCount());
        }
        before.free();
        if (this.totalBDD.isZero()) {
            Log.getLog().log(Level.INFO, "Op no applicable after edeletion: " + this);
        }
    }

    /*private void edeletionTotal(Mutexes mutexes, boolean fw,
     List<Integer> emptyPartitions) {
     this.id = transitionsID++; //Now, it is a different transition
     //Log.getLog().log("Starting edeletion preserve to: " + this);
     ArrayList<BDD> mutexPre = new ArrayList<BDD>();
     ArrayList<BDD> mutexPost = new ArrayList<BDD>();
     System.exit(-1);
     for (NAryVariable pre : this.prevail) {
     mutexPre.add(mutexes.getMutexBDD(pre));
     mutexPost.add(mutexes.getMutexBDD(pre));
     }
     for (PrePost pp : this.prepost) {
     //In regression, we are making true pp.pre
     //So we must negate everything of these.
     mutexPre.add(mutexes.getMutexBDD(pp.pre));
     mutexPost.add(mutexes.getMutexBDD(pp.post));
     }

     for (NAryVariable po : this.post) {
     //We have a post effect over this variable. 
     //That means that every previous value is possible
     int variable = po.getVariable();
     //for each value of the variable
     mutexPost.add(mutexes.getMutexBDD(po));
     for (NAryVariable fluent : this.variables.getnAryVariables(variable)) {
     mutexPre.add(mutexes.getMutexBDD(fluent));
     }
     }

     this.getTotalBDDBiimp();
     //Log.getLog().log("Adding edeletion total to: " + this);
     for (BDD bdd : mutexPre) {
     BDD tmp = this.totalBDD;
     BDD tmp2 = bdd.not();
     this.totalBDD = this.totalBDD.and(tmp2);
     tmp2.free();
     tmp.free();
     }

     for (BDD bdd : mutexPost) {
     BDD tmp = bdd.not();
     BDD tmp2 = tmp.replace(this.variables.getS2sp());

     BDD tmp3 = this.totalBDD;
     this.totalBDD = this.totalBDD.and(tmp2);
     //tmp.free();
     tmp2.free();
     tmp3.free();
     }

     //Log.getLog().log("Done");
     if (this.totalBDD.isZero()) {
     Log.getLog().log("Op no applicable: " + this);
     System.exit(-1);
     }
     }*/
    private BDD and(BDD bdd1, BDD bdd2) {
        BDD res = bdd1.and(bdd2);
        bdd1.free();
        return res;
    }

    private BDDVarSet union(BDDVarSet bdd1, BDDVarSet bdd2) {
        BDDVarSet res = bdd1.union(bdd2);
        bdd1.free();
        return res;
    }

    public boolean hasPrecondition(boolean forward, int var, boolean negated) {
        if (forward) {
            if (negated) {
                return this.binaryNegPre.contains(var);
            } else {
                return this.binaryPosPre.contains(var);
            }
        } else {
            if (negated) {
                return this.binaryNegPreInv.contains(var);
            } else {
                return this.binaryPosPreInv.contains(var);
            }
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Transition other = (Transition) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 31 * hash + this.id;
        return hash;
    }

    public BDD getPrecondition(boolean forward) {
        if (forward) {
            return precondition;
        } else {
            return invertedPrecondition;
        }
    }

    /**
     * Compute the image
     *
     * @return
     */
    public BDD relprodExists(BDD from, boolean forward, long MAX_TIME, long MAX_NODES) {
        if (forceBDDRelprod) {
            return this.relprodBDD(from, forward, MAX_TIME, MAX_NODES);
        }
        BDD tmp = from.exist(this.getEffectVars(forward));
        BDD res = tmp.and(this.getEffect(forward));
        tmp.free();

        return res;
    }

    /**
     * Compute conjuncton with the effects
     *
     * @return
     */
    public BDD relprodWoExists(BDD from, boolean forward, long MAX_TIME, long MAX_NODES) {
        if (forceBDDRelprod) {
            return this.relprodBDD(from, forward, MAX_TIME, MAX_NODES);
        }
        return from.and(this.getEffect(forward));
    }

    /**
     * Compute the image as relational product with a BDD.
     *
     * @return
     */
    public BDD relprodBDD(BDD from, boolean forward, long MAX_TIME, long MAX_NODES) {
        this.getTotalBDD();

        if (forward) {
            BDD tmp;
            if (MAX_TIME == Long.MAX_VALUE) {
                tmp = this.totalBDD.relprod(from, this.getExistVars(forward));
            } else {
                tmp = this.totalBDD.relprod(from,
                        this.getExistVars(forward), MAX_TIME - System.currentTimeMillis(), MAX_NODES);
            }

            if (tmp == null) {
                return null;
            }
            BDD res = tmp.replace(this.getReplaceVars(forward));
            tmp.free();

            return res;
        } else {
            BDD bdd = from.replace(this.getReplaceVars(forward));
            BDD res;
            if (MAX_TIME == Long.MAX_VALUE) {
                res = this.totalBDD.relprod(bdd, this.getExistVars(forward));
            } else {
                res = this.totalBDD.relprod(bdd,
                        this.getExistVars(forward),
                        MAX_TIME - System.currentTimeMillis(), 0);
            }
            bdd.free();
            return res;
        }
    }

    public int getCost() {
        return cost;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return this.cost + ": " + this.name + "(" + this.prepost + ", " + this.prevail + ", {} => " + this.post + ")";
    }

    public BDD getEffect() {
        return effect;
    }

    public BDD getInvertedEffect() {
        return invertedEffect;
    }

    public BDDVarSet getEffectVars() {
        return effectVars;
    }

    public BDD getPrecondition() {
        return precondition;
    }

    public BDDVarSet getInvertedEffectVars() {
        return invertedEffectVars;
    }

    public final void cleanup() {
        if (effect != null) {
            this.effect.free();
        }
        if (invertedEffect != null) {
            this.invertedEffect.free();
        }
        if (effectVars != null) {
            this.effectVars.free();
        }
        if (invertedEffectVars != null) {
            this.invertedEffectVars.free();
        }
        if (precondition != null) {
            this.precondition.free();
        }
        if (invertedPrecondition != null) {
            this.invertedPrecondition.free();
        }
        if (this.existVars != null) {
            this.existVars.free();
            this.existBwVars.free();
        }

        if (totalBDD != null) {
            totalBDD.free();
            totalBDD = null;
        }
    }

    public final BDD getTotalBDD() {
        if (totalBDD == null) {
            BDD replacedEffect = effect.replace(variables.getS2sp());
            totalBDD = precondition.and(replacedEffect);
            replacedEffect.free();
        }

        return totalBDD;
    }

    public final BDD getTotalBDDBiimp() {
        if (totalBDD != null) {
            totalBDD.free();
        }
        BDD replacedEffect = effect.replace(variables.getS2sp());
        totalBDD = precondition.and(replacedEffect);
        replacedEffect.free();
        int numVars = this.variables.numBinaryVariables();
        int[] effVars = this.effectVars.toArray();
        Arrays.sort(effVars);

        // Log.getLog().log(Arrays.toString(effVars));
        for (int var = numVars - 2; var >= 0; var -= 2) {
            if (Arrays.binarySearch(effVars, var) < 0) {
                BDD tmp = this.totalBDD;
                //       Log.getLog().log("Biimp "+ var);
                BDD biimpBDD = this.variables.getBDDVar(var).biimp(this.variables.getBDDVar(var + 1));
                this.totalBDD = this.totalBDD.and(biimpBDD);
                biimpBDD.free();
                tmp.free();
            }
        }

        this.effectVars = this.variables.getCubep().id();
        this.invertedEffectVars = this.variables.getCube().id();
        this.existVars = this.variables.getCube().id();
        this.existBwVars = this.variables.getCubep().id();

        this.replaceVars = this.variables.getS2sp();
        this.replaceVarsBw = this.variables.getSp2s();
        return totalBDD;
    }

    public boolean isForceBDDRelprod() {
        return forceBDDRelprod;
    }

    public boolean isForcePrecondition() {
        return forcePrecondition;
    }

    public int getId() {
        return id;
    }

    public VariablePartitioning getVariables() {
        return variables;
    }

    public BDDVarSet getEffectVars(boolean forward) {
        if (forward) {
            return this.effectVars;
        } else {
            return this.invertedEffectVars;
        }
    }

    public BDD getEffect(boolean forward) {
        if (forward) {
            return this.effect;
        } else {
            return this.invertedEffect;
        }
    }

    public BDDVarSet getExistVars(boolean forward) {
        if (forward) {
            return this.existVars;
        } else {
            return this.existBwVars;
        }
    }

    public BDDPairing getReplaceVars(boolean forward) {
        if (forward) {
            return this.replaceVars;
        } else {
            return this.replaceVarsBw;
        }
    }

    public Set<Integer> getEffectVarsSet() {
        HashSet<Integer> listVars = new HashSet<Integer>();
        for (int var : this.effectVars.toArray()) {
            listVars.add(var);
        }
        return listVars;
    }

    public Set<Integer> getPreVarsSet() {
        HashSet<Integer> listVars = new HashSet<Integer>();
        for (int var : this.precondition.support().toArray()) {
            listVars.add(var);
        }
        return listVars;
    }

    public Set<Integer> getVarsSet() {
        HashSet<Integer> listVars = new HashSet<Integer>();
        for (int var : this.effectVars.toArray()) {
            listVars.add(var);
        }
        for (int var : this.precondition.support().toArray()) {
            listVars.add(var);
        }
        return listVars;
    }

    public Set<Integer> getBinaryNegPre() {
        return binaryNegPre;
    }

    public Set<Integer> getBinaryNegPreInv() {
        return binaryNegPreInv;
    }

    public Set<Integer> getBinaryPosPre() {
        return binaryPosPre;
    }

    public Set<Integer> getBinaryPosPreInv() {
        return binaryPosPreInv;
    }

    class MaxSizeException extends Exception {
    }

    private class PrePost {

        NAryVariable pre;
        NAryVariable post;

        public PrePost(NAryVariable pre, NAryVariable post) {
            this.pre = pre;
            this.post = post;
        }

        @Override
        public String toString() {
            return pre + " => " + post;
        }
    }
}

/* private boolean postOverVar(int variable) {
 for (PrePost pp : this.prepost) {
 if (pp.post.getVariable() == variable) {
 return true;
 }
 }
 for (NAryVariable post : this.post) {
 if (post.getVariable() == variable) {
 return true;
 }
 }
 return false;
 }

 private boolean preOverVar(int variable) {
 for (PrePost pp : this.prepost) {
 if (pp.pre.getVariable() == variable) {
 return true;
 }
 }
 return false;
 }*/
/**
 * Transforms the transition for backward search adding the necessary mutexes
 */
/*public void edeletionAlvaro(Mutexes mutexes){
 this.id = transitionsID++; //Now, it is a different transition
    
 ArrayList <Precondition> newPrevails = new ArrayList<Precondition>();
 this.prevail = new ArrayList<Precondition>();
 //Mutex with prevail are more prevails
 for (Precondition pre : this.prevail){ 
 //newPrevails.addAll(mutexes.getMutexes(pre));
 }
    
 //Mutex with pre in prepost, cannot appear in pre
 //This means
 for (PrePost pp : this.prepost){
 //newPrevails.addAll(pp.pre.getMutexes(mutexes));
 }
 //Mutex with post in prepost, cannot appear in post
    
    
 this.totalBDD.free();
 this.totalBDD = null;
 this.getTotalBDD();
 }*/
/*public void edeletionVidal(Mutexes mutexes){
 this.id = transitionsID++; //Now, it is a different transition
 ArrayList <NAryVariable> edelEffCandidates = new ArrayList<NAryVariable>();
 ArrayList <NAryVariable> edelPreCandidates = new ArrayList<NAryVariable>();
 ArrayList <NAryVariable> edeletesPrev = new ArrayList<NAryVariable>();
 ArrayList <NAryVariable> edeletesPre = new ArrayList<NAryVariable>();
 ArrayList <NAryVariable> edeletesEff = new ArrayList<NAryVariable>();
 //Log.getLog().log("Action: " + this);
 for(Precondition pre : this.prevail){
 ArrayList <NAryVariable> mutex = mutexes.getEdeleters(pre);
 edeletesPrev.addAll(mutex);
 for (NAryVariable m : mutex){
 Log.getLog().log(pre + " add mutex with " + m.getName());
 }
 }
 for(PrePost pp : this.prepost){
 ArrayList <NAryVariable> mutex = mutexes.getEdeleters(pp.post);
 edelEffCandidates.addAll(mutex);
 for (NAryVariable m : mutex){
 Log.getLog().log(pp.post.getName() + " add mutex candidate with " + m.getName());
 }
 mutex = mutexes.getEdeleters(pp.pre);
 edelPreCandidates.addAll(mutex);
 for (NAryVariable m : mutex){
 Log.getLog().log(pp.pre.getName() + " add mutex candidate with " + m.getName());
 }
 }
 for(NAryVariable po : this.post){
 ArrayList <NAryVariable> mutex = mutexes.getEdeleters(po);
 edelEffCandidates.addAll(mutex);
 for (NAryVariable m : mutex){
 Log.getLog().log(po.getName() + " add mutex candidate with " + m.getName());
 }
 }
    
 for(NAryVariable fluent : edelPreCandidates){
 if(!this.postOverVar(fluent.getVariable())){
 Log.getLog().log("Candidate accepted because not appears on post: " + fluent.getName());
 edeletesPrev.add(fluent);
 }
 }
 for(NAryVariable fluent : edelEffCandidates){
 if(!this.preOverVar(fluent.getVariable()) && !this.postOverVar(fluent.getVariable())){
 Log.getLog().log("Candidate accepted because not appears on post nor pre: " + fluent.getName());
 edeletesPrev.add(fluent);
 }
 }
    
 this.getTotalBDD();
 for(NAryVariable fluent : edeletesPrev){
 Log.getLog().log(this.getName() + " edeletes " + fluent);
 BDD tmp = this.totalBDD;
 this.totalBDD = this.totalBDD.and(fluent.getPreBDD().not());
 tmp.free();
 tmp = this.totalBDD;
 this.totalBDD = this.totalBDD.and(fluent.getEffBDD().not());
 tmp.free();
 }
 for(NAryVariable fluent : edeletesEff){
 BDD tmp = this.totalBDD;
 this.totalBDD = this.totalBDD.and(fluent.getEffBDD().not());
 tmp.free();
 }
 for(NAryVariable fluent : edeletesPre){
 BDD tmp = this.totalBDD;
 this.totalBDD = this.totalBDD.and(fluent.getPreBDD().not());
 tmp.free();
 }
    
 if(this.totalBDD.isZero()){
 Log.getLog().log("Op no applicable: " +  this);
 System.exit(-1);
 }
 }*/
