/*
 * Gamer, a tool for finding optimal plans
 * Copyright (C) 2007-2012 by Peter Kissmann
 * modified by Alvaro Torralba (2012-2013)
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */
package pddl2bdd.transition;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.logging.Level;
import net.sf.javabdd.BDD;
import pddl2bdd.problem.VariablePartitioning;
import pddl2bdd.transition.DisjunctionTree.DTStrategy;
import pddl2bdd.transition.Transition.MaxSizeException;
import pddl2bdd.util.Log;
import pddl2bdd.util.Rnd;

/**
 * Tree organization of the operators that allows sharing some conjunctions
 * between them.
 *
 * @author Peter Kissmann and Alvaro Torralba
 */
public class TransitionTree extends TransitionManager {

    private void disj_smaller(ArrayList<Transition> trs, int maxTime, int maxSize) {
        long MAX_TIME = System.currentTimeMillis() + maxTime;
        Comparator<Transition> comp = new Comparator<Transition>() {
            @Override
            public int compare(Transition o1, Transition o2) {
                return o1.getTotalBDD().nodeCount() - o2.getTotalBDD().nodeCount();
            }
        };
        Collections.sort(trs, comp);
        long timeLeft;
        ArrayList<Transition> result = new ArrayList<Transition>();
        while (trs.size() > 1
                && (timeLeft = (MAX_TIME - System.currentTimeMillis())) > 0) {

            Transition tr1 = trs.get(0);
            Transition tr2 = trs.get(1);
            try {
                Transition merged = new Transition(tr1, tr2, timeLeft, maxSize);
                int pos = Collections.binarySearch(trs, merged, comp);
                trs.add(-pos - 1, merged);
            } catch (MaxSizeException e) {
                result.add(tr1);
                result.add(tr2);
            }
        }
        trs.addAll(result);
    }

    boolean dontShareTRs() {
        return !this.ct_order.equals(CTOrder.NO_TREE);
    }

    public enum DisjTRStrategy {

        NO_DISJ, NO_DISJ_WO_IMAGE, DISJ_DT, DISJ_CT, DISJ_SMALLER, DISJ_PRE
    }

    public enum DisjImageStrategy {

        SMALLER, DT, DISJUNCTION_ALL
    }

    public enum CTOrder {

        NO_TREE, NORMAL, INVERSE, RANDOM, DYNAMIC
    }

    private DTStrategy dt_strategy;
    private DisjImageStrategy disjImageStrategy;

    private DisjTRStrategy disjTRStrategy;
    private int maxDisjTime, maxDisjSize;

    private CTOrder ct_order;
    private int minOperatorsLeaf;
    private int maxCTDepth;

    private TTImageStatistics imgstats;
    private TTInitStatistics inistats;

    /**
     * Root node for the transition tree of non-zero cost operators and zero
     * cost operators, respectively
     */
    TTNode root, root0;

    /**
     * Disjunction trees to decide how to perform the disjunction
     */
    HashMap<Integer, DisjunctionTree> disjTrees;

    BDD zeroBDD;

    public TransitionTree(TTreeBuilder builder, VariablePartitioning variables,
            boolean forward) {
        super(builder, variables, forward);

        this.dt_strategy = DTStrategy.valueOf(builder.getDTStrategy().toUpperCase());
        this.disjImageStrategy = DisjImageStrategy.valueOf(builder.getDisjImageStrategy().toUpperCase());

        this.disjTRStrategy = DisjTRStrategy.valueOf(builder.getDisjTRStrategy().toUpperCase());
        this.maxDisjSize = builder.getMaxDisjTRSize();
        this.maxDisjTime = builder.getMaxDisjTRTime();

        this.ct_order = CTOrder.valueOf(builder.getCTOrder().toUpperCase());
        this.minOperatorsLeaf = builder.getMinOperatorsLeaf();
        this.maxCTDepth = builder.getMaxCTDepth();

        this.zeroBDD = variables.getZeroBDD();

        Log.getLog().log("Initializing transition tree.");

        //this.pairing = pairing;
        //this.varSet = varSet;
        this.disjTrees = new HashMap<Integer, DisjunctionTree>();

        ArrayList<Transition> tCost = new ArrayList<Transition>();
        ArrayList<Transition> tZero = new ArrayList<Transition>();
        HashMap<Integer, ArrayList<Transition>> builderTransitions
                = builder.getIndividualTransitions(forward);
        for (Entry<Integer, ArrayList<Transition>> tByCost : builderTransitions.entrySet()) {
            int cost = tByCost.getKey();

            //Aggregate TRs and build the DT (except in DISJ_CT strategy)
            switch (this.disjTRStrategy) {
                case DISJ_SMALLER:
                    //Before making the DTs, 
                    disj_smaller(tByCost.getValue(), this.maxDisjTime / builder.getTransitionCosts().size(), this.maxDisjSize);
                case NO_DISJ:
                case NO_DISJ_WO_IMAGE:
                case DISJ_DT:
                    //Create the DT to aggregate transitions
                    DisjunctionTree dT = new DisjunctionTree(tByCost.getValue(), this.dt_strategy);
                    if (disjTRStrategy.equals(DisjTRStrategy.DISJ_DT)) {
                        dT.unifyTransitions(this.maxDisjTime / builder.getTransitionCosts().size(), this.maxDisjSize);
                    }

                    disjTrees.put(cost, dT);
                    if (cost == 0) {
                        tZero.addAll(dT.getTransitions());
                    } else {
                        tCost.addAll(dT.getTransitions());
                    }
                    break;
                case DISJ_CT:
                    //Skip making the DT until CT is completed and aggregated. 
                    //We are going to merge operators with the conjunction tree,
                    //so we have to postpone the disjunction tree initialization
                    if (cost == 0) {
                        tZero.addAll(tByCost.getValue());
                    } else {
                        tCost.addAll(tByCost.getValue());
                    }
                    break;
                case DISJ_PRE:
                    /* MergeTransitionsPre mergeCost = new MergeTransitionsPre(variables,
                     tCost, forward, this.minOperatorsLeaf, this.maxDisjTime, this.maxDisjSize);
                     MergeTransitionsPre mergeZero = new MergeTransitionsPre(variables,
                     tZero, forward, this.minOperatorsLeaf, this.maxDisjTime, this.maxDisjSize);

                     tCost = new ArrayList<Transition>();
                     tZero = new ArrayList<Transition>();
                     for (Entry<Integer, ArrayList<Transition>> tByCost : builderTransitions.entrySet()) {
                     int cost = tByCost.getKey();
                     List<Transition> unified;
                     if (cost == 0) {
                     unified = mergeZero.merge(tByCost.getValue());
                     tZero.addAll(unified);
                     } else {
                     unified = mergeCost.merge(tByCost.getValue());
                     tCost.addAll(unified);
                     }
                     DisjunctionTree dT = new DisjunctionTree(unified, this.dt_strategy);
                     disjTrees.put(cost, dT);
                     }*/
                    break;
            }
        }

        if (Log.doExpensiveLog) {
            imgstats = new TTImageStatistics();
        }
        inistats = new TTInitStatistics();
        root = getNode(0, tCost, allVarsList(), new ArrayList<Integer>());
        if (root != null) {
            Log.getLog().log(Level.INFO, "CostTree " + (forward ? "fw" : "bw") + ": " + inistats);
        }
        inistats = new TTInitStatistics();
        root0 = getNode(0, tZero, allVarsList(), new ArrayList<Integer>());
        if (root0 != null) {
            Log.getLog().log(Level.INFO, "ZeroTree: " + (forward ? "fw" : "bw") + ": " + inistats);
        }
        if (this.disjTRStrategy.equals(DisjTRStrategy.DISJ_CT)) {
            HashMap<Integer, ArrayList<Transition>> tByCost = new HashMap<Integer, ArrayList<Transition>>();
            root.getTransitionsByCost(tByCost);
            if (root0 != null) {
                root0.getTransitionsByCost(tByCost);
            }

            for (Entry<Integer, ArrayList<Transition>> entry : tByCost.entrySet()) {
                int cost = entry.getKey();
                DisjunctionTree dT = new DisjunctionTree(entry.getValue(),
                        this.dt_strategy);

                disjTrees.put(cost, dT);
                for (Transition t : dT.getTransitions()) {
                    if (t.getCost() == 0) {
                        tZero.add(t);
                    } else {
                        tCost.add(t);
                    }
                }
            }
        }
    }

    public TransitionTree(TransitionTree other, boolean forward) {
        super(other.builder, other.variables, forward);

        this.dt_strategy = other.dt_strategy;
        this.disjImageStrategy = other.disjImageStrategy;

        this.disjTRStrategy = other.disjTRStrategy;
        this.maxDisjSize = other.maxDisjSize;
        this.maxDisjTime = other.maxDisjTime;

        this.ct_order = other.ct_order;
        this.minOperatorsLeaf = other.minOperatorsLeaf;
        this.maxCTDepth = other.maxCTDepth;

        this.zeroBDD = variables.getZeroBDD();

        Log.getLog().log("Initializing transition tree.");

        this.disjTrees = other.disjTrees;
        if (other.root != null) {
            this.root = new TTLeaf((TTLeaf) other.root, forward);
        }
        if (other.root0 != null) {
            this.root0 = new TTLeaf((TTLeaf) other.root0, forward);
        }
    }

    /**
     * Auxiliar function to initialize list of remaining vars when initializing
     * the CT.
     */
    private ArrayList<Integer> allVarsList() {
        ArrayList<Integer> trVars = new ArrayList<Integer>(variables.numBinaryVariables() / 2);
        for (int i = 0; i < variables.numBinaryVariables() / 2; i++) {
            trVars.add(i * 2);
        }
        return trVars;
    }

    @Override
    public HashMap<Integer, BDD> image(BDD from, BDD conjunct, long MAX_TIME, long MAX_NODES) {
        long imgTime = System.currentTimeMillis();
        if (Log.doExpensiveLog) {
            this.imgstats = new TTImageStatistics();
        }

        HashMap<Integer, HashMap<Integer, BDD>> res = new HashMap<Integer, HashMap<Integer, BDD>>();
        if (root != null && !root.image(from, conjunct, MAX_TIME, MAX_NODES, res)) {
            for (HashMap<Integer, BDD> bddList : res.values()) {
                for (BDD bdd : bddList.values()) {
                    bdd.free();
                }
            }
            Log.getLog().log("Returning null");
            return null;
        }

        //Log.getLog().log("Go to disjunction");
        long disjTime = System.currentTimeMillis();
        HashMap<Integer, BDD> disj_res = new HashMap<Integer, BDD>();
        for (Entry<Integer, HashMap<Integer, BDD>> entry : res.entrySet()) {
            if (!entry.getValue().isEmpty()) {
                int cost = entry.getKey();
                BDD merged = null;
                if (this.disjImageStrategy.equals(DisjImageStrategy.DISJUNCTION_ALL)) {
                    BDD[] array = entry.getValue().values().toArray(new BDD[entry.getValue().size()]);
                    merged = array[0].or(array);
                    for (BDD bdd : array) {
                        bdd.free();
                    }
                } else {
                    merged = disjTrees.get(cost).disjunction(entry.getValue(), MAX_TIME);
                }
                if (merged == null) {
                    //Log.getLog().log("RETURN NULL");
                    return null;
                }
                if (!merged.equals(zeroBDD)) {
                    disj_res.put(cost, merged);
                } else {
                    merged.free();
                }
            }
        }
        if (Log.doExpensiveLog) {
            imgstats.addDisjunctionTime(disjTime);
            if (!res.isEmpty()) {
                imgstats.addTotalTime(imgTime);
                Log.getLog().log("Non-zero  " + this.imgstats);
            }
        }

        return disj_res;
    }

    @Override
    public BDD image(int cost, BDD from, BDD conjunct, long MAX_TIME, long MAX_NODES) {
        long imgTime = System.currentTimeMillis();

        if (Log.doExpensiveLog) {
            this.imgstats = new TTImageStatistics();
        }

        HashMap<Integer, BDD> res = new HashMap<Integer, BDD>();
        boolean completed = false;
        if (cost == 0) {
            completed = root0.image(cost, from, conjunct, MAX_TIME, MAX_NODES, res);
        } else {
            completed = root.image(cost, from, conjunct, MAX_TIME, MAX_NODES, res);
        }
        if (!completed) {
            for (BDD bdd : res.values()) {
                bdd.free();
            }
            return null;
        }
        BDD res_disj = disjTrees.get(cost).disjunction(res, MAX_TIME);
        //tmp1.free();

        if (Log.doExpensiveLog) {
            imgstats.addTotalTime(imgTime);
            Log.getLog().log(cost + " transitions " + this.imgstats);
        }

        return res_disj;
    }

    /**
     * Auxiliar function for CT initialization
     *
     * @param numVar num of variable being selected (depth in CT)
     * @param vars number of variables that may be selected on this branch
     * @param trs transitions relations to be split by the CT
     * @return variable used to split transition relations
     */
    int selectVar(int numVar, ArrayList<Integer> vars, ArrayList<Transition> trs) {
        if (numVar >= this.maxCTDepth
                || trs.size() <= this.minOperatorsLeaf
                || vars.isEmpty()) {
            return -1;
        }

        switch (ct_order) {
            case NO_TREE:
                return -1;
            case NORMAL:
                return vars.get(0);
            case INVERSE:
                return vars.get(vars.size() - 1);
            case RANDOM:
                int rnd = Rnd.nextInt(vars.size());
                return vars.get(rnd);
            case DYNAMIC:
                //Select the variable that appears more often in preconditions
                int bestVar = -1;
                int bestValue = 0,
                 bestTie = 0;
                for (int v : vars) {
                    int val0 = 0, val1 = 0;
                    for (Transition t : trs) {
                        if (t.hasPrecondition(this.forward, v, false)) {
                            val0++;
                        }
                        if (t.hasPrecondition(this.forward, v, true)) {
                            val1++;
                        }
                    }
                    if (val0 < val1) { //val0 gets the larger
                        int aux = val0;
                        val0 = val1;
                        val1 = aux;
                    }
                    if (val0 > bestValue || (val0 == bestValue && val1 == bestTie)) {
                        bestVar = v;
                        bestValue = val0;
                        bestTie = val1;
                    }
                }
                return bestVar;
        }
        return -1;
    }

    /**
     *
     * @param variable
     * @param t
     * @param skippedVars list of variables that we have skipped the
     * preconditions for
     * @return
     */
    private TTNode getNode(int variable, ArrayList<Transition> trs,
            ArrayList<Integer> remainingVars, ArrayList<Integer> skippedVars) {
        if (trs.isEmpty()) {
            return null;
        }
        int var = selectVar(variable, remainingVars, trs);
        //Base case: all the variables have been included or Min_Operators_Leaf.
        if (var == -1) {
            ArrayList<Integer> skippedVarsHere = new ArrayList<Integer>(skippedVars);
            skippedVarsHere.addAll(remainingVars);
            return new TTLeaf(trs, this.forward, skippedVarsHere);
        }
        int posVar = remainingVars.indexOf(new Integer(var));
        remainingVars.remove(posVar);

        ArrayList<Transition> t0 = new ArrayList<Transition>();
        ArrayList<Transition> t1 = new ArrayList<Transition>();
        ArrayList<Transition> tX = new ArrayList<Transition>();

        for (Transition transition : trs) {
            if (transition.hasPrecondition(this.forward, var, true)) {
                t0.add(transition);
            } else if (transition.hasPrecondition(this.forward, var, false)) {
                t1.add(transition);
            } else {
                tX.add(transition);
            }
        }

        boolean skipped = false;
        if (!t0.isEmpty() && t0.size() < this.minOperatorsLeaf) {
            skipped = true;
            tX.addAll(t0);
            t0.clear();
        }
        if (!t1.isEmpty() && t1.size() < this.minOperatorsLeaf) {
            skipped = true;
            tX.addAll(t1);
            t1.clear();
        }
        ArrayList<Integer> skippedVarsX = skippedVars;
        if (skipped) {
            skippedVarsX = new ArrayList<Integer>(skippedVars);
            skippedVarsX.add(var);
        }

        TTNode res;
        if (t0.isEmpty() && t1.isEmpty()) {
            res = getNode(variable + 1, tX, remainingVars, skippedVarsX);
        } else {
            TTNode nodeX = null;
            if (!tX.isEmpty()) {
                nodeX = getNode(variable + 1, tX, remainingVars, skippedVarsX);
            }
            //If t0 or t1 are empty, we dont get their node and put null
            //so that the conjunction is never applied
            TTNode node0 = null;
            if (!t0.isEmpty()) {
                node0 = getNode(variable + 1, t0, remainingVars, skippedVars);
            }

            TTNode node1 = null;
            if (!t1.isEmpty()) {
                node1 = getNode(variable + 1, t1, remainingVars, skippedVars);
            }

            res = new TTInternal(var, node0, node1, nodeX);
        }
        remainingVars.add(posVar, var);
        return res;
    }

    @Override
    public BDD applyAction(BDD from, String action) {
        List<Transition> trs = ((TTreeBuilder) this.builder).getTransitionByName(action);
        if(trs.size() == 1) {
            return trs.get(0).relprodBDD(from, forward, Long.MAX_VALUE, 0);
        }else{
            BDD res = variables.getZeroBDD().id();
            for(Transition tr : trs){
                BDD aux = tr.relprodBDD(from, forward, Long.MAX_VALUE, 0);
                BDD aux2 = res.or(aux);
                aux.free();
                res.free();
                res = aux2;
            }
            return res;
        }
    }

    @Override
    public void free() {
        for (DisjunctionTree dT : this.disjTrees.values()) {
            dT.cleanup();
        }
        this.disjTrees.clear();
    }

    /**
     * Interface for the tree nodes
     */
    private interface TTNode {

        /**
         * Compute the image of a bdd.
         *
         * @param from bdd to apply the image
         * @param conjunct pplies a conjunction with it if is null
         * @param res for each cost, a map of transition ids to BDDs
         */
        public boolean image(BDD from, BDD conjunct, long MAX_TIME, long MAX_NODES,
                HashMap<Integer, HashMap<Integer, BDD>> res);

        /**
         * Compute the image only for operators with a particular cost.
         *
         * @param cost cost of the applied operators
         * @param from bdd to apply the image
         * @param conjunct applies a conjunction with it if is null
         * @return The result of image with conjunct
         */
        public boolean image(int cost, BDD from, BDD conjunct, long MAX_TIME, long MAX_NODES,
                HashMap<Integer, BDD> res);

        //public BDD image(String action, BDD from);
        public Set<String> getActions();

        public void getTransitionsByCost(HashMap<Integer, ArrayList<Transition>> tByCost);

        public int numTransitions();

        public void remove(Transition transitionX);
    }

    private class TTInternal implements TTNode {

        TTNode t0, t1, tx;
        Set<String> actions;
        int variable;
        BDD not_condition, yes_condition;

        public TTInternal(int var, TTNode t0, TTNode t1, TTNode tx) {
            inistats.addInternal();
            this.variable = var;
            this.t0 = t0;
            this.t1 = t1;
            this.tx = tx;
            this.actions = new HashSet<String>();
            if (t0 != null) {
                this.actions.addAll(t0.getActions());
            }
            if (t1 != null) {
                this.actions.addAll(t1.getActions());
            }
            if (tx != null) {
                this.actions.addAll(tx.getActions());
            }
            this.yes_condition = variables.getBDDVar(var);
            this.not_condition = variables.getBDDNotVar(var);
        }

        @Override
        public boolean image(BDD from, BDD conjunct, long MAX_TIME, long MAX_NODES,
                HashMap<Integer, HashMap<Integer, BDD>> res) {

            if (t0 != null) {
                if (System.currentTimeMillis() > MAX_TIME) {
                    return false;
                }
                long conjTime = System.currentTimeMillis();
                BDD bdd0 = from.and(this.not_condition);
                if (Log.doExpensiveLog) {
                    imgstats.addConjunctionTime(conjTime);
                }
                //Log.getLog().log("Partial conjunction: " + (bdd0.nodeCount() == from.nodeCount()) + " " + bdd0.nodeCount() + " " + from.nodeCount());

                if (!bdd0.equals(zeroBDD)) {
                    if (!t0.image(bdd0, conjunct, MAX_TIME, MAX_NODES, res)) {
                        bdd0.free();
                        return false;
                    }
                }
                bdd0.free();
            }
            if (t1 != null) {
                if (System.currentTimeMillis() > MAX_TIME) {
                    return false;
                }
                long conjTime = System.currentTimeMillis();
                BDD bdd1 = from.and(this.yes_condition);
                if (Log.doExpensiveLog) {
                    imgstats.addConjunctionTime(conjTime);
                }
                //Log.getLog().log("Partial conjunction: " + (bdd1.nodeCount() == from.nodeCount()) + " " + bdd1.nodeCount() + " " + from.nodeCount());
                if (!bdd1.equals(zeroBDD)) {
                    if (!t1.image(bdd1, conjunct, MAX_TIME, MAX_NODES, res)) {
                        bdd1.free();
                        return false;
                    }
                }
                bdd1.free();
            }

            if (tx != null) {
                if (System.currentTimeMillis() > MAX_TIME) {
                    return false;
                }
                if (!tx.image(from, conjunct, MAX_TIME, MAX_NODES, res)) {
                    return false;
                }
            }

            return true;
        }

        @Override
        public boolean image(int cost, BDD from, BDD conjunct, long MAX_TIME, long MAX_NODES,
                HashMap<Integer, BDD> res) {
            if (t0 != null) {
                if (System.currentTimeMillis() > MAX_TIME) {
                    return false;
                }
                long conjTime = System.currentTimeMillis();
                BDD bdd0 = from.and(this.not_condition);
                if (Log.doExpensiveLog) {
                    imgstats.addConjunctionTime(conjTime);
                }
                //Log.getLog().log("Partial conjunction: " + (bdd0.nodeCount() == from.nodeCount()) + " " + bdd0.nodeCount() + " " + from.nodeCount());

                if (!bdd0.equals(zeroBDD)) {
                    if (!t0.image(cost, bdd0, conjunct, MAX_TIME, MAX_NODES, res)) {
                        bdd0.free();
                        return false;
                    }
                }
                bdd0.free();
            }
            if (t1 != null) {
                if (System.currentTimeMillis() > MAX_TIME) {
                    return false;
                }

                long conjTime = System.currentTimeMillis();
                BDD bdd1 = from.and(this.yes_condition);
                if (Log.doExpensiveLog) {
                    imgstats.addConjunctionTime(conjTime);
                }
                //Log.getLog().log("Partial conjunction: " + (bdd1.nodeCount() == from.nodeCount()) + " " + bdd1.nodeCount() + " " + from.nodeCount());

                if (!bdd1.equals(zeroBDD)) {
                    if (!t1.image(cost, bdd1, conjunct, MAX_TIME, MAX_NODES, res)) {
                        bdd1.free();
                        return false;
                    }
                }
                bdd1.free();
            }

            if (tx != null) {
                if (System.currentTimeMillis() > MAX_TIME) {
                    return false;
                }
                if (!tx.image(cost, from, conjunct, MAX_TIME, MAX_NODES, res)) {
                    return false;
                }
            }

            return true;
        }

        @Override
        public Set<String> getActions() {
            return actions;
        }

        @Override
        public void getTransitionsByCost(HashMap<Integer, ArrayList<Transition>> tByCost) {
            if (this.t1 != null) {
                this.t1.getTransitionsByCost(tByCost);
            }
            if (this.t0 != null) {
                this.t0.getTransitionsByCost(tByCost);
            }
            if (this.tx != null) {
                this.tx.getTransitionsByCost(tByCost);
            }
        }

        @Override
        public int numTransitions() {
            int numT = 0;
            if (this.t0 != null) {
                numT += this.t0.numTransitions();
            }
            if (this.t1 != null) {
                numT += this.t1.numTransitions();
            }
            if (this.tx != null) {
                numT += this.tx.numTransitions();
            }
            return numT;
        }

        @Override
        public void remove(Transition tr) {
            if (this.t0 != null) {
                this.t0.remove(tr);
            }
            if (this.t1 != null) {
                this.t1.remove(tr);
            }
            if (this.tx != null) {
                this.tx.remove(tr);
            }
        }

        /*void aggregateTRs(long MAX_TIME) {
         HashMap<Integer, ArrayList<Transition>> transX = new HashMap<Integer, ArrayList<Transition>>();
         HashMap<Integer, ArrayList<Transition>> trans0 = new HashMap<Integer, ArrayList<Transition>>();
         HashMap<Integer, ArrayList<Transition>> trans1 = new HashMap<Integer, ArrayList<Transition>>();

         if (tx != null) {
         tx.getTransitionsByCost(transX);
         }
         if (t0 != null) {
         t0.getTransitionsByCost(trans0);
         }
         if (t1 != null) {
         t1.getTransitionsByCost(trans1);
         }

         for (int cost : transX.keySet()) {
         Transition merged = null;
         if (transX.get(cost).size() == 1) {
         Transition transitionX = transX.get(cost).get(0);

         if (trans0.containsKey(cost) && trans0.get(cost).size() == 1) {
         Transition transition0 = trans0.get(cost).get(0);
         try {
         merged = new Transition(transition0, transitionX, MAX_TIME - System.currentTimeMillis(), maxDisjSize);
         tx.remove(transitionX);
         t0.remove(transition0);
         } catch (MaxSizeException ex) {
         }
         }
         if (trans1.containsKey(cost) && trans1.get(cost).size() == 1) {
         Transition transition1 = trans1.get(cost).get(0);
         try {
         Transition aux;
         if (merged != null) {
         aux = new Transition(transition1, merged, MAX_TIME - System.currentTimeMillis(), maxDisjSize);
         } else {
         aux = new Transition(transition1, transitionX, MAX_TIME - System.currentTimeMillis(), maxDisjSize);
         tx.remove(transitionX);
         }
         t1.remove(transition1);
         merged = aux;
         } catch (MaxSizeException ex) {
         }
         }
         } else {
         if (trans0.containsKey(cost) && trans0.get(cost).size() == 1
         && trans1.containsKey(cost) && trans1.get(cost).size() == 1) {
         Transition transition0 = trans0.get(cost).get(0);
         Transition transition1 = trans1.get(cost).get(0);
         try {
         merged = new Transition(transition0,
         transition1,
         MAX_TIME - System.currentTimeMillis(), maxDisjSize);
         t1.remove(transition1);
         t0.remove(transition0);
         } catch (MaxSizeException ex) {
         }
         }
         }
         if (merged != null) {
         tx.addInX(merged);
         }
         }

         if (t0 != null && t0.numTransitions() < minOperatorsLeaf) {
         if (tx != null) {
         tx.addAll(t0);
         } else {
         tx = t0;
         }
         t0 = null;
         }

         if (t1 != null && t1.numTransitions() < minOperatorsLeaf) {
         if (tx != null) {
         tx.addAll(t1);
         } else {
         tx = t1;
         }
         t1 = null;
         }

         if (t0 == null && t0 == null) {
         return tx;
         }
         return this;
         }*/
    }

    private class TransitionInLeaf {

        Transition tr;
        boolean hasPrecondition;
        boolean useBDD;

        public TransitionInLeaf(Transition tr, ArrayList<Integer> preconditionVars) {
            this.tr = tr;
            hasPrecondition = false;
            for (int var : preconditionVars) {
                if (tr.hasPrecondition(forward, var, true)
                        || tr.hasPrecondition(forward, var, false)) {
                    hasPrecondition = true;
                    break;
                }
            }
            if (tr.isForcePrecondition()) {
                hasPrecondition = true;
            }
            this.useBDD = disjTRStrategy.equals(DisjTRStrategy.NO_DISJ)
                    || tr.isForceBDDRelprod();
        }

        private TransitionInLeaf(Transition tr) {
            this.hasPrecondition = true;// Does not matter because useBDD is true
            this.useBDD = true;
            this.tr = tr;
        }

        public Transition getTr() {
            return tr;
        }

        private BDD relprod(BDD from, boolean forward, long MAX_TIME, long MAX_NODES) {
            long iniTime = System.currentTimeMillis();
            BDD res;
            if (!useBDD) {
                if (hasPrecondition) {
                    BDD tmp = from.relprod(tr.getPrecondition(forward),
                            tr.getEffectVars(forward));
                    if (Log.doExpensiveLog) {
                        imgstats.addConjunctionTime(iniTime);
                    }
                    iniTime = System.currentTimeMillis();

                    res = tr.relprodWoExists(tmp, forward, MAX_TIME, MAX_NODES);
                    tmp.free();
                } else {
                    res = tr.relprodExists(from, forward, MAX_TIME, MAX_NODES);
                }
            } else {
                res = tr.relprodBDD(from, forward, MAX_TIME, MAX_NODES);
            }
            if (Log.doExpensiveLog) {
                imgstats.addImageTime(iniTime);
            }
            return res;
        }
    }

    private class TTLeaf implements TTNode {

        HashMap<Integer, ArrayList<TransitionInLeaf>> t;
        HashMap<String, ArrayList<TransitionInLeaf>> t_by_name;
        boolean forward;

        public TTLeaf(TTLeaf other, boolean forward) {
            this.t = other.t;
            this.t_by_name = other.t_by_name;
            this.forward = forward;
        }

        private TTLeaf(ArrayList<Transition> transitions, boolean forward,
                ArrayList<Integer> preconditionVars) {
            this.forward = forward;
            inistats.addLeaf(transitions);

            this.t = new HashMap<Integer, ArrayList<TransitionInLeaf>>();
            this.t_by_name = new HashMap<String, ArrayList<TransitionInLeaf>>();
            for (Transition tr : transitions) {
                /*if (tr.getName().equals("move.player-01.pos-11-03.pos-11-02.dir-up")) {
                 Log.getLog().log("ASD");
                 }*/
                if (!t.containsKey(tr.getCost())) {
                    t.put(tr.getCost(), new ArrayList<TransitionInLeaf>());
                }
                if (!t_by_name.containsKey(tr.getName())) {
                    t_by_name.put(tr.getName(), new ArrayList<TransitionInLeaf>());
                }

                TransitionInLeaf til = new TransitionInLeaf(tr, preconditionVars);
                this.t.get(tr.getCost()).add(til);
                this.t_by_name.get(tr.getName()).add(til);

            }
        }

        public void aggregateTRs(long MAX_TIME) {
            Log.getLog().log("Disjunction of tt leaf");
            for (int c : t.keySet()) {
                if (t.get(c).size() > 1) {
                    ArrayList<TransitionInLeaf> newTr = new ArrayList<TransitionInLeaf>();
                    ArrayList<Transition> transitions = new ArrayList<Transition>();
                    for (TransitionInLeaf tL : t.get(c)) {
                        transitions.add(tL.getTr());
                    }

                    while (transitions.size() > 1) {
                        Transition t1 = transitions.remove(0);
                        Transition t2 = transitions.remove(0);
                        try {
                            Transition nue = new Transition(t1, t2,
                                    MAX_TIME - System.currentTimeMillis(),
                                    maxDisjSize);
                            transitions.add(nue);
                            t1.cleanup();
                            t2.cleanup();
                        } catch (MaxSizeException ex) {
                            newTr.add(new TransitionInLeaf(t1));
                            newTr.add(new TransitionInLeaf(t2));
                        }
                    }

                    if (transitions.size() == 1) {
                        newTr.add(new TransitionInLeaf(transitions.get(0)));
                    }

                    if (Log.doExpensiveLog) {
                        if (newTr.size() < t.get(c).size()) {
                            Log.getLog().log("TTLeaf Disj: " + t.get(c).size() + "=>" + newTr.size());
                        }
                    }
                    t.put(c, newTr);
                }
            }
        }

        @Override
        public boolean image(BDD from, BDD conjunct, long MAX_TIME, long MAX_NODES,
                HashMap<Integer, HashMap<Integer, BDD>> result) {
            int numTransitions = 0;
            for (int cost : t.keySet()) {
                BDD tmp1;
                HashMap<Integer, BDD> res;
                if (result.containsKey(cost)) {
                    res = result.get(cost);
                } else {
                    res = new HashMap<Integer, BDD>();
                    result.put(cost, res);
                }

                for (TransitionInLeaf trLeaf : t.get(cost)) {
                    if (System.currentTimeMillis() > MAX_TIME) {
                        return false;
                    }
                    tmp1 = trLeaf.relprod(from, forward, MAX_TIME, MAX_NODES);
                    if (tmp1 == null) {
                        return false;
                    }
                    if (!tmp1.equals(zeroBDD)) {
                        numTransitions++;
                        if (conjunct == null) {
                            res.put(trLeaf.getTr().getId(), tmp1);
                        } else {
                            BDD tmp2 = tmp1.and(conjunct);
                            tmp1.free();
                            if (!tmp2.equals(zeroBDD)) {
                                res.put(trLeaf.getTr().getId(), tmp2);
                            }
                        }
                    }
                }
            }

            if (Log.doExpensiveLog) {
                imgstats.leafImage(numTransitions);
            }
            return true;
        }

        @Override
        public boolean image(int cost, BDD from, BDD conjunct,
                long MAX_TIME, long MAX_NODES, HashMap<Integer, BDD> res) {
            BDD tmp1;
            int numTransitions = 0;

            for (TransitionInLeaf trLeaf : t.get(cost)) {
                if (System.currentTimeMillis() > MAX_TIME) {
                    return false;
                }
                tmp1 = trLeaf.relprod(from, forward, MAX_TIME, MAX_NODES);
                if (tmp1 == null) {
                    return false;
                }
                if (!tmp1.equals(zeroBDD)) {
                    numTransitions++;
                    if (conjunct == null) {
                        res.put(trLeaf.getTr().getId(), tmp1);
                    } else {
                        BDD tmp2 = tmp1.and(conjunct);
                        tmp1.free();
                        if (!tmp2.equals(zeroBDD)) {
                            res.put(trLeaf.getTr().getId(), tmp2);
                        }
                    }
                }
            }
            if (Log.doExpensiveLog) {
                imgstats.leafImage(numTransitions);
            }

            return true;
        }

        /*@Override
         public BDD image(String action, BDD from) {

         BDD res = variables.getZeroBDD().id();
         for (TransitionInLeaf tr : this.t_by_name.get(action)) {
         BDD tmp = res;
         BDD resImg = tr.relprod(from, this.forward, Long.MAX_VALUE);
         res = tmp.or(resImg);
         resImg.free();
         tmp.free();
         }
         return res;
         }*/
        @Override
        public Set<String> getActions() {
            return this.t_by_name.keySet();
        }

        @Override
        public void getTransitionsByCost(HashMap<Integer, ArrayList<Transition>> tByCost) {
            for (Entry<Integer, ArrayList<TransitionInLeaf>> entry : t.entrySet()) {
                if (!tByCost.containsKey(entry.getKey())) {
                    tByCost.put(entry.getKey(), new ArrayList<Transition>());
                }
                ArrayList<Transition> tList = tByCost.get(entry.getKey());
                for (TransitionInLeaf tr : entry.getValue()) {
                    tList.add(tr.getTr());
                }
            }
        }

        @Override
        public int numTransitions() {
            int numT = 0;
            for (ArrayList<TransitionInLeaf> trL : this.t.values()) {
                numT += trL.size();
            }
            return numT;
        }

        @Override
        public void remove(Transition tr) {
            TransitionInLeaf aux = null;
            for (TransitionInLeaf tL : this.t.get(tr.getCost())) {
                if (tL.getTr().getId() == tr.getId()) {
                    aux = tL;
                    break;
                }
            }
            this.t.get(tr.getCost()).remove(aux);
        }
    }

    private class TTInitStatistics {

        private long internalNodes, leafNodes;
        private final ArrayList<Long> transitionsLeafs;
        private final ArrayList<ArrayList<String>> transitionsSize;
        private long transitions;

        public TTInitStatistics() {
            this.transitions = 0;
            this.internalNodes = 0;
            this.leafNodes = 0;
            this.transitionsLeafs = new ArrayList<Long>();
            this.transitionsSize = new ArrayList<ArrayList<String>>();
        }

        private void addInternal() {
            this.internalNodes++;
        }

        private void addLeaf(ArrayList<Transition> t) {
            this.leafNodes++;
            this.transitions += t.size();
            this.transitionsLeafs.add((long) (t.size()));
            ArrayList<String> trSize = new ArrayList<String>();
            for (Transition tr : t) {
                trSize.add(tr.getTotalBDD().nodeCount() + " (" + tr.getCost() + ")");
            }
            this.transitionsSize.add(trSize);
        }

        @Override
        public String toString() {
            return "{" + "innerNodes=" + internalNodes
                    + ", leafNodes=" + leafNodes
                    + ", transitionsLeafs=" + transitionsLeafs
                    + ", transitions=" + transitions
                    + ", trsize=" + this.transitionsSize + '}';
        }
    }

    private class TTImageStatistics {

        private long totalTime;
        private long conjunctionTime;
        private long disjunctionTime;
        private long imageTime;
        private long numLeafs;
        private long numTransitions;

        public TTImageStatistics() {
            totalTime = 0L;
            conjunctionTime = 0L;
            disjunctionTime = 0L;
            imageTime = 0L;
            numLeafs = 0L;
            numTransitions = 0L;
        }

        public void leafImage(int size) {
            this.numLeafs++;
            this.numTransitions += size;
        }

        public void addConjunctionTime(long finalTime) {
            this.conjunctionTime += (System.currentTimeMillis() - finalTime);
        }

        public void addDisjunctionTime(long finalTime) {
            this.disjunctionTime += (System.currentTimeMillis() - finalTime);
        }

        public void addImageTime(long finalTime) {
            this.imageTime += (System.currentTimeMillis() - finalTime);
        }

        public void addTotalTime(long finalTime) {
            this.totalTime += (System.currentTimeMillis() - finalTime);
        }

        @Override
        public String toString() {
            return "TTStatistics{" + "totalTime=" + totalTime
                    + ", conjunctionTime=" + conjunctionTime
                    + ", disjunctionTime=" + disjunctionTime
                    + ", imageTime=" + imageTime
                    + ", numLeafs=" + numLeafs
                    + ", numTransitions=" + numTransitions + '}';
        }
    }
}
