/*
 * Gamer, a tool for finding optimal plans
 * Copyright (C) 2007-2012 by Peter Kissmann
 * modified by Alvaro Torralba (2012-2013)
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */
package pddl2bdd.transition;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import net.sf.javabdd.BDDFactory;
import net.sf.javabdd.BDDVarSet;
import pddl2bdd.problem.Action;
import pddl2bdd.problem.Problem;
import pddl2bdd.problem.VariablePartitioning;
import pddl2bdd.util.Log;

/**
 *
 * @author Peter Kissmann and Alvaro Torralba
 */
@Parameters(optionPrefixes = "_", separators = "=")
public class TTreeBuilder extends TManagerBuilder {

    @Parameter(names = "_disj_size", description = "")
    private int maxDisjTRSize = 100000;
    @Parameter(names = "_disj_time", description = "")
    private int maxDisjTRTime = 120000;

    @Parameter(names = "_disj_st", description = "Different strategies to organize the disjunction tree: \n"
            + "  DT: uses the disjunction trees as criterion \n"
            + "  smaller: aggregates the result of images by selecting the smaller BDDs\n"
            + "  all: to compute the result of the image (instead 2-disjuntions). Deprecated, dont use.")
    private String disjImageStrategy = "DT";

    @Parameter(names = "_dt", description = "Different strategies to organize the disjunction tree: \n"
            + "  balanced: create a balanced partition of the operators \n"
            + "  unbalanced: create an unbalanced partition of the operators \n"
            + "  balanced_random: create a balanced partition of the operators, then randomizes \n"
            + "  cluster: deprecated\n"
            + "  pre: deprecated    \n")
    private String disjTreeStrategy = "balanced";

    @Parameter(names = "_disj", description = "Different criteria to organize the tree: \n"
            + "  no_disj: a single TR per operator \n"
            + "  no_disj_wo_image:  preBDD and effBDD per operator  \n"
            + "  disj_dt: disjunction tree criterion to aggregate TRs\n"
            + "  disj_ct: conjunction tree criterion to aggregate TRs  \n"
            + "  disj_smaller: aggregate smaller BDDs \n"
            + "  disj_pre: aggregate similar TRs")
    private String disjTRStrategy = "disj_dt";

    @Parameter(names = "_ct", description = "Different criteria to organize the tree: \n"
            + "  no_tree: the tree is a single leaf containing all TRs\n"
            + "  normal:  check each variable as in the variable ordering: x1, x2, ..., xn  \n"
            + "  inverse: inverse ordering of variable ordering: xn, ..., x2, x1  \n"
            + "  random:  uses a fixed ordering, determined randomly \n"
            + "  dynamic: in each node selects the variable that appears in more preconditions\n")
    private String ctOrder = "no_tree";
    @Parameter(names = "_min_ops_leaf", description = "Minimum number of operators in a CT leaf")
    private int min_operators_leaf = 0;
    @Parameter(names = "_ct_depth_rel", description = "Maximum CT depth relative to the number of variables [0-1]")
    private double depthRel = 1;

    private int maxCTDepth;

    @Parameter(names = "_help", hidden = true)
    private boolean help;

    @Parameter(names = "_dont_share", description = "If true, different TRs are used for fw and bw search")
    private boolean dontShareTRs = false;

    public enum EdeleteType {

        NONE, PRESERVE, TOTAL, BDD, BDD_RESTRICT, BDD_NP_AND
    };
    private final EdeleteType edeletes;

//    Set<Integer> costs;
    List<Transition> transitions;
    HashMap<String, List<Transition>> transitionsByName;
    Set<Integer> transitionCosts;

    private List<Integer> emptyPartitions;

    TransitionTree mgr_fw, mgr_bw;

    public TTreeBuilder(Problem problem,
            VariablePartitioning variables, List<String> params,
            EdeleteType edeletes) {
        super(problem, variables);
        this.edeletes = edeletes;

        try {
            String[] pArray = (String[]) params.toArray(new String[params.size()]);
            JCommander jCommander = new JCommander(this, pArray);
        } catch (ParameterException e) {
            e.printStackTrace();
            System.err.println("ERROR: " + e.getMessage());
            this.help = true;
        }
        if (this.help) {
            new JCommander(this).usage();
            System.exit(0);
        }

        if (this.depthRel < 0 || this.depthRel > 1) {
            System.err.println("Error tt parameter: DepthRel should be between 0 and 1");
            System.exit(-1);
        }
        this.maxCTDepth = (int) ((variables.numBinaryVariables() / 2) * this.depthRel);
    }

    @Override
    public void init(Problem problem, List<Integer> emptyPartitions, String usedActionsFilename) {
        this.emptyPartitions = emptyPartitions;
        this.transitions = new ArrayList<Transition>();
        this.transitionsByName = new HashMap<String, List<Transition>>();
        this.transitionCosts = new HashSet<Integer>();
        for (Action action : problem.getActions()) {
            this.transitions.addAll(action.getTransitions(variables, emptyPartitions));
        }
        Log.getLog().log("Number of transitions: " + this.transitions.size());
        for (Transition tr : transitions) {
            if (!transitionsByName.containsKey(tr.name)) {
                transitionsByName.put(tr.name, new ArrayList<Transition>());
            }
            this.transitionsByName.get(tr.name).add(tr);
            this.transitionCosts.add(tr.getCost());
            if (!dontShareTRs) {
                tr.edeletion(variables.getMutex(false), this.edeletes);
                tr.edeletion(variables.getMutex(true), this.edeletes);
            }
        }

        if (this.mgr_bw != null) {
            this.mgr_bw.free();
            this.mgr_bw = null;
        }
        if (this.mgr_fw != null && dontShareTRs) {
            this.mgr_fw.free();
            this.mgr_fw = null;
        }
    }

    @Override
    public TransitionManager getTransitionManager(boolean forward) {
        if (forward) {
            if (mgr_fw != null) {
                return mgr_fw;
            }
            mgr_fw = new TransitionTree(this, variables, forward);
            if (!dontShareTRs && !mgr_fw.dontShareTRs()) {
                mgr_bw = new TransitionTree(mgr_fw, false);
            }
            return mgr_fw;
        } else {
            if (mgr_bw != null) {
                return mgr_bw;
            }
            mgr_bw = new TransitionTree(this, variables, forward);
            if (!dontShareTRs && !mgr_bw.dontShareTRs()) {
                mgr_fw = new TransitionTree(mgr_bw, false);
            }
            return mgr_bw;
        }
    }

    public HashMap<Integer, ArrayList<Transition>> getIndividualTransitions(boolean forward) {
        HashMap<Integer, ArrayList<Transition>> costTransitions = new HashMap<Integer, ArrayList<Transition>>();
        for (Transition tr : transitions) {
            int cost = tr.getCost();
            if (!costTransitions.containsKey(cost)) {
                costTransitions.put(cost, new ArrayList<Transition>());
            }

            Transition copy = new Transition(tr);
            if (dontShareTRs) {
                copy.edeletion(variables.getMutex(forward), this.edeletes);
            }
            //copy.abstractExist(this.emptyPartitions):
            costTransitions.get(cost).add(copy);
        }

        return costTransitions;
    }

    @Override
    public void cleanup() {
        for (Transition t : transitions) {
            t.cleanup();
        }
    }

    public Set<Integer> getTransitionCosts() {
        return transitionCosts;
    }

    public int getMaxCTDepth() {
        return maxCTDepth;
    }

    public List<Transition> getTransitionByName(String name) {
        return transitionsByName.get(name);
    }

    public String getDisjImageStrategy() {
        return disjImageStrategy;
    }

    public String getDTStrategy() {
        return disjTreeStrategy;
    }

    public String getDisjTRStrategy() {
        return disjTRStrategy;
    }

    public String getCTOrder() {
        return ctOrder;
    }

    public int getMaxDisjTRSize() {
        return maxDisjTRSize;
    }

    public int getMaxDisjTRTime() {
        return maxDisjTRTime;
    }

    public int getMinOperatorsLeaf() {
        return min_operators_leaf;
    }
}
