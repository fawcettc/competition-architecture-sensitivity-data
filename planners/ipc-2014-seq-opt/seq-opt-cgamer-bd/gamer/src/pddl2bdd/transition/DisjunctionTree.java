/*
 * Gamer, a tool for finding optimal plans
 * Copyright (C) 2007-2012 by Peter Kissmann
 * modified by Alvaro Torralba (2012-2013)
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */
package pddl2bdd.transition;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import net.sf.javabdd.BDD;
import pddl2bdd.transition.Transition.MaxSizeException;
import pddl2bdd.util.Log;
import pddl2bdd.util.Rnd;

/**
 *
 * @author Peter Kissmann and Alvaro Torralba
 */
public class DisjunctionTree {

    public enum DTStrategy {
        BALANCED, UNBALANCED, CLUSTER/*, PRE*/, BALANCED_RANDOM
    }
    private final DisjTTNode disjTree;

    public DisjunctionTree(List<Transition> transitions, DTStrategy strategy) {
        this.disjTree = new DisjTTNode(transitions, getSplitter(strategy, transitions));
    }
    
    final Splitter getSplitter(DisjunctionTree.DTStrategy st,
            List<Transition> transitions) {
        switch (st) {
            case UNBALANCED:
                return new UnbalancedSplit();
            case BALANCED:
                return new BalancedSplit(false);
            case CLUSTER:
                return new ClusterSplit(transitions);
            case BALANCED_RANDOM:
                return new BalancedSplit(true);
        /*    case PRE:
                return new PreconditionSplit();*/
        }
        return null;
    }

    public BDD disjunction(HashMap<Integer, BDD> images, long MAX_TIME) {
        return this.disjTree.disjunction(images, MAX_TIME);

    }

    public void unifyTransitions(int maxTime, int maxTransistionSize) {
        long MAX_TIME = System.currentTimeMillis() + maxTime;
        // 1) Get nodes
        ArrayList<DisjTTNode> listOfNodes = new ArrayList<DisjTTNode>();
        listOfNodes.add(disjTree);
        for(int i = 0; i < listOfNodes.size(); i++){
            if (listOfNodes.get(i).t1 != null){
                listOfNodes.add(listOfNodes.get(i).t1);
            }
            if (listOfNodes.get(i).t2 != null){
                listOfNodes.add(listOfNodes.get(i).t2);
            }
        }
        Log.getLog().log("Unify transitions: " + maxTransistionSize);        
        for(int i = listOfNodes.size() -1; i >= 0; --i){
            listOfNodes.get(i).unifyTransitions(MAX_TIME - System.currentTimeMillis(),
                    maxTransistionSize);
        }
    }

    public List<Transition> getTransitions() {
        ArrayList<Transition> res = new ArrayList<Transition>();
        this.disjTree.getTransitions(res);
        return res;
    }

    public void cleanup() {
        disjTree.cleanup();
    }

    private interface Splitter {
        void splitTransitions(List<Transition> transitions,
                List<Transition> l1, List<Transition> l2);
    }

    private class BalancedSplit implements Splitter {
        
        private final boolean randomize;

        public BalancedSplit(boolean randomize) {
            this.randomize = randomize;
        }

        @Override
        public void splitTransitions(List<Transition> transitions,
                List<Transition> l1, List<Transition> l2) {
            int half = transitions.size() / 2;
            for (int i = 0; i < transitions.size(); i++) {
                if (i < half) {
                    l1.add(transitions.get(i));
                } else {
                    l2.add(transitions.get(i));
                }
            }
            if(randomize){
                for(int i = 0; i < transitions.size()*2; i++){
                    int ind1 = Rnd.nextInt(l1.size());
                    int ind2 = Rnd.nextInt(l2.size());
                    Transition t1 = l1.get(ind1);
                    Transition t2 = l2.get(ind2);
                    l2.set(ind2, t1);
                    l1.set(ind1, t2);
                }
            }
        }
    }

    private class UnbalancedSplit implements Splitter {

        @Override
        public void splitTransitions(List<Transition> transitions,
                List<Transition> l1, List<Transition> l2) {
            l1.add(transitions.get(0));
            for (int i = 1; i < transitions.size(); i++) {
                l2.add(transitions.get(i));
            }
        }
    }

/*    private class PreconditionSplit implements Splitter {

        @Override
        public void splitTransitions(List<Transition> transitions, List<Transition> l1, List<Transition> l2) {
            int var = 0;
            boolean fw = true;
            List<Transition> pending = transitions;
            while (!pending.isEmpty()) {
                List<Transition> lValX = new ArrayList<Transition>();
                List<Transition> lVal0 = new ArrayList<Transition>();
                List<Transition> lVal1 = new ArrayList<Transition>();
                for (Transition t : pending) {
                    if (t.hasPrecondition(fw, var, true)) {
                        lVal0.add(t);
                    } else if (t.hasPrecondition(fw, var, false)) {
                        lVal1.add(t);
                    } else {
                        lValX.add(t);
                    }
                }

                int l1Val0 = 0, l1Val1 = 0, l2Val0 = 0, l2Val1 = 0;
                for (Transition t : l1) {
                    if (t.hasPrecondition(fw, var, true)) {
                        l1Val0++;
                    } else if (t.hasPrecondition(fw, var, false)) {
                        l1Val1++;
                    }
                }


                // int difference = Math.abs(l1.size() - l2.size());
                // if(Math.abs(l1.size() - l2.size()) )
                // pending = lValX;
            }
        }
    }*/

    private class ClusterSplit implements Splitter {

        class Pair {
            int t1, t2;

            public Pair(int t1, int t2) {
                this.t1 = t1;
                this.t2 = t2;
            }

            @Override
            public boolean equals(Object obj) {
                if (obj == null) {
                    return false;
                }
                if (getClass() != obj.getClass()) {
                    return false;
                }
                final Pair other = (Pair) obj;
                if (this.t1 != other.t1) {
                    return false;
                }
                if (this.t2 != other.t2) {
                    return false;
                }
                return true;
            }

            @Override
            public int hashCode() {
                int hash = 5;
                hash = 83 * hash + this.t1;
                hash = 83 * hash + this.t2;
                return hash;
            }
        }
        HashMap<Pair, Integer> distances;

        public ClusterSplit(List<Transition> transitions) {
            distances = new HashMap<Pair, Integer>();
            for (int i = 0; i < transitions.size(); i++) {
                //Log.getLog().log(i + " of " + transitions.size());
                Transition t1 = transitions.get(i);
                Set<Integer> v1 = t1.getVarsSet();
                for (int j = i + 1; j < transitions.size(); j++) {
                    Transition t2 = transitions.get(j);
                    Set<Integer> v2 = t2.getVarsSet();
                    Set<Integer> vUnion = new HashSet(v1);
                    vUnion.addAll(v2);

                    int distance = vUnion.size() * 2 - v1.size() - v2.size();
                    //Log.getLog().log(distance);
                    distances.put(new Pair(t1.getId(), t2.getId()), distance);
                    distances.put(new Pair(t2.getId(), t1.getId()), distance);
                }
            }
        }

        @Override
        public void splitTransitions(List<Transition> transitions,
                List<Transition> l1, List<Transition> l2) {
            double distTrue = 0;
            double distFalse = 0;
            int ids[] = new int[transitions.size()];
            for (int i = 0; i < transitions.size(); i++) {
                ids[i] = transitions.get(i).getId();
            }
            Random rnd = new Random(0);
            boolean[] currentPartitioning = new boolean[transitions.size()];
            int nTrue = 0, nFalse = 0;
            for (int i = 0; i < currentPartitioning.length; i++) {
                currentPartitioning[i] = rnd.nextBoolean();
                if (currentPartitioning[i]) {
                    nTrue++;
                } else {
                    nFalse++;
                }
            }
            while (nTrue < nFalse - 1) {
                int pos = rnd.nextInt(currentPartitioning.length);
                if (!currentPartitioning[pos]) {
                    currentPartitioning[pos] = true;
                    nTrue++;
                    nFalse--;
                }
            }

            while (nFalse < nTrue - 1) {
                int pos = rnd.nextInt(currentPartitioning.length);
                if (currentPartitioning[pos]) {
                    currentPartitioning[pos] = false;
                    nFalse++;
                    nTrue--;
                }
            }

            for (int i = 0; i < currentPartitioning.length; i++) {
                for (int j = i + 1; j < currentPartitioning.length; j++) {
                    if (currentPartitioning[i] && currentPartitioning[j]) {
                        distTrue += distances.get(new Pair(ids[i], ids[j]));
                    } else if (!currentPartitioning[i] && !currentPartitioning[j]) {
                        distFalse += distances.get(new Pair(ids[i], ids[j]));
                    }
                }
            }
            double bestPartValue = distTrue * distTrue + distFalse * distFalse;

            for (int iteration = 0; iteration < 10000; iteration++) {
                int pos = rnd.nextInt(currentPartitioning.length);
                boolean prevValue = currentPartitioning[pos];
//                if((prevValue && nTrue <= 1) ||
                //                     (!prevValue && nFalse <= 1)) continue;

                int pos2 = pos;
                while (currentPartitioning[pos2] == prevValue) {
                    pos2 = rnd.nextInt(currentPartitioning.length);
                }

                double newDistTrue = distTrue;
                double newDistFalse = distFalse;
                for (int i = 0; i < currentPartitioning.length; i++) {
                    if ((i == pos || i == pos2)) {
                        continue;
                    }
                    int distancePos = distances.get(new Pair(ids[pos], ids[i]));
                    int distancePos2 = distances.get(new Pair(ids[pos2], ids[i]));
                    if (currentPartitioning[i] == prevValue) {
                        if (prevValue) {
                            newDistTrue -= distancePos;
                            newDistFalse += distancePos2;
                        } else {
                            newDistFalse -= distancePos;
                            newDistTrue += distancePos2;
                        }
                    } else {
                        if (prevValue) {
                            newDistTrue += distancePos;
                            newDistFalse -= distancePos2;
                        } else {
                            newDistFalse += distancePos;
                            newDistTrue -= distancePos2;
                        }
                    }
                }

                double newPartValue = newDistFalse * newDistFalse + newDistTrue * newDistTrue;
                if (newPartValue < bestPartValue) {
                    bestPartValue = newPartValue;
                    distTrue = newDistTrue;
                    distFalse = newDistFalse;
                    currentPartitioning[pos] = !prevValue;
                    currentPartitioning[pos2] = prevValue;
                    /*                        if(prevValue){
                     nTrue --;
                     nFalse++;
                     }else{
                     nFalse--;
                     nTrue ++;
                     }*/
                }
            }

            for (int i = 0; i < currentPartitioning.length; i++) {
                if (currentPartitioning[i]) {
                    l1.add(transitions.get(i));
                } else {
                    l2.add(transitions.get(i));
                }
            }
        }
    }

    private class DisjTTNode {
        private Transition tr;
        private DisjTTNode t1, t2;

        private DisjTTNode(List<Transition> transitions, Splitter sp) {
            if (transitions.size() == 1) {
                this.tr = transitions.get(0);
                this.t1 = null;
                this.t2 = null;
            } else {
                ArrayList<Transition> l1 = new ArrayList<Transition>();
                ArrayList<Transition> l2 = new ArrayList<Transition>();
                sp.splitTransitions(transitions, l1, l2);
                this.tr = null;
                //Log.getLog().log(l1.size() + " " + l2.size());
                this.t1 = new DisjTTNode(l1, sp);
                this.t2 = new DisjTTNode(l2, sp);
            }
        }

        private BDD disjunction(HashMap<Integer, BDD> images, long MAX_TIME) {
            if (tr != null) {
                if (images.containsKey(tr.getId())) {
                    return images.get(tr.getId());
                } else {
                    return tr.getVariables().getZeroBDD().id();
                }
            }
            BDD resT1 = t1.disjunction(images, MAX_TIME);
            BDD resT2 = t2.disjunction(images, MAX_TIME);
            if (resT1 == null || resT2 == null
                    || System.currentTimeMillis() > MAX_TIME) {
                if (resT1 != null) {
                    resT1.free();
                }
                if (resT2 != null) {
                    resT2.free();
                }
                return null;
            }
            BDD res;
            if (MAX_TIME != Long.MAX_VALUE) {
                res = resT1.or(resT2, MAX_TIME - System.currentTimeMillis(), 0);
            } else {
                res = resT1.or(resT2);
            }

            resT1.free();
            resT2.free();

            return res;
        }

        /**
         * Returns true if the result is a leaf with size not greater than
         * maxSize
         *
         * @param maxTransistionSize
         * @return
         */
        private void unifyTransitions(long maxTime, int maxTransistionSize) {
            if (this.isLeaf()) {
                return;
            }
            //this.t1.unifyTransitions(maxTime, maxTransistionSize);
            //this.t2.unifyTransitions(maxTime, maxTransistionSize);
            if (this.t1.isLeaf() && this.t2.isLeaf()) {
                try {
                    this.tr = new Transition(this.t1.tr, this.t2.tr, maxTime, maxTransistionSize);
                    this.t1.cleanup();
                    this.t2.cleanup();
                    this.t1 = null;
                    this.t2 = null;
                } catch (MaxSizeException ex) {
                    this.tr = null;
                }
            }
        }

        boolean isLeaf() {
            return this.tr != null;
        }

        private void getTransitions(List<Transition> res) {
            if (tr == null) {
                this.t1.getTransitions(res);
                this.t2.getTransitions(res);
            } else {
                res.add(tr);
            }
        }

        private void cleanup() {
            if (this.tr != null) {
                this.tr.cleanup();
            }
            if (this.t1 != null) {
                this.t1.cleanup();
                this.t2.cleanup();
            }
        }
    }
}
