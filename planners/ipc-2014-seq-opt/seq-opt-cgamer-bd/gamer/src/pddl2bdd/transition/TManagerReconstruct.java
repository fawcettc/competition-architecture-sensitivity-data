/*
 * Gamer, a tool for finding optimal plans
 * Copyright (C) 2007-2012 by Peter Kissmann
 * modified by Alvaro Torralba (2012-2013)
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */
package pddl2bdd.transition;

import java.util.HashMap;
import java.util.List;
import net.sf.javabdd.BDD;
import pddl2bdd.problem.VariablePartitioning;

/**
 *
 * @author Peter Kissmann and Alvaro Torralba
 */
public class TManagerReconstruct extends TransitionManager{

    public TManagerReconstruct(TManagerBuilder builder, VariablePartitioning variables, boolean forward) {
        super(builder, variables, forward);
    }


    @Override
    public BDD image(int cost, BDD from, BDD conjunct, long maxTime, long maxNodes) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public HashMap<Integer, BDD> image(BDD from, BDD conjunct, long maxTime, long maxNodes) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public void free() {
        
    }

    @Override
    public BDD applyAction(BDD from, String action) {
        List<Transition> trs = ((TTreeBuilder) this.builder).getTransitionByName(action);
        if(trs.size() == 1) {
            return trs.get(0).relprodBDD(from, forward, Long.MAX_VALUE, 0);
        }else{
            BDD res = variables.getZeroBDD().id();
            for(Transition tr : trs){
                BDD aux = tr.relprodBDD(from, forward, Long.MAX_VALUE, 0);
                BDD aux2 = res.or(aux);
                aux.free();
                res.free();
                res = aux2;
            }
            return res;
        }
    }

}
