/*
 * Gamer, a tool for finding optimal plans
 * Copyright (C) 2007-2012 by Peter Kissmann
 * modified by Alvaro Torralba (2012-2013)
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */
package pddl2bdd.transition;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import net.sf.javabdd.BDD;
import net.sf.javabdd.BDDPairing;
import net.sf.javabdd.BDDVarSet;
import pddl2bdd.problem.VariablePartitioning;

/**
 * Interface for applying images of the transitions.
 * 
  * @author Peter Kissmann and Alvaro Torralba
 */
public abstract class TransitionManager {

    protected TManagerBuilder builder;
    protected VariablePartitioning variables;
    protected boolean forward;

    public TransitionManager(TManagerBuilder builder,
            VariablePartitioning variables, boolean forward) {
        this.builder = builder;
        this.variables = variables;
        this.forward = forward;
    }

    /**
     * Apply an image for transitions with a particular
     * cost and appling a conjunction with the conjunct BDD if it is not null.
     * If the time surpasses max_time then it returns
     */
    public abstract BDD image(int cost, BDD from, BDD conjunct, long maxTime, long maxNodes);

    public BDD image(int cost, BDD from, BDD conjunct) {
        return image(cost, from, conjunct, Long.MAX_VALUE, Long.MAX_VALUE);
    }

    public BDD image(int cost, BDD from, long maxTime, long maxNodes) {
        return image(cost, from, null, maxTime, maxNodes);
    }

    public BDD image(int cost, BDD from) {
        return image(cost, from, null, Long.MAX_VALUE, Long.MAX_VALUE);
    }

    /**
     * Apply an image for all non-zero cost transitions
     * and appling a conjunction with the conjunct BDD.
     */
    public abstract HashMap<Integer, BDD> image(BDD from, BDD conjunct, long maxTime, long maxNodes);

    public HashMap<Integer, BDD> image(BDD from, BDD conjunct) {
        return image(from, conjunct, Long.MAX_VALUE, Long.MAX_VALUE);
    }

    public HashMap<Integer, BDD> image(BDD from, long maxTime, long maxNodes) {
        return image(from, null, maxTime, maxNodes);
    }

    public HashMap<Integer, BDD> image(BDD from) {
        return image(from, null, Long.MAX_VALUE, Long.MAX_VALUE);
    }

    public void applicableOperators(BDD from, ArrayList<String> res) {
        for (List<String> op_cost : builder.getActionNames().values()) {
            for (String op : op_cost) {
                BDD bdd = this.applyAction(from, op);
                if (!bdd.equals(variables.getZeroBDD())) {
                    res.add(op);
                }
                bdd.free();
            }
        }
    }

    /**
     * Free the memory used by the transition manager
     */
    public abstract void free();

    /**
     * Used for solution reconstruction: we need to be able to
     * apply transitions relative to one single action
     */
    public abstract BDD applyAction(BDD tmp1, String action);

    /**
     * Tries a whole step: applying 0-cost actions and then applying cost actions.
     * 
     * @param from
     * @param maxTime
     * @return null if maxTime is reached: 
     */
    public HashMap<Integer, BDD> testImage(BDD from, BDD conjunct, long maxTime, long maxNodes) {
        BDD reached = builder.getFactory().zero();

        if (builder.hasZeroCostOperators()) {
            //Apply zero cost actions
            while (true) {
                BDD img = this.image(0, from, conjunct, maxTime, maxNodes);
                from.free();
                if (img == null) {
                    reached.free();
                    return null;
                }
                BDD notReached = reached.not();
                from = img.and(notReached);
                img.free();
                notReached.free();
                BDD tmp1 = reached;
                reached = reached.or(from);
                tmp1.free();
                if (from.equals(builder.getVariables().getZeroBDD())
                        || System.currentTimeMillis() > maxTime) {
                    break;
                }
            }
            from.free();
        } else {
            reached = from;
        }
        
        if (System.currentTimeMillis() > maxTime) {
            reached.free();
            return null;
        }
        HashMap<Integer, BDD> res = this.image(reached, conjunct, maxTime, maxNodes);
        reached.free();
        return res;
    }
    
    
    /**
     * Tries a whole step: applying 0-cost actions and then applying cost actions.
     * 
     * @param from
     * @param maxTime
     * @return null if maxTime is reached: 
     */
    public HashMap<Integer, BDD> testImage(BDD from, BDD conjunct,
            long maxTime, long maxNodes, List <BDD> mutexBDDs) {
        BDD reached = builder.getFactory().zero();

        if (builder.hasZeroCostOperators()) {
            //Apply zero cost actions
            while (true) {
                BDD img = this.image(0, from, conjunct, maxTime, maxNodes);
                from.free();
                if (img == null) {
                    reached.free();
                    return null;
                }
                BDD notReached = reached.not();
                from = img.and(notReached);
                img.free();
                notReached.free();
                BDD tmp1 = reached;
                reached = reached.or(from);
                tmp1.free();
                if (from.equals(builder.getVariables().getZeroBDD())
                        || System.currentTimeMillis() > maxTime) {
                    break;
                }
            }
            from.free();
        } else {
            reached = from;
        }
        
        if (System.currentTimeMillis() > maxTime) {
            reached.free();
            return null;
        }
        HashMap<Integer, BDD> res = this.image(reached, conjunct, maxTime, maxNodes);
        reached.free();
        return res;
    }


    protected BDDPairing getPairing() {
        if (forward) {
            return this.variables.getSp2s();
        } else {
            return this.variables.getS2sp();
        }
    }

    protected BDDPairing getInversePairing() {
        if (forward) {
            return this.variables.getS2sp();
        } else {
            return this.variables.getSp2s();
        }
    }

    protected BDDVarSet getVarSet() {
        if (forward) {
            return this.variables.getCube();
        } else {
            return this.variables.getCubep();
        }
    }
}
