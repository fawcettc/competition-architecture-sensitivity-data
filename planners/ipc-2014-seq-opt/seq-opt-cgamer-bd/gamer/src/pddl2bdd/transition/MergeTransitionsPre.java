/*
 * Gamer, a tool for finding optimal plans
 * Copyright (C) 2007-2012 by Peter Kissmann
 * modified by Alvaro Torralba (2012-2013)
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */
package pddl2bdd.transition;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import pddl2bdd.problem.VariablePartitioning;
import pddl2bdd.transition.Transition.MaxSizeException;
import pddl2bdd.util.Log;

/**
 *
 * @author Peter Kissmann and Alvaro Torralba
 */
public class MergeTransitionsPre {

    boolean forward;
    int[] rel_pre;
    int MIN_OPERATORS_LEAF;
    int MAX_TRANSITION_SIZE;
    long MAX_TIME;

    public MergeTransitionsPre(VariablePartitioning variables,
            ArrayList<Transition> allTransitions, boolean forward,
            int MIN_OPERATORS_LEAF,  int MAX_TRANSITION_TIME, int MAX_TRANSITION_SIZE) {
        this.forward = forward;
        rel_pre = new int[variables.numBinaryVariables()];
        Arrays.fill(rel_pre, 0);
        for (Transition t : allTransitions) {
            Set<Integer> prePos;
            Set<Integer> preNeg;
            if (forward) {
                prePos = t.getBinaryPosPre();
                preNeg = t.getBinaryNegPre();
            } else {
                prePos = t.getBinaryPosPreInv();
                preNeg = t.getBinaryNegPreInv();
            }


            for (int pre : prePos) {
                //Log.getLog().log(pre);
                rel_pre[pre]++;
            }
            for (int pre : preNeg) {
                //Log.getLog().log(pre+1);
                rel_pre[pre + 1]++;
            }
        }
        //Log.getLog().log("Relevance of preconditions: " + Arrays.toString(rel_pre));

        this.MIN_OPERATORS_LEAF = MIN_OPERATORS_LEAF;
        this.MAX_TRANSITION_SIZE = MAX_TRANSITION_SIZE;
        this.MAX_TIME = System.currentTimeMillis() + MAX_TRANSITION_TIME;
    }

    private class CompareTransitions implements Comparator<Transition>{
        private final HashMap<Integer, Integer> maxVal;
        private CompareTransitions(HashMap<Integer, Integer> maxVal) {
            this.maxVal = maxVal;
        }

        @Override
        public int compare(Transition t1, Transition t2) {
            int id1 = t1.getId();
            int id2 = t2.getId();

            int val1 = this.maxVal.get(id1);
            int val2 = this.maxVal.get(id2);
           
            if(val1 == val2){
                return t1.getId() - t2.getId();
            }else{
                return val1 - val2;
            }
        }
    }

    
    public List<Transition> merge(ArrayList<Transition> transitions) {
        HashMap<Integer, ArrayList<Integer>> preByRel = new HashMap<Integer, ArrayList<Integer>>();
        HashMap<Integer, Integer> maxVal = new HashMap<Integer, Integer>();
        HashMap<Integer, ArrayList<Transition>> preconditionTr = new HashMap<Integer, ArrayList<Transition>>();
        HashMap<Integer, Integer> transitionSize = new HashMap<Integer, Integer>();

        for(int i = 0; i < this.rel_pre.length; i++){
            preconditionTr.put(i, new ArrayList());
        }
        for(Transition t: transitions){
            this.add(t, preconditionTr, preByRel, maxVal, transitionSize);
        }

        CompareTransitions comparator = new CompareTransitions(maxVal);
        Collections.sort(transitions, comparator);
        
        ArrayList<Transition> res = new ArrayList<Transition>();

        int threshold = 0;
        while (transitions.size() > 1) {
            long time = System.currentTimeMillis();
            int bestVal = Integer.MAX_VALUE;
            Transition bestT1 = null, bestT2 = null;
            for (int i = 0; i < transitions.size(); i++) {
                Transition t1 = transitions.get(i);
                int id1 = t1.getId();
                int maxValue = maxVal.get(id1);
                //Log.getLog().log("MAX VAL OF T1: " + maxVal);
                
                if (maxValue >= bestVal) {
                    //Only check those operators with the same precondition in the best case
                    for (Transition t2 : preconditionTr.get(preByRel.get(id1).get(0))) {
                        if(comparator.compare(t1, t2) < 0){
                            int value = compute_difference(t1, t2, preByRel);
                            if (value < bestVal) {
                                bestVal = value;
                                bestT1 = t1;
                                bestT2 = t2;
                                if(bestVal <= threshold){
                                   break;
                                 }
                            }
                        }
                    }
                } else {
                    for (int j = i+1; j < transitions.size(); j++) {
                        Transition t2 = transitions.get(j);
                        //if(comparator.compare(t1, t2) < 0){
                            int value = compute_difference(t1, t2, preByRel);
                            if (value < bestVal) {
                                bestVal = value;
                                bestT1 = t1;
                                bestT2 = t2;
                                 if(bestVal <= threshold){
                                   break;
                                 }
                            }
                       // }
                    }
                }
                if(bestVal <= threshold){
                    break;
                }
            }

            if(threshold < bestVal){
                threshold = bestVal;
            }

            //Now, we have selected t1 and t2. Try merge them
            try {
                if(Log.doExpensiveLog){
                    Log.getLog().log(transitions.size() + " Trying to merge: " + bestT1.getId() +
                        " and " + bestT2.getId() + " distance " +
                        this.compute_difference(bestT1, bestT2, preByRel) + " " +
                        (System.currentTimeMillis() - time) + " ms");
                time = System.currentTimeMillis();
                }

                Transition newT = new Transition(bestT1, bestT2, (int)(MAX_TIME - System.currentTimeMillis()), MAX_TRANSITION_SIZE);
                remove(bestT1, transitions, preconditionTr);
                remove(bestT2, transitions, preconditionTr);
                bestT1.cleanup();
                bestT2.cleanup();
                add(newT, preconditionTr, preByRel, maxVal, transitionSize);
                int pos = Collections.binarySearch(transitions, newT, new CompareTransitions(maxVal));
                transitions.add(-pos -1, newT);
                
                 if(Log.doExpensiveLog)Log.getLog().log("Merged: " + (System.currentTimeMillis() - time) + " ms");
            } catch (MaxSizeException ex) {
                //They could not be merged, consider that we have finished merging the bigger one
                if (transitionSize.get(bestT1.getId()) > transitionSize.get(bestT2.getId())) {
                    remove(bestT1, transitions, preconditionTr);
                    res.add(bestT1);
                } else {
                    remove(bestT2, transitions, preconditionTr);
                    res.add(bestT2);
                }
                 if(Log.doExpensiveLog)Log.getLog().log("Not merged: " + (System.currentTimeMillis() - time) + " ms");
            }
        }

        res.addAll(transitions);

        Log.getLog().log("Finished merge: " + res.size());
        return res;
    }

    private void remove(Transition t, ArrayList<Transition> transitions,
            HashMap<Integer, ArrayList<Transition>> preconditionTr) {
        transitions.remove(t);

        Set<Integer> prePos;
        Set<Integer> preNeg;
        if (forward) {
            prePos = t.getBinaryPosPre();
            preNeg = t.getBinaryNegPre();
        } else {
            prePos = t.getBinaryPosPreInv();
            preNeg = t.getBinaryNegPreInv();
        }
        for (Integer pre : prePos) {
            preconditionTr.get(pre).remove(t);
        }
        for (Integer pre : preNeg) {
            preconditionTr.get(pre + 1).remove(t);
        }
    }

    private void add(Transition t, HashMap<Integer, ArrayList<Transition>> preconditionTr,
            HashMap<Integer, ArrayList<Integer>> preByRel,
            HashMap<Integer, Integer> maxVal,
            HashMap<Integer, Integer> transitionSize) {
        ArrayList<Integer> preT = new ArrayList<Integer>();

        Set<Integer> prePos;
        Set<Integer> preNeg;
        if (forward) {
            prePos = t.getBinaryPosPre();
            preNeg = t.getBinaryNegPre();
        } else {
            prePos = t.getBinaryPosPreInv();
            preNeg = t.getBinaryNegPreInv();
        }
        for (Integer pre : prePos) {
            if (rel_pre[pre] >= MIN_OPERATORS_LEAF) {
                preT.add(pre);
            }
        }
        for (Integer pre : preNeg) {
            if (rel_pre[pre+1] >= MIN_OPERATORS_LEAF) {
                preT.add(pre+1);
            }
        }
        //Log.getLog().log("Relevant preconditions: " + preT.size());
        Collections.sort(preT, new Comparator<Integer>() {
            @Override
            public int compare(Integer pre1, Integer pre2) {
                if (rel_pre[pre1] == rel_pre[pre2]) {
                    return pre1 - pre2;
                } else {
                    return rel_pre[pre2] - rel_pre[pre1];
                }
            }
        });

        if(preT.isEmpty()){
            maxVal.put(t.getId(), 0);
        }else{
            maxVal.put(t.getId(), rel_pre[preT.get(0)]);
        }
        preByRel.put(t.getId(), preT);
        
        transitionSize.put(t.getId(), t.getTotalBDD().nodeCount());

        for (Integer pre : preT) {
            //Log.getLog().log(rel_pre[pre]);
            preconditionTr.get(pre).add(t);
        }
    }

    private int compute_difference(Transition t1, Transition t2, HashMap<Integer, ArrayList<Integer>> preByRel) {
        ArrayList<Integer> pre1 = preByRel.get(t1.getId());
        ArrayList<Integer> pre2 = preByRel.get(t2.getId());
        for (int i = 0; i < pre1.size() && i < pre2.size(); i++) {
            if (pre1.get(i) != pre2.get(i)) {
                return Math.max(rel_pre[pre1.get(i)], rel_pre[pre2.get(i)]);
            }
        }

        if (pre1.size() > pre2.size()) {
            return rel_pre[pre1.get(pre2.size())];
        }
        if (pre2.size() > pre1.size()) {
            return rel_pre[pre2.get(pre1.size())];
        }

        return 0;
    }
}
