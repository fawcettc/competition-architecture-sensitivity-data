/*
 * Gamer, a tool for finding optimal plans
 * Copyright (C) 2007-2012 by Peter Kissmann
 * modified by Alvaro Torralba (2012-2013)
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */
package pddl2bdd.varOrder;

import java.io.FileWriter;
import java.util.logging.Level;
import pddl2bdd.problem.Problem;
import pddl2bdd.util.Log;
import pddl2bdd.util.Rnd;

/**
 * Optimizes the variable ordering to minimize the influences.
 * 
 * @author Peter Kissmann and Alvaro Torralba
 */
public class VarOrderingOpt extends VariableOrdering {
    
    
    public VarOrderingOpt(){
    
    }

    @Override
    public int[] variableOrdering(Problem problem) {
        InfluenceGraph influence = new InfluenceGraph(problem);
        boolean[][] influences = influence.getInfluences();
        int[] variableOrdering = findVariableOrdering(influences);
        try {
            FileWriter ordering = new FileWriter(
                    "variableOrdering_tmp.txt");
            for (int i = 0; i < variableOrdering.length; i++) {
                ordering.write(variableOrdering[i] + "\n");
            }
            ordering.flush();
            ordering.close();
            Runtime.getRuntime().exec(
                    "mv variableOrdering_tmp.txt variableOrdering.txt");
        } catch (Exception e) {
            System.err.println("Error: " + e.getMessage());
            e.printStackTrace(System.err);
            System.exit(1);
        }
        return variableOrdering;

    }

    private int[] findVariableOrdering(boolean[][] influences) {
        int[] variableOrdering = new int[influences.length];
        for (int i = 0; i < variableOrdering.length; i++) {
            variableOrdering[i] = i;
        }
        int[] bestVariableOrdering = new int[influences.length];
        long bestTotalDistance = Long.MAX_VALUE;
        // Random rnd = new Random(System.currentTimeMillis());
        for (int i = 0; i < 20; i++) {
            long totalDistance;
            totalDistance = findOneVariableOrdering(influences,
                    variableOrdering);
            if (totalDistance < bestTotalDistance) {
                bestTotalDistance = totalDistance;
                bestVariableOrdering = variableOrdering.clone();
            }
            //Log.getLog().log(bestTotalDistance);
        }
        Log.getLog().log(Level.INFO, bestTotalDistance + ": ", bestVariableOrdering);
        return bestVariableOrdering;
    }

    private long findOneVariableOrdering(boolean[][] influences,
            int[] variableOrdering) {
        int swapIndex1 = 0;
        int swapIndex2 = 0;
        int tmpVariable;
        for (int i = variableOrdering.length - 1; i >= 1; i--) {
            tmpVariable = variableOrdering[i];
            swapIndex1 = Rnd.nextInt(i + 1);
            variableOrdering[i] = variableOrdering[swapIndex1];
            variableOrdering[swapIndex1] = tmpVariable;
        }

        long oldTotalDistance = Integer.MAX_VALUE;
        long totalDistance = 0;
        for (int i = 0; i < variableOrdering.length - 1; i++) {
            for (int j = i + 1; j < variableOrdering.length; j++) {
                if (influences[variableOrdering[i]][variableOrdering[j]]
                        || influences[variableOrdering[j]][variableOrdering[i]]) {
                    totalDistance += Math.max(i - j, j - i)
                            * Math.max(i - j, j - i);
                }
            }
        }
        Log.getLog().log("Init distance " + totalDistance);
        oldTotalDistance = totalDistance;
        for (int counter = 0; counter < 50000; counter++) {
            swapIndex1 = Rnd.nextInt(variableOrdering.length);
            swapIndex2 = Rnd.nextInt(variableOrdering.length);
            // Log.getLog().log(influences[variableOrdering[swapIndex1]][variableOrdering[swapIndex2]]);
            for (int i = 0; i < variableOrdering.length; i++) {
                if (i == swapIndex1 || i == swapIndex2) {
                    continue;
                }
                if ((influences[variableOrdering[i]][variableOrdering[swapIndex1]] || influences[variableOrdering[swapIndex1]][variableOrdering[i]])
                        && (influences[variableOrdering[i]][variableOrdering[swapIndex2]] || influences[variableOrdering[swapIndex2]][variableOrdering[i]])) {
                    continue;
                }
                if (influences[variableOrdering[i]][variableOrdering[swapIndex1]]
                        || influences[variableOrdering[swapIndex1]][variableOrdering[i]]) {
                    totalDistance = totalDistance
                            - Math.max(i - swapIndex1, swapIndex1 - i)
                            * Math.max(i - swapIndex1, swapIndex1 - i)
                            + Math.max(i - swapIndex2, swapIndex2 - i)
                            * Math.max(i - swapIndex2, swapIndex2 - i);
                }
                if (influences[variableOrdering[i]][variableOrdering[swapIndex2]]
                        || influences[variableOrdering[swapIndex2]][variableOrdering[i]]) {
                    totalDistance = totalDistance
                            - Math.max(i - swapIndex2, swapIndex2 - i)
                            * Math.max(i - swapIndex2, swapIndex2 - i)
                            + Math.max(i - swapIndex1, swapIndex1 - i)
                            * Math.max(i - swapIndex1, swapIndex1 - i);
                }
            }
            if (totalDistance < oldTotalDistance) {
                tmpVariable = variableOrdering[swapIndex1];
                variableOrdering[swapIndex1] = variableOrdering[swapIndex2];
                variableOrdering[swapIndex2] = tmpVariable;
                oldTotalDistance = totalDistance;
            } else {
                totalDistance = oldTotalDistance;
            }
        }
        return oldTotalDistance;
    }

    

    
}
