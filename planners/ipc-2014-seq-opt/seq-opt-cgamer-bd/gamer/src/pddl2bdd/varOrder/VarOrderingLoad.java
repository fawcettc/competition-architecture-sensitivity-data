/*
 * Gamer, a tool for finding optimal plans
 * Copyright (C) 2007-2012 by Peter Kissmann
 * modified by Alvaro Torralba (2012-2013)
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */
package pddl2bdd.varOrder;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import pddl2bdd.problem.Problem;

/**
 * Load the variableOrdering from a file. If there is an error reading the file,
 * uses the default ordering.
 *
 * @author Peter Kissmann and Alvaro Torralba
 */
public class VarOrderingLoad extends VariableOrdering {

    String file;

    public VarOrderingLoad(String file){
    this.file = file;
    }

    @Override
    public int[] variableOrdering(Problem problems) {
        List<Integer> ordering = new ArrayList<Integer>();
        boolean useDefaultOrdering = false;
        try {
            BufferedReader bufferedReader = new BufferedReader(
                    new FileReader(file));
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                ordering.add(Integer.parseInt(line));
            }
            bufferedReader.close();
        } catch (Exception e) {
            System.err.println("Warning: " + e.getMessage());
            e.printStackTrace(System.err);
            System.err.println("using default ordering");
            useDefaultOrdering = true;
        }
        int [] variableOrdering = new int[ordering.size()];
        if (!useDefaultOrdering) {
            List<List<String>> newPartitions = new ArrayList<List<String>>();
            int index = 0;
            for (int nextOrderingItem : ordering){
                newPartitions.add(problems.getPartitioning().get(nextOrderingItem));
                variableOrdering[index++] = nextOrderingItem;
            }
            //partitions = newPartitions;
        } else {
            for (int i = 0; i < variableOrdering.length; i++) {
                variableOrdering[i] = i;
            }
        }

        return variableOrdering;
    }

}
