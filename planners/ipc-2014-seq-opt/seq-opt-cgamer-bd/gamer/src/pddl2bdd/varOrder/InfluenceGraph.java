/*
 * Gamer, a tool for finding optimal plans
 * Copyright (C) 2007-2012 by Peter Kissmann
 * modified by Alvaro Torralba (2012-2013)
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */
package pddl2bdd.varOrder;

import java.util.LinkedList;
import java.util.List;
import pddl2bdd.problem.Action;
import pddl2bdd.problem.Problem;
import pddl2bdd.problem.VariablePartitioning;

/**
 *
 * @author Peter Kissmann and Alvaro Torralba
 */
public class InfluenceGraph {
    private boolean [][] influences;
    
    public InfluenceGraph(Problem problem) {
        this.calculateInfluences(problem, problem.getPartitioning());
    }

    public InfluenceGraph(Problem problem, VariablePartitioning variables) {
        this.calculateInfluences(problem, variables.getPartitionedVariables());
    }

    public InfluenceGraph(Problem problem, List<List<String>> partitions) {
        this.calculateInfluences(problem, partitions);
    }

    public boolean influence(int partition1, int partition2) {
        return influences[partition1][partition2];
    }

    // meaning: influences[a][b] = true if a influences b
    private boolean[][] calculateInfluences(Problem problem,
            List<List<String>> partitions) {

        List<String> allVars = new LinkedList<String>();
        List<Integer> partitionIndices = new LinkedList<Integer>();
        for (List<String> part : partitions) {
            //System.out.println("Influence partition: " + part);
            partitionIndices.add(allVars.size());
            allVars.addAll(part);
        }
        this.influences = new boolean[problem.getPartitioning().size()]
                                     [problem.getPartitioning().size()];

        int actionCounter = 0;
        for (Action currentAction : problem.getActions()) {
            LinkedList<Integer> effectIndices = new LinkedList<Integer>();
            currentAction.effectVariables(effectIndices, allVars, partitionIndices);
            LinkedList<Integer> preIndices = new LinkedList<Integer>();
            currentAction.preconditionVariables(preIndices, allVars, partitionIndices);

            actionCounter++;

            for (int influencingIndex : effectIndices) {
                for (int influencedIndex : effectIndices) {
                    if (influencingIndex != influencedIndex) {
                        influences[influencingIndex][influencedIndex] = true;
                    }
                }
            }
            for (int influencingIndex : preIndices) {
                for (int influencedIndex : effectIndices) {
                    if (influencingIndex != influencedIndex) {
                        influences[influencingIndex][influencedIndex] = true;
                    }
                }
            }
        }
//        System.out.println("Influence counter " + actionCounter);
        /*for (int i = 0; i  < this.influences.length; i++){
            for (int j = 0; j  < this.influences[i].length; j++){
                if(this.influences[i][j]){
                    Log.getLog().log( i + " influences " + j);
                }
            }
        }*/
        return influences;
    }


    public boolean[][] getInfluences() {
        return influences;
    }
}
