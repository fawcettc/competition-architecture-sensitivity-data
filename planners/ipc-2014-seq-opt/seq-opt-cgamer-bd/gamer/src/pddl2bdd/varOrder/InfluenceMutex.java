/*
 * Gamer, a tool for finding optimal plans
 * Copyright (C) 2007-2012 by Peter Kissmann
 * modified by Alvaro Torralba (2012-2013)
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */
package pddl2bdd.varOrder;

import java.util.Set;
import pddl2bdd.problem.Problem;

/**
 *
 * @author Alvaro Torralba
 */
public class InfluenceMutex {
    private boolean [][] influences;

    public InfluenceMutex(Problem problem) {
        this.calculateInfluences(problem);
    }

    public boolean influence(int partition1, int partition2) {
        return influences[partition1][partition2];
    }

    // meaning: influences[a][b] = true if a influences b
    private boolean[][] calculateInfluences(Problem problem) {
        this.influences = new boolean[problem.getPartitioning().size()]
                                     [problem.getPartitioning().size()];

        /*for(Set<String> mutex : problem.getMutex()){
            this.addInfluence(problem, mutex);
        }
        for(Set<String> mutex : problem.getInvariants()){
            this.addInfluence(problem, mutex);
        }*/
        return influences;
    }
    
    private void addInfluence(Problem problem, Set<String> mutex) {
        for(String s1 : mutex){
            int v1 = problem.getVariable(s1);
            for(String s2 : mutex){
                int v2 = problem.getVariable(s2);    
                if(v1 != 2){
                    this.influences[v1][v2] = true;
                    this.influences[v2][v1] = true;
                }
                
            }
        }
    }
    
    

    public boolean[][] getInfluences() {
        return influences;
    }
}
