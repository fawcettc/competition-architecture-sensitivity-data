/*
 * Gamer, a tool for finding optimal plans
 * Copyright (C) 2007-2012 by Peter Kissmann
 * modified by Alvaro Torralba (2012-2013)
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */

package pddl2bdd.deprecated;

import java.util.HashMap;
import java.util.List;
import net.sf.javabdd.BDD;
import net.sf.javabdd.BDDFactory;
import pddl2bdd.problem.Problem;
import pddl2bdd.problem.VariablePartitioning;
import pddl2bdd.transition.TManagerBuilder;
import pddl2bdd.transition.TransitionManager;

/**
 *
 * @author Peter Kissmann and Alvaro Torralba
 */
public class TListBuilder extends TManagerBuilder{

    private HashMap<Integer, List<BDD>> t;
    private HashMap<String, BDD> t_by_name;

    public TListBuilder(Problem problem,
            VariablePartitioning variables, List<String> params) {
        super(problem, variables);

    }


     @Override
    public void init(Problem problem, List<Integer> emptyPartitions,
             String usedActionsFilename) {
        // build the transition relation
        this.t = new HashMap<Integer, List<BDD>>();
        this.t_by_name = new HashMap<String, BDD>();

        //if(!loadTransitions("transitions/")){
            this.initTransitions(problem, emptyPartitions, usedActionsFilename);
         //   this.storeTransitions("transitions/");
        //}

    }
    
    @Override
    public TransitionManager getTransitionManager(boolean forward) {
        if (forward) {
            return new TransitionList(this, this.variables, forward, t, t_by_name);
        }else{
            return new TransitionList(this, this.variables, forward, t, t_by_name);
        }
    }

    public void initTransitions(Problem problem, List<Integer>  emptyPartitions,
            String usedActionsFilename){

       /*         Log.getLog().log("Initializing transition list builder.");


        boolean[] usedActions = new boolean[problem.getActions().size()];
        int usedActionIndex = 0;
        for (Action action : problem.getActions()){
            BDD actionBDD = action.createBDD(variables, emptyPartitions);
            if (actionBDD == null) {
                action.setUnused(true);
            } else {
                int cost = action.getCost();
                if(!t.containsKey(cost)){
                    t.put(cost, new LinkedList<BDD>());
                    actionNames.put(cost, new LinkedList<String>());
                }
                usedActions[usedActionIndex] = true;
                t.get(cost).addLast(actionBDD);
                t_by_name.put(action.getName(), actionBDD);
                actionNames.get(cost).addLast(action.getName());
            }
            usedActionIndex++;
        }

         for (Map.Entry<Integer, LinkedList<BDD>> actionsEntry : t.entrySet()) {
            if (actionsEntry.getValue().size() == 0) {
                actionNames.remove(actionsEntry.getKey());
                actionCosts.remove(actionsEntry.getKey());
            }
        }

         if(usedActionsFilename != null){
            FileWriter usedActionsWriter = null;
            try {
                usedActionsWriter = new FileWriter(usedActionsFilename);
                for (int i = 0; i < usedActions.length; i++) {
                    usedActionsWriter.append(usedActions[i] ? "1" : "0");
                }
                usedActionsWriter.append("\n");
                usedActionsWriter.flush();
                usedActionsWriter.close();
            } catch (Exception e) {
                System.err.println("Error: " + e.getMessage());
                e.printStackTrace(System.err);
            }
         }*/
    }

    public void storeTransitions(String path) {
        // write transition relation to disk (in case of A* search)
	String baseFilename = path + "transitionRelation_";
        for(int key : t.keySet()){
	    String baseFilename2 = baseFilename + key + "_";
	    int counter = 0;
            for(BDD bdd : t.get(key)){
		String filename = baseFilename2 + this.actionNames.get(key).get(counter);
		try {
		    variables.getFactory().save(filename, bdd);
		} catch (Exception e) {
		    System.err.println("Error: " + e.getMessage());
		    e.printStackTrace(System.err);
		    System.exit(1);
		}
		counter++;
	    }
	}
    }

    public boolean loadTransitions(String path) {
        return false;
    }


    @Override
    public void cleanup(){
        for(List<BDD> t_list : t.values()){
            for(BDD bdd : t_list){
                bdd.free();
            }
        }
    }
    
}
