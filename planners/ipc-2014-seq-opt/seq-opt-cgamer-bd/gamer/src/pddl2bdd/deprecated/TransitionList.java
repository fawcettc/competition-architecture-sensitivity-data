
/*
 * Gamer, a tool for finding optimal plans
 * Copyright (C) 2007-2012 by Peter Kissmann
 * modified by Alvaro Torralba (2012-2013)
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */
package pddl2bdd.deprecated;

import java.util.HashMap;
import java.util.List;
import net.sf.javabdd.BDD;
import pddl2bdd.problem.VariablePartitioning;
import pddl2bdd.transition.TManagerBuilder;
import pddl2bdd.transition.TransitionManager;
import pddl2bdd.util.Log;
import pddl2bdd.util.Maths;

/**
 * Stores the list of transitions ordered by cost.
 * 
  * @author Peter Kissmann and Alvaro Torralba
 */
public class TransitionList extends TransitionManager{

    TLImageStatistics imgstats;
    private HashMap<Integer, List<BDD>> t;
    private HashMap<String, BDD> t_by_name;

    public TransitionList(TManagerBuilder builder, VariablePartitioning variables,
            boolean forward, HashMap<Integer, List<BDD>> t,
            HashMap<String, BDD> t_by_name){
        super(builder, variables, forward);
        this.t = t;
        this.t_by_name = t_by_name;
    }

    /*@Override
    public BDD image(int index, BDD from) {
        imgstats = new TLImageStatistics();
        long totalTime = System.currentTimeMillis();
        BDD res = image(index, from, null);
        imgstats.addTotalTime(totalTime);
        Log.getLog().log(index + " cost transitions " + imgstats);
        return res;
    }*/

    @Override
    public BDD image(int index, BDD from, BDD conjunct, long maxTime, long maxNodes) {
        if (!forward) {
            //Original from is freed outside this method
           from = from.replace(this.getPairing());
           //This new from is free before returning this method
        }

        imgstats = new TLImageStatistics();
        BDD tmp1;
        BDD tmp2;
        long tStart = System.currentTimeMillis();
        List<BDD> t_i = t.get(index);
        int size = t_i.size();
        BDD[] array = new BDD[size];
        for (int i = 0; i < size; i++) {
            tmp1 = t_i.get(i).relprod(from, this.getVarSet());
            if(conjunct == null){
                array[i] = tmp1;
            }else{
                array[i] = tmp1.and(conjunct);
                tmp1.free();
            }
            if (System.currentTimeMillis() > maxTime)return null;
            
        }


        imgstats.addImageTime(tStart);
        long tDisjunction = System.currentTimeMillis();
        int prevRemainingElems;
        int remainingElems = size;
        while (remainingElems > 1) {
            prevRemainingElems = remainingElems;
            remainingElems = Maths.div2(prevRemainingElems);
            for (int i = 0; i < remainingElems; i++) {
                if (System.currentTimeMillis() > maxTime) return null;
                array[i] = array[2 * i];
                if (i < remainingElems - 1 || (2 * i) + 1 < prevRemainingElems) {
                    tmp1 = array[i];
                    tmp2 = array[(2 * i) + 1];
                    array[i] = tmp1.or(tmp2);
                    tmp1.free();
                    tmp2.free();
                }
            }
        }
        imgstats.addDisjunctionTime(tDisjunction);
        BDD res = array[0];
        if (forward) {
           res = array[0].replace(this.getPairing());
            array[0].free();
        }else{
            from.free();
        }
        imgstats.addTotalTime(tStart);
        Log.getLog().log(index + " cost transitions " + imgstats);
        return res; 
    }

    /*@Override
    public HashMap<Integer, BDD> image(BDD from) {
        imgstats = new TLImageStatistics();
        long totalTime = System.currentTimeMillis();
        HashMap<Integer, BDD> res =  image(from, null);
        imgstats.addTotalTime(totalTime);
        Log.getLog().log("cost transitions " + imgstats);
        return res;
    }*/

    @Override
    public HashMap<Integer, BDD> image(BDD from, BDD conjunct, long maxTime, long maxNodes) {
        HashMap<Integer, BDD> res = new HashMap<Integer, BDD>();
        for(int cost : t.keySet()){
            if(cost != 0){
                if (System.currentTimeMillis() > maxTime) return null;
                BDD resBDD = image(cost, from, conjunct);
                if(res != null && !resBDD.equals(variables.getZeroBDD())){
                    res.put(cost, resBDD);
                }
            }
        }
        return res;
    }

    @Override
    public void free() {
        
    }

  

    @Override
    public BDD applyAction(BDD bdd, String action) {
        if(!forward){
            bdd = bdd.replace(this.getPairing());
        }
        BDD bdd_action = t_by_name.get(action);
        
        BDD tmp = bdd_action.relprod(bdd, this.getVarSet());

        BDD res = tmp;
        if(forward){
            res = tmp.replace(this.getPairing());
            tmp.free();
        }else{
            bdd.free();
        }
        
        return res;
    }

    private class TLImageStatistics {

        private long totalTime;
        private long disjunctionTime;
        private long imageTime;
        
         public TLImageStatistics() {
            totalTime = 0L;
            disjunctionTime = 0L;
            imageTime = 0L; 
        }

        public void addDisjunctionTime(long finalTime) {
            this.disjunctionTime += (System.currentTimeMillis() - finalTime);
        }

        public void addImageTime(long finalTime) {
            this.imageTime += (System.currentTimeMillis() - finalTime);
        }

        public void addTotalTime(long finalTime) {
            this.totalTime += (System.currentTimeMillis() - finalTime);
        }

        @Override
        public String toString() {
            return "TLStatistics{" + "totalTime=" + totalTime + ", disjunctionTime=" + disjunctionTime + ", imageTime=" + imageTime + '}';
        }
    }
}
