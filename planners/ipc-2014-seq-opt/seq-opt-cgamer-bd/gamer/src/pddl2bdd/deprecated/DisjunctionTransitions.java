/*
 * Gamer, a tool for finding optimal plans
 * Copyright (C) 2007-2012 by Peter Kissmann
 * modified by Alvaro Torralba (2012-2013)
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */
package pddl2bdd.deprecated;

/**
 * It tries to apply disjunction of "closer" operators.
 * It stops when there are no pair of operators with min_distance
 *
 * @author Peter Kissmann and Alvaro Torralba
 */
public class DisjunctionTransitions {
/*
    private int MAX_DISTANCE = 100;
    

    public DisjunctionTransitions() {
    }

    public ArrayList<Transition> apply(ArrayList<Transition> transitions) {
        int MAX_SIZE = transitions.get(0).getVariables().numBinaryVariables()*100;
        ArrayList <Transition> resTransitions = new ArrayList<Transition>();
        ArrayList<Set<Integer>> varSets = new ArrayList<Set<Integer>>();
        ArrayList<Integer> tSize = new ArrayList<Integer>();
        for (int i = 0; i < transitions.size(); i++) {
            resTransitions.add(new Transition(transitions.get(i)));
            varSets.add(transitions.get(i).getEffectVarsSet());
            tSize.add(transitions.get(i).getTotalBDD().nodeCount());
//            Log.getLog().log("Transition size: " + transitions.get(i).getTotalBDD().nodeCount());
        }

        int[][] varSetDistance = new int[varSets.size()][varSets.size()];
        Set<Integer> distances = new HashSet<Integer>();
        for (int i = 0; i < varSets.size(); i++) {
            Set<Integer> setI = varSets.get(i);
            varSetDistance[i][i] = 0;
            for (int j = i + 1; j < varSets.size(); j++) {
                int distance = this.computeDistance(setI, varSets.get(j));
                varSetDistance[i][j] = distance;
                varSetDistance[j][i] = distance;
                distances.add(distance);
                Log.getLog().log("Distance " + i + " " + j + ": " + distance);
            }
        }

        Log.getLog().log("Disjunction Transitions");
        do {
            int minDistance = Collections.min(distances);
            distances.remove(minDistance);
            Log.getLog().log("MinDistance: " + minDistance);
            if (minDistance > MAX_DISTANCE) {
                break;
            }

            for (int i = 0; i < varSetDistance.length; i++) {
                if (varSets.get(i).isEmpty() || tSize.get(i) > MAX_SIZE) {
                    continue;
                }
                for (int j = i + 1; j < varSetDistance.length; j++) {
                    if (varSets.get(j).isEmpty() || tSize.get(j) > MAX_SIZE) {
                        continue;
                    }
                    if (varSetDistance[i][j] == minDistance) {
                        //Unify i and j
                        //Log.getLog().log(i + " " + j);
                        Transition ti = resTransitions.get(i);
                        Transition tj = resTransitions.get(j);
                        varSets.get(i).addAll(varSets.get(j));
                        varSets.get(j).clear();
                        resTransitions.set(j, null);
                        System.out.print("Unify " + i + "(" + ti.getTotalBDD().nodeCount() + ")" +
                                " and " + j + "(" + tj.getTotalBDD().nodeCount() + ")"+ ": ");
                        Transition newTransition = new Transition(Arrays.asList(ti, tj) );
                        ti.cleanup();
                        tj.cleanup();
                        Log.getLog().log(newTransition.getTotalBDD().nodeCount());
                        resTransitions.set(i, newTransition);
                        tSize.set(i, newTransition.getTotalBDD().nodeCount());
                        for (int k = 0; k < varSets.size(); k++) {
                            if (k != i && !varSets.get(k).isEmpty()) {
                                int distance = this.computeDistance(varSets.get(i), varSets.get(k));
                                varSetDistance[i][k] = distance;
                                varSetDistance[k][i] = distance;
                                distances.add(distance);
                            }
                        }
                        break;
                    }
                }
            }


        } while (!distances.isEmpty());

        ArrayList<Transition> res = new ArrayList<Transition>();
        for (Transition tr : resTransitions) {
            if(tr != null){
                res.add(tr);
            }
        }
        
        return res;
    }


    public ArrayList<Transition> apply2(ArrayList<Transition> transitions) {
        int MAX_SIZE = transitions.get(0).getVariables().numBinaryVariables()*100;
        ArrayList <Transition> resTransitions = new ArrayList<Transition>();

        ArrayList<Integer> tSize = new ArrayList<Integer>();
        for (int i = 0; i < transitions.size(); i++) {
            resTransitions.add(new Transition(transitions.get(i)));
            tSize.add(transitions.get(i).getTotalBDD().nodeCount());
        }

        Log.getLog().log("Disjunction Transitions");
        boolean changes;
        do {
            changes = false;
            for (int i = 0; i < transitions.size(); i++) {
                if (resTransitions.get(i) == null || tSize.get(i) > MAX_SIZE) {
                    continue;
                }
                for (int j = i + 1; j < transitions.size(); j++) {
                    if (resTransitions.get(j) == null || tSize.get(j) > MAX_SIZE) {
                        continue;
                    }
                    changes =true;
                        //Unify i and j
                        //Log.getLog().log(i + " " + j);
                        Transition ti = resTransitions.get(i);
                        Transition tj = resTransitions.get(j);
                        resTransitions.set(j, null);
                        System.out.print("Unify " + i + "(" + ti.getTotalBDD().nodeCount() + ")" +
                                " and " + j + "(" + tj.getTotalBDD().nodeCount() + ")"+ ": ");
                        Transition newTransition = new Transition(Arrays.asList(ti, tj));
                        ti.cleanup();
                        tj.cleanup();
                        Log.getLog().log(newTransition.getTotalBDD().nodeCount());
                        resTransitions.set(i, newTransition);
                        tSize.set(i, newTransition.getTotalBDD().nodeCount());
                        
                        break;
                    }
                
            }


        } while (changes);

        ArrayList<Transition> res = new ArrayList<Transition>();
        for (Transition tr : resTransitions) {
            if(tr != null){
                res.add(tr);
            }
        }

        return res;
    }

    protected int computeDistance(Set<Integer> set1, Set<Integer> set2) {
        Set<Integer> unionSet = new HashSet<Integer>(set1);
        unionSet.addAll(set2);
        int distance = Math.abs(set1.size() + set2.size() - unionSet.size() * 2);
        return distance;
    }*/
}
