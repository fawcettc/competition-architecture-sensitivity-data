/*
 * Gamer, a tool for finding optimal plans
 * Copyright (C) 2007-2012 by Peter Kissmann
 * modified by Alvaro Torralba (2012-2013)
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */
package pddl2bdd.search;

import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import net.sf.javabdd.BDD;
import pddl2bdd.problem.Problem;
import pddl2bdd.problem.VariablePartitioning;
import pddl2bdd.transition.TManagerBuilder;
import pddl2bdd.transition.TransitionManager;
import pddl2bdd.util.Log;

/**
 * Abstract class shared for all the AStar versions (Matrix, List and Fold)
 *
 * @author Peter Kissmann and Alvaro Torralba
 */
public abstract class AbstractAStar extends AbstractSolver{
    /**
     * If there is less states, we generate the successors for all the bucket
     * without applying the heuristic.
     * When this is activated, PARTITION_FORWARD_REACHED have to be disabled
     */
    /*protected int MIN_STATES_PARTITION_H = 0;
    if (MIN_STATES_PARTITION_H > 0
                && bucket.satCount(this.variables.getCube()) < MIN_STATES_PARTITION_H) {
            return bucket.id();
        }*/
    protected boolean PARTITION_FORWARD_REACHED = true;
    
    protected TransitionManager tmanager, tManagerBackward;
    
    /**
     * All the states that have been reached.
     */
    protected HashMap<Integer, BDD> forwardReached;
    /**
     * Auxiliar structure for solution reconstruction.
     */
    protected HashMap<Integer, ArrayList<BDD>> solutionBDD;

    protected PDBHeuristic heuristic;

    /**
     * Creates new BDDs for the given domain and problem. <br>
     * <br>
     * In this constructor new BDDs for the given domain and problem will be
     * created. <br>
     * First, the specified number of variables will be allocated. It is
     * important that the number of variables equals twice the number of
     * variables needed for one state, as variables are needed for both, one
     * state and its successor (or predecessor in backward search). <br>
     * Next, BDDs for the transition relation will be generated. Here the
     * actions from the domain will be taken and for each action a BDD will be
     * created as the disjunction takes quite an amount of time sometimes. This
     * has the advantage that every transition-BDD corresponds directly to one
     * action and thus an action-plan can be returned after a search. <br>
     * Next, a BDD for the goal-description is created. First of all, a BDD for
     * the given goal from the problem will be generated. <br>
     * Finally, a BDD for the initial state, given in the problem, is created.
     *
     * @param domain
     *            The root of the domain, representedhas as a tree
     * @param problem
     *            The root of the problem, represented as a Tree
     * @param partitions
     *            The partition of the variables. In this class we do not use
     *            true boolean variables to represent the states, but put as
     *            many as possible together - variables that are mutually
     *            exclusive can be together in one partition. If exactly one of
     *            the variables in one partition can be true, everything is
     *            fine. If it also can happen that none of these are true, we
     *            need an extra variable (something like "none-of-these") to
     *            represent the case that none of the variables of this
     *            partition are true. This extra variable has to be at the last
     *            one of the partition. Note also that all variable-names have
     *            to be unique.
     * @param numberOfVars
     *            The number of boolean variables to be used (equals twice the
     *            number of boolean variables needed for one state).
     * @param library
     *            The BDD library used.
     */
    public AbstractAStar(Problem problem, VariablePartitioning variables,
            TManagerBuilder tManagerBuilder, PDBHeuristic heuristic) {
        super(problem, variables, tManagerBuilder);
        
        this.tmanager = tManagerBuilder.getTransitionManager(true);
        this.tManagerBackward = tManagerBuilder.getReconstructTransitionManager(false);
      
        this.solutionBDD = new HashMap<Integer, ArrayList<BDD>>();
        this.forwardReached = new HashMap<Integer, BDD>();
       
        this.heuristic = heuristic;

        Log.getLog().log("   done.");        
    }
    

    /**
     * Cleans up in that it de-references all BDDs.
     */
    @Override
    public void cleanup() {
        super.cleanup();
        this.free();
        
        for (BDD b : this.forwardReached.values()) {
            b.free();
        }

        this.tManagerBackward.free();
        this.tmanager.free();
        
        this.heuristic.cleanup();
        
        for(List<BDD> bddList : solutionBDD.values()){
            for(BDD bdd : bddList){
                bdd.free();
            }
        }
    }

    public abstract void free();

    @Override
    public abstract void search();

   
    protected BDD getForwardReached(int h) {
        if (!PARTITION_FORWARD_REACHED) {
            h = 0;
        }
        BDD bdd = this.forwardReached.get(h);
        if (bdd == null) {
            bdd = factory.zero();
            this.forwardReached.put(h, bdd);
        }

        //Log.getLog().log("Check forward reached " + h + ": " + bdd.satCount(cube));
        return bdd;
    }

    protected void addForwardReached(int h, BDD newbdd) {
        if (!PARTITION_FORWARD_REACHED) {
            h = 0;
        }
        BDD bdd = this.forwardReached.get(h);
        if (bdd == null) {
            this.forwardReached.put(h, newbdd.id());
        } else {
            this.forwardReached.put(h, bdd.or(newbdd));
            bdd.free();
        }
        // Log.getLog().log("Add forward reached " + h + ": " + this.forwardReached.get(h).satCount(cube));
    }

    /**
     *
     * Process a bucket and all the reachable states with 0 actions. Inserts them
     * in the solutionBDD structure.
     * Returns the union of all the reachable states with 0 actions in the bucket
     *
     * @param gmin
     * @param factory
     * @param fgArrayList
     * @return
     */
    protected BDD put_solution(int gmin, ArrayList<BDD> fgArrayList) {
        BDD tmp2;
        BDD tmp1 = factory.zero();
        if (solutionBDD.containsKey(gmin)) {
            for (int i = 0; i < solutionBDD.get(gmin).size(); i++) {
                tmp2 = tmp1;
                tmp1 = tmp2.or(solutionBDD.get(gmin).get(i));
                tmp2.free();
            }
        }
        tmp2 = tmp1.not();
        tmp1.free();

        BDD forwardFrontier = factory.zero();
        for (int i = 0; i < fgArrayList.size(); i++) {
            tmp1 = fgArrayList.get(i);
            fgArrayList.set(i, tmp1.and(tmp2));
            tmp1.free();
            tmp1 = forwardFrontier;
            forwardFrontier = tmp1.or(fgArrayList.get(i));
            tmp1.free();
        }
        tmp2.free();

        if (solutionBDD.containsKey(gmin)) {
            solutionBDD.get(gmin).addAll(fgArrayList);
        } else {
            solutionBDD.put(gmin, fgArrayList);
        }

        return forwardFrontier;
    }

    protected void reconstructPlanAStar(int index, BDD start) {
        
        LinkedList<String> solution = new LinkedList<String>();
        Log.getLog().log("   reconstructing cheapest plan ...");
        for (int i = solutionBDD.size() - 1; i > index; i--) {
            ArrayList<BDD> forwardLayer = solutionBDD.get(i);
            for (int j = forwardLayer.size() - 1; j >= 0; j--) {
                forwardLayer.remove(j).free();
            }
            solutionBDD.remove(i);
        }

        
        reconstructPlanBackwardAStar(index, start, solution);

        for (int k : solutionBDD.keySet()) {
            while (!solutionBDD.get(k).isEmpty()) {
                solutionBDD.get(k).remove(0).free();
            }
        }

        // print plan
        try {
            FileWriter writer = new FileWriter("plan_output");
            int initialOutputCapacity = 10000;
            StringBuilder output = new StringBuilder(initialOutputCapacity);
            int counter = 0;
            while (!solution.isEmpty()) {
                String nextAction = solution.removeLast();
                String nextActionParts[] = nextAction.split("[.]");
                output.append(counter++).append(": (").append(nextActionParts[0]);
                for (int i = 1; i < nextActionParts.length; i++) {
                    output.append(" ").append(nextActionParts[i]);
                }
                output.append(")\n");
                Log.getLog().log("      (" + nextAction + ")");
                if (output.length() > initialOutputCapacity) {
                    writer.write(output.toString());
                    output = output.delete(0, output.length());
                }
            }
            writer.write(output.toString());
            output = output.delete(0, output.length());
            writer.close();
        } catch (Exception e) {
            System.err.println("Error: " + e.getMessage());
            e.printStackTrace(System.err);
            System.exit(1);
        }
        Log.getLog().log("   done.");
        
    }

    protected void reconstructPlanBackwardAStar(int index, BDD start,
            LinkedList<String> solution) {
        BDD tmp1;
        BDD tmp2;
        BDD intermediate;
        BDD newStart = factory.zero();
        int newIndex = -1;
        boolean stop = false;
        boolean totalStop = false;

        if (solutionBDD.get(index) != null) {
            for (int i = 0; i < solutionBDD.get(index).size(); i++) {
                tmp1 = solutionBDD.get(index).get(i).and(start);
                if (!tmp1.equals(factory.zero())) {
                    tmp1.free();
                    
                    while(i < solutionBDD.get(index).size()){
                        solutionBDD.get(index).remove(i).free();
                    }
                    break;
                }
            }
        }
        List<Integer> actionCosts = this.tManagerBuilder.getActionCosts();

        // calculate plan
        if (this.tManagerBuilder.hasZeroCostOperators()) {
            actionCosts.add(0, 0);
        }
        Collections.reverse(actionCosts);

        for (int d : actionCosts) {
            if (d > index) {
                continue;
            }
            newIndex = index - d;
            if (solutionBDD.get(newIndex) != null) {
                for(String action : this.tManagerBuilder.getActionNames().get(d)){
                    intermediate = tManagerBackward.applyAction(start, action);
                    for (BDD solBDD : solutionBDD.get(newIndex)) {
                        newStart = intermediate.and(solBDD);
                        if (!newStart.equals(factory.zero())) {
                            tmp2 = intermediate.and(init);
                            if (!tmp2.equals(factory.zero())) {
                                tmp2.free();
                                totalStop = true;
                            }

                            solution.addLast(action);
                            stop = true;
                            break;
                        }
                    }
                    intermediate.free();
                    if (stop) {
                        break;
                    }
                }
                if (stop) {
                    break;
                }
            }
        }
        start.free();
        if (!totalStop) {
            reconstructPlanBackwardAStar(newIndex, newStart, solution);
        } else {
            newStart.free();
        }
    }    
    
}