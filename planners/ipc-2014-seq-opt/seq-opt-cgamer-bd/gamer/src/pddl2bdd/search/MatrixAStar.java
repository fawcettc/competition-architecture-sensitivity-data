/*
 * Gamer, a tool for finding optimal plans
 * Copyright (C) 2007-2012 by Peter Kissmann
 * modified by Alvaro Torralba (2012-2013)
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */
package pddl2bdd.search;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import net.sf.javabdd.BDD;
import pddl2bdd.problem.Problem;
import pddl2bdd.problem.VariablePartitioning;
import pddl2bdd.transition.TManagerBuilder;
import pddl2bdd.util.Log;

/**
 * 
 * @author Peter Kissmann and Alvaro Torralba
 */
public class MatrixAStar extends AbstractAStar {

    public MatrixAStar(Problem problem,VariablePartitioning variables,
            TManagerBuilder tManager, PDBHeuristic heuristic) {
        super(problem, variables, tManager, heuristic);
    }

    @Override
    public void free() {

    }

    @Override
    public void search() {
        Log.getLog().log("Using Matrix A*");
        HashMap<Integer, HashMap<Integer, BDD>> searchSpace;
        HashMap<Integer, BDD> fDiagonal;
        
        BDD tmp1;
        BDD intersection;
        BDD forwardFrontier;

        int fmin = this.heuristic.getH(init);

        if (fmin < 0) {
            Log.getLog().log("Error: initial state not found in pattern database!");
            System.exit(1);
        }
        Log.getLog().log("starting f-value: " + fmin);
        fDiagonal = new HashMap<Integer, BDD>();
        fDiagonal.put(0, init.id());
        searchSpace = new HashMap<Integer, HashMap<Integer, BDD>>();
        searchSpace.put(fmin, fDiagonal);

        intersection = init.and(replacedGoal);

        while (intersection.equals(factory.zero())) {
            fmin = Collections.min(searchSpace.keySet());
            Log.getLog().log("f: " + fmin);
            fDiagonal = searchSpace.get(fmin);

            int gmin;
            while (!fDiagonal.isEmpty()) {
                gmin = Collections.min(fDiagonal.keySet());
                Log.getLog().log("  g: " + gmin);

                ArrayList<BDD> fgArrayList = new ArrayList<BDD>();
                fgArrayList.add(fDiagonal.remove(gmin));
                HashMap<Integer, BDD> temp = searchStep(fgArrayList, fmin - gmin);

                forwardFrontier = put_solution(gmin, fgArrayList);

                if (gmin == fmin) {
                    intersection = replacedGoal.and(forwardFrontier);
                    if (!intersection.equals(factory.zero())) {
                        forwardFrontier.free();
                                                
                        Iterator<Integer> searchSpaceIt = searchSpace.keySet().iterator();
                        while (searchSpaceIt.hasNext()) {
                            HashMap<Integer, BDD> searchLayer = searchSpace.get(searchSpaceIt.next());
                            Iterator<Integer> layerIt = searchLayer.keySet().iterator();
                            while (layerIt.hasNext()) {
                                searchLayer.get(layerIt.next()).free();
                            }
                        }
                        Log.getLog().log("   cheapest plan has cost of " + gmin);
                        reconstructPlanAStar(gmin, intersection.replace(variables.getS2sp()));
                        intersection.free();
                        return;
                    }
                }
                forwardFrontier.free();

                for (int d : temp.keySet()) {
                    if (temp.get(d).equals(factory.zero())) {
                        continue;
                    }
                    for (int dist : this.heuristic.getOrderedHeuristicValues()) {
                        BDD lookup = this.heuristic.getBDD(temp.get(d), dist);
                        if (lookup.equals(factory.zero())) {
                            continue;
                        }
                        int newDist = dist;
                        if (newDist < fmin - (gmin + d)) {
                            System.err.print("Error: successor in bucket ("
                                    + (gmin + d) + ", " + newDist
                                    + ") left of f-diagonal (" + fmin
                                    + ") => heuristic not consistent!");
                            System.exit(1);
                        }
                        HashMap<Integer, BDD> succDiagonal;
                        if (searchSpace.containsKey(gmin + d + newDist)) {
                            succDiagonal = searchSpace.get(gmin + d + newDist);
                            if (succDiagonal.containsKey(gmin + d)) {
                                tmp1 = succDiagonal.remove(gmin + d);
                                succDiagonal.put(gmin + d, tmp1.or(lookup));
                                tmp1.free();
                                lookup.free();
                            } else {
                                succDiagonal.put(gmin + d, lookup);
                            }
                        } else {
                            succDiagonal = new HashMap<Integer, BDD>();
                            succDiagonal.put(gmin + d, lookup);
                            searchSpace.put(gmin + d + newDist, succDiagonal);
                        }
                    }
                    temp.get(d).free();
                    //temp.set(d, factory.zero());
                }
                // }
                // gmin++;
            }
            searchSpace.remove(fmin);
            // fmin++;
        }
        intersection.free();
        // cleanup searchspace!
    }

    private HashMap<Integer, BDD> searchStep(ArrayList<BDD> frontier, int h) {
        BDD tmp1;
        BDD tmp2;
        BDD negatedReached;
        BDD to;
        BDD totalFrontier = frontier.get(0).id();
        BDD oldFrontier;

        negatedReached = getForwardReached(h).not();
        if (tManagerBuilder.hasZeroCostOperators()) {
            while (true) {  
                double time_now = (System.currentTimeMillis() - time_init)
                        / 1000.0;
                Log.getLog().log("    bucket size: "
                        + frontier.get(frontier.size() - 1).satCount(this.variables.getCube())
                        + "[" + time_now + "s]");
                to = tmanager.image(0, frontier.get(frontier.size() - 1));
                tmp1 = to.and(negatedReached);
                to.free();
                oldFrontier = totalFrontier;
                tmp2 = oldFrontier.not();
                BDD tmp3 = tmp1.and(tmp2);
                if (tmp3.equals(factory.zero())) {
                    tmp1.free();
                    tmp2.free();
                    break;
                }
                frontier.add(tmp3);
                tmp1.free();
                tmp2.free();
                //tmp3.free();
                
                totalFrontier = oldFrontier.or(frontier.get(frontier.size() - 1));
                oldFrontier.free();
            }
        } else {
            tmp1 = totalFrontier;
            totalFrontier = tmp1.and(negatedReached);
            tmp1.free();
        }
        negatedReached.free();

        addForwardReached(h, totalFrontier);

        int d;
        
        double time_now = (System.currentTimeMillis() - time_init) / 1000.0;
        Log.getLog().log("    bucket size: " + totalFrontier.satCount(variables.getCube())
                + "[" + time_now + "s]");

        HashMap<Integer, BDD> forwardBDDs = tmanager.image(totalFrontier);
        
        totalFrontier.free();
        return forwardBDDs;
    }
}
