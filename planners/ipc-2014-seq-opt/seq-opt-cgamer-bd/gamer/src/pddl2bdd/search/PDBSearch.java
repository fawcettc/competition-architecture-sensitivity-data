/*
 * Gamer, a tool for finding optimal plans
 * Copyright (C) 2007-2012 by Peter Kissmann
 * modified by Alvaro Torralba (2012-2013)
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */
package pddl2bdd.search;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import net.sf.javabdd.BDD;
import pddl2bdd.problem.Problem;
import pddl2bdd.problem.VariablePartitioning;
import pddl2bdd.transition.TManagerBuilder;
import pddl2bdd.transition.TransitionManager;
import pddl2bdd.util.Log;

/**
 * Code for PDB generation (backward search of an abstract state space)
 * 
 * @author Peter Kissmann and Alvaro Torralba
 */
public class PDBSearch extends AbstractSolver {

    TransitionManager transitionManager;
    String pdbFileName;
    private List<BDD> superPDB;
    private int superPDBSize = 0;
    private List<BDD> mutexBDDs;
    NotClosedType mutexType;
    private boolean useMutexBDDs;
    private boolean partialPDB;
    private BDD backwardReached;
    private HashMap<Integer, BDD> backwardBDDs;
    private HashMap<Integer, BDD> closedBDDs;
    //private BDDVarSet abstractedPreVars;

    public PDBSearch(Problem problem, VariablePartitioning variables,
            TManagerBuilder tManagerBuilder,
            List<Integer> emptyPartitions, String pdbFilename, NotClosedType mutexType, boolean useMutexBDDs, boolean partialPDB) {
        super(problem, variables, tManagerBuilder, emptyPartitions, pdbFilename);
        this.pdbFileName = pdbFilename;
        this.partialPDB = partialPDB;
        this.mutexType = mutexType;
        Log.getLog().log(Level.INFO, "PDB SEARCH excluding: " + emptyPartitions);
        /*for (int var : emptyPartitions){
        Log.getLog().log("Ignoring: " + variables.getPartitionedVariables().get(var));
        }*/
        this.transitionManager = tManagerBuilder.getTransitionManager(false);
        this.useMutexBDDs = useMutexBDDs;
        //Get mutexBDDs from fw mutexes
        this.mutexBDDs = this.variables.getMutex(false).getMutexBDDs(emptyPartitions);
    }

    @Override
    public void cleanup() {
        if (this.transitionManager != null) {
            this.transitionManager.free();
        }
        if (mutexBDDs != null) {
            for (BDD bdd : mutexBDDs) {
                bdd.free();
            }
        }

        if (superPDB != null) {
            for (BDD bdd : superPDB) {
                bdd.free();
            }
        }

        for (BDD bdd : this.backwardBDDs.values()) {
            bdd.free();
        }
        for (BDD bdd : this.closedBDDs.values()) {
            bdd.free();
        }

        this.backwardReached.free();
        super.cleanup();
    }

    private void searchStep(int index) {
        if (this.tManagerBuilder.hasZeroCostOperators()) {
            BDD tmp1 = backwardBDDs.remove(index);
            BDD frontier = notClosed(tmp1);
            backwardBDDs.put(index, frontier);
            tmp1.free();

            while (!frontier.equals(factory.zero())) {
                BDD to = this.transitionManager.image(0, frontier);
                BDD oldFrontier = backwardBDDs.remove(index);
                BDD tmp2 = oldFrontier.not();
                frontier = to.and(tmp2);
                to.free();
                tmp2.free();

                if (useMutexBDDs) {
                    for (BDD bdd : this.mutexBDDs) {
                        BDD tmp = frontier;
                        frontier = notClosed(frontier, bdd,
                                mutexType, Long.MAX_VALUE, Long.MAX_VALUE);
                        tmp.free();
                    }
                }
                backwardBDDs.put(index, oldFrontier.or(frontier));
                oldFrontier.free();
            }
        }
        BDD tmp1 = backwardBDDs.remove(index);
        backwardBDDs.put(index, notClosed(tmp1));
        tmp1.free();
        if (backwardReached.equals(factory.zero())) {
            backwardReached = backwardBDDs.get(index).id();
        } else {
            tmp1 = backwardReached;
            backwardReached = tmp1.or(backwardBDDs.get(index));
            tmp1.free();
        }

        BDD frontier = backwardBDDs.get(index);
        HashMap<Integer, BDD> resImage = this.transitionManager.image(frontier);
        for (int d : resImage.keySet()) {
            BDD to = resImage.get(d);
            tmp1 = notClosed(to);
            to.free();
            to = tmp1;

            if (!tmp1.equals(factory.zero())) {
                if (backwardBDDs.containsKey(index + d)) {
                    tmp1 = backwardBDDs.remove(index + d);
                    backwardBDDs.put(index + d, tmp1.or(to));
                    tmp1.free();
                    to.free();
                } else {
                    backwardBDDs.put(index + d, to);
                }
            }
        }
    }

    /**
     * Builds a PDB and computes the average heuristic value if needed
     *
     * @param calculateAverage
     * @return
     */
    public double buildPDB(boolean calculateAverage) {
        //long startingTime = System.currentTimeMillis();
        FileWriter existingPDBs = null;
        FileWriter maxPDB = null;
        if (partialPDB) {
            try {
                existingPDBs = new FileWriter(pdbFileName + "ExPDBs.txt");
                maxPDB = new FileWriter(pdbFileName + "MaxPDB.txt");
            } catch (Exception e) {
                System.err.println("Error: " + e.getMessage());
                e.printStackTrace(System.err);
                System.exit(1);
            }
        }

        backwardBDDs = new HashMap<Integer, BDD>();
        closedBDDs = new HashMap<Integer, BDD>();
        //boolean useSuperPDB = true;

        int index = 0;
        // data.backwardBDDs.setSize(maxCost + 1);
        backwardBDDs.put(index, replacedGoal.id());
        // data.backwardBDDs.set(index, trueGoal.id());
        // for (int i = 1; i < maxCost + 1; i++)
        // data.backwardBDDs.set(i, factory.zero());
        backwardReached = factory.zero();
        // while (index < data.backwardBDDs.size()) {
        while (!backwardBDDs.isEmpty()) {
            index = Collections.min(backwardBDDs.keySet());
            Log.getLog().log(Level.INFO, "   step: " + index);
            if (partialPDB) {
                //Log.getLog().log("   step: " + index);
                try {
                    maxPDB.write((index - 1) + "\n");
                    maxPDB.flush();
                } catch (Exception e) {
                    System.err.println("Error: " + e.getMessage());
                    e.printStackTrace(System.err);
                    System.exit(1);
                }
            }
            searchStep(index);
            BDD closed = backwardBDDs.remove(index);
            if (partialPDB) {
                try {
                    String fullPDBFileName = pdbFileName + "PDB_" + index;
                    factory.save(fullPDBFileName, closed);
                    existingPDBs.write(fullPDBFileName + "\n");
                    existingPDBs.flush();
                    maxPDB.write(index + "\n");
                    maxPDB.flush();
                } catch (Exception e) {
                    System.err.println("Error: " + e.getMessage());
                    System.exit(1);
                }
            }
            this.closedBDDs.put(index, closed);
            index++;
        }

        if (partialPDB) {
            try {
                existingPDBs.write("finished\n");
                existingPDBs.flush();
            } catch (IOException e) {
                System.err.println("Error writing existingPDBs");
                System.exit(-1);
            }
        }
        // Log.getLog().log("   final #nodes: " +
        // data.backwardReached.nodeCount()
        // + "; #sat: " + currentSize);
        double averageHeuristic = 0;
        if (calculateAverage) {
            averageHeuristic = calculateAverage();
            Log.getLog().log("average heuristic value: " + averageHeuristic);
        }
        return averageHeuristic;
    }

    public double calculateAverage() {
        double averageHeuristic = 0;
        double heuristicSize = 0;
        for (int index : this.closedBDDs.keySet()) {
            double currentSize = (double) this.closedBDDs.get(index).satCount(variables.getCubep());
            // Log.getLog().log(currentSize);
            //Log.getLog().log("LALALA: " + index + ": " + currentSize);
            averageHeuristic += currentSize * index;
            heuristicSize += currentSize;
        }
        BDD notReached = backwardReached.not();
        double notClosedSize = (long) notReached.satCount(variables.getCube());
        //Log.getLog().log("LALALA: NotReached" + Collections.max(this.closedBDDs.keySet()) * 2 + ": " + notClosedSize);
        heuristicSize += notClosedSize;
        notReached.free();
        averageHeuristic += notClosedSize * Collections.max(this.closedBDDs.keySet());
        //Log.getLog().log("LALALA: Average" +averageHeuristic + ": " + heuristicSize);
        return averageHeuristic / heuristicSize;

    }

    public void writeToFile() {
        Log.getLog().log("Writing: " + pdbFileName);
        FileWriter existingPDBs = null;
        FileWriter maxPDB = null;
        try {
            existingPDBs = new FileWriter(pdbFileName + "ExPDBs.txt");
            maxPDB = new FileWriter(pdbFileName + "MaxPDB.txt");
        } catch (Exception e) {
            System.err.println("Error: " + e.getMessage());
            e.printStackTrace(System.err);
            System.exit(1);
        }

        for (int index : this.closedBDDs.keySet()) {
            try {
                maxPDB.write((index - 1) + "\n");
                maxPDB.flush();
            } catch (Exception e) {
                System.err.println("Error: " + e.getMessage());
                e.printStackTrace(System.err);
                System.exit(1);
            }
            String fullPDBFileName = pdbFileName + "PDB_" + index;
            try {
                factory.save(fullPDBFileName, closedBDDs.get(index));
                existingPDBs.write(fullPDBFileName + "\n");
                existingPDBs.flush();
                maxPDB.write(index + "\n");
                maxPDB.flush();
            } catch (IOException e) {
                System.err.println("Error: " + e.getMessage());
                e.printStackTrace(System.err);
                System.exit(1);
            }
        }

        try {
                existingPDBs.write("finished\n");
                existingPDBs.flush();
            } catch (IOException e) {
                System.err.println("Error writing existingPDBs");
                System.exit(-1);
            }
        
    }

    @Override
    public void search() {
        this.buildPDB(false);
    }

    private BDD notClosed(BDD from) {
        long startTime = System.currentTimeMillis();
        long startNodes;
        if (Log.doExpensiveLog) {
            startNodes = from.nodeCount();
        }


        BDD res = notClosed(from, backwardReached,
                NotClosedType.AND, Long.MAX_VALUE, Long.MAX_VALUE);
        if (res == null) {
            return null;
        }

        long nodesAfterClosed = res.nodeCount();

        if (useMutexBDDs || backwardReached.equals(this.variables.getZeroBDD())) {
            for (BDD mutBDD : mutexBDDs) {
                BDD tmp = res;
                res = notClosed(res, mutBDD,
                        mutexType, Long.MAX_VALUE, Long.MAX_VALUE);
                tmp.free();
                if (res == null) {
                    return null;
                }

            }
            long totalTime = System.currentTimeMillis() - startTime;
            if (Log.doExpensiveLog) {
                Log.getLog().log("NotClosed: " + startNodes + " => " + nodesAfterClosed + " => " + res.nodeCount() + " in " + totalTime + " ms");
            }
        } else {
            long totalTime = System.currentTimeMillis() - startTime;

            if (Log.doExpensiveLog) {
                Log.getLog().log("NotClosed: " + startNodes + " => " + nodesAfterClosed + " in " + totalTime + " ms");
            }
        }
        return res;
    }

    public void buildSuperPDB() {
        /*long startingTime = System.currentTimeMillis();
        HashMap<Integer, BDD> backwardBDDs = new HashMap<Integer, BDD>();
        int index;
        BDD tmp1;
        DijkstraData data = new DijkstraData();
        
        index = 0;
        backwardBDDs.put(index, trueGoal.id());
        
        data.backwardReached = factory.zero();
        data.backwardBDDs = backwardBDDs;
        while (!data.backwardBDDs.isEmpty()) {
        // while (index < data.backwardBDDs.size()) {
        index = Collections.min(data.backwardBDDs.keySet());
        Log.getLog().log("   step: " + index);
        // if (data.backwardBDDs.get(index).equals(factory.zero())) {
        // index++;
        // continue;
        // }
        if (System.currentTimeMillis() - startingTime > 100 * 1000
        || index == 4) {
        // int oldsize = data.backwardBDDs.size();
        // if (data.backwardBDDs.size() < index + 1) {
        // Log.getLog().log("Error in ending of buildSuperPDB!");
        // System.exit(1);
        // data.backwardBDDs.setSize(index + 1);
        // for (int i = oldsize; i < index + 1; i++)
        // data.backwardBDDs.set(i, factory.zero());
        // }
        break;
        }
        searchStep(index, data);
        s            try {
        tmp1 = data.backwardBDDs.get(index).replace(variables.getSp2s());
        // data.backwardBDDs.get(index).free();
        data.backwardBDDs.remove(index).free();
        // data.backwardBDDs.set(index, factory.zero());
        data.backwardBDDs.put(index, factory.zero());
        factory.save(pdbFileName + "PDB_" + (index), tmp1);
        tmp1.free();
        } catch (Exception e) {
        System.err.println("Error: " + e.getMessage());
        System.exit(1);
        }
        }
        Log.getLog().log("   final #nodes: "
        + data.backwardReached.nodeCount() + "; #sat: "
        + (long) data.backwardReached.satCount(variables.getCube()));
        data.backwardReached.free();*/
    }
}
// read super-database
//if (pdbFileName.startsWith("abstract")) {
            /*int currentVar = 0;
int[] abstractedVars = new int[this.variables.numBinaryVariables() / 2];
int abstractedVarsCounter = 0;
for (int i = 0; i < variables.getPartitionedVariables().size(); i++) {
int numVars = Maths.log2(variables.getPartitionedVariables().get(i).size());
if (emptyPartitions.contains(i)) {
for (int j = 0; j < numVars; j++) {
abstractedVars[abstractedVarsCounter] = (currentVar + j) * 2;
abstractedVarsCounter++;
}
}
currentVar += numVars;
}*/
//int[] abstractedVars2 = new int[abstractedVarsCounter];
//System.arraycopy(abstractedVars, 0,
//       abstractedVars2, 0, abstractedVarsCounter);
//abstractedPreVars = factory.makeSet(abstractedVars2);

/* String pdbFileNameMiddle = pdbFileName + "PDB_";
File file;
String filename;
int pdbSize = 0;
superPDB = new LinkedList<BDD>();
filename = "super-abstract" + pdbFileNameMiddle + "0";
file = new File(filename);
if (file.exists()) {
Log.getLog().log("   reading super-database ...");
BDD tmp1;
BDD tmp2;
try {
Log.getLog().log("reading file " + pdbSize);
tmp1 = factory.load(filename);
tmp2 = tmp1.exist(abstractedPreVars);
tmp1.free();
superPDB.add(tmp2.replace(variables.getS2sp()));
tmp2.free();
} catch (Exception e) {
System.err.println("Error: " + e.getMessage());
System.exit(1);
}
pdbSize++;
while (true) {
filename = "super-abstract" + pdbFileNameMiddle + pdbSize;
file = new File(filename);
if (file.exists()) {
try {
Log.getLog().log("reading file " + pdbSize);
tmp1 = factory.load(filename);
tmp2 = tmp1.exist(abstractedPreVars);
tmp1.free();
superPDB.add(tmp2.replace(variables.getS2sp()));
tmp2.free();
} catch (Exception e) {
System.err.println("Error: " + e.getMessage());
System.exit(1);
}
pdbSize++;
} else {
if (superPDB.getLast().equals(factory.zero()))
superPDB.removeLast();
break;
}
}
superPDBSize = superPDB.size();
Log.getLog().log("   done.");
}
// }*/

/*if (emptyPartitions.size() == 0) {
// write transition relation to disk
String baseFilename = "transitionRelation_";
Iterator<Integer> keyIt = t.keySet().iterator();
while (keyIt.hasNext()) {
int key = keyIt.next();
String baseFilename2 = baseFilename + key + "_";
int counter = 0;
ListIterator<BDD> bddIt = t.get(key).listIterator();
while (bddIt.hasNext()) {
String filename = baseFilename2 + actionNames.get(key).get(counter);
try {
factory.save(filename, bddIt.next());
} catch (Exception e) {
System.err.println("Error: " + e.getMessage());
e.printStackTrace(System.err);
System.exit(1);
}
counter++;
}
}

// write initial state to disk
try {
factory.save("init", init);
} catch (Exception e) {
System.err.println("Error: " + e.getMessage());
e.printStackTrace(System.err);
System.exit(1);
}

// write goal condition to disk
try {
factory.save("goal", trueGoal);
} catch (Exception e) {
System.err.println("Error: " + e.getMessage());
e.printStackTrace(System.err);
System.exit(1);
}
}*/
// after each search step use of super pdb
            /*if (useSuperPDB && index < superPDBSize) {
            Log.getLog().log("index: " + index);
            tmp1 = backwardBDDs.get(index).and(superPDB.get(index));
            if (tmp1.satCount(variables.getCube())
            < superPDB.get(index).satCount(variables.getCube())) {
            tmp1.free();
            useSuperPDB = false;
            Log.getLog().log("stopped in step " + index);
            } else {
            backwardBDDs.remove(index).free();
            backwardBDDs.put(index, tmp1);
            }
            }*/