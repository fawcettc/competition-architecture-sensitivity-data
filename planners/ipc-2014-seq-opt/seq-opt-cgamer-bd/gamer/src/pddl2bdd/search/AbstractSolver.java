/*
 * Gamer, a tool for finding optimal plans
 * Copyright (C) 2007-2012 by Peter Kissmann
 * modified by Alvaro Torralba (2012-2013)
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */
package pddl2bdd.search;

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import net.sf.javabdd.BDD;
import net.sf.javabdd.BDDFactory;
import pddl2bdd.problem.Problem;
import pddl2bdd.problem.VariablePartitioning;
import pddl2bdd.transition.TManagerBuilder;
import pddl2bdd.util.Log;

/**
 * Common class for A star and Bidirectional Dijkstra Search
 * 
 * Contains some common attributes and method utilities to avoid replicate code.
 * 
 * @author Peter Kissmann and Alvaro Torralba
 */
public abstract class AbstractSolver implements Solver{

    public enum NotClosedType {
        AND, CONSTRAIN, RESTRICT, NP_AND, LI_COMPACTION
    };
    
    TManagerBuilder tManagerBuilder;
    protected BDDFactory factory;
    //protected BDD zeroBDD, oneBDD;
    VariablePartitioning variables;
    protected BDD init; // initial state
    // bdd representing the true (i.e. not simplified) goal-state
    protected BDD trueGoal;
    //Time when loading the program
    protected double time_init = System.currentTimeMillis();

    protected BDD replacedGoal; // The goal to apply an intersection when corresponds.
    private Problem problem;

    public AbstractSolver(Problem problem, VariablePartitioning variables,
            TManagerBuilder tManagerBuilder) {
        this.problem = problem;
        this.variables = variables;
        this.factory = variables.getFactory();
        this.tManagerBuilder = tManagerBuilder;
        this.tManagerBuilder.init(problem);
        this.init();
        this.replacedGoal = trueGoal.replace(this.variables.getSp2s());
    }

    public AbstractSolver(Problem problem,VariablePartitioning variables,
            TManagerBuilder tManagerBuilder,
            List<Integer> emptyPartitions,
            String filenamePath) {
        this.problem = problem;
        this.variables = variables;
        this.factory = variables.getFactory();
        this.tManagerBuilder = tManagerBuilder;
        this.tManagerBuilder.init(problem, emptyPartitions,
                filenamePath + "UsedActions.txt");
        this.init = variables.createInitialState(problem, emptyPartitions);
        this.trueGoal = variables.createGoal(problem, emptyPartitions);

        this.replacedGoal = trueGoal.replace(this.variables.getSp2s());
    }


    /**
     * Initialize initialState and goal.
     */
    private void init() {
        //if (!this.loadInitialStateAndGoal()) {
            Log.getLog().log("   building initial state ...");
            this.init = variables.createInitialState(problem, new LinkedList<Integer>());
            Log.getLog().log("   done.");

            Log.getLog().log("   building goal states ...");
            this.trueGoal = variables.createGoal(problem, new LinkedList<Integer>());
            Log.getLog().log("   done.");

        /*    this.storeInitialStateAndGoal();
        }*/
    }

    private boolean loadInitialStateAndGoal() {
        // build initial state
      
        if (!(new File("init").exists() && new File("goal").exists())) {
            return false;
        }
        Log.getLog().log("   loading initial state ...");
        try {
            init = factory.load("init");
        } catch (Exception e) {
            System.err.println("Error: " + e.getMessage());
            e.printStackTrace(System.err);
            return false;
        }
        Log.getLog().log("   done.");

        Log.getLog().log("   loading goal states ...");
        try {
            trueGoal = factory.load("goal");
        } catch (Exception e) {
            this.init.free();
            System.err.println("Error: " + e.getMessage());
            e.printStackTrace(System.err);
            return false;
        }

        Log.getLog().log("   done.");
        return true;
    }

    private void storeInitialStateAndGoal() {
        // write initial state to disk
        try {
            factory.save("init", init);
        } catch (Exception e) {
            System.err.println("Error: " + e.getMessage());
            e.printStackTrace(System.err);
            System.exit(1);
        }

        // write goal condition to disk
        try {
            factory.save("goal", trueGoal);
        } catch (Exception e) {
            System.err.println("Error: " + e.getMessage());
            e.printStackTrace(System.err);
            System.exit(1);
        }
    }


    /**
     * Cleans up in that it de-references all BDDs.
     */
    @Override
    public void cleanup() {
        this.init.free();
        this.trueGoal.free();
        this.replacedGoal.free();
    }

    public VariablePartitioning getVariables() {
        return variables;
    }

    public BDDFactory getFactory() {
        return factory;
    }

    
    protected BDD notClosed(BDD from, BDD reached, NotClosedType t,
            long maxStepNodes, long maxStepTime) {
        BDD negatedReached = reached.not();
        BDD res = null;
        long timeBound = 0, nodesBound = 0;
        if (maxStepNodes != Long.MAX_VALUE) {
            nodesBound = maxStepNodes;
        }
        if (maxStepTime != Long.MAX_VALUE) {
            timeBound = maxStepTime - System.currentTimeMillis();
        }
        switch (t) {
            case AND:
                if (maxStepTime != Long.MAX_VALUE) {
                    res = from.and(negatedReached, timeBound, nodesBound);
                } else {
                    res = from.and(negatedReached, 0, 0);
                }
                break;
            case CONSTRAIN:
                res = from.constrain(negatedReached, timeBound);
                break;
            case NP_AND:
                res = from.nonPollutingAnd(negatedReached, timeBound);
                 //Log.getLog().log("NPAnd: " + from.nodeCount() +" => " + res.nodeCount());
                break;
            case RESTRICT:
                res = from.restrict(negatedReached, timeBound);
                break;
            case LI_COMPACTION:
                res = from.liCompaction(negatedReached, timeBound);
                break;
        }
        negatedReached.free();
        return res;
    }
  
}
