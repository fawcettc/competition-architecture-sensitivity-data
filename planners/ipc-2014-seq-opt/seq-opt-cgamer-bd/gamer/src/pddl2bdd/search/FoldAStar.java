/*
 * Gamer, a tool for finding optimal plans
 * Copyright (C) 2007-2012 by Peter Kissmann
 * modified by Alvaro Torralba (2012-2013)
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */
package pddl2bdd.search;

import java.util.ArrayList;
import java.util.HashMap;
import net.sf.javabdd.BDD;
import net.sf.javabdd.CUDDFactory;
import pddl2bdd.problem.Problem;
import pddl2bdd.problem.VariablePartitioning;
import pddl2bdd.transition.TManagerBuilder;
import pddl2bdd.util.Log;

/**
 * Implements the Fold A* algorithm.
 * It applies partitioning when a bucket has more than PARTITIONING_MAX_STATES
 * so that each partition has at most that number of states.
 *
 * However, there is at most PARTITIONING_MAX_PARTITIONS.
 * The partitioning algorithms are implemented inside the CUDD package.
 *
  * @author Peter Kissmann and Alvaro Torralba
 */
public class FoldAStar extends ListAStar {

    protected static int PARTITIONING_LEX = 1;
    /**
     * 0 => No partitioning at all
     */
    protected int PARTITIONING_TYPE = PARTITIONING_LEX;
    /**
     * Only used on the related partitioning types
     */
    protected int PARTITIONING_MAX_PARTITIONS = 2;
    protected long PARTITIONING_MAX_STATES = 100000L;

     public FoldAStar(Problem problem, VariablePartitioning variables, TManagerBuilder tManager,
             PDBHeuristic heuristic) {
        super(problem, variables, tManager, heuristic);
    }
 
    @Override
    public void search() {
        Log.getLog().log("Using Fold A*");

        this.currentDiagonal = this.heuristic.getH(init);
        
        Log.getLog().log("starting f-value: " + this.currentDiagonal);
        addUnclassifiedStates(0, init);

        BDD intersection = init.and(replacedGoal);
        boolean solution_found = !intersection.equals(factory.zero());
        if (solution_found) {
            Log.getLog().log("Goals are true in the initial state");
            init.printSet();
            replacedGoal.printSet();
            return;
        }
        intersection.free();

        boolean isEmpty;
        //For each diagonal
        do {
            Log.getLog().log("f: " + this.currentDiagonal);

            solution_found = exploreDiagonalPartitioning();

            isEmpty = !nextFDiagonal();
        } while (!solution_found && !isEmpty);
        if (!solution_found) {
            Log.getLog().log("Exhausted state space and no solution found");
        }
    }

    /**
     * Expands the buckets and apply the conjunction with the heuristic to
     * get the new states in the current diagonal. Insert the others in unclassified.
     */
    private HashMap<Integer, BDD> expandBucketDiagonal(ArrayList<BDD> frontier, int g, int h) {
        HashMap<Integer, BDD> result_diagonal = new HashMap<Integer, BDD>();
        BDD tmp1, tmp2, to;
        BDD totalFrontier = frontier.get(0).id();

        BDD oldFrontier;
        BDD negatedReached = getForwardReached(h).not();
        tmp1 = totalFrontier;

        totalFrontier = tmp1.and(negatedReached);
        tmp1.free();

        if (totalFrontier.equals(factory.zero())) {
            negatedReached.free();
            return result_diagonal;
        } //Log.getLog().log("   g: " + g);
        //First we apply 0-cost operators
        if (tManagerBuilder.hasZeroCostOperators()) {
            BDD lastLayer = totalFrontier;

            while (true) {
                double time_now = (System.currentTimeMillis() - time_init) / 1000.0;
                Log.getLog().log("    bucket size: "
                        + lastLayer.satCount(variables.getCube())
                        + "[" + time_now + "s]");
                to = this.tmanager.image(0, lastLayer);

                tmp1 = to.and(negatedReached);
                to.free();
                oldFrontier = totalFrontier;
                tmp2 = oldFrontier.not();
                lastLayer = tmp1.and(tmp2); //Remove seen states
                tmp1.free();
                tmp2.free();

                if (APPLY_HEURISTIC_0_COST && (MIN_STATES_PARTITION_H == 0
                        || lastLayer.satCount(variables.getCube()) > MIN_STATES_PARTITION_H)) {
                    //Compute the set lastLayer and h
                    tmp1 = this.heuristic.getBDD(lastLayer, h);
                    tmp2 = tmp1.not();
                    to = lastLayer.and(tmp2);
                    lastLayer.free();
                    tmp2.free();
                    lastLayer = tmp1;

                    /*Log.getLog().log("We are skiping " + to.satCount(cube) + " states");*/
                    //Add lastLayer and not(h) to unclassified
                    addUnclassifiedStates(g, to);
                    to.free();
                }

                if (lastLayer.equals(factory.zero())) {
                    break;
                }
                frontier.add(lastLayer);

                //tmp3.free();
                totalFrontier = oldFrontier.or(lastLayer);
                oldFrontier.free();
            }
        }

        negatedReached.free();

        // Log.getLog().log("Include total frontier in forward reached");
        //Include total frontier in forward reached
        addForwardReached(h, totalFrontier);

        //Apply all the other operators in order
        double time_now = (System.currentTimeMillis() - time_init) / 1000.0;
        Log.getLog().log("    bucket size: " + totalFrontier.satCount(variables.getCube())
                + "[" + time_now + "s]");

        HashMap <Integer, BDD> successors = null;
        if (!APPLY_DEFERRED_SUCCESSORS){
            successors = this.tmanager.image(totalFrontier);
        }
        for (int d : tManagerBuilder.getActionCosts()) { //For each action cost
            if (APPLY_DEFERRED_SUCCESSORS && g + d > this.currentDiagonal) {
                addDeferredUnclassifiedStates(g + d, d, totalFrontier);
            } else {
                //Compute image
                if(APPLY_DEFERRED_SUCCESSORS){
                    to = this.tmanager.image(d, totalFrontier);
                }else{
                    to = successors.get(d);
                }

                if (this.heuristic.contains(h - d)) {
                    tmp1 = this.heuristic.getBDD(to, h - d);

                    BDD negated_tmp1 = tmp1.not();
                    tmp2 = to.and(negated_tmp1);

                    negated_tmp1.free();
                    to.free();

                    //Add the result to unclassified state buckets
                    addUnclassifiedStates(g + d, tmp2);
                    tmp2.free();
                    result_diagonal.put(g + d, tmp1);
                } else {
                    addUnclassifiedStates(g + d, to);
                    to.free();
                }
            }
        }
        //Log.getLog().log("Free total frontier");
        totalFrontier.free();
        //Log.getLog().log("done.");
        return result_diagonal;
    }

    private boolean exploreDiagonalPartitioning() {
        int f = currentDiagonal;
        HashMap<Integer, BDD> diagonal = new HashMap<Integer, BDD>();
        ArrayList<Integer> unclas_g = new ArrayList<Integer>();
        unclas_g.addAll(this.unclassified_g);

        for (int g : unclas_g) {
            if (g <= currentDiagonal && this.heuristic.contains(f - g)) {
                BDD bucket = getBDD(g, f - g);

                if (!bucket.equals(factory.zero())) {
                    //Log.getLog().log("Diagonal f: " + g + " bucket g " + g + ": " + bucket.satCount(cube));
                    diagonal.put(g, bucket);
                }
            }
        }

        //Log.getLog().log("Ini diagonal: " + diagonal.keySet());
        return exploreDiagonalPartitioning(0, diagonal);
    }

    /**
     * This method explore the diagonal with f = f_min.
     * Expands all the buckets in the diagonal and left the new states in the
     * array of unclassified states.
     *
     * After reaching
     *
     * When adding a new unclassified state set (a new g),
     * the new possible f values are included in searchSpace
     *
     * Returns true if a goal has been found
     *
     * g_min should be 0 when calling the first time
     *
     */
    private boolean exploreDiagonalPartitioning(int g_min,
            HashMap<Integer, BDD> diagonal) {
        //We are going to explore the diagonal with f min
        int f_min = this.currentDiagonal;

        //Get next g >= g_min
        int next_g = Integer.MAX_VALUE;
        for (int g : diagonal.keySet()) {
            if (g >= g_min && g < next_g) {
                next_g = g;
            }
        }
        g_min = next_g;
        //Log.getLog().log("F: " + f_min + " g: " + g_min);
        if (g_min > f_min) { //Base case.
            return false;
        }

        Log.getLog().log("   g: " + g_min);
        BDD bucket = diagonal.remove(g_min);

        BDD[] partitioning = null;
        if (PARTITIONING_TYPE == PARTITIONING_LEX) {
            //Log.getLog().log("Partitioning: " + bucket.satCount(cube));
            //bucket.printSet();
            //Log.getLog().log(bucket.satCount(cube));
            partitioning = bucket.split_states_max_folds(CUDDFactory.SPLIT_STATES_TYPE_LEX,
                    PARTITIONING_MAX_STATES, PARTITIONING_MAX_PARTITIONS, variables.getCube());
            bucket.free();
            //Log.getLog().log("Partitioned: " + partitioning.length);
        } else {
            System.err.println("Invalid partitioning type");
            System.exit(-1);
        }

        if (partitioning.length > 1) {
            Log.getLog().log("      Partitioning: " + partitioning.length);
        }

        for (int i = 0; i < partitioning.length; i++) {
            ArrayList<BDD> fgArrayList = new ArrayList<BDD>();
            fgArrayList.add(partitioning[i]);

            HashMap<Integer, BDD> bucketDiagonal =
                    expandBucketDiagonal(fgArrayList, g_min, f_min - g_min);

            //Log.getLog().log("Adding bucketDiagonal: " + bucketDiagonal.keySet());
            for (int g : bucketDiagonal.keySet()) {
                BDD previousBucket = diagonal.remove(g);
                BDD addBucket = bucketDiagonal.get(g);
                //Log.getLog().log("Bucket diagonal "+ g + ": " + addBucket.satCount(cube));
                if (previousBucket == null) {
                    diagonal.put(g, addBucket);
                } else {
                    BDD newBucket = previousBucket.or(addBucket);
                    addBucket.free();
                    previousBucket.free();
                    diagonal.put(g, newBucket);
                }
            }

            BDD forwardFrontier = put_solution(g_min, fgArrayList);
            if (g_min == f_min) {
                //Log.getLog().log("Checking if goal found");
                //Check if we have found the goal
                BDD intersection = replacedGoal.and(forwardFrontier);

                if (!intersection.equals(factory.zero())) {
                    //Solution found :D
                    forwardFrontier.free();

                    Log.getLog().log("   cheapest plan has cost of " + g_min);
                    reconstructPlanAStar(g_min, intersection.replace(variables.getS2sp()));
                    intersection.free();
                    return true;
                } //We have not found the solution in this branch.
                intersection.free();
                //But maybe the next one is the right one. Be positive.

            } else if (exploreDiagonalPartitioning(g_min + 1, diagonal)) {
                //Solution found
                for(int j = i+1; j < partitioning.length; j++){
                    partitioning[j].free();
                }
                forwardFrontier.free();
                return true;
            }
            forwardFrontier.free();
            //If we have not reached g_min == f_min, next step
        }
        return false;
    }
}