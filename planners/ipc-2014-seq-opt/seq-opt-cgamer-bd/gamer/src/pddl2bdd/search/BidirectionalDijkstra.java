/*
 * Gamer, a tool for finding optimal plans
 * Copyright (C) 2007-2012 by Peter Kissmann
 * modified by Alvaro Torralba (2012-2013)
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */
package pddl2bdd.search;

import java.util.*;

import pddl2bdd.util.Time;
import net.sf.javabdd.*;

import java.io.*;
import java.util.logging.Level;
import pddl2bdd.problem.Problem;
import pddl2bdd.problem.VariablePartitioning;
import pddl2bdd.transition.TManagerBuilder;
import pddl2bdd.transition.TransitionManager;
import pddl2bdd.util.Log;

/**
 * This class creates BDDs for given domains and problems. It binds together
 * some internal variables so that for example 3 internal binary variables can
 * stand for up to 8 true binary ones - in case that these are mutually
 * exclusive. To do this, in this version the user has to give a legal
 * partitioning.<br>
 * An efficient order of variables is used: one variable of the predecessor
 * state and the corresponding variable of the successor state are used
 * alternately.<br>
 * <br>
 * This class not only creates the BDDs but also allows (bidirectional)
 * symbolic Dijkstra search. It also performs solution reconstruction and
 * writes an optimal plan into the file "plan_output".
 * 
 * @author Peter Kissmann and Alvaro Torralba
 * @version 1.8
 */
public class BidirectionalDijkstra extends AbstractSolver {

    private TransitionManager tManagerForward, tManagerBackward;
    private boolean CONJUNCTION_IN_IMAGE;

    public enum SearchDir {

        Bidirectional, Forward, Backward
    };
    private SearchDir searchDir;

    /**
     * NotClosedTypes for closed list and mutexes. 
     * AND by default
     */
    private NotClosedType closedType = NotClosedType.AND,
            mutexType = NotClosedType.AND;
    private boolean useMutexBDDs;
    private long dijkstraStartTime;
    private int optCost;
    private BDD optIntersection;
    private int optGForw, optGBackw;
    private int optVecIndexForw, optVecIndexBackw;
    private Map<Integer, BDD> openForw, openBackw;
    private Map<Integer, List<BDD>> closedForw, closedBackw;
    private List<BDD> closedForwTotal, closedBackwTotal;
    private int gForw, gBackw;
    private long lastForwTime, lastBackwTime;
    private long lastForwNodes, lastBackwNodes;
    private long nextForwNodes, nextBackwNodes;
    private long lastGrowthForwNodes, lastGrowthBackwNodes;
    private long estimatedForwTime, estimatedBackwTime;
    //private long estimatedForwNodes, estimatedBackwNodes;
    private long maxStepTime; //Truncate the step if system.time() > maxStepTime
    private long maxStepNodes; //Truncate the step if nodes > maxStepNodes

    /**
     * Creates new BDDs for the given domain and problem. <br>
     * <br>
     * In this constructor new BDDs for the given domain and problem will be
     * created. <br>
     * First, the specified number of variables will be allocated. It is
     * important that the number of variables equals twice the number of
     * variables needed for one state, as variables are needed for both, one
     * state and its successor (or predecessor in backward search). <br>
     * Next, BDDs for the transition relation will be generated. Here the
     * actions from the domain will be taken and for each action a BDD will be
     * created as the disjunction takes quite an amount of time sometimes. This
     * has the advantage that every transition-BDD corresponds directly to one
     * action and thus an action-plan can be returned after a search. <br>
     * Next, a BDD for the goal-description is created. First of all, a BDD for
     * the given goal from the problem will be generated. <br>
     * Finally, a BDD for the initial state, given in the problem, is created.
     * 
     * @param domain
     *            The root of the domain, represented as a treef
     * @param problem
     *            The root of the problem, represented as a Tree
     * @param partitions
     *            The partition of the variables. In this class we do not use
     *            true boolean variables to represent the states, but put as
     *            many as possible together - variables that are mutually
     *            exclusive can be together in one partition. If exactly one of
     *            the variables in one partition can be true, everything is
     *            fine. If it also can happen that none of these are true, we
     *            need an extra variable (something like "none-of-these") to
     *            represent the case that none of the variables of this
     *            partition are true. This extra variable has to be at the last
     *            one of the partition. Note also that all variable-names have
     *            to be unique.
     * @param numberOfVars
     *            The number of boolean variables to be used (equals twice the
     *            number of boolean variables needed for one state).
     * @param library
     *            The BDD library used.
     */
    public BidirectionalDijkstra(Problem problem, VariablePartitioning variables,
            TManagerBuilder tManagerBuilder) {
        super(problem, variables, tManagerBuilder);
        this.useMutexBDDs = false;
        this.tManagerForward = tManagerBuilder.getTransitionManager(true);
        this.tManagerBackward = tManagerBuilder.getTransitionManager(false);
        this.searchDir = SearchDir.Bidirectional;
        this.CONJUNCTION_IN_IMAGE = false;
        this.maxStepTime = Long.MAX_VALUE;
        this.maxStepNodes = Long.MAX_VALUE;
    }

    public BidirectionalDijkstra(Problem problem, VariablePartitioning variables,
            TManagerBuilder tManagerBuilder, List<String> params, boolean useMutexBDDs) {
        super(problem, variables, tManagerBuilder);
        this.useMutexBDDs = useMutexBDDs;
        this.tManagerForward = tManagerBuilder.getTransitionManager(true);
        this.tManagerBackward = tManagerBuilder.getTransitionManager(false);
        this.searchDir = SearchDir.Bidirectional;
        this.CONJUNCTION_IN_IMAGE = false;
        this.maxStepTime = Long.MAX_VALUE;
        this.maxStepNodes = Long.MAX_VALUE;
        this.parseParams(params);
    }

    private void parseParams(List<String> params) {
        boolean setClosedType = true;
        for (String param : params) {
            param = param.toLowerCase();
            if (param.equals("bw")) {
                Log.getLog().log(Level.INFO, "Backward search");
                this.searchDir = SearchDir.Backward;
            } else if (param.equals("fw")) {
                Log.getLog().log(Level.INFO, "Forward search");
                this.searchDir = SearchDir.Forward;
            } else if (param.equals("bd")) {
                Log.getLog().log(Level.INFO, "Bidirectional search");
                this.searchDir = SearchDir.Bidirectional;
            } else if (param.equals("img_conj")) {
                CONJUNCTION_IN_IMAGE = true;
            } else if (param.equals("constrain")) {
                this.mutexType = NotClosedType.CONSTRAIN;
                if (setClosedType) {
                    setClosedType = false;
                    this.closedType = NotClosedType.CONSTRAIN;
                }
            } else if (param.equals("and")) {
                this.mutexType = NotClosedType.AND;
                if (setClosedType) {
                    setClosedType = false;
                    this.closedType = NotClosedType.AND;
                }
            } else if (param.equals("restrict")) {
                this.mutexType = NotClosedType.RESTRICT;
                if (setClosedType) {
                    setClosedType = false;
                    this.closedType = NotClosedType.RESTRICT;
                }
            } else if (param.equals("npand")) {
                this.mutexType = NotClosedType.NP_AND;
                if (setClosedType) {
                    setClosedType = false;
                    this.closedType = NotClosedType.NP_AND;
                }
            } else if (param.equals("licompaction")) {
                this.mutexType = NotClosedType.LI_COMPACTION;
                if (setClosedType) {
                    setClosedType = false;
                    this.closedType = NotClosedType.LI_COMPACTION;
                }
            } else {
                Log.getLog().log(Level.SEVERE, "Unkwown parameter in bidir dijkstra: " + param);
                System.exit(-1);
            }
        }
    }

    private BDD image(int cost, BDD from, boolean forward) {
        if (forward) {
            return tManagerForward.image(cost, from, this.maxStepTime, maxStepNodes);
        } else {
            return tManagerBackward.image(cost, from, this.maxStepTime, maxStepNodes);
        }
    }

    private HashMap<Integer, BDD> image(BDD from, boolean forward) {
        if (forward) {
            return tManagerForward.image(from, this.maxStepTime, maxStepNodes);
        } else {
            return tManagerBackward.image(from, this.maxStepTime, maxStepNodes);
        }
    }

    private HashMap<Integer, BDD> image(BDD from, BDD conjoint, boolean forward) {
        if (Log.doExpensiveLog) {
            if(Log.doExpensiveLog) Log.getLog().log("ImageConj: " + conjoint.nodeCount());
        }
        if (forward) {
            return tManagerForward.image(from, conjoint, this.maxStepTime, maxStepNodes);
        } else {
            return tManagerBackward.image(from, conjoint, this.maxStepTime, maxStepNodes);
        }
    }
    
    /**
     * Computes the conjunction with not closed total.
     * From gets freed
     */
    private BDD notClosed(BDD from, boolean fw, boolean checkOnlyMutex) {
        long startTime = System.currentTimeMillis();
        long startNodes;
        if (Log.doExpensiveLog) {
            startNodes = from.nodeCount();
        }

        List<BDD> closedTotal = getClosedTotal(fw);
        BDD res;
        if (!checkOnlyMutex) {
            res = notClosed(from, closedTotal.get(0), this.closedType, this.maxStepNodes, this.maxStepTime);
            if (res == null) {
                if(Log.doExpensiveLog) Log.getLog().log(Level.INFO, "Truncated during not closed. Remaining Time: " + (this.maxStepTime - System.currentTimeMillis()) + " " + this.maxStepNodes);
                return null;
            }
        } else {
            res = from.id();
        }

        long nodesAfterClosed = res.nodeCount();
        
        //If is the first bw step and we are not using mutexBDDs: apply mutex over goal states.
        if (!useMutexBDDs && !fw && getG(false) == 0 && this.variables.getMutex(false).getMutexBDDs() != null) {
            for (BDD mutexBDD : this.variables.getMutex(false).getMutexBDDs()) {
                BDD tmp = res;
                res = notClosed(res, mutexBDD, this.mutexType, this.maxStepNodes, this.maxStepTime);
                tmp.free();
                if (res == null) {
                   if(Log.doExpensiveLog) Log.getLog().log(Level.INFO, "Truncated while applying mutexes to goal. Remaining Time: " + (this.maxStepTime - System.currentTimeMillis()) + " Nodes: " + this.maxStepNodes);
                    return null;
                }
            }
            this.variables.getMutex(false).cleanup();
        }

        for (int i = 1; i < closedTotal.size(); i++) {
            BDD tmp = res;
            res = notClosed(res, closedTotal.get(i), this.mutexType, this.maxStepNodes, this.maxStepTime);
            tmp.free();
            if (res == null) {
               if(Log.doExpensiveLog) Log.getLog().log(Level.INFO, "Truncated during not closed. Remaining Time: " + (this.maxStepTime - System.currentTimeMillis()) + " " + this.maxStepNodes);
                return null;
            }
        }
        long totalTime = System.currentTimeMillis() - startTime;
        if (Log.doExpensiveLog) {
            Log.getLog().log("NotClosed: " + startNodes + " => " + nodesAfterClosed + " => " + res.nodeCount() + " in " + totalTime + " ms");
        }
        return res; 
    }

    private void addFrontierToClosedTotal(boolean fw, int index, List<BDD> frontier, BDD frontierTotal) {
        List<BDD> closedTotal = getClosedTotal(fw);
        Map<Integer, List<BDD>> closed = getClosed(fw);
        BDD tmp1 = closedTotal.get(0);
        closedTotal.set(0, tmp1.or(frontierTotal));
        tmp1.free();
        if (closed.containsKey(index)) {
            for (BDD bdd : closed.get(index)) {
                bdd.free();
            }
        }

        closed.put(index, frontier);

    }

    private boolean searchStep(boolean fw, StepDelayedException continued) throws StepDelayedException {
        // int index, Map<Integer, BDD> open, Map<Integer, List<BDD>> closed,
        //List<BDD> closedTotal, BDD otherFrontier) {*/
        //backward:, gBackw, openBackw, closedBackw
        //closedBackwTotal, closedForwTotal.get(0), false);
        //forward:gForw, openForw, closedForw, closedForwTotal, closedBackwTotal.get(0)
        int index = getG(fw);

        Map<Integer, BDD> open = getOpen(fw);
        Map<Integer, List<BDD>> closed = getClosed(fw);
        //List<BDD> closedTotal = getClosedTotal(fw);
        BDD otherFrontier = getClosedTotal(!fw).get(0);
        //BDD negatedReached = closedTotal.get(0).not();

        long iniTimeSearchStep = System.currentTimeMillis();
        if (this.searchDir == SearchDir.Bidirectional) {
            this.maxStepTime = iniTimeSearchStep + 10000 + 2 * this.estimatedNextStepTime(!fw);
            this.maxStepNodes = 100000 + 2 * this.estimatedNextStepNodes(!fw);
        }
        BDD frontierTotal;
        ArrayList<BDD> frontier;
        boolean checkFrontierTotalNotReached = true, checkOnlyMutex = false;
        if (continued != null) {
            if(Log.doExpensiveLog) Log.getLog().log("Continuing with bucket " + index + " in " + getDirName(fw) + " direction");
            frontier = continued.getFrontier();
            frontierTotal = continued.getFrontierTotal();
            checkFrontierTotalNotReached = continued.isNotClosed();
            if (checkFrontierTotalNotReached) {
                for (BDD bdd : frontier) {
                    bdd.free();
                }
                frontier = new ArrayList<BDD>();
            }
            checkOnlyMutex = true;
        } else {
            if(Log.doExpensiveLog) Log.getLog().log("Expanding bucket " + index + " in " + getDirName(fw) + " direction");
            frontier = new ArrayList<BDD>();
            frontierTotal = open.remove(index);
        }

        if (checkFrontierTotalNotReached) {
            BDD tmp1 = frontierTotal;
            frontierTotal = notClosed(tmp1, fw, checkOnlyMutex);
            if (frontierTotal == null) {
                frontierTotal = tmp1;
                frontier.add(frontierTotal.id());
                this.addFrontierToClosedTotal(fw, index, frontier, frontierTotal);
                throw new StepDelayedException(frontier, frontierTotal, true, "Not closed of frontier total");
            }
            tmp1.free();
            //this.setNextStepFrontier(fw, frontierTotal.nodeCount()); //lalala

            if (frontierTotal.equals(factory.zero())) {
                frontierTotal.free();
                this.maxStepTime = Long.MAX_VALUE;
                this.maxStepNodes = Long.MAX_VALUE;
                return this.checkCut(null, fw);
            }

            frontier.add(frontierTotal.id());

            // compare against other frontier
            tmp1 = frontierTotal.and(otherFrontier);
            if (!tmp1.equals(factory.zero())) {
                frontierTotal.free();
                frontier.get(0).free();
                frontier.set(0, tmp1);
                closed.put(index, frontier);
                ArrayList<Integer> ret = new ArrayList<Integer>();
                ret.add(index);
                return this.checkCut(ret, fw);
            }
        }

        ArrayList<Integer> ret = null;
        if (tManagerBuilder.hasZeroCostOperators()) {
            while (true) {
                //We delay it if it has already taken double the time of the last layer 
                //(we add 1s to be sure when lastStepTime is really small)
                //We  don't do that if index == 0 because we need to put something in the closed list
                if (searchDir == SearchDir.Bidirectional && index > 0
                        && System.currentTimeMillis() > this.maxStepTime) {
                    this.addFrontierToClosedTotal(fw, index, frontier, frontierTotal);
                    throw new StepDelayedException(frontier, frontierTotal, false, "Step time exhausted");
                }
                if (Log.doExpensiveLog) {
                    Log.getLog().log(Level.INFO, getDirName(fw) + " step 0: from = " + frontier.get(frontier.size() - 1).nodeCount());
                    Log.getLog().logBDD(frontier.get(frontier.size() - 1), getDirName(fw) + "_" + index + "_0cost");
                }

                BDD to = image(0, frontier.get(frontier.size() - 1), fw);
                if (to == null) {
                    this.addFrontierToClosedTotal(fw, index, frontier, frontierTotal);
                    throw new StepDelayedException(frontier, frontierTotal, false, "0 cost image too expensive. MaxTime: " + (this.maxStepTime - System.currentTimeMillis()) + "ms MaxNodes: " + this.maxStepNodes);
                }
                BDD tmp = to;
                to = notClosed(to, frontierTotal, this.closedType, this.maxStepNodes, this.maxStepTime);
                tmp.free();
                if (to == null) {
                    this.addFrontierToClosedTotal(fw, index, frontier, frontierTotal);
                    throw new StepDelayedException(frontier, frontierTotal, false, "Not closed after 0 cost image too expensive");
                }
                tmp = to;
                to = notClosed(to, fw, false);
                tmp.free();
                if (to == null) {
                    this.addFrontierToClosedTotal(fw, index, frontier, frontierTotal);
                    throw new StepDelayedException(frontier, frontierTotal, false, "Mutex removal after 0-cost image too expensive");
                }
                if (to.equals(factory.zero())) {
                    break;
                }
                frontier.add(to);

                // compare against other frontier
                tmp = to.and(otherFrontier);
                if (!tmp.equals(factory.zero())) {
                    tmp.free();
                    closed.put(index, frontier);
                    frontierTotal.free();
                    ret = new ArrayList<Integer>();
                    ret.add(index);
                    return this.checkCut(ret, fw);
                }

                frontierTotal = frontierTotal.orWith(frontier.get(frontier.size() - 1).id());
            }
        }

        this.addFrontierToClosedTotal(fw, index, frontier, frontierTotal);
        HashMap<Integer, BDD> succ = null;

        List<BDD> closedTotal = getClosedTotal(fw);
        if (Log.doExpensiveLog) {
            Log.getLog().log(getDirName(fw) + " step cost: from = " + frontierTotal.nodeCount() + " closed = " + closedTotal.get(0).nodeCount());
            Log.getLog().logBDD(frontierTotal, getDirName(fw) + "_" + index + "_cost");
        }

        if (CONJUNCTION_IN_IMAGE && closedTotal.size() == 1) {
            BDD negatedReached = closedTotal.get(0).not();
            succ = image(frontierTotal, negatedReached, fw);
            negatedReached.free();
        } else {
            succ = image(frontierTotal, fw);
        }

        if (succ == null) {
            Log.getLog().log(Level.INFO, "Image truncated: Remaining time: " + (this.maxStepTime - System.currentTimeMillis()) + " ms. Max nodes: " + this.maxStepNodes);
            throw new StepDelayedException(frontier, frontierTotal, false, "cost image too expensive");
        }

        for (int c : succ.keySet()) {
            BDD to = succ.get(c);
            BDD tmp1 = open.get(index + c);
            if (tmp1 == null) {
                open.put(index + c, to);
            } else {
                open.put(index + c, tmp1.or(to));
                tmp1.free();
                to.free();
            }

            // compare against other frontier
            tmp1 = open.get(index + c).and(otherFrontier);
            if (!tmp1.equals(factory.zero())) {
                tmp1.free();
                if (ret == null) {
                    ret = new ArrayList<Integer>();
                }
                ret.add(index + c);
            }
        }

        if (Log.doExpensiveLog) {
            long time = System.currentTimeMillis() - iniTimeSearchStep;
            Log.getLog().log(Level.INFO, getDirName(fw) + " step " + index + ": " + frontierTotal.satCount(this.variables.getCube()) + " states, "
                    + frontierTotal.nodeCount() + " nodes, " + time + " ms");
        }else{
            Log.getLog().log(Level.INFO, getDirName(fw) + " step " + index);
        }
        frontierTotal.free();

        return this.checkCut(ret, fw);
    }

    /**
     * Checks the cut between the new generated bucket and the other frontier
     * Returns true if finished
     */
    private boolean checkCut(ArrayList<Integer> nonEmptyCut, boolean fw) {
        if (nonEmptyCut != null) {
            if(Log.doExpensiveLog) Log.getLog().log("   non-empty cut in buckets " + nonEmptyCut);
            for (int nonEmptyIndex = 0; nonEmptyIndex < nonEmptyCut.size(); nonEmptyIndex++) {
                int nonEmptyCutDir = nonEmptyCut.get(nonEmptyIndex);
                BDD intersection;
                if (nonEmptyCutDir == getG(fw)) {
                    List<BDD> closed = getClosed(fw).get(getG(fw));
                    //intersection = closedBack.get(closedBack.size() -1).replace(this.variables.getSp2s());
                    intersection = closed.get(closed.size() - 1);
                } else {
                    //intersection = openBackw.get(nonEmptyCutBackw).replace(this.variables.getSp2s());
                    intersection = getOpen(fw).get(nonEmptyCutDir);
                }
                Map<Integer, List<BDD>> closedOther = getClosed(!fw);
                Set<Integer> keySet = closedOther.keySet();
                int smallestIndex = Integer.MAX_VALUE;
                int vecIndex = -1;
                BDD optTmp = null;

                for (int index : keySet) {
                    if (index < getG(!fw) - tManagerBuilder.getMaxCost()) {
                        continue;
                    }
                    if (index + nonEmptyCutDir >= optCost) {
                        continue;
                    }
                    if (index < smallestIndex) {
                        List<BDD> vec = closedOther.get(index);
                        for (int i = 0; i < vec.size(); i++) {
                            BDD tmp1 = intersection.and(vec.get(i));
                            if (!tmp1.equals(factory.zero())) {
                                if (optTmp != null) {
                                    optTmp.free();
                                }
                                optTmp = tmp1;
                                smallestIndex = index;
                                vecIndex = i;
                            } else {
                                tmp1.free();
                            }
                        }
                    }
                }
                if (smallestIndex != Integer.MAX_VALUE) {
                    setOptG(!fw, smallestIndex);
                    setOptVecIndex(!fw, vecIndex);
                    setOptG(fw, nonEmptyCutDir);
                    if (nonEmptyCutDir == getG(fw)) {
                        setOptVecIndex(fw, getClosed(fw).get(getG(fw)).size() - 1);
                    } else {
                        setOptVecIndex(fw, 0);
                    }
                    //optIntersection = optTmp.replace(this.variables.getS2sp());
                    //optTmp.free();
                    optIntersection = optTmp;
                    optCost = optGForw + optGBackw;
                    Log.getLog().log(Level.INFO, "   plan of cost " + optCost + " found");
                }
            }
        }

        //Check if there are values on the open list
        Set<Integer> keySet = getOpen(fw).keySet();
        if (keySet.isEmpty()) {
            if (optCost == Integer.MAX_VALUE) { //Finished and no plan found
                Log.getLog().log(Level.INFO, "no plan!");
                long dijkstraEndTime = System.currentTimeMillis();
                Log.getLog().log(Level.INFO, "Total time (Dijkstra): " + Time.printTime(dijkstraEndTime - dijkstraStartTime));
                return true;
            } else {
                //Solution found in other steps
                return true;
            }
        }
        //Not finished: update g
        int g = setG(fw, Collections.min(keySet));
        if(Log.doExpensiveLog) Log.getLog().log("   next g value in " + this.getDirName(fw) + " direction: " + g);

        return false;
    }

    public void findPlanDijkstra() {
        dijkstraStartTime = System.currentTimeMillis();
        openForw = new HashMap<Integer, BDD>();
        openBackw = new HashMap<Integer, BDD>();
        closedForw = new HashMap<Integer, List<BDD>>();
        closedBackw = new HashMap<Integer, List<BDD>>();
        closedForwTotal = new ArrayList<BDD>(); // ArrayList for change within searchStep
        closedBackwTotal = new ArrayList<BDD>(); // ArrayList for change within searchStep
        gForw = 0;
        gBackw = 0;
        lastForwNodes = 1;
        lastBackwNodes = 1;
        lastForwTime = -1;
        lastBackwTime = -1;
        this.estimatedForwTime = -1;
        this.estimatedBackwTime = -1;
        optGForw = Integer.MAX_VALUE;
        optVecIndexForw = Integer.MAX_VALUE;
        optGBackw = Integer.MAX_VALUE;
        optVecIndexBackw = Integer.MAX_VALUE;
        optCost = Integer.MAX_VALUE;

        StepDelayedException delayedFw = null, delayedBw = null;
        
        if (this.searchDir == SearchDir.Bidirectional
                || this.searchDir == SearchDir.Forward) {
            closedForwTotal.add(factory.zero());
            if (this.useMutexBDDs) {
                for (BDD mutexBDD : this.variables.getMutex(true).getMutexBDDs()) {
                    closedForwTotal.add(mutexBDD.id());
                }
            }
        } else {
            closedForwTotal.add(init.id());
            ArrayList<BDD> vec = new ArrayList<BDD>();
            vec.add(init.id());
            closedForw.put(0, vec);
        }

        if (this.searchDir == SearchDir.Bidirectional
                || this.searchDir == SearchDir.Backward) {
            closedBackwTotal.add(factory.zero());
            if (this.useMutexBDDs) {
                for (BDD mutexBDD : this.variables.getMutex(false).getMutexBDDs()) {
                    closedBackwTotal.add(mutexBDD.id());
                }
            }
        } else {
            closedBackwTotal.add(replacedGoal.id());
            ArrayList<BDD> vec = new ArrayList<BDD>();
            vec.add(replacedGoal.id());
            closedBackw.put(0, vec);
        }

        openForw.put(gForw, init.id());
        openBackw.put(gBackw, replacedGoal.id());

        boolean stop = false;
        // Check if the goals are satisfaced in the initial state.
        BDD intersection = init.and(replacedGoal);
        if (!intersection.equals(factory.zero())) {
            intersection.free();
            stop = true;
        }

        while (!stop && gForw + gBackw < optCost) {
            if(Log.doExpensiveLog){
            if (optCost == Integer.MAX_VALUE) {
                Log.getLog().log("so far, not plan found");
            } else {
                Log.getLog().log("best plan so far has cost: " + optCost);
            }
            }

            boolean stepFw = (searchDir == SearchDir.Forward)
                    || (searchDir == SearchDir.Bidirectional
                    && this.estimatedNextStepTime(false) >= this.estimatedNextStepTime(true));
            long startTime = System.currentTimeMillis();
            try {
                stop = searchStep(stepFw, stepFw ? delayedFw : delayedBw);
                this.maxStepTime = Long.MAX_VALUE;
                this.maxStepNodes = Long.MAX_VALUE;
                if (stepFw) {
                    delayedFw = null;
                } else {
                    delayedBw = null;
                }

                long lastStepTime = System.currentTimeMillis() - startTime;
                if(Log.doExpensiveLog) Log.getLog().log("   took: " + Time.printTime(lastStepTime));
                setLastStepTime(stepFw, lastStepTime);
            } catch (StepDelayedException e) {
                if (stepFw) {
                    delayedFw = e;
                } else {
                    delayedBw = e;
                }
                long lastStepTime = System.currentTimeMillis() - startTime;
                if(Log.doExpensiveLog) Log.getLog().log("   delayed took: " + Time.printTime(lastStepTime) + " reason: " + e.getReason());
                long remainingTime = this.maxStepTime - System.currentTimeMillis();

                if (remainingTime <= 0) {
                    //Penalize, doubling the time for the other direction                                                                             
                    setNextEstimatedTime(stepFw, 2 * lastStepTime);
                } else { //Memory exceeded
                    setNextEstimatedTime(stepFw, 2 * (remainingTime + lastStepTime));
                    //Set estimated number of nodes in this direction                                                                                 
                    setNextStepFrontier(stepFw, Math.max(getNextStepFrontier(stepFw), this.maxStepNodes * 2));
                    setNextStepFrontier(!stepFw, this.maxStepNodes); //estimated number of nodes in the other direction                               
                }
            }
        }

        if (optCost == Integer.MAX_VALUE) {
            Log.getLog().log(Level.INFO, "Proved unsolvable by bidirectional Dijkstra");

            if (delayedBw != null) {
                delayedBw.free();
            }
            if (delayedFw != null) {
                delayedFw.free();
            }

            for (BDD bdd : openForw.values()) {
                bdd.free();
            }
            for (BDD bdd : openBackw.values()) {
                bdd.free();
            }
            for (BDD bdd : closedForwTotal) {
                bdd.free();
            }
            for (BDD bdd : closedBackwTotal) {
                bdd.free();
            }
            for (List<BDD> vector : closedForw.values()) {
                for (BDD bdd : vector) {
                    bdd.free();
                }
            }

            for (List<BDD> vector : closedBackw.values()) {
                for (BDD bdd : vector) {
                    bdd.free();
                }
            }

            return;
        }

        Log.getLog().log(Level.INFO, "Solution found; optimal cost: " + optCost);

        Set<Integer> keySet;
        int key;

        //Put things in openForw into closedForw
        while (!(keySet = openForw.keySet()).isEmpty()
                && (key = Collections.min(keySet)) <= optGForw) {
            ArrayList<BDD> vec = new ArrayList<BDD>();
            vec.add(openForw.remove(key));
            closedForw.put(key, vec);
        }
        for (BDD bdd : openForw.values()) {
            bdd.free();
        }

        //Put things in openBackw into closedBackw
        while (!(keySet = openBackw.keySet()).isEmpty()
                && (key = Collections.min(keySet)) <= optGBackw) {
            if (closedBackw.containsKey(key)) {
                for (BDD bdd : closedBackw.get(key)) {
                    bdd.free();
                }
            }
            ArrayList<BDD> vec = new ArrayList<BDD>();
            vec.add(openBackw.remove(key));
            closedBackw.put(key, vec);
        }

        if (delayedBw != null) {
            delayedBw.free();
        }
        if (delayedFw != null) {
            delayedFw.free();
        }

        for (BDD bdd : openBackw.values()) {
            bdd.free();
        }

        for (BDD bdd : closedForwTotal) {
            bdd.free();
        }
        for (BDD bdd : closedBackwTotal) {
            bdd.free();
        }


        //Log.getLog().log("OptIntersection: " + optIntersection.nodeCount());
        do {
            key = Collections.max(closedForw.keySet());
            if (key > optGForw) {
                for (BDD bdd : closedForw.remove(key)) {
                    bdd.free();
                }
            } else if (key == optGForw) {
                List<BDD> vec = closedForw.get(key);
                for (int i = vec.size() - 1; i >= optVecIndexForw; i--) {
                    vec.remove(i).free();
                }
                vec.add(optIntersection);
            }
        } while (key > optGForw);


        do {
            key = Collections.max(closedBackw.keySet());
            if (key > optGBackw) {
                for (BDD bdd : closedBackw.remove(key)) {
                    bdd.free();
                }
            } else if (key == optGBackw) {
                List<BDD> vec = closedBackw.get(key);
                //Free memory
                for (int i = vec.size() - 1; i >= optVecIndexBackw; i--) {
                    vec.remove(i).free();
                }
                vec.add(optIntersection.id());
                break;
            }
        } while (key > optGBackw);

        reconstructPlanDijkstra(closedForw, optGForw, closedBackw, optGBackw);

        for (List<BDD> vector : closedForw.values()) {
            for (BDD bdd : vector) {
                bdd.free();
            }
        }

        for (List<BDD> vector : closedBackw.values()) {
            for (BDD bdd : vector) {
                bdd.free();
            }
        }

        Log.getLog().log(Level.INFO, "Solved by Bidirectional Dijkstra");
    }

    private void reconstructPlanDijkstra(Map<Integer, List<BDD>> forwBDDs, int forwIndex,
            Map<Integer, List<BDD>> backwBDDs, int backwIndex) {
        List<String> solution = new ArrayList<String>();
        List<BDD> vec;
        int bfsIndex;
        BDD currentStates = null;
        int index = 0;

        Log.getLog().log(Level.INFO, "   reconstructing cheapest plan ...");
        if (forwBDDs.size() > 0) {
            vec = forwBDDs.get(forwIndex);
            bfsIndex = vec.size() - 1;
            currentStates = vec.get(bfsIndex).id();//.replace(this.variables.getS2sp());
            reconstructPlanDijkstraOneDir(forwBDDs, forwIndex, bfsIndex,
                    currentStates, solution, this.tManagerBackward);
            currentStates = applyPlan(solution);
            index = printPlan(Level.INFO, solution, index, true);
            solution.clear();
        }
        if (backwBDDs.size() > 0) {
            vec = backwBDDs.get(backwIndex);
            bfsIndex = vec.size() - 1;
            if (currentStates == null) {
                currentStates = vec.get(bfsIndex).id();//.replace(this.variables.getSp2s());
            }
            reconstructPlanDijkstraOneDir(backwBDDs, backwIndex, bfsIndex,
                    currentStates, solution, this.tManagerForward);
            printPlan(Level.INFO, solution, index, false);
        }
        Log.getLog().log(Level.INFO, "   done.");
    }

    private void reconstructPlanDijkstraOneDir(Map<Integer, List<BDD>> closedBDDs,
            int g, int bfsIndex, BDD currentStates,
            List<String> solution, TransitionManager tManager) {
        BDD tmp1;
        BDD tmp2;
        BDD tmpStates;
        List<BDD> vec;

        // apply zero-cost actions
        if (bfsIndex > 0) {
            vec = closedBDDs.get(g);

            while (bfsIndex > 0) {
                bfsIndex--;
                for (String nameAction : this.tManagerBuilder.getActionNames().get(0)) {
                    tmp1 = tManager.applyAction(currentStates, nameAction);
                    tmp2 = tmp1.and(vec.get(bfsIndex));
                    tmp1.free();
                    if (!tmp2.equals(factory.zero())) {
                        currentStates.free();
                        currentStates = tmp2;
                        solution.add(0, nameAction);
                        break;
                    }
                }
            }
        }


        if (g == 0) {
            currentStates.free();
            return;
        }

        // find states in some predecessor bucket
        Iterator<Integer> itActionCosts = tManagerBuilder.getDescendingActionCosts();
        while (itActionCosts.hasNext()) {
            int cost = itActionCosts.next();
            if (g - cost < 0) {
                continue;
            }
            vec = closedBDDs.get(g - cost);
            if (vec == null) {
                continue;
            }

            List<String> names = this.tManagerBuilder.getActionNames().get(cost);
            for (String nameAction : names) {
                tmpStates = tManager.applyAction(currentStates, nameAction);
                for (int vecIndex = 0; vecIndex < vec.size(); vecIndex++) {
                    tmp1 = vec.get(vecIndex).and(tmpStates);
                    if (!tmp1.equals(factory.zero())) {
                        currentStates.free();
                        currentStates = tmp1;
                        tmpStates.free();
                        solution.add(0, nameAction);
                        reconstructPlanDijkstraOneDir(closedBDDs, g - cost, vecIndex, currentStates, solution, tManager);
                        return;
                    }
                }
                tmpStates.free();
            }
        }

        Log.getLog().log(Level.SEVERE, "Something went wrong in the solution reconstruction. Partial solution: " + solution);
        System.exit(1);
    }

    private BDD applyPlan(List<String> plan) {
        BDD tmp1;
        BDD ret = init.id();
        for (String action : plan) {
            tmp1 = ret;
            ret = tManagerForward.applyAction(tmp1, action);
            tmp1.free();
        }
        return ret;
    }

    private int printPlan(Level level, List<String> plan,
            int startIndex, boolean correctOrder) {
        try {
            FileWriter writer = new FileWriter("plan_output", true);
            int initialOutputCapacity = 10000;
            StringBuilder output = new StringBuilder(initialOutputCapacity);
            int counter = startIndex;
            while (!plan.isEmpty()) {
                String nextAction = "";
                if (correctOrder) {
                    nextAction = plan.remove(0);
                } else {
                    nextAction = plan.remove(plan.size()-1);
                }
                String nextActionParts[] = nextAction.split("[.]");
                output.append(counter++).append(": (").append(nextActionParts[0]);
                for (int i = 1; i < nextActionParts.length; i++) {
                    output.append(" ").append(nextActionParts[i]);
                }
                output.append(")\n");
                Log.getLog().log(level, "      (" + nextAction + ")");
                if (output.length() > initialOutputCapacity) {
                    writer.write(output.toString());
                    output = output.delete(0, output.length());
                }
            }
            writer.write(output.toString());
            output = output.delete(0, output.length());
            writer.close();
            return counter;
        } catch (Exception e) {
            Log.getLog().log(Level.SEVERE,"Error: " + e.getMessage());
            e.printStackTrace(System.err);
            System.exit(1);
        }
        return -1;
    }
    private final long MAX_TIME = 30 * 1000;

    @Override
    public void cleanup() {
        this.tManagerBackward.free();
        this.tManagerForward.free();

        super.cleanup();
    }

    @Override
    public void search() {
        /*if (this.searchDir == SearchDir.Bidirectional
        && checkBackwardVsForwardTime() == Double.MAX_VALUE) {
        this.searchDir = SearchDir.Forward;
        }*/

        this.findPlanDijkstra();
    }

    private int getOptG(boolean fw) {
        if (fw) {
            return optGForw;
        } else {
            return optGBackw;
        }
    }

    private void setOptG(boolean fw, int optG) {
        if (fw) {
            this.optGForw = optG;
        } else {
            this.optGBackw = optG;
        }
    }

    private String getDirName(boolean fw) {
        if (fw) {
            return "forward";
        } else {
            return "backward";
        }
    }

    private int getG(boolean fw) {
        if (fw) {
            return gForw;
        } else {
            return gBackw;
        }
    }

    public long estimatedNextStepNodes(boolean fw) {
        long res;
        if (fw) {
            long growth = Math.max(0, this.nextForwNodes - this.lastForwNodes);
            if (growth > 0) {
                growth = 2 * growth - this.lastGrowthForwNodes;
            }
            res = this.nextForwNodes + growth;
            //    Log.getLog().log("Estimated Miau " + this.lastForwNodes + " " + this.nextForwNodes + " " + res);
        } else {
            long growth = Math.max(0, this.nextBackwNodes - this.lastBackwNodes);
            if (growth > 0) {
                growth = 2 * growth - this.lastGrowthBackwNodes;
            }
            res = this.nextBackwNodes + growth;
            //       Log.getLog().log("Estimated Miau " + this.lastBackwNodes + " " + this.nextBackwNodes + " " + res);
        }
        Log.getLog().log("Estimated " + getDirName(fw) + " nodes: " + res);
        return res;
    }

    private long estimatedNextStepTime(boolean fw, long lastTime, long lastNodes, long nextNodes) {
        long res;
        if (lastTime < 1000) {
            res = lastTime;
        } else {
            double timePerNode = ((double) lastTime) / lastNodes;
            res = (long) (timePerNode * nextNodes);
        }
        Log.getLog().log("Estimated " + this.getDirName(fw) + " Time: " + res
                + " (" + lastNodes + "=>" + nextNodes + ")");
        return res;
    }

    public long estimatedNextStepTime(boolean fw) {
        if (fw) {
            //Log.getLog().log("Bucket forward " + index);
            if (this.estimatedForwTime == -1) {
                int index = this.getG(fw);
                BDD tmp = this.getOpen(fw).get(index);
                this.nextForwNodes = tmp.nodeCount();
                this.estimatedForwTime = this.estimatedNextStepTime(fw, this.lastForwTime,
                        this.lastForwNodes, this.nextForwNodes);
            }
            return this.estimatedForwTime;
        } else {
            if (this.estimatedBackwTime == -1) {
                int index = this.getG(fw);
                BDD tmp = this.getOpen(fw).get(index);
                this.nextBackwNodes = tmp.nodeCount();
                this.estimatedBackwTime = this.estimatedNextStepTime(fw, lastBackwTime, lastBackwNodes, nextBackwNodes);
            }
            return this.estimatedBackwTime;
        }
    }

    private void setNextStepFrontier(boolean fw, long nodeCount) {
        Log.getLog().log("Next step frontier in " + getDirName(fw) + " direction set to: " + nodeCount);
        if (fw) {
            this.nextForwNodes = nodeCount;
        } else {
            this.nextBackwNodes = nodeCount;
        }
    }

    private long getNextStepFrontier(boolean fw) {
        if (fw) {
            return this.nextForwNodes;
        } else {
            return this.nextBackwNodes;
        }
    }

    private void setLastStepTime(boolean fw, long stepTime) {
        if (fw) {
            this.lastForwTime = stepTime;
            this.estimatedForwTime = -1;
            this.lastGrowthForwNodes = this.nextForwNodes - this.lastForwNodes;
            this.lastForwNodes = this.nextForwNodes;
        } else {
            this.lastBackwTime = stepTime;
            this.estimatedBackwTime = -1;
            this.lastGrowthBackwNodes = this.nextBackwNodes - this.lastBackwNodes;
            this.lastBackwNodes = this.nextBackwNodes;
        }
    }

    public void setNextEstimatedTime(boolean fw, long stepTime) {
        Log.getLog().log("Next estimated time " + getDirName(fw) + " set to " + stepTime);
        if (fw) {
            this.estimatedForwTime = stepTime;
        } else {
            this.estimatedBackwTime = stepTime;
        }
    }

    private Map<Integer, BDD> getOpen(boolean fw) {
        if (fw) {
            return this.openForw;
        } else {
            return this.openBackw;
        }
    }

    private Map<Integer, List<BDD>> getClosed(boolean fw) {
        if (fw) {
            return this.closedForw;
        } else {
            return this.closedBackw;
        }
    }

    private List<BDD> getClosedTotal(boolean fw) {
        if (fw) {
            return this.closedForwTotal;
        } else {
            return this.closedBackwTotal;
        }
    }

    private int setG(boolean fw, int value) {
        if (fw) {
            this.gForw = value;
        } else {
            this.gBackw = value;
        }
        return value;
    }

    private void setOptVecIndex(boolean fw, int value) {
        if (fw) {
            this.optVecIndexForw = value;
        } else {
            this.optVecIndexBackw = value;
        }
    }

    /**
     * This class allows to delay some step.
     * Used when there are zero cost actions and it is taking too long to
     * give an step (we cancel it and try the other direction)
     */
    private class StepDelayedException extends Exception {

        private ArrayList<BDD> frontier;
        private BDD frontierTotal;
        private boolean notClosed;
        private String reason;

        public StepDelayedException(ArrayList<BDD> frontier, BDD frontierTotal,
                boolean notClosed, String reason) {
            ArrayList<BDD> copyFrontier = new ArrayList<BDD>();
            for (BDD bdd : frontier) {
                copyFrontier.add(bdd.id());
            }
            this.frontier = copyFrontier;
            this.frontierTotal = frontierTotal;
            this.notClosed = notClosed;
            this.reason = reason;
        }

        public ArrayList<BDD> getFrontier() {
            return frontier;
        }

        public BDD getFrontierTotal() {
            return frontierTotal;
        }

        public boolean isNotClosed() {
            return notClosed;
        }

        public void free() {
            this.frontierTotal.free();
            for (BDD bdd : frontier) {
                bdd.free();
            }
        }

        public String getReason() {
            return reason;
        }
    }
}
