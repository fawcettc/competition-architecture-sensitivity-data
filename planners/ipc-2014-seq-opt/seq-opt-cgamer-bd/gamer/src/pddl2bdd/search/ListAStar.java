/*
 * Gamer, a tool for finding optimal plans
 * Copyright (C) 2007-2012 by Peter Kissmann
 * modified by Alvaro Torralba (2012-2013)
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */
package pddl2bdd.search;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.logging.Level;
import net.sf.javabdd.BDD;
import pddl2bdd.problem.Problem;
import pddl2bdd.problem.VariablePartitioning;
import pddl2bdd.transition.TManagerBuilder;
import pddl2bdd.util.Log;

/**
 *
 * @author Peter Kissmann and Alvaro Torralba
 */
public class ListAStar extends AbstractAStar {

    protected int MIN_STATES_PARTITION_H = 0;
    protected boolean APPLY_HEURISTIC_0_COST = true;
    protected boolean APPLY_DEFERRED_SUCCESSORS = false;
    /**
     * Sets of unclassified states, organized by g.
     */
    public HashMap<Integer, BDD> unclassifiedStates;
    protected HashMap<Integer, HashMap<Integer, BDD>> deferredUnclassifiedStates;
    /**
     * This array is the unclassified states key set ordered in ascending order
     */
    protected ArrayList<Integer> unclassified_g;
    /**
     * F value of the current diagonal.
     */
    int currentDiagonal;
    /**
     * List with the f diagonals that are available.
     * Ordered ascendently.
     */
    public HashSet<Integer> searchSpace;

    public ListAStar(Problem problem, VariablePartitioning variables, TManagerBuilder tManager,
            PDBHeuristic heuristic) {
        super(problem, variables, tManager, heuristic);


        this.unclassifiedStates = new HashMap<Integer, BDD>();
        this.deferredUnclassifiedStates = new HashMap<Integer, HashMap<Integer, BDD>>();
        this.unclassified_g = new ArrayList<Integer>();
        this.currentDiagonal = -1;
        this.searchSpace = new HashSet<Integer>();


        if (MIN_STATES_PARTITION_H > 0 || !APPLY_HEURISTIC_0_COST) {
            PARTITION_FORWARD_REACHED = false;
        }
    }

    @Override
    public void search() {
        Log.getLog().log("Using List A*");
        this.currentDiagonal = this.heuristic.getH(init);
        if (this.currentDiagonal == -1) {
            Log.getLog().log("Error: initial state not found in pattern database!");
            System.exit(1);
        }
        
        Log.getLog().log("starting f-value: " + this.currentDiagonal);
        addUnclassifiedStates(0, init);

        BDD intersection = init.and(replacedGoal);
        boolean solution_found = !intersection.equals(factory.zero());
        if (solution_found) {
            Log.getLog().log("Goals are true in the initial state");
            init.printSet();
            replacedGoal.printSet();
            return;
        }
        intersection.free();

        boolean isEmpty;
        //For each diagonal
        do {
            Log.getLog().log(Level.INFO,"f: " + this.currentDiagonal);
            solution_found = exploreDiagonal();
            isEmpty = !nextFDiagonal();
        } while (!solution_found && !isEmpty);
        if (!solution_found) {
            Log.getLog().log("Exhausted state space and no solution found");
        }
    }

    /**
     * Expands a bucket (frontier is an array with only this bucket)
     * Apply all the operators.
     * The result is inserted on the corresponding unclassified buckets.
     *
     * If 0-cost is present, the h pdb is used with them. Or Not.
     * Besides, all the 0-cost reachable states are included in the frontier
     *
     * @return
     */
    private void expandBucket(ArrayList<BDD> frontier, int g, int h) {
        BDD tmp1, tmp2, to;
        BDD totalFrontier = frontier.get(0).id();

        BDD negatedReached = getForwardReached(h).not();
        tmp1 = totalFrontier;
        totalFrontier = tmp1.and(negatedReached);
        tmp1.free();
        if (totalFrontier.equals(factory.zero())) {
            negatedReached.free();
            totalFrontier.free();
            return;
        }

        Log.getLog().log(Level.INFO, "   g: " + g);

        //First we apply 0-cost operators
        if (tManagerBuilder.hasZeroCostOperators()) {
            BDD lastLayer = totalFrontier;

            while (true) {
                double time_now = (System.currentTimeMillis() - time_init) / 1000.0;
                /*Log.getLog().log("    bucket size: "
                        + lastLayer.satCount(variables.getCube())
                        + "[" + time_now + "s]");*/

                to = this.tmanager.image(0, lastLayer);

                tmp1 = to.and(negatedReached);
                to.free();
                tmp2 = totalFrontier.not();
                lastLayer = tmp1.and(tmp2); //Remove seen states
                tmp1.free();
                tmp2.free();

                if (APPLY_HEURISTIC_0_COST && (MIN_STATES_PARTITION_H == 0
                        || lastLayer.satCount(variables.getCube()) > MIN_STATES_PARTITION_H)) {
                    //Compute the set lastLayer and h
                    tmp1 = this.heuristic.getBDD(lastLayer, h);
                    tmp2 = tmp1.not();
                    to = lastLayer.and(tmp2);
                    lastLayer.free();
                    tmp2.free();
                    lastLayer = tmp1;

                    /*Log.getLog().log("We are skiping "
                    + to.satCount(cube) + " states");*/
                    //Add lastLayer and not(h) to unclassified
                    addUnclassifiedStates(g, to);
                    to.free();
                }
                if (lastLayer.equals(factory.zero())) {
                    break;
                }
                frontier.add(lastLayer);
                //tmp3.free();
                BDD oldFrontier = totalFrontier;
                totalFrontier = oldFrontier.or(lastLayer);
                oldFrontier.free();
            }
        }

        negatedReached.free();

        // Log.getLog().log("Include total frontier in forward reached");
        //Include total frontier in forward reached
        addForwardReached(h, totalFrontier);

        //Apply all the other operators in order
        double time_now = (System.currentTimeMillis() - time_init) / 1000.0;
        Log.getLog().log("    bucket size: " + totalFrontier.satCount(variables.getCube())
                + "[" + time_now + "s]");
        if (!APPLY_DEFERRED_SUCCESSORS) {
            HashMap<Integer, BDD> succ = this.tmanager.image(totalFrontier);
            for (int d : succ.keySet()) {
                to = succ.get(d);
                //Log.getLog().log("Add unclassified states");
                //Add the result to unclassified state buckets
                addUnclassifiedStates(g + d, to);
                to.free();
            }
        } else {
            for (int d : tManagerBuilder.getActionCosts()) { //For each action cost
                if (g + d > this.currentDiagonal) {
                    addDeferredUnclassifiedStates(g + d, d, totalFrontier);
                } else {
                    //Compute image
                    to = this.tmanager.image(d, totalFrontier);
                    //Log.getLog().log("Add unclassified states");
                    //Add the result to unclassified state buckets
                    addUnclassifiedStates(g + d, to);
                    to.free();
                }
            }
        }
        //Log.getLog().log("Free total frontier");
        totalFrontier.free();
        //Log.getLog().log("done.");
    }

    /**
     * This method explore the diagonal with f = f_min.
     * Expands all the buckets in the diagonal and left the new states in the
     * array of unclassified states.
     *
     * After reaching
     *
     * When adding a new unclassified state set (a new g),
     * the new possible f values are included in searchSpace
     *
     * Returns true if a goal has been found
     *
     */
    private boolean exploreDiagonal() {
        //We are going to explore the diagonal with f min
        int f_min = this.currentDiagonal;
        //All this g_values correspond with a position in the
        int g_min = -1;
        //For each unclassified g bucket
        while ((g_min = getNextUnclassifiedBucket(g_min)) <= f_min) {
            //Skip if there is not pdb for h
            if (!this.heuristic.contains(f_min - g_min)) {
                continue;
            }

            //Log.getLog().log("Next bucket");
            BDD bucket = getBDD(g_min, f_min - g_min);

            ArrayList<BDD> fgArrayList = new ArrayList<BDD>();
            fgArrayList.add(bucket);

            expandBucket(fgArrayList, g_min, f_min - g_min);

            //Log.getLog().log("Put solution");
            BDD forwardFrontier = put_solution(g_min, fgArrayList);

            if (g_min == f_min) {
                //Log.getLog().log("Checking if goal found");
                //Check if we have found the goal
                BDD intersection = replacedGoal.and(forwardFrontier);
                if (!intersection.equals(factory.zero())) {
                    //Solution found :D
                    forwardFrontier.free();

                    Log.getLog().log("   cheapest plan has cost of " + g_min);
                    reconstructPlanAStar(g_min, intersection);//.replace(variables.getS2sp()));

                    //intersection.free();
                    return true;
                }
                intersection.free();
            } else {
                forwardFrontier.free();
            }
        }

        //All the buckets in the diagonal are expanded and no solution found :(
        return false;
    }

    protected BDD getBDD(int g, int h) {
        if (APPLY_DEFERRED_SUCCESSORS
                && this.deferredUnclassifiedStates.containsKey(g)) {
            //Log.getLog().log("Recovering deferred successors of cost " + g);
            HashMap<Integer, BDD> deferredSuccessors =
                    this.deferredUnclassifiedStates.remove(g);
            for (int op : deferredSuccessors.keySet()) {
                BDD deferredBDD = deferredSuccessors.get(op);
                BDD tmp1 = this.tmanager.image(op, deferredBDD);

                deferredBDD.free();

                addUnclassifiedStates(g, tmp1);
                tmp1.free();
            }
        }

        BDD unclas = unclassifiedStates.remove(g);
        BDD states = this.heuristic.getBDD(unclas, h);
        BDD tmp = states.not();
        BDD newUnclas = unclas.and(tmp);
        unclas.free();
        tmp.free();

        if (newUnclas.equals(factory.zero())) {
            unclassified_g.remove(new Integer(g));
        } else {
            unclassifiedStates.put(g, newUnclas);
        }

        return states;
    }

    protected void addUnclassifiedStates(int g, BDD states) {
        BDD tmp = unclassifiedStates.get(g);
        if (tmp != null) {
            unclassifiedStates.put(g, tmp.or(states));
            tmp.free();
        } else {
            //New unclassified state bucket
            unclassifiedStates.put(g, states.id());
            int pos = Collections.binarySearch(unclassified_g, g);
            if (pos < 0) {
                unclassified_g.add(-pos - 1, g);
                //Here, we are adding new diagonals
                for (int h : this.heuristic.getOrderedHeuristicValues()) {
                    if (g + h > currentDiagonal) {
                        searchSpace.add(g + h);
                    }
                }
            }
        }
    }

    /**
     * Returns the unclassified bucket with lower g but strictly greater than g_min.
     *
     * @param g_min
     * @return
     */
    protected int getNextUnclassifiedBucket(int g_min) {
        int pos = Collections.binarySearch(unclassified_g, g_min);
        if (pos < 0) {
            pos = -pos - 1;
        } else {
            pos++;
        }
        if (pos >= unclassified_g.size()) {
            return Integer.MAX_VALUE;
        } else {
            return unclassified_g.get(pos);
        }
    }

    /**
     * Returns the f diagonal with lowest f
     * @return
     */
    protected boolean nextFDiagonal() {
        if (searchSpace.isEmpty()) {
            return false;
        }

        this.currentDiagonal = Collections.min(searchSpace);
        searchSpace.remove(this.currentDiagonal);
        return true;
    }

    protected void addDeferredUnclassifiedStates(int g, int op, BDD bdd) {
        //Log.getLog().log("Storing deferred successors of cost " + g);
        HashMap<Integer, BDD> def_pairs = this.deferredUnclassifiedStates.get(g);
        if (def_pairs == null) {
            def_pairs = new HashMap<Integer, BDD>();
            this.deferredUnclassifiedStates.put(g, def_pairs);
            int pos = Collections.binarySearch(unclassified_g, g);
            unclassified_g.add(-pos - 1, g);
            //Here, we are adding new diagonals
            for (int h : this.heuristic.getHeuristicValues()) {
                if (g + h > currentDiagonal) {
                    searchSpace.add(g + h);
                }
            }
        }
        BDD def_states = def_pairs.get(op);
        if (def_states == null) {
            def_pairs.put(op, bdd.id());
        } else {
            BDD tmp = def_states.or(bdd);
            def_states.free();
            def_pairs.put(op, tmp);
        }
    }

    @Override
    public void free() {
        for (BDD bdd : this.unclassifiedStates.values()) {
            bdd.free();
        }

        for (HashMap<Integer, BDD> def : this.deferredUnclassifiedStates.values()) {
            for (BDD bdd : def.values()) {
                bdd.free();
            }
        }
    }
}
