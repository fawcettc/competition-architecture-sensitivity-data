/*
 * Gamer, a tool for finding optimal plans
 * Copyright (C) 2007-2012 by Peter Kissmann
 * modified by Alvaro Torralba (2012-2013)
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */

package pddl2bdd.search;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import pddl2bdd.parser.gamer.logic.Predicate;
import pddl2bdd.problem.Problem;
import pddl2bdd.varOrder.InfluenceGraph;
import pddl2bdd.problem.VariablePartitioning;
import pddl2bdd.search.AbstractSolver.NotClosedType;
import pddl2bdd.transition.TManagerBuilder;
import pddl2bdd.util.Log;
import pddl2bdd.util.Time;

/**
 * This class has the algorithm for PDB generation.
 *
 * It uses a series of abstract backward search for improving the heuristic estimate
 * 
 * @author Peter Kissmann and Alvaro Torralba
 */
public class PDBGeneration implements Solver {

    Problem problem;
    TManagerBuilder tManagerBuilder;
    VariablePartitioning variables;
    AbstractSolver.NotClosedType mutexType;
    boolean useMutexBDDs;
    boolean useAbstraction;
    boolean superAbstraction;

    public PDBGeneration(Problem problem, VariablePartitioning variables,
            TManagerBuilder tManagerBuilder,
            List<String> params, boolean useMutexBDDs) {
        this.problem = problem;
        this.mutexType = AbstractSolver.NotClosedType.AND;
        this.variables = variables;
        this.useAbstraction = true;
        this.superAbstraction = false;
        this.tManagerBuilder = tManagerBuilder;
        this.useMutexBDDs = useMutexBDDs;
        this.parseParams(params);
    }

    private void parseParams(List<String> params) {
        for (String param : params) {
            param = param.toLowerCase();
            if (param.equals("constrain")) {
                this.mutexType = NotClosedType.CONSTRAIN;
            } else if (param.equals("and")) {
                this.mutexType = NotClosedType.AND;
            } else if (param.equals("restrict")) {
                this.mutexType = NotClosedType.RESTRICT;
            } else if (param.equals("npand")) {
                this.mutexType = NotClosedType.NP_AND;
            } else if (param.equals("licompaction")) {
                this.mutexType = NotClosedType.LI_COMPACTION;
            } else {
                System.err.println("Unkwown parameter in pdb generation: " + param);
                System.exit(-1);
            }
        }
        
        Log.getLog().log("MutexType: " + this.mutexType.toString());
    }

    public void search() {
        if (useAbstraction) {
            this.useAbstraction(problem, variables);
        } else {
            this.useNoMoreAbstraction(null, null);
        }
    }

    @Override
    public void cleanup() {
    }

    /**
     * Construct a PDB without abstraction (pure backward searchs).
     * 
     * @param pdbID init with partFileName.split("-", 2)[0].substring(8);
     * @param abstractionFileName
     * initialization with partFileName.substring(0, partFileName.length() - 8);
     */
    private void useNoMoreAbstraction(String pdbID, String abstractionFileName) {
        try {
            File usedPDBsFile = new File("usePDBs.txt");
            FileWriter usePDBs = new FileWriter("usePDBs_tmp.txt");
            if (usedPDBsFile.exists()) {
                LinkedList<String> usedPDBs = new LinkedList<String>();
                BufferedReader bufferedReader = new BufferedReader(
                        new FileReader("usePDBs.txt"));
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    usedPDBs.add(line);
                }
                bufferedReader.close();
                for (String used : usedPDBs) {
                    usePDBs.write(used + "\n");
                }
            }
            usePDBs.write(pdbID + "\n");
            usePDBs.flush();
            usePDBs.close();

            FileWriter maxPDB = new FileWriter(abstractionFileName + "MaxPDB.txt");
            maxPDB.write("-1\n");
            maxPDB.flush();
            maxPDB.close();
            FileWriter exPDB = new FileWriter(abstractionFileName + "ExPDBs.txt");
            exPDB.write("");
            exPDB.flush();
            exPDB.close();
            Runtime.getRuntime().exec(
                    "mv usePDBs_tmp.txt usePDBs.txt");
        } catch (Exception e) {
            System.err.println("Error: " + e.getMessage());
            e.printStackTrace(System.err);
            System.exit(1);
        }

        Log.getLog().log("building PDB ...");
        long time1 = System.currentTimeMillis();
        LinkedList<Integer> emptyPartitions = new LinkedList<Integer>();
        PDBSearch absMaker = new PDBSearch(problem, variables, tManagerBuilder,
                emptyPartitions, "pdb0", this.mutexType, useMutexBDDs, true);

        if (superAbstraction) {
            absMaker.buildSuperPDB();
        } else {
            absMaker.buildPDB(false);
        }
        long time2 = System.currentTimeMillis();
        Log.getLog().log("done.");
        Log.getLog().log("construction time: "
                + Time.printTime(time2 - time1));
        Log.getLog().log("cleaning up ...");
        absMaker.cleanup();
        Log.getLog().log("done.");
    }

    private void useAbstraction(Problem problem, VariablePartitioning variables) {
        InfluenceGraph influences = new InfluenceGraph(problem, variables);
        LinkedList<Integer> emptyPartitions = new LinkedList<Integer>();
        LinkedList<Integer> chosenPartitions = new LinkedList<Integer>();
        ArrayList<Predicate> goalPreds = new ArrayList<Predicate>();
        problem.getGoalDescription().getAllPredicates(goalPreds);

        int index = 0;
        for (List<String> group : variables.getPartitionedVariables()) {
            boolean isGoal = false;
            for (Predicate goal : goalPreds) {
                if (group.contains(goal.getName())) {
                    isGoal = true;
                    break;
                }
            }
            if (isGoal) {
                chosenPartitions.add(index);
            } else {
                emptyPartitions.add(index);
            }
            index++;
        }
        int bestIndex = 1;
        double bestAverage = -1;
        int additional;
        double[] averages;

        writeFile("-1\n", "abstract1-1MaxPDB.txt");
        writeFile("", "abstract1-1ExPDBs.txt");
        writeFile("1\n", "usePDBs.txt");

        long endingTime = System.currentTimeMillis();
        //Create first pdb with only goal variables
        PDBSearch absMaker = new PDBSearch(problem, variables, tManagerBuilder,
                emptyPartitions, "abstract1-1", this.mutexType, this.useMutexBDDs, true); //first PDB is partial
        Log.getLog().log("Initialization took "
                + Time.printTime(System.currentTimeMillis() - endingTime));

        Log.getLog().log("building PDB ...");
        long time1 = System.currentTimeMillis();
        if (superAbstraction) {
            absMaker.buildSuperPDB();
        } else {
            bestAverage = absMaker.buildPDB(true);
        }
        long time2 = System.currentTimeMillis();
        Log.getLog().log("done.");
        Log.getLog().log("construction time: "
                + Time.printTime(time2 - time1));
        Log.getLog().log("cleaning up ...");
        absMaker.cleanup();
        Log.getLog().log("done.");

        while (bestIndex != -1) {
            additional = bestIndex + 1;
            bestIndex = -1;
            averages = new double[emptyPartitions.size()];
            long timeLastIt = System.currentTimeMillis();
            //For each element interface empty partitions influencing our
            //already chosen partitions we try to remove it and generate a new PDB
            for (index = 0; index < emptyPartitions.size(); index++) {
                boolean isInfluencing = false;
                Log.getLog().log("index: " + index);
                int oldEmptyPartition = emptyPartitions.get(index);
                for (int chosen : chosenPartitions) {
                    if (influences.influence(oldEmptyPartition, chosen)) {//influences[variableOrdering[oldEmptyPartition]][variableOrdering[chosen]]) {
                        isInfluencing = true;
                        break;
                    }
                }
                if (!isInfluencing) {
                    continue;
                }

                emptyPartitions.remove(index); //Remove the variable
                endingTime = System.currentTimeMillis();

                //Initialize the PDB
                absMaker = new PDBSearch(problem, variables, tManagerBuilder,
                        emptyPartitions,
                        "abstract" + (index + additional) + "-1", this.mutexType, this.useMutexBDDs, false);

                Log.getLog().log("Initialization took "
                        + Time.printTime(System.currentTimeMillis() - endingTime));
                Log.getLog().log("building PDB ...");
                time1 = System.currentTimeMillis();


                if (superAbstraction) {
                    absMaker.buildSuperPDB();
                } else {
                    averages[index] = absMaker.buildPDB(true);
                }

                if (averages[index] > bestAverage) {
                    bestAverage = averages[index];
                    bestIndex = index + additional;
                    absMaker.writeToFile();
                    writeFile(bestIndex + "\n", "usePDBs.txt");
                    Log.getLog().log("Best selected: " + bestIndex + " with value " + bestAverage);
                    
                }
                time2 = System.currentTimeMillis();
                Log.getLog().log("done.");
                Log.getLog().log("construction time: " + Time.printTime(time2 - time1));
                Log.getLog().log("cleaning up ...");
                absMaker.cleanup();
                Log.getLog().log("done.");
                emptyPartitions.add(index, oldEmptyPartition);
            }
            Log.getLog().log("best index: " + bestIndex
                    + "; best average: " + bestAverage);

            if (bestIndex != -1) {
                int numberAdded = 0;
                for (int i = emptyPartitions.size() - 1; i >= 0; i--) {
                    if (averages[i] >= 0.999 * bestAverage) {
                        Log.getLog().log("also chosen index: "
                                + (i + additional) + "; average: "
                                + averages[i]);
                        chosenPartitions.add(emptyPartitions.remove(i));
                        numberAdded++;
                    }
                }
                if (numberAdded > 1) {
                    // if adding more than one group, the new PDB
                    // will be generated, as it might contain all
                    // groups and would not be generated otherwise
                    absMaker = new PDBSearch(problem, variables, tManagerBuilder,
                            emptyPartitions, "abstract" + (bestIndex + 1) + "-1", this.mutexType, this.useMutexBDDs, false);
                    Log.getLog().log("building new PDB ...");
                    time1 = System.currentTimeMillis();
                    if (superAbstraction) {
                        absMaker.buildSuperPDB();
                    } else {
                        averages[0] = absMaker.buildPDB(true);
                        Log.getLog().log("average: " + averages[0]);
                        if (averages[0] > bestAverage) {
                            bestAverage = averages[0];
                            bestIndex = bestIndex + 1;
                            absMaker.writeToFile();
                            writeFile(bestIndex + "\n", "usePDBs.txt");
                            Log.getLog().log("Best selected2: " + bestIndex + " with value " + bestAverage);
                        }
                    }
                    time2 = System.currentTimeMillis();
                    Log.getLog().log("done.");
                    Log.getLog().log("construction time: "
                            + Time.printTime(time2 - time1));
                    Log.getLog().log("cleaning up ...");
                    absMaker.cleanup();
                    Log.getLog().log("done.");
                }
            }
        }
        Log.getLog().log("done.");
    }

    private static void writeFile(String text, String file) {
        String tmpFile = file + ".tmp";
        try {
            FileWriter fw = new FileWriter(tmpFile);
            fw.write(text);
            fw.flush();
            fw.close();
            //Log.getLog().log("Writing " + file + " with " +text);
            Runtime.getRuntime().exec("mv " + tmpFile + " " + file);
        } catch (Exception e) {
            System.err.println("Error: " + e.getMessage());
            e.printStackTrace(System.err);
            System.exit(1);
        }
    }
}
