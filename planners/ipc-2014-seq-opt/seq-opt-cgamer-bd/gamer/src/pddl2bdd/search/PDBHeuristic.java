/*
 * Gamer, a tool for finding optimal plans
 * Copyright (C) 2007-2012 by Peter Kissmann
 * modified by Alvaro Torralba (2012-2013)
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */
package pddl2bdd.search;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import net.sf.javabdd.BDD;
import net.sf.javabdd.BDDFactory;
import pddl2bdd.util.Log;

/**
 * Methods for loading a set of PDBs from disk and using them as heuristic.
 * 
 * @author Peter Kissmann and Alvaro Torralba
 */
public class PDBHeuristic {

    BDDFactory factory;
    BDD zeroBDD;

    /**
     * PDB BDDs organized by h.
     */
    protected HashMap<Integer, BDD> heuristicToBDDs;

    /**
     * List of loaded pdbs
     */
    protected int maxH;

    public PDBHeuristic(BDDFactory factory) {
        this.factory = factory;
        this.zeroBDD = factory.zero();
        File file = new File("usePDBs.txt");
        LinkedList<String> pdbsToUse = new LinkedList<String>();
        if (file.exists()) {
            try {
                BufferedReader bufferedReader = new BufferedReader(
                        new FileReader("usePDBs.txt"));
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    Log.getLog().log("Using PDB: " + line);
                    pdbsToUse.add(line);
                }
                bufferedReader.close();
            } catch (Exception e) {
                System.err.println("Error: " + e.getMessage());
                e.printStackTrace(System.err);
            }
        }

        // read PDBs
        Log.getLog().log("   reading PDBs ...");
        String filename;
         LinkedList<HashMap<Integer, BDD>> pdbs = new LinkedList<HashMap<Integer, BDD>>();

        
        for (String nextPDB : pdbsToUse) {
            filename = "abstract" + nextPDB + "-1ExPDBs.txt";
            Log.getLog().log("      reading PDB " + filename);
            HashMap<Integer, BDD> loadedPDB = loadPDB(filename);
            pdbs.add(loadedPDB);
        }

        if (pdbs.size() == 0) {
            System.err.println("Warning: no PDB-files found!");
            System.err.println("Using empty PDB!");
            heuristicToBDDs = new HashMap<Integer, BDD>();
            heuristicToBDDs.put(0, factory.one());
            this.maxH = 0;
        } else {
            this.heuristicToBDDs = pdbs.get(0);
        }
        

        // build heuristic
        Log.getLog().log("   building the heuristic ...");

        for(int h : this.heuristicToBDDs.keySet()){
            Log.getLog().log("h value: " + h + " nodes: " + this.heuristicToBDDs.get(h).nodeCount());
        }

        Log.getLog().log("maximal possible h-value: " + maxH);
        Log.getLog().log("   done.");
    }

    private HashMap<Integer, BDD> loadPDB(String filename) {
        File file = new File(filename);
        HashMap<Integer, BDD> newPDB = new HashMap<Integer, BDD>();
        ArrayList<Integer> hValues = new ArrayList<Integer>();
        int maxHPDB = -1;
        try {
            BDD statesNotInPDB = factory.one();
            //Read max pdb to initialize maxH
            //String filename2 = filename + "MaxPDB.txt";
            //BufferedReader bufferedReader = new BufferedReader(
            //        new FileReader(filename2));
            String line;
            /*while ((line = bufferedReader.readLine()) != null) {
                maxH = Integer.parseInt(line);
            }
            bufferedReader.close();*/
            
            BufferedReader fpdb = new BufferedReader(new FileReader(filename));
            boolean partialPDB = true;
            while ((line = fpdb.readLine()) != null) {
                if(line.trim().toLowerCase().equals("finished")){
                    partialPDB = false;
                    continue;
                }
                file = new File(line);
                String[] lineParts = line.split("_");
                int index = Integer.parseInt(lineParts[1]);
                maxHPDB = Math.max(index, maxHPDB);
                if (file.exists()) {
                    BDD pdbState = factory.load(line);
                    newPDB.put(index, pdbState);
                    BDD tmp1 = pdbState.not();
                    BDD tmp2 = statesNotInPDB;
                    statesNotInPDB = tmp2.and(tmp1);
                    tmp1.free();
                    tmp2.free();
                    hValues.add(index);
                }
            }
            fpdb.close();
            
            if(partialPDB){
                if (!statesNotInPDB.equals(zeroBDD)) {
                    newPDB.put(maxHPDB + 1, statesNotInPDB);
                    hValues.add(maxHPDB + 1);
                }
            }else {
                maxHPDB = Integer.MAX_VALUE;
                hValues.add(Integer.MAX_VALUE);
            }
            
        } catch (Exception e) {
            System.err.println("Error: " + e.getMessage());
            e.printStackTrace(System.err);
            System.exit(1);
        }

        this.maxH = Math.max(maxH, maxHPDB);

        return newPDB;
    }

    private HashMap<Integer, BDD> findAllHeuristicValues(LinkedList<HashMap<Integer, BDD>> pdbs) {
        if(pdbs.size() == 1) return pdbs.get(0);
        
        HashMap<Integer, BDD> heuristicValues = new HashMap<Integer, BDD>();
        int h_value = 0;
        HashSet <Integer> hValuesSet = new HashSet<Integer>();
        for(HashMap<Integer, BDD> pdb : pdbs){
            hValuesSet.addAll(pdb.keySet());
        }
        ArrayList <Integer> hValues = new ArrayList<Integer>(hValuesSet);
        Collections.sort(hValues);

        BDD openList = factory.zero();
        for (int h : hValues) {
            BDD bddH = factory.zero();

            for(HashMap<Integer, BDD> pdb : pdbs){
                if(pdb.containsKey(h)){
                    BDD tmp = bddH;
                    bddH = tmp.or(pdb.get(h));
                    tmp.free();
                }
            }
            BDD tmp = bddH;
            bddH = openList.and(bddH);
            tmp.free();
            tmp = openList;
            openList = openList.or(bddH);
            tmp.free();

            if (!bddH.equals(factory.zero())) {
                heuristicValues.put(h, bddH);
            }
        }
        
        openList.free();

        return heuristicValues;
    }


    protected BDD getBDD(BDD bucket, int h) {
        if (this.heuristicToBDDs.containsKey(h)) {
            return this.heuristicToBDDs.get(h).and(bucket);
        }else{
            return zeroBDD;
        }
    }

    /**
     * Returns the minimun h value for any state in the BDD
     * @param bdd
     * @return -1 if the states are not found in the PDB
     */
    public int getH(BDD bdd) {
        Set<Integer> keySet = heuristicToBDDs.keySet();
        List<Integer> allKeys = new ArrayList<Integer>(keySet);
        Collections.sort(allKeys);
        for (int h : allKeys) {
            BDD foundBDD = this.getBDD(bdd, h);
            if (!foundBDD.equals(zeroBDD)) {
                foundBDD.free();
                return h;
            }
            foundBDD.free();
        }

        //Result not found in the bdd
        return -1;
    }

    public Set<Integer> getHeuristicValues() {
        return this.heuristicToBDDs.keySet();
    }

    public List<Integer> getOrderedHeuristicValues() {
        List<Integer> allKeys =
                new ArrayList<Integer>(this.getHeuristicValues());
        Collections.sort(allKeys);
        return allKeys;
    }

    public int getMinH() {
        return Collections.min(this.getHeuristicValues());
    }

    public boolean contains(int h_value) {
        return this.heuristicToBDDs.containsKey(h_value);
    }

    public void cleanup(){
        for(BDD bdd : this.heuristicToBDDs.values()){
            bdd.free();
        }
        this.zeroBDD.free();
    }
}
