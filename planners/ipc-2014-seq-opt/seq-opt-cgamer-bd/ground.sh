#!/bin/bash

mkdir ground
cp $1 ground/domain.pddl
cp $2 ground/problem.pddl
launchpath=$(pwd)
scriptpath=$(dirname "$0")
cd $scriptpath
scriptpath=$(pwd)
cd $launchpath

cd ground
sed "s/).*-.*number/)/" domain.pddl > dom.pddl
mv dom.pddl domain.pddl
$scriptpath/ground/ground domain.pddl problem.pddl
sed "s/.*total-cost.*//g" problem.pddl > prob.pddl
mv prob.pddl problem.pddl
sed "s/^/(/g" problem.psas > prob.psas
mv prob.psas newProblem.psas
sed "s/$/)/g" newProblem.psas > prob.psas
mv prob.psas newProblem.psas
sed "s/()/[]/g" newProblem.psas > prob.psas
mv prob.psas newProblem.psas
sed '$d' newProblem.psas > prob.psas
mv prob.psas newProblem.psas
cp newProblem.psas ../orig-1nPar.gdl
#cp newProblem.psas ../abstract0-1nPar.gdl

cd ..

mv ground/domain.pddl orig-1Dom.gdl
mv ground/problem.pddl orig-1Prob.gdl
mv ground/problem.psas orig-1Part.gdl

