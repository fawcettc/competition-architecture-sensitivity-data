#!/bin/bash

#module load java/Oracle-jdk1.7.0_45/i386

launchpath=$(pwd)
scriptpath=$(dirname "$0")
cd $scriptpath
scriptpath=$(pwd)
cd $launchpath

if [ $SYSTEM_VERSION = "x86_64" ]; then
    CG_JAVA_OPTIONS="-d64 -Xms96m -Xmx1536m -XX:-UseGCOverheadLimit -Dbdd=cudd"
else
    CG_JAVA_OPTIONS="-Xms96m -Xmx1000m -XX:-UseGCOverheadLimit -Dbdd=cudd"
fi 

CG_JAVA_CLASSPATH="-classpath $scriptpath/gamer.jar:$scriptpath/javabdd-2.0.jar:$scriptpath/gamer/lib/jcommander-1.31-SNAPSHOT.jar"

cp orig-1Dom.gdl abstract0-1Dom.gdl
cp orig-1Prob.gdl abstract0-1Prob.gdl
cp orig-1Part.gdl abstract0-1Part.gdl

sleep $1 &
sleepPid=$!

java $CG_JAVA_OPTIONS $CG_JAVA_CLASSPATH pddl2bdd.Main -P abstract0-1nPar.gdl -a pdb "${@:2}"
javaPid=$!

while [ 1 ]
do
    ps -p $sleepPid | grep sleep > /dev/null
    result=$?
    if [ $result -eq 1 ]
    then
	ps -p $javaPid | grep java > /dev/null
	result=$?
	if [ $result -eq 0 ]
	then
	    echo "KILLING PDB GENERATION"
	    kill $javaPid
	fi
	break
    fi
    ps -p $javaPid | grep java > /dev/null
    result=$?
    if [ $result -eq 1 ]
    then
	ps -p $sleepPid | grep sleep > /dev/null
	result=$?
	if [ $result -eq 0 ]
	then
	    echo "KILLING SLEEP"
	    kill $sleepPid
	fi
	break
    fi
    sleep 1
done

echo "done pdb_gen"
