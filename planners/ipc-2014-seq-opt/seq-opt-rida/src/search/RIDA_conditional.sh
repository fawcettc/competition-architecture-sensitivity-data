#!/bin/bash
BASEDIR="$(dirname "$0")"
$BASEDIR/downward --plan-file $1 --Phase "SAMPLING" --search "astar(max([\
    hmax(),\
    blind(),\
    ]))"
