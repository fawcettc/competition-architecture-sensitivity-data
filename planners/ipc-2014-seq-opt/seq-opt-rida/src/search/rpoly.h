#ifndef _RPOLY_
#define _RPOLY_

#include "math.h"

int rpoly(double *op, int degree, double *zeror, double *zeroi);

#endif
