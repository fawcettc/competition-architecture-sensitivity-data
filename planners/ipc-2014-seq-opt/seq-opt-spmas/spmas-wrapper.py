#!/usr/bin/env python2.7
# encoding: utf-8

'''
spmas-wrapper -- Target algorithm wrapper for the version of spmas used in the IPC 2014 for the sequential optimal track

@author:     Andrea F. Bocchese
@copyright:  
@license:    
@contact:    afbocch@cs.ubc.ca

example call:
python <path to wrapper>/spmas-wrapper.py <instance> "<domain>" <runtime cutoff> <runlength cutoff> <seed>
'''

import sys
import re
import os

from subprocess import Popen, PIPE

from genericWrapper import AbstractWrapper

class SpmasWrapper(AbstractWrapper):
    '''
        Simple wrapper for spmas
    '''
    def __init__(self):
        AbstractWrapper.__init__(self)
        self.__script_dir = os.path.abspath(os.path.split(__file__)[0])

        self.parser.add_argument("--solution-validator", dest="solution_validator", default=None, help="binary of the VAL plan validator")

    def get_command_line_args(self, runargs, config):
        '''
        Returns the command line call string to execute the target algorithm
        Args:
            runargs: a map of several optional arguments for the execution of the target algorithm.
                    {
                      "instance": <instance>,
                      "specifics" : <extra data associated with the instance>,
                      "cutoff" : <runtime cutoff>,
                      "runlength" : <runlength cutoff>,
                      "seed" : <seed>
                    }
            config: a mapping from parameter name to parameter value
        Returns:
            A command call list to execute the target algorithm.
        '''
        tmpdir = os.path.abspath(runargs["tmp"])

        self.__solution_file_path = "%s/planner_output" % (tmpdir)

        self.__instance_abs_path = os.path.abspath(runargs["instance"])
        self.__domain_abs_path = os.path.abspath(runargs["specifics"])

        cmd = ["%s/plan" % (self.__script_dir), self.__domain_abs_path, self.__instance_abs_path, self.__solution_file_path]

        for key, value in config.iteritems():
            cmd.extend([key, value])

        return cmd

    def process_results(self, filepointer, out_args):
        '''
        Parse a results file to extract the run's status (SUCCESS/CRASHED/etc) and other optional results.

        Args:
            filepointer: a pointer to the file containing the solver execution standard out.
            out_args : a map with {"exit_code" : exit code of target algorithm}
        Returns:
            A map containing the standard AClib run results. The current standard result map as of AClib 2.06 is:
            {
                "status" : <"SAT"/"UNSAT"/"TIMEOUT"/"CRASHED"/"ABORT">,
                "runtime" : <runtime of target algrithm>,
                "quality" : <a domain specific measure of the quality of the solution [optional]>,
                "misc" : <a (comma-less) string that will be associated with the run [optional]>
            }
            ATTENTION: The return values will overwrite the measured results of the runsolver (if runsolver was used). 
        '''
        data = filepointer.read()
        resultMap = {}

        matchResult = re.search("Solution found.\n", data)

        if os.path.isfile(self.__solution_file_path):
            # check for validator, validate if present
            validator_abspath = os.path.abspath(self.args.solution_validator)
            cmd = [validator_abspath, "-S", self.__domain_abs_path, self.__instance_abs_path, self.__solution_file_path]
            io = Popen(cmd, stdout=PIPE)
            out_,err_ = io.communicate()
            failedResult = re.search('failed', out_)
            planCostResult = re.search("(%s)" % (self.float_regex()), out_)
            if  planCostResult and (not failedResult):
                resultMap['status'] = 'SUCCESS'
                resultMap['quality'] = planCostResult.group(1)
                resultMap['misc'] = "plan verified by VAL"
            else:
                resultMap['status'] = 'CRASHED'
                resultMap['quality'] = "-1"
                resultMap['misc'] = "produced plan was ruled invalid by VAL"
            if (not matchResult):
                resultMap['misc'] = "solver does not found solution, but solution file found"
        elif matchResult:
            resultMap['status'] = 'CRASHED'
            resultMap['quality'] = "-1"
            resultMap['misc'] = "solver found a solution, but no solution file found"

        return resultMap

if __name__ == "__main__":
    wrapper = SpmasWrapper()
    wrapper.main()

