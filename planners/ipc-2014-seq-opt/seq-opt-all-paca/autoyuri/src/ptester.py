#!/usr/local/bin/python

import sys, getopt, os, csv, re, random, collections

import numpy as np
from sklearn.ensemble  import RandomForestRegressor
import cPickle

# python ptester.py -m ../results -f "2,6,0,6,3,248,25977,11,0,0,0,0,0,0,0,0,0,0,0,0,0,3,6,4,1.4142,2,5,3,1.4142,2,4,2.6667,0.9428,1,2,1.3333,0.4714,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1"

def main(argv):
    mfolder=''
    features=''
    help_msg = ''
    
    #######################################################################################
    #######################################################################################
    
    # Parse the input arguments
    try:
        opts, args = getopt.getopt(argv,"hm:f:",["mfolder=","features="])
    except getopt.GetoptError:
        print help_msg
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print help_msg
            sys.exit()
        elif opt in ("-m", "--mfolder"):
            mfolder = arg
        elif opt in ("-f", "--features"):
            features = arg

    features = [float(x) for x in features.split(',')]

    #######################################################################################
    #######################################################################################

    predictions = []
    solvers = []

    model_files = os.listdir(mfolder)
    for model in model_files:
        if model[0] == ".":
            continue
        
        with open(("%s/%s"%(mfolder,model)), 'rb') as f:
            rf = cPickle.load(f)

        solver = model.split("_##_")[1].split(".")[0]
        preds = rf.predict(features)

        predictions.append(preds)
        solvers.append(solver)

    print predictions

    print solvers[predictions.index(min(predictions))]

if __name__ == "__main__":
    main(sys.argv[1:])