#!/usr/local/bin/python

import sys, getopt, os, csv, re, random, collections

import numpy as np
from sklearn.ensemble  import RandomForestRegressor
import cPickle

# python ptrainer.py -c 1800 -n 2011-seq -o ../results -f ../data/PLAN/2011-seq-opt.features -t ../data/PLAN/2011-seq-opt.times

def main(argv):
    prefix = 'temp'
    ofolder = '../results'
    ffile = ''
    tfile = ''
    
    timeout = 5000
    seed = 12345
    maximize = False
    useTimeout = True
    help_msg = 'ptrainer.py -i <inputfolder> -f <featurefolder> -o <outputroot>'
    
    #######################################################################################
    #######################################################################################
    
    # Parse the input arguments
    try:
        opts, args = getopt.getopt(argv,"hc:n:s:o:f:t:",["timeout=","name=","seed=","outfolder=","features=","times="])
    except getopt.GetoptError:
        print help_msg
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print help_msg
            sys.exit()
        elif opt in ("-c", "--timeout"):
            timeout = int(arg)
        elif opt in ("-n", "--name"):
            prefix = arg
        elif opt in ("-s", "--seed"):
            seed = int(arg)
        elif opt in ("-o", "--outfolder"):
            ofolder = arg
        elif opt in ("-f", "--features"):
            ffile = arg
        elif opt in ("-t", "--times"):
            tfile = arg


    print 'Output folder is ', ofolder
    print 'Feature file is ', ffile
    print 'Time file is ', tfile
    print 'Output prefix is ', prefix

    random.seed(seed)

    #######################################################################################
    #######################################################################################

    # Read in the instance feature and performance data from file
    features = np.delete(np.array(list(csv.reader(open(ffile, 'rb'), delimiter=','))), (0), axis=0)
    times    = np.array(list(csv.reader(open(tfile, 'rb'), delimiter=',')))

    solvers  = times[0,1:]

    times    = np.delete(times, (0), axis=0)

    nFeatures = len(features[0,1:])
    nSolvers  = len(times[0,1:])

    # Find the instances that are in both sets
    f_inst = features[:,0].tolist()
    t_inst = times[:,0].tolist()
    a_inst = [x for x, y in collections.Counter(f_inst+t_inst).items() if y > 1]

    # Create training sets for those instances we have both feature and time data, and also
    # those where at least one solver returns a solution within the timeout
    train_features = np.empty(shape=[0,nFeatures])
    train_times = np.empty(shape=[0,nSolvers])
    for inst in a_inst:
        index_f = f_inst.index(inst)
        index_t = t_inst.index(inst)

        new_feat = [ float(x) for x in features[index_f,1:] ]
        new_time = [ float(x) for x in times[index_t,1:] ]

        if any(i < timeout for i in new_time ):
            if not np.std(new_time) == 0:
                new_time = (new_time - np.mean(new_time))/np.std(new_time)
            else:
                new_time = [0]*len(new_time)
            
            train_features = np.vstack([train_features, new_feat])
            train_times = np.vstack([train_times, new_time])

    # Train and save a random forest for each solver
    for ss in range(0, nSolvers):
        rf = RandomForestRegressor(n_estimators=300,
                                      random_state=seed)
        rf.fit( train_features, train_times[:,ss] )
        with open(("%s/%s-forest_##_%s.pkl" %(ofolder, prefix, solvers[ss])), 'wb') as f:
            cPickle.dump(rf, f)


if __name__ == "__main__":
    main(sys.argv[1:])