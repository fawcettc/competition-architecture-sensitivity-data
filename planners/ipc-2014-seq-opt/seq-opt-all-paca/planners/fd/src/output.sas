begin_version
3
end_version
begin_metric
1
end_metric
82
begin_variable
var0
-1
2
Atom arrived(car0, junction0-0)
NegatedAtom arrived(car0, junction0-0)
end_variable
begin_variable
var1
-1
2
Atom arrived(car0, junction0-1)
NegatedAtom arrived(car0, junction0-1)
end_variable
begin_variable
var2
-1
2
Atom arrived(car0, junction1-0)
NegatedAtom arrived(car0, junction1-0)
end_variable
begin_variable
var3
-1
2
Atom arrived(car0, junction1-1)
NegatedAtom arrived(car0, junction1-1)
end_variable
begin_variable
var4
-1
2
Atom arrived(car1, junction0-0)
NegatedAtom arrived(car1, junction0-0)
end_variable
begin_variable
var5
-1
2
Atom arrived(car1, junction0-1)
NegatedAtom arrived(car1, junction0-1)
end_variable
begin_variable
var6
-1
2
Atom arrived(car1, junction1-0)
NegatedAtom arrived(car1, junction1-0)
end_variable
begin_variable
var7
-1
2
Atom arrived(car1, junction1-1)
NegatedAtom arrived(car1, junction1-1)
end_variable
begin_variable
var8
-1
2
Atom at_car_jun(car0, junction0-0)
NegatedAtom at_car_jun(car0, junction0-0)
end_variable
begin_variable
var9
-1
2
Atom at_car_jun(car0, junction0-1)
NegatedAtom at_car_jun(car0, junction0-1)
end_variable
begin_variable
var10
-1
2
Atom at_car_jun(car0, junction1-0)
NegatedAtom at_car_jun(car0, junction1-0)
end_variable
begin_variable
var11
-1
2
Atom at_car_jun(car0, junction1-1)
NegatedAtom at_car_jun(car0, junction1-1)
end_variable
begin_variable
var12
-1
2
Atom at_car_jun(car1, junction0-0)
NegatedAtom at_car_jun(car1, junction0-0)
end_variable
begin_variable
var13
-1
2
Atom at_car_jun(car1, junction0-1)
NegatedAtom at_car_jun(car1, junction0-1)
end_variable
begin_variable
var14
-1
2
Atom at_car_jun(car1, junction1-0)
NegatedAtom at_car_jun(car1, junction1-0)
end_variable
begin_variable
var15
-1
2
Atom at_car_jun(car1, junction1-1)
NegatedAtom at_car_jun(car1, junction1-1)
end_variable
begin_variable
var16
-1
2
Atom at_car_road(car0, road0)
NegatedAtom at_car_road(car0, road0)
end_variable
begin_variable
var17
-1
2
Atom at_car_road(car0, road1)
NegatedAtom at_car_road(car0, road1)
end_variable
begin_variable
var18
-1
2
Atom at_car_road(car0, road2)
NegatedAtom at_car_road(car0, road2)
end_variable
begin_variable
var19
-1
2
Atom at_car_road(car0, road3)
NegatedAtom at_car_road(car0, road3)
end_variable
begin_variable
var20
-1
2
Atom at_car_road(car1, road0)
NegatedAtom at_car_road(car1, road0)
end_variable
begin_variable
var21
-1
2
Atom at_car_road(car1, road1)
NegatedAtom at_car_road(car1, road1)
end_variable
begin_variable
var22
-1
2
Atom at_car_road(car1, road2)
NegatedAtom at_car_road(car1, road2)
end_variable
begin_variable
var23
-1
2
Atom at_car_road(car1, road3)
NegatedAtom at_car_road(car1, road3)
end_variable
begin_variable
var24
-1
2
Atom clear(junction0-0)
NegatedAtom clear(junction0-0)
end_variable
begin_variable
var25
-1
2
Atom clear(junction0-1)
NegatedAtom clear(junction0-1)
end_variable
begin_variable
var26
-1
2
Atom clear(junction1-0)
NegatedAtom clear(junction1-0)
end_variable
begin_variable
var27
-1
2
Atom clear(junction1-1)
NegatedAtom clear(junction1-1)
end_variable
begin_variable
var28
-1
2
Atom in_place(road0)
NegatedAtom in_place(road0)
end_variable
begin_variable
var29
-1
2
Atom in_place(road1)
NegatedAtom in_place(road1)
end_variable
begin_variable
var30
-1
2
Atom in_place(road2)
NegatedAtom in_place(road2)
end_variable
begin_variable
var31
-1
2
Atom in_place(road3)
NegatedAtom in_place(road3)
end_variable
begin_variable
var32
-1
2
Atom road_connect(road0, junction0-0, junction0-1)
NegatedAtom road_connect(road0, junction0-0, junction0-1)
end_variable
begin_variable
var33
-1
2
Atom road_connect(road0, junction0-0, junction1-0)
NegatedAtom road_connect(road0, junction0-0, junction1-0)
end_variable
begin_variable
var34
-1
2
Atom road_connect(road0, junction0-0, junction1-1)
NegatedAtom road_connect(road0, junction0-0, junction1-1)
end_variable
begin_variable
var35
-1
2
Atom road_connect(road0, junction0-1, junction0-0)
NegatedAtom road_connect(road0, junction0-1, junction0-0)
end_variable
begin_variable
var36
-1
2
Atom road_connect(road0, junction0-1, junction1-0)
NegatedAtom road_connect(road0, junction0-1, junction1-0)
end_variable
begin_variable
var37
-1
2
Atom road_connect(road0, junction0-1, junction1-1)
NegatedAtom road_connect(road0, junction0-1, junction1-1)
end_variable
begin_variable
var38
-1
2
Atom road_connect(road0, junction1-0, junction0-0)
NegatedAtom road_connect(road0, junction1-0, junction0-0)
end_variable
begin_variable
var39
-1
2
Atom road_connect(road0, junction1-0, junction0-1)
NegatedAtom road_connect(road0, junction1-0, junction0-1)
end_variable
begin_variable
var40
-1
2
Atom road_connect(road0, junction1-0, junction1-1)
NegatedAtom road_connect(road0, junction1-0, junction1-1)
end_variable
begin_variable
var41
-1
2
Atom road_connect(road0, junction1-1, junction0-0)
NegatedAtom road_connect(road0, junction1-1, junction0-0)
end_variable
begin_variable
var42
-1
2
Atom road_connect(road0, junction1-1, junction0-1)
NegatedAtom road_connect(road0, junction1-1, junction0-1)
end_variable
begin_variable
var43
-1
2
Atom road_connect(road0, junction1-1, junction1-0)
NegatedAtom road_connect(road0, junction1-1, junction1-0)
end_variable
begin_variable
var44
-1
2
Atom road_connect(road1, junction0-0, junction0-1)
NegatedAtom road_connect(road1, junction0-0, junction0-1)
end_variable
begin_variable
var45
-1
2
Atom road_connect(road1, junction0-0, junction1-0)
NegatedAtom road_connect(road1, junction0-0, junction1-0)
end_variable
begin_variable
var46
-1
2
Atom road_connect(road1, junction0-0, junction1-1)
NegatedAtom road_connect(road1, junction0-0, junction1-1)
end_variable
begin_variable
var47
-1
2
Atom road_connect(road1, junction0-1, junction0-0)
NegatedAtom road_connect(road1, junction0-1, junction0-0)
end_variable
begin_variable
var48
-1
2
Atom road_connect(road1, junction0-1, junction1-0)
NegatedAtom road_connect(road1, junction0-1, junction1-0)
end_variable
begin_variable
var49
-1
2
Atom road_connect(road1, junction0-1, junction1-1)
NegatedAtom road_connect(road1, junction0-1, junction1-1)
end_variable
begin_variable
var50
-1
2
Atom road_connect(road1, junction1-0, junction0-0)
NegatedAtom road_connect(road1, junction1-0, junction0-0)
end_variable
begin_variable
var51
-1
2
Atom road_connect(road1, junction1-0, junction0-1)
NegatedAtom road_connect(road1, junction1-0, junction0-1)
end_variable
begin_variable
var52
-1
2
Atom road_connect(road1, junction1-0, junction1-1)
NegatedAtom road_connect(road1, junction1-0, junction1-1)
end_variable
begin_variable
var53
-1
2
Atom road_connect(road1, junction1-1, junction0-0)
NegatedAtom road_connect(road1, junction1-1, junction0-0)
end_variable
begin_variable
var54
-1
2
Atom road_connect(road1, junction1-1, junction0-1)
NegatedAtom road_connect(road1, junction1-1, junction0-1)
end_variable
begin_variable
var55
-1
2
Atom road_connect(road1, junction1-1, junction1-0)
NegatedAtom road_connect(road1, junction1-1, junction1-0)
end_variable
begin_variable
var56
-1
2
Atom road_connect(road2, junction0-0, junction0-1)
NegatedAtom road_connect(road2, junction0-0, junction0-1)
end_variable
begin_variable
var57
-1
2
Atom road_connect(road2, junction0-0, junction1-0)
NegatedAtom road_connect(road2, junction0-0, junction1-0)
end_variable
begin_variable
var58
-1
2
Atom road_connect(road2, junction0-0, junction1-1)
NegatedAtom road_connect(road2, junction0-0, junction1-1)
end_variable
begin_variable
var59
-1
2
Atom road_connect(road2, junction0-1, junction0-0)
NegatedAtom road_connect(road2, junction0-1, junction0-0)
end_variable
begin_variable
var60
-1
2
Atom road_connect(road2, junction0-1, junction1-0)
NegatedAtom road_connect(road2, junction0-1, junction1-0)
end_variable
begin_variable
var61
-1
2
Atom road_connect(road2, junction0-1, junction1-1)
NegatedAtom road_connect(road2, junction0-1, junction1-1)
end_variable
begin_variable
var62
-1
2
Atom road_connect(road2, junction1-0, junction0-0)
NegatedAtom road_connect(road2, junction1-0, junction0-0)
end_variable
begin_variable
var63
-1
2
Atom road_connect(road2, junction1-0, junction0-1)
NegatedAtom road_connect(road2, junction1-0, junction0-1)
end_variable
begin_variable
var64
-1
2
Atom road_connect(road2, junction1-0, junction1-1)
NegatedAtom road_connect(road2, junction1-0, junction1-1)
end_variable
begin_variable
var65
-1
2
Atom road_connect(road2, junction1-1, junction0-0)
NegatedAtom road_connect(road2, junction1-1, junction0-0)
end_variable
begin_variable
var66
-1
2
Atom road_connect(road2, junction1-1, junction0-1)
NegatedAtom road_connect(road2, junction1-1, junction0-1)
end_variable
begin_variable
var67
-1
2
Atom road_connect(road2, junction1-1, junction1-0)
NegatedAtom road_connect(road2, junction1-1, junction1-0)
end_variable
begin_variable
var68
-1
2
Atom road_connect(road3, junction0-0, junction0-1)
NegatedAtom road_connect(road3, junction0-0, junction0-1)
end_variable
begin_variable
var69
-1
2
Atom road_connect(road3, junction0-0, junction1-0)
NegatedAtom road_connect(road3, junction0-0, junction1-0)
end_variable
begin_variable
var70
-1
2
Atom road_connect(road3, junction0-0, junction1-1)
NegatedAtom road_connect(road3, junction0-0, junction1-1)
end_variable
begin_variable
var71
-1
2
Atom road_connect(road3, junction0-1, junction0-0)
NegatedAtom road_connect(road3, junction0-1, junction0-0)
end_variable
begin_variable
var72
-1
2
Atom road_connect(road3, junction0-1, junction1-0)
NegatedAtom road_connect(road3, junction0-1, junction1-0)
end_variable
begin_variable
var73
-1
2
Atom road_connect(road3, junction0-1, junction1-1)
NegatedAtom road_connect(road3, junction0-1, junction1-1)
end_variable
begin_variable
var74
-1
2
Atom road_connect(road3, junction1-0, junction0-0)
NegatedAtom road_connect(road3, junction1-0, junction0-0)
end_variable
begin_variable
var75
-1
2
Atom road_connect(road3, junction1-0, junction0-1)
NegatedAtom road_connect(road3, junction1-0, junction0-1)
end_variable
begin_variable
var76
-1
2
Atom road_connect(road3, junction1-0, junction1-1)
NegatedAtom road_connect(road3, junction1-0, junction1-1)
end_variable
begin_variable
var77
-1
2
Atom road_connect(road3, junction1-1, junction0-0)
NegatedAtom road_connect(road3, junction1-1, junction0-0)
end_variable
begin_variable
var78
-1
2
Atom road_connect(road3, junction1-1, junction0-1)
NegatedAtom road_connect(road3, junction1-1, junction0-1)
end_variable
begin_variable
var79
-1
2
Atom road_connect(road3, junction1-1, junction1-0)
NegatedAtom road_connect(road3, junction1-1, junction1-0)
end_variable
begin_variable
var80
-1
2
Atom starting(car0, garage0)
NegatedAtom starting(car0, garage0)
end_variable
begin_variable
var81
-1
2
Atom starting(car1, garage0)
NegatedAtom starting(car1, garage0)
end_variable
0
begin_state
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
0
0
0
0
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
1
0
0
end_state
begin_goal
2
3 0
6 0
end_goal
298
begin_operator
build_diagonal_oneway junction0-0 junction1-1 road0
1
27 0
2
0 28 1 0
0 34 -1 0
30
end_operator
begin_operator
build_diagonal_oneway junction0-0 junction1-1 road1
1
27 0
2
0 29 1 0
0 46 -1 0
30
end_operator
begin_operator
build_diagonal_oneway junction0-0 junction1-1 road2
1
27 0
2
0 30 1 0
0 58 -1 0
30
end_operator
begin_operator
build_diagonal_oneway junction0-0 junction1-1 road3
1
27 0
2
0 31 1 0
0 70 -1 0
30
end_operator
begin_operator
build_diagonal_oneway junction0-1 junction1-0 road0
1
26 0
2
0 28 1 0
0 36 -1 0
30
end_operator
begin_operator
build_diagonal_oneway junction0-1 junction1-0 road1
1
26 0
2
0 29 1 0
0 48 -1 0
30
end_operator
begin_operator
build_diagonal_oneway junction0-1 junction1-0 road2
1
26 0
2
0 30 1 0
0 60 -1 0
30
end_operator
begin_operator
build_diagonal_oneway junction0-1 junction1-0 road3
1
26 0
2
0 31 1 0
0 72 -1 0
30
end_operator
begin_operator
build_diagonal_oneway junction1-0 junction0-1 road0
1
25 0
2
0 28 1 0
0 39 -1 0
30
end_operator
begin_operator
build_diagonal_oneway junction1-0 junction0-1 road1
1
25 0
2
0 29 1 0
0 51 -1 0
30
end_operator
begin_operator
build_diagonal_oneway junction1-0 junction0-1 road2
1
25 0
2
0 30 1 0
0 63 -1 0
30
end_operator
begin_operator
build_diagonal_oneway junction1-0 junction0-1 road3
1
25 0
2
0 31 1 0
0 75 -1 0
30
end_operator
begin_operator
build_diagonal_oneway junction1-1 junction0-0 road0
1
24 0
2
0 28 1 0
0 41 -1 0
30
end_operator
begin_operator
build_diagonal_oneway junction1-1 junction0-0 road1
1
24 0
2
0 29 1 0
0 53 -1 0
30
end_operator
begin_operator
build_diagonal_oneway junction1-1 junction0-0 road2
1
24 0
2
0 30 1 0
0 65 -1 0
30
end_operator
begin_operator
build_diagonal_oneway junction1-1 junction0-0 road3
1
24 0
2
0 31 1 0
0 77 -1 0
30
end_operator
begin_operator
build_straight_oneway junction0-0 junction0-1 road0
1
25 0
2
0 28 1 0
0 32 -1 0
20
end_operator
begin_operator
build_straight_oneway junction0-0 junction0-1 road1
1
25 0
2
0 29 1 0
0 44 -1 0
20
end_operator
begin_operator
build_straight_oneway junction0-0 junction0-1 road2
1
25 0
2
0 30 1 0
0 56 -1 0
20
end_operator
begin_operator
build_straight_oneway junction0-0 junction0-1 road3
1
25 0
2
0 31 1 0
0 68 -1 0
20
end_operator
begin_operator
build_straight_oneway junction0-0 junction1-0 road0
1
26 0
2
0 28 1 0
0 33 -1 0
20
end_operator
begin_operator
build_straight_oneway junction0-0 junction1-0 road1
1
26 0
2
0 29 1 0
0 45 -1 0
20
end_operator
begin_operator
build_straight_oneway junction0-0 junction1-0 road2
1
26 0
2
0 30 1 0
0 57 -1 0
20
end_operator
begin_operator
build_straight_oneway junction0-0 junction1-0 road3
1
26 0
2
0 31 1 0
0 69 -1 0
20
end_operator
begin_operator
build_straight_oneway junction0-1 junction0-0 road0
1
24 0
2
0 28 1 0
0 35 -1 0
20
end_operator
begin_operator
build_straight_oneway junction0-1 junction0-0 road1
1
24 0
2
0 29 1 0
0 47 -1 0
20
end_operator
begin_operator
build_straight_oneway junction0-1 junction0-0 road2
1
24 0
2
0 30 1 0
0 59 -1 0
20
end_operator
begin_operator
build_straight_oneway junction0-1 junction0-0 road3
1
24 0
2
0 31 1 0
0 71 -1 0
20
end_operator
begin_operator
build_straight_oneway junction0-1 junction1-1 road0
1
27 0
2
0 28 1 0
0 37 -1 0
20
end_operator
begin_operator
build_straight_oneway junction0-1 junction1-1 road1
1
27 0
2
0 29 1 0
0 49 -1 0
20
end_operator
begin_operator
build_straight_oneway junction0-1 junction1-1 road2
1
27 0
2
0 30 1 0
0 61 -1 0
20
end_operator
begin_operator
build_straight_oneway junction0-1 junction1-1 road3
1
27 0
2
0 31 1 0
0 73 -1 0
20
end_operator
begin_operator
build_straight_oneway junction1-0 junction0-0 road0
1
24 0
2
0 28 1 0
0 38 -1 0
20
end_operator
begin_operator
build_straight_oneway junction1-0 junction0-0 road1
1
24 0
2
0 29 1 0
0 50 -1 0
20
end_operator
begin_operator
build_straight_oneway junction1-0 junction0-0 road2
1
24 0
2
0 30 1 0
0 62 -1 0
20
end_operator
begin_operator
build_straight_oneway junction1-0 junction0-0 road3
1
24 0
2
0 31 1 0
0 74 -1 0
20
end_operator
begin_operator
build_straight_oneway junction1-0 junction1-1 road0
1
27 0
2
0 28 1 0
0 40 -1 0
20
end_operator
begin_operator
build_straight_oneway junction1-0 junction1-1 road1
1
27 0
2
0 29 1 0
0 52 -1 0
20
end_operator
begin_operator
build_straight_oneway junction1-0 junction1-1 road2
1
27 0
2
0 30 1 0
0 64 -1 0
20
end_operator
begin_operator
build_straight_oneway junction1-0 junction1-1 road3
1
27 0
2
0 31 1 0
0 76 -1 0
20
end_operator
begin_operator
build_straight_oneway junction1-1 junction0-1 road0
1
25 0
2
0 28 1 0
0 42 -1 0
20
end_operator
begin_operator
build_straight_oneway junction1-1 junction0-1 road1
1
25 0
2
0 29 1 0
0 54 -1 0
20
end_operator
begin_operator
build_straight_oneway junction1-1 junction0-1 road2
1
25 0
2
0 30 1 0
0 66 -1 0
20
end_operator
begin_operator
build_straight_oneway junction1-1 junction0-1 road3
1
25 0
2
0 31 1 0
0 78 -1 0
20
end_operator
begin_operator
build_straight_oneway junction1-1 junction1-0 road0
1
26 0
2
0 28 1 0
0 43 -1 0
20
end_operator
begin_operator
build_straight_oneway junction1-1 junction1-0 road1
1
26 0
2
0 29 1 0
0 55 -1 0
20
end_operator
begin_operator
build_straight_oneway junction1-1 junction1-0 road2
1
26 0
2
0 30 1 0
0 67 -1 0
20
end_operator
begin_operator
build_straight_oneway junction1-1 junction1-0 road3
1
26 0
2
0 31 1 0
0 79 -1 0
20
end_operator
begin_operator
car_arrived junction0-0 car0
0
3
0 0 -1 0
0 8 0 1
0 24 -1 0
0
end_operator
begin_operator
car_arrived junction0-0 car1
0
3
0 4 -1 0
0 12 0 1
0 24 -1 0
0
end_operator
begin_operator
car_arrived junction0-1 car0
0
3
0 1 -1 0
0 9 0 1
0 25 -1 0
0
end_operator
begin_operator
car_arrived junction0-1 car1
0
3
0 5 -1 0
0 13 0 1
0 25 -1 0
0
end_operator
begin_operator
car_arrived junction1-0 car0
0
3
0 2 -1 0
0 10 0 1
0 26 -1 0
0
end_operator
begin_operator
car_arrived junction1-0 car1
0
3
0 6 -1 0
0 14 0 1
0 26 -1 0
0
end_operator
begin_operator
car_arrived junction1-1 car0
0
3
0 3 -1 0
0 11 0 1
0 27 -1 0
0
end_operator
begin_operator
car_arrived junction1-1 car1
0
3
0 7 -1 0
0 15 0 1
0 27 -1 0
0
end_operator
begin_operator
car_start junction0-1 car0 garage0
0
3
0 9 -1 0
0 25 0 1
0 80 0 1
0
end_operator
begin_operator
car_start junction0-1 car1 garage0
0
3
0 13 -1 0
0 25 0 1
0 81 0 1
0
end_operator
begin_operator
destroy_road junction0-0 junction0-1 road0
0
6
1 16 0 8 -1 0
1 20 0 12 -1 0
0 16 -1 1
0 20 -1 1
0 28 0 1
0 32 0 1
10
end_operator
begin_operator
destroy_road junction0-0 junction0-1 road1
0
6
1 17 0 8 -1 0
1 21 0 12 -1 0
0 17 -1 1
0 21 -1 1
0 29 0 1
0 44 0 1
10
end_operator
begin_operator
destroy_road junction0-0 junction0-1 road2
0
6
1 18 0 8 -1 0
1 22 0 12 -1 0
0 18 -1 1
0 22 -1 1
0 30 0 1
0 56 0 1
10
end_operator
begin_operator
destroy_road junction0-0 junction0-1 road3
0
6
1 19 0 8 -1 0
1 23 0 12 -1 0
0 19 -1 1
0 23 -1 1
0 31 0 1
0 68 0 1
10
end_operator
begin_operator
destroy_road junction0-0 junction1-0 road0
0
6
1 16 0 8 -1 0
1 20 0 12 -1 0
0 16 -1 1
0 20 -1 1
0 28 0 1
0 33 0 1
10
end_operator
begin_operator
destroy_road junction0-0 junction1-0 road1
0
6
1 17 0 8 -1 0
1 21 0 12 -1 0
0 17 -1 1
0 21 -1 1
0 29 0 1
0 45 0 1
10
end_operator
begin_operator
destroy_road junction0-0 junction1-0 road2
0
6
1 18 0 8 -1 0
1 22 0 12 -1 0
0 18 -1 1
0 22 -1 1
0 30 0 1
0 57 0 1
10
end_operator
begin_operator
destroy_road junction0-0 junction1-0 road3
0
6
1 19 0 8 -1 0
1 23 0 12 -1 0
0 19 -1 1
0 23 -1 1
0 31 0 1
0 69 0 1
10
end_operator
begin_operator
destroy_road junction0-0 junction1-1 road0
0
6
1 16 0 8 -1 0
1 20 0 12 -1 0
0 16 -1 1
0 20 -1 1
0 28 0 1
0 34 0 1
10
end_operator
begin_operator
destroy_road junction0-0 junction1-1 road1
0
6
1 17 0 8 -1 0
1 21 0 12 -1 0
0 17 -1 1
0 21 -1 1
0 29 0 1
0 46 0 1
10
end_operator
begin_operator
destroy_road junction0-0 junction1-1 road2
0
6
1 18 0 8 -1 0
1 22 0 12 -1 0
0 18 -1 1
0 22 -1 1
0 30 0 1
0 58 0 1
10
end_operator
begin_operator
destroy_road junction0-0 junction1-1 road3
0
6
1 19 0 8 -1 0
1 23 0 12 -1 0
0 19 -1 1
0 23 -1 1
0 31 0 1
0 70 0 1
10
end_operator
begin_operator
destroy_road junction0-1 junction0-0 road0
0
6
1 16 0 9 -1 0
1 20 0 13 -1 0
0 16 -1 1
0 20 -1 1
0 28 0 1
0 35 0 1
10
end_operator
begin_operator
destroy_road junction0-1 junction0-0 road1
0
6
1 17 0 9 -1 0
1 21 0 13 -1 0
0 17 -1 1
0 21 -1 1
0 29 0 1
0 47 0 1
10
end_operator
begin_operator
destroy_road junction0-1 junction0-0 road2
0
6
1 18 0 9 -1 0
1 22 0 13 -1 0
0 18 -1 1
0 22 -1 1
0 30 0 1
0 59 0 1
10
end_operator
begin_operator
destroy_road junction0-1 junction0-0 road3
0
6
1 19 0 9 -1 0
1 23 0 13 -1 0
0 19 -1 1
0 23 -1 1
0 31 0 1
0 71 0 1
10
end_operator
begin_operator
destroy_road junction0-1 junction1-0 road0
0
6
1 16 0 9 -1 0
1 20 0 13 -1 0
0 16 -1 1
0 20 -1 1
0 28 0 1
0 36 0 1
10
end_operator
begin_operator
destroy_road junction0-1 junction1-0 road1
0
6
1 17 0 9 -1 0
1 21 0 13 -1 0
0 17 -1 1
0 21 -1 1
0 29 0 1
0 48 0 1
10
end_operator
begin_operator
destroy_road junction0-1 junction1-0 road2
0
6
1 18 0 9 -1 0
1 22 0 13 -1 0
0 18 -1 1
0 22 -1 1
0 30 0 1
0 60 0 1
10
end_operator
begin_operator
destroy_road junction0-1 junction1-0 road3
0
6
1 19 0 9 -1 0
1 23 0 13 -1 0
0 19 -1 1
0 23 -1 1
0 31 0 1
0 72 0 1
10
end_operator
begin_operator
destroy_road junction0-1 junction1-1 road0
0
6
1 16 0 9 -1 0
1 20 0 13 -1 0
0 16 -1 1
0 20 -1 1
0 28 0 1
0 37 0 1
10
end_operator
begin_operator
destroy_road junction0-1 junction1-1 road1
0
6
1 17 0 9 -1 0
1 21 0 13 -1 0
0 17 -1 1
0 21 -1 1
0 29 0 1
0 49 0 1
10
end_operator
begin_operator
destroy_road junction0-1 junction1-1 road2
0
6
1 18 0 9 -1 0
1 22 0 13 -1 0
0 18 -1 1
0 22 -1 1
0 30 0 1
0 61 0 1
10
end_operator
begin_operator
destroy_road junction0-1 junction1-1 road3
0
6
1 19 0 9 -1 0
1 23 0 13 -1 0
0 19 -1 1
0 23 -1 1
0 31 0 1
0 73 0 1
10
end_operator
begin_operator
destroy_road junction1-0 junction0-0 road0
0
6
1 16 0 10 -1 0
1 20 0 14 -1 0
0 16 -1 1
0 20 -1 1
0 28 0 1
0 38 0 1
10
end_operator
begin_operator
destroy_road junction1-0 junction0-0 road1
0
6
1 17 0 10 -1 0
1 21 0 14 -1 0
0 17 -1 1
0 21 -1 1
0 29 0 1
0 50 0 1
10
end_operator
begin_operator
destroy_road junction1-0 junction0-0 road2
0
6
1 18 0 10 -1 0
1 22 0 14 -1 0
0 18 -1 1
0 22 -1 1
0 30 0 1
0 62 0 1
10
end_operator
begin_operator
destroy_road junction1-0 junction0-0 road3
0
6
1 19 0 10 -1 0
1 23 0 14 -1 0
0 19 -1 1
0 23 -1 1
0 31 0 1
0 74 0 1
10
end_operator
begin_operator
destroy_road junction1-0 junction0-1 road0
0
6
1 16 0 10 -1 0
1 20 0 14 -1 0
0 16 -1 1
0 20 -1 1
0 28 0 1
0 39 0 1
10
end_operator
begin_operator
destroy_road junction1-0 junction0-1 road1
0
6
1 17 0 10 -1 0
1 21 0 14 -1 0
0 17 -1 1
0 21 -1 1
0 29 0 1
0 51 0 1
10
end_operator
begin_operator
destroy_road junction1-0 junction0-1 road2
0
6
1 18 0 10 -1 0
1 22 0 14 -1 0
0 18 -1 1
0 22 -1 1
0 30 0 1
0 63 0 1
10
end_operator
begin_operator
destroy_road junction1-0 junction0-1 road3
0
6
1 19 0 10 -1 0
1 23 0 14 -1 0
0 19 -1 1
0 23 -1 1
0 31 0 1
0 75 0 1
10
end_operator
begin_operator
destroy_road junction1-0 junction1-1 road0
0
6
1 16 0 10 -1 0
1 20 0 14 -1 0
0 16 -1 1
0 20 -1 1
0 28 0 1
0 40 0 1
10
end_operator
begin_operator
destroy_road junction1-0 junction1-1 road1
0
6
1 17 0 10 -1 0
1 21 0 14 -1 0
0 17 -1 1
0 21 -1 1
0 29 0 1
0 52 0 1
10
end_operator
begin_operator
destroy_road junction1-0 junction1-1 road2
0
6
1 18 0 10 -1 0
1 22 0 14 -1 0
0 18 -1 1
0 22 -1 1
0 30 0 1
0 64 0 1
10
end_operator
begin_operator
destroy_road junction1-0 junction1-1 road3
0
6
1 19 0 10 -1 0
1 23 0 14 -1 0
0 19 -1 1
0 23 -1 1
0 31 0 1
0 76 0 1
10
end_operator
begin_operator
destroy_road junction1-1 junction0-0 road0
0
6
1 16 0 11 -1 0
1 20 0 15 -1 0
0 16 -1 1
0 20 -1 1
0 28 0 1
0 41 0 1
10
end_operator
begin_operator
destroy_road junction1-1 junction0-0 road1
0
6
1 17 0 11 -1 0
1 21 0 15 -1 0
0 17 -1 1
0 21 -1 1
0 29 0 1
0 53 0 1
10
end_operator
begin_operator
destroy_road junction1-1 junction0-0 road2
0
6
1 18 0 11 -1 0
1 22 0 15 -1 0
0 18 -1 1
0 22 -1 1
0 30 0 1
0 65 0 1
10
end_operator
begin_operator
destroy_road junction1-1 junction0-0 road3
0
6
1 19 0 11 -1 0
1 23 0 15 -1 0
0 19 -1 1
0 23 -1 1
0 31 0 1
0 77 0 1
10
end_operator
begin_operator
destroy_road junction1-1 junction0-1 road0
0
6
1 16 0 11 -1 0
1 20 0 15 -1 0
0 16 -1 1
0 20 -1 1
0 28 0 1
0 42 0 1
10
end_operator
begin_operator
destroy_road junction1-1 junction0-1 road1
0
6
1 17 0 11 -1 0
1 21 0 15 -1 0
0 17 -1 1
0 21 -1 1
0 29 0 1
0 54 0 1
10
end_operator
begin_operator
destroy_road junction1-1 junction0-1 road2
0
6
1 18 0 11 -1 0
1 22 0 15 -1 0
0 18 -1 1
0 22 -1 1
0 30 0 1
0 66 0 1
10
end_operator
begin_operator
destroy_road junction1-1 junction0-1 road3
0
6
1 19 0 11 -1 0
1 23 0 15 -1 0
0 19 -1 1
0 23 -1 1
0 31 0 1
0 78 0 1
10
end_operator
begin_operator
destroy_road junction1-1 junction1-0 road0
0
6
1 16 0 11 -1 0
1 20 0 15 -1 0
0 16 -1 1
0 20 -1 1
0 28 0 1
0 43 0 1
10
end_operator
begin_operator
destroy_road junction1-1 junction1-0 road1
0
6
1 17 0 11 -1 0
1 21 0 15 -1 0
0 17 -1 1
0 21 -1 1
0 29 0 1
0 55 0 1
10
end_operator
begin_operator
destroy_road junction1-1 junction1-0 road2
0
6
1 18 0 11 -1 0
1 22 0 15 -1 0
0 18 -1 1
0 22 -1 1
0 30 0 1
0 67 0 1
10
end_operator
begin_operator
destroy_road junction1-1 junction1-0 road3
0
6
1 19 0 11 -1 0
1 23 0 15 -1 0
0 19 -1 1
0 23 -1 1
0 31 0 1
0 79 0 1
10
end_operator
begin_operator
move_car_in_road junction0-0 junction0-1 car0 road0
2
28 0
32 0
3
0 8 0 1
0 16 -1 0
0 24 -1 0
1
end_operator
begin_operator
move_car_in_road junction0-0 junction0-1 car0 road1
2
29 0
44 0
3
0 8 0 1
0 17 -1 0
0 24 -1 0
1
end_operator
begin_operator
move_car_in_road junction0-0 junction0-1 car0 road2
2
30 0
56 0
3
0 8 0 1
0 18 -1 0
0 24 -1 0
1
end_operator
begin_operator
move_car_in_road junction0-0 junction0-1 car0 road3
2
31 0
68 0
3
0 8 0 1
0 19 -1 0
0 24 -1 0
1
end_operator
begin_operator
move_car_in_road junction0-0 junction0-1 car1 road0
2
28 0
32 0
3
0 12 0 1
0 20 -1 0
0 24 -1 0
1
end_operator
begin_operator
move_car_in_road junction0-0 junction0-1 car1 road1
2
29 0
44 0
3
0 12 0 1
0 21 -1 0
0 24 -1 0
1
end_operator
begin_operator
move_car_in_road junction0-0 junction0-1 car1 road2
2
30 0
56 0
3
0 12 0 1
0 22 -1 0
0 24 -1 0
1
end_operator
begin_operator
move_car_in_road junction0-0 junction0-1 car1 road3
2
31 0
68 0
3
0 12 0 1
0 23 -1 0
0 24 -1 0
1
end_operator
begin_operator
move_car_in_road junction0-0 junction1-0 car0 road0
2
28 0
33 0
3
0 8 0 1
0 16 -1 0
0 24 -1 0
1
end_operator
begin_operator
move_car_in_road junction0-0 junction1-0 car0 road1
2
29 0
45 0
3
0 8 0 1
0 17 -1 0
0 24 -1 0
1
end_operator
begin_operator
move_car_in_road junction0-0 junction1-0 car0 road2
2
30 0
57 0
3
0 8 0 1
0 18 -1 0
0 24 -1 0
1
end_operator
begin_operator
move_car_in_road junction0-0 junction1-0 car0 road3
2
31 0
69 0
3
0 8 0 1
0 19 -1 0
0 24 -1 0
1
end_operator
begin_operator
move_car_in_road junction0-0 junction1-0 car1 road0
2
28 0
33 0
3
0 12 0 1
0 20 -1 0
0 24 -1 0
1
end_operator
begin_operator
move_car_in_road junction0-0 junction1-0 car1 road1
2
29 0
45 0
3
0 12 0 1
0 21 -1 0
0 24 -1 0
1
end_operator
begin_operator
move_car_in_road junction0-0 junction1-0 car1 road2
2
30 0
57 0
3
0 12 0 1
0 22 -1 0
0 24 -1 0
1
end_operator
begin_operator
move_car_in_road junction0-0 junction1-0 car1 road3
2
31 0
69 0
3
0 12 0 1
0 23 -1 0
0 24 -1 0
1
end_operator
begin_operator
move_car_in_road junction0-0 junction1-1 car0 road0
2
28 0
34 0
3
0 8 0 1
0 16 -1 0
0 24 -1 0
1
end_operator
begin_operator
move_car_in_road junction0-0 junction1-1 car0 road1
2
29 0
46 0
3
0 8 0 1
0 17 -1 0
0 24 -1 0
1
end_operator
begin_operator
move_car_in_road junction0-0 junction1-1 car0 road2
2
30 0
58 0
3
0 8 0 1
0 18 -1 0
0 24 -1 0
1
end_operator
begin_operator
move_car_in_road junction0-0 junction1-1 car0 road3
2
31 0
70 0
3
0 8 0 1
0 19 -1 0
0 24 -1 0
1
end_operator
begin_operator
move_car_in_road junction0-0 junction1-1 car1 road0
2
28 0
34 0
3
0 12 0 1
0 20 -1 0
0 24 -1 0
1
end_operator
begin_operator
move_car_in_road junction0-0 junction1-1 car1 road1
2
29 0
46 0
3
0 12 0 1
0 21 -1 0
0 24 -1 0
1
end_operator
begin_operator
move_car_in_road junction0-0 junction1-1 car1 road2
2
30 0
58 0
3
0 12 0 1
0 22 -1 0
0 24 -1 0
1
end_operator
begin_operator
move_car_in_road junction0-0 junction1-1 car1 road3
2
31 0
70 0
3
0 12 0 1
0 23 -1 0
0 24 -1 0
1
end_operator
begin_operator
move_car_in_road junction0-1 junction0-0 car0 road0
2
28 0
35 0
3
0 9 0 1
0 16 -1 0
0 25 -1 0
1
end_operator
begin_operator
move_car_in_road junction0-1 junction0-0 car0 road1
2
29 0
47 0
3
0 9 0 1
0 17 -1 0
0 25 -1 0
1
end_operator
begin_operator
move_car_in_road junction0-1 junction0-0 car0 road2
2
30 0
59 0
3
0 9 0 1
0 18 -1 0
0 25 -1 0
1
end_operator
begin_operator
move_car_in_road junction0-1 junction0-0 car0 road3
2
31 0
71 0
3
0 9 0 1
0 19 -1 0
0 25 -1 0
1
end_operator
begin_operator
move_car_in_road junction0-1 junction0-0 car1 road0
2
28 0
35 0
3
0 13 0 1
0 20 -1 0
0 25 -1 0
1
end_operator
begin_operator
move_car_in_road junction0-1 junction0-0 car1 road1
2
29 0
47 0
3
0 13 0 1
0 21 -1 0
0 25 -1 0
1
end_operator
begin_operator
move_car_in_road junction0-1 junction0-0 car1 road2
2
30 0
59 0
3
0 13 0 1
0 22 -1 0
0 25 -1 0
1
end_operator
begin_operator
move_car_in_road junction0-1 junction0-0 car1 road3
2
31 0
71 0
3
0 13 0 1
0 23 -1 0
0 25 -1 0
1
end_operator
begin_operator
move_car_in_road junction0-1 junction1-0 car0 road0
2
28 0
36 0
3
0 9 0 1
0 16 -1 0
0 25 -1 0
1
end_operator
begin_operator
move_car_in_road junction0-1 junction1-0 car0 road1
2
29 0
48 0
3
0 9 0 1
0 17 -1 0
0 25 -1 0
1
end_operator
begin_operator
move_car_in_road junction0-1 junction1-0 car0 road2
2
30 0
60 0
3
0 9 0 1
0 18 -1 0
0 25 -1 0
1
end_operator
begin_operator
move_car_in_road junction0-1 junction1-0 car0 road3
2
31 0
72 0
3
0 9 0 1
0 19 -1 0
0 25 -1 0
1
end_operator
begin_operator
move_car_in_road junction0-1 junction1-0 car1 road0
2
28 0
36 0
3
0 13 0 1
0 20 -1 0
0 25 -1 0
1
end_operator
begin_operator
move_car_in_road junction0-1 junction1-0 car1 road1
2
29 0
48 0
3
0 13 0 1
0 21 -1 0
0 25 -1 0
1
end_operator
begin_operator
move_car_in_road junction0-1 junction1-0 car1 road2
2
30 0
60 0
3
0 13 0 1
0 22 -1 0
0 25 -1 0
1
end_operator
begin_operator
move_car_in_road junction0-1 junction1-0 car1 road3
2
31 0
72 0
3
0 13 0 1
0 23 -1 0
0 25 -1 0
1
end_operator
begin_operator
move_car_in_road junction0-1 junction1-1 car0 road0
2
28 0
37 0
3
0 9 0 1
0 16 -1 0
0 25 -1 0
1
end_operator
begin_operator
move_car_in_road junction0-1 junction1-1 car0 road1
2
29 0
49 0
3
0 9 0 1
0 17 -1 0
0 25 -1 0
1
end_operator
begin_operator
move_car_in_road junction0-1 junction1-1 car0 road2
2
30 0
61 0
3
0 9 0 1
0 18 -1 0
0 25 -1 0
1
end_operator
begin_operator
move_car_in_road junction0-1 junction1-1 car0 road3
2
31 0
73 0
3
0 9 0 1
0 19 -1 0
0 25 -1 0
1
end_operator
begin_operator
move_car_in_road junction0-1 junction1-1 car1 road0
2
28 0
37 0
3
0 13 0 1
0 20 -1 0
0 25 -1 0
1
end_operator
begin_operator
move_car_in_road junction0-1 junction1-1 car1 road1
2
29 0
49 0
3
0 13 0 1
0 21 -1 0
0 25 -1 0
1
end_operator
begin_operator
move_car_in_road junction0-1 junction1-1 car1 road2
2
30 0
61 0
3
0 13 0 1
0 22 -1 0
0 25 -1 0
1
end_operator
begin_operator
move_car_in_road junction0-1 junction1-1 car1 road3
2
31 0
73 0
3
0 13 0 1
0 23 -1 0
0 25 -1 0
1
end_operator
begin_operator
move_car_in_road junction1-0 junction0-0 car0 road0
2
28 0
38 0
3
0 10 0 1
0 16 -1 0
0 26 -1 0
1
end_operator
begin_operator
move_car_in_road junction1-0 junction0-0 car0 road1
2
29 0
50 0
3
0 10 0 1
0 17 -1 0
0 26 -1 0
1
end_operator
begin_operator
move_car_in_road junction1-0 junction0-0 car0 road2
2
30 0
62 0
3
0 10 0 1
0 18 -1 0
0 26 -1 0
1
end_operator
begin_operator
move_car_in_road junction1-0 junction0-0 car0 road3
2
31 0
74 0
3
0 10 0 1
0 19 -1 0
0 26 -1 0
1
end_operator
begin_operator
move_car_in_road junction1-0 junction0-0 car1 road0
2
28 0
38 0
3
0 14 0 1
0 20 -1 0
0 26 -1 0
1
end_operator
begin_operator
move_car_in_road junction1-0 junction0-0 car1 road1
2
29 0
50 0
3
0 14 0 1
0 21 -1 0
0 26 -1 0
1
end_operator
begin_operator
move_car_in_road junction1-0 junction0-0 car1 road2
2
30 0
62 0
3
0 14 0 1
0 22 -1 0
0 26 -1 0
1
end_operator
begin_operator
move_car_in_road junction1-0 junction0-0 car1 road3
2
31 0
74 0
3
0 14 0 1
0 23 -1 0
0 26 -1 0
1
end_operator
begin_operator
move_car_in_road junction1-0 junction0-1 car0 road0
2
28 0
39 0
3
0 10 0 1
0 16 -1 0
0 26 -1 0
1
end_operator
begin_operator
move_car_in_road junction1-0 junction0-1 car0 road1
2
29 0
51 0
3
0 10 0 1
0 17 -1 0
0 26 -1 0
1
end_operator
begin_operator
move_car_in_road junction1-0 junction0-1 car0 road2
2
30 0
63 0
3
0 10 0 1
0 18 -1 0
0 26 -1 0
1
end_operator
begin_operator
move_car_in_road junction1-0 junction0-1 car0 road3
2
31 0
75 0
3
0 10 0 1
0 19 -1 0
0 26 -1 0
1
end_operator
begin_operator
move_car_in_road junction1-0 junction0-1 car1 road0
2
28 0
39 0
3
0 14 0 1
0 20 -1 0
0 26 -1 0
1
end_operator
begin_operator
move_car_in_road junction1-0 junction0-1 car1 road1
2
29 0
51 0
3
0 14 0 1
0 21 -1 0
0 26 -1 0
1
end_operator
begin_operator
move_car_in_road junction1-0 junction0-1 car1 road2
2
30 0
63 0
3
0 14 0 1
0 22 -1 0
0 26 -1 0
1
end_operator
begin_operator
move_car_in_road junction1-0 junction0-1 car1 road3
2
31 0
75 0
3
0 14 0 1
0 23 -1 0
0 26 -1 0
1
end_operator
begin_operator
move_car_in_road junction1-0 junction1-1 car0 road0
2
28 0
40 0
3
0 10 0 1
0 16 -1 0
0 26 -1 0
1
end_operator
begin_operator
move_car_in_road junction1-0 junction1-1 car0 road1
2
29 0
52 0
3
0 10 0 1
0 17 -1 0
0 26 -1 0
1
end_operator
begin_operator
move_car_in_road junction1-0 junction1-1 car0 road2
2
30 0
64 0
3
0 10 0 1
0 18 -1 0
0 26 -1 0
1
end_operator
begin_operator
move_car_in_road junction1-0 junction1-1 car0 road3
2
31 0
76 0
3
0 10 0 1
0 19 -1 0
0 26 -1 0
1
end_operator
begin_operator
move_car_in_road junction1-0 junction1-1 car1 road0
2
28 0
40 0
3
0 14 0 1
0 20 -1 0
0 26 -1 0
1
end_operator
begin_operator
move_car_in_road junction1-0 junction1-1 car1 road1
2
29 0
52 0
3
0 14 0 1
0 21 -1 0
0 26 -1 0
1
end_operator
begin_operator
move_car_in_road junction1-0 junction1-1 car1 road2
2
30 0
64 0
3
0 14 0 1
0 22 -1 0
0 26 -1 0
1
end_operator
begin_operator
move_car_in_road junction1-0 junction1-1 car1 road3
2
31 0
76 0
3
0 14 0 1
0 23 -1 0
0 26 -1 0
1
end_operator
begin_operator
move_car_in_road junction1-1 junction0-0 car0 road0
2
28 0
41 0
3
0 11 0 1
0 16 -1 0
0 27 -1 0
1
end_operator
begin_operator
move_car_in_road junction1-1 junction0-0 car0 road1
2
29 0
53 0
3
0 11 0 1
0 17 -1 0
0 27 -1 0
1
end_operator
begin_operator
move_car_in_road junction1-1 junction0-0 car0 road2
2
30 0
65 0
3
0 11 0 1
0 18 -1 0
0 27 -1 0
1
end_operator
begin_operator
move_car_in_road junction1-1 junction0-0 car0 road3
2
31 0
77 0
3
0 11 0 1
0 19 -1 0
0 27 -1 0
1
end_operator
begin_operator
move_car_in_road junction1-1 junction0-0 car1 road0
2
28 0
41 0
3
0 15 0 1
0 20 -1 0
0 27 -1 0
1
end_operator
begin_operator
move_car_in_road junction1-1 junction0-0 car1 road1
2
29 0
53 0
3
0 15 0 1
0 21 -1 0
0 27 -1 0
1
end_operator
begin_operator
move_car_in_road junction1-1 junction0-0 car1 road2
2
30 0
65 0
3
0 15 0 1
0 22 -1 0
0 27 -1 0
1
end_operator
begin_operator
move_car_in_road junction1-1 junction0-0 car1 road3
2
31 0
77 0
3
0 15 0 1
0 23 -1 0
0 27 -1 0
1
end_operator
begin_operator
move_car_in_road junction1-1 junction0-1 car0 road0
2
28 0
42 0
3
0 11 0 1
0 16 -1 0
0 27 -1 0
1
end_operator
begin_operator
move_car_in_road junction1-1 junction0-1 car0 road1
2
29 0
54 0
3
0 11 0 1
0 17 -1 0
0 27 -1 0
1
end_operator
begin_operator
move_car_in_road junction1-1 junction0-1 car0 road2
2
30 0
66 0
3
0 11 0 1
0 18 -1 0
0 27 -1 0
1
end_operator
begin_operator
move_car_in_road junction1-1 junction0-1 car0 road3
2
31 0
78 0
3
0 11 0 1
0 19 -1 0
0 27 -1 0
1
end_operator
begin_operator
move_car_in_road junction1-1 junction0-1 car1 road0
2
28 0
42 0
3
0 15 0 1
0 20 -1 0
0 27 -1 0
1
end_operator
begin_operator
move_car_in_road junction1-1 junction0-1 car1 road1
2
29 0
54 0
3
0 15 0 1
0 21 -1 0
0 27 -1 0
1
end_operator
begin_operator
move_car_in_road junction1-1 junction0-1 car1 road2
2
30 0
66 0
3
0 15 0 1
0 22 -1 0
0 27 -1 0
1
end_operator
begin_operator
move_car_in_road junction1-1 junction0-1 car1 road3
2
31 0
78 0
3
0 15 0 1
0 23 -1 0
0 27 -1 0
1
end_operator
begin_operator
move_car_in_road junction1-1 junction1-0 car0 road0
2
28 0
43 0
3
0 11 0 1
0 16 -1 0
0 27 -1 0
1
end_operator
begin_operator
move_car_in_road junction1-1 junction1-0 car0 road1
2
29 0
55 0
3
0 11 0 1
0 17 -1 0
0 27 -1 0
1
end_operator
begin_operator
move_car_in_road junction1-1 junction1-0 car0 road2
2
30 0
67 0
3
0 11 0 1
0 18 -1 0
0 27 -1 0
1
end_operator
begin_operator
move_car_in_road junction1-1 junction1-0 car0 road3
2
31 0
79 0
3
0 11 0 1
0 19 -1 0
0 27 -1 0
1
end_operator
begin_operator
move_car_in_road junction1-1 junction1-0 car1 road0
2
28 0
43 0
3
0 15 0 1
0 20 -1 0
0 27 -1 0
1
end_operator
begin_operator
move_car_in_road junction1-1 junction1-0 car1 road1
2
29 0
55 0
3
0 15 0 1
0 21 -1 0
0 27 -1 0
1
end_operator
begin_operator
move_car_in_road junction1-1 junction1-0 car1 road2
2
30 0
67 0
3
0 15 0 1
0 22 -1 0
0 27 -1 0
1
end_operator
begin_operator
move_car_in_road junction1-1 junction1-0 car1 road3
2
31 0
79 0
3
0 15 0 1
0 23 -1 0
0 27 -1 0
1
end_operator
begin_operator
move_car_out_road junction0-0 junction0-1 car0 road0
2
28 0
32 0
3
0 9 -1 0
0 16 0 1
0 25 0 1
1
end_operator
begin_operator
move_car_out_road junction0-0 junction0-1 car0 road1
2
29 0
44 0
3
0 9 -1 0
0 17 0 1
0 25 0 1
1
end_operator
begin_operator
move_car_out_road junction0-0 junction0-1 car0 road2
2
30 0
56 0
3
0 9 -1 0
0 18 0 1
0 25 0 1
1
end_operator
begin_operator
move_car_out_road junction0-0 junction0-1 car0 road3
2
31 0
68 0
3
0 9 -1 0
0 19 0 1
0 25 0 1
1
end_operator
begin_operator
move_car_out_road junction0-0 junction0-1 car1 road0
2
28 0
32 0
3
0 13 -1 0
0 20 0 1
0 25 0 1
1
end_operator
begin_operator
move_car_out_road junction0-0 junction0-1 car1 road1
2
29 0
44 0
3
0 13 -1 0
0 21 0 1
0 25 0 1
1
end_operator
begin_operator
move_car_out_road junction0-0 junction0-1 car1 road2
2
30 0
56 0
3
0 13 -1 0
0 22 0 1
0 25 0 1
1
end_operator
begin_operator
move_car_out_road junction0-0 junction0-1 car1 road3
2
31 0
68 0
3
0 13 -1 0
0 23 0 1
0 25 0 1
1
end_operator
begin_operator
move_car_out_road junction0-0 junction1-0 car0 road0
2
28 0
33 0
3
0 10 -1 0
0 16 0 1
0 26 0 1
1
end_operator
begin_operator
move_car_out_road junction0-0 junction1-0 car0 road1
2
29 0
45 0
3
0 10 -1 0
0 17 0 1
0 26 0 1
1
end_operator
begin_operator
move_car_out_road junction0-0 junction1-0 car0 road2
2
30 0
57 0
3
0 10 -1 0
0 18 0 1
0 26 0 1
1
end_operator
begin_operator
move_car_out_road junction0-0 junction1-0 car0 road3
2
31 0
69 0
3
0 10 -1 0
0 19 0 1
0 26 0 1
1
end_operator
begin_operator
move_car_out_road junction0-0 junction1-0 car1 road0
2
28 0
33 0
3
0 14 -1 0
0 20 0 1
0 26 0 1
1
end_operator
begin_operator
move_car_out_road junction0-0 junction1-0 car1 road1
2
29 0
45 0
3
0 14 -1 0
0 21 0 1
0 26 0 1
1
end_operator
begin_operator
move_car_out_road junction0-0 junction1-0 car1 road2
2
30 0
57 0
3
0 14 -1 0
0 22 0 1
0 26 0 1
1
end_operator
begin_operator
move_car_out_road junction0-0 junction1-0 car1 road3
2
31 0
69 0
3
0 14 -1 0
0 23 0 1
0 26 0 1
1
end_operator
begin_operator
move_car_out_road junction0-0 junction1-1 car0 road0
2
28 0
34 0
3
0 11 -1 0
0 16 0 1
0 27 0 1
1
end_operator
begin_operator
move_car_out_road junction0-0 junction1-1 car0 road1
2
29 0
46 0
3
0 11 -1 0
0 17 0 1
0 27 0 1
1
end_operator
begin_operator
move_car_out_road junction0-0 junction1-1 car0 road2
2
30 0
58 0
3
0 11 -1 0
0 18 0 1
0 27 0 1
1
end_operator
begin_operator
move_car_out_road junction0-0 junction1-1 car0 road3
2
31 0
70 0
3
0 11 -1 0
0 19 0 1
0 27 0 1
1
end_operator
begin_operator
move_car_out_road junction0-0 junction1-1 car1 road0
2
28 0
34 0
3
0 15 -1 0
0 20 0 1
0 27 0 1
1
end_operator
begin_operator
move_car_out_road junction0-0 junction1-1 car1 road1
2
29 0
46 0
3
0 15 -1 0
0 21 0 1
0 27 0 1
1
end_operator
begin_operator
move_car_out_road junction0-0 junction1-1 car1 road2
2
30 0
58 0
3
0 15 -1 0
0 22 0 1
0 27 0 1
1
end_operator
begin_operator
move_car_out_road junction0-0 junction1-1 car1 road3
2
31 0
70 0
3
0 15 -1 0
0 23 0 1
0 27 0 1
1
end_operator
begin_operator
move_car_out_road junction0-1 junction0-0 car0 road0
2
28 0
35 0
3
0 8 -1 0
0 16 0 1
0 24 0 1
1
end_operator
begin_operator
move_car_out_road junction0-1 junction0-0 car0 road1
2
29 0
47 0
3
0 8 -1 0
0 17 0 1
0 24 0 1
1
end_operator
begin_operator
move_car_out_road junction0-1 junction0-0 car0 road2
2
30 0
59 0
3
0 8 -1 0
0 18 0 1
0 24 0 1
1
end_operator
begin_operator
move_car_out_road junction0-1 junction0-0 car0 road3
2
31 0
71 0
3
0 8 -1 0
0 19 0 1
0 24 0 1
1
end_operator
begin_operator
move_car_out_road junction0-1 junction0-0 car1 road0
2
28 0
35 0
3
0 12 -1 0
0 20 0 1
0 24 0 1
1
end_operator
begin_operator
move_car_out_road junction0-1 junction0-0 car1 road1
2
29 0
47 0
3
0 12 -1 0
0 21 0 1
0 24 0 1
1
end_operator
begin_operator
move_car_out_road junction0-1 junction0-0 car1 road2
2
30 0
59 0
3
0 12 -1 0
0 22 0 1
0 24 0 1
1
end_operator
begin_operator
move_car_out_road junction0-1 junction0-0 car1 road3
2
31 0
71 0
3
0 12 -1 0
0 23 0 1
0 24 0 1
1
end_operator
begin_operator
move_car_out_road junction0-1 junction1-0 car0 road0
2
28 0
36 0
3
0 10 -1 0
0 16 0 1
0 26 0 1
1
end_operator
begin_operator
move_car_out_road junction0-1 junction1-0 car0 road1
2
29 0
48 0
3
0 10 -1 0
0 17 0 1
0 26 0 1
1
end_operator
begin_operator
move_car_out_road junction0-1 junction1-0 car0 road2
2
30 0
60 0
3
0 10 -1 0
0 18 0 1
0 26 0 1
1
end_operator
begin_operator
move_car_out_road junction0-1 junction1-0 car0 road3
2
31 0
72 0
3
0 10 -1 0
0 19 0 1
0 26 0 1
1
end_operator
begin_operator
move_car_out_road junction0-1 junction1-0 car1 road0
2
28 0
36 0
3
0 14 -1 0
0 20 0 1
0 26 0 1
1
end_operator
begin_operator
move_car_out_road junction0-1 junction1-0 car1 road1
2
29 0
48 0
3
0 14 -1 0
0 21 0 1
0 26 0 1
1
end_operator
begin_operator
move_car_out_road junction0-1 junction1-0 car1 road2
2
30 0
60 0
3
0 14 -1 0
0 22 0 1
0 26 0 1
1
end_operator
begin_operator
move_car_out_road junction0-1 junction1-0 car1 road3
2
31 0
72 0
3
0 14 -1 0
0 23 0 1
0 26 0 1
1
end_operator
begin_operator
move_car_out_road junction0-1 junction1-1 car0 road0
2
28 0
37 0
3
0 11 -1 0
0 16 0 1
0 27 0 1
1
end_operator
begin_operator
move_car_out_road junction0-1 junction1-1 car0 road1
2
29 0
49 0
3
0 11 -1 0
0 17 0 1
0 27 0 1
1
end_operator
begin_operator
move_car_out_road junction0-1 junction1-1 car0 road2
2
30 0
61 0
3
0 11 -1 0
0 18 0 1
0 27 0 1
1
end_operator
begin_operator
move_car_out_road junction0-1 junction1-1 car0 road3
2
31 0
73 0
3
0 11 -1 0
0 19 0 1
0 27 0 1
1
end_operator
begin_operator
move_car_out_road junction0-1 junction1-1 car1 road0
2
28 0
37 0
3
0 15 -1 0
0 20 0 1
0 27 0 1
1
end_operator
begin_operator
move_car_out_road junction0-1 junction1-1 car1 road1
2
29 0
49 0
3
0 15 -1 0
0 21 0 1
0 27 0 1
1
end_operator
begin_operator
move_car_out_road junction0-1 junction1-1 car1 road2
2
30 0
61 0
3
0 15 -1 0
0 22 0 1
0 27 0 1
1
end_operator
begin_operator
move_car_out_road junction0-1 junction1-1 car1 road3
2
31 0
73 0
3
0 15 -1 0
0 23 0 1
0 27 0 1
1
end_operator
begin_operator
move_car_out_road junction1-0 junction0-0 car0 road0
2
28 0
38 0
3
0 8 -1 0
0 16 0 1
0 24 0 1
1
end_operator
begin_operator
move_car_out_road junction1-0 junction0-0 car0 road1
2
29 0
50 0
3
0 8 -1 0
0 17 0 1
0 24 0 1
1
end_operator
begin_operator
move_car_out_road junction1-0 junction0-0 car0 road2
2
30 0
62 0
3
0 8 -1 0
0 18 0 1
0 24 0 1
1
end_operator
begin_operator
move_car_out_road junction1-0 junction0-0 car0 road3
2
31 0
74 0
3
0 8 -1 0
0 19 0 1
0 24 0 1
1
end_operator
begin_operator
move_car_out_road junction1-0 junction0-0 car1 road0
2
28 0
38 0
3
0 12 -1 0
0 20 0 1
0 24 0 1
1
end_operator
begin_operator
move_car_out_road junction1-0 junction0-0 car1 road1
2
29 0
50 0
3
0 12 -1 0
0 21 0 1
0 24 0 1
1
end_operator
begin_operator
move_car_out_road junction1-0 junction0-0 car1 road2
2
30 0
62 0
3
0 12 -1 0
0 22 0 1
0 24 0 1
1
end_operator
begin_operator
move_car_out_road junction1-0 junction0-0 car1 road3
2
31 0
74 0
3
0 12 -1 0
0 23 0 1
0 24 0 1
1
end_operator
begin_operator
move_car_out_road junction1-0 junction0-1 car0 road0
2
28 0
39 0
3
0 9 -1 0
0 16 0 1
0 25 0 1
1
end_operator
begin_operator
move_car_out_road junction1-0 junction0-1 car0 road1
2
29 0
51 0
3
0 9 -1 0
0 17 0 1
0 25 0 1
1
end_operator
begin_operator
move_car_out_road junction1-0 junction0-1 car0 road2
2
30 0
63 0
3
0 9 -1 0
0 18 0 1
0 25 0 1
1
end_operator
begin_operator
move_car_out_road junction1-0 junction0-1 car0 road3
2
31 0
75 0
3
0 9 -1 0
0 19 0 1
0 25 0 1
1
end_operator
begin_operator
move_car_out_road junction1-0 junction0-1 car1 road0
2
28 0
39 0
3
0 13 -1 0
0 20 0 1
0 25 0 1
1
end_operator
begin_operator
move_car_out_road junction1-0 junction0-1 car1 road1
2
29 0
51 0
3
0 13 -1 0
0 21 0 1
0 25 0 1
1
end_operator
begin_operator
move_car_out_road junction1-0 junction0-1 car1 road2
2
30 0
63 0
3
0 13 -1 0
0 22 0 1
0 25 0 1
1
end_operator
begin_operator
move_car_out_road junction1-0 junction0-1 car1 road3
2
31 0
75 0
3
0 13 -1 0
0 23 0 1
0 25 0 1
1
end_operator
begin_operator
move_car_out_road junction1-0 junction1-1 car0 road0
2
28 0
40 0
3
0 11 -1 0
0 16 0 1
0 27 0 1
1
end_operator
begin_operator
move_car_out_road junction1-0 junction1-1 car0 road1
2
29 0
52 0
3
0 11 -1 0
0 17 0 1
0 27 0 1
1
end_operator
begin_operator
move_car_out_road junction1-0 junction1-1 car0 road2
2
30 0
64 0
3
0 11 -1 0
0 18 0 1
0 27 0 1
1
end_operator
begin_operator
move_car_out_road junction1-0 junction1-1 car0 road3
2
31 0
76 0
3
0 11 -1 0
0 19 0 1
0 27 0 1
1
end_operator
begin_operator
move_car_out_road junction1-0 junction1-1 car1 road0
2
28 0
40 0
3
0 15 -1 0
0 20 0 1
0 27 0 1
1
end_operator
begin_operator
move_car_out_road junction1-0 junction1-1 car1 road1
2
29 0
52 0
3
0 15 -1 0
0 21 0 1
0 27 0 1
1
end_operator
begin_operator
move_car_out_road junction1-0 junction1-1 car1 road2
2
30 0
64 0
3
0 15 -1 0
0 22 0 1
0 27 0 1
1
end_operator
begin_operator
move_car_out_road junction1-0 junction1-1 car1 road3
2
31 0
76 0
3
0 15 -1 0
0 23 0 1
0 27 0 1
1
end_operator
begin_operator
move_car_out_road junction1-1 junction0-0 car0 road0
2
28 0
41 0
3
0 8 -1 0
0 16 0 1
0 24 0 1
1
end_operator
begin_operator
move_car_out_road junction1-1 junction0-0 car0 road1
2
29 0
53 0
3
0 8 -1 0
0 17 0 1
0 24 0 1
1
end_operator
begin_operator
move_car_out_road junction1-1 junction0-0 car0 road2
2
30 0
65 0
3
0 8 -1 0
0 18 0 1
0 24 0 1
1
end_operator
begin_operator
move_car_out_road junction1-1 junction0-0 car0 road3
2
31 0
77 0
3
0 8 -1 0
0 19 0 1
0 24 0 1
1
end_operator
begin_operator
move_car_out_road junction1-1 junction0-0 car1 road0
2
28 0
41 0
3
0 12 -1 0
0 20 0 1
0 24 0 1
1
end_operator
begin_operator
move_car_out_road junction1-1 junction0-0 car1 road1
2
29 0
53 0
3
0 12 -1 0
0 21 0 1
0 24 0 1
1
end_operator
begin_operator
move_car_out_road junction1-1 junction0-0 car1 road2
2
30 0
65 0
3
0 12 -1 0
0 22 0 1
0 24 0 1
1
end_operator
begin_operator
move_car_out_road junction1-1 junction0-0 car1 road3
2
31 0
77 0
3
0 12 -1 0
0 23 0 1
0 24 0 1
1
end_operator
begin_operator
move_car_out_road junction1-1 junction0-1 car0 road0
2
28 0
42 0
3
0 9 -1 0
0 16 0 1
0 25 0 1
1
end_operator
begin_operator
move_car_out_road junction1-1 junction0-1 car0 road1
2
29 0
54 0
3
0 9 -1 0
0 17 0 1
0 25 0 1
1
end_operator
begin_operator
move_car_out_road junction1-1 junction0-1 car0 road2
2
30 0
66 0
3
0 9 -1 0
0 18 0 1
0 25 0 1
1
end_operator
begin_operator
move_car_out_road junction1-1 junction0-1 car0 road3
2
31 0
78 0
3
0 9 -1 0
0 19 0 1
0 25 0 1
1
end_operator
begin_operator
move_car_out_road junction1-1 junction0-1 car1 road0
2
28 0
42 0
3
0 13 -1 0
0 20 0 1
0 25 0 1
1
end_operator
begin_operator
move_car_out_road junction1-1 junction0-1 car1 road1
2
29 0
54 0
3
0 13 -1 0
0 21 0 1
0 25 0 1
1
end_operator
begin_operator
move_car_out_road junction1-1 junction0-1 car1 road2
2
30 0
66 0
3
0 13 -1 0
0 22 0 1
0 25 0 1
1
end_operator
begin_operator
move_car_out_road junction1-1 junction0-1 car1 road3
2
31 0
78 0
3
0 13 -1 0
0 23 0 1
0 25 0 1
1
end_operator
begin_operator
move_car_out_road junction1-1 junction1-0 car0 road0
2
28 0
43 0
3
0 10 -1 0
0 16 0 1
0 26 0 1
1
end_operator
begin_operator
move_car_out_road junction1-1 junction1-0 car0 road1
2
29 0
55 0
3
0 10 -1 0
0 17 0 1
0 26 0 1
1
end_operator
begin_operator
move_car_out_road junction1-1 junction1-0 car0 road2
2
30 0
67 0
3
0 10 -1 0
0 18 0 1
0 26 0 1
1
end_operator
begin_operator
move_car_out_road junction1-1 junction1-0 car0 road3
2
31 0
79 0
3
0 10 -1 0
0 19 0 1
0 26 0 1
1
end_operator
begin_operator
move_car_out_road junction1-1 junction1-0 car1 road0
2
28 0
43 0
3
0 14 -1 0
0 20 0 1
0 26 0 1
1
end_operator
begin_operator
move_car_out_road junction1-1 junction1-0 car1 road1
2
29 0
55 0
3
0 14 -1 0
0 21 0 1
0 26 0 1
1
end_operator
begin_operator
move_car_out_road junction1-1 junction1-0 car1 road2
2
30 0
67 0
3
0 14 -1 0
0 22 0 1
0 26 0 1
1
end_operator
begin_operator
move_car_out_road junction1-1 junction1-0 car1 road3
2
31 0
79 0
3
0 14 -1 0
0 23 0 1
0 26 0 1
1
end_operator
0
