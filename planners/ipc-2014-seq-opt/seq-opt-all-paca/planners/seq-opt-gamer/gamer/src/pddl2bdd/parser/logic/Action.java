package pddl2bdd.parser.logic;

import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Vector;

import pddl2bdd.util.Maths;

import net.sf.javabdd.*;

public class Action {
    public Action() {
        unused = false;
    }

    /*
     * for (int i = 0; i < topActionNode.numNodes(); i++) { PNode action =
     * topActionNode.getNode(i); if
     * (action.getToken().token.equals(":multi-action")) continue; PNode effect
     * = action.getNode(2); if (!effect.getToken().token.equals("and"))
     * continue; HashMap<String, PNode> addEffects = new HashMap<String,
     * PNode>(); HashMap<String, PNode> delEffects = new HashMap<String,
     * PNode>(); for (int j = 0; j < effect.numNodes(); j++) { PNode node =
     * effect.getNode(j); if (((TypeClass)
     * node.getToken().type).equals(TypeClass.NOT)) { PNode node2 =
     * node.getNode(0); if (!((TypeClass)
     * node2.getToken().type).equals(TypeClass.NAME))
     * System.err.println("Warning! Unexpected token in deleteAddAndDeleteEffects"
     * ); delEffects.put(node2.getToken().token, node); } else if (((TypeClass)
     * node.getToken().type).equals(TypeClass.NAME)) {
     * addEffects.put(node.getToken().token, node); } else
     * System.err.println("Warning! Unexpected token in deleteAddAndDeleteEffects"
     * ); } Iterator<String> delEffectsIt = delEffects.keySet().iterator();
     * while (delEffectsIt.hasNext()) { String effectName = delEffectsIt.next();
     * if (addEffects.containsKey(effectName)) delEffectsIt.remove(); } int
     * newSize = addEffects.size() + delEffects.size(); if (newSize <
     * effect.numNodes()) { PNode newEffect = new PNode(newSize);
     * newEffect.setToken(effect.getToken()); int counter = 0; for (PNode node :
     * addEffects.values()) { // System.out.println(node.getToken().token);
     * newEffect.setNode(counter++, node); } for (PNode node :
     * delEffects.values()) { // System.out.println(node.getToken().token);
     * newEffect.setNode(counter++, node); } // System.out.println();
     * action.setNode(2, newEffect); } }
     */

    public BDD createBDD(BDDFactory factory, LinkedList<String> nAryVariables,
            LinkedList<BDD> nAryVariablesPreBDDs,
            LinkedList<BDD> nAryVariablesEffBDDs,
            LinkedList<LinkedList<String>> partitionedVariables,
            BDD[] variables, boolean[] unusedVarIndices) {
        BDD tmp1;
        BDD tmp2;
        BDD action;

        tmp1 = precondition.createBDD(factory, nAryVariables,
                nAryVariablesPreBDDs, unusedVarIndices);
        if (tmp1 == null)
            tmp1 = factory.one();
        tmp2 = effect.createBDD(factory, nAryVariables, nAryVariablesEffBDDs,
                unusedVarIndices);
        if (tmp2 == null) {
            tmp1.free();
            return null;
        }
        action = tmp1.and(tmp2);
        tmp1.free();
        tmp2.free();
        Vector<String> addEffects = new Vector<String>();
        Vector<String> deleteEffects = new Vector<String>();
        effect.classifyEffects(addEffects, deleteEffects, false);
        int partitionCounter = 0;
        int currentVariable = 0;
        ListIterator<LinkedList<String>> partitionIt = partitionedVariables
                .listIterator();
        while (partitionIt.hasNext()) {
            LinkedList<String> group = partitionIt.next();
            int size = group.size();
            int numberOfVars = Maths.log2(size);
            boolean oneVariableInserted = false;
            boolean positiveVariableInserted = false;
            ListIterator<String> groupIt = group.listIterator();
            while (groupIt.hasNext()) {
                String pred = groupIt.next();
                if (addEffects.contains(pred)) {
                    oneVariableInserted = true;
                    positiveVariableInserted = true;
                    break;
                }
                if (!oneVariableInserted && deleteEffects.contains(pred))
                    oneVariableInserted = true;
            }
            if (!oneVariableInserted) {
                for (int i = 0; i < numberOfVars; i++) {
                    tmp1 = variables[(currentVariable + i) * 2];
                    tmp1 = tmp1.biimp(variables[(currentVariable + i) * 2 + 1]);
                    tmp2 = action;
                    action = tmp1.and(tmp2);
                    tmp1.free();
                    tmp2.free();
                }
            } else if (!positiveVariableInserted) {
                if (group.getLast().startsWith("none-of-these")) {
                    tmp1 = action;
                    action = tmp1.and(nAryVariablesEffBDDs.get(nAryVariables
                            .indexOf(group.getLast())));
                    tmp1.free();
                } else {
                    System.err
                            .println("Error: only negated variable of group "
                                    + partitionCounter
                                    + " in effect though there is no \'none-of-these\'-variable!");
                    System.exit(1);
                }
            }
            partitionCounter++;
            currentVariable += numberOfVars;
        }
        return action;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public int getCost() {
        return cost;
    }

    public void setPrecondition(Expression precondition) {
        this.precondition = precondition;
    }

    public Expression getPrecondition() {
        return precondition;
    }

    public void setEffect(Expression effect) {
        this.effect = effect;
        Vector<String> delEffects = new Vector<String>();
        Vector<String> addEffects = new Vector<String>();
        this.effect.findDelAdds(delEffects, addEffects, true);
        ListIterator<String> delIt = delEffects.listIterator();
        Vector<String> delEffectsToEliminate = new Vector<String>();
        while (delIt.hasNext()) {
            String predName = delIt.next();
            if (addEffects.contains(predName)) {
                delEffectsToEliminate.add(predName);
            }
        }
        if (delEffectsToEliminate.size() > 0) {
            effect.eliminateDelEffects(delEffectsToEliminate, true);
        }
    }

    public Expression getEffect() {
        return effect;
    }

    public void setUnused(boolean unused) {
        this.unused = unused;
    }

    public boolean getUnused() {
        return unused;
    }

    public String toString() {
        String ret = "action " + name + ":\n";
        ret += "cost: " + cost + "\n";
        ret += "precondition:\n";
        ret += precondition.toString() + "\n";
        ret += "effect:\n";
        ret += effect.toString() + "\n\n";
        return ret;
    }

    private String name;
    private int cost;
    private Expression precondition;
    private Expression effect;
    private boolean unused;
}
