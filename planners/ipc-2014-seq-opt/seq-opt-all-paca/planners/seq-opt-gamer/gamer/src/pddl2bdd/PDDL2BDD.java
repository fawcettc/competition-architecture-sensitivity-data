/*
 * Gamer, a tool for finding optimal plans
 * Copyright (C) 2007/2008 by Peter Kissmann
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */

package pddl2bdd;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.File;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Vector;
import pddl2bdd.parser.GroundedPDDLParser;
import pddl2bdd.parser.logic.*;
import pddl2bdd.pddl2bdd.MakeFDD;
import pddl2bdd.pddl2bdd.MakeFDDAStar;
import pddl2bdd.pddl2bdd.MakeAbstractFDD;
import pddl2bdd.util.Maths;
import pddl2bdd.util.Time;
import java.util.Random;

/**
 * This class converts the (instantiated) domain- and problem-files into BDDs.
 * 
 * @author Peter Kissmann
 * @version 1.7
 */
public class PDDL2BDD {
    public static boolean COSTS = false;
    public static boolean BIDIRECTIONAL = true;
    public static boolean BFS = false;
    public static boolean ASTAR = true;
    public static boolean ABSTRACT = false;
    public static boolean USENOMOREABSTRACTION = true;
    public static boolean USEBETTERVARIABLEORDERING = true;
    public static boolean USEABSTRACTION = true;
    public static boolean AUTOMATEDBIDIR = true;

    public static void printCall() {
        System.err.println("Error in program call!");
        System.err
                .println("call: java -classpath <path to gamer.jar>:<path to JavaBDD jar> -Dbdd=<BDD package> <name of problem file> <BDD package> [options]");
        System.err.println("possible options:");
        System.err
                .println("\t-a (--astar): use of A* search, if only uniform costs present (default: uses BFS)");
        System.err
                .println("\t-u (--unidir): use unidirectional search in case of BFS (default: uses bidirectional search)");
        System.err
                .println("\t-d (--dijkstra): use no heuristic in case of A* search (default: builds a pattern database)");
        System.err
                .println("\t-s (--smaller-pdb): use smaller patterns for pattern database (default: no abstraction)");
        System.err
                .println("\t-f (--fixed-ordering): use variable ordering specified by partition file (default: calculates better ordering randomly)");
        System.err
                .println("\t-c (--choice): use bidirectional vs. unidirectional BFS or PDB construction style as specified (default: will determine which to choose depending on expansion times for first forward and backward BFS layers)");
        System.exit(1);
    }

    /**
     * The main method that performs the conversion.
     * 
     * @param args
     *            Contains all the parameters of the program call.
     */
    public static void main(String[] args) {
        MakeFDD maker = null;
        MakeFDDAStar makerA = null;
        MakeAbstractFDD absMaker = null;
        long startingTime;
        long endingTime = 0;
        String partFileName;
        String bddLibrary;
        boolean useAstarAnyway = false;

        if (args.length < 2) {
            System.err.println("Error: not enough arguments in program call!");
            printCall();
        }
        partFileName = args[0];
        bddLibrary = args[1];
        for (int i = 2; i < args.length; i++) {
            if (args[i].equals("-a") || args[i].equals("--astar"))
                useAstarAnyway = true;
            else if (args[i].equals("-u") || args[i].equals("--unidir"))
                BIDIRECTIONAL = false;
            else if (args[i].equals("-d") || args[i].equals("--dijkstra"))
                USEABSTRACTION = false;
            else if (args[i].equals("-s") || args[i].equals("--smaller-pdb"))
                USENOMOREABSTRACTION = false;
            else if (args[i].equals("-f") || args[i].equals("--fixed-ordering"))
                USEBETTERVARIABLEORDERING = false;
            else if (args[i].equals("-c") || args[i].equals("--choice"))
                AUTOMATEDBIDIR = false;
            else {
                System.err.println("Error: unknown argument " + args[i]
                        + " in program call!");
                printCall();
            }
        }

        startingTime = System.currentTimeMillis();

        // delete derived predicates
        GroundedPDDLParser.parse(partFileName);
        COSTS = !GroundedPDDLParser.isUniformCost();
        if (useAstarAnyway)
            COSTS = true;
        BFS = !COSTS;
        ASTAR = COSTS;

        endingTime = System.currentTimeMillis();
        System.out.println("parsing took "
                + Time.printTime(endingTime - startingTime));

        // transform into BDD
        if (partFileName.startsWith("abstract")) {
            ABSTRACT = true;
        }
        LinkedList<Integer> emptyPartitions = new LinkedList<Integer>();
        if (ABSTRACT && !COSTS) {
            System.out.println("Stopping due to abstract but no costs!");
            System.out
                    .println("Total time: "
                            + Time.printTime(System.currentTimeMillis()
                                    - startingTime));
            System.exit(0);
        }
        int numberOfVariables = 0;
        LinkedList<LinkedList<String>> partitions = new LinkedList<LinkedList<String>>();
        ListIterator<Vector<Predicate>> partitionIt = GroundedPDDLParser.partitioning
                .listIterator();
        while (partitionIt.hasNext()) {
            Vector<Predicate> group = partitionIt.next();
            ListIterator<Predicate> groupIt = group.listIterator();
            LinkedList<String> partition = new LinkedList<String>();
            while (groupIt.hasNext()) {
                partition.add(groupIt.next().getName());
            }
            partitions.add(partition);
            numberOfVariables += Maths.log2(group.size());
        }
        numberOfVariables = numberOfVariables * 2;
        boolean[][] influences = calculateInfluences(partitions);
        int[] variableOrdering;
        if (USEBETTERVARIABLEORDERING) {
            if (ASTAR && !ABSTRACT) {
                LinkedList<Integer> ordering = new LinkedList<Integer>();
                boolean useDefaultOrdering = false;
                try {
                    BufferedReader bufferedReader = new BufferedReader(
                            new FileReader("variableOrdering.txt"));
                    String line;
                    while ((line = bufferedReader.readLine()) != null) {
                        ordering.add(Integer.parseInt(line));
                    }
                    bufferedReader.close();
                } catch (Exception e) {
                    System.err.println("Warning: " + e.getMessage());
                    e.printStackTrace();
                    System.err.println("using default ordering");
                    useDefaultOrdering = true;
                }
                variableOrdering = new int[influences.length];
                if (!useDefaultOrdering) {
                    LinkedList<LinkedList<String>> newPartitions = new LinkedList<LinkedList<String>>();
                    ListIterator<Integer> orderIt = ordering.listIterator();
                    int index = 0;
                    int nextOrderingItem;
                    while (orderIt.hasNext()) {
                        nextOrderingItem = orderIt.next();
                        newPartitions.add(partitions.get(nextOrderingItem));
                        variableOrdering[index] = nextOrderingItem;
                    }
                    partitions = newPartitions;
                } else {
                    for (int i = 0; i < variableOrdering.length; i++) {
                        variableOrdering[i] = i;
                    }
                }
            } else {
                variableOrdering = findVariableOrdering(influences);
                try {
                    FileWriter ordering = new FileWriter(
                            "variableOrdering_tmp.txt");
                    for (int i = 0; i < variableOrdering.length; i++) {
                        ordering.write(variableOrdering[i] + "\n");
                    }
                    ordering.flush();
                    ordering.close();
                    Runtime.getRuntime().exec(
                            "mv variableOrdering_tmp.txt variableOrdering.txt");
                } catch (Exception e) {
                    System.err.println("Error: " + e.getMessage());
                    e.printStackTrace();
                    System.exit(1);
                }
                LinkedList<LinkedList<String>> newPartitions = new LinkedList<LinkedList<String>>();
                for (int i = 0; i < variableOrdering.length; i++) {
                    newPartitions.add(partitions.get(variableOrdering[i]));
                }
                partitions = newPartitions;
            }
        } else {
            variableOrdering = new int[partitions.size()];
            for (int i = 0; i < variableOrdering.length; i++) {
                variableOrdering[i] = i;
            }
        }

        if (AUTOMATEDBIDIR) {
            if ((USEABSTRACTION && ABSTRACT && USENOMOREABSTRACTION)
                    || (BFS && BIDIRECTIONAL)) {
                endingTime = System.currentTimeMillis();
                System.out.println("Initializing planner after "
                        + Time.printTime(endingTime - startingTime));
                maker = new MakeFDD(partitions, numberOfVariables, bddLibrary);
                System.out.println("Initialization done; took: "
                        + Time.printTime(System.currentTimeMillis()
                                - endingTime));
                endingTime = System.currentTimeMillis();
                System.out
                        .println("checking time for first backward step vs. time for first forward step ...");
                double factor = maker.checkBackwardVsForwardTime();
                if (!BFS) {
                    maker.cleanup();
                    maker = null;
                }
                System.out.println("   result: " + factor);
                if (factor > 25.0) {
                    System.out.println("   too long for no abstraction");
                    USENOMOREABSTRACTION = false;
                }
                if (factor == Double.MAX_VALUE) {
                    System.out.println("   too long for backward BFS");
                    BIDIRECTIONAL = false;
                }
                System.out.println("done.");
                System.out.println("took: "
                        + Time.printTime(System.currentTimeMillis()
                                - endingTime));
            }
        }

        endingTime = System.currentTimeMillis();
        System.out.println("Total time so far: "
                + Time.printTime(endingTime - startingTime));
        System.out.println("creating BDD ...");
        if (ABSTRACT) {
            if (USEABSTRACTION) {
                if (USENOMOREABSTRACTION) {
                    try {
                        File usedPDBsFile = new File("usePDBs.txt");
                        FileWriter usePDBs = new FileWriter("usePDBs_tmp.txt");
                        if (usedPDBsFile.exists()) {
                            LinkedList<String> usedPDBs = new LinkedList<String>();
                            BufferedReader bufferedReader = new BufferedReader(
                                    new FileReader("usePDBs.txt"));
                            String line;
                            while ((line = bufferedReader.readLine()) != null) {
                                usedPDBs.add(line);
                            }
                            bufferedReader.close();
                            ListIterator<String> usedIt = usedPDBs
                                    .listIterator();
                            while (usedIt.hasNext()) {
                                usePDBs.write(usedIt.next() + "\n");
                            }
                        }
                        String pdbID = partFileName.split("-", 2)[0];
                        pdbID = pdbID.substring(8);
                        usePDBs.write(pdbID + "\n");
                        usePDBs.flush();
                        usePDBs.close();
                        String abstractionFileName = partFileName.substring(0,
                                partFileName.length() - 8);
                        FileWriter maxPDB = new FileWriter(abstractionFileName
                                + "MaxPDB.txt");
                        maxPDB.write("-1\n");
                        maxPDB.flush();
                        maxPDB.close();
                        FileWriter exPDB = new FileWriter(abstractionFileName
                                + "ExPDBs.txt");
                        exPDB.write("");
                        exPDB.flush();
                        exPDB.close();
                        Runtime.getRuntime().exec(
                                "mv usePDBs_tmp.txt usePDBs.txt");
                    } catch (Exception e) {
                        System.err.println("Error: " + e.getMessage());
                        e.printStackTrace();
                        System.exit(1);
                    }
                    absMaker = new MakeAbstractFDD(partitions, emptyPartitions,
                            numberOfVariables, bddLibrary, partFileName);
                    System.out.println("building PDB ...");
                    long time1 = System.currentTimeMillis();
                    if (partFileName.startsWith("super-abstract"))
                        absMaker.buildSuperPDB();
                    else
                        absMaker.buildPDB(false);
                    long time2 = System.currentTimeMillis();
                    System.out.println("done.");
                    System.out.println("construction time: "
                            + Time.printTime(time2 - time1));
                    System.out.println("cleaning up ...");
                    absMaker.cleanup();
                    System.out.println("done.");
                } else {
                    LinkedList<Integer> chosenPartitions = new LinkedList<Integer>();
                    Vector<Predicate> allPreds = new Vector<Predicate>();
                    GroundedPDDLParser.goalDescription
                            .getAllPredicates(allPreds);
                    Vector<String> goalTokens = new Vector<String>(allPreds
                            .size());
                    ListIterator<Predicate> predIt = allPreds.listIterator();
                    while (predIt.hasNext()) {
                        goalTokens.add(predIt.next().getName());
                    }
                    ListIterator<LinkedList<String>> partIt = partitions
                            .listIterator();
                    LinkedList<String> group;
                    int index = 0;
                    index = 0;
                    while (partIt.hasNext()) {
                        boolean foundElement = false;
                        group = partIt.next();
                        for (int i = 0; i < goalTokens.size(); i++) {
                            if (group.contains(goalTokens.elementAt(i))) {
                                foundElement = true;
                                break;
                            }
                        }
                        if (foundElement) {
                            chosenPartitions.add(index);
                        } else {
                            emptyPartitions.add(index);
                        }
                        index++;
                    }
                    int bestIndex = 1;
                    double bestAverage = -1;
                    int additional;
                    double[] averages;
                    try {
                        FileWriter maxPDB = new FileWriter(
                                "abstract1-1MaxPDB.txt");
                        maxPDB.write("-1\n");
                        maxPDB.flush();
                        maxPDB.close();
                        FileWriter exPDB = new FileWriter(
                                "abstract1-1ExPDBs.txt");
                        exPDB.write("");
                        exPDB.flush();
                        exPDB.close();
                        FileWriter usePDBs = new FileWriter("usePDBs_tmp.txt");
                        usePDBs.write("1\n");
                        usePDBs.flush();
                        usePDBs.close();
                        Runtime.getRuntime().exec(
                                "mv usePDBs_tmp.txt usePDBs.txt");
                    } catch (Exception e) {
                        System.err.println("Error: " + e.getMessage());
                        e.printStackTrace();
                        System.exit(1);
                    }
                    String partitionFileName = new String("abstract1-1Part.gdl");
                    endingTime = System.currentTimeMillis();
                    absMaker = new MakeAbstractFDD(partitions, emptyPartitions,
                            numberOfVariables, bddLibrary, partitionFileName);
                    System.out.println("Initialization took "
                            + Time.printTime(System.currentTimeMillis()
                                    - endingTime));
                    System.out.println("building PDB ...");
                    long time1 = System.currentTimeMillis();
                    if (partFileName.startsWith("super-abstract"))
                        absMaker.buildSuperPDB();
                    else
                        bestAverage = absMaker.buildPDB(true);
                    long time2 = System.currentTimeMillis();
                    System.out.println("done.");
                    System.out.println("construction time: "
                            + Time.printTime(time2 - time1));
                    System.out.println("cleaning up ...");
                    absMaker.cleanup();
                    System.out.println("done.");
                    while (bestIndex != -1) {
                        additional = bestIndex + 1;
                        bestIndex = -1;
                        averages = new double[emptyPartitions.size()];
                        for (index = 0; index < emptyPartitions.size(); index++) {
                            boolean isInfluencing = false;
                            System.out.println("index: " + index);
                            int oldEmptyPartition = emptyPartitions.get(index);
                            ListIterator<Integer> chosenIt = chosenPartitions
                                    .listIterator();
                            while (chosenIt.hasNext()) {
                                if (influences[variableOrdering[oldEmptyPartition]][variableOrdering[chosenIt
                                        .next()]]) {
                                    isInfluencing = true;
                                    break;
                                }
                            }
                            if (!isInfluencing)
                                continue;
                            emptyPartitions.remove(index);
                            partitionFileName = new String("abstract"
                                    + (index + additional) + "-1Part.gdl");
                            endingTime = System.currentTimeMillis();
                            absMaker = new MakeAbstractFDD(partitions,
                                    emptyPartitions, numberOfVariables,
                                    bddLibrary, partitionFileName);
                            System.out.println("Initialization took "
                                    + Time.printTime(System.currentTimeMillis()
                                            - endingTime));
                            System.out.println("building PDB ...");
                            time1 = System.currentTimeMillis();
                            if (partFileName.startsWith("super-abstract"))
                                absMaker.buildSuperPDB();
                            else
                                averages[index] = absMaker.buildPDB(true);
                            if (averages[index] > bestAverage) {
                                bestAverage = averages[index];
                                bestIndex = index + additional;
                                try {
                                    FileWriter usePDBs = new FileWriter(
                                            "usePDBs_tmp.txt");
                                    usePDBs.write(bestIndex + "\n");
                                    usePDBs.flush();
                                    usePDBs.close();
                                    Runtime.getRuntime().exec(
                                            "mv usePDBs_tmp.txt usePDBs.txt");
                                } catch (Exception e) {
                                    System.err.println("Error: "
                                            + e.getMessage());
                                    e.printStackTrace();
                                    System.exit(1);
                                }
                            }
                            time2 = System.currentTimeMillis();
                            System.out.println("done.");
                            System.out.println("construction time: "
                                    + Time.printTime(time2 - time1));
                            System.out.println("cleaning up ...");
                            absMaker.cleanup();
                            System.out.println("done.");
                            emptyPartitions.add(index, oldEmptyPartition);
                        }
                        System.out.println("best index: " + bestIndex
                                + "; best average: " + bestAverage);
                        if (bestIndex != -1) {
                            int numberAdded = 0;
                            for (int i = emptyPartitions.size() - 1; i >= 0; i--) {
                                if (averages[i] >= 0.999 * bestAverage) {
                                    System.out.println("also chosen index: "
                                            + (i + additional) + "; average: "
                                            + averages[i]);
                                    chosenPartitions.add(emptyPartitions
                                            .remove(i));
                                    numberAdded++;
                                }
                            }
                            if (numberAdded > 1) {
                                // if adding more than one group, the new PDB
                                // will be generated, as it might contain all
                                // groups and would not be generated otherwise
                                partitionFileName = new String("abstract"
                                        + (bestIndex + 1) + "-1Part.gdl");
                                absMaker = new MakeAbstractFDD(partitions,
                                        emptyPartitions, numberOfVariables,
                                        bddLibrary, partitionFileName);
                                System.out.println("building new PDB ...");
                                time1 = System.currentTimeMillis();
                                if (partFileName.startsWith("super-abstract"))
                                    absMaker.buildSuperPDB();
                                else {
                                    averages[0] = absMaker.buildPDB(true);
                                    System.out.println("average: "
                                            + averages[0]);
                                    if (averages[0] > bestAverage) {
                                        bestAverage = averages[0];
                                        bestIndex = bestIndex + 1;
                                        try {
                                            FileWriter usePDBs = new FileWriter(
                                                    "usePDBs_tmp.txt");
                                            usePDBs.write(bestIndex + "\n");
                                            usePDBs.flush();
                                            usePDBs.close();
                                            Runtime
                                                    .getRuntime()
                                                    .exec(
                                                            "mv usePDBs_tmp.txt usePDBs.txt");
                                        } catch (Exception e) {
                                            System.err.println("Error: "
                                                    + e.getMessage());
                                            e.printStackTrace();
                                            System.exit(1);
                                        }
                                    }
                                }
                                time2 = System.currentTimeMillis();
                                System.out.println("done.");
                                System.out.println("construction time: "
                                        + Time.printTime(time2 - time1));
                                System.out.println("cleaning up ...");
                                absMaker.cleanup();
                                System.out.println("done.");
                            }
                        }
                    }
                    System.out.println("done.");
                }
            }
        } else if (BFS) {
            if (maker == null) {
                maker = new MakeFDD(partitions, numberOfVariables, bddLibrary);
                System.out.println("Initialization took: "
                        + Time.printTime(System.currentTimeMillis()
                                - endingTime));
                System.out.println("done.");
            }
            GroundedPDDLParser.cleanup();
            System.out.println("finding shortest plan ...");
            endingTime = System.currentTimeMillis();
            maker.findPlanBFS(BIDIRECTIONAL);
            System.out.println("done.");
            System.out.println("took: "
                    + Time.printTime(System.currentTimeMillis() - endingTime));
            System.out.println("cleaning up ...");
            maker.cleanup();
            System.out.println("done.");
        } else if (ASTAR) {
            makerA = new MakeFDDAStar(partitions, numberOfVariables,
                    bddLibrary, partFileName);
            System.out.println("Initialization took: "
                    + Time.printTime(System.currentTimeMillis() - endingTime));
            System.out.println("done.");
            GroundedPDDLParser.cleanup();
            System.out.println("finding cheapest plan ...");
            long time1 = System.currentTimeMillis();
            // makerA.findPlanAStar();
            makerA.findPlanAStarNew();
            long time2 = System.currentTimeMillis();
            System.out.println("done.");
            System.out.println("A* search time: "
                    + Time.printTime(time2 - time1));
            System.out.println("cleaning up ...");
            makerA.cleanup();
            System.out.println("done.");
        }

        endingTime = System.currentTimeMillis() - startingTime;
        System.out.println("\ntotal time: " + Time.printTime(endingTime));
    }

    // meaning: influences[a][b] = true if a influences b
    private static boolean[][] calculateInfluences(
            LinkedList<LinkedList<String>> partitions) {
        ListIterator<LinkedList<String>> partIt = partitions.listIterator();
        LinkedList<String> allVars = new LinkedList<String>();
        LinkedList<Integer> partitionIndices = new LinkedList<Integer>();
        while (partIt.hasNext()) {
            partitionIndices.add(allVars.size());
            allVars.addAll(partIt.next());
        }
        boolean[][] influences = new boolean[GroundedPDDLParser.partitioning
                .size()][GroundedPDDLParser.partitioning.size()];
        ListIterator<Action> actionIt = GroundedPDDLParser.actions
                .listIterator();
        int actionCounter = 0;
        while (actionIt.hasNext()) {
            Action currentAction = actionIt.next();
            LinkedList<Integer> effectIndices = new LinkedList<Integer>();
            findAllVariables(effectIndices, allVars, partitionIndices,
                    currentAction.getEffect());
            LinkedList<Integer> preIndices = new LinkedList<Integer>();
            findAllVariables(preIndices, allVars, partitionIndices,
                    currentAction.getPrecondition());
            ListIterator<Integer> influencingIndicesIt = effectIndices
                    .listIterator();
            actionCounter++;
            int influencingIndex;
            int influencedIndex;
            while (influencingIndicesIt.hasNext()) {
                influencingIndex = influencingIndicesIt.next();
                ListIterator<Integer> influencedIndicesIt = effectIndices
                        .listIterator();
                while (influencedIndicesIt.hasNext()) {
                    influencedIndex = influencedIndicesIt.next();
                    if (influencingIndex != influencedIndex) {
                        influences[influencingIndex][influencedIndex] = true;
                    }
                }
            }
            influencingIndicesIt = preIndices.listIterator();
            while (influencingIndicesIt.hasNext()) {
                influencingIndex = influencingIndicesIt.next();
                ListIterator<Integer> influencedIndicesIt = effectIndices
                        .listIterator();
                while (influencedIndicesIt.hasNext()) {
                    influencedIndex = influencedIndicesIt.next();
                    if (influencingIndex != influencedIndex) {
                        influences[influencingIndex][influencedIndex] = true;
                    }
                }
            }
        }
        return influences;
    }

    private static void findAllVariables(LinkedList<Integer> allIndices,
            LinkedList<String> allVariables, LinkedList<Integer> indices,
            Expression formula) {
        formula.getClass();
        if (formula instanceof AndOrTerm) {
            ListIterator<Expression> termIt = ((AndOrTerm) formula).getTerms()
                    .listIterator();
            while (termIt.hasNext()) {
                findAllVariables(allIndices, allVariables, indices, termIt
                        .next());
            }
        } else if (formula instanceof NotTerm) {
            findAllVariables(allIndices, allVariables, indices,
                    ((NotTerm) formula).getTerm());
        } else if (formula instanceof Predicate) {
            if (!((Predicate) formula).getName().equalsIgnoreCase("foo")) {
                int index = allVariables.indexOf(((Predicate) formula)
                        .getName());
                ListIterator<Integer> indicesIt = indices.listIterator();
                int partIndex = -1;
                while (indicesIt.hasNext()) {
                    if (indicesIt.next() > index)
                        break;
                    partIndex++;
                }
                if (!allIndices.contains(partIndex)) {
                    allIndices.add(partIndex);
                }
            }
        } else {
            System.err.println("Error: class " + formula.getClass()
                    + " in findAllVariables!");
            System.err.println("Expression was:");
            System.err.println(formula);
            System.exit(1);
        }
    }

    private static int[] findVariableOrdering(boolean[][] influences) {
        int[] variableOrdering = new int[influences.length];
        for (int i = 0; i < variableOrdering.length; i++) {
            variableOrdering[i] = i;
        }
        int[] bestVariableOrdering = new int[influences.length];
        long bestTotalDistance = Integer.MAX_VALUE;
        // Random rnd = new Random(System.currentTimeMillis());
        Random rnd = new Random(0);
        for (int i = 0; i < 20; i++) {
            long totalDistance;
            totalDistance = findOneVariableOrdering(influences,
                    variableOrdering, rnd);
            if (totalDistance < bestTotalDistance) {
                bestTotalDistance = totalDistance;
                bestVariableOrdering = variableOrdering.clone();
            }
            System.out.println(bestTotalDistance);
        }
        System.out.println(bestTotalDistance);
        for (int i = 0; i < bestVariableOrdering.length; i++) {
            System.out.print(bestVariableOrdering[i] + " ");
        }
        System.out.println();
        return bestVariableOrdering;
    }

    private static long findOneVariableOrdering(boolean[][] influences,
            int[] variableOrdering, Random rnd) {
        int swapIndex1 = 0;
        int swapIndex2 = 0;
        int tmpVariable;
        for (int i = variableOrdering.length - 1; i >= 1; i--) {
            tmpVariable = variableOrdering[i];
            swapIndex1 = rnd.nextInt(i + 1);
            variableOrdering[i] = variableOrdering[swapIndex1];
            variableOrdering[swapIndex1] = tmpVariable;
        }

        long oldTotalDistance = Integer.MAX_VALUE;
        long totalDistance = 0;
        for (int i = 0; i < variableOrdering.length - 1; i++) {
            for (int j = i + 1; j < variableOrdering.length; j++) {
                if (influences[variableOrdering[i]][variableOrdering[j]]
                        || influences[variableOrdering[j]][variableOrdering[i]]) {
                    totalDistance += Math.max(i - j, j - i)
                            * Math.max(i - j, j - i);
                }
            }
        }
        System.out.println("initial distance: " + totalDistance);
        oldTotalDistance = totalDistance;
        for (int counter = 0; counter < 50000; counter++) {
            swapIndex1 = rnd.nextInt(variableOrdering.length);
            swapIndex2 = rnd.nextInt(variableOrdering.length);
            // System.out.println(influences[variableOrdering[swapIndex1]][variableOrdering[swapIndex2]]);
            for (int i = 0; i < variableOrdering.length; i++) {
                if (i == swapIndex1 || i == swapIndex2)
                    continue;
                if ((influences[variableOrdering[i]][variableOrdering[swapIndex1]] || influences[variableOrdering[swapIndex1]][variableOrdering[i]])
                        && (influences[variableOrdering[i]][variableOrdering[swapIndex2]] || influences[variableOrdering[swapIndex2]][variableOrdering[i]]))
                    continue;
                if (influences[variableOrdering[i]][variableOrdering[swapIndex1]]
                        || influences[variableOrdering[swapIndex1]][variableOrdering[i]])
                    totalDistance = totalDistance
                            - Math.max(i - swapIndex1, swapIndex1 - i)
                            * Math.max(i - swapIndex1, swapIndex1 - i)
                            + Math.max(i - swapIndex2, swapIndex2 - i)
                            * Math.max(i - swapIndex2, swapIndex2 - i);
                if (influences[variableOrdering[i]][variableOrdering[swapIndex2]]
                        || influences[variableOrdering[swapIndex2]][variableOrdering[i]])
                    totalDistance = totalDistance
                            - Math.max(i - swapIndex2, swapIndex2 - i)
                            * Math.max(i - swapIndex2, swapIndex2 - i)
                            + Math.max(i - swapIndex1, swapIndex1 - i)
                            * Math.max(i - swapIndex1, swapIndex1 - i);
            }
            if (totalDistance < oldTotalDistance) {
                tmpVariable = variableOrdering[swapIndex1];
                variableOrdering[swapIndex1] = variableOrdering[swapIndex2];
                variableOrdering[swapIndex2] = tmpVariable;
                oldTotalDistance = totalDistance;
            } else {
                totalDistance = oldTotalDistance;
            }
        }
        return oldTotalDistance;
    }
}
