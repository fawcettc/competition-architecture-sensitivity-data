#include "lm_graph_paths.h"

#include <math.h>

LmGraphPaths::LmGraphPaths(LandmarksGraph& lgraph) {
	// Sorting nodes topologically and setting the initial coloring (numbering)
	vector<int> nodes;
	int max_id = 0;
	// Loop on all landmarks, take singletons out
	for (set<LandmarkNode*>::const_iterator it = lgraph.get_nodes().begin(); it
			!= lgraph.get_nodes().end(); it++) {
		LandmarkNode& curr_land = **it;
    	int curr_id = curr_land.get_id();
    	if (curr_land.children.empty()) { 	    // The node is sink
        	if (curr_land.parents.empty()) {    // The node is singleton
//        		cout << "Adding singleton " << curr_id << endl;
        		initially_no_edges.push_back(*it);
        	    continue;
        	}
        	sorted_nodes.push_back(curr_id);
        	target_nodes.push_back(curr_id);
//    		cout << "Sink node " << curr_id << endl;
    	} else { // Going over the children and adding an edge.
        	if (curr_land.parents.empty()) {    // The node is source
        		source_nodes.push_back(curr_id);
        	}
    	    for (hash_map<LandmarkNode*, edge_type, hash_pointer>::const_iterator
    	            child_it = curr_land.children.begin(); child_it
    	            != curr_land.children.end(); child_it++) {
    	    	// Adding the edges
		        LandmarkNode* child_p = child_it->first;
		        to_nodes[curr_id].push_back(child_p->get_id());
//	    		cout << "Adding edge (" << curr_id << "," << child_p->id << ")" << endl;
    	    }
    	}

    	if (curr_id > max_id)
	    	max_id = curr_id;
	    nodes.push_back(curr_id);
//		cout << "Adding node " << curr_id << endl;
	    landmarks_by_id[curr_id] = *it;
	}
//	cout << "Done adding nodes" << endl;

	// set initial marking, order nodes topologically.
	land_color.assign(max_id+1,-1);

	// Sorting nodes in topological order (bottom first)
	for (int i=0; i < sorted_nodes.size(); i++) {
		land_color[sorted_nodes[i]] = 0;
	}

	while (sorted_nodes.size() < nodes.size()) {
		for (int i=0; i < nodes.size(); i++) {
			if (land_color[nodes[i]] != -1)   // Already marked
				continue;

			bool to_mark = true;
			int mark = 0;
			vector<int> children = to_nodes[nodes[i]];
			for (int j=0; j < children.size(); j++) {
		    	if (land_color[children[j]] == -1) {
		    		to_mark = false;
		    		break;
		    	}
		    	if (land_color[children[j]] > mark)
		    		mark = land_color[children[j]];
			}

		    if (to_mark) {
		    	land_color[nodes[i]] = mark + 1;
		    	sorted_nodes.push_back(nodes[i]);
		    }
		}
	}
/*	cout << "Done ordering nodes" << endl;
	for (int i=0; i < sorted_nodes.size(); i++) {
    	cout << "Node " << sorted_nodes[i] << " mark " << land_color[sorted_nodes[i]] << endl;
	}*/
}


LmGraphPaths::~LmGraphPaths() {
}

void LmGraphPaths::update_marking() {

	for (int i=0; i < sorted_nodes.size(); i++) {
		int mark = -1;
		if (to_nodes.find(sorted_nodes[i]) == to_nodes.end()) {
	    	land_color[sorted_nodes[i]] = mark + 1;
	    	continue;
		}
		vector<int> children = to_nodes.find(sorted_nodes[i])->second;
		for (int j=0; j < children.size(); j++) {
	    	if (land_color[children[j]] > mark)
	    		mark = land_color[children[j]];
		}
    	land_color[sorted_nodes[i]] = mark + 1;
//    	cout << "Node " << sorted_nodes[i] << " mark " << land_color[sorted_nodes[i]] << endl;
	}
}

bool LmGraphPaths::is_max_child(int node_id, int child_id) const {
	return (land_color[child_id] == land_color[node_id] - 1);
}

void LmGraphPaths::erase_link(int node_id, int to_index) {

	hash_map<int, vector<int> >::iterator it = to_nodes.find(node_id);
	if (it == to_nodes.end())
		return;

	it->second.erase(it->second.begin()+to_index);
	if (it->second.size() == 0)
		to_nodes.erase(it);
}

void LmGraphPaths::erase_node(int node_id) {
	cout << "Erazing node " << node_id << endl;

	// Removing links from node
	hash_map<int, vector<int> >::iterator from = to_nodes.find(node_id);
	if (from != to_nodes.end()) {
		for (int i=0; i<from->second.size(); i++) {
			cout << "Erazing link " << node_id << " -> " << from->second[i] << endl;
		}
		to_nodes.erase(from);
	} else {
		cout << "No from links to erase" << endl;
	}

	// Removing links to node
	for (hash_map<int, vector<int> >::iterator it = to_nodes.begin(); it
					!= to_nodes.end(); it++) {
		for (int i=0; i<it->second.size(); i++) {
			if (it->second[i] == node_id) {
				cout << "Erazing link " << it->first << " -> " << it->second[i] << endl;
				it->second.erase(it->second.begin()+i);
				break;
			}
		}
	}
}

int LmGraphPaths::get_max_node() const {
	int max_len = 0;
	int max_node = -1;
	for (int i=0; i < sorted_nodes.size(); i++) {
		if (land_color[sorted_nodes[i]] > max_len) {
			max_len = land_color[sorted_nodes[i]];
			max_node = sorted_nodes[i];
		}
	}
	return max_node;
}


LandmarkNode* LmGraphPaths::extract_max_child_node(int node_id) {
	vector<int> children = to_nodes.find(node_id)->second;
	int max_ch = -1, ch_sz = children.size();
	for (int ch = 0; ch < ch_sz; ch++) {
		if (is_max_child(node_id,children[ch])) {
			max_ch = children[ch];
			erase_link(node_id,ch);
			break;
		}
	}
	assert(max_ch != -1);
//	return landmarks_by_id[max_ch];
	return get_landmark_by_id(max_ch);
}


void LmGraphPaths::remove_shortcuts(vector<LandmarkNode*>& path) {

	for (int i=0; i < path.size() - 2; i++) {
		for (int j=i+2; j < path.size(); j++) {
			// Removing shortcut from i to j
			int to = path[i]->get_id();
			int from = path[j]->get_id();
//			cout << "Erasing link (" << from << "," << to << ")" << endl;
			hash_map<int, vector<int> >:: const_iterator it = to_nodes.find(from);
			if (it == to_nodes.end()) {
//				cout << "Not found" << endl;
				continue;
			}
			for (int ch=0; ch < it->second.size(); ch++) {
//				cout << to_nodes[from][ch];
				if (it->second[ch] == to) {
//					cout << "  Found!" << endl;
					erase_link(from, ch);
					break;
				}
//				cout << endl;
			}
		}
	}
}

LandmarkNode* LmGraphPaths::get_landmark_by_id(int node) const {

	hash_map<int, LandmarkNode*>:: const_iterator it = landmarks_by_id.find(node);
	if (it != landmarks_by_id.end())
		return it->second;
	return NULL;
}

//////////////////////////////////////////////////////////////////////////////////

int LmGraphPaths::get_num_paths() const {

	int num_nodes = land_color.size();

	int** A = new int *[num_nodes];
	for( int i = 0 ; i < num_nodes ; i++ )
		A[i] = new int [num_nodes];

	for( int i = 0 ; i < num_nodes ; i++ )
		for( int j = 0 ; j < num_nodes ; j++ )
			A[i][j] = 0;

	// Initiating the adjacent matrix
	for (hash_map<int, vector<int> >::const_iterator it = to_nodes.begin(); it
					!= to_nodes.end(); it++) {
		int from = it->first;
		vector<int> to = it->second;
		for (int i=0; i<to.size(); i++) {
			A[from][to[i]] = 1;
		}
	}
/*
	for( int i = 0 ; i < num_nodes ; i++ ) {
		for( int j = 0 ; j < num_nodes ; j++ )
			cout << A[i][j] << " " ;

		cout << endl;
	}
*/
	// Counting the number of s-t paths
	for( int i = 0 ; i < num_nodes ; i++ )
		for( int j = 0 ; j < num_nodes ; j++ )
			for( int k = 0 ; k < num_nodes ; k++ )
				A[i][j] += A[i][k]*A[k][j];

	int val = 0;
	// Getting the total number of paths
	for( int i = 0 ; i < source_nodes.size() ; i++ ) {
		for( int j = 0 ; j < target_nodes.size() ; j++ ) {
			val += A[source_nodes[i]][target_nodes[j]];
		}
	}

	for( int i = 0 ; i < num_nodes ; i++ )
		delete [] A[i];

	delete [] A;

	return val;

}

bool LmGraphPaths::is_edgeless() const {
	if (to_nodes.empty()) return true;
	return (-1 == get_max_node());
}


void LmGraphPaths::get_singleton_nodes(vector<LandmarkNode*>& nodes) {
	assert(nodes.size() == 0);
	nodes = initially_no_edges;
	get_edgeless_nodes(nodes);
/*
	int sz = sorted_nodes.size();

	vector<int> to_add;
	to_add.assign(sz,0);

	for (int i=0; i < sz; i++) {
		vector<int> children = to_nodes[sorted_nodes[i]];
		for (int j=0; j < children.size(); j++) {
			to_add[children[j]] = 1;
		}
		if (children.size() > 0)
			to_add[sorted_nodes[i]] = 1;
	}
	for (int i=0; i < sz; i++) {
		if (to_add[i] == 0)
			nodes.push_back(landmarks_by_id[i]);
	}
*/
}

void LmGraphPaths::get_edgeless_nodes(vector<LandmarkNode*>& nodes) {
	for (hash_map<int, vector<int> >::iterator it = to_nodes.begin(); it
				!= to_nodes.end(); it++) {
		if (it->second.size() == 0) {
			nodes.push_back(get_landmark_by_id(it->first));
		} else {
			cout << "Non edgeless nodes left!!!" << endl;
		}
	}
}

void LmGraphPaths::dump() {
	cout << "Landmark graph:" << endl << "Nodes:" << endl;
	for (hash_map<int, vector<int> >::iterator it = to_nodes.begin(); it
					!= to_nodes.end(); it++) {
		cout << it->first << endl;
	}
	cout << "Edges:" << endl;

	for (hash_map<int, vector<int> >::iterator it = to_nodes.begin(); it
					!= to_nodes.end(); it++) {
		for (int i=0; i<it->second.size(); i++) {
			cout << " " << it->first << " -> " << it->second[i] << ";" << endl;
		}
	}
/*
	cout << "Landmark graph:" << endl << "Nodes:" << endl;
    for (hash_map<int, LandmarkNode*>::const_iterator
            it = landmarks_by_id.begin(); it
            != landmarks_by_id.end(); it++) {
    	cout << it->second->get_id() << endl;
    }
    for (int i=0;i < initially_no_edges.size(); i++)
    	cout << initially_no_edges[i]->get_id() << endl;

	cout << "Edges:" << endl;


	int sz = sorted_nodes.size();

	for (int i=0; i < sz; i++) {
		vector<int> children = to_nodes[sorted_nodes[i]];
		for (int j=0; j < children.size(); j++) {
			cout << " " << sorted_nodes[i] << " -> " << children[j] << ";" << endl;
		}
	}
*/
}


/////////////////////////////////////////////////////////////////////////////////

LmGraphPathsLinks::LmGraphPathsLinks(LandmarksGraph& lgraph) : LmGraphPaths(lgraph){
}
LmGraphPathsLinks::~LmGraphPathsLinks() {
}

void LmGraphPathsLinks::extract_longest_path(vector<LandmarkNode*>& path) {
	assert(path.size()==0);
	int next_node = get_max_node();
	int max_len = get_marking(next_node);
//	cout << "Next node: " << next_node << " distance " << max_len <<endl;
	// Collecting the path, removing links, updating the marks of nodes.
	path.push_back(get_landmark_by_id(next_node));
	for (int i=0; i < max_len; i++) {
		LandmarkNode* node = extract_max_child_node(next_node);
		assert(node!=NULL);
		path.insert(path.begin(),node);
		next_node = node->get_id();
	}
	remove_shortcuts(path);
	update_marking();
}

/////////////////////////////////////////////////////////////////////////////////

LmGraphPathsNodes::LmGraphPathsNodes(LandmarksGraph& lgraph) : LmGraphPaths(lgraph){
}
LmGraphPathsNodes::~LmGraphPathsNodes() {
}

void LmGraphPathsNodes::extract_longest_path(vector<LandmarkNode*>& path) {

	cout << "Extracting longest path" << endl;
	assert(path.size()==0);
	int next_node = get_max_node();
	int max_len = get_marking(next_node);
//	cout << "Next node: " << next_node << " distance " << max_len <<endl;
	// Collecting the path, removing links, updating the marks of nodes.
	path.push_back(get_landmark_by_id(next_node));
	for (int i=0; i < max_len; i++) {
		LandmarkNode* node = extract_max_child_node(next_node);
		assert(node!=NULL);
		path.insert(path.begin(),node);
		erase_node(next_node);
		next_node = node->get_id();
	}
	erase_node(next_node);
	cout << "Updating marking" << endl;
	update_marking();
	cout << "Done updating marking" << endl;
}

