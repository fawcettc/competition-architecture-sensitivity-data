#ifndef OPERATOR_H
#define OPERATOR_H

#include <cassert>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>

#include "globals.h"
#include "state.h"


struct Prevail {
    int var;
    int prev;
    Prevail(std::istream &in);
    Prevail(int v, int p) : var(v), prev(p) {}

    bool is_applicable(const State &state) const {
	assert(var >= 0 && var < g_variable_name.size());
	assert(prev >= 0 && prev < g_variable_domain[var]);
	return state[var] == prev;
    }

    void dump() const;
    // Added by Michael
    bool operator==(const Prevail &other) const {
    	return (var == other.var && prev == other.prev);
    }
    bool operator!=(const Prevail &other) const {
    	return !(*this == other);
    }
    bool operator<(const Prevail &other) const {
    	return (var < other.var || (var == other.var && prev < other.prev));
    }
    void dump_SAS(ofstream& os) const {
    	os << var << " " << prev << endl;
    }
};

int get_value_for_var(int v, const vector<Prevail>& prv);

struct PrePost {
    int var;
    int pre, post;
    std::vector<Prevail> cond;
    PrePost() {} // Needed for axiom file-reading constructor, unfortunately.
    PrePost(std::istream &in);
    PrePost(int v, int pr, int po, const std::vector<Prevail> &co)
	: var(v), pre(pr), post(po), cond(co) {
    	  // Michael: sorting the condition vector.
    	  ::sort(cond.begin(), cond.end());
    }

    bool is_applicable(const State &state) const {
	assert(var >= 0 && var < g_variable_name.size());
	assert(pre == -1 || (pre >= 0 && pre < g_variable_domain[var]));
	return pre == -1 || state[var] == pre;
    }

    bool does_fire(const State &state) const {
	for(int i = 0; i < cond.size(); i++)
	    if(!cond[i].is_applicable(state))
		return false;
	return true;
    }

    void dump() const;
    // Added by Michael
    bool operator==(PrePost &other) {
    	if (cond.size() != other.cond.size()) return false;
    	if (var != other.var || pre != other.pre || post != other.post) return false;
//    	::sort(cond.begin(), cond.end());
//    	::sort(other.cond.begin(), other.cond.end());
    	for (int i = 0; i < cond.size(); i++)
    		if (cond[i] != other.cond[i]) return false;

    	return true;
    }
    bool operator<(const PrePost &other) const {
    	//TODO: include condition in the check
    	return (var < other.var || (var == other.var && post < other.post)
    			|| (var == other.var && post == other.post && pre < other.pre));
    }

    void dump_SAS(ofstream& os) const {
      	os << cond.size() << endl;
    	for(int i = 0; i < cond.size(); i++){
    		cond[i].dump_SAS(os);
    	}
    	os << var << " " << pre << " " << post << endl;
    }


};

class Operator {
    bool is_an_axiom;
    std::vector<Prevail> prevail;      // var, val
    std::vector<PrePost> pre_post;     // var, old-val, new-val, effect conditions
    std::string name;
    int cost;
    int index;          				// Added by Michael
    double d_cost;       				// Added by Michael

    mutable bool marked; // Used for short-term marking of preferred operators
public:
    Operator(std::istream &in, bool is_axiom);
    void dump() const;
    std::string get_name() const {return name;}

    bool is_axiom() const {return is_an_axiom;}

    const std::vector<Prevail> &get_prevail() const {return prevail;}
    const std::vector<PrePost> &get_pre_post() const {return pre_post;}

    bool is_applicable(const State &state) const {
	for(int i = 0; i < prevail.size(); i++)
	    if(!prevail[i].is_applicable(state))
		return false;
	for(int i = 0; i < pre_post.size(); i++)
	    if(!pre_post[i].is_applicable(state))
		return false;
	return true;
    }

    bool is_marked() const {
        return marked;
    }
    void mark() const {
        marked = true;
    }
    void unmark() const {
        marked = false;
    }

    mutable bool marker1, marker2; // HACK! HACK!

    int get_cost() const {return cost;}

    // Added by Michael
    Operator(bool is_axiom,
    		 std::vector<Prevail> prv,
    		 std::vector<PrePost> pre,
    		 std::string nm,
    		 double c)
    {
    	marked = false;
    	is_an_axiom = is_axiom;
        prevail = prv;
        pre_post = pre;
        name = nm;
        d_cost = c;
        sort_and_remove_duplicates();
        marker1 = marker2 = false;
    }

    double get_double_cost() const {return d_cost;}
    void set_double_cost(double c) {d_cost=c;}
    int get_index() const {return index;}
    void set_index(int ind) {index = ind;}

    bool is_prevailed_by(int v) const {
    	for(int i = 0; i < prevail.size(); i++)
    		if (prevail[i].var == v) return true;
    	return false;
    }

    bool is_affecting(int v) const {
    	for(int i = 0; i < pre_post.size(); i++)
    		if (pre_post[i].var == v) return true;
    	return false;
    }

    int get_prevail_val(int v) const {
    	return get_value_for_var(v, prevail);
//    	for(int i = 0; i < prevail.size(); i++)
//    		if (prevail[i].var == v) return prevail[i].prev;
//    	return -1;
    }

    void get_pre_post(int v, vector<PrePost>& pp) const {
    	assert(pp.size() == 0);
    	for(int i = 0; i < pre_post.size(); i++)
    		if (pre_post[i].var == v)
    			pp.push_back(pre_post[i]);
    }

    //TODO: find all places where it is used and replace with get_pre_post
    int get_pre_val(int v) const {
    	for(int i = 0; i < pre_post.size(); i++)
    		if (pre_post[i].var == v) return pre_post[i].pre;
    	return -1;
    }

    //TODO: find all places where it is used and replace with get_pre_post
    int get_post_val(int v) const {
    	for(int i = 0; i < pre_post.size(); i++)
    		if (pre_post[i].var == v) return pre_post[i].post;
    	return -1;
    }

    void get_explicit_pre_post_for_var(int v, vector<PrePost>& pp) const {
    	// Creates explicit preposts by expanding the condition using prevail and other
    	// preconditions and updating the respective pre value.
    	assert(pp.size() == 0);
    	vector<Prevail> new_prev = prevail;

    	vector<PrePost> pptemp;
    	for(int i = 0; i < pre_post.size(); i++) {
    		if (pre_post[i].var == v) {
    			pptemp.push_back(pre_post[i]);
    		} else {
        		if (pre_post[i].pre != -1)
        			new_prev.push_back(Prevail(pre_post[i].var, pre_post[i].pre));
    		}
    	}
    	// sorting and removing duplicates
        ::sort(new_prev.begin(), new_prev.end());
    	for(int i = new_prev.size() - 1; i > 0; i--){
    		if (new_prev[i] == new_prev[i-1])
    			new_prev.erase(new_prev.begin()+i);
    	}
    	// going over all relevant conditional effects and creating an explicit effect for each
    	for(int i = 0; i < pptemp.size(); i++) {
        	int pre = pptemp[i].pre;
        	vector<Prevail> new_cond = new_prev;
        	// going over the condition, pushing it into new condition, updating pre, if at all
        	for(int j = 0; j < pptemp[i].cond.size(); j++) {
        		if (pptemp[i].cond[j].var == v) {
        			assert(-1 == pre || pre == pptemp[i].cond[j].prev);
        			pre = pptemp[i].cond[j].prev;
        		} else {
        			new_cond.push_back(pptemp[i].cond[j]);
        		}
        	}
        	// sorting and removing duplicates
            ::sort(new_cond.begin(), new_cond.end());
        	for(int j = new_cond.size() - 1; j > 0; j--){
        		if (new_cond[j] == new_cond[j-1])
        			new_cond.erase(new_cond.begin()+j);
        	}
        	pp.push_back(PrePost(v,pre,pptemp[i].post,new_cond));
    	}
    }


    bool is_redundant() const {
    	for (int i=0;i<pre_post.size();i++) {
    		if (pre_post[i].pre != pre_post[i].post)
    			return false;
    	}
    	return (0==pre_post.size());
    }

    void sort_and_remove_duplicates() {
        ::sort(pre_post.begin(), pre_post.end());
    	for(int i = pre_post.size() - 1; i > 0; i--){
    		if (pre_post[i] == pre_post[i-1])
    			pre_post.erase(pre_post.begin()+i);
    	}
    }

    void dump_SAS(ofstream& os) const {
    	if (is_an_axiom) {
    		os << "begin_rule" << endl;
    		pre_post[0].dump_SAS(os);
    		os << "end_rule" << endl;
    	} else {
    		os << "begin_operator" << endl;
    	   	os << name << endl;
    	   	os << prevail.size() << endl;
        	for(int i = 0; i < prevail.size(); i++){
        		prevail[i].dump_SAS(os);
        	}
    	   	os << pre_post.size() << endl;
        	for(int i = 0; i < pre_post.size(); i++){
        		pre_post[i].dump_SAS(os);
        	}

        	if (g_use_metric)
        		os << (int) d_cost << endl;
        	else
        		os << "0" << endl;

    		os << "end_operator" << endl;
    	}
    }

    bool is_single_effect() { return (pre_post.size() == 1);}

    void simplify_single_effect() {
    	assert(pre_post.size() == 1);
    	if (pre_post[0].cond.size() == 0)    // Nothing to do
    		return;
    	// Moving condition into pre and prevail
		int var = pre_post[0].var;
		int new_pre = pre_post[0].pre;
		int post = pre_post[0].post;

    	vector<Prevail> emp_cond, cond = pre_post[0].cond;
		for (int c=0; c < cond.size(); c++) {
			if (var == cond[c].var) {
				assert(new_pre == -1 || new_pre == cond[c].prev);
				new_pre = cond[c].prev;
				continue;
			}
			int new_prev = get_value_for_var(cond[c].var, prevail);
			if (-1 == new_prev) {
				// Adding condition to prevail
				prevail.push_back(cond[c]);
				continue;
			}
			assert(new_prev == cond[c].prev);
		}
		// Setting the new prepost
		pre_post.pop_back();
		pre_post.push_back(PrePost(var, new_pre, post, emp_cond));
    }

};


#endif
