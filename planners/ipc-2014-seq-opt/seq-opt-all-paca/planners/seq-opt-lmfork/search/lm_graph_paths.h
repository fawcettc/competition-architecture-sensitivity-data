#ifndef LM_GRAPH_PATHS_H
#define LM_GRAPH_PATHS_H


#include "landmarks/landmarks_graph.h"
#include "landmarks/landmark_status_manager.h"

class LmGraphPaths {

	hash_map<int, LandmarkNode*> landmarks_by_id;
	vector<LandmarkNode*> initially_no_edges;  // Landmarks with no edges
	vector<int> sorted_nodes;
	hash_map<int, vector<int> > to_nodes;
	vector<int> land_color;
	vector<int> source_nodes, target_nodes;

protected:
	void update_marking();
	bool is_max_child(int node_id, int child_id) const;
	void erase_link(int node_id, int to_index);
	void erase_node(int node_id);
	int get_max_node() const;

	LandmarkNode* extract_max_child_node(int node_id);
	void remove_shortcuts(vector<LandmarkNode*>& path);
	void get_edgeless_nodes(vector<LandmarkNode*>& nodes);

public:
	LmGraphPaths(LandmarksGraph& lgraph);
	~LmGraphPaths();

	int get_marking(int node) const {return land_color[node];}
	LandmarkNode* get_landmark_by_id(int node) const;
	int get_num_paths() const;
	bool is_edgeless() const;

	void get_singleton_nodes(vector<LandmarkNode*>& nodes);
	virtual void extract_longest_path(vector<LandmarkNode*>& path) = 0;
	void dump();

};


class LmGraphPathsLinks: public LmGraphPaths {

public:
	LmGraphPathsLinks(LandmarksGraph& lgraph);
	~LmGraphPathsLinks();

	virtual void extract_longest_path(vector<LandmarkNode*>& path);

};

class LmGraphPathsNodes: public LmGraphPaths {

public:
	LmGraphPathsNodes(LandmarksGraph& lgraph);
	~LmGraphPathsNodes();

	virtual void extract_longest_path(vector<LandmarkNode*>& path);

};

#endif
