;; EACH of these commands must be evaluated separately
;; NOTE: If using SLIME, put your cursor in each top-level-form and hold down "C-M-x" (ctrl, meta, and x)

(asdf:load-system 'make-executable)

(asdf:load-system 'pddl-feature-extractor)

(make-executable::make-executable-from-asdf 
 "pddl-feature-extractor.exe" 
 "~/MERS-GIT/mars-toolkit/PDDL/pddl-feature-extractor/bin/linux64/"
 'pddl-feature-extractor
 'pddl-feature-extractor::feature-extractor-main
 :debug-build nil
 :init-file-pathname "~/clinit.cl")
