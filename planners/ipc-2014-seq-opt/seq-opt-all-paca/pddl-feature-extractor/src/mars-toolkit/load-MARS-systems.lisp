(in-package :CL-USER)

;;; !!!BEFORE LOADING THIS FILE!!!
;;; You need to specify the location of the Mars Toolkit root directory, 
;;; as described below, under SETUP.   
;;; !!!BEFORE LOADING THIS FILE!!!

;;; *************************************************************************
;;; ***   Loading the Mars Toolkit   ***
;;; *************************************************************************

;;; This file defines some convenience functions and loads the system 
;;; definitions for the (sub)systems that comprise the Mars Toolkit. 
;;; Each system definition is in the subdirectory that contains the 
;;; source files for that system. Current we use both Franz .system
;;; definitions, and .asdf definitions, the later for portability.

;;; See the file README in the Mars Toolkit (MTK) root directory for more details.

;;; SECTION A) Defines three convenience functions for manipulating
;;;    files relative to the MTK root directory.

;;; o (mars-pathname FILENAME)
;;;     Returns the absolute pathname for FILENAME 
;;;     relative to the Mars Toolkit root directory.

;;; o (mars-load FILENAME)
;;;     Loads FILENAME relative to the Mars Toolkit root directory.

;;; o (cd-mars)
;;;     Sets the default path to the Mars Toolkit root directory.

;;; o (mars-system-load FILENAME)
;;;    Loads definition for, SYSTEM, defined by FILE-NAME 
;;;    with in the Mars Toolkit directory.

;;; SECTION B) Loads ASDF2 and automatically loads all .ASD files 
;;;    recursively contained within the MTK root directory. 

;;; SECTION C) Defines one convenience function for loading
;;;    Franz .system files, and automatically loads all .ASD files
;;;    recursively contained within the MTK root directory.

;;; The system definitions currently loaded include the following:

;;; o utilities:              :mtk-utils, :iota, :graphs, :causal-ordering
;;;                           :xml-utils
;;; o constraint solvers:     :opsat, :isat, :bfig, :itc
;;; o languages:              :MPL, :RMPL
;;; o automata objects:       :CCA
;;; o mode estimators:        :Sleuth, :MiniMe
;;; o planners:               :Burton
;;; o executives:             :TPN (Kirk), :Drake
;;; o Examples:               :mpl-examples, :isairas-spacecraft, :newmaap,
;;;                           :rax, :techsat21, and :ds4.


;;; *************************************************************************
;;; ***   Setup                                                           ***
;;; *************************************************************************

;;; Before loading the Mars Toolkit System definitions, you should bind
;;; the variable cl-user::*mars-toolkit-pathname* to
;;; the pathname for your MARS toolkit top-level directory,
;;; and define the logical pathname host mtk: as this directory.

;;; This can be accomplished by adding an expression like the
;;; following to your clinit.cl file; this file should reside 
;;; within your home directory:

;;;(eval-when (compile load eval)
;;;  (defconstant *mars-toolkit-pathname*
;;;  ;;   *** UPDATE THE BINDING OF THIS VARIABLE TO YOUR MTK ROOT DIRECTORY ***
;;;	  "~/Code/mers_SVN/src/Mers/Mars toolkit"
;;;	"Location of the Mars Toolkit top-level directory.")

;;; *************************************************************************
;;; ***  A) Support Functions for loading Mars Toolkit System Definitions ***
;;; *************************************************************************

;; NOTE: The naming convention used in the Mars Toolkit is not perfectly portable.
;;       To be portable across both different implementations of common lisp
;;       and across different operating systems, the following needs to be true:
;;       -- All files and folders needs to have lower case names.
;;          (some lisp implementations auto downcase logical pathnames)
;;       -- a file name can only use two dots. i.e. "filename.type" is legal.
;;          "file.name.type" is NOT legal, and cannot be reliably resolved
;;          using logical pathnames. 
;;          (some lisp implementations interpret the word following the second
;;           dot to be the version number, and will signal an error when it is not)

;; The following methods keep all names as physical pathnames. That means, these
;; pathnames are never interpreted as logical pathnames, but instead, are parsed
;; using the customs of the running operating system.

;; NOTE: Almost all LISP implementations use some CLOS object, called PATHNAME,
;; to store pathnames. Just because (type-of #P"c:/folder/filename.type") returns
;; PATHNAME, does not mean the pathname is logical or physical. The string is
;; either parsed as a logical or physical pathname. Then the appropriate slots
;; of the PATHNAME object are filled.

(eval-when (:compile-toplevel :load-toplevel :execute)

  (if (not (symbolp '*mars-toolkit-pathname*))
	  (error "The variable CL-USER::*mars-toolkit-pathname* must be set to the root directory of your Mars Toolkit."))
  
  ;; Specify MTK as the HOST of a logical pathname
  ;; that points to *mars-toolkit-pathname*.
  (setf (logical-pathname-translations "mtk")
		`(("**;*.*.*", (format nil "~a**/*.*" *mars-toolkit-pathname*))
          ("**;*.*", (format nil "~a**/*.*" *mars-toolkit-pathname*))))

  (defun mars-pathname (filename)
	"Returns the absolute pathname for FILE-NAME 
     relative to the Mars Toolkit root directory."
	(merge-pathnames filename (translate-logical-pathname "mtk:")))

  (defun mars-load (filename)
	"Loads FILENAME relative to the Mars Toolkit root directory."
 	(load (merge-pathnames filename (translate-logical-pathname "mtk:"))))
  
  (defun cd-mars ()
	"Sets the default path to the Mars Toolkit root directory."
	(setq cl-user::*default-pathname-defaults* 
		  (translate-logical-pathname "mtk:")))

  (export '(mars-pathname mars-load cd-mars))

  (cd-mars)
  )

;;; *************************************************************************
;;; ***  B) ASDF Loads
;;; *************************************************************************

;;Ensure ASDF2 is loaded

;; First, try loading ASDF bundled with Lisp distro. If that fails (or
;; is too old), fall back to MTK bundled ASDF.
(handler-case (require "asdf")
  (program-error () nil))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (let ((asdf-version (or #+asdf2 (asdf:asdf-version) #+asdf :old)))
  	;; if there is no asdf installed, load asdf2.
  	(when (null asdf-version) (load "mtk:asdf.lisp"))
  	;; if the version is old (from before 1.635), load asdf2.
  	(when (eq asdf-version :old) (load "mtk:asdf.lisp"))
  	;; some version of asdf2 is probably installed, so leave it.
  	)
)

;; It is no longer necessary to load individual ASDF files, 
;; all ".asd" files within the mars toolkit are loaded. 
;; Use of "asdf:*central-registry*" should no longer be used 
;; within the mars toolkit.

;; Appends a source-registry entry to the existing entry.
(defun append-source-registry (parameter)
  "Appends a new source registry onto any existing registry.
   Works for ASDF 2.015-?(currently, 2.020)."
  ;; This function is almost identical to asdf's internal
  ;; initialize-source-registry function, except that it does
  ;; NOT overwrite the previous *source-registry* hash-table
  ;; with a new hash-table.
  ;; The function compute-source-registry will take care of merging
  ;; the parameter provided here into the existing *source-registry*.
  (cond ((not (asdf::source-registry-initialized-p))
		 (asdf:ensure-source-registry parameter))
		(:otherwise (asdf:compute-source-registry parameter)))
)

;; The following call ensures that there is a source-registry loaded.

(if (and (boundp '*use-ros?*)
		 *use-ros?*
		 (excl.osi:getenv "ROS_PACKAGE_PATH"))
	;; We've requested that ROS be loaded. Add the roslisp core
	;; libraries to ASDF in addition to the mars toolkit and load the
	;; ros-load-manifest package to finish ROS setup.
	(progn
	  (append-source-registry 
	   `(:source-registry (:tree ,*ros-lisp-home*) (:tree,(translate-logical-pathname "mtk:")) :inherit-configuration))
	  (asdf:load-system :ros-load-manifest))
	;; Load mars toolkit like normal.
	(append-source-registry 
	 `(:source-registry (:tree,(translate-logical-pathname "mtk:")) :inherit-configuration)))


;; There are many options as to where the FASL files can be cached.
;; The following line of code caches the FASL files with their source files.
;; Comment the line below to cache the FASL files in their "default"
;; location, which is NOT with the source.
(asdf:disable-output-translations)


;;; *************************************************************************
;;; ***   C) Loading Mars Toolkit System Definitions of Interest          ***
;;; *************************************************************************

(load "mtk:check-foreign-libraries.lisp")

;;; *************************************************************************
;;; ***   D) Message to User about compiling in the Toolkit directory     ***
;;; *************************************************************************

(format t "~2%  ***   The Mars Toolkit (MTK) Systems are in: ~
           ~%     ~a" *mars-toolkit-pathname*)
