#!/bin/bash

##################################################################
# Canonical code below from:
# http://blog.publicobject.com/2006/06/canonical-path-of-file-in-bash.html
# posted by "Ferry"

# Absolute path to this script. /home/user/bin/foo.sh
SCRIPT=$(readlink -f $0)
# Absolute path this script is in. /home/user/bin
SCRIPTPATH=`dirname $SCRIPT`

function path-canonical-simple {
  local dst="${1}"
  cd -P -- "$(dirname -- "${dst}")" &> /dev/null && echo "$(pwd -P)/$(basename -- "${dst}")"
}

function path-canonical {
  cd $SCRIPTPATH
  local dst="$(path-canonical-simple "${1}")"
  while [[ -h "${dst}" ]]; do
  local linkDst="$(ls -l "${dst}" | sed -r -e 's/^.*[[:space:]]*->[[:space:]]*(.*)[[:space:]]*$/\1/g')"
  if [[ -z "$(echo "${linkDst}" | grep -E '^/')" ]]; then
  # relative link destination
  linkDst="$(dirname "${dst}")/${linkDst}"
  fi
  dst="$(path-canonical-simple "${linkDst}")"
  done
  echo "${dst}"
}

##################################################################

cd $SCRIPTPATH
TRANSLATE=$(path-canonical "./translate/translate.py")

"$TRANSLATE" "$1" "$2"
