;; TO BUILD AN EXECUTABLE (using AllegroCL):
;; 1.) load the package to which this deliver.lisp file belongs
;;     >> (asdf:load-system 'pddl-feature-extractor)
;; 3.) Decide which function you want to execute below, and modify its parameters accordingly.
;; 4.) Save this file.
;; 5.) Load this file.
;;     * If you are using emacs with slime: Open this file in emacs. 
;;       Make sure it is selected by clicking on it (active). 
;;       Then use the keyboard commands  Ctrl-c, Ctrl-k to compile the file.
;;     * If on a REPL:
;;       >> (load (mars-pathname "PDDL/pddl-feature-extractor/deliver.lisp"))
;; 6.) On the REPL, evaluate the function you are interested in. i.e.:
;;     >> (pddl-feature-extractor::deliver-win)
;;     >> (pddl-feature-extractor::deliver-source)
;; Note: There is no cross-compilation capability. 
;;   (deliver-win) must be evaluated within common-lisp on a windows machine.
;;   (deliver-linux64) must be evaluated within common-lisp on a linux 64 bit machine.


;; GENERIC INSTRUCTIONS FOR DELIVER.LISP
;; TO BUILD AN EXECUTABLE (using AllegroCL):
;; 1.) load the package to which this deliver.lisp file belongs
;;     >> (asdf:load-system 'this-package-name)
;; 3.) Decide which function you want to execute below, and modify its parameters accordingly.
;; 4.) Save this file.
;; 5.) Load this file.
;;     * If you are using emacs with slime: Open this file in emacs. 
;;       Make sure it is selected by clicking on it (active). 
;;       Then use the keyboard commands  Ctrl-c, Ctrl-k to compile the file.
;;     * If on a REPL:
;;       >> (load "String with the absolute path to this file.lisp")
;; 6.) On the REPL, evaluate the function you are interested in. i.e.:
;;     >> (this-package-name::deliver-win)
;;     >> (this-package-name::deliver-source)
;; Note: There is no cross-compilation capability. 
;;   (deliver-win) must be evaluated within common-lisp on a windows machine.
;;   (deliver-linux64) must be evaluated within common-lisp on a linux 64 bit machine.

(in-package #:pddl-feature-extractor)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (asdf:load-system 'make-executable))

(defun deliver-win ()
  (let (;; PARAMETERS YOU SHOULD SET!
		(;; The name of the executable you wish to build.
		 exec-name "pddl-feature-extractor.exe")
		(;;	The path where all the of the binaries should be put.
		 exec-path (mars-pathname "PDDL/pddl-feature-extractor/bin/windows/"))
		(;; Path to your common lisp init file - the file that is loaded and executed 
		 ;; automatically by your running lisp instance.
		 init-path "C:/Users/David Wang/clinit.cl")
		;; PARAMETERS YOU SHOULD NOT TOUCH!
		(closopt-fasl (mars-pathname "PDDL/pddl-feature-extractor/closopt.fasl")) )
	
	;; Build the executable
	(make-executable:make-executable-from-asdf 
	 exec-name
	 exec-path
	 'pddl-feature-extractor
	 'pddl-feature-extractor::feature-extractor-main
	 :debug-build nil
	 :init-file-pathname init-path
	 :extra-input-files (if (probe-file closopt-fasl) (list closopt-fasl) '())
	 :lisp-heap-start "1000m"
	 :aclmalloc-heap-start "4000m"
	 ;; :lisp-heap-start "1000m"
	 ;; :lisp-heap-size "4000m"
	 ;; :aclmalloc-heap-start "4000m"
	 ;; :aclmalloc-heap-size "2000m"
	)
	))

(defun deliver-linux64 ()
  (let (;; PARAMETERS YOU SHOULD SET!
		(;;The name of the executable you wish to build.
		 exec-name "pddl-feature-extractor.exe" )
		(;; The path where all the of the binaries should be put.
		 exec-path (mars-pathname "PDDL/pddl-feature-extractor/bin/linux64/"))
		(;; "Path to your common lisp init file - 
		 ;; the file that is loaded and executed 
		 ;; automatically by your running lisp instance.
		 init-path "~/clinit.cl")
		;; PARAMETERS YOU SHOULD NOT TOUCH!
		(closopt-fasl (mars-pathname "PDDL/pddl-feature-extractor/closopt.fasl")) )

	;; Build the executable
	(make-executable:make-executable-from-asdf 
	 exec-name
	 exec-path
	 'pddl-feature-extractor
	 'pddl-feature-extractor::feature-extractor-main
	 :debug-build nil
	 :init-file-pathname init-path
	 :extra-input-files (if (probe-file closopt-fasl) (list :trace :constructor :foreign closopt-fasl) '())
	 ;; :lisp-heap-start "1000m"
	 :lisp-heap-size "4000m"
	 ;; :aclmalloc-heap-start "4000m"
	 ;; :aclmalloc-heap-size "2000m"
	)))

(defun train ()
  (let* ((closopt-lisp (mars-pathname "PDDL/pddl-feature-extractor/closopt.cl"))
		 (closopt-fasl (merge-pathnames (make-pathname :type "fasl") closopt-lisp))
		 (packages '(:CL-USER :LISP :PARSE-NUMBER :MTK-UTILS :MTK-UTILS-INFINITY :NAMING-UTILS :CLOS-UTILS
					:ABSTRACT-DATA-TYPES :CL-PPCRE :PDDL :CSV-PARSER :ALEXANDRIA
					:BORDEAUX-THREADS :CL-FAD :CL-UTILITIES :SAS :PDDL-FEATURE-EXTRACTOR)))
	(with-open-file 
		(st closopt-lisp :direction :output :if-exists :supersede)
	  (format st "(excl::preload-forms)~%")
	  (format st "(excl::preload-constructors ~s)~%" (append '(list) packages))
	  (format st "(excl::precache-generic-functions ~s)" (append '(list) packages)))
	(test-feature-extractor-satellite-with-sas-to-file)
	(compile-file closopt-lisp)
	))

(defun deliver-source ()
  (let (;; PARAMETERS YOU SHOULD SET!
		(;; The path where all of the source files should be copied to.
		 src-path (mars-pathname "PDDL/pddl-feature-extractor/src/"))
		(;;Path to your common lisp init file -
		 ;; the file that is loaded and executed 
		 ;; automatically by your running lisp instance.
		 init-path #+linux "~/clinit.cl"
				   #+mswindows "C:/Users/David Wang/clinit.cl"))

	;; Build the executable
	(make-executable:make-source-folder src-path 'pddl-feature-extractor :include-make-executable t :verbose t)
	;; copy the deliver file
	(make-executable:copy-file-to-source-folder (mars-pathname "PDDL/pddl-feature-extractor/deliver.lisp") src-path)
	;; copy the translate folder and translate script
	(make-executable:copy-file-to-source-folder (mars-pathname "PDDL/pddl-feature-extractor/bin/linux64/pddl-to-sas-translate.sh") src-path)
	(make-executable:copy-folder (mars-pathname "PDDL/pddl-feature-extractor/bin/linux64/translate/") 
								 (merge-pathnames "mars-toolkit/PDDL/pddl-feature-extractor/bin/linux64/" src-path)
								 :contents-only nil)
	;; copy the clinit.cl file
	(make-executable:copy-file-to-source-folder init-path src-path :keep-pathname-function #'make-executable:default-keep-pathname)
	;; copy the load-MARS-systems.lisp file
	(make-executable:copy-file-to-source-folder (mars-pathname "load-MARS-systems.lisp") src-path)
	))





;; This is convenience function, intended to be run manually to get a 
;; quick a dirty listing of all the packages your system depends on.
(defun get-package-names (asdf-system-name)
  "Try to get the names of packages, assuming they are named identically to their asdf systems."
  (let* ((poss-pkg-names (mapcar (lambda (system) (intern (string-upcase (asdf::component-name system)) :keyword)) 
								 (make-executable:get-all-systems asdf-system-name))))
	(mapcan #'(lambda (package-name) 
				(if (find-package package-name)
					(progn
					  (list package-name))
					(progn
					  (format t "~a is NOT a package.~%" package-name)
					  '())
					)) 
			poss-pkg-names)))
