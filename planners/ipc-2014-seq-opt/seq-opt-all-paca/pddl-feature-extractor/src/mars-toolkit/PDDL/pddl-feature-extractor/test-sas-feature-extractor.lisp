(in-package #:pddl-feature-extractor)

#+linux
(defun test-pddl-to-sas-translator ()
  (let ((pddl-domain-pathname (mars-pathname "PDDL/tests/logistics00/domain.pddl") )
		(pddl-problem-pathname (mars-pathname "PDDL/tests/logistics00/probLOGISTICS-10-0.pddl"))
		(sas-translator-path (mars-pathname "PDDL/pddl-feature-extractor/bin/linux64/pddl-to-sas-translate.sh")))
	(pddl-to-sas-translator sas-translator-path pddl-domain-pathname pddl-problem-pathname :verbose t)
	))

#+linux
(defun test-extract-sas-features-from-pddl ()
  (let ((pddl-domain-pathname (mars-pathname "PDDL/tests/logistics00/domain.pddl") )
		(pddl-problem-pathname (mars-pathname "PDDL/tests/logistics00/probLOGISTICS-10-0.pddl"))
		(sas-translator-path (mars-pathname "PDDL/pddl-feature-extractor/bin/linux64/pddl-to-sas-translate.sh")))
	(extract-sas-features-from-pddl sas-translator-path pddl-domain-pathname pddl-problem-pathname)
	))

(defun test-extract-sas-features-from-large-scanalyzer ()
  (let ((sas-path (mars-pathname "PDDL/pddl-feature-extractor/tests/output-scanalyzer-p20.sas")))
	(extract-sas-features sas-path)
	))
