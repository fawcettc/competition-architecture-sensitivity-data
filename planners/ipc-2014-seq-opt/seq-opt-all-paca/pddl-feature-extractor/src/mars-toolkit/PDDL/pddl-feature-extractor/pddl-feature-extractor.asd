;;;; Copyright (c) 2013 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   David C. Wang

(in-package :cl-user)

(asdf:defsystem #:pddl-feature-extractor
  :name "PDDL Feature Extractor"
  :version "0.1"
  :description "MERS Toolkit: PDDL"
  :author "David Wang"
  :maintainer "MIT MERS Group"
  :components ((:file "package")
			   (:file "utils"
					  :depends-on ("package"))

			   (:file "sas-feature-extractor"
					  :depends-on ("package" "utils"))
			   (:file "test-sas-feature-extractor"
					  :depends-on ("package" "sas-feature-extractor"))

			   (:file "pddl-feature-extractor"
					  :depends-on ("package" "utils"))
			   (:file "test-pddl-feature-extractor"
					  :depends-on ("package" "pddl-feature-extractor"))
			   )
  
  :depends-on (#:mtk-utils #:mtk-utils-infinity #:pddl #:csv-parser #:abstract-data-types #:cl-fad #:parse-number #:cl-ppcre #:sas))


