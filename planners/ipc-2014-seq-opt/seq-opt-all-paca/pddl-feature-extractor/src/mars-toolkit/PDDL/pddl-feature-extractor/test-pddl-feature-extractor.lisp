;;;; Copyright (c) 2013 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   David C. Wang

(in-package #:pddl-feature-extractor)

(defun test-feature-extractor-domain-problem-output ()
  (let ((pddl-domain (cl-user::mars-pathname "PDDL/tests/logistics00/domain.pddl") )
		(pddl-problem (cl-user::mars-pathname "PDDL/tests/logistics00/probLOGISTICS-10-0.pddl")))
	(feature-extractor (list pddl-domain pddl-problem))
	))

(defun test-feature-extractor-csv-problems-output ()
  (let ((pddl-csv-problems (cl-user::mars-pathname "PDDL/pddl-feature-extractor/tests/pddl-problems.csv") ))
	(feature-extractor (list pddl-csv-problems))
	))

(defun test-feature-extractor-domain-problem-file ()
  (let ((pddl-domain (cl-user::mars-pathname "PDDL/tests/logistics00/domain.pddl") )
		(pddl-problem (cl-user::mars-pathname "PDDL/tests/logistics00/probLOGISTICS-10-0.pddl"))
		(csv-output (cl-user::mars-pathname "PDDL/pddl-feature-extractor/tests/csv-output.csv")) )
	(feature-extractor (list pddl-domain pddl-problem) :output csv-output)
	))

(defun test-feature-extractor-csv-problems-file ()
  (let ((pddl-csv-problems (cl-user::mars-pathname "PDDL/pddl-feature-extractor/tests/pddl-problems.csv") )
		(csv-output (cl-user::mars-pathname "PDDL/pddl-feature-extractor/tests/csv-output.csv")) )
	(feature-extractor (list pddl-csv-problems) :output csv-output)
	))

(defun test-feature-extractor-crewplanning-output ()
  (let ((pddl-domain (cl-user::mars-pathname "PDDL/tests/ipc2008-crewplanning-tempo-sat/p01-domain.pddl") )
		(pddl-problem (cl-user::mars-pathname "PDDL/tests/ipc2008-crewplanning-tempo-sat/p01.pddl")) )
	(feature-extractor (list pddl-domain pddl-problem))
	))

(defun test-feature-extractor-crewplanning-file ()
  (let ((pddl-domain (cl-user::mars-pathname "PDDL/tests/ipc2008-crewplanning-tempo-sat/p01-domain.pddl") )
		(pddl-problem (cl-user::mars-pathname "PDDL/tests/ipc2008-crewplanning-tempo-sat/p01.pddl"))
		(csv-output (cl-user::mars-pathname "PDDL/pddl-feature-extractor/tests/csv-output.csv")))
	(feature-extractor (list pddl-domain pddl-problem) :output csv-output)
	))

(defun test-feature-extractor-trucks-strips-file ()
  (let ((pddl-domain (cl-user::mars-pathname "PDDL/tests/trucks-strips/p08-domain.pddl") )
		(pddl-problem (cl-user::mars-pathname "PDDL/tests/trucks-strips/p08.pddl"))
		(csv-output (cl-user::mars-pathname "PDDL/pddl-feature-extractor/tests/csv-output.csv")))
	(feature-extractor (list pddl-domain pddl-problem) :output csv-output)
	))

(defun test-feature-extractor-logistics-with-sas-to-stream ()
  (let ((pddl-domain-pathname (mars-pathname "PDDL/tests/logistics00/domain.pddl") )
		(pddl-problem-pathname (mars-pathname "PDDL/tests/logistics00/probLOGISTICS-10-0.pddl"))
		#+linux
		(sas-translator-path (mars-pathname "PDDL/pddl-feature-extractor/bin/linux64/pddl-to-sas-translate.sh")) )
	(feature-extractor (list pddl-domain-pathname pddl-problem-pathname) 
					   #+linux :sas-translator-path #+linux sas-translator-path)
	))

(defun test-feature-extractor-logistics-with-sas-to-file ()
  (let ((pddl-domain-pathname (mars-pathname "PDDL/tests/logistics00/domain.pddl") )
		(pddl-problem-pathname (mars-pathname "PDDL/tests/logistics00/probLOGISTICS-10-0.pddl"))
		(csv-output (cl-user::mars-pathname "PDDL/pddl-feature-extractor/tests/csv-output.csv"))
		#+linux
		(sas-translator-path (mars-pathname "PDDL/pddl-feature-extractor/bin/linux64/pddl-to-sas-translate.sh")) )
	(feature-extractor (list pddl-domain-pathname pddl-problem-pathname) 
					   :output csv-output
					   #+linux :sas-translator-path #+linux sas-translator-path)
	))

(defun test-feature-extractor-satellite-with-sas-to-file ()
  (let ((problems-csv-pathname (mars-pathname "PDDL/pddl-feature-extractor/tests/ipc2011-satellite/satellite-problems-subset.csv") )
		(csv-output (cl-user::mars-pathname "PDDL/pddl-feature-extractor/tests/ipc2011-satellite/satellite-features.csv"))
		#+linux
		(sas-translator-path (mars-pathname "PDDL/pddl-feature-extractor/bin/linux64/pddl-to-sas-translate.sh")) )
	(feature-extractor (list problems-csv-pathname) 
					   :output csv-output
					   #+linux :sas-translator-path #+linux sas-translator-path)
	))


(defun test-feature-extractor-storage-with-sas-to-stream ()
  (let ((pddl-domain-pathname (mars-pathname "PDDL/pddl-feature-extractor/tests/ipc2011-storage/domain/p01-domain.pddl") )
		(pddl-problem-pathname (mars-pathname "PDDL/pddl-feature-extractor/tests/ipc2011-storage/problems/p01.pddl"))
		#+linux
		(sas-translator-path (mars-pathname "PDDL/pddl-feature-extractor/bin/linux64/pddl-to-sas-translate.sh")) )
	(feature-extractor (list pddl-domain-pathname pddl-problem-pathname) 
					   #+linux :sas-translator-path #+linux sas-translator-path)
	))
