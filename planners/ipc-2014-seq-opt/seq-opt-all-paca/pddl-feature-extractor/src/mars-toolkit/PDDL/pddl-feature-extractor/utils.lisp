(in-package #:pddl-feature-extractor)

(defun stat-feature-names (base-feature-name)
  (let ((base-feature-string (string-upcase (symbol-name base-feature-name))))
	(list
	 (intern (concatenate 'string "MIN-" base-feature-string) :keyword)
	 (intern (concatenate 'string "MAX-" base-feature-string) :keyword)
	 (intern (concatenate 'string "AVG-" base-feature-string) :keyword)
	 (intern (concatenate 'string "STDEV-"  base-feature-string) :keyword))
   ))

(defun remove-all (original-list &rest remove-lists)
  (let ((copy (copy-list original-list)))
	(dolist (rm-list remove-lists)
	  (cond (;; remove element element if it is a list
			 (listp rm-list)
			 (dolist (rm-elem rm-list)
			   (setf copy (delete rm-elem copy)))) 
			(;; remove the single element if it is a symbol
			 :otherwise
			 (setf copy (delete rm-list copy)))
			)
	  )
	copy))

(defun add-stat-features (fmap feature-name value-list &key (default-value -1))
  ;; if there is nothing on the VALUE-LIST, we add the default-value
  ;; to the VALUE-LIST to make the calls to #'max, #'min, etc... happy.
  (unless value-list
	(setf value-list (list default-value)))
  ;; compute statistics
  (let ((min-name (pddl::get-keyword-symbol "MIN-" (symbol-name feature-name)))
		(max-name (pddl::get-keyword-symbol "MAX-" (symbol-name feature-name)))
		(avg-name (pddl::get-keyword-symbol "AVG-" (symbol-name feature-name)))
		(stdev-name (pddl::get-keyword-symbol "STDEV-" (symbol-name feature-name))))
	(map-add! fmap min-name (apply #'min value-list))
	(map-add! fmap max-name (apply #'max value-list))
	(map-add! fmap avg-name (funcall #'arithmetic-average value-list))
	(map-add! fmap stdev-name (funcall #'standard-deviation value-list))
	))

(defun arithmetic-average (samples)
  (/ (reduce #'+ samples)
     (length samples)))

(defun standard-deviation (samples)
  (let ((mean (arithmetic-average samples)))
    (sqrt (* (/ 1.0d0 (length samples))
             (reduce #'+ samples
                     :key (lambda (x)
                            (expt (- x mean) 2)))))))

(defun split-pathname (string)
  (delete-if (lambda (x) (= (length (string-trim " " x)) 0))  (split-pathname-r string)))
 
(defun split-pathname-r (string &optional (r nil))
  (let ((n (position-if (lambda (x) (or (string= x "\\") (string= x "/"))) string
					 :from-end t)))
	(if n
		(split-pathname-r (subseq string 0 n) (cons (subseq string (1+ n)) r))
		(cons string r)
		)))