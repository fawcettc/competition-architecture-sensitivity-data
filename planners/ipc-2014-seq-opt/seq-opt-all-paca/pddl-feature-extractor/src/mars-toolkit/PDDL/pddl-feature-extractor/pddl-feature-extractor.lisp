;;;; Copyright (c) 2013 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   David C. Wang

(in-package #:pddl-feature-extractor)

(defvar *enable-measure-macros* t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; FEATURE NAMES
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun requirement-feature-names ()
  (mapcar (lambda (requirement) (intern (concatenate 'string "USES-REQ-" (symbol-name requirement)) :keyword))
		  *requirements*))

(defparameter *all-pddl-feature-names* (append '(:num-types
											:num-constants
											:num-predicates 
											:num-actions 
											:num-objects 
											:num-initial
											:num-goals
											:num-objects-and-constants)
										  (stat-feature-names :num-predicate-vars)
										  (stat-feature-names :num-action-parameters)
										  (stat-feature-names :num-action-conditions)
										  (stat-feature-names :num-action-effects)
										  (stat-feature-names :num-action-add-effects)
										  (stat-feature-names :num-action-delete-effects)
										  '(:num-ground-propositions)
										  (stat-feature-names :num-groundings-per-pred)
										  '(:num-ground-actions)
										  (stat-feature-names :num-groundings-per-action)
										  (stat-feature-names :arity-of-goal-propositions)
										  )
    "List of all the feature names that the PDDL feature extractor can compute.")

(defparameter *default-feature-names* (append *all-sas-feature-names*
											  (remove-all *all-pddl-feature-names* 
														  :num-requirement :action-duration-based-on-fluents
														  (stat-feature-names :action-durations)
														  (stat-feature-names :action-min-durations)
														  (stat-feature-names :action-max-durations)
														  (requirement-feature-names)))
  "List of feature names that will be displayed in the CSV file.")


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; FEATURE-EXTRACTOR MAIN
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

#+allegro
(defmacro exit-on-error (&rest body)
	"If an error in BODY occurs, this form will print the error 
     and force the application to exit with code 0."
	`(unwind-protect 
		  (handler-case
			  (progn ,@body)
			(error (e) (format t "Error: ~a~%" e)))
	   (cl-user::exit 0)))

#+allegro
(defun feature-extractor-main ()
  (exit-on-error
   (system:with-command-line-arguments
	   (("o"      :short csv-features-path :required-companion)
		("i"      :short identifier :required-companion)
		("t"      :short add-timing-features)
		("h"      :short shelp)
		("help"   :long  lhelp))
	   (rest)
	 ;; (continue) will continue executions, but first giving the
	 ;; person running the app a chance to poke around.
	 
	 ;; if the user asked for help, just print the help then return.
	 (when (or shelp lhelp (= (length rest) 0))
	   (print-feature-extractor-help)
	   (cl-user::exit 0))

	 ;; parse the instance identifier naming scheme
	 (if identifier
		 (setf identifier (parse-number:parse-number identifier))
		 (setf identifier 1))

	 ;; enable timing measure macros
	 (if add-timing-features
		 (setf *enable-measure-macros* t)
		 (setf *enable-measure-macros* nil))
	 
	 ;; continue with algorithm
	 (let*  ((app-path (let ((temp (pathname (sys:command-line-argument 0))))
						;; get the path from which the current application was run
						(unless (pathname-directory temp)
						  (setf temp (translate-logical-pathname
									  (concatenate 'string "sys:" (sys:command-line-argument 0)))))
						temp))
			(curr-path (excl:current-directory))
			(full-app-path (merge-pathnames app-path curr-path))
			(full-app-parent-path (cl-fad:pathname-parent-directory (cl-fad:pathname-as-directory full-app-path)))
			(sas-translator-path (merge-pathnames "pddl-to-sas-translate.sh" full-app-parent-path)))
	   
	   (cond (;; interpret single argument as csv file
			  (= (length rest) 1)
			  (let ((csv-problems-path (merge-pathnames (first rest) curr-path)))
				(feature-extractor (list csv-problems-path) 
								   :output csv-features-path 
								   :feature-names *default-feature-names* 
								   :identifier identifier 
								   :sas-translator-path sas-translator-path)))
			 (;; interpret two arguments as domain and problem file
			  (= (length rest) 2)
			  (let ((pddl-domain-path (merge-pathnames (first rest) curr-path))
					(pddl-problem-path (merge-pathnames (second rest) curr-path)))
				(feature-extractor (list pddl-domain-path pddl-problem-path) 
								   :output csv-features-path 
								   :feature-names *default-feature-names* 
								   :identifier identifier
								   :sas-translator-path sas-translator-path)))
			 (:otherwise 
			  (error "Too many arguments provided."))
			 )
	   )
	 (format t "Completed Successfully!~%"))
   ))

(defun print-feature-extractor-help ()
  (format t 
"USAGE
   feature-extractor [OPTIONS] [CSV-FILE-CONTAINING-PROBLEMS]
   feature-extractor [OPTIONS] [PDDL-DOMAIN-FILE] [PDDL-PROBLEM-FILE]

DESCRIPTION 
   Extracts features from PDDL domain and problem files. There are two ways to use this function. When given one argument, the argument is assumed to be the path to a CSV file in which each line has the form \"domain/file/path.pddl, problem/file/path.pddl\". When given two arguments, the first is assumed to be the path to a PDDL domain file and the second the PDDL problem file.

OPTIONS
   -o
       Specify a file for the features to be written into. The file will be in CSV format, with one header row, and the features for each pddl problem specified in a row.
   -i 
       Specify an instance naming strategy. 0 for no name. 1 for \"domainpath problempath\". 2 for \"domainname domainfilename problemname problemfilename\". 3 to attempt to extract the domain name and problem number from the pathnames. 4 to attempt to extract the domain name from the pathnames, and assume the problem numbers correspond to the order of the linux file listing in the problem directory. 1 is default.
   -t
       Add the \"feature-extractor-cputime\" and \"feature-extractor-realtime\" features, which records the amount of time in seconds the feature extractor took to generate the features for this instance. cputime is a measure of how long the cpu is actually running. realtime is a measure of the amount of time since the function was invoked. 
   -h  --help
       Print this help message.
"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; FEATURE-EXTRACTOR API
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun feature-extractor (filepaths &key (output nil) (feature-names *default-feature-names*) (identifier 1) (sas-translator-path nil))
  (let ((pddl-problems (make-smart-list)))
	      
	;; READ INPUT PROBLEMS
	(cond (;; interpret single argument as csv file
		   (= (length filepaths) 1)
		   (let ((csv-problems-path (first filepaths))
				 (line-num 1))
			 (do-csv-file ((fields num-fields) csv-problems-path)
			   (if (= num-fields 2)
				   (let ((pddl-domain-path (merge-pathnames (first fields) csv-problems-path))
						 (pddl-problem-path (merge-pathnames (second fields) csv-problems-path)))
					 (coll-add! pddl-problems (list pddl-domain-path pddl-problem-path)))
				   (format t "Failed to parse line ~a of ~a." line-num csv-problems-path))
			   (incf line-num))))
		  (;; interpret two arguments as domain and problem file
		   (= (length filepaths) 2)
		   (let ((pddl-domain-path (first filepaths))
				 (pddl-problem-path (second filepaths)))
			 (coll-add! pddl-problems (list pddl-domain-path pddl-problem-path)))) )

	;; DECIDE WHETHER TO ADD THE TIMING FEATURE
	(when *enable-measure-macros*
	  (setf feature-names (append feature-names (list :feature-extractor-cputime :feature-extractor-realtime))))
	
	;; WRITE OUTPUT FEATURES
	(flet ((format-feature-value (fmap feature-name)
			 (let ((value (map-get fmap feature-name :default 0)))
			   (cond ((null value) "0") 
					 ((integerp value)
					  (format nil "~a" value))
					 ((numberp value)
					  (format nil "~6$" value))
					 (:otherwise "1"))
			   )))
	  (if output
		  ;; write output to csv file
		  (let ((csv-features-path output))
			(with-open-file (output-stream csv-features-path :direction :output :if-does-not-exist :create :if-exists :overwrite)
			  ;; write the header
			  (let ((lowercase-feature-names (mapcar (lambda (x) (format nil "~(~a~)" x)) feature-names)))
				(push "instance" lowercase-feature-names)
				(write-csv-line output-stream lowercase-feature-names))
			  ;; write the features
			  (loop for (pddl-domain-path pddl-problem-path) in (coll-tolist pddl-problems :copy nil) 
				 ;; these next two variables are only used if identifier options 3 or 4 are used.
				 with problem-number = 0
				 with curr-domain-name = nil
				 do
				   ;; do a garbage collect
				   #+allegro (excl:gc t)
				   ;; start 
				   (format t "Extracting features from: ~a, ~a ~%" (file-namestring pddl-domain-path) (file-namestring pddl-problem-path))
				   (multiple-value-bind (feature-map domain problem runtime realtime)
					   (measure-runrealtime	(extract-features-file pddl-domain-path pddl-problem-path :sas-translator-path sas-translator-path))
					 (when *enable-measure-macros* 
					   (map-add! feature-map :feature-extractor-cputime runtime)
					   (map-add! feature-map :feature-extractor-realtime realtime)  )
					 (let* ((features nil)
							(id ""))
					   ;;; calculate the features
					   (setf features (mapcar (lambda (feature-name) (format-feature-value feature-map feature-name)) feature-names))
					   ;;; calculate the identifier
					   (case identifier
						 (0 (setf id ""))
						 (1 (setf id (format nil "~a ~a" pddl-domain-path pddl-problem-path)))
						 (2 (setf id (format nil "~a ~a ~a ~a" (domain-name domain) (file-namestring pddl-domain-path) (problem-name problem) (file-namestring pddl-problem-path)) ))
						 (3 (let* ((split-domain-path (split-pathname (namestring pddl-domain-path)))
								   (domain-name (if (>= (length split-domain-path) 3)
													(nth (- (length split-domain-path) 3) split-domain-path)
													"err"))
								   (problem-number (multiple-value-bind (match regs)
													   (cl-ppcre:scan-to-strings "[^0-9]*([0-9]*)[^0-9]*" (file-namestring pddl-problem-path) )
													 (if match 
														 (1- (parse-number:parse-number (aref regs 0)))
														 "err")))
								   )
							  (setf id (format nil "~a.~a" domain-name problem-number))))
						 (4 (let* ((split-domain-path (split-pathname (namestring pddl-domain-path)))
								   (domain-name (if (>= (length split-domain-path) 3)
													(nth (- (length split-domain-path) 3) split-domain-path)
													"err"))   )
							  (if (string= domain-name curr-domain-name)
								  (incf problem-number)
								  (setf problem-number 0))
							  (setf curr-domain-name domain-name)
							  (setf id (format nil "~a.~a" domain-name problem-number)))
						  )
						 (:otherwise "")
						 )
					   (push id features)
					   (write-csv-line output-stream features)
					   (force-output output-stream))
					 ))
			  ))
		  ;; write output to standard output
		  (loop for (pddl-domain-path pddl-problem-path) in (coll-tolist pddl-problems :copy nil) do
			   (multiple-value-bind (feature-map domain problem)
				   (extract-features-file pddl-domain-path pddl-problem-path :sas-translator-path sas-translator-path)
				 (format t "Features for...~%")
				 (format t "  Domain: ~a (~a)~%" (domain-name domain) pddl-domain-path)
				 (format t "  Problem: ~a (~a)~%" (problem-name problem) pddl-problem-path)
				 (loop for feature-name in feature-names do
					  (format t "~(~a~): ~a~%" feature-name (format-feature-value feature-map feature-name)) )))
		  ))
	))

(defun extract-features-file (pddl-domain-pathname pddl-problem-pathname &key (sas-translator-path nil))
  "Reads a domain and problem file. Returns three values, a feature map, the domain, and the problem."
  (setf *recent-domain* nil)
  (setf *recent-problem* nil)
  (load-pddl pddl-domain-pathname)
  (load-pddl pddl-problem-pathname)
  ;; make sure the parser doesn't save previously 
  ;; parsed domains to conserve memory.
  (pddl:clear-domain-cache)
  (let ((domain *recent-domain*)
		(problem *recent-problem*))
	(cond (;; if the domain file doesn't exist
		   (null domain)
		   (format t "Domain file cannot be found."))
		  (;; if the problem file doesn't exist
		   (null problem)
		   (format t "Problem file cannot be found."))
		  (;; domain and problem file exist
		   :otherwise
		   (when (and domain problem)
			 (let ((feature-map (extract-pddl-features domain problem))
				   (sas-feature-map (make-linked-hashmap)))
			   (when sas-translator-path
				 (setf sas-feature-map (extract-sas-features-from-pddl sas-translator-path pddl-domain-pathname pddl-problem-pathname)))
			   (map-add-all! feature-map sas-feature-map)
			   (values feature-map domain problem))) )
		  )))


  ;; stats on action duration
  ;; number of actions
  ;; # of goals
  ;; # predicates at start
  ;; # objects

  ;; # stats on preconditions
  ;; # stats on effects
  ;; # of parameters in actions
  ;; # of delete effects per action
  ;; # reachability analysis
  ;; # of overlapping durative actions

(defun extract-pddl-features (domain problem)
  (let ((fmap (make-linked-hashmap))
		(objects-per-type-map (make-hashbag)))
	;; compute objects per type map
	(add-to-objects-per-type-map objects-per-type-map (domain-constants domain))	
	(add-to-objects-per-type-map objects-per-type-map (problem-objects problem))
	;; (format t "objects-per-type-map: ~a~% " objects-per-type-map)
	
	;; compute the number of ground propositions
	(let ((num-groundings-per-pred '()))
	  (do-domain-predicates (domain predicate)
		(push (num-groundings (domain-types domain) objects-per-type-map (rest predicate))
			  num-groundings-per-pred)
		)
	  (map-add! fmap :num-ground-propositions (apply #'+ num-groundings-per-pred))
	  (add-stat-features fmap :num-groundings-per-pred num-groundings-per-pred)
	  )

	;; compute the number of ground actions
	(let ((num-groundings-per-action '()))
	  (do-domain-actions (domain action)
		(push (num-groundings (domain-types domain) objects-per-type-map (action-parameters action))
			  num-groundings-per-action)
		)
	  (map-add! fmap :num-ground-actions (apply #'+ num-groundings-per-action))
	  (add-stat-features fmap :num-groundings-per-action num-groundings-per-action)
	  )

	;; requirement features
	(add-requirement-features fmap domain problem)
	;; number features
	(map-add! fmap :num-requirements (coll-size (domain-requirements domain)))
	(map-add! fmap :num-types (map-size (domain-types domain)))
	(map-add! fmap :num-constants (coll-size (domain-constants domain)))
	(map-add! fmap :num-predicates (coll-size (domain-predicates domain)))
	(map-add! fmap :num-actions (coll-size (domain-actions domain)))

	(map-add! fmap :num-objects (coll-size (problem-objects problem)))
	(map-add! fmap :num-initial (length (problem-init problem)))
	(map-add! fmap :num-goals (length (problem-goal problem)))

	(map-add! fmap :num-objects-and-constants (+ (coll-size (domain-constants domain))
												 (coll-size (problem-objects problem)) ))
	;; goal proposition arity
	(add-stat-features fmap :arity-of-goal-propositions (compute-arity-of-goal-propositions (domain-predicates domain) (problem-goal problem)) )
	
	;; add predicate variable statistics
	(let ((pred-var-list '()))
	  (pddl::map-domain-predicates domain
								   #'(lambda (predicate)
									   (push (length (rest predicate)) pred-var-list)))
	  (add-stat-features fmap :num-predicate-vars pred-var-list))

	;; add action duration statistics
	(let ((durations-list '())
		  (min-durations-list '())
		  (max-durations-list '())
		  (duration-based-on-fluents nil))
	  (pddl::map-domain-actions domain 
								#'(lambda (action) 
									;; compute action duration statistics
									(when (pddl::durative-action-p action)
									  (multiple-value-bind (lb ub action-duration-based-on-fluents)
										  (compute-action-duration-intervals action)
										(push (- ub lb) durations-list)
										(push lb min-durations-list)
										(push ub max-durations-list)
										(setf duration-based-on-fluents 
											  (or duration-based-on-fluents action-duration-based-on-fluents))
										))
									))
	  (add-stat-features fmap :action-durations durations-list)
	  (add-stat-features fmap :action-min-durations min-durations-list)
	  (add-stat-features fmap :action-max-durations max-durations-list)
	  (map-add! fmap :action-duration-based-on-fluents duration-based-on-fluents))	
	
	;; add other action statistics
	(let ((predicates (domain-predicates domain))
		  (num-parameters-list '())
		  (num-conditions-list '())
		  (num-effects-list '())
		  (num-add-effects-list '())
		  (num-delete-effects-list '()))
	  (coll-map (domain-actions domain)
				#'(lambda (action) 
					(cond ((action-p action)
						   ;; compute parameter statistics
						   (push (length (action-parameters action)) num-parameters-list)
						   ;; compute the number of conditions
						   (push (num-conditions (action-precondition action) predicates) num-conditions-list)
						   ;; compute the number of effects
						   (push (num-effects (action-effect action) predicates) num-effects-list)
						   ;; compute the number of delete effects
						   (push (num-delete-effects (action-effect action)) num-delete-effects-list))
						  ((durative-action-p action)
						   (with-slots (parameters overall-cond atstart-cond  atend-cond atstart-effect atend-effect) action 
							 ;; compute parameter statistics
							 (push (length parameters) num-parameters-list)
							 ;; compute the number of conditions
							 (let ((count 0))
							   (incf count (num-conditions atstart-cond predicates))
							   (incf count (num-conditions overall-cond predicates))
							   (incf count (num-conditions atend-cond predicates))
							   (push count num-conditions-list))
							 ;; compute the number of effects and delete effects
							 (let ((predicates (domain-predicates domain))
								   (count-effects 0)
								   (count-del-effects 0))
							   ;; compute the number of effects
							   (incf count-effects (num-effects atstart-effect predicates))
							   (incf count-effects (num-effects atend-effect predicates))
							   (push count-effects num-effects-list)
							   ;; compute the number of delete effects
							   (incf count-del-effects (num-delete-effects atstart-effect))
							   (incf count-del-effects (num-delete-effects atend-effect))
							   (push count-del-effects num-delete-effects-list)
							   ;; compute the number of add effects
							   (push (- count-effects count-del-effects) num-add-effects-list)
							   ))
						   )
						  )))
	  (add-stat-features fmap :num-action-parameters num-parameters-list)
	  (add-stat-features fmap :num-action-conditions num-conditions-list)
	  (add-stat-features fmap :num-action-effects num-effects-list)
	  (add-stat-features fmap :num-action-add-effects num-add-effects-list)
	  (add-stat-features fmap :num-action-delete-effects num-delete-effects-list)
	  )
	fmap))

(defun num-conditions (conditions predicates)
  (block body
	(cond (;; base case
		   (null conditions) 0)
		  (;; 
		   (listp conditions)
		   (let ((sum 0))
			 (if (coll-contains? predicates (first conditions) :use-key-function nil)
				 (incf sum))
			 (dolist (condition conditions)
			   (incf sum (num-conditions condition predicates)))
			 (return-from body sum)))
		  (;; otherwise, it is a symbol or number, skip it
		   :otherwise 0)
		  )))

(defun num-effects (effects predicates)
  (block body
	(cond (;; base case
		   (null effects) 0)
		  (;; 
		   (listp effects)
		   (let ((sum 0))
			 (if (coll-contains? predicates (first effects) :use-key-function nil)
				 (incf sum))
			 (dolist (effect effects)
			   (incf sum (num-effects effect predicates)))
			 (return-from body sum)))
		  (;; otherwise, it is a symbol or number, skip it
		   :otherwise 0)
		  )))

(defun num-delete-effects (effects)
  (block body
	(cond (;; base case
		   (null effects) 0)
		  (;; 
		   (listp effects)
		   (let ((sum 0))
			 (if (eql (first effects) 'pddl::not)
				 (incf sum))
			 (dolist (effect effects)
			   (incf sum (num-delete-effects effect)))
			 (return-from body sum)))
		  (;; otherwise, it is a symbol or number, skip it
		   :otherwise 0)
		  )))

(defun add-to-objects-per-type-map (objects-per-type-map objects)
  (docoll (objects object)
		  (dolist (type (rest object))
			(coll-add! objects-per-type-map type)))
	)

(defun num-objects-per-type (types objects-per-type-map typeobj &key (recurse t))
  "Return the number of objects of a given type.
   TYPEOBJ can be a symbol indicating a type-name or an object's name, 
   or it can be a pddl-type object. If it is an object-name (when TYPEOBJ
   does not name anything in the objects-per-type-map), the number 1 is returned."
  (assert (typep types 'pddl:pddl-type-graph))
  (let ((count 0))
	(cond (;; if typeobj is already a type object, do nothing
		   (typep typeobj 'pddl:pddl-type))
		  (;; if typeobj names a type, then get the type object
		   (and (symbolp typeobj) (coll-contains? objects-per-type-map typeobj))
		   (setf typeobj (get-type-object typeobj types)))
		  (;; if we get here, assume typeobj names an object 
		   (symbolp typeobj)
		   (return-from num-objects-per-type 1))
		  (:otherwise
		   (return-from num-objects-per-type 0))
		  )
	;; if the thing is a pddl type
	;; get the number of objects under the current type
	(setf count (coll-get objects-per-type-map (type-name typeobj)))
	;; get the number of objects under this type's subtypes
	(when recurse
	  (docoll ((direct-subtypes typeobj) subtype)
			  (incf count (num-objects-per-type types objects-per-type-map subtype))))  
	count))

(defun num-groundings (types objects-per-type-map typed-list)
  (let ((i 1)
		(j 0))
	;; loop over each variable in the typed-list
	(dolist (var-types-list typed-list)
	  ;; the number of ways this var can be grounded
	  (setf j 0)
	  (dolist (type (rest var-types-list))
		(incf j (num-objects-per-type types objects-per-type-map type)))
	  ;; the number of ways this predicate can be grounded
	  (setf i (* i j))
	  )
	i))

(defun compute-arity-of-goal-propositions (predicates goals)
  (mapcar #'(lambda (predicate) 
			  (length (rest predicate)))
		  (get-predicates-that-appear-in-goals predicates goals))  )

(defun get-predicates-that-appear-in-goals (predicates goals &optional (startinglist (make-smart-list)))
  (cond ((null goals))
		((listp goals)
		 (let ((predicate (coll-get predicates (first goals) :use-key-function nil)))
		   (if predicate
			(coll-add! startinglist predicate)
			(dolist (goal goals)
			  (get-predicates-that-appear-in-goals predicates goal startinglist))
			))))
  (coll-tolist startinglist))


(defun compute-action-duration-intervals (action)
  "Returns three values. 
   For actions that are not durative, returns <nil,nil,nil>
   For actions with durations that are only a function of constants, it will return <lowerbound,upperbound,nil>. 
   For actions that use numerical fluents in the computation of durations, it will return <0,0,t>."
  (block body
	(let ((duration (if (pddl::durative-action-p action) 
						(durative-action-duration action)
						'()))
		  (lb 0)
		  (ub *inf*)
		  (simple-duration-constraints (make-smart-list)))
	  (cond (;; if the duration is nil, return nil
			 (null duration)
			 (return-from body (values 0 0 nil))) 
			(;; if the duration starts with a conjunction, add rest to simple-duration-constraints
			 (eql (first duration) 'pddl::and)
			 (coll-add-all! simple-duration-constraints (rest duration)))
			(;; if the duration is anything else, add it to the set of simple-duration-constraints
			 (coll-add! simple-duration-constraints duration)))
	  ;; loop over the simple-duration-constraints
	  (dolist (constraint (coll-tolist simple-duration-constraints))
		(case (first constraint)
		  (pddl::at
		   (return-from body (values 0 0 t)))
		  (pddl::<= 
		   (if (numberp (third constraint))
			   (setf ub (min ub (third constraint)))
			   (return-from body (values 0 0 t))  ))
		  (pddl::>= 
		   (if (numberp (third constraint))
			   (setf lb (max lb (third constraint)))
			   (return-from body (values 0 0 t))  ))
		  (pddl::= 
		   (if (numberp (third constraint))
			   (progn (setf lb (max lb (third constraint)))
					  (setf ub (min ub (third constraint))))
			   (return-from body (values 0 0 t))  ))
			  ))
	  (when (= ub *inf*) (setf ub 10000))
	  (return-from body (values lb ub nil))
	  )))

(defun add-requirement-features (fmap domain problem)
  (let ((problem-requirements (make-hashset))
		(requirement-feature-name nil))
	(coll-add! problem-requirements (domain-requirements domain))
	(coll-add! problem-requirements (problem-requirements problem))
	(dolist (requirement *requirements*)
	  (setf requirement-feature-name (intern (concatenate 'string "USES-REQ-" (symbol-name requirement)) :keyword))
	  (if (coll-contains? problem-requirements requirement)
		  (map-add! fmap requirement-feature-name 1)
		  (map-add! fmap requirement-feature-name 0))
	  )))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Timing Code
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(defmacro measure-runtime (&rest body)
  (let ((start (gensym "START-TIME-"))
		(returns (gensym "RETURNS-")) )
	`(if *enable-measure-macros*
		 (let ((,start (get-internal-run-time))
			   (,returns (multiple-value-list (progn ,@body))))
		   (apply #'values 
				  (append ,returns
						  (list (/ (- (get-internal-run-time) ,start) internal-time-units-per-second))) ))
		 (progn ,@body))
	))

(defmacro measure-realtime (&rest body)
  (let ((start (gensym "START-TIME-"))
		(returns (gensym "RETURNS-")) )
	`(if pddl-feature-extractor:*enable-measure-macros*
		 (let ((,start (get-internal-real-time))
			   (,returns (multiple-value-list (progn ,@body))))
		   (apply #'values 
				  (append ,returns
						  (list (/ (- (get-internal-real-time) ,start) internal-time-units-per-second))) ))
		 (progn ,@body))
	))

(defmacro measure-runrealtime (&rest body)
  (let ((runstart (gensym "RUN-START-TIME-"))
		(realstart (gensym "REAL-START-TIME-"))
		(returns (gensym "RETURNS-")) )
	`(if pddl-feature-extractor:*enable-measure-macros*
		 (let ((,runstart (get-internal-run-time))
			   (,realstart (get-internal-real-time))
			   (,returns (multiple-value-list (progn ,@body))))
		   (apply #'values 
				  (append ,returns
						  (list (/ (- (get-internal-run-time) ,runstart) internal-time-units-per-second))
						  (list (/ (- (get-internal-real-time) ,realstart) internal-time-units-per-second))
						  )))
		 (progn ,@body))
	))
