(in-package #:pddl-feature-extractor)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; FEATURE NAMES
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defparameter *all-sas-feature-names* (append '(:num-sas-variables 
												:num-sas-mutexes 
												:num-sas-mutexes-and-variables 
												:num-sas-goals
												:num-sas-operators 
												:num-sas-axioms 
												:num-sas-operators-and-axioms)
										  (stat-feature-names :num-sas-op-prevails)
										  (stat-feature-names :num-sas-op-preconditions)
										  (stat-feature-names :num-sas-op-effects)
										  (stat-feature-names :size-sas-var-domain)
										  )
    "List of all the feature names that the PDDL feature extractor can compute.")

(defun pddl-to-sas-translator (sas-translator-path pddl-domain-pathname pddl-problem-pathname &key (verbose nil))
  (let* ((command "")
		 (exit-code nil)
		 (sas-translator-parent-directory (cl-fad:pathname-parent-directory (cl-fad:pathname-as-directory sas-translator-path)))
		 (sas-file-path (merge-pathnames "output.sas" sas-translator-parent-directory )))
	;; create the command
	(setf command (format nil "\"~0A\" \"~0A\" \"~0A\""
						   sas-translator-path pddl-domain-pathname pddl-problem-pathname))
	;; print the command
	(when verbose
	  (format t "~a~%" command))

	;; run the command
	(let ((asdf:*verbose-out* verbose))
	  (setf exit-code (asdf:run-shell-command command)))

	;; print errors
    (when (not (= 0 exit-code))
	  (format t "Error: Failed to translate PDDL files to SAS.~%"))
	;; return sas file path
	(if (and (= 0 exit-code) (probe-file sas-file-path))
		sas-file-path
		nil)
	))

(defun extract-sas-features-from-pddl (sas-translator-path pddl-domain-pathname pddl-problem-pathname)
  (let ((sas-file-pathname (pddl-to-sas-translator sas-translator-path pddl-domain-pathname pddl-problem-pathname :verbose nil)))
	(unless sas-file-pathname
	  (format t "Error: Failed to extract sas features.~%")
	  (return-from extract-sas-features-from-pddl (make-linked-hashmap)))
	(extract-sas-features sas-file-pathname)
	))


(defun extract-sas-features (sas-file-pathname)
  (format t "Parsing SAS file...~%")
  (let ((sas-object (sas:parse-sas-file sas-file-pathname))
		(fmap (make-linked-hashmap)))
	(format t "Beginning Feature Extraction...~%")
	(with-slots (sas:variables sas:mutex sas:goal sas:operators sas:axioms) sas-object
	  ;; extract features
	  
	  (map-add! fmap :num-sas-variables (array-dimension sas:variables 0))
	  (map-add! fmap :num-sas-mutexes (array-dimension sas:mutex 0))
	  (map-add! fmap :num-sas-mutexes-and-variables (+ (array-dimension sas:variables 0)
													   (array-dimension sas:mutex 0)   ))
	  (map-add! fmap :num-sas-goals (array-dimension sas:goal 0))
	  (map-add! fmap :num-sas-operators (array-dimension sas:operators 0))
	  (map-add! fmap :num-sas-axioms (array-dimension sas:axioms 0))
	  (map-add! fmap :num-sas-operators-and-axioms (+ (array-dimension sas:operators 0)
													  (array-dimension sas:axioms 0)  ))

	  ;; add statistics on operator preconditions and effects
	  (let ((num-cond-list '()))
		(loop for op across sas:operators do
			 (push (array-dimension (sas:sas-operator-prevails op) 0)
				   num-cond-list)
			 )
		(add-stat-features fmap :num-sas-op-prevails num-cond-list)
		)
	  (let ((num-eff-list '()))
		(loop for op across sas:operators do
			 (push (array-dimension (sas:sas-operator-effects op) 0)
				   num-eff-list)
			 )
		(add-stat-features fmap :num-sas-op-effects num-eff-list)
		)
	  (let ((num-pre-list '()))
		(loop for op across sas:operators do
			 (push (loop for op-eff across (sas:sas-operator-effects op)
					  unless (= -1 (sas:sas-effect-pre-val op-eff))
					  sum 1 into num-preconditions
					  finally (return num-preconditions))
				   num-pre-list)
			 )
		(add-stat-features fmap :num-sas-op-preconditions num-pre-list)
		)
	  
	  ;; statistics on the size of each variable's domain (range)
	  (let ((domain-size-list '()))
		(loop for var across sas:variables do
			 (push (array-dimension (sas:sas-variable-range var) 0)
				   domain-size-list))
		(add-stat-features fmap :size-sas-var-domain domain-size-list))
	  )
	fmap))


