(in-package :pddl)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; CLASS PDDL-TYPE-GRAPH
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass pddl-type-graph (hashmap)
  (
   (default-supertype
   	   :type pddl-type
   	 :initform (make-instance 'pddl-type :name 'object)
   	 :initarg :default-supertype
   	 :accessor default-supertype
   	 :documentation "The default supertype for all types, object.")
   )
  )

(defun make-pddl-type-graph ()
  (let* ((type-graph (make-instance 'pddl-type-graph))
  		 (object (default-supertype type-graph)))
  	(map-add! type-graph (type-name object) object)
  	type-graph)
  ;; (make-instance 'pddl-type-graph)
)

(defmethod num-types ((type-graph pddl-type-graph))
  "Returns the number of types in the TYPE-GRAPH."
  (map-size type-graph))

(defmethod list-type-names ((type-graph pddl-type-graph))
  "Returns a list of all type objects in the TYPE-GRAPH."
  (let ((type-objects (make-smart-list)))
	(map-map type-graph 
			 #'(lambda (type-name type-obj) 
				 (declare (ignore type-obj))
				 (coll-add! type-objects type-name)))
	(coll-tolist type-objects :copy nil)))

(defmethod list-type-objects ((type-graph pddl-type-graph))
  "Returns a list of all type objects in the TYPE-GRAPH."
  (let ((type-objects (make-smart-list)))
	(map-map type-graph 
			 #'(lambda (type-name type-obj) 
				 (declare (ignore type-name))
				 (coll-add! type-objects type-obj)))
	(coll-tolist type-objects :copy nil)))

(defmethod map-types ((type-graph pddl-type-graph) f)
  "Calls a function F on each type-object in the TYPE-GRAPH.
   The function must take two arguments, 
   the first will be the symbol name of the type, 
   the second is the type-object."
  (map-map type-graph f))

(defmethod print-object ((type-graph pddl-type-graph)(stream stream))
  (map-map type-graph 
		   (lambda (name type-obj) 
			 (declare (ignore name))
			 (format stream "~a~%" type-obj))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; CLASS PDDL-TYPE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass pddl-type ()
  ((name
	:type symbol
	:initform nil
	:initarg :name
	:accessor type-name
	:documentation "The symbol name of this type.")
   (direct-supertypes
	:type hashset
	:initform (make-hashset)
	:initarg :direct-supertypes
	:accessor direct-supertypes
	:documentation "A collection containing all the direct supertypes of this type.")
   (direct-subtypes
	:type hashset
	:initform (make-hashset)
	:initarg :direct-subtypes
	:accessor direct-subtypes
	:documentation "A collection containing all the direct subtypes of this type.")
   ))

(defmethod print-object ((type pddl-type)(stream stream))
  (with-slots (name direct-supertypes direct-subtypes) type
	(format stream "#TYPE(")
	;; print type name
	(format stream "~a:" name)
	;; print subs
	(coll-map direct-subtypes
			  (lambda (subtype) (format stream "~a," (type-name subtype))))
	;; print arrow
	(format stream "->" name)
	;; print type name
	(format stream "~a" name)
	;; print arrow
	(format stream "->" name)
	;; print supers
	(coll-map direct-supertypes 
			  (lambda (supertype) (format stream "~a," (type-name supertype))))
	(format stream ")")
	))

(declaim (inline ensure-type-object))
(defmethod ensure-type-object ((type-name symbol)(type-graph pddl-type-graph))
  "Returns two values, a pddl-type object and whether the type-object 
   was found or created. 
   If the pddl-type object with name TYPE-NAME exists in the TYPE-GRAPH, 
   that object will be returned, followed by t.
   Otherwise a new one will be created with with TYPE name,
   that object will be returned followed by nil."
  (let ((type-object (map-get type-graph type-name :default nil)))
	(if type-object
		(values type-object t)
		(let (;; make the new type
			  (new-type-obj (make-instance 'pddl-type :name type-name)))
		  ;; make sure the default super type points at it
		  (coll-add! (direct-supertypes new-type-obj) (default-supertype type-graph))
		  (coll-add! (direct-subtypes (default-supertype type-graph)) new-type-obj)
		  ;; add it to the type graph
		  (map-add! type-graph type-name new-type-obj)
		  (values new-type-obj nil)))
	))

(declaim (inline get-type-object))
(defmethod get-type-object ((type symbol)(type-graph pddl-type-graph))
  "Returns a pddl-type object ONLY if it exists in the TYPE-GRAPH,
   otherwise nil is returned."
  (map-get type-graph type :default nil))


;; (defmethod supertypes ((type-obj pddl-type) &optional (initial-supertypes (make-hashset)))
;;   "Returns a collection of all the supertypes of this type."
;;   (coll-add-all! initial-supertypes  (direct-supertypes type-obj))
;;   (coll-map (direct-supertypes type-obj)
;; 			(lambda (super-type-obj) 
;; 			  (supertypes super-type-obj initial-supertypes)))
;;   )

(defmethod supertypes ((type-obj pddl-type) &key (depth-first t))
  "Returns a collection of all the supertypes of this type."
  (let ((supertypes (make-linked-hashset))
		(Q (make-smart-list))
		(dummy nil)
		(curr-type nil))
	(declare (ignore dummy))
	;; add initial 
	(coll-add! Q type-obj)
	;; loop and add to Q
	(loop until (coll-empty? Q) do
		 (multiple-value-setq (dummy curr-type)
		   (if depth-first
			   (coll-remove-last! Q)
			   (coll-remove-first! Q)))
		 (coll-add-all! supertypes (direct-supertypes curr-type))
		 (coll-add-all! Q (direct-supertypes curr-type)))
	supertypes))

;; (defmethod subtypes ((type-obj pddl-type) &optional (initial-subtypes (make-hashset)))
;;   "Returns a collection of all the subtypes of this type."
;;   (coll-add-all! initial-subtypes  (direct-subtypes type-obj))
;;   (coll-map (direct-subtypes type-obj)
;; 			(lambda (sub-type-obj) 
;; 			  (subtypes sub-type-obj initial-subtypes)))
;;   )

(defmethod subtypes ((type-obj pddl-type) &key (depth-first t))
  "Returns a collection of all the subtypes of this type.
   If there are no subtypes, an empty collection is returned."
  (let ((subtypes (make-hashset))
		(Q (make-smart-list))
		(dummy nil)
		(curr-type nil))
	(declare (ignore dummy))
	;; add initial 
	(coll-add! Q type-obj)
	;; loop and add to Q
	(loop until (coll-empty? Q) do
		 (multiple-value-setq (dummy curr-type)
		   (if depth-first
			   (coll-remove-last! Q)
			   (coll-remove-first! Q)))
		 (coll-add-all! subtypes (direct-subtypes curr-type))
		 (coll-add-all! Q (direct-subtypes curr-type)))
	subtypes))


(defmethod add-type ((name symbol) (new-super-names t) (new-sub-names t) (type-graph pddl-type-graph))
  "Adds to or updates the type-graph with this type.
   NAME must be the symbol name of this type.
   NEW-SUPER-NAMES can be nil, a symbol, a list, or collection of the symbol names of supertypes.
   NEW-SUB-NAMES can be nil, a symbol, a list, or collection of the symbol names of subtypes.
   Returns the type-object."
  ;; get or make the type object with name NAME.
  (let ((curr-type-obj (ensure-type-object name type-graph)))
	(flet ((update-super (new-super-name)
			 (let ((super-type-obj (ensure-type-object new-super-name type-graph)))
			   (coll-add! (direct-supertypes curr-type-obj) super-type-obj)
			   (coll-add! (direct-subtypes super-type-obj) curr-type-obj)))
		   (update-sub (new-sub-name)
			 (let ((sub-type-obj (ensure-type-object new-sub-name type-graph)))
			   (coll-add! (direct-subtypes curr-type-obj) sub-type-obj)
			   (coll-add! (direct-supertypes sub-type-obj) curr-type-obj)))
		   )
	  ;; merge with supers
	  (cond (;; do nothing if the super is nil
			 (null new-super-names))
			((typep new-super-names 'adt-collection)
			 (coll-map new-super-names #'update-super))
			((symbolp new-super-names)
			 (update-super new-super-names))
			((listp new-super-names)
			 (mapcar #'update-super new-super-names))
			(:otherwise
			 ;; the super type was not recognized
			 (format t "Error adding type ~a. The supertype parameter ~a could not be recognized." name new-super-names)
			 (return-from add-type nil)
			 )
			)
	  ;; merge with subs
	  (cond (;; do nothing if the sub is nil
			 (null new-sub-names))
			((typep new-sub-names 'adt-collection)
			 (coll-map new-sub-names #'update-sub))
			((symbolp new-sub-names)
			 (update-sub new-sub-names))
			((listp new-sub-names)
			 (mapcar #'update-sub new-sub-names))
			(:otherwise
			 ;; the sub type was not recognized
			 (format t "Error adding type ~a. The subtype parameter ~a could not be recognized." name new-super-names)
			 (return-from add-type nil)
			 )
			)
		)
	(remove-duplicate-inheritance type-graph)
	;; return the type object corresponding to this type
	curr-type-obj))

(defun remove-duplicate-inheritance (tg &optional (root (default-supertype tg)))
  ;; clean up any duplicate inheritances, i.e.
  ;; Define "->" to mean "is a subtype of"
  ;; When "dog->object" and "dog->animal->object"
  ;; We want to keep the inheritance order "dog->animal->object"
  ;; and discard "object" from the direct supertypes of "dog".
  (unless (null root)	
	(let ((direct-subs (direct-subtypes root)))
	  ;; remove duplicates from my direct subtypes
	  (docoll (direct-subs direct-sub)
			  (docoll ((subtypes direct-sub) child-sub)
					  ;; if the current type's direct-subs contains
					  ;; a child sub, remove it.
					  (when (coll-contains? direct-subs child-sub)
						(coll-remove! direct-subs child-sub)
						(coll-remove! (direct-supertypes child-sub) root))
					  ))
	  ;; remove duplicates from each of the direct subtypes
	  (docoll (direct-subs direct-sub)
			  (remove-duplicate-inheritance tg direct-sub))
	  )))


(defmethod union-type-graphs ((tg1 pddl-type-graph) (tg2 pddl-type-graph) &key (copy nil))
  "Unites the types from two type graphs into one type-graph.
   If COPY is t, the resulting type-graph is a new object distinct from parameters TG1 or TG2.
   If COPY is nil, all the types from the second pddl-type-graph are added to the first type-graph."
  (let ((tg-union (if copy 
					  (union-type-graphs (make-pddl-type-graph) tg1)
					  tg1)))
	;; copy tg2 into tg1
	(map-map tg2 
			 (lambda (name type-obj)
			   (declare (ignore name))
			   (let ((sup-names '())
					 (sub-names '()))
				 ;; create lists of the super and sub types symbolic names
				 (coll-map (direct-supertypes type-obj) (lambda (sup) (push (type-name sup) sup-names)))
				 (coll-map (direct-subtypes type-obj) (lambda (sub) (push (type-name sub) sub-names)))
				 ;; use the add-type method to add each type to the new type-graph
				 (add-type (type-name type-obj) sup-names sub-names tg-union)))
			 )
	tg-union))