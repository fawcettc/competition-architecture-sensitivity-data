(in-package :pddl)

;; Forward Declarations
;; (defgeneric domain-name (domain))
;; (defgeneric problem-name (problem))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; GLOBAL VARIABLES
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; A list of all loaded domain objects
(defvar *domains* (make-hashset :key-function #'domain-name)
  "A list of all loaded domain objects.")
;; A list of all loaded problem objects
(defvar *problems* (make-hashset :key-function #'problem-name)
  "A list of all loaded problem objects.")

;; Most recently parsed domain
(defvar *recent-domain* nil)
;; Most recently parsed problem
(defvar *recent-problem* nil)

(defvar *use-domain-cache* nil
  "Tells the parser whether it should save off parsed PDDL
   problems into the global variables *domains* and *problems*. 
   If nil (default), the most recently parsed domain will still be
   saved in *recent-domain* and *recent-problem*, but nothing will
   be put into *domains* and *problems*. If t, all parsed PDDL
   problems will be saved and can be retrieved using (get-domain)
   and (get-problem).")

(defun clear-domain-cache ()
  "Empties the global variables *domains* and *problems* 
   of previously parsed and cached domains."
  (coll-clear! *domains*)
  (coll-clear! *problems*))

(defun get-domain (name)
  "Finds and returns the domain object named NAME. 
   NAME can be either a String or Symbol.
   If no domain is found, nil will be returned."
  (cond (;; if arg is a string, convert to symbol
		 (stringp name) (setf name (intern name)))
		(;; if arg is a symbol, do nothing
		 (symbolp name))
		(:otherwise
		 (error "Function get-domain does not recognize the argument ~a~%" name)))
  (coll-get *domains* name :use-key-function nil :default nil))

(defun get-problem (name)
  "Finds and returns the problem object named NAME. 
   NAME can be either a String or Symbol.
   If no problem is found, nil will be returned."
  (cond (;; if arg is a string, convert to symbol
		 (stringp name) (setf name (intern name)))
		(;; if arg is a symbol, do nothing
		 (symbolp name))
		(:otherwise
		 (error "Function get-problem does not recognize the argument ~a~%" name)))
  (coll-get *problems* name :default nil))


(defmethod get-action ((name symbol)(domain domain))
  "Returns the actions named by the symbol NAME from the DOMAIN object."
  (coll-get (domain-actions domain) name))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; PDDL PARSING INTERFACE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun load-pddl (pddl-file-pathname &key (verbose nil))
  "Loads the given PDDL file. The PDDL file can contain the domain, problem, 
   or both domain and problem definitions."
	(let ((*package* (find-package :pddl)))
	  (load pddl-file-pathname :verbose verbose)))

;;; Functions that accept domain and problem definitions in UCPOP format

(defmacro DEFINE ((dtype name) &body body)
  (case dtype
    (domain `(setf *recent-domain* (apply #'def-domain '(,name ,@body))))
    (problem `(setf *recent-problem* (apply #'def-problem '(,name ,@body))))
    ))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; DOMAIN support code
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun def-domain (name &rest clauses)
  (let ((domain (make-domain :name name))
        (warnings nil))
	(with-slots (requirements types constants predicates actions) domain
	  (dolist (clause clauses)
		  (case (car clause)
			(:extends
	      ;;; This domain inherits actions, constants, etc. from one or
	      ;;; more other domains.
			 (let ((extended-domains (mapcar #'(lambda (name) (get-domain name))
											 (cdr clause))))
			   (dolist (domain extended-domains)
				 ;; Inherit requirements.
				 (coll-add-all! requirements (domain-requirements domain) :replace nil)
				 ;; Inherit types.
				 (setf types (union-type-graphs types (domain-types domain) :copy nil))
				 ;; Inherit constants.
				 (coll-add-all! constants (domain-constants domain) :replace nil)
				 ;; Inherit predicates.
				 (coll-add-all! predicates (domain-predicates domain) :replace nil)
				 ;; Inherit actions.
				 (coll-add-all! actions (domain-actions domain) :replace nil)
				 )
			   ))
			(:requirements 
			 (coll-add-all! requirements (cdr clause) :replace t))
			(:types
			 (setf types (union-type-graphs types (parse-types (cdr clause)) :copy nil)))
			(:constants
			 (coll-add-all! constants (parse-pddl-typed-list (cdr clause)) :replace t))
			(:predicates
			 (map nil 
				  #'(lambda (predicate) (coll-add! predicates (parse-predicate predicate) :replace t))
				  (cdr clause))	 )
			(:action
			 (coll-add! actions (apply #'def-action (cdr clause)) :replace t) )
			(:durative-action
			 (coll-add! actions (apply #'def-durative-action (cdr clause)) :replace t) )
			(t (pushnew (car clause) warnings)))))
    (when warnings
      (format t "~&;~4TWarning: unable to process parameter(s), ~{~S, ~}in domain ~A~%" 
			  warnings name))
	;; Now that we have been successful at parsing
	;; Let's save off the domain
	(setf *recent-domain* domain)
	(when *use-domain-cache*
	  (coll-add! *domains* domain))
	domain))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; ACTION support code
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun def-action (name &key parameters precondition effect)
  (let* ((typed-params (parse-pddl-typed-list parameters))
		 (variables typed-params)) ;; TODO: What is VARIABLES used for?
    ;; Create the new action
	(make-action :name name
				 :parameters typed-params
				 :precondition precondition
				 :effect effect
				 :variables variables)
	))


(defun def-durative-action (name &key parameters duration condition effect)
  (let*  ((typed-params (parse-pddl-typed-list parameters))
		  (variables typed-params))
	(multiple-value-bind (atstart-cond overall-cond atend-cond) (separate-durative-conditions condition)
	  (multiple-value-bind (atstart-effect dummy atend-effect) (separate-durative-conditions effect)
		(declare (ignore dummy))
		;; Create the new action
		(make-durative-action :name name
							  :parameters typed-params
							  :duration duration
							  :atstart-cond (coll-tolist atstart-cond :copy nil)
							  :overall-cond (coll-tolist overall-cond :copy nil)
							  :atend-cond (coll-tolist atend-cond :copy nil)
							  :atstart-effect (coll-tolist atstart-effect :copy nil)
							  :atend-effect (coll-tolist atend-effect :copy nil)
							  :variables variables
							  )))
    ))

(defun separate-durative-conditions (precs &optional 
									 (atstart (make-smart-list :initial-contents '(and) :copy t)) 
									 (overall (make-smart-list :initial-contents '(and) :copy t)) 
									 (atend (make-smart-list :initial-contents '(and) :copy t)) )
  (cond (;; do nothing
		 (null precs))
		(;; do nothing
		 (symbolp precs))
		(;; recognize at start
		 (and (listp precs) (eq (first precs) 'at) (eq (second precs) 'start))
		 (coll-add! atstart (third precs)))
		(;; recognize over all
		 (and (listp precs) (eq (first precs) 'over) (eq (second precs) 'all))
		 (coll-add! overall (third precs)))
		(;; recognize at end
		 (and (listp precs) (eq (first precs) 'at) (eq (second precs) 'end))
		 (coll-add! atend (third precs)))
		(;; loop over the list
		 (listp precs)
		 (loop for prec in precs do 
			  (separate-durative-conditions prec atstart overall atend)) )
		(;; do nothing
		 :otherwise) )
  (values atstart overall atend)
  )

;; (defun group-durative-precs (precs)
;;   (let ((over-all '())
;; 		(at-start '())
;; 		(at-end '()))
;; 	;; add an and if the precs does not start with and
;; 	(unless (eql (car precs) 'and)
;; 	  (setf precs `(and ,precs)))
;; 	;; loop for conditions
;; 	(loop for prec in (cdr precs) do
;; 		 (case (car prec)
;; 		   (over (push (caddr prec) over-all))
;; 		   (at (case (cadr prec)
;; 				 (start (push (caddr prec) at-start))
;; 				 (end (push (caddr prec) at-end))))))
;; 	(list (cons 'over-all over-all) 
;; 		  (cons 'at-start at-start)
;; 		  (cons 'at-end at-end))  ))


;; (reorder-precs 
;;  (preprocess-precs precondition))


(defun reorder-precs (preconditions)
  ;; Accumulate all the equality and inequality constraints from the
  ;; preconditions.  Put them at the end of the list of preconditions.
  ;; Right before the (in)equality constraints come the negated
  ;; propositions.
  (cond ((null preconditions) nil)
		((not (listp preconditions)) preconditions)
		((eq (car preconditions) 'and)
		 ;; Move all the eq and ineq conjuncts to the end
		 (cons 'and
			   (sort (mapcar #'reorder-precs (cdr preconditions)) #'prec-order)))
		(t (cons (car preconditions)
				 (mapcar #'reorder-precs (cdr preconditions))))))
	 
(defun prec-ordinal (p)
  (cond ((not (listp p)) 0)		; Atoms go first
		((eq (car p) '=) 2)		; = and not = are last
		((and (eq (car p) 'not)
			  (eq (caadr p) '=)) 2)
		((eq (car p) 'not) 1)		; Second are negated props
		(t 0)))				; Positive propositions also lead off

(defun prec-order (p1 p2)
  (< (prec-ordinal p1) (prec-ordinal p2)))

(defun preprocess-precs (precs)
  (cond ((null precs) precs)
		((not (listp precs)) precs)
		((eq (car precs) 'imply)
		 (list 'or
			   (list 'not (preprocess-precs (second precs)))
			   (preprocess-precs (third precs))))
		(t (mapcar #'preprocess-precs precs))))

;; (defun remove-durative-precs (precs)
;;   (let ((new-precs 'nil))
;; 	;; add an and if the precs does not start with and
;; 	(unless (eql (car precs) 'and)
;; 	  (setf precs `(and ,precs)))
;; 	;; loop for conditions that start with and
;; 	(loop for prec in (cdr precs) do
;; 		 (pushnew (caddr prec) new-precs :test #'equalp))
;; 	`(and ,@(reverse new-precs))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; PROBLEM support code
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun def-problem (name &rest clauses)
  (let ((problem (make-problem :name name))
		(warnings nil)
		)
	(with-slots (domain-name requirements objects init goal) problem
		(dolist (clause clauses)
		  (case (car clause)
			(:domain (setf domain-name (second clause)))
			(:requirements (coll-add-all! requirements (rest clause) :replace t))
			(:objects (coll-add-all! objects (parse-pddl-typed-list (cdr clause)) :replace t))
			(:init (setf init (append '(and) (rest clause))))
			(:goal (setf goal (preprocess-precs (second clause))))
			(:length nil)			; Just ignore this piece of information
			(t (pushnew (car clause) warnings))))
	  
	  (when warnings
		(format t "~&;~4TWarning: unable to process parameter(s), ~{~S, ~}in problem ~A~%"
				warnings name))

	  (setf *recent-problem* problem)
	  ;; save off this problem
	  (when *use-domain-cache*
		(coll-add! *problems* problem))
	  ;; add this problem to the domain  
	  problem)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; PARSE PREDICATE 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(declaim (inline parse-predicate))
(defun parse-predicate (pddl-predicate)
  "Parses a PDDL predicate and returns a list-based representation of the predicate:
   (predicate (var1 type-obj) (var2 type-obj) ... (varn type-obj)"
  (cons (car pddl-predicate) (parse-pddl-typed-list (cdr pddl-predicate)))
  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; TYPE HANDLING
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun parse-types (pddl-types-list)
  "Iteratively translates PDDL style typed list into a pddl type-graph."
  (let ((type-graph (make-pddl-type-graph))
		(untyped-types '())
		(mode 'type)) ;; 'type - looking for type,  'super - looking for a supertype
	(loop for typed-element in pddl-types-list do
		 (cond (;; we have received a type delimiter
				(eql typed-element '-)
				(setf mode 'super))
			   (;; a type delimiter has been received, this element must be the type
				(eql mode 'super) (setf mode 'type)
				(let (supertype)
				  ;; check if the type is a list that starts with "either" or 
				  ;; if it is a singular type
				  (cond (;; this is a singular type
						 (not (consp typed-element))
						 (setf supertype (list typed-element)))
						(;; this should be an either type
						 (or (eq (car typed-element) :either) (eq (car typed-element) 'either))
						 (setf supertype (rest typed-element)))
						(:otherwise (format t "Warning: unable to parse typed-list ~A. Some types or supertypes were skipped~%" pddl-types-list)))
				  ;; add the types to the type graph with supertypes
				  (mapcar #'(lambda (type) (add-type type supertype nil type-graph)) untyped-types)
				  ;; erase the list of untyped types
				  (setf untyped-types nil)
				  ))
			   (; look for more types
				(eql mode 'type)
				(push typed-element untyped-types))
			   )
		 )
	;; return all the typed and untyped variables
	(mapcar #'(lambda (type) (add-type type nil nil type-graph)) untyped-types)
	type-graph))

(defun parse-pddl-typed-list (pddl-typed-list)
  "Iteratively translates PDDL style typed list into a list of elements of the form
  ((var type) (var type))."
  (let ((typed-vars (make-smart-list))
		(untyped-vars (make-smart-list))
		(mode 'var)) ;; 'var - looking for variables,  'type - looking for a type
	(loop for typed-element in pddl-typed-list do
		 (cond (;; we have received a type delimiter
				(eql typed-element '-)
				(setf mode 'type))
			   (;; a type delimiter has been received, this element must be the type
				(eql mode 'type) (setf mode 'var)
				(let (type)
				  ;; check if the type is a list that starts with "either" or 
				  ;; if it is a singular type
				  (cond (;; this is a singular type
						 (not (consp typed-element))
						 (setf type (list typed-element)))
						(;; this should be an either type
						 (or (eq (car typed-element) :either) (eq (car typed-element) 'either))
						 (setf type (rest typed-element)))
						(:otherwise (format t "Warning: unable to parse typed-list ~A. Some variables or types were skipped~%" pddl-typed-list)))
				  ;; add the untyped variables to the typed list with types
				  (coll-map untyped-vars #'(lambda (var) (coll-add! typed-vars (cons var type))))
				  ;; erase the list of untyped vars
				  (coll-clear! untyped-vars)
				  ))
			   (; look for variables
				(eql mode 'var)
				(coll-add! untyped-vars typed-element))
			   )
		 )
	;; return all the typed and untyped variables, with untyped vars labeled as type object
	(coll-map untyped-vars #'(lambda (var) (coll-add! typed-vars (cons var nil))))
	(coll-tolist typed-vars)))




