(in-package #:sas)


(defstruct (sas (:print-function print-sas))
  (metric nil)       ;; metric - a number 0 or 1
  (variables nil)	 ;; an array of sas-variables
  (mutex nil)        ;; an array of mutexes
  (init nil)         ;; an array of variable initial values
  (goal nil)         ;; an array of variable assignments
  (operators nil)    ;; an array of operators
  (axioms (make-linked-hashset))
  )

(defun print-sas (sas &optional (stream t) depth)
  (declare (ignore depth))
  (format stream "<SAS~%")
  (format stream " Metric: ~A~%" (sas-metric sas))
  (format stream " Variables:~% ~A~%" (sas-variables sas))
  (format stream " Mutex: ~A~%" (sas-mutex sas))
  (format stream " Initial State: ~A~%" (sas-init sas))
  (format stream " Goal: ~A~%" (sas-goal sas))
  (format stream " Operators: ~A~%" (sas-operators sas))
  (format stream " Axioms: ~A~%" (sas-axioms sas))
  (format stream ">~%"))


(defstruct (sas-variable (:print-function print-sas-variable))
  (name nil)    ;; a string
  (axiom-layer -1)		 
  (range nil)   ;; an array of values  
  )

(defun print-sas-variable (sas-variable &optional (stream t) depth)
  (declare (ignore depth))
  (with-slots (name axiom-layer range) sas-variable
	(format stream "<SAS-VARIABLE~%")
	(format stream " Name: ~A~%" name)
	(format stream " Axiom-Layer: ~A~%" axiom-layer)
	(format stream " Range: ~A~%" range)
	(format stream ">~%")))

(defstruct (sas-operator (:print-function print-sas-operator))
  (name nil)      ;; a string
  (prevails nil)  ;; a 2D array of variable-value assignments
  (effects nil)   ;; an array of SAS-effect objects
  (cost 1)        ;; a number representing the cost of the action, defaults to 1 if metric is 0.
  )

(defun print-sas-operator (sas-operator &optional (stream t) depth)
  (declare (ignore depth))
  (with-slots (name prevails effects cost) sas-operator
	(format stream "<SAS-OPERATOR~%")
	(format stream " Name: ~A~%" name)
	(format stream " Prevails: ~A~%" prevails)
	(format stream " Effects: ~A~%" effects)
	(format stream " Cost: ~A~%" cost)
	(format stream ">~%")))

(defstruct (sas-effect (:print-function print-sas-effect))
  (variable nil)     ;; a number, indexing into the variable array
  (pre-val -1)      ;; a number, indexing into that variable's value array
  (eff-val -1)      ;; a number, indexing into that variable's value array
  (conditions nil)   ;; a 2D array of variable-value pairs that must be true for this effect.
  )

(defun print-sas-effect (sas-effect &optional (stream t) depth)
  (declare (ignore depth))
  (with-slots (variable pre-val eff-val conditions) sas-effect
	(format stream "SAS-EFFECT<")
	(format stream " Variable:~A," variable)
	(format stream " Pre:~A," pre-val)
	(format stream " Eff:~A," eff-val)
	(format stream " Conditions:~A" conditions)
	(format stream ">")))


(defstruct (sas-axiom (:print-function print-sas-axiom))
  (conditions nil)  ;; a 2D array of variable-value assignments		 
  (head nil)        ;; an array with three elements: the variable, pre-value, new-value
  )

(defun print-sas-axiom (sas-axiom &optional (stream t) depth)
  (declare (ignore depth))
  (with-slots (conditions head) sas-axiom
	(format stream "<SAS-AXIOM~%")
	(format stream " Conditions: ~A~%" conditions)
	(format stream " Head: ~A~%" head)
	(format stream ">~%")))
