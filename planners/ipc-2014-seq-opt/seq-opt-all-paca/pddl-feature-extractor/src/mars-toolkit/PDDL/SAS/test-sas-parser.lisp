(in-package #:sas)

(defun test-sas-parser ()
  (let ((sas-pathname (mars-pathname "PDDL/pddl-feature-extractor/tests/output.sas")))
	(parse-sas-file sas-pathname)
	nil))

(defun test-sas-parser-large-scanalyzer-sas ()
  (let ((sas-pathname (mars-pathname "PDDL/pddl-feature-extractor/tests/output-scanalyzer-p20.sas")))
	(parse-sas-file sas-pathname)
	nil))

