(in-package #:sas)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; SAS PARSER
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun parse-sas-file (sas-file-pathname)
  (with-open-file (stream sas-file-pathname)
	(parse-sas-stream stream)))

(defun parse-sas-stream (stream)
  (let ((sas (make-sas)))
	;; TODO - tune garbage collector
	;; #+allegro (setf (sys:gsgc-parameter :free-bytes-new-other) 5000000)
	;; #+allegro (setf (sys:gsgc-parameter :free-bytes-new-pages) 5000000)
	(let ((start (get-internal-real-time)))
	  (handler-case 
		  (with-slots (metric variables mutex init goal operators axioms) sas
			(parse-sas-version stream)
			(setf metric (parse-sas-metric stream))
			(setf variables (parse-sas-variables stream))
			(setf mutex (parse-sas-mutex stream))
			(setf init (parse-sas-init stream (array-dimension (sas-variables sas) 0)))
			(setf goal (parse-sas-goal stream))
			(setf operators (parse-sas-operators stream))
			(setf axioms (parse-sas-axiom stream))
			)
		(end-of-file ()
		  (format t "Error: Parsing of SAS ended prematurely.~%")
		  nil))
	(format t "SAS parsing time: ~6$ seconds~%" (/ (- (get-internal-real-time) start) internal-time-units-per-second)))
	sas))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; LOW LEVEL PARSERS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun parse-sas-version (stream)
  (let ((version 0))
	;; begin version
	(read-line stream t)
	;; version number
	(setf version (parse-integer (read-line stream t)))
	;; end version
	(read-line stream t) 
	version))

(defun parse-sas-metric (stream)
  (let ((metric 0))
	;; begin metric
	(read-line stream t)
	;; metric number
	(parse-integer (read-line stream t))
	;; end metric
	(read-line stream t)
	metric))

(defun parse-sas-variables (stream)
  (let* ((num-vars (parse-integer (read-line stream t)))
		 (vars (make-array num-vars)))
	(loop for i from 0 to (1- num-vars) do
		 (let ((var (make-sas-variable))
			   (num-vals 0))
		   (with-slots (name axiom-layer range) var
			 ;; begin variable
			 (read-line stream t)
			 ;; variable name
			 (setf name (read-line stream t))
			 ;; axiom layer
			 (setf axiom-layer (parse-integer (read-line stream t)))
			 ;; number of values in range
			 (setf num-vals (parse-integer (read-line stream t)))
			 (setf range (make-array num-vals))
			 ;; parse the range of this variable
			 (loop for v from 0 to (1- num-vals) do
				;; TODO: figure out how to parse the variable values.
				  (let ((line (read-line stream t)) 
						match 
						regs)
					(cond ((progn (multiple-value-setq (match regs)
									(cl-ppcre:scan-to-strings "^Atom\\s(.*)"  line))
								  match)
						   (setf (aref range v) (list 'atom (aref regs 0)))
						   )
						  ((progn (multiple-value-setq (match regs)
									(cl-ppcre:scan-to-strings "^<none of those>\\s*"  line))
								  match)
						   (setf (aref range v) '(none-of-those))
						   )
						  ((progn (multiple-value-setq (match regs)
									(cl-ppcre:scan-to-strings "^NegatedAtom\\s(.*)"  line))
								  match)
						   (setf (aref range v) (list 'negated-atom (aref regs 0)))
						   )
						  )
					)
				  )
			 ;; end variable
			 (read-line stream t)
			 ;; save off the variable
			 (setf (aref vars i) var)
			 ))
		 )
	vars))

(defun parse-sas-mutex (stream)
  (let* ((num-mutexes (parse-integer (read-line stream t)))
		 (mutexes (make-array num-mutexes)))
	(loop for i from 0 to (1- num-mutexes) do
		 (let ((mutex nil)
			   (num-facts 0))
		   ;; begin mutex group
		   (read-line stream t)
		   ;; number of number of facts in this mutex
		   (setf num-facts (parse-integer (read-line stream t)))
		   ;; parse the facts in this mutex
		   (setf mutex (parse-var-val-to-array stream num-facts))
		   ;; end mutex group
		   (read-line stream t)
		   ;; save off the mutex
		   (setf (aref mutexes i) mutex))
		 )
	mutexes))

(defun parse-sas-init (stream num-vars)
  (let ((init (make-array num-vars)))
	;; begin state
	(read-line stream t)
	;; metric number
	(loop for i from 0 to (1- num-vars) do
		 (setf (aref init i) (parse-integer (read-line stream t))))
	;; end state
	(read-line stream t)
	init))

(defun parse-sas-goal (stream)
  (let ((goal nil)
		(num-goals 0))
	;; begin goal
	(read-line stream t)
	;; number of goals
	(setf num-goals (parse-integer (read-line stream t)))
	;; parse goals
	(setf goal (parse-var-val-to-array stream num-goals))
	;; end goal
	(read-line stream t)
	goal))

(defun parse-sas-operators (stream)
  (let* ((num-ops (parse-integer (read-line stream t)))
		 (ops (make-array num-ops)))
	(loop for i from 0 to (1- num-ops) do
		 (when (eq (mod i 10000) 0)
		   (format t "  Parsing SAS op ~a.~%" i))
		 (let ((operator (make-sas-operator))
			   (num-prevails 0)
			   (num-effects 0))
		   (with-slots (name effects prevails cost) operator
			 ;; begin operator
			 (read-line stream t)
			 ;; operator name
			 (setf name (read-line stream t))
			 ;; number of prevail conditions in this operator
			 (setf num-prevails (parse-integer (read-line stream t)))
			 ;; parse the prevail conditions
			 (setf prevails (parse-var-val-to-array stream num-prevails))
			 ;; number of effects in this operator
			 (setf num-effects (parse-integer (read-line stream t)))
			 (setf effects (make-array num-effects))
			 ;; parse the effects
			 (loop for e from 0 to (1- num-effects) do
				  (setf (aref effects e) (parse-sas-effects (read-line stream t))))
			 ;; operator cost
			 (setf cost (parse-integer (read-line stream t)))
			 ;; end operator
			 (read-line stream t)
			 ;; save off the operator
			 (setf (aref ops i) operator)
		   ))
		 )
	ops))

(defun parse-sas-effects (effect-string)
  (let* ((eff-vals (parse-numerical-string effect-string))
		 (num-eff-vals (length eff-vals))
		 (num-eff-conds (first eff-vals))
		 (conditions (make-efficient-array `(,num-eff-conds 2))))
	#+allegro (declare (dynamic-extent eff-vals))
	;; populate an array of effect conditions
	(loop for nec from 0 to (1- num-eff-conds) by 2 do
		 (setf (aref conditions nec 0) (nth (+ (* nec 2) 1) eff-vals))
		 (setf (aref conditions nec 1) (nth (+ (* nec 2) 2) eff-vals)))
	;; get the variable, precondition value and effect value
	(let ((var (nth (- num-eff-vals 3)  eff-vals))
		  (pre-val (nth (- num-eff-vals 2)  eff-vals))
		  (eff-val (nth (- num-eff-vals 1)  eff-vals)))
	  (make-sas-effect :variable var :pre-val pre-val :eff-val eff-val :conditions conditions))
	))

(defun parse-sas-axiom (stream)
  (let* ((num-axioms (parse-integer (read-line stream t)))
		 (axioms (make-array num-axioms)))
	(loop for i from 0 to (1- num-axioms) do
		 (let ((axiom (make-sas-axiom))
			   (num-conds 0))
		   (with-slots (conditions head) axiom
			 ;; begin rule
			 (read-line stream t)
			 ;; number of conditions in the body of this rule
			 (setf num-conds (parse-integer (read-line stream t)))
			 ;; parse the  conditions
			 (setf conditions (parse-var-val-to-array stream num-conds))
			 ;; parse the head
			 (setf head (make-array :initial-contents (parse-numerical-string (read-line stream t))))
			 ;; end rule
			 (read-line stream t)
			 ;; save off the axiom
			 (setf (aref axioms i) axiom)
		   ))
		 )
	axioms))

(declaim (inline parse-numerical-string))
(defun parse-numerical-string (num-string)
  "Parses a string for space-separated numbers, and returns a list of those numbers."
  ;; (mapcar #'parse-integer (cl-utilities:split-sequence #\Space num-string))
  (parse-integer-string num-string))

(declaim (inline parse-integer-string))
(defun parse-integer-string (int-string)
  "Parses a string for space-separated numbers, and returns a list of those numbers."
  (let ((start 0)
		(length (length int-string))
		(int 0))
	(loop for c across int-string 
	   for i from 0 to length
	   when  (char= c #\Space)
	   collect (progn (setf int (parse-integer int-string :start start :end i))
					  (setf start (1+ i))
					  int) into ints
	   when (= i (1- length))
	   collect (parse-integer int-string :start start :end (1+ i)) into ints
	   finally (return ints))
	))

;; (defun parse-integer-string (int-string)
;;   "Parses a string for space-separated numbers, and returns a list of those numbers."
;;   (read-from-string (concatenate 'string "(" int-string ")")))

(declaim (inline make-efficient-array))
(defun make-efficient-array (dim)
  ;; TODO: we assume there will never be more than 2^31 SAS variables,
  ;; and that each variables has less than 2^31 values.
  (make-array dim
			  #+allegro :element-type #+allegro '(signed-byte 32)
			  #+allegro :allocation #+allegro :static-reclaimable))

(declaim (inline parse-var-val-to-array))
(defun parse-var-val-to-array (stream num)
  (let ((arr (make-efficient-array `(,num 2))))
	;; parse the  conditions
	(loop for n from 0 to (1- num) do
	   ;; get variable, value from string matching
		 (let ((numbers (parse-numerical-string (read-line stream t))))
		   #+allegro (declare (dynamic-extent numbers))
		   ;; save off variable and value
		   (setf (aref arr n 0) (first numbers))
		   (setf (aref arr n 1) (second numbers))
		   ))
	arr))