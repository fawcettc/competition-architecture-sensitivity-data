;;;; Copyright (c) 2013 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   David C. Wang

(in-package #:cl-user)

(defpackage #:pddl
  (:use #:cl #:cl-user #:mtk-utils-infinity #:mtk-utils #:abstract-data-types)
  (:export 

   ;; from pddl-def.lisp - convenience functions
   #:get-pddl-symbol
   #:get-keyword-symbol

   ;; from pddl-def.lisp - domain
   #:domain
   #:name #:domain-name
   #:requirements #:domain-requirements 
   #:add-domain-requirement! #:add-domain-requirements! 
   #:contains-domain-requirement? #:contains-domain-requirements? 
   #:num-domain-requirements #:list-domain-requirements 
   #:map-domain-requirements #:do-domain-requirements
   #:types #:domain-types 
   #:add-domain-type! #:add-domain-types! 
   #:contains-domain-type? #:contains-domain-types? 
   #:num-domain-types #:list-domain-types
   #:map-domain-types #:do-domain-types
   #:constants #:domain-constants
   #:add-domain-constant! #:add-domain-constants! 
   #:contains-domain-constant? #:contains-domain-constants? 
   #:num-domain-constants #:list-domain-constants
   #:map-domain-constants #:do-domain-constants
   #:predicates #:domain-predicates
   #:add-domain-predicate! #:add-domain-predicates! 
   #:contains-domain-predicate? #:contains-domain-predicates?
   #:num-domain-predicates #:list-domain-predicates 
   #:map-domain-predicates #:do-domain-predicates
   #:actions #:domain-actions
   #:add-domain-action! #:add-domain-actions! 
   #:contains-domain-action? #:contains-domain-actions?
   #:num-domain-actions #:list-domain-actions
   #:map-domain-actions  #:do-domain-actions
   #:make-domain
   #:domain-p

   ;; from pddl-def.lisp - action
   #:action
   #:name #:action-name
   #:parameters #:action-parameters
   #:precondition #:action-precondition
   #:effect #:action-effect
   #:make-action
   #:action-p
   ;; #:action-variables
   ;; #:action-components

    ;; from pddl-def.lisp - durative action
   #:durative-action
   #:name #:durative-action-name
   #:parameters #:durative-action-parameters
   #:duration #:durative-action-duration
   #:atstart-cond #:durative-action-atstart-cond
   #:overall-cond #:durative-action-overall-cond
   #:atend-cond #:durative-action-atend-cond
   #:atstart-effect #:durative-action-atstart-effect
   #:atend-effect #:durative-action-atend-effect
   #:make-durative-action
   #:durative-action-p
   ;; #:durative-action-variables
   ;; #:durative-action-components

   ;; from pddl-def.lisp - problem
   #:problem
   #:name #:problem-name
   #:domain #:problem-domain
   #:requirements #:problem-requirements
   #:add-problem-requirement! #:add-problem-requirements! 
   #:contains-problem-requirement? #:contains-problem-requirements?
   #:num-problem-requirements #:list-problem-requirements 
   #:map-problem-requirements #:do-problem-requirements
   #:objects #:problem-objects
   #:add-problem-object! #:add-problem-objects! 
   #:contains-problem-object? #:contains-problem-objects?
   #:num-problem-objects #:list-problem-objects
   #:map-problem-objects #:do-problem-objects
   #:init #:problem-init
   #:goal #:problem-goal
   #:make-problem
   #:problem-p

   ;; from pddl-def.lisp - predicates
   #:predicate-name
   #:predicate-typed-list
   #:predicate-vars
   #:predicate-types

   ;; from pddl-def.lisp - requirements
   #:*requirements* #:requirements-reductions
   #:check-requirements
   #:reduce-requirements


   ;; from pddl-def.lisp - typed list handling
   #:variables-typed-list
   #:types-typed-list

   process-pddl-typed-list process-adl-typed-list
   supertypeof supertypeof-list

   comp-precondition comp-effect comp-operator comp-name
   check-requirements 

   oneof uncertain iff observes forall exists
   


   ;; from pddl-parser.lisp - parsing functions
   #:*domains* #:*problems* #:*recent-domain* #:*recent-problem* 
   #:*use-domain-cache*
   #:clear-domain-cache
   #:get-domain
   #:get-action
   #:get-problem

   #:load-pddl
   #:define
   
   ;; from types.lisp
   #:pddl-type-graph
   #:make-pddl-type-graph
   #:num-types
   #:list-type-names
   #:list-type-objects
   #:map-types
   #:pddl-type
   #:name  #:type-name 
   #:direct-supertypes
   #:direct-subtypes 
   #:ensure-type-object
   #:get-type-object
   #:supertypes
   #:subtypes
   #:add-type
   #:union-type-graphs
   
   ;; from pddl-pretty-printer.lisp
   #:pprint-domain
   #:pprint-problem

   )
)

