(in-package :pddl)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; HELPFUL METHODS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun get-pddl-symbol (&rest names)
  "Returns a symbol interned in the PDDL package.
   NAME can be either a string or a symbol."
  (let ((capital-names (mapcar #'(lambda (name)
								   (cond ((stringp name)
										  (string-upcase name))
										 ((symbolp name)
										  (string-upcase (string name)))
										 (:otherwise
										  (format nil "~a" name))) ) 
							   names)))
	(setf capital-names (apply #'concatenate 'string capital-names))
	(intern capital-names 'pddl)))

(defun get-keyword-symbol (&rest names)
  "Returns a symbol interned in the PDDL package.
   NAME can be either a string or a symbol."
  (let ((capital-names (mapcar #'(lambda (name)
								   (cond ((stringp name)
										  (string-upcase name))
										 ((symbolp name)
										  (string-upcase (string name)))
										 (:otherwise
										  (format nil "~a" name))) ) 
							   names)))
	(setf capital-names (apply #'concatenate 'string capital-names))
	(intern capital-names :keyword)))

(defmacro create-hashset-convenience-functions (prefix name accessor)
  (let* ((sprefix (if (= (length (string prefix)) 0)
					  ""
					  (concatenate 'string (string-upcase (string prefix)) "-")))
		 (sname (string-upcase (string name)))
		 (add-name (intern (concatenate 'string "ADD-" sprefix sname "!")))  
		 (add-all-name (intern (concatenate 'string "ADD-" sprefix sname "S!")))
		 (contains-name (intern (concatenate 'string "CONTAINS-" sprefix sname "?")))
		 (contains-all-name (intern (concatenate 'string "CONTAINS-" sprefix sname "S?")))
		 (num-name (intern (concatenate 'string "NUM-" sprefix sname "S")))
		 (list-name (intern (concatenate 'string "LIST-" sprefix sname "S")))
		 (map-name (intern (concatenate 'string "MAP-" sprefix sname "S")))
		 (do-name (intern (concatenate 'string "DO-" sprefix sname "S")))
  		 )
	`(progn
	  (declaim (inline ',add-name))
	  (declaim (inline ',add-all-name))
	  (declaim (inline ',contains-name))
	  (declaim (inline ',contains-all-name))
	  (declaim (inline ',num-name))
	  (declaim (inline ',list-name))
	  (declaim (inline ',map-name))
	  (declaim (inline ',do-name))
	  (defun ,add-name (obj element &key (replace t))
		(adt::hs-add! (funcall ,accessor obj) element :replace replace))
	  (defun ,add-all-name (obj list-or-coll &key (replace t))
		(coll-add-all! (funcall ,accessor obj) list-or-coll :replace replace))
	  (defun ,contains-name (obj element &key (use-key-function t))
		(adt::hs-contains? (funcall ,accessor obj) element :use-key-function use-key-function))
	  (defun ,contains-all-name (obj element &key (use-key-function t))
		(coll-contains-all? (funcall ,accessor obj) element :use-key-function use-key-function))
	  (defun ,num-name (obj)
		(adt::hs-size (funcall ,accessor obj)))
	  (defun ,list-name (obj)
		(adt::hs-tolist (funcall ,accessor obj)))
	  (defun ,map-name (obj f)
		(adt::hs-map (funcall ,accessor obj) f))
	  (defmacro ,do-name ((obj elem &optional (i nil i-supplied-p)) &rest rest)
		`(if ,i-supplied-p
			 (docoll ((funcall ,,accessor ,obj) ,elem ,i) ,@rest)
			 (docoll ((funcall ,,accessor ,obj) ,elem) ,@rest) ))
	  ))
  )


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; DOMAIN INTERFACE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defstruct (domain (:print-function print-domain))
  (name nil)         ; symbol - the domain name.
  (requirements (make-hashset)) ; list of symbols - each symbol is the name of a requirement
  (types (make-pddl-type-graph))		 ; an assoc list of (type . '(super-types))
  (constants (make-hashset :key-function #'car))    ; list of lists - each element of constants
										; is a conjunctive clause that delcares the 
										; appropriate constant.
  (predicates (make-hashset :key-function #'car))   ; list of predicates
  (actions (make-hashset :key-function #'action-name))    ; list of actions
  ;; (problems '(make-hashset :key-function #'problem-name))     ; list of problems associated with this domain
  )

(create-hashset-convenience-functions domain requirement #'domain-requirements)
(create-hashset-convenience-functions domain constant #'domain-constants)
(create-hashset-convenience-functions domain predicate #'domain-predicates)
(create-hashset-convenience-functions domain action #'domain-actions)


(defun print-domain (domain &optional (stream t) depth)
  (declare (ignore depth))
  (format stream "<DOMAIN~%")
  (format stream " Name: ~A~%" (domain-name domain))
  (format stream " Requirements: ~A~%" (domain-requirements domain))
  (format stream " Types: ~A~%" (domain-types domain))
  (format stream " Constants: ~A~%" (domain-constants domain))
  (format stream " Predicates: ~A~%" (domain-predicates domain))
  (format stream " Actions: ~%")
  (map-domain-actions domain
					  #'(lambda (action) (format stream "   <ACTION: ~a>~%" (action-name action))) )
  ;; (format stream " Problems: ~A~%" (domain-problems domain))
  (format stream ">~%"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; PROBLEM INTERFACE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defstruct (problem (:print-function print-problem))
  (name nil)              ; symbol - the problem name.
  (domain-name nil)            ; symbol - the name of the domain to which this problem belongs.
  (requirements (make-hashset))      ; hashset of symbols - each symbol is the name of a requirement
  (objects (make-hashset))			 ; hashet of props, like init, that
						  ;  are the objects in this problem
  (init '(and))              ; list of predicates
  (goal '())              ; list of goals
  )

(create-hashset-convenience-functions problem requirement #'problem-requirements)
(create-hashset-convenience-functions problem object #'problem-objects)

(defun print-problem (problem &optional (stream t) depth)
  (declare (ignore depth))
  (format stream "<PROBLEM~%")
  (format stream " Name: ~A~%" (problem-name problem))
  (format stream " Domain: ~A~%" (problem-domain-name problem))
  (format stream " Requirements: ~A~%" (problem-requirements problem))
  (format stream " Objects: ~A~%" (problem-objects problem))
  (format stream " Init: ~A~%" (problem-init problem))
  (format stream " Goal: ~A~%"  (problem-goal problem))
  (format stream ">~%")
  )


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; ACTION INTERFACE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defstruct (action (:print-function print-action))
  name
  (parameters nil)
  (precondition nil)
  (effect nil)

  variables               ; derived from parameters
  (components nil)        ; The components derived from this action.
                          ; This list is computed at plan time, because
				          ; we wish to unroll universally quantified 
					      ; effects, and the universal base is known only
					      ; at plan time.
  )

(defun print-action (action &optional (stream t) depth)
  (declare (ignore depth))
  (with-slots (name parameters precondition effect) action
	(format stream "<ACTION:~%")
	(format stream " Name: ~A~%" name)
	(format stream " Parameters: ~A~%" parameters)
	(format stream " Precondition: ~A~%" precondition)
	(format stream " Effect: ~A~%" effect)
	(format stream ">~%")))


(defstruct (durative-action (:print-function print-durative-action))
  name
  (parameters nil)
  (duration nil)
  (atstart-cond nil)
  (overall-cond nil)
  (atend-cond nil)
  (atstart-effect nil)
  (atend-effect nil)

  variables               ; derived from parameters
  (components nil)        ; The components derived from this action.
                          ; This list is computed at plan time, because
				          ; we wish to unroll universally quantified 
					      ; effects, and the universal base is known only
					      ; at plan time.
  )

(defun print-durative-action (action &optional (stream t) depth)
  (declare (ignore depth))
  (with-slots (name parameters duration atstart-cond overall-cond atend-cond atstart-effect atend-effect) action
	(format stream "<DURATIVE-ACTION:~%")
	(format stream " Name: ~A~%" name)
	(format stream " Parameters: ~A~%" parameters)
	(format stream " Duration: ~A~%" duration)
	(format stream " At Start Condition: ~A~%" atstart-cond)
	(format stream " Over All Condition: ~A~%" overall-cond)
	(format stream " At End Condition: ~A~%" atend-cond)
	(format stream " At Start Effect: ~A~%" atstart-effect)
	(format stream " At End Effect: ~A~%" atend-effect)
	(format stream ">~%")))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; PREDICATE INTERFACE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(declaim (inline predicate-name))
(defun predicate-name (predicate)
  (car predicate))

(declaim (inline predicate-typed-list))
(defun predicate-typed-list (predicate)
  (rest predicate))

(declaim (inline predicate-vars))
(defun predicate-vars (predicate)
  (variables-typed-list (rest predicate)))

(declaim (inline predicate-types))
(defun predicate-types (predicate)
  (types-typed-list (rest predicate)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; REQUIREMENTS INTERFACE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun check-requirements (capabilities problem domain)
  (let* ((domain-requirements (domain-requirements domain))
         (problem-requirements (problem-requirements problem))
         (trouble
          (set-difference (reduce-requirements (union problem-requirements
                                                      domain-requirements))
                          (reduce-requirements capabilities))
		   ))
    (when trouble
      (warn "Planner is unlikely to work on ~A because of requirement
~{~S ~}
in the domain or problem description." (problem-name problem) trouble))

    (null trouble)))

(defparameter *requirement-reductions*
  '((:quantified-preconditions :existential-preconditions :universal-preconditions)
	(:adl :strips :typing :negative-preconditions :disjunctive-preconditions
	      :equality :quantified-preconditions :conditional-effects)
	;; this last requirement reduction is only standard to the language 
    ;;  accepted by the UCPOP algorithm, not to the PDDL language
    (:ucpop :adl :domain-axioms :procedural-attachments :safety-constraints))
  "An association list, in which the first element of each list is the super-requirement,
   The remaining elements in the list are the requirements that super-requirement expands into.")

(defparameter *requirements*
  '(:strips :typing :negative-preconditions :disjunctive-preconditions :equality :existential-preconditions :universal-preconditions :quantified-preconditions :condititional-effects :fluents :numeric-fluents :adl :durative-actions :duration-inequalities :continuous-effects :derived-predicates :timed-initial-literals :preferences :constraints :action-costs))

(defun reduce-requirements (reqs)
  "Given a list of requirements REQS, returns a list of base requirements.
   Or, each requirement in the returned list cannot be expanded into any 
   set of other requirements."
  (loop for req in reqs nconc
        (let ((reduction (assoc req *requirement-reductions*)))
          (if reduction 
            (reduce-requirements (rest reduction))
            (list req)))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; TYPED lIST HANDLING INTERFACE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; (defun clausify-typed-list (typed-list)
;;   "Returns a set of logical clauses represents a typed-list that 
;;    has the form ((var type type) (var type)) in clausal form."
;;   (cons 'and  (mapcar #'(lambda (var-types) 
;; 						  (cons 'or (mapcar #'(lambda (type) (list type (car var-types))) (rest var-types)))) 
;; 					  typed-list)))

(defun variables-typed-list (typed-list) 
  "Returns the variables described in the typed-list."
  (mapcar #'(lambda (var-types) (first var-types)) typed-list))

(defun types-typed-list (typed-list) 
  "Returns the types used in the typed-list."
  (delete-duplicates (mapcan #'(lambda (var-types) (copy-list (rest var-types))) typed-list)))

;; (defun make-typed-pair-from-typed-list (typed-list)
;;   (mapcan #'(lambda (typed-element)
;; 			  (mapcar #'(lambda (type) (list (first typed-element) type)) (rest typed-element))) 
;; 		  typed-list))

;; (defun simplify-typed-list (typed-list)
;;   (let ((supertypeof-table (make-hash-table))
;; 		(new-typed-list '()))
;; 	(map nil #'(lambda (typed-element) 
;; 				 (let ((type (first typed-element))
;; 					   (supertypes (rest typed-element)))
;; 				   (setf (gethash type supertypeof-table) (union supertypes (gethash type supertypeof-table nil))))) 
;; 		 typed-list)
;; 	(loop for type being the hash-key in supertypeof-table using (hash-value supertypes) do
;; 		 (push (cons type supertypes) new-typed-list))
;; 	new-typed-list))

;; (defun supertypeof-list (typed-list &key (include-object t))
;;   "Given a TYPED-LIST of the form ((type supertype) (type supertype supertype)),
;;    returns a typed-list of the same form, where all sypertypes for a given type
;;    are explicitly listed. i.e. ((a b)(b c)) -> ((a b c)(b c)(a object)(b object)(c object))."
;;   (let ((types (reduce #'union typed-list))
;; 		(typeof (make-hashmultimap :make-coll '(make-hashset)))
;; 		(typed-pairs (make-typed-pair-from-typed-list typed-list)))
;; 	;; populate typed-pairs with the object type
;; 	(setf typed-pairs (nconc typed-pairs (mapcar #'(lambda (type) (list type 'object)) types)))
;; 	;; populate the typeof table with supertypes for each type
;; 	;; we start from the root of the type-tree and descend.
;; 	(let (;; queue maintains a list of nodes in the tree whose parents have already been expanded.
;; 		  (queue '(object))
;; 		  ;; a loop variable, that maintains the current node in the tree.
;; 		  (curr-type nil))
;; 	  (loop until (null typed-pairs) do
;; 		   (setf curr-type (pop queue))
;; 		   (setf typed-pairs
;; 				 (loop for (subtype supertype) in typed-pairs append
;; 					  (if (eql curr-type supertype)
;; 						  (progn (push subtype queue)
;; 								 (map-add! typeof subtype supertype) 
;; 								 (when (map-get typeof curr-type)
;; 								   (coll-map (map-get typeof curr-type)
;; 											 (lambda (supertype) (map-add! typeof subtype supertype))))
;; 								 nil)
;; 						  `((,subtype ,supertype)))
;; 					  ))
;; 		   ))
;; 	;; produce the returned list of the form ((type supertype supertype) (type supertype))
;; 	(let ((typeof-list '())
;; 		  (supertype-list '()))
;; 	  (loop for type being the hash-keys in (abstract-data-types::hmm-table typeof) using (hash-value supertypes) do
;; 		   (setf supertype-list (if supertypes (coll-tolist supertypes)	nil))
;; 		   (push (cons type (if include-object supertype-list (delete 'object supertype-list)))
;; 				 typeof-list))
;; 	  typeof-list)
;; 	))

;; (defun supertypeof (type typed-list &key (include-object t))
;;   "Given a TYPED-LIST of the form ((type supertype)(type supertype supertype)),
;;    returns all of the supertypes of TYPE as a list."
;;   (let ((typed-list (simplify-typed-list typed-list))
;; 		(queue (list type))
;; 		(expanded '())
;; 		(curr-type nil)
;; 		(allsupertypes '()))
;; 	(loop until (null queue) do
;; 		 (setf curr-type (pop queue))
;; 		 (unless (member curr-type expanded)
;; 		   (setf typed-list
;; 				 (loop for typed-element in typed-list append
;; 					  (let ((subtype (first typed-element))
;; 							(supertypes (rest typed-element)))
;; 						(if (eql curr-type subtype)
;; 							(progn (setf queue (append queue supertypes))
;; 								   (setf allsupertypes (union allsupertypes supertypes))
;; 								   (push subtype expanded)
;; 								   nil)
;; 							`(,(cons subtype supertypes)))))
;; 				 )))
;; 	(if include-object 
;; 		(cons 'object allsupertypes)
;; 		allsupertypes)))

				 
;; subtypes is a list of atomic types.  super-types
;; is either an atomic type or a list of 'either
;; followed by one or more super-types.

;; (defun expand-super (subtypes super-types)
;;   (cond ((null super-types) nil)
;; 	((not (consp super-types))
;; 	 (mapcar #'(lambda (sub) (list super-types sub)) subtypes))
;; 	((eq (first super-types) 'either)
;; 	 (expand-super subtypes (cdr super-types)))
;; 	(t (append (expand-super subtypes (car super-types))
;; 		   (expand-super subtypes (cdr super-types))))))


;; Compute the list of all reachable supertypes from the given
;; subtype.
;; (defun compute-reachable (sub supers type-pairs all-type-pairs)
;;   (cond ((null type-pairs) supers)
;; 		((and (eq (first (car type-pairs)) sub)
;; 			  (not (member (second (car type-pairs)) supers)))
;; 		 ;; Recursively descend on supertype.
;; 		 (union
;; 		  (compute-reachable (second (car type-pairs))
;; 							 (cons (second (car type-pairs)) supers)
;; 							 all-type-pairs all-type-pairs)
;; 		  (compute-reachable sub supers (cdr type-pairs) all-type-pairs)))
;; 		(t (compute-reachable sub supers (cdr type-pairs) all-type-pairs))))

;; (defun simple-invert (p)
;;   (if (eq (first p) 'not) (second p)
;;     (list 'not p)))

