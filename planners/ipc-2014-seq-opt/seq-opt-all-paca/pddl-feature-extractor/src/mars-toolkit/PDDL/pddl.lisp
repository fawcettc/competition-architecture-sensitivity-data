(in-package :pddl)

;; Forward Declarations
;; (defgeneric domain-name (domain))
;; (defgeneric problem-name (problem))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; HELPFUL METHODS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(defun get-pddl-symbol (&rest names)
  "Returns a symbol interned in the PDDL package.
   NAME can be either a string or a symbol."
  (let ((capital-names (mapcar #'(lambda (name)
								   (cond ((stringp name)
										  (string-upcase name))
										 ((symbolp name)
										  (string-upcase (string name)))
										 (:otherwise
										  (format nil "~a" name))) ) 
							   names)))
	(setf capital-names (apply #'concatenate 'string capital-names))
	(intern capital-names 'pddl)))

(defun get-keyword-symbol (&rest names)
  "Returns a symbol interned in the PDDL package.
   NAME can be either a string or a symbol."
  (let ((capital-names (mapcar #'(lambda (name)
								   (cond ((stringp name)
										  (string-upcase name))
										 ((symbolp name)
										  (string-upcase (string name)))
										 (:otherwise
										  (format nil "~a" name))) ) 
							   names)))
	(setf capital-names (apply #'concatenate 'string capital-names))
	(intern capital-names :keyword)))

(defmacro create-hashset-convenience-functions (prefix name accessor)
  (let* ((sprefix (if (= (length (string prefix)) 0)
					  ""
					  (concatenate 'string (string-upcase (string prefix)) "-")))
		 (sname (string-upcase (string name)))
		 (add-name (intern (concatenate 'string "ADD-" sprefix sname "!")))  
		 (add-all-name (intern (concatenate 'string "ADD-" sprefix sname "S!")))
		 (contains-name (intern (concatenate 'string "CONTAINS-" sprefix sname "?")))
		 (contains-all-name (intern (concatenate 'string "CONTAINS-" sprefix sname "S?")))
		 (num-name (intern (concatenate 'string "NUM-" sprefix sname "S")))
		 (list-name (intern (concatenate 'string "LIST-" sprefix sname "S")))
		 (map-name (intern (concatenate 'string "MAP-" sprefix sname "S")))
		 (do-name (intern (concatenate 'string "DO-" sprefix sname "S")))
  		 )
	`(progn
	  (declaim (inline ',add-name))
	  (declaim (inline ',add-all-name))
	  (declaim (inline ',contains-name))
	  (declaim (inline ',contains-all-name))
	  (declaim (inline ',num-name))
	  (declaim (inline ',list-name))
	  (declaim (inline ',map-name))
	  (declaim (inline ',do-name))
	  (defun ,add-name (obj element &key (replace t))
		(adt::hs-add! (funcall ,accessor obj) element :replace replace))
	  (defun ,add-all-name (obj list-or-coll &key (replace t))
		(coll-add-all! (funcall ,accessor obj) list-or-coll :replace replace))
	  (defun ,contains-name (obj element &key (use-key-function t))
		(adt::hs-contains? (funcall ,accessor obj) element :use-key-function use-key-function))
	  (defun ,contains-all-name (obj element &key (use-key-function t))
		(coll-contains-all? (funcall ,accessor obj) element :use-key-function use-key-function))
	  (defun ,num-name (obj)
		(adt::hs-size (funcall ,accessor obj)))
	  (defun ,list-name (obj)
		(adt::hs-tolist (funcall ,accessor obj)))
	  (defun ,map-name (obj f)
		(adt::hs-map (funcall ,accessor obj) f))
	  (defmacro ,do-name ((obj elem &optional (i nil i-supplied-p)) &rest rest)
		`(if ,i-supplied-p
			 (docoll ((funcall ,,accessor ,obj) ,elem ,i) ,@rest)
			 (docoll ((funcall ,,accessor ,obj) ,elem) ,@rest) ))
	  ))
  )


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; PDDL PARSING INTERFACE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun load-pddl (pddl-file-pathname &key (verbose nil))
  "Loads the given PDDL file. The PDDL file can contain the domain, problem, 
   or both domain and problem definitions."
	(let ((*package* (find-package :pddl)))
	  (load pddl-file-pathname :verbose verbose)))

;;; Functions that accept domain and problem definitions in UCPOP format

(defmacro DEFINE ((dtype name) &body body)
  (case dtype
    (domain `(setf *recent-domain* (apply #'def-domain '(,name ,@body))))
    (problem `(setf *recent-problem* (apply #'def-problem '(,name ,@body))))
    ))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; DOMAIN support code
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defstruct (domain (:print-function print-domain))
  (name nil)         ; symbol - the domain name.
  (requirements (make-hashset)) ; list of symbols - each symbol is the name of a requirement
  (types (make-pddl-type-graph))		 ; an assoc list of (type . '(super-types))
  (constants (make-hashset :key-function #'car))    ; list of lists - each element of constants
										; is a conjunctive clause that delcares the 
										; appropriate constant.
  (predicates (make-hashset :key-function #'car))   ; list of predicates
  (actions (make-hashset :key-function #'action-name))    ; list of actions
  ;; (problems '(make-hashset :key-function #'problem-name))     ; list of problems associated with this domain
  )

(create-hashset-convenience-functions domain requirement #'domain-requirements)
(create-hashset-convenience-functions domain constant #'domain-constants)
(create-hashset-convenience-functions domain predicate #'domain-predicates)
(create-hashset-convenience-functions domain action #'domain-actions)

(defun print-domain (domain &optional (stream t) depth)
  (declare (ignore depth))
  (format stream "<DOMAIN~%")
  (format stream " Name: ~A~%" (domain-name domain))
  (format stream " Requirements: ~A~%" (domain-requirements domain))
  (format stream " Types: ~A~%" (domain-types domain))
  (format stream " Constants: ~A~%" (domain-constants domain))
  (format stream " Predicates: ~A~%" (domain-predicates domain))
  (format stream " Actions: ~%")
  (map-domain-actions domain
					  #'(lambda (action) (format stream "   <ACTION: ~a>~%" (action-name action))) )
  ;; (format stream " Problems: ~A~%" (domain-problems domain))
  (format stream ">~%"))

(defun def-domain (name &rest clauses)
  (let ((domain (make-domain :name name))
        (warnings nil))
	(with-slots (requirements types constants predicates actions) domain
	  (dolist (clause clauses)
		  (case (car clause)
			(:extends
	      ;;; This domain inherits actions, constants, etc. from one or
	      ;;; more other domains.
			 (let ((extended-domains (mapcar #'(lambda (name) (get-domain name))
											 (cdr clause))))
			   (dolist (domain extended-domains)
				 ;; Inherit requirements.
				 (coll-add-all! requirements (domain-requirements domain) :replace nil)
				 ;; Inherit types.
				 (setf types (union-type-graphs types (domain-types domain) :copy nil))
				 ;; Inherit constants.
				 (coll-add-all! constants (domain-constants domain) :replace nil)
				 ;; Inherit predicates.
				 (coll-add-all! predicates (domain-predicates domain) :replace nil)
				 ;; Inherit actions.
				 (coll-add-all! actions (domain-actions domain) :replace nil)
				 )
			   ))
			(:requirements 
			 (coll-add-all! requirements (cdr clause) :replace t))
			(:types
			 (setf types (union-type-graphs types (parse-types (cdr clause)) :copy nil)))
			(:constants
			 (coll-add-all! constants (parse-pddl-typed-list (cdr clause)) :replace t))
			(:predicates
			 (map nil 
				  #'(lambda (predicate) (coll-add! predicates (parse-predicate predicate) :replace t))
				  (cdr clause))	 )
			(:action
			 (coll-add! actions (apply #'def-action (cdr clause)) :replace t) )
			(:durative-action
			 (coll-add! actions (apply #'def-durative-action (cdr clause)) :replace t) )
			(t (pushnew (car clause) warnings)))))
    (when warnings
      (format t "~&;~4TWarning: unable to process parameter(s), ~{~S, ~}in domain ~A~%" 
			  warnings name))
	;; Now that we have been successful at parsing
	;; Let's save off the domain
	(setf *recent-domain* domain)
	(coll-add! *domains* domain)
	domain))


(defun get-domain (name)
  "Finds and returns the domain object named NAME. 
   NAME can be either a String or Symbol.
   If no domain is found, nil will be returned."
  (cond (;; if arg is a string, convert to symbol
		 (stringp name) (setf name (intern name)))
		(;; if arg is a symbol, do nothing
		 (symbolp name))
		(:otherwise
		 (error "Function get-domain does not recognize the argument ~a~%" name)))
  (coll-get *domains* name :use-key-function nil :default nil))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; ACTION support code
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defstruct (action (:print-function print-action))
  name
  (parameters nil)
  (precondition nil)
  (effect nil)

  variables               ; derived from parameters
  (components nil)        ; The components derived from this action.
                          ; This list is computed at plan time, because
				          ; we wish to unroll universally quantified 
					      ; effects, and the universal base is known only
					      ; at plan time.
  )

(defun print-action (action &optional (stream t) depth)
  (declare (ignore depth))
  (with-slots (name parameters precondition effect) action
	(format stream "<ACTION:~%")
	(format stream " Name: ~A~%" name)
	(format stream " Parameters: ~A~%" parameters)
	(format stream " Precondition: ~A~%" precondition)
	(format stream " Effect: ~A~%" effect)
	(format stream ">~%")))

(defun def-action (name &key parameters precondition effect)
  (let* ((typed-params (parse-pddl-typed-list parameters))
		 (variables typed-params)) ;; TODO: What is VARIABLES used for?
    ;; Create the new action
	(make-action :name name
				 :parameters typed-params
				 :precondition precondition
				 :effect effect
				 :variables variables)
	))

(defstruct (durative-action (:print-function print-durative-action))
  name
  (parameters nil)
  (duration nil)
  (atstart-cond nil)
  (overall-cond nil)
  (atend-cond nil)
  (atstart-effect nil)
  (atend-effect nil)

  variables               ; derived from parameters
  (components nil)        ; The components derived from this action.
                          ; This list is computed at plan time, because
				          ; we wish to unroll universally quantified 
					      ; effects, and the universal base is known only
					      ; at plan time.
  )

(defun print-durative-action (action &optional (stream t) depth)
  (declare (ignore depth))
  (with-slots (name parameters duration atstart-cond overall-cond atend-cond atstart-effect atend-effect) action
	(format stream "<DURATIVE-ACTION:~%")
	(format stream " Name: ~A~%" name)
	(format stream " Parameters: ~A~%" parameters)
	(format stream " Duration: ~A~%" duration)
	(format stream " At Start Condition: ~A~%" atstart-cond)
	(format stream " Over All Condition: ~A~%" overall-cond)
	(format stream " At End Condition: ~A~%" atend-cond)
	(format stream " At Start Effect: ~A~%" atstart-effect)
	(format stream " At End Effect: ~A~%" atend-effect)
	(format stream ">~%")))

(defun def-durative-action (name &key parameters duration condition effect)
  (let*  ((typed-params (parse-pddl-typed-list parameters))
		  (variables typed-params))
	(multiple-value-bind (atstart-cond overall-cond atend-cond) (separate-durative-conditions condition)
	  (multiple-value-bind (atstart-effect dummy atend-effect) (separate-durative-conditions effect)
		(declare (ignore dummy))
		;; Create the new action
		(make-durative-action :name name
							  :parameters typed-params
							  :duration duration
							  :atstart-cond (coll-tolist atstart-cond :copy nil)
							  :overall-cond (coll-tolist overall-cond :copy nil)
							  :atend-cond (coll-tolist atend-cond :copy nil)
							  :atstart-effect (coll-tolist atstart-effect :copy nil)
							  :atend-effect (coll-tolist atend-effect :copy nil)
							  :variables variables
							  )))
    ))

(defun separate-durative-conditions (precs &optional 
									 (atstart (make-smart-list :initial-contents '(and) :copy t)) 
									 (overall (make-smart-list :initial-contents '(and) :copy t)) 
									 (atend (make-smart-list :initial-contents '(and) :copy t)) )
  (cond (;; do nothing
		 (null precs))
		(;; do nothing
		 (symbolp precs))
		(;; recognize at start
		 (and (listp precs) (eq (first precs) 'at) (eq (second precs) 'start))
		 (coll-add! atstart (third precs)))
		(;; recognize over all
		 (and (listp precs) (eq (first precs) 'over) (eq (second precs) 'all))
		 (coll-add! overall (third precs)))
		(;; recognize at end
		 (and (listp precs) (eq (first precs) 'at) (eq (second precs) 'end))
		 (coll-add! atend (third precs)))
		(;; loop over the list
		 (listp precs)
		 (loop for prec in precs do 
			  (separate-durative-conditions prec atstart overall atend)) )
		(;; do nothing
		 :otherwise) )
  (values atstart overall atend)
  )

;; (defun group-durative-precs (precs)
;;   (let ((over-all '())
;; 		(at-start '())
;; 		(at-end '()))
;; 	;; add an and if the precs does not start with and
;; 	(unless (eql (car precs) 'and)
;; 	  (setf precs `(and ,precs)))
;; 	;; loop for conditions
;; 	(loop for prec in (cdr precs) do
;; 		 (case (car prec)
;; 		   (over (push (caddr prec) over-all))
;; 		   (at (case (cadr prec)
;; 				 (start (push (caddr prec) at-start))
;; 				 (end (push (caddr prec) at-end))))))
;; 	(list (cons 'over-all over-all) 
;; 		  (cons 'at-start at-start)
;; 		  (cons 'at-end at-end))  ))


;; (reorder-precs 
;;  (preprocess-precs precondition))


(defun reorder-precs (preconditions)
  ;; Accumulate all the equality and inequality constraints from the
  ;; preconditions.  Put them at the end of the list of preconditions.
  ;; Right before the (in)equality constraints come the negated
  ;; propositions.
  (cond ((null preconditions) nil)
		((not (listp preconditions)) preconditions)
		((eq (car preconditions) 'and)
		 ;; Move all the eq and ineq conjuncts to the end
		 (cons 'and
			   (sort (mapcar #'reorder-precs (cdr preconditions)) #'prec-order)))
		(t (cons (car preconditions)
				 (mapcar #'reorder-precs (cdr preconditions))))))
	 
(defun prec-ordinal (p)
  (cond ((not (listp p)) 0)		; Atoms go first
		((eq (car p) '=) 2)		; = and not = are last
		((and (eq (car p) 'not)
			  (eq (caadr p) '=)) 2)
		((eq (car p) 'not) 1)		; Second are negated props
		(t 0)))				; Positive propositions also lead off

(defun prec-order (p1 p2)
  (< (prec-ordinal p1) (prec-ordinal p2)))

(defun preprocess-precs (precs)
  (cond ((null precs) precs)
		((not (listp precs)) precs)
		((eq (car precs) 'imply)
		 (list 'or
			   (list 'not (preprocess-precs (second precs)))
			   (preprocess-precs (third precs))))
		(t (mapcar #'preprocess-precs precs))))

;; (defun remove-durative-precs (precs)
;;   (let ((new-precs 'nil))
;; 	;; add an and if the precs does not start with and
;; 	(unless (eql (car precs) 'and)
;; 	  (setf precs `(and ,precs)))
;; 	;; loop for conditions that start with and
;; 	(loop for prec in (cdr precs) do
;; 		 (pushnew (caddr prec) new-precs :test #'equalp))
;; 	`(and ,@(reverse new-precs))))

(defmethod get-action ((name symbol)(domain domain))
  "Returns the actions named by the symbol NAME from the DOMAIN object."
  (coll-get (domain-actions domain) name))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; PROBLEM support code
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defstruct (problem (:print-function print-problem))
  (name nil)              ; symbol - the problem name.
  (domain-name nil)            ; symbol - the name of the domain to which this problem belongs.
  (requirements (make-hashset))      ; hashset of symbols - each symbol is the name of a requirement
  (objects (make-hashset))			 ; hashet of props, like init, that
						  ;  are the objects in this problem
  (init '(and))              ; list of predicates
  (goal '())              ; list of goals
  )

(create-hashset-convenience-functions problem requirement #'problem-requirements)
(create-hashset-convenience-functions problem object #'problem-objects)

;;;The problem structure remembers the domain, goals, and initial conditions
;;;associated with a single problem instance.  Don't try to reuse these.  Create
;;;it, use it and throw it away.

(defun print-problem (problem &optional (stream t) depth)
  (declare (ignore depth))
  (format stream "<PROBLEM~%")
  (format stream " Name: ~A~%" (problem-name problem))
  (format stream " Domain: ~A~%" (problem-domain-name problem))
  (format stream " Requirements: ~A~%" (problem-requirements problem))
  (format stream " Objects: ~A~%" (problem-objects problem))
  (format stream " Init: ~A~%" (problem-init problem))
  (format stream " Goal: ~A~%"  (problem-goal problem))
  (format stream ">~%")
  )

(defun def-problem (name &rest clauses)
  (let ((problem (make-problem :name name))
		(warnings nil)
		)
	(with-slots (domain-name requirements objects init goal) problem
		(dolist (clause clauses)
		  (case (car clause)
			(:domain (setf domain-name (second clause)))
			(:requirements (coll-add-all! requirements (rest clause) :replace t))
			(:objects (coll-add-all! objects (parse-pddl-typed-list (cdr clause)) :replace t))
			(:init (setf init (append '(and) (rest clause))))
			(:goal (setf goal (preprocess-precs (second clause))))
			(:length nil)			; Just ignore this piece of information
			(t (pushnew (car clause) warnings))))
	  
	  (when warnings
		(format t "~&;~4TWarning: unable to process parameter(s), ~{~S, ~}in problem ~A~%"
				warnings name))

	  (setf *recent-problem* problem)
	  ;; save off this problem
	  (coll-add! *problems* problem)
	  ;; add this problem to the domain  
	  problem)))

(defun get-problem (name)
  "Finds and returns the problem object named NAME. 
   NAME can be either a String or Symbol.
   If no problem is found, nil will be returned."
  (cond (;; if arg is a string, convert to symbol
		 (stringp name) (setf name (intern name)))
		(;; if arg is a symbol, do nothing
		 (symbolp name))
		(:otherwise
		 (error "Function get-problem does not recognize the argument ~a~%" name)))
  (coll-get *problems* name :default nil))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; PREDICATE METHODS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(declaim (inline parse-predicate))
(defun parse-predicate (pddl-predicate)
  "Parses a PDDL predicate and returns a list-based representation of the predicate:
   (predicate (var1 type-obj) (var2 type-obj) ... (varn type-obj)"
  (cons (car pddl-predicate) (parse-pddl-typed-list (cdr pddl-predicate)))
  )

(declaim (inline predicate-name))
(defun predicate-name (predicate)
  (car predicate))

(declaim (inline predicate-typed-list))
(defun predicate-typed-list (predicate)
  (rest predicate))

(declaim (inline predicate-vars))
(defun predicate-vars (predicate)
  (variables-typed-list (rest predicate)))

(declaim (inline predicate-types))
(defun predicate-types (predicate)
  (types-typed-list (rest predicate)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; TYPE HANDLING
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(defun parse-types (pddl-types-list)
  "Iteratively translates PDDL style typed list into a pddl type-graph."
  (let ((type-graph (make-pddl-type-graph))
		(untyped-types '())
		(mode 'type)) ;; 'type - looking for type,  'super - looking for a supertype
	(loop for typed-element in pddl-types-list do
		 (cond (;; we have received a type delimiter
				(eql typed-element '-)
				(setf mode 'super))
			   (;; a type delimiter has been received, this element must be the type
				(eql mode 'super) (setf mode 'type)
				(let (supertype)
				  ;; check if the type is a list that starts with "either" or 
				  ;; if it is a singular type
				  (cond (;; this is a singular type
						 (not (consp typed-element))
						 (setf supertype (list typed-element)))
						(;; this should be an either type
						 (or (eq (car typed-element) :either) (eq (car typed-element) 'either))
						 (setf supertype (rest typed-element)))
						(:otherwise (format t "Warning: unable to parse typed-list ~A. Some types or supertypes were skipped~%" pddl-types-list)))
				  ;; add the types to the type graph with supertypes
				  (mapcar #'(lambda (type) (add-type type supertype nil type-graph)) untyped-types)
				  ;; erase the list of untyped types
				  (setf untyped-types nil)
				  ))
			   (; look for more types
				(eql mode 'type)
				(push typed-element untyped-types))
			   )
		 )
	;; return all the typed and untyped variables
	(mapcar #'(lambda (type) (add-type type nil nil type-graph)) untyped-types)
	type-graph))

(defun parse-pddl-typed-list (pddl-typed-list)
  "Iteratively translates PDDL style typed list into a list of elements of the form
  ((var type) (var type))."
  (let ((typed-vars (make-smart-list))
		(untyped-vars (make-smart-list))
		(mode 'var)) ;; 'var - looking for variables,  'type - looking for a type
	(loop for typed-element in pddl-typed-list do
		 (cond (;; we have received a type delimiter
				(eql typed-element '-)
				(setf mode 'type))
			   (;; a type delimiter has been received, this element must be the type
				(eql mode 'type) (setf mode 'var)
				(let (type)
				  ;; check if the type is a list that starts with "either" or 
				  ;; if it is a singular type
				  (cond (;; this is a singular type
						 (not (consp typed-element))
						 (setf type (list typed-element)))
						(;; this should be an either type
						 (or (eq (car typed-element) :either) (eq (car typed-element) 'either))
						 (setf type (rest typed-element)))
						(:otherwise (format t "Warning: unable to parse typed-list ~A. Some variables or types were skipped~%" pddl-typed-list)))
				  ;; add the untyped variables to the typed list with types
				  (coll-map untyped-vars #'(lambda (var) (coll-add! typed-vars (cons var type))))
				  ;; erase the list of untyped vars
				  (coll-clear! untyped-vars)
				  ))
			   (; look for variables
				(eql mode 'var)
				(coll-add! untyped-vars typed-element))
			   )
		 )
	;; return all the typed and untyped variables, with untyped vars labeled as type object
	(coll-map untyped-vars #'(lambda (var) (coll-add! typed-vars (cons var nil))))
	(coll-tolist typed-vars)))

;; (parse-pddl-typed-list '(a b - t1 c - t2 d e f g - t3))

;; (defun clausify-typed-list (typed-list)
;;   "Returns a set of logical clauses represents a typed-list that 
;;    has the form ((var type type) (var type)) in clausal form."
;;   (cons 'and  (mapcar #'(lambda (var-types) 
;; 						  (cons 'or (mapcar #'(lambda (type) (list type (car var-types))) (rest var-types)))) 
;; 					  typed-list)))

(defun variables-typed-list (typed-list) 
  "Returns the variables described in the typed-list."
  (mapcar #'(lambda (var-types) (first var-types)) typed-list))

(defun types-typed-list (typed-list) 
  "Returns the types used in the typed-list."
  (delete-duplicates (mapcan #'(lambda (var-types) (copy-list (rest var-types))) typed-list)))

;; (defun make-typed-pair-from-typed-list (typed-list)
;;   (mapcan #'(lambda (typed-element)
;; 			  (mapcar #'(lambda (type) (list (first typed-element) type)) (rest typed-element))) 
;; 		  typed-list))

;; (defun simplify-typed-list (typed-list)
;;   (let ((supertypeof-table (make-hash-table))
;; 		(new-typed-list '()))
;; 	(map nil #'(lambda (typed-element) 
;; 				 (let ((type (first typed-element))
;; 					   (supertypes (rest typed-element)))
;; 				   (setf (gethash type supertypeof-table) (union supertypes (gethash type supertypeof-table nil))))) 
;; 		 typed-list)
;; 	(loop for type being the hash-key in supertypeof-table using (hash-value supertypes) do
;; 		 (push (cons type supertypes) new-typed-list))
;; 	new-typed-list))

;; (defun supertypeof-list (typed-list &key (include-object t))
;;   "Given a TYPED-LIST of the form ((type supertype) (type supertype supertype)),
;;    returns a typed-list of the same form, where all sypertypes for a given type
;;    are explicitly listed. i.e. ((a b)(b c)) -> ((a b c)(b c)(a object)(b object)(c object))."
;;   (let ((types (reduce #'union typed-list))
;; 		(typeof (make-hashmultimap :make-coll '(make-hashset)))
;; 		(typed-pairs (make-typed-pair-from-typed-list typed-list)))
;; 	;; populate typed-pairs with the object type
;; 	(setf typed-pairs (nconc typed-pairs (mapcar #'(lambda (type) (list type 'object)) types)))
;; 	;; populate the typeof table with supertypes for each type
;; 	;; we start from the root of the type-tree and descend.
;; 	(let (;; queue maintains a list of nodes in the tree whose parents have already been expanded.
;; 		  (queue '(object))
;; 		  ;; a loop variable, that maintains the current node in the tree.
;; 		  (curr-type nil))
;; 	  (loop until (null typed-pairs) do
;; 		   (setf curr-type (pop queue))
;; 		   (setf typed-pairs
;; 				 (loop for (subtype supertype) in typed-pairs append
;; 					  (if (eql curr-type supertype)
;; 						  (progn (push subtype queue)
;; 								 (map-add! typeof subtype supertype) 
;; 								 (when (map-get typeof curr-type)
;; 								   (coll-map (map-get typeof curr-type)
;; 											 (lambda (supertype) (map-add! typeof subtype supertype))))
;; 								 nil)
;; 						  `((,subtype ,supertype)))
;; 					  ))
;; 		   ))
;; 	;; produce the returned list of the form ((type supertype supertype) (type supertype))
;; 	(let ((typeof-list '())
;; 		  (supertype-list '()))
;; 	  (loop for type being the hash-keys in (abstract-data-types::hmm-table typeof) using (hash-value supertypes) do
;; 		   (setf supertype-list (if supertypes (coll-tolist supertypes)	nil))
;; 		   (push (cons type (if include-object supertype-list (delete 'object supertype-list)))
;; 				 typeof-list))
;; 	  typeof-list)
;; 	))

;; (defun supertypeof (type typed-list &key (include-object t))
;;   "Given a TYPED-LIST of the form ((type supertype)(type supertype supertype)),
;;    returns all of the supertypes of TYPE as a list."
;;   (let ((typed-list (simplify-typed-list typed-list))
;; 		(queue (list type))
;; 		(expanded '())
;; 		(curr-type nil)
;; 		(allsupertypes '()))
;; 	(loop until (null queue) do
;; 		 (setf curr-type (pop queue))
;; 		 (unless (member curr-type expanded)
;; 		   (setf typed-list
;; 				 (loop for typed-element in typed-list append
;; 					  (let ((subtype (first typed-element))
;; 							(supertypes (rest typed-element)))
;; 						(if (eql curr-type subtype)
;; 							(progn (setf queue (append queue supertypes))
;; 								   (setf allsupertypes (union allsupertypes supertypes))
;; 								   (push subtype expanded)
;; 								   nil)
;; 							`(,(cons subtype supertypes)))))
;; 				 )))
;; 	(if include-object 
;; 		(cons 'object allsupertypes)
;; 		allsupertypes)))

				 
;; subtypes is a list of atomic types.  super-types
;; is either an atomic type or a list of 'either
;; followed by one or more super-types.

;; (defun expand-super (subtypes super-types)
;;   (cond ((null super-types) nil)
;; 	((not (consp super-types))
;; 	 (mapcar #'(lambda (sub) (list super-types sub)) subtypes))
;; 	((eq (first super-types) 'either)
;; 	 (expand-super subtypes (cdr super-types)))
;; 	(t (append (expand-super subtypes (car super-types))
;; 		   (expand-super subtypes (cdr super-types))))))


;; Compute the list of all reachable supertypes from the given
;; subtype.
;; (defun compute-reachable (sub supers type-pairs all-type-pairs)
;;   (cond ((null type-pairs) supers)
;; 		((and (eq (first (car type-pairs)) sub)
;; 			  (not (member (second (car type-pairs)) supers)))
;; 		 ;; Recursively descend on supertype.
;; 		 (union
;; 		  (compute-reachable (second (car type-pairs))
;; 							 (cons (second (car type-pairs)) supers)
;; 							 all-type-pairs all-type-pairs)
;; 		  (compute-reachable sub supers (cdr type-pairs) all-type-pairs)))
;; 		(t (compute-reachable sub supers (cdr type-pairs) all-type-pairs))))

;; (defun simple-invert (p)
;;   (if (eq (first p) 'not) (second p)
;;     (list 'not p)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; REQUIREMENTS HANDLING
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun check-requirements (capabilities problem domain)
  (let* ((domain-requirements (domain-requirements domain))
         (problem-requirements (problem-requirements problem))
         (trouble
          (set-difference (reduce-requirements (union problem-requirements
                                                      domain-requirements))
                          (reduce-requirements capabilities))
		   ))
    (when trouble
      (warn "Planner is unlikely to work on ~A because of requirement
~{~S ~}
in the domain or problem description." (problem-name problem) trouble))

    (null trouble)))

(defparameter *requirement-reductions*
  '((:quantified-preconditions :existential-preconditions :universal-preconditions)
	(:adl :strips :typing :negative-preconditions :disjunctive-preconditions
	      :equality :quantified-preconditions :conditional-effects)
	;; this last requirement reduction is only standard to the language 
    ;;  accepted by the UCPOP algorithm, not to the PDDL language
    (:ucpop :adl :domain-axioms :procedural-attachments :safety-constraints))
  "An association list, in which the first element of each list is the super-requirement,
   The remaining elements in the list are the requirements that super-requirement expands into.")

(defparameter *requirements*
  '(:strips :typing :negative-preconditions :disjunctive-preconditions :equality :existential-preconditions :universal-preconditions :quantified-preconditions :condititional-effects :fluents :numeric-fluents :adl :durative-actions :duration-inequalities :continuous-effects :derived-predicates :timed-initial-literals :preferences :constraints :action-costs))

(defun reduce-requirements (reqs)
  "Given a list of requirements REQS, returns a list of base requirements.
   Or, each requirement in the returned list cannot be expanded into any 
   set of other requirements."
  (loop for req in reqs nconc
        (let ((reduction (assoc req *requirement-reductions*)))
          (if reduction 
            (reduce-requirements (rest reduction))
            (list req)))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; GLOBAL VARIABLES
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; A list of all loaded domain objects
(defvar *domains* (make-hashset :key-function #'domain-name))
;; A list of all loaded problem objects
(defvar *problems* (make-hashset :key-function #'problem-name))

;; Most recently parsed domain
(defvar *recent-domain* nil)
;; Most recently parsed problem
(defvar *recent-problem* nil)