;;;; Copyright (c) 2013 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   David C. Wang

(in-package :pddl)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; PPRINT DOMAIN
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun pprint-domain (domain &optional (*standard-output* *standard-output*))
  (pprint-logical-block (nil '() :prefix "(" :suffix ")")
	(pprint-symbol-name 'define)
	(write-char #\Space)
	(pprint-logical-block (nil '() :prefix "(" :suffix ")")
	  (pprint-symbol-name 'domain)
	  (write-char #\Space)
	  (pprint-symbol-name (domain-name domain))
	  )
	(pprint-newline :mandatory)
	(pprint-newline :mandatory)
	;; print requirements
	(pprint-requirements (domain-requirements domain))
	(pprint-newline :mandatory)
	(pprint-newline :mandatory)
	;; print types
	(pprint-types (domain-types domain))
	(pprint-newline :mandatory)
	(pprint-newline :mandatory)
	;; print constants
	(pprint-constants (domain-constants domain))
	(pprint-newline :mandatory)
	(pprint-newline :mandatory)
	;; print predicates
	(pprint-predicates (domain-predicates domain))
	(pprint-newline :mandatory)
	(pprint-newline :mandatory)
	;; print actions
	(coll-map (domain-actions domain) 
			  #'(lambda (action)
				  (if (durative-action-p action)
					  (pprint-durative-action action)
					  (pprint-action action))
				  (pprint-newline :mandatory)
				  (pprint-newline :mandatory)))
	))

(defun pprint-requirements (requirements &optional (*standard-output* *standard-output*))
  (pprint-logical-block (nil '() :prefix "(" :suffix ")")
	(pprint-symbol-as-keyword :requirements)
	(coll-map requirements 
			  #'(lambda (req)
				  (write-char #\Space)
				  (pprint-symbol-as-keyword req) ))
	))

(defun pprint-types (types &optional (*standard-output* *standard-output*))
  (let ((untyped-list '()))
	(pprint-logical-block (nil '() :prefix "(" :suffix ")")
	  (pprint-symbol-as-keyword :types)
	  (map-types types 
				 #'(lambda (type-name type-obj)
					 (pprint-logical-block (nil '() :prefix "" :suffix "")
					   (let ((coll-size-no-default-obj (coll-size (direct-supertypes type-obj))))
						 ;; remove the default type from the count
						 (when (coll-contains? (direct-supertypes type-obj) (default-supertype types))
						   (decf coll-size-no-default-obj))
						 (case coll-size-no-default-obj
						   (;; save off the type to be printed at the end
							0
							(push type-name untyped-list) )
						   (;; 1 supertype
							1
							(write-char #\Space)
							(pprint-symbol-name type-name)
							(write-char #\Space)
							(write-char #\-)
							(write-char #\Space)
							(coll-map (direct-supertypes type-obj) 
									  #'(lambda (supertype)
										  (pprint-symbol-name (type-name supertype))))
							)
						   (;; more than 1 supertype
							t
							(write-char #\Space)
							(pprint-symbol-name type-name)
							(write-char #\Space)
							(write-char #\-)
							(write-char #\Space)
							(pprint-logical-block (nil '() :prefix "(" :suffix ")")
							  (pprint-symbol-name 'either)
							  (coll-map (direct-supertypes type-obj) 
										#'(lambda (supertype)
											(write-char #\Space)
											(pprint-symbol-name (type-name supertype)))) )
							)))
					   ))
				 )
	  ;; print all of the untyped types
	  (dolist (untyped-name untyped-list)
		(pprint-logical-block (nil '() :prefix "" :suffix "")
		  (write-char #\Space)
		  (pprint-symbol-name untyped-name)
		  ))
	  )))

(defun pprint-single-typed-element (typed-element &optional (*standard-output* *standard-output*))
  (pprint-logical-block (nil '() :prefix "" :suffix "")
	(pprint-symbol-name (first typed-element))
	(let ((types (rest typed-element)))
	  (case (length types)
		(;; do nothing
		 0)
		(;; 1 supertype
		 1
		 (write-char #\Space)
		 (write-char #\-)
		 (write-char #\Space)
		 (pprint-symbol-name (second typed-element))
		 )
		(;; more than 1 supertype
		 t
		 (write-char #\Space)
		 (write-char #\-)
		 (write-char #\Space)
		 (pprint-logical-block (nil '() :prefix "(" :suffix ")")
		   (pprint-symbol-name 'either)
		   (dolist (type types)
			 (write-char #\Space)
			 (pprint-symbol-name type)) )
		 ))
	  )))

(defun pprint-constants (constants &optional (*standard-output* *standard-output*))
  (pprint-logical-block (nil '() :prefix "(" :suffix ")")
	(pprint-symbol-as-keyword :constants)
	(let ((num-remain (coll-size constants)))
	  (coll-map constants
				#'(lambda (constant)
					(pprint-indent :block 8)
					(write-char #\Space)
					(pprint-single-typed-element constant)
					(decf num-remain)
					(unless (= num-remain 0)
						(pprint-newline :mandatory))
					)))
	))

(defun pprint-predicates (predicates &optional (*standard-output* *standard-output*))
  (pprint-logical-block (nil '() :prefix "(" :suffix ")")
	(pprint-symbol-as-keyword :predicates)
	(write-char #\Space)
	(coll-map predicates 
			  #'(lambda (predicate)
				  (pprint-indent :block 12)
				  (pprint-logical-block (nil '() :prefix "(" :suffix ")")
					(pprint-symbol-name (first predicate))
					(loop for params in (rest predicate) do
						 (write-char #\Space)
						 (pprint-single-typed-element params)
						 ))
				  (pprint-newline :fill))
			  )
	))


(defun pprint-action (action &optional (*standard-output* *standard-output*))
  (pprint-logical-block (nil '() :prefix "(" :suffix ")")
	(pprint-symbol-as-keyword :action)
	(write-char #\Space)
	(pprint-symbol-name (action-name action))
	(pprint-newline :mandatory)
	;; print parameters
	(pprint-symbol-as-keyword :parameters)
	(write-char #\Space)
	(pprint-logical-block (nil '() :prefix "(" :suffix ")")
	  (loop for params in (action-parameters action) 
		 with first = 't
		 do
		   (if first (setf first nil) (write-char #\Space))
		   (pprint-single-typed-element params)
		   ))
	(pprint-newline :mandatory)
	;; print condition
	(pprint-symbol-as-keyword :precondition)
	(write-char #\Space)
	(cond (;; the first element is either a list or a predicate
		   (listp (first (action-precondition action))) 
		   (pprint-symbol-list (append '(pddl::and) (action-precondition action)))
		   )
		  (;; the first element is a symbol of type 'and
		   (eq (first (action-precondition action)) 'pddl::and)
		   (pprint-symbol-list (action-precondition action))
		   )
		  (;; the `first element is a symbol not of type 'and
		   :otherwise
		   (pprint-symbol-list (cons 'pddl::and (list (action-precondition action))))
		   ))
	(pprint-newline :mandatory)
	;; print effect
	(pprint-symbol-as-keyword :effect)
	(write-char #\Space)
	(cond (;; the first element is either a list or a predicate
		   (listp (first (action-effect action))) 
		   (pprint-symbol-list (append '(pddl::and) (action-effect action)))
		   )
		  (;; the first element is a symbol of type 'and
		   (eq (first (action-effect action)) 'pddl::and)
		   (pprint-symbol-list (action-effect action))
		   )
		  (;; the `first element is a symbol not of type 'and
		   :otherwise
		   (pprint-symbol-list (cons 'pddl::and (list (action-effect action))))
		   ))
	)
  )

(defun pprint-durative-action (action &optional (*standard-output* *standard-output*))
  (pprint-logical-block (nil '() :prefix "(" :suffix ")")
	(pprint-symbol-as-keyword :durative-action)
	(write-char #\Space)
	(pprint-symbol-name (action-name action))
	(pprint-newline :mandatory)
	;; print parameters
	(pprint-symbol-as-keyword :parameters)
	(write-char #\Space)
	(pprint-logical-block (nil '() :prefix "(" :suffix ")")
	  (loop for params in (action-parameters action) 
		 with first = 't
		 do
		   (if first (setf first nil) (write-char #\Space))
		   (pprint-single-typed-element params)
		   ))
	(pprint-newline :mandatory)
	;; print duration
	(pprint-symbol-as-keyword :duration)
	(write-char #\Space)
	(pprint-symbol-list (durative-action-duration action))
	(pprint-newline :mandatory)
	;; print condition
	(pprint-symbol-as-keyword :condition)
	(write-char #\Space)
	(with-slots (atstart-cond overall-cond atend-cond) action
	  (let ((num-conds-remain (- (+ (length atstart-cond)
									(length overall-cond)
									(length atend-cond)) 3)))
		(pprint-logical-block (nil '() :prefix "(" :suffix ")")
		  (pprint-symbol-name 'and)
		  (setf num-conds-remain (pprint-conditions '(at start) atstart-cond num-conds-remain))
		  (setf num-conds-remain (pprint-conditions '(over all) overall-cond num-conds-remain))
		  (setf num-conds-remain (pprint-conditions '(at end) atend-cond num-conds-remain num-conds-remain))
		  )))
	(pprint-newline :mandatory)
	;; print effect
	(pprint-symbol-as-keyword :effect)
	(write-char #\Space)
	(with-slots (atstart-effect atend-effect) action
	  (let ((num-conds-remain (- (+ (length atstart-effect)
									(length atend-effect)) 2)))
		(pprint-logical-block (nil '() :prefix "(" :suffix ")")
		  (pprint-symbol-name 'and)
		  (setf num-conds-remain (pprint-conditions '(at start) atstart-effect num-conds-remain))
		  (setf num-conds-remain (pprint-conditions '(at end) atend-effect num-conds-remain))
		  )))
	))

(defun pprint-conditions (prefix-list conditions num-conds-remain &optional (*standard-output* *standard-output*))
  (let ((first-loop t) )
	(dolist (condition conditions)
	  (if first-loop
		  (progn
			(setf first-loop nil))
		  (progn 
			(pprint-indent :block 3)
			(write-char #\Space)
			(pprint-logical-block (nil '() :prefix "(" :suffix ")")
			  (loop for prefix in prefix-list do
				   (pprint-symbol-name prefix)
				   (write-char #\Space) )
			  (pprint-symbol-list condition)  )
			(decf num-conds-remain)
			(unless (= num-conds-remain 0)
			  (pprint-newline :mandatory))	)
		  )
	  )
	num-conds-remain))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; PPRINT PROBLEM
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun pprint-problem (problem &optional (*standard-output* *standard-output*))
  (pprint-logical-block (nil '() :prefix "(" :suffix ")")
	(pprint-symbol-name 'define)
	(write-char #\Space)
	(pprint-logical-block (nil '() :prefix "(" :suffix ")")
	  (pprint-symbol-name 'problem)
	  (write-char #\Space)
	  (pprint-symbol-name (problem-name problem))
	  )
	(pprint-newline :mandatory)
	(pprint-newline :mandatory)
	;; print domain
	(pprint-logical-block (nil '() :prefix "(" :suffix ")")
	  (pprint-symbol-as-keyword :domain)
	  (let ((domain (problem-domain-name problem)))
		(cond (;; do nothing if no domain
			   (null domain))
			  (;; if the domain is a domain object
			   (domain-p domain)
			   (write-char #\Space)
			   (pprint-symbol-name (domain-name domain)))
			  (:otherwise
			   (write-char #\Space)
			   (pprint-symbol-name domain))
			  )))
	(pprint-newline :mandatory)
	(pprint-newline :mandatory)
	;; print requirements
	;; TODO - This line is commented out because the fast downward 
	;;        parser does not support problem requirements.
	;;        (Even thought problem requirements are allowed by
	;;        the PDDL 2.1 spec).
	;; (pprint-requirements (problem-requirements problem))
	;; (pprint-newline :mandatory)
	;; (pprint-newline :mandatory)
	;; print objects
	(pprint-objects (problem-objects problem))
	(pprint-newline :mandatory)
	(pprint-newline :mandatory)
	;; print init
	(pprint-init (problem-init problem))
	(pprint-newline :mandatory)
	(pprint-newline :mandatory)
	;; print goal
	(pprint-logical-block (nil '() :prefix "(" :suffix ")")
	  (pprint-symbol-as-keyword :goal)
	  (write-char #\Space)
	  (pprint-symbol-list (problem-goal problem)))
	)
  (pprint-newline :mandatory))


(defun pprint-objects (objects &optional (*standard-output* *standard-output*))
  (pprint-logical-block (nil '() :prefix "(" :suffix ")")
	(pprint-symbol-as-keyword :objects)
	(let ((num-remain (coll-size objects)))
	  (coll-map objects
				#'(lambda (object)
					(pprint-indent :block 8)
					(write-char #\Space)
					(pprint-single-typed-element object)
					(decf num-remain)
					(unless (= num-remain 0)
						(pprint-newline :mandatory))
					)))
	))

(defun pprint-init (inits &optional (*standard-output* *standard-output*))
	(pprint-logical-block (nil '() :prefix "(" :suffix ")")
	  (pprint-symbol-as-keyword :init)
	  (let ((num-remain (- (length inits) 1)))
		   (dolist (init (rest inits))
			 (pprint-indent :block 5)
			 (write-char #\Space)
			 (pprint-symbol-list init)
			 (decf num-remain)
			 (unless (= num-remain 0)
			   (pprint-newline :mandatory))
			 )))
	)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; PPRINT HELPERS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun pprint-symbol-as-keyword (symbol &optional (*standard-output* *standard-output*))
  (write-char #\:)
  (write-string (symbol-name symbol))
  )

(defun pprint-symbol-name (symbol &optional (*standard-output* *standard-output*))
  (write-string (symbol-name symbol))
  )

(defun pprint-symbol-list (list &optional (*standard-output* *standard-output*))
  (pprint-logical-block (nil list :prefix "(" :suffix ")")
	(loop
	   (let ((curr-element (pprint-pop)))
		 (cond ((null curr-element)
				(pprint-exit-if-list-exhausted))
			   ((listp curr-element)
				(pprint-symbol-list curr-element)
				(pprint-exit-if-list-exhausted)
				(write-char #\Space))
			   ((symbolp curr-element)
				(pprint-symbol-name curr-element)
				(pprint-exit-if-list-exhausted)
				(write-char #\Space))
			   (:otherwise
				(write curr-element)
				(pprint-exit-if-list-exhausted)
				(write-char #\Space))
			   )
		 ))
	))