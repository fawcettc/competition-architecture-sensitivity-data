;;; -*- Mode:Common-Lisp; Package:mtk-utils; Base:10 -*-

;;; Copyright (c) 2000 - 2008 Brian C. Williams, MIT.

;;; This software may not be redistributed, and can only be retained
;;; and used with the explicit written consent of the author,
;;; Brian C. Williams.

;;; This software is made available as is; no guarantee is provided
;;; with respect to performance or correct behavior.

;;; This software may only be used for non-commercial, non-profit,
;;; research activities.

(in-package :mtk-utils)

;;; **************************************************************
;;;
;;;                     ***   To Do   ***
;;;
;;; **************************************************************

;;; To Do:

;;; o This file is duplicated in Iota -
;;;   This file merges the bindings macros from mtk-utils with the file 
;;;   bindings.lisp from RMPL.  The bindings.lisp file from RMPL 
;;;   and Iota were duplicates.  Iota should also be made to depend
;;;   on mtk-utils, and the duplicate bindings file in Iota should 
;;;   be removed.

;;;   ************************************************************

;;; *** Utilities for Manipulating Variable/Value Bindings ***

;;;   ************************************************************

;;; The following is a combination of routines taken from an early 
;;; mtk-utils and from RMPL and Iota.

(defconstant *empty-bindings*
	`()
  
  "The empty binding list.")

(defmacro get-binding (var bindings)

  "Returns the value that VAR is bound to in BINDINGS."

  `(assoc ,var ,bindings))

(defmacro empty-binding? (binding)

  "Is BINDING an empty binding list?"

  `(null ,binding))

(defmacro binding-variable (binding)

  "Returns the variable of BINDING."

  `(first ,binding))

;;; Version from original mtk-utils
;;;(defmacro binding-value (binding)
;;;  
;;;  "Returns the VALUE of binding."
;;;
;;;  `(rest ,binding))

(defmacro binding-value (binding)

  "Returns the value of BINDING."

  `(second ,binding))

;;; From the original mtk-utils
;;; Changed from a CONS to a LIST, to be
;;; consistent with Iota and RMPL.
;;;(defmacro make-binding (variable value)
;;;  
;;;  "Create a binding of VARIABLE to VALUE."
;;;
;;;  `(cons ,variable ,value))

(defmacro make-binding (variable value)

  "Creates a binding of VARIABLE to VALUE."

  `(list ,variable ,value))

;;; From original mtk-utils
(defmacro make-binding-list (variables values)
  
  "Create a binding of VARIABLE to VALUE."

  `(mapcar #'cons ,variables ,values))

(defmacro add-binding (variable value bindings)

  "Adds to BINDINGS a binding of VARIABLE to VALUE."

  `(cons (make-binding ,variable ,value) ,bindings))

(defmacro add-bindings (additional-bindings bindings)

  "Adds ADDITIONAL-BINDINGS to the set of BINDINGS."

  `(append ,additional-bindings ,bindings))

;;; From the original mtk-utils binding macros.
(defmacro instantiate-bindings (expression binding-list)
  `(sublis ,binding-list ,expression))
