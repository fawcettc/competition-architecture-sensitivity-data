;;; -*- Mode:Common-Lisp; Package:mtk-utils; Base:10 -*-

;;; Copyright (c) 1999 - 2008 Brian C. Williams, MIT.

;;; This software may not be redistributed, and can only be retained
;;; and used with the explicit written consent of the author,
;;; Brian C. Williams.

;;; This software is made available as is; no guarantee is provided
;;; with respect to performance or correct behavior.
;;; This software may only be used for non-commercial, non-profit,
;;; research activities.

(in-package :mtk-utils)

;;; ***********************************************************************
;;;
;;;                      ***  Things To Do ***
;;;
;;; ***********************************************************************

;;; o Be consistent about placing the word "next" at the start 
;;;   or the end of the name of a combinator.

;;; o Make these functions inline, rather than using macros.

;;; o Remove calls and code for obsolete functions, as indicated below.

;;; o Add comments.

;;; o RMPL combinators used to be the following, decide what is consistent:

;;;   Parallel Composition:
;;;      (AND automata1 automata2 ...)

;;;   Probabilistic Choice:
;;;      (CHOOSE-PROBABILITY [(probability automata)]+)

;;;   Decision-theorectic Choice:
;;;      (CHOOSE-REWARD [(probability automata)]+)

;;;   Premption:
;;;      (DO-WATCHING constraint automaton)

;;;   Iteration:
;;;      (ALWAYS automaton)

;;;   Transition on condition:
;;;      (When-Next condition automaton)

;;;   Transition on absence of information:
;;;      (Unless-Entailed-Next condition automaton)

;;;   Immediate, Intuitionistic If:
;;;      (IF condition automaton)

;;;    Delay:
;;;      (NEXT automaton)

;;;    Immediate, Intuitionistic If Then Else:
;;;      (IF condition then-automaton else-automaton)

;;;    Series composition:
;;;      (SEQUENCE automata1 automata2 ...)

;;; ***********************************************************************
;;;
;;; ***  Constants for Defined Operators and Values ***
;;;
;;; ***********************************************************************

;;; Derived Logical Connectives

(defconstant *when* 'when)
(defconstant *unless* 'unless)
(defconstant *if* 'if)

(defconstant *conditional* 'cond)
(defconstant *else* 'else)

;;; RMPL Combinators

;;;   RMPL Primitive Combinators

(defconstant *next-when* 'next-when)
(defconstant *next-unless-entailed* 'next-unless-entailed)
(defconstant *always* 'always)
(defconstant *parallel* 'parallel)
(defconstant *choose-probability* 'choose-probability)
(defconstant *choose-reward* 'choose-reward)

;;;   RMPL Derived Combinators

(defconstant *do-watching* 'do-watching)
(defconstant *sequence* 'sequence)
(defconstant *if-else-next* 'if-else-next)

(defconstant *every-combinator* 'every)

(defvar *rmpl-combinators* (list *next-when*
								  *next-unless-entailed*
								  *always*
								  *parallel*
								  *choose-probability*
								  *choose-reward*
								  *do-watching*
								  *sequence*
								  *if-else-next*
								  ;; next if
								  ))

;;; ***********************************************************************
;;;
;;;          ***  Derived Logical Combinators ***
;;;
;;; ***********************************************************************

;;; *********************************************************************
;;;
;;; ***  When (Implication)
;;;
;;; *********************************************************************

(defmacro when-operator? (op)
  `(eql ,op *when*))

(defmacro when? (exp)
  `(and (compound-expression? ,exp)
		(when-operator? (expression-operator ,exp))))

(defmacro check-when? (exp)
  `(legal-form? when (>= (length ,exp) 3) ,exp))

(defmacro make-when (condition clauses)
  `(make-expression *when* (cons ,condition ,clauses)))

(defmacro when-condition (expression)
  `(first (expression-operands ,expression)))

(defmacro when-clauses (expression)
  `(rest (expression-operands ,expression)))

;;; *********************************************************************
;;;
;;; ***  Unless (Implication)
;;;
;;; *********************************************************************

(defmacro unless-operator? (op)
  `(eql ,op *unless*))

(defmacro unless? (exp)
  `(and (compound-expression? ,exp)
		(unless-operator? (expression-operator ,exp))))

(defmacro check-unless? (exp)
  `(legal-form? unless (>= (length ,exp) 3) ,exp))

(defmacro make-unless (condition clauses)
  `(make-expression *unless* (cons ,condition ,clauses)))

(defmacro unless-condition (expression)
  `(first (expression-operands ,expression)))

(defmacro unless-clauses (expression)
  `(rest (expression-operands ,expression)))

;;; *********************************************************************
;;;
;;; ***  If (Then Else)
;;;
;;; *********************************************************************

(defmacro if-operator? (op)
  `(eql ,op *if*))

(defmacro if? (exp)
  `(and (compound-expression? ,exp)
		(if-operator? (expression-operator ,exp))))

(defmacro make-if (condition true-clause false-clause)
  `(make-expression *if* 
					(if (null ,false-clause)
						(list ,condition ,true-clause)
					  (list ,condition ,true-clause
							,false-clause))))
 
(defmacro check-if? (exp)
  `(legal-form? if (or (= (length ,exp) 4) 
					   (= (length ,exp) 3)) ,exp))

(defmacro if-condition (expression)
  `(first (expression-operands ,expression)))

(defmacro if-true-clause (expression)
  `(second (expression-operands ,expression)))

(defmacro if-false-clause (expression)
  `(second (expression-operands ,expression)))

;;; *********************************************************************
;;;
;;; ***  Condition
;;;
;;; *********************************************************************

;;; To Do:
;;; o Write check-* fn.
;;;   - Check that the else always
;;;     appears as the last clause.

(defmacro conditional-operator? (op)
  `(eql ,op *conditional*))

(defmacro conditional? (exp)
  `(and (compound-expression? ,exp)
		(conditional-operator? (expression-operator ,exp))))

(defmacro make-conditional (clauses)
  `(make-expression *conditional* 
					,clauses))

(defmacro conditional-clauses (expression)
  `(expression-operands ,expression))

(defmacro conditional-clause-predicate (conditional-clause)
  `(first ,conditional-clause))

(defmacro conditional-clause-actions (conditional-clause)
  `(rest ,conditional-clause))

(defmacro conditional-else-clause? (conditional-clause)
  `(eql (conditional-clause-predicate ,conditional-clause)
 		*else*))

;;; ***********************************************************************
;;;
;;;          ***  Synchronous Reactive Combinators ***
;;;
;;; ***********************************************************************

(defun rmpl-combinator? (op)
  "Returns T iff op is an RMPL combinator."

  `(member ,op *rmpl-combinators*))

(defun rmpl-combinator-expression? (exp)

  "Returns T iff exp is an RMPL expression involving an RMPL combinator."
  
  (and (compound-expression? exp)
	   (rmpl-combinator? (expression-operator exp))))

;;;   ************************************************************
;;;   **
;;;   **  RMPL Temporal Implication:
;;;   **
;;;   **            (NEXT-WHEN <condition> <form>+)
;;;   **
;;;   ** where CONDITION is a constraint (e.g., wff) and
;;;   **       each FORM is an RMPL expression.
;;;   **
;;;   ** If CONDITION is entailed in the initial state in which
;;;   ** the next-when is evaluated, then FORMS is executed
;;;   ** sequentially, starting in the next state.
;;;   **
;;;   ************************************************************

;;; o Change from next-when to when-next,
;;;   as is the case in rmpl-defs.lisp
(defmacro next-when-operator? (op)
  
  "Returns T iff op is an RMPL NEXT-WHEN operator."

  `(eql ,op *next-when*))

(defmacro next-when? (exp) 

  "Returns T iff exp is an RMPL NEXT-WHEN expression."
  
  `(and (compound-expression? ,exp)
		(next-when-operator? (expression-operator ,exp))))

(defmacro check-next-when? (exp)
  
  "Returns true iff EXP is a valid next-when expression."
  
  `(legal-form? ,*next-when* (length>2? ,exp) ,exp))

(defmacro make-next-when (condition forms)

  "Given a wff CONDITION, and a list FORMS of rmpl expressions,
   Returns an rmpl expression that executes FORMS
   sequentially, if CONDITION is satisfied in the initial state
   in which that expression is executed."

  `(make-expression *next-when* (cons ,condition ,forms)))

(defmacro next-when-condition (expression)
  `(first (expression-operands ,expression)))

(defmacro next-when-forms (expression)
  `(rest (expression-operands ,expression)))

;;;   ************************************************************
;;;   **
;;;   **  RMPL Negative information implication:
;;;   **
;;;   **            (NEXT-UNLESS-ENTAILED <condition> <form>+)
;;;   **
;;;   ** where CONDITION is a constraint (wff) and
;;;   **       each FORM is an RMPL expression.
;;;   **
;;;   ** If CONDITION is not entailed in the initial state in which
;;;   ** the NEXT-UNLESS-ENTAILED is evaluated, then FORMS is executed
;;;   ** sequentially, starting in the next state.
;;;   **
;;;   ************************************************************

;;; To Do:
;;; o Change from "condition not entailed" to "condition not satisfied," 
;;;   - change name from next-unless-entailed -> next-unless.
;;;   - Update documentation.
;;; o Change from next-unless-entailed to unless-entailed-next,
;;;   as is the case in rmpl-defs.lisp

(defmacro next-unless-entailed-operator? (op)
  
  "Returns T iff op is an RMPL next-unless-entailed operator."

  `(eql ,op *next-unless-entailed*))

(defmacro next-unless-entailed? (exp)

  "Returns T iff exp is an RMPL next-unless-entailed expression."
  
  `(and (compound-expression? ,exp)
		(next-unless-entailed-operator? (expression-operator ,exp))))

(defmacro check-next-unless-entailed? (exp)
  `(legal-form? next-unless-entailed (length>2? ,exp) ,exp))

(defmacro make-next-unless-entailed (condition forms)
  `(make-expression *next-unless-entailed*
					(cons ,condition ,forms)))

(defmacro next-unless-entailed-condition (expression)
  `(first (expression-operands ,expression)))

(defmacro next-unless-entailed-forms (expression)
  `(rest (expression-operands ,expression)))

;;;   ************************************************************
;;;   **
;;;   **  RMPL ALWAYS expression:
;;;   **
;;;   **            (ALWAYS <form>+)
;;;   **
;;;   ** where each FORM is an RMPL expression.
;;;   **
;;;   ** Executes FORMS sequentially, starting in the initial state
;;;   ** in which ALWAYS is executed; upon completion, repeat forever.
;;;   **
;;;   ************************************************************

(defmacro always-operator? (op)
  
  "Returns T iff op is an RMPL always operator."

  `(eql ,op *always*))

(defmacro always? (exp) 

  "Returns T iff exp is an RMPL ALWAYS expression."
  
  `(and (compound-expression? ,exp)
		(always-operator? (expression-operator ,exp))))

(defmacro check-always? (exp)
  `(legal-form? always (length>1? ,exp) ,exp))

(defmacro make-always (forms)
  `(make-expression *always* ,forms))

(defmacro always-forms (expression)
  `(expression-operands ,expression))

;;;   ************************************************************
;;;   **
;;;   **  RMPL PARALLEL expression:
;;;   **
;;;   **            (PARALLEL <form>+)
;;;   **
;;;   ** where each FORM is an RMPL expression.
;;;   **
;;;   ** Executes each of FORMS in parallel, starting in the
;;;   ** initial state in which the parallel expression has been 
;;;   ** executed.
;;;   **
;;;   ************************************************************

(defmacro parallel-operator? (op)
  
  "Returns T iff op is an RMPL parallel operator."

  `(eql ,op *parallel*))

(defmacro parallel? (exp) 

  "Returns T iff exp is an RMPL parallel expression."
  
  `(and (compound-expression? ,exp)
		( parallel-operator? (expression-operator ,exp))))

(defmacro check-parallel? (exp)
  `(legal-form?  parallel (length>1? ,exp) ,exp))

(defmacro make-parallel (forms)
  `(make-expression *parallel* ,forms))

(defmacro parallel-forms (expression)
  `(expression-operands ,expression))

;;;   ************************************************************
;;;   **
;;;   **  RMPL PROBABILISTIC CHOICE expression:
;;;   **
;;;   **     (CHOOSE-PROBABILITY (<probability_i> . <forms_i>)+)
;;;   **
;;;   **  Let FORMS_i be a list of rmpl expressions, and
;;;   **  0 < probability_i <= 1.
;;;   **
;;;   **  Choose-reward makes a probabilistic choice of forms, 
;;;   **  in which FORMs_i is selected with PROBABILITY_i.
;;;   **  The selected forms are initiated in the same time step
;;;   **  in which its choose-probability is executied.
;;;   **
;;;   ************************************************************

(defmacro choose-probability-operator? (op)
  
  "Returns T iff op is an RMPL choose-probability operator."

  `(eql ,op *choose-probability*))

(defmacro choose-probability? (exp) 

  "Returns T iff exp is an RMPL choose-probability expression."
  
  `(and (compound-expression? ,exp)
		(choose-probability-operator? (expression-operator ,exp))))

(defmacro check-choose-probability? (exp)
  `(legal-form? choose-probability (length>1? ,exp) ,exp))

(defmacro make-choose-probability (choices)
  `(make-expression *choose-probability* ,choices))

(defmacro make-probabilistic-choice (probability forms)
  `(cons ,probability ,forms))

(defmacro choose-probability-choices (expression)
  `(expression-operands ,expression))

(defmacro probabilistic-choice-probability (choice)
  `(first ,choice))

(defmacro probabilistic-choice-forms (choice)
  `(rest ,choice))

;;;   ************************************************************
;;;   **
;;;   **  RMPL DECISION THEORETIC CHOICE expression:
;;;   **
;;;   **     (CHOOSE-REWARD (<reward_i> . <forms_i>)+)
;;;   **
;;;   **  Let FORMS_i be a list of rmpl expressions, and
;;;   **  0 <= reward_i.
;;;   **
;;;   **  Choose-reward makes a non-deterministic choice of forms,
;;;   **  in which a reward of REWARD_i is received for choosing 
;;;   **  forms_i.  The selected forms are initiated in the same 
;;;   **  time step in which its choose-reward is executied.
;;;   **
;;;   ************************************************************

(defmacro choose-reward-operator? (op)
  
  "Returns T iff op is an RMPL choose-reward operator."

  `(eql ,op *choose-reward*))

(defmacro choose-reward? (exp) 

  "Returns T iff exp is an RMPL choose-reward expression."
  
  `(and (compound-expression? ,exp)
		(choose-reward-operator? (expression-operator ,exp))))

(defmacro check-choose-reward? (exp)
  `(legal-form? choose-reward (length>1? ,exp) ,exp))

(defmacro make-choose-reward (choices)
  `(make-expression *choose-reward* ,choices))

(defmacro make-choose-reward-choice (reward forms)
  `(cons ,reward ,forms))

(defmacro choose-reward-choices (expression)
  `(expression-operands ,expression))

(defmacro reward-choice-reward (method)
  `(first ,method))

(defmacro reward-choice-forms (method)
  `(rest ,method))


;;;   ************************************************************
;;;   **
;;;   **  RMPL D0-WATCH expression:
;;;   **
;;;   **            (DO-WATCHING <condition> <form>+)
;;;   **
;;;   **  Given a wff CONDITION, and a list FORMS of rmpl expressions,
;;;   **  Returns an rmpl expression that executes FORMS
;;;   **  sequentially, while monitoring CONDITION.  If condition
;;;   **  is satisfied, then terminates execution in that state.
;;;   **
;;;   ************************************************************

(defmacro do-watching-operator? (op)
  
  "Returns T iff op is an RMPL do-watching operator."

  `(eql ,op *do-watching*))

(defmacro do-watching? (exp) 

  "Returns T iff exp is an RMPL do-watching expression."
  
  `(and (compound-expression? ,exp)
		(do-watching-operator? (expression-operator ,exp))))

(defmacro check-do-watching? (exp)
  `(legal-form? do-watching (length>2? ,exp) ,exp))

(defmacro make-do-watching (condition forms)

  "Given a wff CONDITION, and a list FORMS of rmpl expressions,
   Returns an rmpl expression that executes FORMS
   sequentially, while monitoring CONDITION.  If condition
   is satisfied, then terminates execution in that state."

  `(make-expression *do-watching* (cons ,condition ,forms)))

(defmacro do-watching-condition (expression)
  `(first (expression-operands ,expression)))

(defmacro do-watching-forms (expression)
  `(rest (expression-operands ,expression)))

;;;   ************************************************************
;;;   **
;;;   **  RMPL NEXT:
;;;   **
;;;   **            (NEXT <form>)
;;;   **
;;;   ** Initiate form in the next step.
;;;   **
;;;   ************************************************************

;;;   See temporal-logic-expressions.lisp

;;;   ************************************************************
;;;   **
;;;   **  RMPL SEQUENCE expression:
;;;   **
;;;   **            (SEQUENCE <form>+)
;;;   **
;;;   ************************************************************

(defmacro sequence-operator? (op)
  
  "Returns T iff op is an RMPL sequence operator."

  `(eql ,op *sequence*))

(defmacro sequence? (exp)

  "Returns T iff exp is an RMPL sequence expression."
  
  `(and (compound-expression? ,exp)
		(sequence-operator? (expression-operator ,exp))))

(defmacro check-sequence? (exp)
  `(legal-form? sequence (length>1? ,exp) ,exp))

(defmacro make-rmpl-sequence (forms)
  `(make-expression *sequence* ,forms))

(defmacro sequence-forms (expression)
  `(expression-operands ,expression))


;;;   ************************************************************
;;;   **
;;;   **  RMPL Temporal IF:
;;;   **
;;;   **        (IF-ELSE-NEXT <condition> <then-form> <else-form>)
;;;   **
;;;   ** If <condition> is entailed in the start state, 
;;;   **   then THEN-FORM is evaluated in the next state.
;;;   ** If (not <condition>) is entailed in the start state, 
;;;   **   then ELSE-FORM is evaluated in the next state.
;;;   **
;;;   ************************************************************

(defmacro if-else-next-operator? (op)
  
  "Returns T iff op is an RMPL if-else-next operator."

  `(eql ,op *if-else-next*))

(defmacro if-else-next? (exp)

  "Returns T iff exp is an RMPL if-else-next expression."
  
  `(and (compound-expression? ,exp)
		(if-else-next-operator?
		 (expression-operator ,exp))))

(defmacro check-if-else-next? (exp)
  `(legal-form? if-else-next (length-4? ,exp) ,exp))

(defmacro make-if-else-next (condition true-form false-form)
  `(make-expression *if-else-next* 
					(list ,condition ,true-form ,false-form)))

(defmacro if-else-next-condition (expression)
  `(first (expression-operands ,expression)))

(defmacro if-else-next-true-form (expression)
  `(second (expression-operands ,expression)))

(defmacro if-else-next-false-form (expression)
  `(second (expression-operands ,expression)))

;;;   ************************************************************
;;;   **
;;;   **  RMPL Iteration over combinators:
;;;   **
;;;   **            (Every (var domain) <form>+)
;;;   **
;;;   ** Instantiates <forms> with var bound to each element in 
;;;   ** domain.  Executes each instantiation in parallel, when 
;;;   ** EVERY is initiated.  Executes FORMS for each instantiation
;;;   ** in sequence.
;;;   **
;;;   ************************************************************

;;; To Do:
;;; o Write check-* fn.

;;; Fix:
;;; o Does this conflict with MPL?
(defmacro every-combinator? (op)
  
  "Returns T iff op is an RMPL every operator."

  `(eql ,op *every-combinator*))

(defmacro combinator-every? (exp) 

  "Returns T iff exp is an RMPL every expression."
  
  `(and (compound-expression? ,exp)
		(every-combinator? (expression-operator ,exp))))

(defmacro make-rmpl-iteration (iterator variable domain forms)

  "Given a wff CONDITION, and a list FORMS of rmpl expressions,
   Returns an rmpl expression that executes FORMS
   sequentially, if CONDITION is satisfied in the initial state
   in which that expression is executed."

  `(make-expression ,iterator
					(cons (list ,variable ,domain) 
						  ,forms)))

(defmacro rmpl-iteration-variable (expression)
  `(first (first (expression-operands ,expression))))

(defmacro rmpl-iteration-domain (expression)
  `(second (first (expression-operands ,expression))))

(defmacro rmpl-iteration-forms (expression)
  `(rest (expression-operands ,expression)))