;;; -*- Mode:Common-Lisp; Package:mtk-utils; Base:10 -*-

;;; Copyright (c) 1999 - 2008 Brian C. Williams, MIT.

;;; This software may not be redistributed, and can only be retained
;;; and used with the explicit written consent of the author,
;;; Brian C. Williams.

;;; This software is made available as is; no guarantee is provided
;;; with respect to performance or correct behavior.
;;; This software may only be used for non-commercial, non-profit,
;;; research activities.

(in-package :mtk-utils)

;;; ***********************************************************************
;;;
;;;                      ***  Things To Do ***
;;;
;;; ***********************************************************************

;;; o Move all check-* functions to this file.
;;;   Write as needed, in the style of rmpl-expression checks.

;;; o Make these functions inline, rather than using macros.

;;; o Most of the following can be generated using defstructs
;;;   in which the type is an unnamed list.

;;; o Add comments.

;;; ***********************************************************************
;;;
;;;    ***  Constants for Non-deterministic Choice Operators ***
;;;
;;; ***********************************************************************

(defconstant *named-choose* 'named-choose)

(defconstant *choose* 'choose)

(defconstant *type-choose* 'type-choose)

(defconstant *unnamed-choice-ops* (list *choose* *type-choose*))

(defconstant *choice-ops* (cons *named-choose* *unnamed-choice-ops*))

;;; *********************************************************************
;;;
;;; ***  Non-deterministic Choice ***
;;;
;;; *********************************************************************

;;; In support of :wff and, in turn, :mpl.

(defmacro choice-operator? (op)

  "An operator denoting a choice (named or unamed) between options."
  
  `(member ,op *choice-ops* :test #'eql))

(defmacro named-choice-operator? (op)

  "Operator denoting named choice of options."
  
  `(eql ,op *named-choose*))

(defmacro unnamed-choice-operator? (op)

  "Operator denoting an unnamed choice of options
   for a type or instance.."
  
  `(member ,op *unnamed-choice-ops* :test #'eql))

(defmacro type-choice-operator? (op)

  "An operator denoting type-choice."
  
  `(eql ,op *type-choose*))

;;; ***  Named Choice ***

(defmacro named-choice? (expression)
  `(and (compound-expression? ,expression)
        (named-choice-operator? (expression-operator ,expression))))

(defmacro make-named-choice (name choices)
  `(make-expression 'NAMED-CHOOSE (cons ,name ,choices)))

(defmacro named-choice-name (expression)
  `(second ,expression))

(defmacro named-choice-choices (expression)
  `(nthcdr 2 ,expression))

;;; ***  Unnamed Instance Choice ***

;;; A choice that is made once per instance.
(defmacro choice? (expression)
  `(and (compound-expression? ,expression)
        (choice-operator? (expression-operator ,expression))))

(defmacro make-choice (choices)
  `(make-expression *choose* ,choices))

(defmacro choice-choices (expression)
  `(rest ,expression))

;;; ***  Type Choice ***

;;; A choice that is made once per type,
;;; and holds for all instances of type.
(defmacro type-choice? (expression)
  `(and (compound-expression? ,expression)
        (type-choice-operator? (expression-operator ,expression))))

(defmacro make-type-choice (choices)
  `(cons *type-choose* ,choices))

(defmacro type-choice-choices (expression)
  `(rest ,expression))

(defmacro unnamed-choice-choices (expression)
  `(rest ,expression))
