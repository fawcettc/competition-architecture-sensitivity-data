;;; -*- Mode:Common-Lisp; Package:mtk-utils; Base:10 -*-

;;; Copyright (c) 1999 - 2010 Brian C. Williams, MIT.

;;; This software may not be redistributed, and can only be retained
;;; and used with the explicit written consent of the author,
;;; Brian C. Williams.

;;; This software is made available as is; no guarantee is provided
;;; with respect to performance or correct behavior.

;;; This software may only be used for non-commercial, non-profit,
;;; research activities.

(in-package :mtk-utils)

;;;(eval-when (compile load eval)
;;;  (export '(*debug*
;;;			debug-progn
;;;
;;;			debug-print-progn
;;;			debug-print
;;;
;;;			debug-print-on
;;;			debug-print-off
;;;
;;;			debug-print-progn-for
;;;			debug-print-for
;;;
;;;			declare-debug-print-system!
;;;			debug-print-on-for
;;;			debug-print-off-for
;;;			debug-print-systems
;;;
;;;			debug-level
;;;			set-debug-level
;;;			)
;;;		  :mtk-utils)
;;;    
;;;  (import '(
;;;			declare-debug-print-system!
;;;			debug-print-systems
;;;			debug-print-on-for
;;;			debug-print-off-for
;;;			
;;;			debug-level
;;;			set-debug-level
;;;			)
;;;		  :cl-user)
;;;  )

;;; To Do:
;;; o Implement debug-level for for debug-print-for.
;;;   Each print statement should be indented proportional to the debug level.

;;; o Debug-level, Debug-print and Debug-progn are made obsolete by
;;;   _-for.  Remove its use and delete.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;  
;;;  Debugging Related functions
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;  
;;;  Evaluating expressions only when debugging
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defvar *debug* nil
  "This variable controls whether or not the system is built for
   debugging.  This variable is expected to be NIL during normal
   operation."
  )

(defmacro debug-progn (&rest body)
  
  " Only evaluate expression when debuggging is turn on."

  `(when *debug* ,@body))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;  
;;  Printing debugging information, 
;;  enabled for a particular system.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defvar *debug-print-p* nil

  "This variable controls whether or not DEBUG-PRINT prints anything. 
   DEBUG-PRINT prints only if this variable is non-NIL."
  )

(defmacro debug-print-progn (&rest body)

  "Evaluates BODY iff DEBUG-PRINT is set on."
  
  `(when *debug-print-p* ,@body))

(defun debug-print (stream format-string &rest args)
  
  "This function prints out FORMAT-STRING with ARGS to STREAM,
   only if debug-print is on.  It behaves exactly like FORMAT, 
   and is provided as a way of turning off printing."

;;; (declare (ignore stream))
  (when *debug-print-p*
    (format 
     stream
	 ;; log:*rax-log* 
     "~&;; ~a ~%" (apply #'format nil  format-string args))))

(defun debug-print-on ()
  
  "Turns debug printing on."
  
  (setq *debug-print-p* 't))

(defun debug-print-off ()

  "Turns debug printing off."
  
  (setq *debug-print-p* 'nil))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;  
;;  Printing debugging information, 
;;  enabled for a particular system.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defvar *debug-progn-on-for* nil
  "List of system-names currently being traced for progn's.")

(defvar *debug-print-on-for* nil
  "List of system-names currently-being traced.")

(defvar *debug-print-systems* nil 
  "List of the legal system names that can be used by debug-print.")

;;;(defvar *indent-debug-print-by* 2
;;;  "Specifies the number of spaces to indent for each indentation level")

(defun declare-debug-print-system! (system-name)
  
  "Specifies that SYSTEM-NAME is a valid system-name to
   be used in debug printing."
  
  (setq *debug-print-systems*
    (adjoin system-name *debug-print-systems*)))

(defun debug-print-systems ()

  "Returns the list of systems that can be
   traced by debug-print."
  
  *debug-print-systems*)

(defun check-valid-debug-print-system (system-name)
  
  "Returns True iff SYSTEM-NAME is defined as a
   system for debug print.  Prints a warning if
   SYSTEM-NAME is not valid."
  
  (cond ((member system-name *debug-print-systems*)
		 t)
		(:otherwise
		 (format t "~%~a is not a valid debug-print-name, ~
                    use one of ~a."
				 system-name  (debug-print-systems))
		 nil)))

(defun debug-progn-on-for? (system-name)

  "Returns true if debug-progn is enabled-for SYSTEM-NAME."
  
  (member system-name *debug-progn-on-for*))

(defun debug-print-on-for? (system-name)

  "Returns true if debug-print is enabled-for SYSTEM-NAME."
  
  (member system-name *debug-print-on-for*))

(defmacro debug-progn-for (system-name &rest body)
  
  "Evaluates BODY iff debug-progn is on for SYSTEM-NAME."
  
  `(when (debug-progn-on-for? ,system-name) ,@body))

(defmacro debug-print-progn-for (system-name &rest body)
  
  "Evaluates BODY iff debug-print is on for SYSTEM-NAME."
  
  `(when (debug-print-on-for? ,system-name) ,@body))

;;; Check:
;;; o Using apply or funcall, below both work.  Confirm which is
;;;   more efficient.
;;;     I think both are equivalent please check - Henri Badaro.
(defmacro debug-print-for (system-name stream format-string &rest args)
  
  "Prints out FORMAT-STRING with ARGS to STREAM for SYSTEM-NAME
   iff debug-print is on for SYSTEM-NAME.

   Behaves exactly like FORMAT, except that printing
   is turned off unless debug-print is enabled for SYSTEM-NAME."

;;; (declare (ignore stream))
  
  `(when (debug-print-on-for? ,system-name)
	 
	 (format 
	  ,stream
	  "~&;;~a: ~a ~%" 
	  ,system-name
;;;	 (funcall #'format nil ,format-string . ,args)
	  (apply #'format (list nil  ,format-string ,@args))
	  )))

(Defun debug-progn-on-for (&optional system-name)

  "Adds SYSTEM-NAME to the list of systems for which
   debug-progn is turned on.  Returns the list of
   systems currently being debug-progn traced.

   If system-name is NIL, then returns the list of all
   systems currently being traced."
  
  (if (null system-name)
	  
      *debug-progn-on-for*
	
    (when (check-valid-debug-print-system system-name)
	  
      (setq *debug-progn-on-for*
		(adjoin system-name *debug-progn-on-for*)))))

(Defun debug-print-on-for (&optional system-name)

  "Adds SYSTEM-NAME to the list of systems for which
   debug-print is turned on.  Returns the list of
   systems currently being debug-print traced.

   If system-name is NIL, then returns the list of all
   systems currently being traced."
  
  (if (null system-name)
	  
      *debug-print-on-for*
	
    (when (check-valid-debug-print-system system-name)
	  
      (setq *debug-print-on-for*
		(adjoin system-name *debug-print-on-for*)))))

(defun debug-progn-off-for (&optional system-name)

  "Removes SYSTEM-NAME from the list of systems for which
   debug-progn is turned on.  

   If no SYSTEM-NAME is provided or NIL is supplied,
   then turns off debug-progn for all systems.

   Returns the list of systems currently being debug-progn traced,
   after deletion."
  
  (if (null system-name)
	  
      (setq *debug-progn-on-for* nil)
	
    (when (check-valid-debug-print-system system-name)
	  
      (setq *debug-progn-on-for*
		(delete system-name *debug-progn-on-for*)))))

(defun debug-print-off-for (&optional system-name)

  "Removes SYSTEM-NAME from the list of systems for which
   debug-print is turned on.  

   If no SYSTEM-NAME is provided or NIL is supplied,
   then turns off debug-print for all systems.

   Returns the list of systems currently being debug-print traced,
   after deletion."
  
  (if (null system-name)
	  
      (setq *debug-print-on-for* nil)
	
    (when (check-valid-debug-print-system system-name)
	  
      (setq *debug-print-on-for*
		(delete system-name *debug-print-on-for*)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;  
;;  Setting the level of debugging information
;;  printed out.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; The following routines set and examine the current debugging level.  
;;; The debugging level is an integer from 0 to 3, specifying the level of
;;; detailed of the debugging information printed out.  Currently, the levels 
;;; have the following meaning:
;;;   0  means that no trace information is printed out, and no debugging 
;;;      information is stored;
;;;   1  means that some trace information is printed out, and no debugging
;;;      information is stored;
;;;   2  means that some trace information is printed out, and debugging
;;;      information is stored, and
;;;   3  means that more trace information is printed out, and debugging 
;;;      information is stored.

(defvar *debug-level* 0 "The level of debugging information.")

(defun set-debug-level (&optional (level 2))

  "Level is an integer between 0 and 3."
  
  (if (member level '(0 1 2 3))
	  
      (setq *debug-level* level)
	
    (cerror "Continue" "Unknown level ~a; Must be one of 0, 1, 2, or 3"
			level)))

(defun debug-level () *debug-level*)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;  Special format-time
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defvar *format-time* nil)

(defun format-time (destination control-string &rest arguments)
  (let ((dtime (get-internal-real-time)))
	(unless *format-time* 
	  (setf *format-time* dtime))
	(setf dtime (- dtime *format-time*))
	(multiple-value-bind (s d) (floor dtime internal-time-units-per-second)
	  (multiple-value-bind (m s) (floor s 60)
		(multiple-value-bind (h m) (floor m 60)
		  (format destination (colorize :blue (format nil "~D:~2,'0D:~2,'0D:~3,'0D: " h m s d))))))
	(apply #'format destination control-string arguments)))

;; Helper function to reset the time.
(defun format-time-reset ()
  (setf *format-time* (get-internal-real-time)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; A few more format helpers
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun format-error (destination control-string &rest arguments)
  (format destination (colorize :red "Error: "))
  (apply #'format destination control-string arguments))

(defun format-warning (destination control-string &rest arguments)
  (format destination (colorize :yellow "Warning: "))
  (apply #'format destination control-string arguments))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Color outputs for terminals!!! (Doesn't work in
;; emacs though). Note that the default is to not use
;; any color ouput - it only terns on if a 
;; colorize-set-<output> method is called.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defvar *colorize-output-mode* :none)
;; Current supported colorize modes: :terminal, and :none

(defun colorize-set-none ()
  (setf *colorize-output-mode* :none))

(defun colorize-set-terminal ()
  (setf *colorize-output-mode* :terminal))

(defun colorize (color-kw text)
  (let* ((color-code ""))
	(cond ((equalp *colorize-output-mode* :terminal)
		   (cond ((equalp color-kw :red)
				  (setf color-code "[31m"))
				 ((equalp color-kw :green)
				  (setf color-code "[32m"))
				 ((equalp color-kw :yellow)
				  (setf color-code "[33m"))
				 ((equalp color-kw :blue)
				  (setf color-code "[34m"))
				 ((equalp color-kw :purple)
				  (setf color-code "[35m"))) 
		   (format nil "~c~a~a~c[0m" #\esc color-code text #\esc))
		  (t ;; Otherwise, don't format the text!
		   text))))

;; Doesn't really work that well...
;; Also requires you to (setq slime-enable-evaluate-in-emacs t) in your .emacs file
;;(defun print-color-emacs (text color)
;;  (swank::eval-in-emacs
;;   `(with-current-buffer (slime-repl-buffer)
;;	 (insert (propertize ,text 'font-lock-face '(:foreground ,color))))))