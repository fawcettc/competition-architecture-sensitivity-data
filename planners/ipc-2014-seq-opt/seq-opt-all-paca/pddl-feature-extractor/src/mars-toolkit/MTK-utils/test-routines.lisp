;;; -*- Mode:Common-Lisp; Package :mtk-utils; Base:10 -*-

;;; Copyright (c) 1999 - 2010 Brian C. Williams, MIT.

;;; This software may not be redistributed, and can only be retained
;;; and used with the explicit written consent of the author,
;;; Brian C. Williams.

;;; This software is made available as is; no guarantee is provided
;;; with respect to performance or correct behavior.

;;; This software may only be used for non-commercial, non-profit,
;;; research activities.

(in-package :mtk-utils)

;;; To Do:
;;; o Add a function to reset a single test suite.  
;;; o Write a function to print all test expressions for
;;;   a suite.

;;; Check:
;;; o Make sure that when a function is redefined, it
;;;   is replaced in the suite, not duplicated.

;;;(eval-when (compile load eval)
;;;
;;;;;;  ;; Couldn't get access to util.test:test after shadowing.
;;;;;;  ;; To get around this, I didn't add util.test to the use package
;;;;;;  ;; list of :mtk-utils.   Instead I call util.test:test
;;;;;;  ;; explicitly wherever it is used.
;;;;;;
;;;;;;;;; (shadow '(test)
;;;;;;;;;   :mtk-utils)
;;;  
;;;  (export '(run-tests
;;;			print-tests
;;;			reset-test-suites
;;;			deftest-suite
;;;			deftest
;;;			;; Franz Lisp test routine
;;;			;; Note any other Franz functions used 
;;;			;; must be exported from mtk-utils as well.
;;;            test
;;;			)
;;;		  :mtk-utils)
;;;  
;;;  (import '(run-tests)
;;;		  :cl-user)
;;;  )

;;; (DEFTEST-SUITE NAME START-LOG-EXPRESSION STOP-AND-PRINT-LOG-EXPRESSION)
;;;     Defines a test suite called NAME.
;;;
;;;     NAME is a symbol, by convention a keyword.
;;;
;;;     START-LOG-EXPRESSION is a lisp expression that is
;;;     evaluated just before the tests of SUITE are invoked,
;;;     and begins logging test data for the tests in SUITE.
;;;
;;;     STOP-PRINT-LOG-EXPRESSION is a lisp expression that is
;;;     called after all test expressions in SUITE are evaluated.
;;;     It terminates logging data and prints thata data.

;;; (RESET-TEST-SUITES)
;;;    Resets all data associated with all test suites.

;;; (DEFTEST (NAME SUITE-NAME) () &REST BODY)
;;;
;;;     Defines a test function:
;;;      (defun name () &rest body)
;;;     which is evaluated when test suite SUITE-NAME is run."

;;; (RUN-TESTS SUITE)
;;;   Runs all test functions in test SUITE, where SUITE
;;;   is a symbol and by convention a keyword.
;;;   Starts logging data before running tests and
;;;   stops and prints this data at the end using the
;;;   logging functions associated with SUITE.

;;; (PRINT-TESTS SUITE)

;;;   Prints all test functions in test SUITE, 
;;;   and log function calls
;;;   where SUITE is a symbol and by convention a keyword.

;;; Things to Do:

;;; o Write a TEST macro that shadows the Allegro test fns (and like)
;;;   and makes the problem with multiple values returned transparent.
;;;   To do this call (shadow test) above.  Then write a function
;;;   that checks if the multiple-values keyword is bound to true, 
;;;   and if so,  removes it, and wraps multiple-value-list around
;;;   the call.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;                                                               ;;;
;;;             Note About Writing Franz Tests                    ;;;
;;;                                                               ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; To ensure that the Franz Lisp test infrastructure is loaded,
;;; in the mtk-utils system definition:
;;; 1) Include the following requires:
;;;             (require :tester)

#+allegro
(eval-when (compile) (require :tester))

(defvar *num-tests-passed* 0 "Records number of tests passed.")
(defvar *num-tests-failed* 0 "Records number of tests failed.")

(defmacro allegro-test (expected-form test-form &key (test '(function eql)) multiple-values (fail-info "Expected: ~a.~%Actual: ~a.") known-failure)
  "This function implements a simpler version of the test() method from Franz-Allegro's util.test package."
  (declare (ignore multiple-values known-failure))
  `(let ((actual-value ,test-form)
		 (expected-value ,expected-form)
		 (fail-string ,fail-info))
	 (let ((result (funcall ,test expected-value actual-value)))
	   (if result
		   (incf *num-tests-passed*)
		   (progn
			 (incf *num-tests-failed*)
			 (when fail-string
			   (format t (concatenate 'string 
									  "~&============Expected==============~%"
									  "~a~%"
									  "============Actual================~%"
									  "~a~%"
									  "~&============Fail Info=============~%"
									  fail-string "~%"
									  "==================================~%") expected-value actual-value expected-value actual-value))))
	   result)))

(defmacro test-error (form &rest rest &key announce catch-breaks fail-info known-failure (condition-type (quote simple-error)) include-subtypes format-control format-arguments)
  "This function implements a simpler version of the test-error() method from Franz-Allegro's util.test package."
  #+allegro
  (declare (ignore announce catch-breaks fail-info known-failure condition-type include-subtypes format-control format-arguments))
  #-allegro
  (declare (ignore form rest announce catch-breaks fail-info known-failure condition-type include-subtypes format-control format-arguments))
  #+allegro
  `(util.test::test-error ,form ,@rest)
  #-allegro
  nil)

(defmacro test-no-error (form &rest rest &key announce catch-breaks fail-info known-failure)
  "This function implements a simpler version of the test-error() method from Franz-Allegro's util.test package."
  #+allegro
  (declare (ignore announce catch-breaks fail-info known-failure condition-type include-subtypes format-control format-arguments))
  #-allegro
  (declare (ignore form rest announce catch-breaks fail-info known-failure))
  #+allegro
  `(util.test::test-no-error ,form ,@rest)
  #-allegro
  nil)

;;; 2) Import any desired external functions of package :util.test.

;;; Note: Franz's test facility util.test:test is supposed to 
;;  take multiple returned values from a test function, and 
;;  collect them into a list when :multiple-values is set to T.
;;; This functionality does not appear to be working properly.

;;; Instead below we implement this functionality by shadowing
;;; util.test:test with a macro function that calls util.test:test
;;; while replacing TESTFORM with (multiple-value-list TESTFORM) 
;;; when :MULTIPLE-VALUES is non-nil, and while removing that keyword
;;; value pair:

(defmacro test (&rest args)
  "The :FAIL-INFO data is passed directly to FORMAT, so any and all
FORMAT directives are allowed. However, only two format arguments are
supplied. The first is EXPECTED-VALUE, the second is TEST-FORM."
  (let* ((keywords (nthcdr 2 args))
		 (multiple-values? 
		  (getf keywords :multiple-values)))
	(if (null multiple-values?)
		
		`(allegro-test ,@args)
		
		(let ((expected-value (first args))
			  (test-form (second args))
			  (nkeywords (copy-list keywords)))
		  
		  (remf nkeywords :multiple-values)
		  `(allegro-test ,expected-value
						 (multiple-value-list ,test-form)
						 . ,nkeywords)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;                                                               ;;;
;;;             Generic Support for Test Suites                   ;;;
;;;                                                               ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(eval-when (compile load eval)
  (defvar *test-suites-initialized?* nil)
  (defvar *test-suites-test-expressions* (make-hash-table :test #'equal))
  (defvar *test-suites-start-log* '())
  (defvar *test-suites-stop-and-print-log* '())
  
  (defmacro test-suite-test-expressions (suite)

    "Returns a list of lisp expressions to be evaluated,
     associated with test SUITE."

	`(gethash ,suite *test-suites-test-expressions*))
  
  (defmacro test-suite-start-log (suite)

    "Returns an expression that when evaluated,
     begins logging data for test SUITE."

    ` (getf *test-suites-start-log* ,suite))
  
  (defmacro test-suite-stop-and-print-log (suite)

    "Returns an expression that when evaluated,
     stops logging data for test SUITE, and prints
     that data."

    ` (getf *test-suites-stop-and-print-log* ,suite))

  (defun add-test-to-suite (test suite)

    "Adds TEST to those to be performed when test SUITE
     is run, where

       TEST is a function that takes no arguments.
       SUITE is a symbol, by convention a keyword."
    
    (setf (test-suite-test-expressions suite)
      (adjoin (list test) 
			  (test-suite-test-expressions suite)
			  :test #'equal)))
  
  (defun add-start-stop-print-to-test-suite 
      (suite start-log-expression stop-print-log-expression)

    "Associates START-LOG and STOP-PRINT-LOG expressions
     with test SUITE.

     START-LOG-EXPRESSION is a lisp expression that is
     evaluated just before the tests of SUITE are invoked,
     and begins logging test data for the tests in SUITE.

     STOP-PRINT-LOG-EXPRESSION is a lisp expression that is
     called after all test expressions in SUITE are evaluated.
     It terminates logging data and prints thata data."
	
    (setf (test-suite-start-log suite)
      start-log-expression)
    (setf (test-suite-stop-and-print-log suite)
      stop-print-log-expression))

  (defun reset-test-suites ()
	
    "Resets all data associated with all test suites."

	(clrhash *test-suites-test-expressions*)
    (setq *test-suites-start-log* '()
		  *test-suites-stop-and-print-log* '()))
  
  (defun initialize-test-suites ()

	(unless *test-suites-initialized?*
	  
	  (reset-test-suites)
	  (setq *test-suites-initialized?* t)))
  )

(defmacro deftest-suite (name 
						 &optional
						 start-log-expression
						 stop-and-print-log-expression)

  "Defines a test suite called NAME.

     NAME is a symbol, by convention a keyword.

     START-LOG-EXPRESSION is a lisp expression that is
     evaluated just before the tests of SUITE are invoked,
     and begins logging test data for the tests in SUITE.
     Provide nil for no start log expression.

     STOP-PRINT-LOG-EXPRESSION is a lisp expression that is
     called after all test expressions in SUITE are evaluated.
     It terminates logging data and prints thata data.
     Provide nil for no stop and print log expression."

  `(eval-when (compile load eval)
	 
     (add-start-stop-print-to-test-suite
      ',name ',start-log-expression
      ',stop-and-print-log-expression)))
  

(defvar *suite-test-stats* nil 
  "A list of plists containing information on every test run in the
   suite. Properties are: :NAME :PASSED? :NUM-PASSED :NUM-FAILED")

(defmacro deftest ((name suite-name) () &rest body)

  "Defines a test function:
      (defun name () &rest body)
   which is evaluated when test suite SUITE-NAME is run."
  
  `(eval-when (compile load eval)
	 
	 (add-test-to-suite ',name ',suite-name)
	 (defun ,name ()
	   ;; Establish a new context for the number of calls to TEST
	   ;; macro that fail or pass within this DEFTEST.
	   (let ((*num-tests-passed* 0)
			 (*num-tests-failed* 0)
			 (result nil)
			 (exception? nil))
		 ;; Wrap the actual test in a restart case to provide the
		 ;; option higher up the stack to automatically continue the
		 ;; test or drop into the debugger.
		 (restart-case (setf result (progn . ,body))
		   ;; If there was an exception, set the flag.
		   (skip-test () (progn
						   (setf result nil)
						   (setf exception? T))))
		 ;; Record statistics 
		 (push (list :name (quote ,name) 
					 :passed? (and (not exception?) (eql 0 *num-tests-failed*))
					 :num-passed *num-tests-passed* 
					 :num-failed *num-tests-failed*) 
			   *suite-test-stats*)
		 ;; Return the result like expected.
		 result))))

(defun print-stats (suite-name suite-stats-plist)
  (let ((passed? (loop for test in suite-stats-plist always (getf test :passed?) )))
	(format t "~&=================== Test Results for ~a ===================
~aSuite name: ~a
~aStatus: ~:[FAIL~;PASS~]
~:[~aTests Failed: ~{~a~^~%                  ~}~;~]~%" suite-name
                                      #\Tab suite-name
									  #\Tab passed?
									  passed? #\Tab (map 'list #'(lambda (x) (getf x :name)) 
												 (remove-if #'(lambda (x) (getf x :passed?)) 
															suite-stats-plist))))
  )
(defvar *test-results* nil "List of plists for every test
suite. Properties are :SUITE-NAME :RESULTS.")

;;; Optimization:
;;; o Avoid consing in the call to reverse.
(defun run-tests (suite &key (print-summary? T) (pre-clear-*test-results*? T) (unattended? 'nil))

  "Runs all test functions in test SUITE, where SUITE
   is a symbol and by convention a keyword.

   If SUITE is :ALL, then runs all defined suites.

   Starts logging data before running tests and
   stops and prints this data at the end using the
   logging functions associated with SUITE."
  
  (when pre-clear-*test-results*?
	(setf *test-results* nil))
  
  (cond ((eql suite :ALL)

		 (run-all-suites :unattended? unattended?)
		 (when print-summary? (loop for suite in *test-results* do (print-stats (getf suite :suite-name) (getf suite :results)))))
		
		(:otherwise
		 
		 (let ((start-exp (test-suite-start-log suite))
			   (stop-print-exp (test-suite-stop-and-print-log suite)))
	
		   (unless (null start-exp)
			 (eval start-exp))
	
		   ;; Establish new binding for the stats related to this
		   ;; suite.
		   (let ((*suite-test-stats* nil))
			 (loop for test-expression 
				in (reverse (test-suite-test-expressions suite))
				do (format t "~& Evaluating ~a." test-expression)
				  (if unattended?
					  (handler-bind ((error #'(lambda (c) 
												(format t "~&~aFailed due to exception!" #\Tab)
												(invoke-restart 'skip-test))))
						(eval test-expression)

						;; (multiple-value-bind (result exception?) (eval test-expression)
						;;   (declare (ignore result))
						;;   (when exception? (format t "~&~aFailed due to exception!" #\Tab)))
						)
					  (eval test-expression)))
			 (push (list :suite-name suite :results *suite-test-stats*) *test-results*)
			 (when print-summary?
			   (print-stats suite *suite-test-stats*)))
	
		   (unless (null stop-print-exp)
	  
			 (eval stop-print-exp))
	
		   'T))))

(defun print-tests (suite)

  "Prints all test functions in test SUITE, 
   and log function calls.

   SUITE is a symbol and by convention a keyword."
  
  (let ((start-exp (test-suite-start-log suite))
		(stop-print-exp (test-suite-stop-and-print-log suite)))
	
    (unless (null start-exp)
	  
      (format t "~%Starts by enabling logging with ~A."
			  start-exp))
	
    (loop for test-expression 
		in (reverse (test-suite-test-expressions suite))
		do (format t "~% Evaluates ~a" test-expression))
	
    (unless (null stop-print-exp)
	  
      (format t "~%Ends by disabling logging and printing with ~A."
			  start-exp))
	
    'T))

(defun run-all-suites (&key (unattended? T))
  
  "Runs all tests for each defined suite."

  (do-ht (suite test-expressions *test-suites-test-expressions*)
	(declare (ignore test-expressions))
	(format t "~&=================== Beginning Tests For ~a ===================~%" suite)
	(run-tests suite :print-summary? nil :pre-clear-*test-results*? nil :unattended? unattended?))
  'T)
