;;; -*- Mode:Common-Lisp; Package:mtk-utils; Base:10 -*-

;;; Copyright (c) 1999 - 2013 Brian C. Williams, MIT.

;;; This software may not be redistributed, and can only be retained
;;; and used with the explicit written consent of the author,
;;; Brian C. Williams.

;;; This software is made available as is; no guarantee is provided
;;; with respect to performance or correct behavior.

;;; This software may only be used for non-commercial, non-profit,
;;; research activities.

(in-package :mtk-utils)

;;; ***********************************************************************
;;;
;;;          ***  Utilities for Manipulating Strings ***
;;;
;;; ***********************************************************************

(defun string-to-list (str)
  "Converts a string with whitespace-separated elements into a list."
  (let ((str-list nil))
	(do ((values 
		  (multiple-value-list (read-from-string str nil nil :start 0))
		  (multiple-value-list (read-from-string str nil nil :start (second values))))) 
		
		((null (first values)) (nreverse str-list))		 
	  
	  (push (first values) str-list))))