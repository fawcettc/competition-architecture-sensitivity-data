;;; -*- Mode:Common-Lisp; Package:mtk-utils; Base:10 -*-

;;; Copyright (c) 1999 - 2008 Brian C. Williams, MIT.

;;; This software may not be redistributed, and can only be retained
;;; and used with the explicit written consent of the author,
;;; Brian C. Williams.

;;; This software is made available as is; no guarantee is provided
;;; with respect to performance or correct behavior.
;;; This software may only be used for non-commercial, non-profit,
;;; research activities.

(in-package :mtk-utils)

;;; ***********************************************************************
;;;
;;;                      ***  Things To Do ***
;;;
;;; ***********************************************************************

;;; Things to do:

;;; o Make these functions inline, rather than using macros.

;;; o Add comments.

;;;   ************************************************************
;;;   **
;;;   **  Error Checking
;;;   **
;;;   ************************************************************

;;; *** The following performs minimal error checking for now. 

(defmacro legal-form? (type test expression)
  `(if ,test
	   t
	 ;; Evaluates to NIL of course.
	 (format t "Badly formed ~S:  ~S~%" ',type ,expression)))

;;; *********************************************************************
;;;
;;; ***  Compound Expressions ***
;;;
;;; *********************************************************************

(defmacro compound-expression? (exp)
  `(consp ,exp))

(defmacro make-expression (operator operands)
  `(cons ,operator ,operands))

(defmacro expression-operator (exp)
  `(first ,exp))

(defmacro expression-operands (exp)
  `(rest ,exp))

;;; ***  Unary Expressions

(defmacro unary-expression? (exp)
  `(and (compound-expression? ,exp)
		(length-1? (expression-operands ,exp))))

(defmacro make-unary-expression (operator operand)
  `(list ,operator ,operand))

(defmacro unary-operand (exp)
  `(first (expression-operands ,exp)))

;;; ***  Binary Expressions

(defmacro binary-expression? (exp)
  `(and (compound-expression? ,exp)
		(length-2? (expression-operands ,exp))))

(defmacro make-binary-expression 
	(operator left-operand right-operand)
  `(list ,operator ,left-operand ,right-operand))

(defmacro left-operand (exp)
  `(first (expression-operands ,exp)))

(defmacro right-operand (exp)
  `(second (expression-operands ,exp)))

