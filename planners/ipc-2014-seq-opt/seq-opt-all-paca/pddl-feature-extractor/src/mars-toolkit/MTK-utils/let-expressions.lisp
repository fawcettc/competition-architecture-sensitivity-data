;;; -*- Mode:Common-Lisp; Package:mtk-utils; Base:10 -*-

;;; Copyright (c) 1999 - 2008 Brian C. Williams, MIT.

;;; This software may not be redistributed, and can only be retained
;;; and used with the explicit written consent of the author,
;;; Brian C. Williams.

;;; This software is made available as is; no guarantee is provided
;;; with respect to performance or correct behavior.
;;; This software may only be used for non-commercial, non-profit,
;;; research activities.

(in-package :mtk-utils)

;;; ***********************************************************************
;;;
;;;                      ***  Things To Do ***
;;;
;;; ***********************************************************************

;;; o Make these functions inline, 
;;;   rather than using macros.

;;; o Unify the form of LET for WFF/MPL and 
;;    RMPL, if possible;
;;;   Otherwise,
;;;   - distinguish who uses accessors
;;;     by MPL-* and RMPL-*, and
;;;   - pull treatment of let out of WFF
;;;     and into MPL.

;;; o Write check functions for LET.

;;; o Unify the form and treatment of the
;;;   iterators for RMPL and WFF/MPL.
;;;   - accessors are defined in quantifier-expressions.lisp
;;;   - RMPL currently has an EVERY combinator, as well
;;;     as quantifier terms of 'some and 'every.
;;;     This represents a conflict in names.
;;;     Do we want/need this redundancy?  
;;;     It seems like we should eliminate the EVERY combinator.

;;; ***********************************************************************
;;;
;;;  ***  Constants for Let Expression ***
;;;
;;; ***********************************************************************

(defconstant *let* 'let
  "The symbol denoting the let operator.")

;;; *********************************************************************
;;;
;;;               ***  Let Expression ***
;;;
;;; *********************************************************************

(defmacro let-operator? (op)

  "Returns true iff OP is an operator denoting a LET."
  
  `(eql ,op *let*))

;;; Supports :RMPL and :WFF (and in turn :MPL).
(defmacro let? (expression)
  
  "Given a symbolic EXPRESSION,
   Returns true iff EXPRESSION denotes a LET."

  `(and (compound-expression? ,expression)
        (let-operator? 
		 (expression-operator ,expression))))

;;;   ************************************************************
;;;   **
;;;   **   MPL Variant of Let:
;;;   **
;;;   **            (Let <variable-name> <form> <wff>)
;;;   **
;;;   ************************************************************

;;; Supports :WFF (and in turn :MPL), NOT :RMPL.

(defmacro let-variable (expression)
  
  "Given MPL Let EXPRESSION,
   returns the name of the variable bound in the let."
  
  `(second ,expression))

(defmacro let-form (expression)
  
  "Given MPL Let EXPRESSION,
   returns the FORM denoting the value(s) that 
   let's variable is bound to."
  
  `(third ,expression))

(defmacro let-wff (expression)

  "Given MPL Let EXPRESSION,
   returns a WFF denoting the body of the let.
   All appearances of the let's variable in WFF
   is bound to the value(s) denoted by the FORM
   of WFF."
  
  `(fourth ,expression))

;;;   ************************************************************
;;;   **
;;;   **   RMPL Variant of Let:
;;;   **
;;;   **            (LET (<binding>*) . <body>)
;;;   **
;;;   **  where each binding is of the form:
;;;   **   (<variable> <value>) OR
;;;   **   (<variable> <iterator>)
;;;   **
;;;   **  a variable is a symbol, and
;;;   **
;;;   **  an iterator is of the form:
;;;   **   (EVERY <Class>)
;;;   **   (SOME <Class>)
;;;   **
;;;   **  Creates a variable/term binding environment substituting 
;;;   **  all occurrences of variable for its binding in body.
;;;   **
;;;   ************************************************************

;;; Supports :WFF (and in turn :MPL), NOT :RMPL.

(defmacro let-bindings (exp)

  "Given an RMPL LET expression
   returns the bindings of the let."

  `(first (expression-operands ,exp)))

(defmacro let-body (exp)

  "Given an RMPL LET expression
   returns the body of the let."

  `(rest (expression-operands ,exp)))

(defmacro let-variables (bindings)

  "Given a list of variable/value BINDINGS,
   returns a list of all variables of the 
   bindings."

  `(mapcar #'let-binding-variable ,bindings))

(defmacro let-values (bindings)
  
  "Given a list of variable/value BINDINGS,
   returns a list of all values of the 
   bindings."

  `(mapcar #'let-binding-value ,bindings))

(defun let-binding-variable (binding)
  
  "Given a variable/value BINDING,
   returns the variable of the binding."
  
  (if (consp binding)
	  (first binding)
	binding))

(defun let-binding-value (binding)

  "Given a variable/value BINDING,
   returns the value of the binding."

  (if (consp binding)
	  (second binding)
	nil))


