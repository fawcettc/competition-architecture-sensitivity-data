;;; -*- Mode:Common-Lisp; Package:mtk-utils; Base:10 -*-

;;; Copyright (c) 1999 - 2010 Brian C. Williams, MIT.

;;; This software may not be redistributed, and can only be retained
;;; and used with the explicit written consent of the author,
;;; Brian C. Williams.

;;; This software is made available as is; no guarantee is provided
;;; with respect to performance or correct behavior.
;;; This software may only be used for non-commercial, non-profit,
;;; research activities.

(in-package :mtk-utils)

;;; ***********************************************************************
;;;
;;;                      ***  Things To Do ***
;;;
;;; ***********************************************************************

;;; o Integrate check-* functions here with check-wff-P.

;;; o Make these functions inline, rather than using macros.

;;; o Remove calls and code for obsolete functions, as indicated below.

;;; o Most of the following can be generated using defstructs
;;;   in which the type is an unnamed list.

;;; o Add comments.

;;; ***********************************************************************
;;;
;;; ***  Constants for Defined Operators and Values ***
;;;
;;; ***********************************************************************

;;; *** Truth values

(defconstant *true* 'true "Symbol denoting a true assignment.")

(defconstant *false* 'false "Symbol denoting a false assignment.")

(defconstant *none* 'none "Symbol denoting no truth assignment.")

(defconstant *truth-values* (list *true* *false*))

;;;(defconstant *true* :true "symbol denoting a true assignment")
;;;(defconstant *false* :false "symbol denoting a false assignment")
;;;(defconstant *none* :none "symbol denoting no truth assignment")

;;; *** Primitive logical connectives

(defconstant *and* 'and
  "Denotes logical conjunction.")

(defconstant *or* 'or
  "Denotes logical disjunction.")

(defconstant *and-or* (list *OR* *AND*)
    "Denotes the conjunctive and disjunctive logical operators.")

(defconstant *not* 'not 
  "Denotes logical negation.")

(defconstant *negation-ops* (list *not*))

(defconstant *primitive-logical-ops* (list *and* *or* *not*))


;;; *** Derived logical connectives

(defconstant *implies* 'implies)

(defconstant *iff* 'iff)

(defconstant *derived-logical-ops* (list *implies* *iff*))

(defconstant *logical-ops* 
	(append *derived-logical-ops* *primitive-logical-ops*))

;;; *** Modal operator unknown

(defconstant *unknown-op* 'unknown
  "Denotes logic modal operator UNKNOWN.")

(defconstant *unknown-ops* (list *unknown-op*))


;;; *** Logical Operator Argument Count 

(defconstant *unary-logical-ops* (list *not* *unknown-op*)
  "Operators that take on exactly one argument.")

(defconstant *binary-logical-ops* (list *implies* *iff* *=*)
  "Operators that take on exactly two arguments.")

(defconstant *nary-logical-ops* (list *and* *or*)
  "Operators that can take on zero or more arguments.")

;;; *********************************************************************
;;;
;;; *** Manipulating Logical Formula ***
;;;
;;; *********************************************************************

;;; ***********************************************************************
;;;
;;; ***  Truth Values ***
;;;
;;; ***********************************************************************

(defmacro truth-value? (truth-value)
  `(member ,truth-value *truth-values* :test #'eql))

(defmacro true-value? (truth-value)

  "Returns T if truth-value is *true* and nil otherwise."
  `(eql ,truth-value *true*))

(defmacro contains-true-value? (elements)

  "Returns T if *true* is a member of ELEMENTS and nil otherwise."

  `(member *true* ,elements  :test #'eql))

(defmacro false-value? (truth-value)

  "Returns T if truth-value is *false* and nil otherwise."

  `(eql ,truth-value *false*))

(defmacro contains-false-value? (elements)

  "Returns T if *false* is a member of ELEMENTS and nil otherwise."

  `(member *false* ,elements :test #'eql))

(defmacro unknown-value? (truth-value)

  "Returns T if truth-value is *none* and nil otherwise."

  `(eql ,truth-value *none*))

(defmacro contains-unknown-value? (elements)

  "Returns T if *true* is a member of ELEMENTS and nil otherwise."

  `(member *unknown* ,elements  :test #'eql))

(defmacro opposite-truth (truth-value)

  "Returns the opposite truth assignment from
   TRUTH-VALUE."

  `(cond ((eql ,truth-value *true*) *false*)
		 ((eql ,truth-value *false*) *true*)
		 (:otherwise *none*)))

(defmacro random-truth ()

  "Returns a randomly assigned truth value,
   with the two values having equal probability
   of being assigned."

  `(if (eql (random 2) 1) *true* *false*))

(defmacro tautology? (exp)
  `(eql ,exp *true*))

(defmacro unsatisfiable? (exp)
  `(eql ,exp *false*))

;;; *********************************************************************
;;;
;;; ***  Propositions 
;;;
;;; *********************************************************************

(defun equal-propositions? (proposition1 proposition2)

  "Let PROPOSITION1 and PROPOSITION2 be propositional logic propositions,
   returns True iff the two propositions are equivalent."
  
  (equal proposition1 proposition2))

(defun proposition-less? (proposition1 proposition2)
  
  "Returns true iff PROPOSITION1 appears 
   lexicographically before PROPOSITION2."
  
  (sexp-less? proposition1 proposition2))

;;; *********************************************************************
;;;
;;; *** Compound Logical Expressions ***
;;;
;;; *********************************************************************

(defmacro compound-logical-expression? (expression)
  
  "Returns true iff expression is a compound
   expression with a valid logical operator."

  `(and (compound-expression? ,expression)
		(logical-operator? (expression-operator ,expression))))

(defmacro logical-operator? (op)
  `(member ,op *logical-ops* :test #'eql))

(defmacro primitive-logical-operator? (op)
  `(member ,op *primitive-logical-ops* :test #'eql))

(defmacro derived-logical-operator? (op)
  `(member ,op *derived-logical-ops* :test #'eql))

(defmacro unary-logical-operator? (op)
  `(member ,op *unary-logical-ops* :test #'eql))

(defmacro binary-logical-operator? (op)
  `(member ,op *binary-logical-ops* :test #'eql))

(defmacro nary-logical-operator? (op)
  `(member ,op *nary-logical-ops* :test #'eql))

;;; *********************************************************************
;;;
;;; ***  Negation
;;;
;;; *********************************************************************

(defmacro negation-operator? (op)
  `(member ,op *negation-ops* :test #'eql))

;;; To Do:
;;; o Write a separate check-* function in the style of the rmpl operators.
;;;   check (unary-expression? ,exp).
(defmacro negation? (exp)
  `(and (compound-expression? ,exp)
		(negation-operator? (expression-operator ,exp))))

;;; To Do:
;;; o Use consistent naming - negation rather than NOT.
(defmacro check-not? (exp)
  `(legal-form? NOT (length-1? (expression-operands ,exp)) ,exp))

(defmacro make-negation (negand)

  "Given a wff NEGAND,
  Returns a wff that is the negation of NEGAND."

  `(make-expression *not* (list ,negand)))

(defmacro negand (negation)

  "Given a wff NEGATION,
  Returns its NEGAND."
  
  `(first (expression-operands ,negation)))

;;; *********************************************************************
;;;
;;; ***  Literals
;;;
;;; *********************************************************************

(defun proposition-of-literal (literal)
  
  "Returns the proposition of a positive
   or negative LITERAL."
  
  (if (negation? literal)
	  (negand literal)
	literal))

(defun negate-literal (literal)

  "Return the negation of LITERAL, that is,
      LITERAL -> (not LITERAL), 
      (not LITERAL) -> LITERAL."
  
  (if (negation? literal)
      (negand literal)
    (make-negation literal)))

(defun equal-literals? (literal1 literal2)

  "Let LITERAL1 and LITERAL2 be propositional logic literals,
   returns True iff the two literals are equivalent."
  
  (if (negation? literal1)
	  (if (negation? literal2)
		  (equal-propositions? (negand literal1) 
							   (negand literal2))
		nil)
	(if (not (negation? literal1))
		(equal-propositions? literal1 literal2)
	  nil)))

(defun literal-less? (literal1 literal2)
  
  "Returns true iff LITERAL1 appears lexicographically before LITERAL2.
   Note: (not P) appears before P."
  
  (let* ((p1 (proposition-of-literal literal1))
		 (p2 (proposition-of-literal literal2))
		 (p-less? (proposition-less? p1 p2)))
	
	(cond (p-less?
		   
		   ;; proposition(literal1) < proposition(literal2),
		   ;; hence literal1 < literal2.
		   T)
		  
		  ((and (equal-propositions? p1 p2)
				(negation? literal1)
				(not (negation? literal2)))
		   
		   ;; proposition(literal1) = proposition(literal2), and
		   ;; negative?(literal1) while positive?(literal2),
		   ;; hence literal1 < literal2.

		   T)

		  (:otherwise
		   
		   ;; literal1 >= literal2.

		   nil))))

(defun find-literal-with-proposition (proposition literals)

  "Given a list of LITERALS,
   Returns the first literal in list containing PROPOSITION."
  
  (find proposition 
		literals
		:key #'proposition-of-literal
		:test #'equal-literals?))

(defun order-literals (literals)
  
  "Returns list of LITERALS in lexicographic order.
   Destructively modifies the list of LITERALS.
   Copy list first if you want to preserve LITERALS."
  
  (sort literals #'literal-less?))

;;; *********************************************************************
;;;
;;; ***  Assignment Literal
;;;
;;; *********************************************************************

(defmacro assignment-literal? (exp)

  "Returns true iff EXP is a literal denoting an 
   assignment or its negation."
  
  `(or (assignment? ,exp)
	   (and (negation? ,exp)
			(assignment? (negand ,exp)))))

;;; *********************************************************************
;;;
;;; ***  Obsolete ***
;;;
;;; *********************************************************************

;;; Fix:
;;; o Remove "state" in the name, it is
;;;   redundant and inconsistent with other names.
;;; o This is the same as proposition of literal.  
;;;   Why reimplement it here?
(defmacro state-literal-assignment (assignment-literal)
  "Returns the assignment of literal."
  `(if (negation? ,assignment-literal)
       (negand ,assignment-literal)
     ,assignment-literal))

;;; *********************************************************************
;;;
;;; ***  Disjunction
;;;
;;; *********************************************************************

;;; To Do:
;;; o Once confirmed it isn't called, delete.
;;;(defmacro or? (exp) `(eql (expression-operator ,exp) *or*))

(defmacro disjunction-operator? (op)
  
  "Returns T iff op is a disjunction operator."

  `(eql ,op *or*))

(defmacro disjunction? (exp)
  
  "Returns T iff EXP is a compound expression denoting a disjunction."

  `(and (compound-expression? ,exp)
		(disjunction-operator? (expression-operator ,exp))))

;;; To Do:
;;; o Use consistent naming - disjunction rather than OR.
(defmacro check-OR? (exp)
  `(legal-form? OR (length>1? ,exp) ,exp))

(defmacro make-disjunction (disjuncts)
  `(make-expression *or* ,disjuncts))

(defmacro disjuncts (exp)
  `(rest ,exp))

(defun strip-disjunction (clause)
  (if (disjunction? clause)
      (expression-operands clause)
    clause))

;;; *********************************************************************
;;;
;;; ***  Simplified (Implicit) Disjunctions
;;;
;;; *********************************************************************

(defun make-simplified-disjunction (disjuncts)
  
  "Given a list of zero or more disjuncts,
   Returns (OR . disjuncts), or 
   an equivalent literal or truth value.

   Note: zero disjuncts is equivalent to *false*."

  (cond 
   
   ((null disjuncts)

	;; A disjunction with no disjuncts
	;; is equivalent to *false*.
	*false*)
	  
   ((> (length disjuncts) 1)

	;; Put in an explicit OR operator.
	(make-disjunction disjuncts))
		
   (t

	;; else merely return the single disjunct
	(first disjuncts))))

(defun implicit-disjunction-disjuncts (disjunction)
  
  "Takes a formula denoting a DISJUNCTION.
   and returns a list of its disjuncts.

   The disjunction operator may be implicit, 
   if the disjunction has only one disjunct,
   that is, (and P) => P."

  (if (disjunction? disjunction)
		
	  ;; Disjunction operator is explicit,
	  ;; return operands as disjuncts.
	  (expression-operands disjunction)
	  
	;; Else DISJUNCTION is implicitly the disjunct
	;; of a disjunction with one disjunct, return it.
	(list disjunction)))

;;; *********************************************************************
;;;
;;; ***  Conjunction
;;;
;;; *********************************************************************

;;; To Do:
;;; o Once confirmed it isn't called, delete.
;;;(defmacro and? (exp) 
;;;  `(eql (first ,exp) 'AND))

(defmacro conjunction-operator? (op)
  
  "Returns T iff op is a conjunction operator."

  `(eql ,op *and*))

(defmacro conjunction? (exp)
  
  "Returns T iff WFF is a compound expression denoting a conjunction."

  `(and (compound-expression? ,exp)
		(conjunction-operator? (expression-operator ,exp))))

;;; To Do:
;;; o Use consistent naming - conjunction rather than AND.
(defmacro check-AND? (exp)
  `(legal-form? AND (> (length ,exp) 1) ,exp))

(defmacro make-conjunction (conjuncts)
  `(make-expression *and* ,conjuncts))

(defmacro conjuncts (exp)
  `(rest ,exp))

;;; *********************************************************************
;;;
;;; ***  Simplified (Implicit) Conjunctions
;;;
;;; *********************************************************************

(defun make-simplified-conjunction (conjuncts)
  
  "Given a list of zero or more conjuncts,
   Returns (AND . conjuncts), or 
   an equivalent literal or truth value.

   Note: zero conjuncts is equivalent to *true*."

  (cond 

   ((null conjuncts)

	;; A conjunction with no conjuncts
	;; is equivalent to *true*.
	*true*)
   
   ((> (length conjuncts) 1)
	;; Put in an explicit AND operator.
	(make-conjunction conjuncts))
		
   (:otherwise
	;; else merely return the single conjunct
	(first conjuncts))))

(defun implicit-conjunction-conjuncts (conjunction)
  
  "Takes a formula denoting a CONJUNCTION.
   and returns a list of its conjuncts.

   The conjunction operator may be implicit, 
   if the conjunction has only one conjunct,
   that is, (and P) => P."
  
  (if (conjunction? conjunction)
		
	  ;; Conjunction operator is explicit,
	  ;; return operands as conjuncts.
	  (expression-operands conjunction)
	  
	;; Else CONJUNCTION is implicitly the conjunct
	;; of a conjunction with one conjunct, return it.
	(list conjunction)))

;;; *********************************************************************
;;;
;;; ***  Implication
;;;
;;; *********************************************************************

;;; To Do:
;;; o Once confirmed it isn't called, delete.
;;;(defmacro implies? (exp)
;;;  `(eql (expression-operator ,exp) *implies*))

(defmacro implies-operator? (op)
  `(eql ,op *implies*))

(defmacro implication? (exp)
  `(and (compound-expression? ,exp)
		(implies-operator? (expression-operator ,exp))))

;;; To Do:
;;; o Use consistent naming - implication rather than implies.
(defmacro check-implies? (exp)
  `(legal-form? IMPLIES (length-2? (expression-operands ,exp)) ,exp))

(defmacro make-implication (antecedent consequent)
  `(make-expression *implies* (list ,antecedent ,consequent)))

(defmacro antecedent (exp)
  `(second ,exp))

(defmacro consequent (exp)
  `(third ,exp))

;;;FIX:
;;; o change callers.
(defmacro implication-antecedent (exp)
  `(antecedent ,exp))

;;;FIX:
;;; o change callers.
(defmacro implication-consequent (exp)
  `(consequent ,exp))

;;; *********************************************************************
;;;
;;; ***  Logical Equivalence (IFF)
;;;
;;; *********************************************************************

(defmacro iff-operator? (op)
  `(eql ,op *iff*))

(defmacro iff? (exp)
  `(and (compound-expression? ,exp)
		(iff-operator? (expression-operator ,exp))))

(defmacro check-iff? (exp)
  `(legal-form? IFF (= (length ,exp) 3) ,exp))

(defmacro make-iff (left-operand right-operand)
  `(make-expression *iff* (list ,left-operand ,right-operand)))


;;; *********************************************************************
;;;
;;; ***  The Modal Operator Unknown
;;;
;;; *********************************************************************

(defmacro unknown-operator? (op)
  `(member ,op *unknown-ops* :test #'eql))

(defmacro unknown? (exp)
  `(and (compound-expression? ,exp)
		(unknown-operator? (expression-operator ,exp))))

(defmacro check-unknown? (exp)
  `(legal-form? UNKNOWN (length-1? (expression-operands ,exp)) ,exp))

(defmacro make-unknown (wff)

	"Given a WFF,
     Returns a wff indicating that WFF's truth value is unknown."

	`(make-expression *unknown-op* (list ,wff)))

(defmacro wff-of-unknown (unknown-wff)

  "Given an unknown modal expresion (unknown wff)
  Returns its wff."
  
  `(first (expression-operands ,unknown-wff)))
