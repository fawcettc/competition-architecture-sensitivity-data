;;; -*- Mode:Common-Lisp; Package:mtk-utils; Base:10 -*-

;;; Copyright (c) 1999 - 2008 Brian C. Williams, MIT.

;;; This software may not be redistributed, and can only be retained
;;; and used with the explicit written consent of the author,
;;; Brian C. Williams.

;;; This software is made available as is; no guarantee is provided
;;; with respect to performance or correct behavior.
;;; This software may only be used for non-commercial, non-profit,
;;; research activities.

(in-package :mtk-utils)

;;; ***********************************************************************
;;;
;;;                      ***  Things To Do ***
;;;
;;; ***********************************************************************

;;; o Move all check-* functions to this file.
;;;   Write in the style of rmpl-expression checks, as needed.

;;; o Make these functions inline, rather than using macros.

;;; o Remove calls and code for obsolete functions, as indicated below.

;;; o Most of the following can be generated using defstructs
;;;   in which the type is an unnamed list.

;;; o Add comments.

;;; ***********************************************************************
;;;
;;;   ***  Constants for Quantification and Iteration ***
;;;
;;; ***********************************************************************

;;; Note: Used in MPL.
(defconstant *forall* 'forall 
  "A quantifier that binds to each element of a domain,
   synonymous with *every*.")

;;; Note: Used in RMPL.
(defconstant *every* 'every
  "A quantifier that binds to each element of a domain,
   synonymous with *forall*.")

;;; Note: Used in MPL and RMPL.
(defconstant *some* 'some
  "A quantifier that binds to one element of a domain.")

(defconstant *universal-quantifiers* (list *forall* *every*)
  "A quantifier that binds to each element of a domain.")

(defconstant *existential-quantifiers* (list *some*)
    "A quantifier that binds to one element of a domain.")

(defconstant *quantifier-ops*
	(append *universal-quantifiers* *existential-quantifiers*))

;;; *********************************************************************
;;;
;;;               ***  Quantification / Iteration ***
;;;
;;; *********************************************************************

(defmacro quantifier-operator? (op)
  `(member ,op *quantifier-ops* :test #'eql))

;;; To Do:
;;; o Redundant with above.  Change callers and delete.
;;;(defmacro quantification-operator? (op)
;;;  `(quantifier-operator? ,op))

(defmacro forall-operator? (op)
  `(member ,op
		   *universal-quantifiers*
		   :test #'eql))

(defmacro exists-operator? (op)
  `(member ,op
		   *existential-quantifiers*
		   :test #'eql))

;;; To Do:
;;; o Should this check the number of arguments in this predicate,
;;;   or in a separate check-* function? (unary-expression? ,expression)
(defmacro forall? (expression)

  "EXPRESSION denotes a universal
   quantification."

  `(and (compound-expression? ,expression)
		(forall-operator?
		 (expression-operator ,expression))))

;;; Synonym for forall?
(defmacro every? (expression)
  `(forall? ,expression))

;;; To Do:
;;; o Write a separate check-* function that checks the
;;;   number of arguments (unary-expression? ,expression).
(defmacro exists? (expression)

  "EXPRESSION denotes an existential
   quantification."

  `(and (compound-expression? ,expression)
		(exists-operator?
		 (expression-operator ,expression))))

;;; To Do:
;;; o Should this check the number of arguments in this predicate,
;;;   or in a separate check-iterator function? (unary-expression? ,exp)
(defmacro iterator? (exp)

  "Expression EXP denotes a universal or existential
   quantification."

  `(and (compound-expression? ,exp)
		(quantifier-operator?
		 (expression-operator ,exp))
;;;	(or (eq (first ,exp) 'some)
;;;	    (eq (first ,exp) 'every))
		))

;;; To Do:
;;; o Delete commented code when debugged.

;;; Synonym for exists?
(defmacro some? (expression)
  `(exists? ,expression)
	
;;;  `(and (compound-expression? ,expression)
;;;        (eql (expression-operator ,expression) *some*))
  )

(defmacro quantification-variable (expression)
  `(second ,expression))

(defmacro quantification-list (expression)
  `(fourth ,expression))

(defmacro quantification-wff (expression)
  `(fifth ,expression))
