;;; -*- Mode:Common-Lisp; Package:mtk-utils; Base:10 -*-

;;; Copyright (c) 1999 - 2009 Brian C. Williams, MIT.

;;; This software may not be redistributed, and can only be retained
;;; and used with the explicit written consent of the author,
;;; Brian C. Williams.

;;; This software is made available as is; no guarantee is provided
;;; with respect to performance or correct behavior.

;;; This software may only be used for non-commercial, non-profit,
;;; research activities.

(in-package :mtk-utils)

;;; ***********************************************************************
;;;
;;;          ***  Keywords Used in CCA Model Object File Format ***
;;;
;;; ***********************************************************************

(defconstant *mode-type-nominal* :nominal)
(defconstant *mode-type-failure* :fault)
(defconstant *mode-type-unknown* :unknown)
