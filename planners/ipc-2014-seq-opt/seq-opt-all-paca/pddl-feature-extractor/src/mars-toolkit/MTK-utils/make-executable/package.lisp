;;; -*- Mode:Common-Lisp; Package:cl-user; Base:10 -*-

;;; Copyright (c) 1999 - 2009 Brian C. Williams, MIT.

;;; This software may not be redistributed, and can only be retained
;;; and used with the explicit written consent of the author,
;;; Brian C. Williams.

;;; This software is made available as is; no guarantee is provided
;;; with respect to performance or correct behavior.

;;; This software may only be used for non-commercial, non-profit,
;;; research activities.

(in-package :cl-user)

(defpackage #:make-executable
  (:use #:cl #:cl-user #:asdf-config)
  (:export
   ;; from utils.lisp
   #:get-source-pathnames
   #:get-binary-pathnames
   #:get-fasl-pathnames
   #:get-all-systems

   #:ensure-directory-pathname
   #:remove-filename
   #:get-filename 
   #:copy-file-to-folder
   #:copy-folder
   #:get-relative-pathname 

   ;; from make-executable.lisp
   #:make-executable-from-asdf

   ;; from make-source-folder.lisp
   #:make-source-folder
   #:copy-file-to-source-folder
   #:default-keep-pathname
   )
)