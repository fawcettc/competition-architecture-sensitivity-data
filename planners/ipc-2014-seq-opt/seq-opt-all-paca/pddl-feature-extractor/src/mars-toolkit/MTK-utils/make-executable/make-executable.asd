;;; -*- Mode:Common-Lisp; Package:cl-user; Base:10 -*-

;;; Copyright (c) 1999 - 2012 Brian C. Williams, MIT.

;;; This software may not be redistributed, and can only be retained
;;; and used with the explicit written consent of the author,
;;; Brian C. Williams.

;;; This software is made available as is; no guarantee is provided
;;; with respect to performance or correct behavior.

;;; This software may only be used for non-commercial, non-profit,
;;; research activities.

(in-package :cl-user)

(asdf:defsystem #:make-executable
  :components ((:file "package")
			   (:file "utils" :depends-on ("package"))
			   (:file "make-executable" :depends-on ("package" "utils"))
			   (:file "make-source-folder" :depends-on ("package" "utils"))
			   (:file "test-make-executable" :depends-on ("package" "make-source-folder" "make-executable"))
			   )
  :depends-on (#:asdf-config #:cl-fad #:cl-ppcre)
)