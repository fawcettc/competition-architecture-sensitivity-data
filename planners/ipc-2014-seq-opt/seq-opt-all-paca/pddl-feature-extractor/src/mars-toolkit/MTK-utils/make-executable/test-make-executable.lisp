
(in-package :make-executable)


(defun test-copy-folder ()
  (let ((src-folder (mars-pathname "MTK-utils/abstract-data-types/"))
		(dest-folder (pathname "~/make-executable-test/")))
	(copy-folder src-folder dest-folder :verbose t :contents-only nil)
	))

(defun test-copy-folder-contents-only ()
  (let ((src-folder (mars-pathname "MTK-utils/abstract-data-types/"))
		(dest-folder (pathname "~/make-executable-test/")))
	(copy-folder src-folder dest-folder :verbose t :contents-only t)
	))