(in-package :make-executable)


(defun make-source-folder (src-pathname asdf-package &key (keep-pathname-function #'mars-toolkit-keep-pathname) (include-make-executable t) (verbose nil))
  "Creates a folder located at SRC-PATHNAME, and copies into 
   that folder all files needed by ASDF-PACKAGE.
   The function KEEP-PATHNAME-FUNCTION will be called with the 
   pathname of each file to be copied and should return the 
   portion of the pathname that will be appended to SRC-PATHNAME 
   to make the destination pathname for that file."
  (let (source-files)
	;; create a list of source files we need to include
	(setf source-files (get-source-pathnames asdf-package))
	(setf source-files (nconc source-files (mapcar #'asdf::system-source-file (get-all-systems asdf-package))))
	(when include-make-executable
	  (setf source-files (nconc source-files (get-source-pathnames 'make-executable)))
	  (setf source-files (nconc source-files (mapcar #'asdf::system-source-file (get-all-systems 'make-executable))))
	  )
	;; ensure src-pathname is a directory
	(setf src-pathname (cl-fad:pathname-as-directory src-pathname))
	;; copy files to src-pathname
	(map 'nil 
		 (lambda (source-file) 
		   (let* ((dest-file (cl-fad:merge-pathnames-as-file src-pathname (funcall keep-pathname-function source-file)))
				  (parent-pathname (remove-filename dest-file)))
			 (when verbose
			   (format t "Copying \"~a\" to \"~a\"~%" source-file dest-file))
			 (ensure-directories-exist parent-pathname)
			 (cl-fad:copy-file source-file dest-file :overwrite t)
			 ))
		 source-files)
	))

(defun copy-file-to-source-folder (file-pathname dest-dir-pathname &key (keep-pathname-function #'mars-toolkit-keep-pathname) (verbose nil))
  "Same as COPY-FILE-TO-FOLDER function, but defaults to trimming off the mars-toolkit pathname."
  (copy-file-to-folder file-pathname dest-dir-pathname :keep-pathname-function keep-pathname-function :verbose verbose))

(defun mars-toolkit-keep-pathname (pathname)
  "Returns the pathname relative to the mars-toolkit"
  (multiple-value-bind (match regs)
	  (cl-ppcre:scan-to-strings  ".*(mars-toolkit[\\\\/].*)" (namestring pathname))
	(if match
		(aref regs 0)
		pathname)
	))