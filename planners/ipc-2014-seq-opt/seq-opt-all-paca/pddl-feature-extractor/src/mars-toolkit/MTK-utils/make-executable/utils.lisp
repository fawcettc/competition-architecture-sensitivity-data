(in-package :make-executable)

;;; *************************************************************************
;;; ***   HELPER METHODS
;;; *************************************************************************

(defun get-source-pathnames (asdf-package &key (force-recompile nil))
  "Creates a list of all .lisp source files used by package
and its dependenices."
  (require :asdf)
  (asdf:load-system asdf-package :force force-recompile)
  (asdf-config::get-lisp-source-filenames asdf-package)
  )

(defun get-binary-pathnames (asdf-package &key (force-recompile nil))
  "Creates a list of all the compiled files used by package 
and its dependenices."
  (require :asdf)
  (asdf:load-system asdf-package :force force-recompile)
  (mapcar 
   (lambda (filename)
	 (format t "~a~%" (make-pathname :defaults filename :type "fasl"))
	 (asdf:apply-output-translations (make-pathname :defaults filename :type "fasl")))
   (get-source-pathnames asdf-package))
  )

(defun get-fasl-pathnames (asdf-package &key (force-recompile nil))
  "An alias for function get-binary-pathnames."
  (get-binary-pathnames asdf-package :force-recompile force-recompile))

(defparameter *visited* nil)
(defun get-all-systems (system-name &key (reset-visited t))
  "Returns all of the systems in the dependency tree of SYSTEM-NAME."
  (when reset-visited (setf *visited* nil))
  (let ((system (find-system-mod system-name)))
	(push system *visited*)
	(append
	 (mapcan #'(lambda (dep)
				 (let ((new (find-system-mod dep)))
				   (when (not (member new *visited*))
					 (copy-list (get-all-systems new :reset-visited nil)))))
			 (mapcan #'(lambda (record)
						 (destructuring-bind (op &rest systems) record
						   (declare (ignore op))
						   (loop for system in systems
							  when (typep system 'asdf::system)
							  collect system)
						   ))
					 (asdf-config::component-depends-on (make-instance 'asdf:load-op) system)))
	 (list system))))

;; Added on January 7th, 2014 by David Wang
;; Note that an identical version of this function
;; has also been placed in the 3rd-party package
;; asdf-config's asdf-config.lisp file.
(defun find-system-mod (system-name)
  "A temporary replacement for find-system, that supports
   the more advanced system dependency syntax that comes with
   the more recent releases of ASDF (i.e. ASDF 3)"
  (cond (;; simple-component-name
		 (stringp system-name)
		 (asdf:find-system system-name))
		(;; simple-component-name
		 (symbolp system-name)
		 (asdf:find-system system-name))
		(;; (:version simple-component-name version-specifier)
		 (and (listp system-name) (eql (first system-name) :version))
		 (asdf:find-system (second system-name)))
		(;; otherwise the system-name might be a system object
		 ;; or something we don't support yet.
		 :otherwise
		 (asdf:find-system system-name))
		))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; FILE AND FOLDER HANDLING UTILITIES
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; NOTE: There is a better function, called in the cl-fad package
;; called "pathname-as-directory" that provides identical functionality.
(defun ensure-directory-pathname (path)
  "A convenience method that returns a directory pathname even if
the 'path' given ends in a file-name. This is useful if the user
forgets to add a slash at the end of the path:
C:/folder1/folder2/file => #P'C:/folder1/folder2/file/'
C:/folder1/folder2/      => #P'C:/folder1/folder2/'"
  ;; make the path absolute
  (setf path (merge-pathnames path cl-user::*default-pathname-defaults*))
  ;; make sure the path ends in a slash
  (if (null (file-namestring path))
	  ;; there is no filename - great!
	  path
	  ;; there is a filename...
	  (merge-pathnames 
	   (concatenate 'string 
					(pathname-name path)
					(when (pathname-type path)	  
					  (concatenate 'string "." (pathname-type path)))
					"/")
	   (remove-filename path)))
  )

(defun remove-filename (path)
  "Given a pathname PATH, returns everthing but the filename and type."
  (make-pathname :host (pathname-host path)
				 :device (pathname-device path)
				 :directory (pathname-directory path))
)

(defun get-filename (path)
  "Given a pathname PATH, returns only the filename.type"
  (make-pathname :name (pathname-name path)
				 :type (pathname-type path))
  )

(defun copy-file-to-folder (file-pathname dest-dir-pathname &key (keep-pathname-function #'default-keep-pathname) (verbose nil))
  "Creates a folder located at DEST-DIR-PATHNAME, and copies into 
   that folder the file identified by FILE-PATHNAME.
   The function KEEP-PATHNAME-FUNCTION will be called with 
   argument FILE-PATHNAME and should return the 
   portion of the pathname that will be appended to DEST-DIR-PATHNAME 
   to make the destination pathname for that file."
  ;; ensure dest-dir-pathname is a directory
  (setf dest-dir-pathname (cl-fad:pathname-as-directory dest-dir-pathname))
  ;; copy files to dest-dir-pathname
  (let* ((dest-file (cl-fad:merge-pathnames-as-file dest-dir-pathname (funcall keep-pathname-function file-pathname)))
		 (parent-pathname (remove-filename dest-file)))
	(when verbose
	  (format t "Copying \"~a\" to \"~a\"~%" file-pathname dest-file))
	(ensure-directories-exist parent-pathname)
	(cl-fad:copy-file file-pathname dest-file :overwrite t)
	))


;; (defun default-keep-pathname (pathname)
;;   (multiple-value-bind (match regs)
;; 	  (cl-ppcre:scan-to-strings  ".*[\\\\/](.+)$" (namestring pathname))
;; 	(if match
;; 		(aref regs 0)
;; 		pathname)
;; 	))

(defun default-keep-pathname (pathname)
  (get-filename pathname))

(defun copy-folder (folder-pathname dest-dir-pathname &key  (verbose nil) (follow-symlinks nil) (contents-only nil))
  "Creates a folder located at DEST-DIR-PATHNAME, and copies into 
   that folder the file identified by FOLDER-PATHNAME."
  ;; ensure dest-dir-pathname is a directory
  (setf dest-dir-pathname (cl-fad:pathname-as-directory dest-dir-pathname))
  ;; ensure the folder-pathname is a directory
  (setf folder-pathname (cl-fad:pathname-as-directory folder-pathname))
  ;; ensure the destination directory exists
  (ensure-directories-exist dest-dir-pathname)
  (let ((parent-folder-pathname (cl-fad:pathname-parent-directory folder-pathname)))
	;; walk directory and copy files
	(cl-fad:walk-directory folder-pathname 
						   #'(lambda (file-pathname)
							   (let* ((relative-folder-name 
									   (get-relative-pathname (remove-filename file-pathname)
															  (if contents-only folder-pathname parent-folder-pathname)))
									  (curr-dest-pathname (merge-pathnames relative-folder-name dest-dir-pathname))
									  )
								 ;; make sure the destination directory exists
								 (ensure-directories-exist curr-dest-pathname)
								 (unless (cl-fad:directory-pathname-p file-pathname)
								   (copy-file-to-folder file-pathname curr-dest-pathname 
														:keep-pathname-function #'default-keep-pathname 
														:verbose verbose))
								 ))
						   :directories :breadth-first
						   :follow-symlinks follow-symlinks))
  )

(defun get-relative-pathname (pathname working-pathname)
  "Returns PATHNAME as a pathname relative to WORKING-PATHNAME.
   In this implementation, WORKING-PATHNAME must be a parent
   path of PATHNAME."
  ;; ensure the working-pathname is a directory
  (setf working-pathname (cl-fad:pathname-as-directory working-pathname))
  ;; get the relative pathname by string operations
  (subseq (namestring pathname)
		  (length (namestring working-pathname)))
  )