;; $Id: deliver.cl,v 1.4 2004/03/21 15:48:10 layer Exp $

;; This file is a modified 'example' file found in the ACL installation directory
;; "[acl]/examples/testapp". This file should be called by GNU make, 
;; which executes "makefile" which in turn uses this file to create a
;; temporary build script for allegro.

;; The following variables are set by the execution of the makefile:
;; *debug-testapp* - t if the application should have debugging turned on (o/w nil).
;; *console-app* - t if a console application should be created (o/w nil).
;; *current-directory* - the directory in which the makefile resides.

(in-package :make-executable)

(defparameter *GENERATE-APPLICATION-KEYWORDS* '(:case-mode :dst :include-clim :include-common-graphics :include-compiler :include-composer :include-debugger :include-devel-env :include-ide :include-tpl :include-xcw :init-file-names :load-local-names-info :load-source-file-info :load-xref-info :pll-file :print-startup-message :read-init-files :record-source-file-info :record-xref-info :require-search-list :restart-app-function :restart-init-function :aclmalloc-heap-size :aclmalloc-heap-start :autoload-warning :build-debug :build-input :build-output :build-executable :c-heap-size :c-heap-start :close-oldspace :copy-shared-libraries :debug-on-error :discard-arglists :discard-compiler :discard-local-name-info :discard-source-file-info :discard-xref-info :dribble-file :exit-after-image-build :generate-fonts ignore-command-line-arguments :include-* :include-debugger :internal-debug :lisp-heap-size :lisp-heap-start :lisp-files :newspace :oldspace :opt-debug :opt-safety :opt-space :opt-speed :opt-compilation-speed :post-load-form :pre-load-form :preserve-documentation-strings :restart-app-function :restart-init-function :runtime :server-name :show-window :splash-from-file :temporary-directory :us-government :user-shared-libraries :verbose :wait)
  "This is a list of all of the keywords accepted by the AllegroCL function generate-application.")

(defmacro with-break-on-signals (new-signal &rest code)
  "Temporarily sets the excl::*break-on-signals* global variable"
  `(let ((original-signal excl::*break-on-signals*))
	 ;; overwrite the original value
	 (setf excl::*break-on-signals* ,new-signal)
	 ,@code
	 ;; rewrite the original value
	 (setf excl::*break-on-signals* original-signal)
	 )
  )

(defun make-executable-from-asdf (exec-name dist-pathname asdf-package main-function 
								  &rest rest
								  &key (extra-input-files nil)
								  (console-app #+mswindows t #-mswindows nil) 
								  (debug-app nil) (debug-build nil) (allow-existing-directory t)
								  (init-file-pathname nil)
								  &allow-other-keys)
  "Creates a distribution directory that contains an executable and an executable image of the asdf-package.
executable-name - the name of your executable.
asdf-package    - the symbol-name of the package containing the main-function.
dist-pathname   - the pathname to the folder in which you want the distribution installed.
main-function   - the function that will get executed.
extra-input-files - a list of additional files that will be included in the running image.
console-app      - t if the application should work on the command-line. nil if the application starts its own window. (default t)
debug-app        - t if the application should be made with debug capabilities. nil o/w (default nil)
debug-build      - t if the the input and output.lisp scripts used to create the distribution directory 
                    should be saved in the distribution directory AND a trace be returned by the image building process
allow-existing-directory - t if the distribution directory should be saved and new files overwrite old files. 
                           nil if the distribution directory should be deleted. (default t)
init-file-pathname - allows the user to specify the clinit file to be included.

Additional keyword arguments can be provided, and will be passed on to the AllegroCL function generate-application."
  #-mswindows
  (declare (ignore console-app))
  
  (let* (;; clean up executable name - remove the ".exe" if it exists
		 (exec-name (pathname-name (pathname exec-name)))
		 ;; clean up the distribution-pathname - make it a pathname object with defaults from *default-pathname-defaults*
		 (dist-pathname (cl-fad:pathname-as-directory dist-pathname))
		 ;; build a bunch of convenience names with absolute
		 (buildin-pathname (merge-pathnames "build.in" dist-pathname))
		 (buildout-pathname (merge-pathnames "build.out" dist-pathname))
		 (input-files (append (when debug-app '(:inspect :trace))
							  '(#-mswindows :process :list2 :seq2)
							  '(:defsys)
							  (when init-file-pathname `(,(pathname init-file-pathname)))
							  (mapcar #'asdf::system-source-file (get-all-systems asdf-package))
							  (make-executable::get-source-pathnames asdf-package)
							  extra-input-files))
		 (default-keyword-args '())
		 (keyword-args '()))
	
	;; delete any previous outputs
	(when (not allow-existing-directory)
	  (require :osi)
	  (excl:delete-directory-and-files dist-pathname :if-does-not-exist :ignore))
	(when (not debug-build)
	  (handler-case
		  (user::delete-file buildin-pathname)
		(file-error () nil))
	  (handler-case
		  (user::delete-file buildout-pathname)
		(file-error () nil)))
	
	; build a list of all the keyword arguments we will use by default
	(setf default-keyword-args 
		  (list :restart-init-function main-function
				#-mswindows
				:application-administration
				#-mswindows ;; Quiet startup (See below for Windows version of this.)
				'(:resource-command-line "-Q")
				:read-init-files nil			; don't read ACL init files
				;;:init-file-names '(".clinit.cl" "clinit.cl")
				
				:print-startup-message nil	; don't print ACL startup messages
				:ignore-command-line-arguments t ; ignore ACL (not app) cmd line options
				:suppress-allegro-cl-banner t
				
				;; Adds the compiler, required for loading asdf.
				:include-compiler t
				:discard-compiler t
				
				;; Change the following to `t', if:
				;; - the program (vs. data) is large
				;; - you'll have lots of users of the app (so sharing the code is important)
				:purify nil
				
				;; don't give autoload warning, but you should still be aware that
				;; autoloads.out will contain a list of autoloadable names.
				:autoload-warning nil
				
				:include-debugger debug-app
				:include-tpl debug-app
				:include-ide nil
				:include-devel-env nil
				;;:include-compiler nil
				:discard-arglists (not debug-app)
				:discard-local-name-info (not debug-app)
				:discard-source-file-info (not debug-app)
				:discard-xref-info (not debug-app)
				  
				;; for debugging:
				:verbose debug-build
				:build-debug debug-build
				:build-input (when debug-build buildin-pathname)
				:build-output (when debug-build buildout-pathname)
				
				;; for file handling
				:allow-existing-directory allow-existing-directory
				
				:runtime :standard))
	
	;; traverse the keyword list provided and replace any keywords in the default list
	(let (;; get the keys provided in the call to make-executable
		  (rest-keys (loop for key in rest by #'cddr collect key))
			;; get the values associated with those keys
		  (rest-vals (loop for val in (rest rest) by #'cddr collect val)))
		;;;; only keep keyword arguments from rest that are recognized
		;;;; by the generate-application function.
	  (loop for key in rest-keys
		 for val in rest-vals do
		   (when (member key  *GENERATE-APPLICATION-KEYWORDS*)
			 (setf keyword-args (append keyword-args (list key val)))))
		;;;; if a keyword argument was not specified in rest,
        ;;;; add all the keyword arguments from the defaults. 
	  (loop for key in default-keyword-args by #'cddr
		 for val in (rest default-keyword-args) by #'cddr  do
		   (unless (member key rest-keys)
			 (setf keyword-args (append keyword-args (list key val)))))
	  )
	
	(format t "Calling generate-application with:~%")
	(format t "  app-name: ~s~%" exec-name)
	(format t "  dest-dir: ~s~%" dist-pathname)
	(format t "  input-files: ~s~%" input-files)
	(format t "  keywords: ~s~%" keyword-args)
	
	;; generate the new application distribution
	(apply #'cl-user::generate-application
		   exec-name
		   dist-pathname
		   input-files
		   keyword-args)
	
	;; do windows specific stuff to create a console application
	#+mswindows
	(let ((exec-pathname (merge-pathnames (concatenate 'string exec-name ".exe") dist-pathname)))
	  ;; Quiet startup:
	  (when (not console-app)
		  (user::run-shell-command
		   ;; Replace +cm with +cn to see the window, but have it not be in the
		   ;; foreground.
		   (format nil "\"~a\" -o \"~a\" +B +M +cm -Q"
				   (translate-logical-pathname "sys:bin;setcmd.exe")
				   exec-pathname)
		   :show-window :hide))
	  ;; add to features
	  (when console-app
		(push :windows-console-app *features*))
		;; Allow the executable to read and write to std streams.
	  (when console-app
		(delete-file exec-pathname)
		(sys:copy-file "sys:buildi.exe" exec-pathname)))
	
	))



