;;;; Copyright (c) 1999 - 2010 Brian C. Williams, MIT.

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

(in-package :CL-USER)

(asdf:defsystem #:mtk-utils
  :name "MTK-utils"
  :version "0.1"
  :description "MERS Toolkit: Utilities"
  :author "Brian Williams"
  :maintainer "MIT MERS Group"
  :serial t
  :components ((:file "package")

			   (:file "utils")
			   (:file "reversible-stream")
			   (:file "ht-iteration")	  
			   (:file "debug-support")
	 
	
			   (:file "bindings")
			   (:file "cca-model-keywords")
			   (:file "probability")
	 	 
			   (:file "test-routines")
			   (:file "manipulating-expressions")
			   (:file "assignment-expressions")
			   (:file "logical-expressions")
			   (:file "numeric-expressions")
			   (:file "temporal-logic-expressions")
			   (:file "quantifier-expressions")
			   (:file "choice-expressions")
			   (:file "let-expressions")
			   (:file "guard-expressions")
			   (:file "rmpl-expressions")
			   (:file "string-utils")

			   (:file "compare"))

  :depends-on (#:parse-number)

)
