;;; -*- Mode:Common-Lisp; Package:mtk-utils; Base:10 -*-

(in-package :mtk-utils)

;;;(eval-when (compile load eval)
;;;  (export '(do-ht)
;;;		  :MTK-UTILS)
;;;  )

(defmacro do-ht ((key value hash-table) &body body)
  
  "Loops through the elements of a hash table, similar to dolist.  
   For each entry of HASH-TABLE, KEY is bound to the key and VALUE 
   to the value of the entry, and BODY is executed with this binding."

  (let ((function (gensym))
		(found (gensym)))
	`(with-hash-table-iterator (,function ,hash-table)
	   (loop 
		 (multiple-value-bind
			 (,found ,key ,value)
			 (,function)
		   ,@(extract-declare-clause body)
		   (unless ,found (return))
		   ,@(strip-declare-clause body))))))

;;; ***************************************************************************
;;;
;;;                 Support routines
;;;
;;; ***************************************************************************

(defun extract-declare-clause (body)
  
  "Returns a list containing the declaration clause of BODY, if any."
  
  (when (and (listp body)
	     (listp (first body))
	     (eql (caar body) 'declare))
    (list (first body))))

(defun strip-declare-clause (body)
  
  "Removes any prefix declaration clause in BODY, and returns 
   the remainder of body."
  
  (if (and (listp body)
	   (listp (first body))
	   (eql (caar body) 'declare))
      (rest body)
    body))


