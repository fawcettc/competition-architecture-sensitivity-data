;;; -*- Mode:Common-Lisp; Package:mtk-utils; Base:10 -*-

;;; Copyright (c) 1999 - 2011 Andreas Hofmann and Brian C. Williams, MIT.

;;; This software may not be redistributed, and can only be retained
;;; and used with the explicit written consent of the authors,
;;; Brian C. Williams and Andreas Hofmann.

;;; This software is made available as is; no guarantee is provided
;;; with respect to performance or correct behavior.
;;; This software may only be used for non-commercial, non-profit,
;;; research activities.

(in-package :mtk-utils)

;;; ***********************************************************************
;;;
;;;                      ***  Things To Do ***
;;;
;;; ***********************************************************************

;;; o Integrate check-* functions here with check-wff-P.

;;; o Make these functions inline, rather than using macros.



;;; ***********************************************************************
;;;
;;; ***  Constants for Defined Operators and Values ***
;;;
;;; ***********************************************************************



;;; *** Primitive arithmetic connectives

(defconstant *+* '+
  "Denotes addition.")

(defconstant *-* '-
  "Denotes subtraction.")

(defconstant *mult* '*
  "Denotes multiplication.")

(defconstant */* '/
  "Denotes multiplication.")

(defconstant *continuous-equal* 'continuous-equal
  "Denotes equality.")

(defconstant *continuous-<* 'continuous-<
  "Denotes a less than or equal to inequality.")

(defconstant *continuous->* 'continuous->
  "Denotes a less than or equal to inequality.")

(defconstant *primitive-arithmetic-ops* (list *+* *-* *mult* */*))



;;; *** Arithmetic Operator Argument Count 

(defconstant *unary-arithmetic-ops* (list *+* *-*)
  "Operators that take on exactly one argument.")

(defconstant *binary-arithmetic-ops* (list *+* *-* *mult* */*)
  "Operators that take on exactly two arguments.")

(defconstant *nary-arithmetic-ops* (list *+* *-* *mult*)
  "Operators that can take on zero or more arguments.")

(defconstant *equality-arithmetic-ops* (list *continuous-equal*)
  "Operators that require equality of arguments.")

(defconstant *inequality-arithmetic-ops* (list *continuous-<* *continuous->*)
  "Operators that imply inequality of arguments.")

;;; *********************************************************************
;;;
;;; *** Compound Numeric Expressions ***
;;;
;;; *********************************************************************

(defmacro compound-numeric-expression? (expression)
  
  "Returns true iff expression is a compound
   expression with a valid logical operator."

  `(and (compound-expression? ,expression)
		(numeric-operator? (expression-operator ,expression))))

(defmacro numeric-equality-expression? (expression)
  
  "Returns true iff expression is a compound
   expression with a valid logical operator."

  `(and (compound-expression? ,expression)
		(numeric-equality-operator? (expression-operator ,expression))))

(defmacro numeric-inequality-expression? (expression)
  
  "Returns true iff expression is a compound
   expression with a valid logical operator."

  `(and (compound-expression? ,expression)
		(numeric-inequality-operator? (expression-operator ,expression))))

(defmacro numeric-operator? (op)
  `(member ,op *primitive-arithmetic-ops* :test #'eql))

(defmacro unary-numeric-operator? (op)
  `(member ,op *unary-arithmetic-ops* :test #'eql))

(defmacro binary-numeric-operator? (op)
  `(member ,op *binary-arithmetic-ops* :test #'eql))

(defmacro nary-numeric-operator? (op)
  `(member ,op *nary-arithmetic-ops* :test #'eql))

; (defmacro numeric-equality-operator? (op)
;  `(member ,op *equality-arithmetic-ops* :test #'eql))

; (defmacro numeric-equality-operator? (op)
; `(eql ,op *continuous-equal*))

(defmacro numeric-equality-operator? (op)
  `(equal (symbol-name ,op) (symbol-name *continuous-equal*)))
;; Figure out how to do this better!!!

(defmacro numeric-inequality-operator? (op)
  `(member ,op *inequality-arithmetic-ops* :test #'eql))

(defmacro numeric-relation-operator? (op)
  `(or (numeric-equality-operator? ,op)
	   (numeric-inequality-operator? ,op)))