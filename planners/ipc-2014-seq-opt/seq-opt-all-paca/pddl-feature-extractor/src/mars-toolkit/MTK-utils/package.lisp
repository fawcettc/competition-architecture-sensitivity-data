;;; -*- Mode:Common-Lisp; Package:cl-user; Base:10 -*-

;;; Copyright (c) 1999 - 2010 Brian C. Williams, MIT.

;;; This software may not be redistributed, and can only be retained
;;; and used with the explicit written consent of the author,
;;; Brian C. Williams.

;;; This software is made available as is; no guarantee is provided
;;; with respect to performance or correct behavior.

;;; This software may only be used for non-commercial, non-profit,
;;; research activities.

(in-package :cl-user)

;;; The following 'requires' ensures that the Franz Lisp test 
;;; infrastructure is loaded.
(require :tester)

(defpackage #:MTK-UTILS 
  (:use #:cl)
  (:nicknames "MTU")
  (:export
   ;; From compare.lisp
   "EXTRACT-PROPOSITION-DESCRIPTION"	;support method to be
										; supplied by all constraint 
										; systems using the following
										; comparison functions.

   "LEXICOGRAPHIC-<"
			 
   "EQUAL-SETS"
   "EQUAL-MODELS"
   "SET-CONTAINS-MODEL"
   "EQUAL-MODEL-SETS"
   "EQUAL-MODEL-SEQUENCES"
   "EQUAL-MODEL-SEQUENCES-OF-SETS"
   "GET-TEST-FN-FOR-SEQ-OF-SETS"
   "EQUAL-CONFLICTS"
   "EQUAL-CONFLICT"
   "EQUAL-ASSIGNMENTS"
   "CONTAINS-ASSIGNMENT?"
			
   "EQUAL-CONSISTENT?-CONFLICT-RESULT"
   "EQUAL-MODEL-CONFLICT-RESULT"
			 
   "EQUAL-EXCLUSION"
   "EQUAL-RELAXATION"
   "EQUAL-EXPLANATION"
   "EQUAL-EXPLANATIONS"
   "EQUAL-EXCLUSION-EXPLANATIONS"
   "EQUAL-CONSTRAINT-CONFLICTS"
   "EQUAL-TPN-EXPLANATION"
   "EQUAL-TPN-EXPLANATIONS"
   "EQUAL-EXPLANATIONS-AND-CONFLICTS"
   "EQUAL-TPN-EXPLANATIONS-AND-CONFLICTS"
			 
   ;; From debug-support.lisp
   "*DEBUG*"
   "DEBUG-PROGN"

   "DEBUG-PRINT-PROGN"
   "DEBUG-PRINT"

   "DEBUG-PRINT-ON"
   "DEBUG-PRINT-OFF"

   "DECLARE-DEBUG-PRINT-SYSTEM!"
   "DEBUG-PRINT-SYSTEMS"
   "DEBUG-PROGN-ON-FOR?"
   "DEBUG-PRINT-ON-FOR?"
   "DEBUG-PROGN-FOR"
   "DEBUG-PRINT-PROGN-FOR"
   "DEBUG-PRINT-FOR"
   "DEBUG-PROGN-ON-FOR"
   "DEBUG-PRINT-ON-FOR"
   "DEBUG-PROGN-OFF-FOR"
   "DEBUG-PRINT-OFF-FOR"
			 
   "DEBUG-LEVEL"
   "SET-DEBUG-LEVEL"

   #:format-time
   #:format-time-reset
   #:format-warning
   #:format-error
   #:colorize
   #:*colorize-output-mode*
   #:colorize-set-none
   #:colorize-set-terminal
			 
   ;; From ht-iteration.lisp
   "DO-HT"
			 
   ;; From bindings.lisp
   "*EMPTY-BINDINGS*"
   "MAKE-BINDING"
   "MAKE-BINDING-LIST"
   "BINDING-VARIABLE"
   "BINDING-VALUE"
   "EMPTY-BINDING?"
   "ADD-BINDINGS"
   "GET-BINDING"
   "INSTANTIATE-BINDINGS"
			
   ;; From manipulating-expressions.lisp Set 1

   "LEGAL-FORM?"
			 
   "COMPOUND-EXPRESSION?"
   "MAKE-EXPRESSION"
   "EXPRESSION-OPERATOR"
   "EXPRESSION-OPERANDS"
			
   "UNARY-EXPRESSION?"
   "MAKE-UNARY-EXPRESSION"
   "UNARY-OPERAND"
			
   "BINARY-EXPRESSION?"
   "MAKE-BINARY-EXPRESSION"
   "LEFT-OPERAND"
   "RIGHT-OPERAND"

   ;; From assignment-expressions.lisp

   ;; Any variable assignment
   "*NO-VALUE*"
   "NO-VALUE?"

   ;;   Assignment operators
   "*=*" "="
   "*ASSIGNMENT-OPS*"

   ;;   Equivalence
   "*EQUALITY-RELATIONS*"
   "EQUALITY-RELATION?"
			
   ;;   Assignments
   "ASSIGNMENT-OPERATOR?"
   "ASSIGNMENT?"
   "EQUAL-ASSIGNMENTS?"
   "EQUAL-ASSIGNMENT-VALUES?"
   "MAKE-ASSIGNMENT"
   "EXTEND-ASSIGNMENTS"

   "ASSIGNMENT-VARIABLE"
   "ASSIGNMENT-VALUE"
   "ASSIGNMENT-EXPRESSION?"				;Old remove callers

   "ASSIGN-VARIABLE"					;Old remove callers
   "ASSIGN-VALUE"						;Old remove callers
	    
   "MAKE-VARIABLE-TERM"
   "VARIABLE-TERM-RELATIVE-VARIABLE-NAME"
   "VARIABLE-TERM-INSTANCE-NAME"

   ;;   Implicit assignments 
   ;;   support MPL legacy format
   "IMPLICIT-ASSIGNMENT?"
   "MAKE-IMPLICIT-ASSIGNMENT"
   "IMPLICIT-ASSIGNMENT-VARIABLE"
   "IMPLICIT-ASSIGNMENT-VALUE"
   "MAP-IMPLICIT-TO-EXPLICIT-ASSIGNMENT"

   ;; From logical-expressions.lisp

   ;;   Truth assignments
   "*TRUE*" "TRUE"
   "*FALSE*" "FALSE"
   "*TRUTH-VALUES*"
   "*NONE*" "NONE"
   "*IMPLIES*" "IMPLIES"

   "TRUTH-VALUE?"

   "TRUE-VALUE?"  
   "CONTAINS-TRUE-VALUE?"
;;;			 "contains"    ; Looks like a bug.  Delete if systems work.
			 
   "FALSE-VALUE?"
   "CONTAINS-FALSE-VALUE?"

   "UNKNOWN-VALUE?"
   "CONTAINS-UNKNOWN-VALUE?"

   "OPPOSITE-TRUTH"
   "RANDOM-TRUTH"

   ;;   Propositions

   "EQUAL-PROPOSITIONS?"
   "PROPOSITION-LESS?"
			 
   ;;   Literals
			 			 
   "NEGATE-LITERAL"
   "EQUAL-LITERALS"
   "PROPOSITION-OF-LITERAL"
			 
   "EQUAL-LITERALS?"
   "LITERAL-LESS?"
   "ORDER-LITERALS"
			 
   "FIND-LITERAL-WITH-PROPOSITION"
			 
   "ASSIGNMENT-LITERAL?"

   "STATE-LITERAL-ASSIGNMENT"			;Old remove callers (e.g. in cca-model)
			 
   ;;   Logical operators

   "*LOGICAL-OPS*"
   "*DERIVED-LOGICAL-OPS*"
   "*PRIMITIVE-LOGICAL-OPS*"

   "LOGICAL-OPERATOR?"
   "PRIMITIVE-LOGICAL-OPERATOR?"
   "DERIVED-LOGICAL-OPERATOR?"

   "UNARY-LOGICAL-OPERATOR?"
   "BINARY-LOGICAL-OPERATOR?"
   "NARY-LOGICAL-OPERATOR?"

   "COMPOUND-LOGICAL-EXPRESSION?"
   "TAUTOLOGY?"
   "UNSATISFIABLE?"

   "*AND*" "AND"
   "*OR*" "OR"
   "*NOT*" "NOT"
   "*NEGATION-OPS*"

   "*AND-OR*"
	    
   "*NOT*" "NOT"
   "NEGATION-OPERATOR?"
   "NEGATION?"
   "CHECK-NOT?"
   "MAKE-NEGATION"
   "NEGAND"

   "*OR*" "OR"
   "DISJUNCTION-OPERATOR?"
   "DISJUNCTION?"
   "CHECK-OR?"
   "MAKE-DISJUNCTION"
   "DISJUNCTS"
   "STRIP-DISJUNCTION"
   "MAKE-SIMPLIFIED-DISJUNCTION"
			 
   "IMPLICIT-DISJUNCTION-DISJUNCTS"
			 
   "*AND* AND"
   "CONJUNCTION-OPERATOR?"
   "CONJUNCTION?"
   "CHECK-AND?"
   "MAKE-CONJUNCTION"
   "CONJUNCTS"
   "MAKE-SIMPLIFIED-CONJUNCTION"
			 
   "IMPLICIT-CONJUNCTION-CONJUNCTS"
			 
   ;;   Derived logical operators
			 
   "*IMPLIES*" "IMPLIES"
   "IMPLIES-OPERATOR?"
   "IMPLICATION?"
   "CHECK-IMPLIES?"
   "MAKE-IMPLICATION"
   "ANTECEDENT"
   "CONSEQUENT"

   "IMPLICATION-ANTECEDENT"				;Old remove callers
   "IMPLICATION-CONSEQUENT"				;Old remove callers

   "*IFF*" "IFF"
   "IFF-OPERATOR?"
   "IFF?"
   "CHECK-IFF?"
   "MAKE-IFF"
			 
   ;; Modal operator UNKNOWN
			 
   "*UNKNOWN-OP*" "UNKNOWN"
   "UNKNOWN-OPERATOR?"
   "UNKNOWN?"
   "CHECK-NOT?"
   "MAKE-UNKNOWN"
   "WFF-OF-UNKNOWN"

   ;; From numeric-expressions.lisp
   "*+*"
   "*-*"
   "*MULT*"
   "*/*"	
   "*CONTINUOUS-EQUAL*"
   "CONTINUOUS-EQUAL"
   "*CONTINUOUS-<*"
   "CONTINUOUS-<"
   "CONTINUOUS->"
   
   "*PRIMITIVE-ARITHMETIC-OPS*"
   "*UNARY-ARITHMETIC-OPS*"
   "*BINARY-ARITHMETIC-OPS*"
   "*NARY-ARITHMETIC-OPS*"
   "*EQUALITY-ARITHMETIC-OPS*"
   "*INEQUALITY-ARITHMETIC-OPS*"

   "NUMERIC-OPERATOR?"
   "UNARY-NUMERIC-OPERATOR?"
   "BINARY-NUMERIC-OPERATOR?"
   "NARY-NUMERIC-OPERATOR?"
   "NUMERIC-EQUALITY-OPERATOR?"
   "NUMERIC-INEQUALITY-OPERATOR?"
   "COMPOUND-NUMERIC-EXPRESSION?"
   "NUMERIC-EQUALITY-EXPRESSION?"
   "NUMERIC-INEQUALITY-EXPRESSION?"
   "NUMERIC-RELATION-OPERATOR?"
   
   ;; From temporal-logic-expressions.lisp
   "*NEXT*"
   "NEXT-OPERATOR?"
   "NEXT?"
   "CHECK-NEXT?"
   "MAKE-NEXT"
   "NEXT-EXPRESSION-OPERAND"

   "NEXT-EXPRESSION?"					; Old remove callers

   ;; From quantifier-expressions.lisp
   "*FORALL*" "FORALL"
   "*EVERY*" "EVERY"
   "*SOME*" "SOME"
			 
   "*UNIVERSAL-QUANTIFIERS*"
   "*EXISTENTIAL-QUANTIFIERS*"
			 
   "*QUANTIFIER-OPS*"

   "FORALL-OPERATOR?"
   "EXISTS-OPERATOR?"
   "QUANTIFICATION-OPERATOR?"
			 
   "ITERATOR?"
			 
   "FORALL?"
   "EVERY?"			 

   "EXISTS?"
   "SOME?"

   "QUANTIFICATION-VARIABLE"
   "QUANTIFICATION-LIST"
   "QUANTIFICATION-WFF"

   ;; From choice-expressions.lisp

   "NAMED-CHOICE-OPERATOR?"
   "UNNAMED-CHOICE-OPERATOR?"
   "CHOICE-OPERATOR?"
   "QUANTIFIER-OPERATOR?"
			
   "*NAMED-CHOOSE*" "NAMED-CHOOSE"
   "NAMED-CHOICE?"
   "MAKE-NAMED-CHOICE"
   "NAMED-CHOICE-NAME"
   "NAMED-CHOICE-CHOICES"
			
   "*CHOOSE*" "CHOOSE"
   "CHOICE?"
   "MAKE-CHOICE"
   "CHOICE-CHOICES"

   "*TYPE-CHOOSE*" "TYPE-CHOOSE"
   "TYPE-CHOICE?"
   "MAKE-TYPE-CHOICE"
   "TYPE-CHOICE-CHOICES"
			
   "*UNNAMED-CHOICE-OPS*"
   "UNNAMED-CHOICE-CHOICES"

   "*CHOICE-OPS*"

   ;; From let-expressions.lisp
   ;;   (Let substitution operator)
   "*LET*" "LET"
   "LET?"
			 
   ;;   Support for MPL LET
   "LET-VARIABLE"
   "LET-FORM"
   "LET-WFF"

   ;;   Support for RMPL LET
   "LET-BINDINGS"
   "LET-BODY"
   "LET-VARIABLES"
   "LET-VALUES"
   "LET-BINDING-VARIABLE"
   "LET-BINDING-VALUE"

   ;; From guard-expressions.lisp
   "*ALWAYS-ACTIVE*"
   "ALWAYS-ACTIVE?"
   "CONTAINS-ALWAYS-ACTIVE?"

   ;; From rmpl-expressions.lisp
   ;;   RMPL derived logical operators

   "*WHEN*" "WHEN"
   "WHEN-OPERATOR?"
   "WHEN?"
   "CHECK-WHEN?"
   "MAKE-WHEN"
   "WHEN-CONDITION"
   "WHEN-CLAUSES"

   "*UNLESS*" "UNLESS"
   "UNLESS-OPERATOR?"
   "UNLESS?"
   "CHECK-UNLESS?"
   "MAKE-UNLESS"
   "UNLESS-CONDITION"
   "UNLESS-CLAUSES"

   "*IF*" "IF"
   "IF-OPERATOR?"
   "IF?"
   "CHECK-IF?"
   "MAKE-IF"
   "IF-CONDITION"
   "IF-TRUE-CLAUSE"
   "IF-FALSE-CLAUSE"

   "*CONDITIONAL*" "COND"
   "CONDITIONALS-OPERATOR?"
   "CONDITIONAL?"
   "MAKE-CONDITIONAL"
   "CONDITIONAL-CLAUSES"
   "CONDITIONAL-CLAUSE-PREDICATE"
   "CONDITIONAL-CLAUSE-ACTIONS"
   "CONDITIONAL-ELSE-CLAUSE?"

   ;;   RMPL Combinators
   "*RMPL-COMBINATORS*"
   "RMPL-COMBINATOR?"
   "RMPL-COMBINATOR-EXPRESSION?"
			  
   ;;     Primitive
   "*NEXT-WHEN*" "NEXT-WHEN"
   "*NEXT-UNLESS-ENTAILED*" "NEXT-UNLESS-ENTAILED"
   "*ALWAYS*" "ALWAYS"
   "*PARALLEL*" "PARALLEL"
   "*CHOOSE-PROBABILITY*" "CHOOSE-PROBABILITY"
   "*CHOOSE-REWARD*" "CHOOSE-REWARD"
			  
   ;;     Derived
   "*DO-WATCHING*" "DO-WATCHING"
   "*SEQUENCE*" "SEQUENCE"
   "*IF-ELSE-NEXT*" "IF-ELSE-NEXT"
   "*EVERY*" "EVERY"

   ;; Primitives
   "NEXT-WHEN-OPERATOR?"
   "NEXT-WHEN?"
   "CHECK-NEXT-WHEN?"
   "MAKE-NEXT-WHEN"
   "NEXT-WHEN-CONDITION"
   "NEXT-WHEN-FORMS"
			  
   "NEXT-UNLESS-ENTAILED-OPERATOR?"
   "NEXT-UNLESS-ENTAILED?"
   "CHECK-NEXT-UNLESS-ENTAILED?"
   "MAKE-NEXT-UNLESS-ENTAILED"
   "NEXT-UNLESS-ENTAILED-CONDITION"
   "NEXT-UNLESS-ENTAILED-FORMS"
			  
   "ALWAYS-OPERATOR?"
   "ALWAYS?"
   "CHECK-ALWAYS?"
   "MAKE-ALWAYS"
   "ALWAYS-FORMS"
			  
   "PARALLEL-OPERATOR?"
   "PARALLEL?"
   "CHECK-PARALLEL?"
   "MAKE-PARALLEL"
   "PARALLEL-FORMS"
			  
   "CHOOSE-PROBABILITY-OPERATOR?"
   "CHOOSE-PROBABILITY?"
   "CHECK-CHOOSE-PROBABILITY?"
   "MAKE-CHOOSE-PROBABILITY"
   "CHOOSE-PROBABILITY-CHOICES"
   "MAKE-PROBABILISTIC-CHOICE"
   "PROBABILISTIC-CHOICE-PROBABILITY"
   "PROBABILISTIC-CHOICE-FORMS"
			  
   "CHOOSE-REWARD-OPERATOR?"
   "CHOOSE-REWARD?"
   "CHECK-CHOOSE-REWARD?"
   "MAKE-CHOOSE-REWARD"
   "MAKE-CHOOSE-REWARD-CHOICE"
   "CHOOSE-REWARD-CHOICES"
   "REWARD-CHOICE-REWARD"
   "REWARD-CHOICE-FORMS"
			  
   "DO-WATCHING-OPERATOR?"
   "DO-WATCHING?"
   "CHECK-DO-WATCHING?"
   "MAKE-DO-WATCHING"
   "DO-WATCHING-CONDITION"
   "DO-WATCHING-FORMS"
			  
   "SEQUENCE-OPERATOR?"
   "SEQUENCE?"
   "CHECK-SEQUENCE?"
   "MAKE-RMPL-SEQUENCE"
   "SEQUENCE-FORMS"
			  
   "IF-ELSE-NEXT-OPERATOR?"
   "IF-ELSE-NEXT?"
   "CHECK-IF-ELSE-NEXT?"
   "MAKE-IF-ELSE-NEXT"
   "IF-ELSE-NEXT-CONDITION"
   "IF-ELSE-NEXT-TRUE-FORM"
   "IF-ELSE-NEXT-FALSE-FORM"
			  
   "EVERY-OPERATOR?"
   "COMBINATOR-EVERY?"
   "MAKE-RMPL-ITERATION"
   "RMPL-ITERATION-VARIABLE"
   "RMPL-ITERATION-DOMAIN"
   "RMPL-ITERATION-FORMS"
			  
   ;; From test-routines.lisp
   "RUN-TESTS"
   "PRINT-TESTS"
   "RESET-TEST-SUITES"
   "DEFTEST-SUITE"
   "DEFTEST"
   "RUN-ALL-SUITES"
   ;; Franz Lisp test routine
   ;; Note any other Franz functions used 
   ;; must be exported from mtk-utils as well.
   "TEST"
			 "TEST-ERROR"
			 "TEST-NO-ERROR"
			 
   ;; From utils.lisp
			 
   "SWAP"
			 
   "INSERT-IN-ORDER"

   "SEXP-LESS?"

   "RECONS"

   "SAME-LENGTH?"

   "LENGTH-1?"
   "LENGTH-2?"
   "LENGTH-3?"
   "LENGTH-4?"

   "LENGTH>1?"
   "LENGTH>2?"

   "READ-FILE-TO-STRING"
   "READ-STREAM-TO-STRING"
   "COPY-FILE"

   ;; From reversible-stream.lisp
			 
   ;;support for inputing file names
   "FILE-EXISTS?"
   "REQUEST-INPUT-FILE-NAME"
   "REQUEST-OUTPUT-FILE-NAME"

   ;;LIFO queue
   "MAKE-FIFO"
   "HEAD-FIFO"
   "LAST-FIFO"
   "EMPTY-FIFO?"
   "ADD-TO-FIFO"
   "REMOVE-FROM-FIFO"

   ;;reversible stream operation
   "REVERSIBLE-READ-STREAM"
   "CREATE-REVERSIBLE-READ-STREAM"
   "REVERSIBLE-READ"
   "REVERSIBLE-WRITE"

   ;;searching for tokens/symbols
   "STRIP-SYMBOL-SEQUENCE-IF-NEXT"
   "STRIP-SYMBOL-IF-NEXT"
			 
   ;; From cca-model-keywords.lisp
   "*MODE-TYPE-NOMINAL*"
   "*MODE-TYPE-FAILURE*"
   "*MODE-TYPE-UNKNOWN*"

   ;; From probability.lisp
   "*LIKELY-PROBABILITY*"
   "*LESS-LIKELY-PROBABILITY*"
   "*UNLIKELY-PROBABILITY*"
   "*RARE-PROBABILITY*"
   "*EPSILON-PROBABILITY*"
			 
   "TRANSLATE-TO-NUMERIC-PROBABILITY"

   "LOG-PROBABILITY"

   ;; From string-utils.lisp
   "STRING-TO-LIST"      
   )
  )

(eval-when (compile load eval)
  (import '(
			;; From debug-support.lisp
			MTU::DECLARE-DEBUG-PRINT-SYSTEM!
			MTU::DEBUG-PRINT-SYSTEMS
			MTU::DEBUG-PRINT-ON-FOR
			MTU::DEBUG-PRINT-OFF-FOR
			
			MTU::DEBUG-LEVEL
			MTU::SET-DEBUG-LEVEL
			
			;; From test-routines.lisp
			MTU::RUN-TESTS
			MTU::RUN-ALL-SUITES
			)
		  :CL-USER)
  )

;; Note:  In the defpackage we did not include package :util.test within 
;;;       the :use list hence calls to util.test's exported functions need 
;;;       to be preceded by util.test:.
;;        
;;        The reason why symbols in :util.test were not imported
;;        is because the function TEST needs to be shadowed by
;;        a function TEST that is defined in :mtk-utils --
;;        this function makes a call to util.test:test.  For some
;;;       reason util.test:test was not accessibe to test when
;;;        the :use list and shadowing was employed.

(pushnew :MTK-UTILS *features*)
