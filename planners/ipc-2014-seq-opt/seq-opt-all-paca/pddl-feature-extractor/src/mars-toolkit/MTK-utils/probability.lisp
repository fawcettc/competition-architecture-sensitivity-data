;;; -*- Mode:Common-Lisp; Package:mtk-utils; Base:10 -*-
;;;; Copyright (c) 1999-2013 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Brian C. Williams

(in-package :mtk-utils)

;;; ***********************************************************************
;;;
;;;          ***  Utilities for Manipulating Probabilities ***
;;;
;;; ***********************************************************************


;;; ***********************************************************************
;;;
;;;          ***  Symbolic, Order of Magnitude Probabilities ***
;;;
;;; ***********************************************************************

;;; FIX:
;;; o Old values used in CCA-Model.  Delete when SLEUTH works.
;;;(defvar *unlikely-probability* 0.01 "Probability for an unlikely event")
;;;(defvar *rare-probability* 0.001 "Probability for a rare event")

;;; Note:
;;; o The following are set to the values used in MPL by Livingstone.

(defconstant *likely-probability* 0.01 "Probability for a likely event")

(defconstant *less-likely-probability* 0.005 "Probability for a less likely event")

(defconstant *unlikely-probability* 0.0025 "Probability for an unlikely event")

(defconstant *rare-probability* 0.00001 "Probability for a rare event")

(defconstant *epsilon-probability* 0.0000000001 "Probability of an event with nearly 0 probability,
                                                 needed when using log probabilities because 
                                                 log (0) is undefined.")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Probabilities
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defconstant *likely-symbol* "LIKELY")
(defconstant *less-likely-symbol* "LESS-LIKELY")
(defconstant *unlikely-symbol* "UNLIKELY")
(defconstant *rare-symbol* "RARE")
(defconstant *epsilon-symbol* "EPSILON")
(defconstant *none-symbol* "NONE")

(defun translate-to-numeric-probability (probability object)
  
  "Takes a numeric or symbolic probablity, and returns a
   numeric probability, or nil, if the symbol is NONE.
   Accepts symbol LIKELY, LESS-LIKELY, UNLIKEY, RARE and NONE.
   The correspoondings probabilities are defined by constants
   *<symbol>-PROBABILITY*.

   OBJECT is the assignment or transition that is being
   given a probability."
  
  (cond ((numberp probability)
		 (when (or (> probability 1)
				   (> 0 probability))
		   
		   (error "Probability ~a for ~a is not in the range [0,1]." 
				  probability object))
		 
		 probability)
		
		((symbolp probability)
		 
		 (cond ((equal (symbol-name probability)
					   *none-symbol*)
				
				nil)
			   
			   ((equal (symbol-name probability)
					   *likely-symbol*)
				
				*likely-probability*)
			   
			   ((equal (symbol-name probability)
					   *less-likely-symbol*)
				
				*less-likely-probability*)
			   
			   ((equal (symbol-name probability)
					   *unlikely-symbol*)
				
				*unlikely-probability*)
			   
			   ((equal (symbol-name probability)
					   *rare-symbol*)
				
				*rare-probability*)

			   (:otherwise
				
				(error "Undefined symbolic probability ~a, for ~a" 
					   probability object)
				)))
		
		(:otherwise
		 
		 (error "Illegal probability ~a, for ~a" 
				probability object))))

;;; ***********************************************************************
;;;
;;;          ***  Log Probabilities ***
;;;
;;; ***********************************************************************

(defun log-probability (probability)
  
  "Returns the log base 10 of probability, a number between
   0 and 1.  If probability is 0, then replace with *epsilon-probability*"
  
  (if (= 0.0 probability)
	  ;; Since log 0 is undefined, we take 0 to be a very
	  ;; small probability.
	  (log *epsilon-probability* 10) 
	(log probability 10)))



