;;; -*- Mode:Common-Lisp; Package:mtk-utils; Base:10 -*-

;;; Copyright (c) 1999 - 2008 Brian C. Williams, MIT.

;;; This software may not be redistributed, and can only be retained
;;; and used with the explicit written consent of the author,
;;; Brian C. Williams.

;;; This software is made available as is; no guarantee is provided
;;; with respect to performance or correct behavior.
;;; This software may only be used for non-commercial, non-profit,
;;; research activities.

(in-package :mtk-utils)

;;; ***********************************************************************
;;;
;;;                      ***  Things To Do ***
;;;
;;; ***********************************************************************

;;; o Make these functions inline, rather than using macros.

;;; o Remove calls and code for obsolete functions, as indicated below.

;;; o Add comments.

;;; ***********************************************************************
;;;
;;; ***  Constants for Temporal Operators and Values ***
;;;
;;; ***********************************************************************

(defconstant *next* 'next)


;;; *********************************************************************
;;;
;;; ***  Temporal Logical Formula ***
;;;
;;; *********************************************************************

(defmacro next-operator? (op)

  "An operator denoting next."
  
  `(eql ,op *next*))

(defmacro next? (exp)
  `(and (compound-expression? ,exp)
		(next-operator? (expression-operator ,exp))))

(defmacro check-next? (exp)
  `(legal-form? next (length-2? ,exp) ,exp))

(defmacro make-next (operand)
  `(make-unary-expression *next* ,operand))

(defmacro next-expression-operand (exp)
  `(unary-operand ,exp))

;;; Fix:
;;; o Old, remove callers.
(defmacro next-expression? (exp)
  `(next? ,exp))
