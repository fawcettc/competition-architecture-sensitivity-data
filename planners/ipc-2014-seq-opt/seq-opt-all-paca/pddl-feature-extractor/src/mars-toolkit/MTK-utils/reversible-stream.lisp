;;; -*- Mode:Common-Lisp; Package:MTK-UTILS; Base:10 -*-

(in-package :MTK-UTILS)

;;;   This file contains an implemention of reversible stream, in which 
;;;   items that have been read can be returned to stream in order to be 
;;;   later read.

;;;   It also contains a few generic file functions,
;;;   and an implementation of a LiFo queue.

;;;   ************************************************************
;;;   **
;;;   **  Debugging Flag
;;;   **
;;;   ************************************************************

(defvar *reversible-stream-debug* nil
  "When T, prints debugging information for reversible streams.")
	
;;;   ************************************************************
;;;   **
;;;   **  Support for Querying File Names
;;;   **
;;;   ************************************************************

(defmacro file-exists? (pathname)
  `(probe-file ,pathname))

(defun request-input-file-name ()
  
  "Reads name of a filename from standard-input and returns its pathname,
   or nil, if no file is supplied."

  ;;  (ccl:choose-file-dialog)  ; Used in Macintosh Common Lisp, 
										; doesn't work in Franz lisp

  (format t "~% Please supply a file ~
                to be read ~% (default directory: ~s):" 
		  *default-pathname-defaults*)
  
  ;; Asks user for a file name and returns the resulting pathname.
  (loop for file-string = (read-line)
						  ;; Pathname converts string to path, and 
						  ;; then merge-pathnames fills in defaults
      for pathname = (if (string-equal file-string "")
						 '()
					   (merge-pathnames (pathname file-string)))
      until (file-exists? pathname)
      do (format t "~% File doesn't exist, please supply another name, ~
                       or type return to end: ~% (~A): "
				 *default-pathname-defaults*)
      finally (return (namestring pathname))))

(defun request-output-file-name ()

  "Reads name of a filename from standard-input and returns its pathname, 
   or nil, if no file is supplied."

  ;;  (ccl:choose-file-dialog)  Used in Macintosh Common Lisp, doesn't work in Franz lisp

  (format t "~% Please supply a file to be saved to ~% (default directory: ~s):" 
		  *default-pathname-defaults*)
  
  ;; Asks user for a file name and returns the resulting pathname.
  (loop for file-string = (read-line)
						  ;; Pathname converts string to path, and 
						  ;; then merge-pathnames fills in defaults
      for pathname = (if (string-equal file-string "")
						 '()
					   (merge-pathnames (pathname file-string)))
      until (or (not (file-exists? pathname))
				(y-or-n-p "~% ~A is an existing file, write over it?" (namestring pathname)))
      do (format t "~% please supply another name, or type return to end: ~
                    ~% (~A): "
				 *default-pathname-defaults*)
      finally (return (namestring pathname))))

;;;   ************************************************************
;;;   **
;;;   **  LIFO Queue
;;;   **
;;;   ************************************************************

(defun make-fifo () (list '() '()))
(defmacro head-fifo (fifo)`(first ,fifo))
(defmacro last-fifo (fifo)`(second ,fifo))

(defmacro empty-fifo? (fifo)`(null (head-fifo ,fifo)))

(defun add-to-fifo (item fifo)
  
  (let ((addition (list item)))
    (cond ((null (head-fifo fifo))

		   (setf (head-fifo fifo) addition)
		   (setf (last-fifo fifo) addition))
		  
		  (:otherwise

		   (setf (rest (last-fifo fifo)) addition)
		   (setf (last-fifo fifo) addition)))
    t))

(defun remove-from-fifo (fifo)
  (let ((item (pop (head-fifo fifo))))

    (when (null (head-fifo fifo))
      (setf (last-fifo fifo) '()))
	
    item))

;;;   ************************************************************
;;;   **
;;;   **  Reversible Stream Operation
;;;   **
;;;   ************************************************************

;;;   Implements a stream in which items that have been read can be
;;;   returned to stream in order to be later read.
(defstruct (reversible-read-stream 
			(:print-function print-reversible-read-stream))
  stream
  fifo)

(defun print-reversible-read-stream (reversible-read-stream stream depth)

  (declare (ignore depth))

  (format stream
		  "<~a ~a>"
		  (ignore-errors (reversible-read-stream-stream 
						  reversible-read-stream))
		  (ignore-errors (reversible-read-stream-fifo 
						  reversible-read-stream))))

(defun create-reversible-read-stream (stream)
  (make-reversible-read-stream
   :stream stream
   :fifo (make-fifo)))

(defun reversible-read (reversible-read-stream 
						&optional (eof-error-p T) (eof-value nil))
  
  "Reads next symbol from stream, while allowing symbols later
   to be virtually returned to stream."

  (let* ((fifo (reversible-read-stream-fifo reversible-read-stream))
		 (token
		  (if (empty-fifo? fifo)
			  (read (reversible-read-stream-stream reversible-read-stream)
					eof-error-p eof-value)
			(remove-from-fifo fifo))))

    (when *reversible-stream-debug*
      (format t "~% Reading ~A" token))

    token))

(defun reversible-write (symbol reversible-read-stream)

  "Allows symbol to be virtually returned to stream, to be read later."
  
  (when *reversible-stream-debug*
	(format t "~% Writing ~A" symbol))
	
  (add-to-fifo symbol (reversible-read-stream-fifo reversible-read-stream)))

;;;   ************************************************************
;;;   **
;;;   **  Searching for Tokens/Symbols
;;;   **
;;;   ************************************************************

(defun strip-symbol-sequence-if-next 
	(stream sequence
	 &optional (signal-error? nil))

  "Removes SEQUENCE, a list of symbols, from reversible-read-stream STREAM.
   Returns :FOUND, NIL or :EOF (end of file).  If not :FOUND or :EOF,
   then returns to STREAM any objects that were read.
   If signal-error? is true, then signals an error unless SEQUENCE is found."

  (when *reversible-stream-debug*
	(format t "~% Looking for sequence ~A" sequence))
  
  (loop with symbols-read = '()
      for symbol in sequence
      for read-symbol = (reversible-read stream nil :eof)

      do (when *reversible-stream-debug*
		   (format t "~% Looking for ~A, ~
						read ~A from stream." 
				   symbol read-symbol))
		 
      do (push read-symbol symbols-read)
		 (cond ((eql read-symbol :eof)
				
				(when signal-error?
				  (error "End-of-file reached when expecting symbols ~A" 
						 sequence))
				
				(loop for s in symbols-read
					do (reversible-write s stream))
				
				(return :eof))
			   
			   ((equal (symbol-name symbol) (symbol-name read-symbol))

				(when *reversible-stream-debug*
				  (format t "~% Found ~a" symbol))
			   
				nil)
		 
			   (:otherwise
				
				(when *reversible-stream-debug*
				  (format t "~% Looking for ~A but found ~A" 
						  symbol read-symbol))
		
				(when signal-error?
				  (error "Expecting symbols ~A, but found ~% ~A"
						 sequence (reverse symbols-read)))
				
				(loop for s in symbols-read
					do (reversible-write s stream))
				
				(return nil)))
		 
      finally (return :found)))

(defun strip-symbol-if-next (stream symbol &optional (signal-error? nil))

  "Removes SYMBOL if it appears as the next symbol of reversible-read STREAM.
   Returns :FOUND if found, NIL if not found, or :EOF (end of file).  
   If :NOT-FOUND or :EOF, returns to STREAM any objects that were read.
   If SIGNAL-ERROR? is true, then signals an error unless SYMBOL is found."
  
  (when *reversible-stream-debug*
	(format t "~% Looking for ~a" symbol))
  
  (let ((read-symbol (reversible-read stream nil :eof)))

    (cond ((eql read-symbol :eof)
		   
		   (when signal-error?
			 (error "End-of-file reached when expecting symbol ~A" symbol))
		   :eof)
		  
		  ((equal (symbol-name symbol) (symbol-name read-symbol))
		   
		   (when *reversible-stream-debug*
			 (format t "~% Found ~a" symbol))
		   :found)
		  
		  (:otherwise
		   
		   (when signal-error?
			 (error "Expecting symbol ~A, but found ~% ~A"
					symbol read-symbol))
		   (reversible-write read-symbol stream)
		   nil))))
