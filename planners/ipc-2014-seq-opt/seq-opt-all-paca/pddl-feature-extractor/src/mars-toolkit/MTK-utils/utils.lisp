;;; -*- Mode:Common-Lisp; Package:mtk-utils; Base:10 -*-

(in-package :mtk-utils)

;;;(eval-when (compile load eval)
;;;  (export '(insert-in-order
;;;			length-1?
;;;			length-2?)
;;;		  :mtk-utils)
;;;  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;  
;;  Miscellaneous Utilities
;;
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;  
;;  Swapping Assignments
;;
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmacro swap 
	
	(&rest vars)

  "Swaps the values of every pair of variables in VARS.
   For example, (SWAP A B C D) swaps a with b and c with d."

  `(psetq . ,(loop for (x y) on vars by #'cddr
				 nconcing `(,x ,y ,y ,x))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;  
;;  Management of Ordered Lists
;;
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  
(defun insert-in-order 
	(element list &key (key #'identity) (order #'<=))
  
  "Inserts ELEMENT into LIST in order.  If ORDER is #'<=
   then it inserts it in increasing order, and if it is #'>= it inserts
   it in decreasing order.  The KEY specifies a function that is used to
   extract a value for comparison from each element."
  
  (do ((weight (funcall key element))
       (curr-weight nil)
       (current list)
       (previous nil)
       (done nil))

      ((or done
		   (null current))
       (unless done
		 ;; we've run out, and ELEMENT must be put in at the end
		 ;; note that, once again, we ensure that LIST points to the whole
		 ;; list with ELEMENT

		 (if previous
			 ;; then splice it in
			 (rplacd previous (list element))
		   ;; else there was nothing in LIST
		   (setq list (list element))))
	   ;; and return LIST
       list)
	
    (setq curr-weight 
      (funcall key (first current)))

    (if (funcall order weight curr-weight)
		;; then ELEMENT must be inserted here
		;; Note that in either of these cases, LIST will still point to the
		;; whole list with ELEMENT inserted

		(if previous
			;; then we can splice it in
			(progn
			  (rplacd previous
					  (cons element current))
			  (setq done t))

		  ;; else its still at the beginning
		  (progn
			(setq list
			  (cons element current))
			(setq done t)))

	  ;; Else move the pointers along
      (progn
		(setq previous current)
		(setq current (rest current))))))


;;; ***  Ordering Symbolic Expressions

(defun sexp-less? (s1 s2)

  "Let S1 and S2 be symbolic expressions.
   Returns true iff s1 is lexicographically before s2."
  
  (not (null (string-lessp (format nil "~a" s1)
						   (format nil "~a" s2)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;  
;;  Reconsing:
;;  Efficient Memory Management when Modifying Lists
;;
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun recons (old-pair new-first new-rest)
  
  "Given OLD-PAIR = (NEW-FIRST . NEW-REST) which have been subsequently
   modified, return the pair (NEW-FIRST . NEW-REST),
   while avoiding unnecessary consing."

  (if (and (eql (first old-pair) new-first)
		   (eql (rest old-pair) new-rest))
	  old-pair
	(cons new-first new-rest)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;  
;;  Predicates
;;
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmacro same-length? (exp1 exp2)
  `(eql (length ,exp1)(length ,exp2)))

(defmacro length-1? (l)
  `(and ,l (not (rest ,l))))

(defmacro length>1? (l)
  `(not (null (rest ,l))))

(defmacro length-2? (l)
  `(and (rest ,l)(not (nthcdr 2 ,l))))

(defmacro length>2? (l)
  `(not (null (nthcdr 2 ,l))))

(defmacro length-3? (l)
  `(and (nthcdr 2 ,l)(not (nthcdr 3 ,l))))

(defmacro length-4? (l)
  `(and (nthcdr 3 ,l)(not (nthcdr 4 ,l))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;  
;;  String-Stream Utilities
;;
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun read-file-to-string (filename &key (multibyte nil))
  "Reads a file in as a string.
  MULTIBYTE - If the file contains characters that use more than one 
    byte to represent them, set multibyte to T."
  (with-open-file (stream filename)
	(if multibyte 
		(let ((seq (make-array (file-length stream) :element-type 'character :fill-pointer t)))
		  (setf (fill-pointer seq) (read-sequence seq stream))
		  seq)
		(let ((seq (make-string (file-length stream))))
		  (read-sequence seq stream)
		  seq))))

;; (defun read-stream-to-string (stream &key (multibyte nil))
;;   "Reads a stream in as a string.
;;   MULTIBYTE - If the stream contains characters that use more than one 
;;     byte to represent them, set multibyte to T."
;;   (if multibyte 
;; 	  (let ((seq (make-array (file-length stream) :element-type 'character :fill-pointer t)))
;; 		(setf (fill-pointer seq) (read-sequence seq stream))
;; 		seq)
;; 	  (let ((seq (make-string (file-length stream))))
;; 		(read-sequence seq stream)
;; 		seq)))

(defun read-stream-to-string (stream &key (multibyte nil))
  "Reads a stream in as a string"
  (declare (ignore multibyte))
  (with-output-to-string (out)
    (let ((seq (make-array 1024 :element-type 'character
                          :adjustable t
                          :fill-pointer 1024)))
      (loop
       (setf (fill-pointer seq) (read-sequence seq stream))
       (when (zerop (fill-pointer seq))
        (return))
       (write-sequence seq out)))))


(defun copy-file (in-path-name out-path-name)
  ;; Open the source file.
  (with-open-file (inp in-path-name)
	;; Create a destination file named path-name-TMP.
	(with-open-file (outp out-path-name :direction :output :if-exists :supersede)
	  
	  ;; Transfer the contents of the source file to the temporary 
	  ;; destination, while wrapping the contents with <Spec2> ... </Spec2>.
		
	  ;; Read and write, line by line, stopping at end of file.
	  (loop for line = (read-line inp nil 'end-of-file)
		 until (eql line 'end-of-file)
		 do (format outp "~a~%" line))
)))

;; ;; The following function is by a 3rd party, and is copied from:
;; ;; http://hampshire.edu/~lasCCS/courses/string-to-list.lisp
;; ;;
;; ;; Lee Spector (lspector@hampshire.edu), 10/15/97
;; (defun string-to-list (string)
;;   "Returns a list of the data items represented in the given list."
;;   (let ((the-list nil) ;; we'll build the list of data items here
;;         (end-marker (gensym))) ;; a unique value to designate "done"
;;     (loop (multiple-value-bind (returned-value end-position)
;;                                (read-from-string string nil end-marker)
;;             (when (eq returned-value end-marker)
;;               (return the-list))
;;             ;; if not done, add the read thing to the list
;;             (setq the-list 
;;                   (append the-list (list returned-value)))
;;             ;; and chop the read characters off of the string
;;             (setq string (subseq string end-position))))))

