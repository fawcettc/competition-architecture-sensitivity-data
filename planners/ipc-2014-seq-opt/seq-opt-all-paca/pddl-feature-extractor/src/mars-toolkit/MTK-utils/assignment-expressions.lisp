;;; -*- Mode:Common-Lisp; Package:mtk-utils; Base:10 -*-

;;; Copyright (c) 1999 - 2008 Brian C. Williams, MIT.

;;; This software may not be redistributed, and can only be retained
;;; and used with the explicit written consent of the author,
;;; Brian C. Williams.

;;; This software is made available as is; no guarantee is provided
;;; with respect to performance or correct behavior.
;;; This software may only be used for non-commercial, non-profit,
;;; research activities.

(in-package :mtk-utils)

;;; ***********************************************************************
;;;
;;;                      ***  Things To Do ***
;;;
;;; ***********************************************************************

;;; o Cleanup treatment of assignments.

;;; o Make these functions inline, rather than using macros.

;;; o Remove calls and code for obsolete functions, as indicated below.

;;; o Add comments.

;;; ***********************************************************************
;;;
;;; ***  Constants for Defined Operators and Values ***
;;;
;;; ***********************************************************************

;;; *** Assignment and equality relations

(defconstant *=* '=)

(defconstant *assignment-ops* (list *=*))

(defconstant *equality-relations* (list *=*))

;;; *********************************************************************
;;;
;;; ***  Equivalence ***
;;;
;;; *********************************************************************

(defmacro equality-relation? (op)
  `(member ,op *equality-relations* :test #'eql))

;;; *********************************************************************
;;;
;;; ***  Assignments ***
;;;
;;; *********************************************************************

(defmacro assignment-operator? (op)
  `(member ,op *assignment-ops* :test #'eql))

;;; Expressions describing variable value assignments:
;;; (= <var> <value)

(defmacro assignment? (exp)
  `(and (binary-expression? ,exp)
		(assignment-operator? (expression-operator ,exp))))

;; To Do:
;;; o Old remove.
(defmacro assignment-expression? (exp)
  `(assignment? ,exp))

(defmacro make-assignment (&key variable-name value)
  `(make-expression *=* (list ,variable-name ,value)))

(defun assignment-variable (exp)
  (second exp))

(defmacro assignment-value (exp)
  `(third ,exp))

(defun equal-assignments?
	(asgn1 asgn2)

  "Returns True iff two assignment expressions are equivalent."

  (equal asgn1 asgn2))

(defun equal-assignment-values?
	(asgn1 asgn2)

  "Returns True iff two assignment expressions are equivalent."

  (equal (assignment-value asgn1)
		 (assignment-value asgn2)))

(defun contains-assignment? (assignment assignments)

  "Returns true iff ASSIGNMENTS contains ASSIGNMENT."
  
  (loop for assignment2 in assignments
	  when (equal-assignments? assignment assignment2)
	  return t
	  finally (return nil)))

(defmacro extend-assignments
	(assignment assignments)

  "Returns the assignment resulting from the addition of 
   ASSIGNMENT to ASSIGNMENTS."

  `(cons ,assignment ,assignments))

;;; *********************************************************************
;;;
;;; ***  Implicit Assignments ***
;;;
;;; *********************************************************************

;;; In MPL:  An assignment of value to variable was represented with out
;;;   an explicit assignment operator, as:
;;;      (value variable)
;;;   rather than:
;;;      (= variable value)

;;; The following macros support this implicit form of assignment.
;;; Use of this form in the future is discouraged.

(defmacro implicit-assignment? (exp)
  "Expression MAY BE of the form (value variable)."
  
  `(and (listp ,exp)
		(eql (length ,exp) 2)))

;;; Fix:
;;; o Remove keywords, it seems clumsy.
(defmacro make-implicit-assignment (&key variable-name value)
  `(list ,value ,variable-name))

(defmacro implicit-assignment-variable (exp)
  `(second ,exp))

(defmacro implicit-assignment-value (exp)
  `(first ,exp))

;;; Fix:
;;; o Remove keywords, it seems clumsy.
(defmacro map-implicit-to-explicit-assignment (exp)
  `(make-assignment 
	:variable-name (implicit-assignment-variable ,exp)
	:value (implicit-assignment-value ,exp)))

;;; *********************************************************************
;;;
;;; ***  Obsolete ***
;;;
;;; *********************************************************************

;;; Fix:
;;; Old: remove callers
(defmacro assign-variable (exp)
  `(second ,exp))

;;; Fix:
;;; o old: remove callers
(defmacro assign-value (exp)
  `(third ,exp))

;;; *********************************************************************
;;;
;;; ***  Variable Terms ***
;;;
;;; *********************************************************************

(defmacro make-variable-term 
	(relative-variable-name instance-name)
  `(list ,relative-variable-name , instance-name))

(defmacro variable-term-relative-variable-name 
	(variable-term)
  `(first ,variable-term))

(defmacro variable-term-instance-name 
	(variable-term)
  `(second ,variable-term))

;;; *********************************************************************
;;;
;;; ***  Values ***
;;;
;;; *********************************************************************

;;; *** Unspecified value

(defconstant *no-value* 'none "Indicates that no value was specified.
                               Applies to any variable type.")

(defmacro no-value? (value)

  "Returns true if VALUE indicates that no value was specified,
   and returns nil otherwise. Applies to any variable type."
  
  `(eql ,value *no-value*))
