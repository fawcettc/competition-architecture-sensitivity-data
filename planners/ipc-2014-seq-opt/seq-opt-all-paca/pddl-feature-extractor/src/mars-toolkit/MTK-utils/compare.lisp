;;; -*- Mode:Common-Lisp; Package:mtk-utils; Base:10 -*-

;;; Copyright (c) 1999 - 2009 Brian C. Williams, MIT.

;;; This software may not be redistributed, and can only be retained
;;; and used with the explicit written consent of the author,
;;; Brian C. Williams.

;;; This software is made available as is; no guarantee is provided
;;; with respect to performance or correct behavior.

;;; This software may only be used for non-commercial, non-profit,
;;; research activities.

(in-package :mtk-utils)

;;;(eval-when (compile load eval)
;;;  (export '(
;;;			extract-proposition-description ;support method to be
;;;										; supplied by all constraint 
;;;										; systems using the following
;;;										; comparison functions.
;;;
;;;			equal-models
;;;			set-contains-model
;;;			equal-model-sets
;;;			equal-model-sequences
;;;			equal-conflicts
;;;			equal-conflict
;;;			equal-assignments
;;;			
;;;			equal-consistent?-conflict-result
;;;			equal-model-conflict-result
;;;			)
;;;		  :mtk-utils))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;                                                               ;;;
;;;  Interface Functions:    	                                  ;;;
;;;     Comparing Symbols                              	          ;;;
;;;                                                               ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; (lexicographic-< symbol1 symbol2)
;;;  
;;;  Returns true iff the string name of SYMBOL1
;;;   precedes that of SYMBOL2.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;                                                               ;;;
;;;  Interface Functions:    	                                  ;;;
;;;     Comparing Sets                              	          ;;;
;;;                                                               ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; (EQUAL-SETS S1 S2)
;;;   Let s1 and s2 be sets.
;;;   Returns true iff the members of s1 
;;;   are equal to s2.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;                                                               ;;;
;;;  Interface Functions:    	                                  ;;;
;;;     Comparing Models, Conflicts and Assignments    	          ;;;
;;;                                                               ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; The following functions are used to compare sets of
;;; assignments, particularly truth assignments.
;;; These assignments may be consistent wrt a theory (Models) or
;;; inconsistent (Conflict).

;;; An assignment L is expected to assign a truth-value 
;;; (*true* or *false*) to a proposition: (= PROPOSITION TRUTH-VALUE).
   
;;; Proposition is either a propositionalal description (sexpression)
;;; or an object that may be mapped to a propositional description
;;; using method PROPOSITION-DESCRIPTION.

;;; In reality, an assignment may be an arbitrary sexpressions whose 
;;; elements are dereferenced using proposition-description,
;;; wherever possible.

;;; (EQUAL-MODELS INTERP1 INTERP2)
;;;   Returns true if two models returned by
;;;   ISAT are equivalent.

;;; (SET-CONTAINS-MODEL MODEL-SET MODEL)
;;;   Returns true if MODEL is contained in the
;;;   set of models Model-set.

;;; (EQUAL-MODEL-SETS MODEL-SET1 MODEL-SET2)
;;;   Returns true iff MODEL-SET1 and MODEL-SET2
;;;   denote the same sets of models  Each model
;;;   is a set of truth assignments to propositions
;;;   that are consistent.

;;; (EQUAL-MODEL-SEQUENCES MODEL-SEQ1 MODEL-SEQ2)
;;;
;;;   Returns true iff MODEL-SEQ1 and MODEL-SEQ2
;;;   denote the same sequences of models  Each model
;;;   is a set of truth assignments to propositions
;;;   that are consistent.

;;; (EQUAL-CONFLICTS CONFLICTS1 CONFLICTS2)
;;;   Returns true iff CONFLICTS1 and CONFLICTS2 
;;;   denote the same sets of conflicts.  Each conflict 
;;;   is a set of truth assignments to propositions
;;;   that together are mutually inconsistent.

;;; (EQUAL-CONFLICT CONFLICT1 CONFLICT2)
;;;   Returns true iff CONFLICT1 and CONFLICT2 
;;;   denote the same conflict.  Each conflict is a set
;;;   of truth assignments to propositions, which together are
;;;   mutually inconsistent.

;;; (EQUAL-ASSIGNMENTS A1 A2)
;;;   Returns true iff and only if A1 and A2 denote the same
;;;   truth assignment. a1 and a2 are of the form (p truth),
;;;   where p is a proposition described by a proposition structure
;;;   or a symbol (the proposition description) and truth is
;;;   a truth value.

;;; (EXTRACT-PROPOSITION-DESCRIPTION (P Type))
;;;
;;;   Returns a proposition description for p.
;;;   By default, this is p itself.

;;; (EQUAL-CONSISTENT?-CONFLICT-RESULT RESULT1 RESULT2)
;;;
;;;    Checks equality of returned values of the form:
;;;         (values consistent? conflict) 
;;;      or (values inconsistent? conflict).

;;; (EQUAL-MODEL-CONFLICT-RESULT RESULT1 RESULT2)
;;;
;;;   Checks equality of returned values of the form:
;;;      (values found-assignment conflict).
;;;    found assignment is a list of pairs (P truth-value)
;;;    where P is a proposition structure or symbol,
;;;    and truth-value is *true* or *false*.


;;; Optimization:
;;; o None of these comparison functions are designed
;;;   for efficiency.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;                                                               ;;;
;;;                      Comparing Symbols                        ;;;
;;;                                                               ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun lexicographic-< (symbol1 symbol2)
  
  "Returns true iff the string name of SYMBOL1
   precedes that of SYMBOL2."

  (string-lessp (format nil "~s" symbol1)
				(format nil "~s" symbol2)))
	    
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;                                                               ;;;
;;;                      Comparing Sets                           ;;;
;;;                                                               ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun equal-sets (s1 s2)

  "Let s1 and s2 be sets.
   Returns true iff the members of s1 
   are equal to s2."
  
  (and (subsetp s1 s2 :test #'equal)
	   (subsetp s2 s1 :test #'equal)))
				  
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;                                                               ;;;
;;;         Comparing Models, Conflicts and Assignments           ;;;
;;;                                                               ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun equal-models (interp1 interp2)

  "Returns true if two models returned by
   ISAT are equivalent."
  
  (equal-assignments interp1 interp2))

(defun set-contains-model (model-set model)
  
  "Returns true if MODEL is contained in the
   set of models MODEL-SET."

  (member model model-set :test #'equal-models))

(defun equal-model-sets (model-set1 model-set2)
  "Returns true iff MODEL-SET1 and MODEL-SET2
   denote the same sets of models.  Each model
   is a set of truth assignments to propositions
   that are consistent."

  (and (subsetp model-set1 model-set2 :test #'equal-models)
       (subsetp model-set2 model-set1 :test #'equal-models)))

(defun equal-model-sequences (model-seq1 model-seq2)

  "Returns true iff MODEL-SEQ1 and MODEL-SEQ2
   denote the same sequences of models  Each model
   is a set of truth assignments to propositions
   that are consistent."
  
  (and (eql (length model-seq1) (length model-seq2))
       (loop for m1 in model-seq1
		   for m2 in model-seq2
		   always (equal-models m1 m2))))

;;; To Do:
;;; o Add document string.
(defmacro get-test-fn-for-seq-of-sets (sets-intervals)
  `(lambda (x y) (equal-model-sequences-of-sets x y ,sets-intervals)))

(defun equal-model-sequences-of-sets (model-seq-of-set1 model-seq-of-set2
									  &optional (sets-intervals nil))
  
  "Returns true iff MODEL-SEQ-OF-SET1 and MODEL-SEQ-OF-SET2
   denote the same sequence of sets of models. Each model
   is a set of truth assignments to propositions that are consistent.
   This is necessary because there are models with the same cost, 
   thus there is no preferred order over them."
  
  (if (null sets-intervals)
	  
      (equal-model-sequences model-seq-of-set1 model-seq-of-set2)
	
    (let ((watch-indices (first sets-intervals)))
	  
      (and 
	   
	   (equal-model-sequences 
		(subseq model-seq-of-set1 0 (first watch-indices))
		(subseq model-seq-of-set2 0 (first watch-indices)))
	   
	   (equal-model-sets
		
		(subseq model-seq-of-set1 
				(first watch-indices) 
				(min (length model-seq-of-set1)
					 (1+ (second watch-indices))))
		
		(subseq model-seq-of-set2 
				(first watch-indices) 
				(min (length model-seq-of-set2)
					 (1+ (second watch-indices)))))

	   (equal-model-sequences-of-sets
		
		(subseq model-seq-of-set1
				(1+ (second watch-indices)))
		
		(subseq model-seq-of-set2
				(1+ (second watch-indices)))
			
		(update-sets-intervals 
		 (rest sets-intervals)
		 (1+ (second watch-indices))))))))

;;; To Do:
;;; o Add document string, explaining arguments and function.
(defun update-sets-intervals (sets-intervals translation-factor)

  (loop for set-interval in sets-intervals
      collect (list (- (first set-interval) translation-factor)
					(- (second set-interval) translation-factor))))

(defun equal-conflicts (conflicts1 conflicts2)

  "Returns true iff CONFLICTS1 and CONFLICTS2 
   denote the same sets of conflicts.  Each conflict 
   is a set of truth assignments to propositions
   that together are mutually inconsistent."

  (and (subsetp conflicts1 conflicts2 :test #'equal-conflict)
       (subsetp conflicts2 conflicts1 :test #'equal-conflict)))

(defun equal-conflict (conflict1 conflict2)

  "Returns true iff CONFLICT1 and CONFLICT2 
   denote the same conflict.  Each conflict is a set
   of truth assignments to propositions, which together are
   mutually inconsistent."

  (equal-assignments conflict1 conflict2))

(defun equal-assignments (a1s a2s)

  "Returns true iff and only if A1s and A2s denote the same
   sets of truth assignments. Each a1 in A1s and a2 in A2s 
   are of the form (p truth),
   where p is a proposition described by a proposition object
   or a symbol (the proposition description) and truth is
   a truth value."

  (and (subsetp a1s a2s :test #'equal-assignment)
       (subsetp a2s a1s :test #'equal-assignment)))

(defun equal-assignment (a1 a2)
  
  "Assignments a1 and a2 are expected to be
   truth assignments to propositions (= PROPOSITION TRUTH-VALUE).
   
   Proposition is either a propositional description (sexpression)
   or an object that may be mapped to a propositional description
   using method PROPOSITION-DESCRIPTION.

   In reality, Ln are arbitrary sexpressions whose elements are
   dereferenced using proposition-description whereever possible."

  (equal-proposition-description a1 a2))

;;; To Do:
;;; o proposition-description -> description.
(defun equal-proposition-descriptions (l1 l2)

  "Returns true iff and only if l1 and l2 denote the same
   sets of propositions. l1 and l2 are of the form (p truth),
   where p is a proposition described by an object
   or a symbol (the proposition description) and truth is
   a truth value."


  (and (subsetp l1 l2 :test #'equal-proposition-description)
       (subsetp l2 l1 :test #'equal-proposition-description)))

;;; To Do:
;;; o proposition-description -> description.
(defun equal-proposition-description (l1 l2)
  
  "Returns true if and only if L1 and L2 denote the same
   proposition descriptions. L1 and L2 can be arbitrary 
   nested lists. Proposition objects within L1 and L2 are dereferenced 
   to their proposition descriptions during comparison,
   by applying method EXTRACT-PROPOSITION-DESCRIPTION TO LN."


  (if (null l1)
      (null l2)
    (let ((l1-pd (extract-proposition-description l1))
		  (l2-pd (extract-proposition-description l2)))

      (cond ((listp l1-pd)
			 (if (listp l2-pd)
				 (and (equal-proposition-description (first l1-pd)(first l2-pd))
					  (equal-proposition-description (rest l1-pd) (rest l2-pd)))
			   nil))
			
			((and (numberp l1-pd)
				  (numberp l2-pd))
			 (= l1-pd l2-pd))
			
			(:otherwise
			 ;; Modified by peng; Using string comparison for models. Is this ok?
			 (equal (format nil "~A" l1-pd) (format nil "~A" l2-pd)))))))

;;; To Do:
;;; o The following function and all its callers need to be cleaned up.
;;    This code was written as being specific to proposition objects.
;;;   However, in general each object should be developed to support 
;;;   an extract-description method.  Then a set of methods are built on
;;;   top of extract-description, which are used for comparison.
;;;   They should check that objects passed in are of the proper type.
(defmethod extract-proposition-description ((p T))

  "Returns a proposition description for p.
   By default, this is p itself."

  p)

;;;(defun extract-proposition-description (p)
;;;
;;;  "p is a proposition object or a proposition description.
;;;   returns a proposition description for p."
;;;  
;;;  (if (typep p 'proposition)
;;;      (proposition-description p)
;;;    p))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;                                                               ;;;
;;;                  Support for ISAT Testing:                    ;;;
;;;        Comparing Results of ISAT test expressions             ;;;
;;;                                                               ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun equal-consistent?-conflict-result (result1 result2)

  " Checks equality of returned values of the form:
         (values consistent? conflict) 
      or (values inconsistent? conflict)."

  (and 
   (length-2? result1)

   (length-2? result2)

   (let ((consistent1? (not (null (first result1))))
		 (consistent2? (not (null (first result2)))))
	 
     (eql consistent1? consistent2?))
   
   (let ((conflict1 (second result1))
		 (conflict2 (second result2)))
	 
	 (equal-conflict conflict1 conflict2))))

(defun equal-model-conflict-result (result1 result2)

  "Checks equality of returned values of the form:
      (values found-assignment conflict).
    found assignment is a list of pairs (P truth-value)
    where P is a proposition structure or symbol,
    and truth-value is *true* or *false*."
  
  (and 
   
   (length-2? result1)
   (length-2? result2)
   
   (let ((model1 (first result1))
		 (model2 (first result2)))
	 (equal-models model1 model2))
   
   (let ((conflict1 (second result1))
		 (conflict2  (second result2)))
	 (equal-conflict conflict1 conflict2))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;                                                               ;;;
;;;                  Support for Explanation Testing              ;;;
;;;                                                               ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun equal-explanations-and-conflicts (result1 result2)

  "Returns true iff and only if Results1 and Result2 denote
   equivalent pairs of explanations and conflicts."

  (and 
   (length-2? result1)
   (length-2? result2)
   (let ((explanation1 (first result1))
		 (explanation2 (first result2)))
	 (equal-explanations explanation1 explanation2))
   (let ((conflict1 (second result1))
		 (conflict2 (second result2)))
	 (equal-conflicts conflict1 conflict2))))

(defun equal-explanations (e1 e2)

  "Returns true iff and only if E1 and E2 denote the same
   set of explanations."

  (and (subsetp e1 e2 :test #'equal-explanation)
       (subsetp e2 e1 :test #'equal-explanation)))

(defun equal-explanation (explanation1 explanation2)

  " Checks equality of explanations, where an explanation
    is composed of a relaxation and an exclusion set."

  (and 
   (length-2? explanation1)
   (length-2? explanation2)
   (equal-relaxation (first explanation1) (first explanation2))
   (equal-exclusion (second explanation1) (second explanation2))))

(defun equal-relaxation (relaxation1 relaxation2)

  "Returns true iff RELAXATION1 and RELAXATION2
   denote the same relaxation.  Each relaxation is a set
   of constraints that together are mutually consistent."

  (equal-constraints relaxation1 relaxation2)
;;; Fix Delete the following when works.  
;;;  (equal-assignments relaxation1 relaxation2)
  )

(defun equal-exclusion (exclusion1 exclusion2)

  "Returns true iff EXCLUSION1 and EXCLUSION2
   denote the same exclusion set.  Each exclusion is a set
   of constraints that when relaxed makes the origninal test of
   constraints consistent."

  (equal-constraints exclusion1 exclusion2)
;;; Fix Delete the following when works.  
;;;  (equal-assignments exclusion1 exclusion2)
  )

(defun equal-constraint-conflicts (conflicts1 conflicts2)

  "Returns true iff CONFLICTS1 and CONFLICTS2 
   denote the same sets of conflicts.  Each conflict 
   is a set of constraints (not assignments)
   that together are mutually inconsistent."

  (and (subsetp conflicts1 conflicts2 :test #'equal-constraint-conflict)
       (subsetp conflicts2 conflicts1 :test #'equal-constraint-conflict)))

(defun equal-constraint-conflict (conflict1 conflict2)

  "Returns true iff CONFLICT1 and CONFLICT2 
   denote the same conflict.  Each conflict is a set
   of constraints (not assignments), which together are
   mutually inconsistent."

  (equal-constraints conflict1 conflict2))

(defun equal-constraints (c1s c2s)

  "Returns true iff and only if A1s and A2s denote the same
   sets of constraints. Each c1 in C1s and c2 in C2s 
   is a constraint object or a symbolic expression denoting
   a constraint."

  (and (subsetp c1s c2s :test #'equal-constraint)
       (subsetp c2s c1s :test #'equal-constraint)))

;;; To do:
;;; o Change proposition-description to just description.
(defun equal-constraint (c1 c2)
  
  "Constraints c1 and c2 denote the same relation.
   
   Each constraint is either described by a symbolic expression
   a constraint object, which is mapped to a propositional description
   using method PROPOSITION-DESCRIPTION, or
   an arbitrary sexpressions whose elements are
   dereferenced using PROPOSITION-DESCRIPTION whereever possible."
  
  (equal-proposition-description c1 c2))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;                                                               ;;;
;;;                  Support for TPN Explanation Testing          ;;;
;;;                                                               ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun equal-tpn-explanations-and-conflicts
	(result1 result2)

  "Returns true iff and only if Results1 and Result2 denote
   equivalent pairs of lists of tpn explanations and conflicts."

  (and 
   (length-2? result1)
   (length-2? result2)
   (let ((explanations1 (first result1))
		 (explanations2 (first result2)))
	 (equal-tpn-explanations explanations1 explanations2))
   (let ((conflict1 (second result1))
		 (conflict2 (second result2)))
	 (equal-constraint-conflicts conflict1 conflict2))))

(defun equal-tpn-explanations (e1 e2)

  "Returns true iff and only if E1 and E2 denote the same
   set of explanations for TPNs."

  (and (subsetp e1 e2 :test #'equal-tpn-explanation)
       (subsetp e2 e1 :test #'equal-tpn-explanation)))

(defun equal-tpn-explanation (explanation1 explanation2)

  " Checks equality of TPN explanations, where a TPN explanation
    is composed of a relaxation an exclusion set, and explanations for
    each excluded constraint."

  (and 
   (length-3? explanation1)
   (length-3? explanation2)
   (equal-relaxation (first explanation1) (first explanation2))
   (equal-exclusion (second explanation1) (second explanation2))
   (equal-exclusion-explanations (third explanation1)
								 (third explanation2))))

(defun equal-exclusion-explanations (e1 e2)

  "Returns true iff and only if E1 and E2 denote the same
   set of explanations for exclusions."

  (and (subsetp e1 e2 :test #'equal-exclusion-explanation)
       (subsetp e2 e1 :test #'equal-exclusion-explanation)))

(defun equal-exclusion-explanation (explanation1 explanation2)

  " Checks equality of TPN exclusion explanations, where a TPN explanation
    is composed of an excluded constraint and a set of constraints that are
    mutually inconsistent with the excluded constraint."

  (and 
   (length-2? explanation1)
   (length-2? explanation2)
   (equal-constraint (first explanation1) (first explanation2))
   (equal-explanation-antecedent (second explanation1) (second explanation2))))

(defun equal-explanation-antecedent (a1 a2)

  "Returns true iff and only if E1 and E2 denote the same
   set of antedecedents of an explanation."

  (and (subsetp a1 a2 :test #'equal-constraint)
       (subsetp a2 a1 :test #'equal-constraint)))