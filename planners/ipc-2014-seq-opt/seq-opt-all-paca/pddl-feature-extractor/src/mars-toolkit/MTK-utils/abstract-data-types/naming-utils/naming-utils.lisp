;;;; Copyright (c) 2013 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   David C. Wang

(in-package #:naming-utils)

(defun is-keyword? (arg)
  "Returns t if the argument ARG is eq to a keyword symbol that can appear in a lambda list."
  (some (lambda (keyword) (eq arg keyword)) '(&allow-other-keys &key &rest &aux &optional)))

(defun make-lambda-list (specialized-lambda-list)
  "Returns a lambda-list from a SPECIALIZED-LAMBDA-LIST"
  (let ((keyword-detected nil))
	(mapcar (lambda (arg)
			  (unless keyword-detected
				(setf keyword-detected (is-keyword? arg)))
			  (cond (keyword-detected arg)
					((listp arg) (car arg))
					(:otherwise arg)))
			specialized-lambda-list)))

(defun make-call-list (specialized-lambda-list)
  "Returns a list of variables names from a SPECIALIZED-LAMBDA-LIST"
  (let ((keyword nil))
	(mapcan (lambda (arg)
			  (cond ((is-keyword? arg)
					 (setf keyword arg) nil)
					((eq keyword '&aux)
					 (if (listp arg) `(,(intern (car arg) 'keyword) ,(car arg)) `(:,arg ,arg)))
					((eq keyword '&allow-other-keys)
					 nil)
					((eq keyword '&key)
					 (if (listp arg) 
						 (if (listp (car arg))
							 (car arg)
							 `(,(intern (car arg) 'keyword) ,(car arg)))
						  `(:,arg ,arg)))
					((eq keyword '&rest)
					 `(,arg))
					((eq keyword '&optional)
					 (if (listp arg) `(,(car arg)) `(,arg)))
					(:otherwise
					 (if (listp arg) `(,(car arg)) `(,arg))) ))
			specialized-lambda-list)))

(defun make-call-list-with-rest (specialized-lambda-list rest-var)
  "Returns a list of variables names from a SPECIALIZED-LAMBDA-LIST 
   that uses the &rest keyword, with the REST-VAR symbol."
  (let ((keyword nil)
		(list1 nil))
	(loop for arg in specialized-lambda-list do
		 (when (or (eq arg '&rest)
				   (eq arg '&key)
				   (eq arg '&allow-other-keys)
				   (eq arg '&aux))
		   (return))
		 (cond ((is-keyword? arg)
				(setf keyword arg))
			   ((eq keyword '&optional)
				(setf list1 (nconc list1
								   (if (listp arg) `(,(car arg)) `(,arg)))))
			   (:otherwise
				(setf list1 (nconc list1
								   (if (listp arg) `(,(car arg)) `(,arg))))) )
		 )
	(append list1 `(,rest-var)) ))

(defun make-lambda-list-with-rest (lambda-list)
  "Returns two values. The first value is the lambda list with a rest variable, 
   if it does not already exist. The second value is the rest variable as a symbol.
   The lambda list can be a special lambda list."
  (let ((rest-var nil)
		(rest-inserted nil)
		(list1 nil)
		(list2 nil))
	(loop for arg in lambda-list
	   with flag = nil do
		 (when (or (eq arg '&key)
				   (eq arg '&allow-other-keys)
				   (eq arg '&aux))
		   (setf flag t))
		 (when (and rest-inserted (not rest-var))
		   (setf rest-var arg))
		 (when (eq arg '&rest)
		   (setf rest-inserted t))
		 (if flag
			 (setf list2 (nconc list2 `(,arg)))
			 (setf list1 (nconc list1 `(,arg)))))
	(if rest-inserted
		(values (append list1 list2) rest-var)
		(progn (setf rest-var (intern (gensym "REST-")))
			   (values (append list1 `(&rest ,rest-var) list2) rest-var)))
	))

(defun vars-after-rest (lambda-list) 
  "Returns a list of variables specified in the lambda list 
   that appears after a &rest that should be declared ignored 
   if the rest variable is used."
  (let ((keyword nil))
	(mapcan (lambda (arg)
			  (cond ((is-keyword? arg)
					 (setf keyword arg) nil)
					((eq keyword '&aux)
					 (if (listp arg) `(,(car arg)) `(,arg)))
					((eq keyword '&allow-other-keys)
					 nil)
					((eq keyword '&key)
					 (if (listp arg)
						 ;; handle list based key expression
						 (let ((vars nil))
						   ;; check the first item
						   (if (listp (car arg))
							   ;; handle ((:keyword var) ...)
							   (push (cadar arg) vars)
							   ;; handle (var ...)
							   (push (car arg) vars))
						   (when (eql (length arg) 3)
							 (push (caddr arg) vars))
						   vars)
						 ;; handle single variable expression
						 `(arg))) ))
			lambda-list)))

(defmacro defun-synonym (names &rest defdata)
  "Uses the same function definition to define multiple functions. 
   Each function takes a name from the list of NAMES."
  `(progn ,@(mapcar #'(lambda (n) `(defun ,n ,@defdata)) names)))

(defmacro defmethod-synonym (function-names method-names &rest defdata)
  "Uses the same method definition to define multiple functions and methods. 
   Each function takes a name from the list FUNCTION-NAMES. 
   Each method takes a name from the list METHOD-NAMES."
  `(progn ,@(mapcar (lambda (n) `(defmethod ,n ,@defdata)) method-names)
		  ,@(mapcar (lambda (n) `(defun ,n ,(make-lambda-list (car defdata)) ,@(rest defdata))) function-names)))

(defmacro defmethod-alias (function-name method-names &rest defdata)
  "Defines one function named FUNCTION-NAME, and provides aliases for it using different METHOD-NAMES.
   The aliased methods will be defined with the same arguments as the function, and call FUNCTION-NAME.
   For convenience, METHOD-NAMES can either be one name or a list."
  (multiple-value-bind (spec-lambda-list rest-var) (make-lambda-list-with-rest (car defdata))
	`(progn (defun ,function-name ,(make-lambda-list (car defdata)) ,@(rest defdata))
			,@(mapcar (lambda (n) `(defmethod ,n ,spec-lambda-list (declare (ignore ,@(vars-after-rest (car defdata))))
									 (apply #',function-name ,@(make-call-list-with-rest (car defdata) rest-var))))
					  (if (listp method-names) method-names `(,method-names))))
	))

(defmacro abbrev (short long)
  `(defmacro ,short (&rest args)
	 `(,',long ,@args)))

;; (defmacro abbrevs (&rest names)
;;   `(progn
;; 	 ,@(mapcar #'(lambda (pair)
;; 				   `(abbrev ,@pair))
;; 			   (group names 2))))




