;;;; Copyright (c) 2011 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   David Wang

(in-package #:abstract-data-types)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; HASHBAG
;; Stores the key associated with the element. If the key is
;; added more than once, a number is stored indicating the 
;; number of times it has been added. 
;;
;; The key is computed by passing the element to a user-defined
;; key-function. By default, the key-function returns the 
;; element as its own key.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass hashbag (adt-collection)
  ((table 
	:type hash-table
	:initform (make-hash-table)
	:initarg :table
	:accessor hb-table
	:documentation "The lisp hash table storing the hash map.")
   (key-function
	:type function
	:initform (lambda (x) x)
	:initarg :key-function
	:accessor hb-key-function
	:documentation "Computes the key, given the element, that will be used to test for membership.")
   (size
	:type integer
	:initform 0
	:initarg :size
	:accessor hb-size
	:documentation "The numbers of elements in the hash set"))
)

(defun make-hashbag (&key (key-function (lambda (x) x)) 
					 (test 'eql) (size 67)
					 (rehash-size 1.2) (rehash-threshold 0.6407767))
  (make-instance 
   'hashbag
   :table (make-hash-table :test test :size size 
							  :rehash-size rehash-size
							  :rehash-threshold rehash-threshold)
   :key-function key-function
   :size 0))

(defmethod-alias hb-copy coll-copy ((bag hashbag) &key &allow-other-keys)
  "Returns a copy of this BAG."
  (with-slots (table key-function size) bag
	(let ((copy (make-hashbag :key-function key-function 
							  :test (hash-table-test table) 
							  :size (hash-table-size table) 
							  :rehash-size (hash-table-rehash-size table) 
							  :rehash-threshold (hash-table-rehash-threshold table))))
	  (coll-add-all! copy bag)
	  copy)))

(defmethod-alias hb-add! coll-add! ((bag hashbag)(element t) &key (replace nil)(use-key-function t) &allow-other-keys)
  "Adds ELEMENT to the BAG.
   Returns two values. 
   The first value is boolean and indicates a change was made to the BAG.
   The second value will be the number of occurences of ELEMENT in the BAG.
   If REPLACE is nil (default), the element will be added.
   If REPLACE is t, the current count of ELEMENT will be set to 1.
   If USE-KEY-FUNCTION is nil, the ELEMENT is used to test for set membership.
   If USE-KEY-FUNCTION is t (default), the key computed by applying the 
   key-function to the ELEMENT is used to test for set membership."
  (with-slots (table key-function size) bag
	(let ((key (if use-key-function (funcall key-function element) element)))
	  (multiple-value-bind (old-count there?)
		  (gethash key table nil)
		(cond 
		  ;; if the key already exists and replace is t,
		  ;; set element-count to 1, decrease size by elements removed 
		  ((and there? replace)
		   (setf (gethash key table) 1)
		   (setf size (+ size (- old-count) 1))
		   (values t 1))
		  ;; if the key already exists and replace is nil,
		  ;; increment element-size by 1, increment size by 1
		  ((and there? (not replace))
		   (incf (gethash key table))
		   (incf size)
		   (values t (gethash key table)))
		  ;; if the key does not exist, add it.
		  ;; set element-size to 1, increment size by 1
		  (:otherwise
		   (setf (gethash key table) 1)
		   (incf size)
		   (values t 1))
		  )))))

(defmethod-alias hb-add-list! coll-add-all! ((bag hashbag)(elements list) &key &allow-other-keys)
  "Appends all of the ELEMENTS to the end of the BAG.
   Complexity: O(1)*<length of elements>"
  (loop for element in elements do
	   (hb-add! bag element)))

(defmethod-alias hb-add-coll! coll-add-all! ((bag hashbag)(elements adt-collection) &key &allow-other-keys)
  "Appends all of the ELEMENTS to the end of the BAG.
   Complexity: O(1)*<length of elements>"
  (coll-map elements 
			(lambda (element) (coll-add! bag element))))

(defmethod-alias hb-get coll-get ((bag hashbag)(element t) &key (use-key-function t) &allow-other-keys)
  "Returns the number of times ELEMENT appears in the BAG."
  "Note that this function/method is not specified nor required by the set interface. 
   Implementations of coll-get on sets may return implementation specific values."
  (with-slots (table key-function size) bag
	(let ((key (if use-key-function (funcall key-function element) element)))
	  (multiple-value-bind (count there?)
		  (gethash key table nil)
		(if there? count 0)))))

(defmethod-alias hb-remove! coll-remove! ((bag hashbag)(element t) &key (use-key-function t) &allow-other-keys)
  "Removes ELEMENT from the BAG.
   Returns t if the collection was modified, nil otherwise."
  (with-slots (table key-function size) bag
	(let ((key (if use-key-function (funcall key-function element) element)))
	  (multiple-value-bind (old-count there?)
		  (gethash key table nil)
		(cond 
		  ;; if there, and only 1 left, 
		  ;; remove the key, decrement size, return (key t count)
		  ((and there? (equal element 1))
		   (remhash key table)
		   (decf size 1)
		   (values key t 0))
		  ;; if there and > 1 left, 
		  ;; decrement count, decrement size, return (key t count)
		  ((and there? (> element 1))
		   (setf (gethash key table) (- old-count 1))
		   (decf size 1)
		   (values key t (- old-count 1)))
		  ;; if not there, return (nil nil 0)
		  (:otherwise
		   (values nil nil 0))
		  )))))

(defmethod-alias hb-clear! coll-clear! ((bag hashbag) &key &allow-other-keys) 
  "Clears the BAG of all contents"
  (with-slots (table size) bag
	(clrhash table)
	(setf size 0)))

(defmethod-alias hb-contains? coll-contains? ((bag hashbag)(element t) &key (use-key-function t) &allow-other-keys)
  "If the set contains the key, returns two values: 
   t and the number of instances of that element's key.
   and (nil 0) if it does not."
  (with-slots (table key-function) bag
	(let ((key (if use-key-function (funcall key-function element) element)))
	  (multiple-value-bind (count there?)
		  (gethash key table nil)
		(if there?
			(values t count)
			(values nil 0))))))

;; the hb-size method is already provided for by the hashbag class's accessor.
(defmethod coll-size ((bag hashbag) &key (unique-element nil) &allow-other-keys)
  "Returns the total number of elements in the BAG.
   If UNIQUE-ELEMENT is t, returns the number of unique elements."
  (if unique-element
	  (hash-table-count (hb-table bag))
	  (hb-size bag)))

(defmethod-alias hb-count coll-count ((bag hashbag)(element t) &key (use-key-function nil))
  "Returns the count of an element's key in this bag."
  (with-slots (table key-function) bag
	(let ((key (if use-key-function (funcall key-function element) element)))
	  (multiple-value-bind (old-count there?)
		  (gethash key table nil)
		(if there?
			old-count
			0)))))

(defmethod-alias hb-empty? coll-empty? ((bag hashbag) &key &allow-other-keys)
  "Returns t if the hashbag is empty, nil otherwise."
  (equal (hash-table-count (hb-table bag)) 0))

(defmethod-alias hb-map coll-map ((bag hashbag) (f function) &key (unique-element nil) &allow-other-keys)
  "Applies a function F to each element in the SET.
   The function is required to take one argument - a set element.
   If UNIQUE-ELEMENT is nil, the function F is called once for each element in the bag.
   If UNIQUE-ELEMENT is t, the function F is called once for each unique element in the bag.
   The function is required to take one argument - an element's key."
  (if unique-element
	  (maphash (lambda (k v) (declare (ignore v)) (funcall f k)) 
			   (hb-table bag))
	  (maphash (lambda (k v) (dotimes (i v) (funcall f k))) 
			   (hb-table bag))))

(defmethod-alias hb-tolist coll-tolist((bag hashbag) &key (unique-element nil) &allow-other-keys)
  "Returns a list of the elements in the BAG.
   If UNIQUE-ELEMENT is nil, each element in the bag will be included in the list.
   If UNIQUE-ELEMENT is t, each unique element in the bag will be included once in the list."
  (let ((hslist (make-smart-list)))
	(hb-map bag 
			(lambda (k v) (declare (ignore k)) 
					(slist-push-end v hslist))
			:unique-element unique-element)
	(slist-head hslist)))

(defmethod-alias hb-toarray coll-toarray((bag hashbag) &key (unique-element nil)(adjustable nil)(fill-pointer nil)(displaced-to nil)(displaced-index-offset 0)  &allow-other-keys)
  "Returns an array of the elements in the BAG.
   If UNIQUE-ELEMENT is nil, each element in the bag will be included in the list.
   If UNIQUE-ELEMENT is t, each unique element in the bag will be included once in the list."
  (make-array (coll-size bag :unique-element unique-element) 
			  :initial-contents (hb-tolist bag :unique-element unique-element)
			  :adjustable adjustable
			  :fill-pointer fill-pointer
			  :displaced-to displaced-to
			  :displaced-index-offset displaced-index-offset))

(defmethod print-object ((bag hashbag) stream)
  (let ((first-loop t))
	(hb-map bag
			(lambda (value) 
			  (unless first-loop 
				(format stream ","))
			  (format stream "~a" value)
			  (setf first-loop nil)))
	))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ITERATOR
;; Not supported by hashbag class.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmethod make-coll-iterator ((bag hashbag) &key &allow-other-keys)
  "Creates an iterator object that maintains its state over the BAG.
  This method should throw an error if it is not supported."
  (error 'unsupported-function))

(check-implementation 'hashbag)