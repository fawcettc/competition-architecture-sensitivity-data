;;;; Copyright (c) 2013 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Eric Timmons

(in-package #:abstract-data-types)

(defconstant *tm-it-state-left* :left)
(defconstant *tm-it-state-center* :center)
(defconstant *tm-it-state-right* :right)
(defconstant *tm-it-state-new* :new)

(defstruct tree-map-node
  "node for binary search trees."
  value        ; The element(s) stored in this node.
  key           ; The key value used to sort this element.
  (parent nil :type tree-map-node)        ; The parent node. Needed for efficient deletion.
  (left nil :type tree-map-node)          ; The left node
  (right nil :type tree-map-node)         ; The right node
  )

(defmethod print-object ((node tree-map-node) stream)
  (format stream "#<TREE-MAP-NODE~tvalue: ~a~tkey: ~a>~%" (tree-map-node-value node) (tree-map-node-key node)))

(defclass tree-map (adt-ordered-map)
  ((root
	:type tree-map-node
	:initform nil)
   (number-elements
	:type fixnum
	:initform 0)
   (order-predicate
	:type function
	:initform #'(lambda (x y) (- y x))
	:initarg :order-predicate
	:accessor tm-order-predicate
	:documentation "Function that, given two element keys, returns a
	negative number if the elements are reversed, zero if the elements
	are equal, and a positive number if the elements are in order.")))

;; (defun make-tree-map (&key contents (key-function #'identity) (order-predicate #'(lambda (x y) (- y x))))
;;   (let ((tm (make-instance 'tree-map :order-predicate order-predicate :key-function key-function)))
;; 	(when contents
;; 	  (map-add-all! tm contents))
;; 	tm))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Querying state                                                           ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmethod-alias tm-size map-size ((map tree-map) &key &allow-other-keys)
  "Returns the number of KEY VALUE pairs stored in the MAP."
  (slot-value map 'number-elements))

(defmethod-alias tm-empty? map-empty? ((map tree-map) &key &allow-other-keys)
  "Returns t if the MAP contains no keys, nil otherwise"
  (zerop (slot-value map 'number-elements)))

(defmethod-alias tm-contains-key? map-contains-key? ((map tree-map) (key t) &key &allow-other-keys)
  "Returns t if the MAP contains the KEY
   and nil if it does not."
  (multiple-value-bind (item found?) (tm-get map key)
	(declare (ignore item))
	found?))

(defmethod-alias tm-contains-value? map-contains-value? ((map tree-map) (value t) &key &allow-other-keys)
  "Returns two values. 
   The first value is t if the MAP contains the VALUE, NIL otherwise. 
   The second value is the key associated with the VALUE."
  (with-slots (root) map
	(tm-get-by-value-helper root value)))

(defun tm-get-by-value-helper (node value)
  (declare (type tree-map-node node))

  (if (null node)
	  (values nil nil)
	  (progn
		(multiple-value-bind (found? key) (tm-get-by-value-helper (tree-map-node-left node) value)
		  (if found?
			  (values found? key)
			  (if (eql (tree-map-node-value node) value)
				  (values T (tree-map-node-key node))
				  (tm-get-by-value-helper (tree-map-node-right node) value)))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Retrieving elements                                                      ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmethod-alias tm-get map-get ((map tree-map) (key t) &key (default nil) &allow-other-keys)
  "Returns the value associated with KEY in the MAP.
   Returns two values. 
     If the KEY is found, the first value will be the value 
   associated with the key, the second value will be t, 
   indicating the key was found.
     If the KEY is not found, the first value will be DEFAULT 
   the second value will be nil."
  (with-slots (root order-predicate number-elements) map
	(multiple-value-bind (node found?) (tm-get-helper root key order-predicate default)
	  (if found?
		  (values (tree-map-node-value node) t)
		  (values default nil)))))

(defun tm-get-helper (node key order-p default)
  (declare (type tree-map-node node)
		   (type function order-p))
  (if (null node)
	  (values default nil)
	  (let ((comparison (funcall order-p (tree-map-node-key node) key)))
		(cond
		  ((minusp comparison)
		   (tm-get-helper (tree-map-node-left node) key order-p default))
		  ((plusp comparison)
		   (tm-get-helper (tree-map-node-right node) key order-p default))
		  (t
		   (values node t))))))

(defmethod-alias tm-get-range ordered-map-get-range ((map tree-map) &key (low-key nil low-key-p) (high-key nil high-key-p) &allow-other-keys)
  "Returns a LIST of the values that have keys between LOW-KEY and
   HIGH-KEY. Returns NIL if no values exist in that range."
  (let ((arg-list (list map)))
	(when high-key-p
	  (setf arg-list (nconc arg-list (list :high-key high-key))))
	(when low-key-p
	  (setf arg-list (nconc arg-list (list :low-key low-key))))
	(let ((it (apply #'make-tm-iterator arg-list)))
	  (loop while (map-it-hasnext it)
		   collecting (map-it-next it)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Mapping elements                                                         ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmethod-alias tm-map map-map ((map tree-map) (f function) &key &allow-other-keys)
  "Applies a function F to each entry in the MAP.
   The function is required to take two arguments - the key and the value."
  (with-slots (root) map
	(tm-map-helper root f)))

(defun tm-map-helper (node f)
  (declare (type tree-map-node node)
		   (type function f))

  (unless (null node)
	(tm-map-helper (tree-map-node-left node) f)
	(funcall f (tree-map-node-key node) (tree-map-node-value node))
	(tm-map-helper (tree-map-node-right node) f)))

(defmethod-alias tm-toplist map-toplist ((map tree-map) &key &allow-other-keys)
  "Returns a p-list. Returns nil if the p-list is empty."
  (let ((it (make-tm-iterator map)))
	(loop with k and v
	   while (map-it-hasnext it)
	   for nil = (multiple-value-setq (k v) (map-it-next it))
	   appending (list k v))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Add Elements                                                             ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmethod-alias tm-add! map-add! ((map tree-map) (key t) (value t) &key (replace t) (default nil) &allow-other-keys)
  "Adds the KEY VALUE pair to the MAP.
   Returns two values. The first value is boolean and represents if the KEY VALUE 
   pair was added. The second value is the original value associated with the KEY, 
   DEFAULT otherwise.
   If REPLACE is t (default), the new key-value pair will replace the old key-value pair."
  
  (with-slots (root order-predicate number-elements) map
	(if (null root)
		(progn
		  (setf root (make-tree-map-node :value value
										 :key key))
		  (incf number-elements)
		  (values T default))
		(multiple-value-bind (added? e replaced?) (tm-add!-helper root value key order-predicate :replace replace :default default)
		  (unless replaced?
			(incf number-elements))
		  (values added? e)))))

(defun tm-add!-helper (node element element-key order-p &key replace default)
  (declare (type tree-map-node node)
   		   (type function order-p))
  (let ((comparison (funcall order-p
							 (tree-map-node-key node)
							 element-key)))
	(cond
	  ((zerop comparison)
	   ;; ELEMENT is equal to the KEY used in this search node.
	   (if replace
		   (let ((old-value (tree-map-node-value node)))
			 (setf (tree-map-node-value node) element)
			 (values T old-value T))
		   (values nil default)))
	  ((minusp comparison)
	   ;; Add ELEMENT to the left of this node.
	   (let ((left-node (tree-map-node-left node)))
		 (if (null left-node)
			 (progn
			   (setf (tree-map-node-left node) (make-tree-map-node :value element
																   :key element-key
																   :parent node))
			   (values t default))
			 (tm-add!-helper left-node element element-key order-p))))
	  ((plusp comparison)
	   ;; Add ELEMENT to the right of this node.
	   (let ((right-node (tree-map-node-right node)))
		 (if (null right-node)
			 (progn
			   (setf (tree-map-node-right node) (make-tree-map-node :value element
																	:key element-key
																	:parent node))
			   (values t default))
			 (tm-add!-helper right-node element element-key order-p))))
	  (T
	   ;; Signal an error
	   (error "Invalid value returned by ORDER-P")))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Removing Elements                                                        ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmethod-alias tm-clear! map-clear! ((map tree-map) &key &allow-other-keys)
  "Clears the collection MAP of all contents."
  (setf (slot-value map 'root) nil)
  (setf (slot-value map 'number-elements) 0))

(defmethod-alias tm-remove! map-remove! ((map tree-map) (key t) &key (default nil) &allow-other-keys)
  "Removes the KEY and associated value from the MAP.
   Returns two values. 
     If the KEY is found, the first value will be the value 
   associated with the key, the second value will be t, 
   indicating something was removed.
     If the KEY is not found, the first value will be DEFAULT 
   the second value will be nil."
  (with-slots (root order-predicate number-elements) map
	(multiple-value-bind (item found?) (tm-remove!-helper root key order-predicate default)
	  (if found?
		  (decf number-elements))
	  (values item found?))))

(defun tm-replace-node-in-parent! (node new-value)
  (declare (type tree-map-node node)
		   (type tree-map-node new-value))
  (let ((parent (tree-map-node-parent node)))
	(when parent
	  (if (eq node (tree-map-node-left parent))
		  (setf (tree-map-node-left parent) new-value)
		  (setf (tree-map-node-right parent) new-value)))
	(when new-value
	  (setf (tree-map-node-parent new-value) parent))))

(defun tm-remove!-helper (node key order-p default)
  (declare (type tree-map-node node)
		   (type function order-p))
  (if (null node)
	  (values default nil)
	  (let ((comparison (funcall order-p (tree-map-node-key node) key)))
		(cond
		  ((minusp comparison)
		   (tm-remove!-helper (tree-map-node-left node) key order-p default))
		  ((plusp comparison)
		   (tm-remove!-helper (tree-map-node-right node) key order-p default))
		  ((zerop comparison)
		   (cond
			 ((and (tree-map-node-left node) (tree-map-node-right node))
			  (let ((successor (tm-find-min (tree-map-node-right node)))
					(old-value (tree-map-node-value node)))
				(setf (tree-map-node-key node) (tree-map-node-key successor))
				(setf (tree-map-node-value node) (tree-map-node-value successor))
				(tm-replace-node-in-parent! successor nil)
				(values old-value t)))
			 ((tree-map-node-left node)
			  (tm-replace-node-in-parent! node (tree-map-node-left node))
			  (values (tree-map-node-value node) t))
			 ((tree-map-node-right node)
			  (tm-replace-node-in-parent! node (tree-map-node-right node))
			  (values (tree-map-node-value node) t))
			 (T
			  (tm-replace-node-in-parent! node nil)
			  (values (tree-map-node-value node) t))))
		  (t
		   (error "Invalid value returned by ORDER-P"))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Searching for elements                                                   ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun tm-find-min (node)
  (declare (type tree-map-node node))
  (tm-find-min-helper node 1))

(defun tm-find-min-helper (node num-explored)
  (declare (type tree-map-node node)
		   (type fixnum num-explored))
  (let ((left-node (tree-map-node-left node)))
	(if (null left-node)
		node
		(tm-find-min-helper left-node (+ 1 num-explored)))))

(defun tm-find-min-path (node)
  (declare (type tree-map-node node))
  (tm-find-min-path-helper node nil))

(defun tm-find-min-path-helper (node path)
  (declare (type tree-map-node node)
		   (type list path))
  (let ((left-node (tree-map-node-left node)))
	(if (null left-node)
		(values node path)
		(tm-find-min-path-helper left-node (nconc (list *tm-it-state-left*) path)))))

(defmethod tm-find-min-range ((map tree-map) low-key)
  (declare (type tree-map map))
  (with-slots (root order-predicate) map
	(tm-find-min-range-helper root low-key order-predicate nil)))

(defun tm-find-min-range-helper (node low-key order-p best-so-far)
  (declare (type tree-map-node node best-so-far)
   		   (type function order-p))
  (if (null node)
	  best-so-far
	  (let ((comparison (funcall order-p (tree-map-node-key node) low-key)))
		(cond
		  ((zerop comparison)
		   ;; This is THE answer.
		   node)
		  ((minusp comparison)
		   ;; It's possible there's a node to the left of this one that is
		   ;; still above the LOW-KEY.
		   (tm-find-min-range-helper (tree-map-node-left node) low-key order-p node))
		  ((plusp comparison)
		   ;; Don't explore to the left any more! Look to the right to see
		   ;; if there's anything above LOW-KEY, but below BEST-SO-FAR's key.
		   (tm-find-min-range-helper (tree-map-node-right node) low-key order-p best-so-far))))))

(defmethod tm-find-min-range-path (node (map tree-map) low-key)
  (declare (type tree-map map)
		   (type tree-map-node node))
  (with-slots (order-predicate) map
	(tm-find-min-range-path-helper node low-key order-predicate nil nil nil)))

(defun tm-find-min-range-path-helper (node low-key order-p best-so-far current-path best-so-far-path)
  (declare (type tree-map-node node best-so-far)
   		   (type function order-p)
		   (type list current-path best-so-far-path))
  (if (null node)
	  (values best-so-far best-so-far-path)
	  (let ((comparison (funcall order-p (tree-map-node-key node) low-key)))
		(cond
		  ((zerop comparison)
		   ;; This is THE answer.
		   (values node current-path))
		  ((minusp comparison)
		   ;; It's possible there's a node to the left of this one that is
		   ;; still above the LOW-KEY.
		   (tm-find-min-range-path-helper (tree-map-node-left node) low-key order-p node (append (list *tm-it-state-left*) current-path) current-path))
		  ((plusp comparison)
		   ;; Don't explore to the left any more! Look to the right to see
		   ;; if there's anything above LOW-KEY, but below BEST-SO-FAR's key.
		   (tm-find-min-range-path-helper (tree-map-node-right node) low-key order-p best-so-far (append (list *tm-it-state-right*) current-path) best-so-far-path))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Iterating                                                                ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass tree-map-iterator (adt-ordered-map-iterator)
  ((tree-map
	:initarg :tree-map
	:reader tree-map)
   (current-node
	:reader current-node)
   (order-p
	:reader order-p)
   (search-state
	:reader search-state)))

(defclass bounded-tree-map-iterator (tree-map-iterator)
  ((has-lower-key?
	:initarg :has-lower-key?
	:reader has-lower-key?)
   (lower-key
	:initarg :lower-key
	:reader lower-key)
   (has-upper-key?
	:initarg :has-upper-key?
	:reader has-upper-key?)
   (upper-key
	:initarg :upper-key
	:reader upper-key)))

(defun tm-it-increment-search! (it)
  (declare (type tree-map-iterator it))
  (with-slots (search-state current-node) it
	(let ((last-state (pop search-state)))
	  (unless (null current-node)
		(cond
		  ((eql last-state *tm-it-state-new*)
		   ;; I haven't explored any children of CURRENT-NODE yet.
		   (let ((left-child (tree-map-node-left current-node)))
			 (if left-child
				 (progn
				   (push *tm-it-state-left* search-state)
				   (push *tm-it-state-new* search-state)
				   (setf current-node left-child)
				   (tm-it-increment-search! it))
				 (progn
				   ;; Done
				   (push *tm-it-state-center* search-state)))))
		  ((eql last-state *tm-it-state-left*)
		   (push *tm-it-state-center* search-state))
		  ((eql last-state *tm-it-state-center*)
		   (let ((right-child (tree-map-node-right current-node)))
			 (if right-child
				 (progn
				   (push *tm-it-state-right* search-state)
				   (push *tm-it-state-new* search-state)
				   (setf current-node right-child)
				   (tm-it-increment-search! it))
				 (progn
				   (setf current-node (tree-map-node-parent current-node))
				   (tm-it-increment-search! it)))))
		  ((eql last-state *tm-it-state-right*)
		   (setf current-node (tree-map-node-parent current-node))
		   (tm-it-increment-search! it))
		  (T
		   (error "You shouldn't have reached this case.")))))))

(defmethod initialize-instance :after ((it tree-map-iterator) &key &allow-other-keys)
  (with-slots (tree-map search-state order-p current-node) it
	(with-slots (root order-predicate) tree-map
	  (setf order-p order-predicate)
	  (tm-it-initialize! it))))

(defmethod tm-it-initialize! ((it tree-map-iterator))
  (with-slots (tree-map search-state current-node) it
	(with-slots (root) tree-map
	  ;; Find the path to the min node, guarded on the fact we
	  ;; actually HAVE a tree.
	  (when root
		(multiple-value-bind (min-node path) (tm-find-min-path root)
		  (setf current-node min-node)
		  (setf search-state (nconc (list *tm-it-state-center*) path)))))))

(defmethod tm-it-initialize! ((it bounded-tree-map-iterator))
  (with-slots (tree-map search-state current-node has-lower-key? lower-key) it
	(with-slots (root) tree-map
	  ;; Find the path to the min node, guarded on the fact we
	  ;; actually HAVE a tree.
	  (when root
		(if has-lower-key?
			(multiple-value-bind (min-node path) (tm-find-min-range-path root tree-map lower-key)
			  (setf current-node min-node)
			  (setf search-state (nconc (list *tm-it-state-center*) path)))
			(call-next-method))))))

(defmethod-alias make-tm-iterator make-map-iterator ((map tree-map) &key (low-key nil low-key-p) (high-key nil high-key-p) &allow-other-keys)
  "Creates an iterator object that maintains its state over the collection.
  If provided, iterates in order between LOW-KEY and HIGH-KEY.
  This method should throw an error if it is not supported."
  (if (or low-key-p high-key-p)
	  (make-instance 'bounded-tree-map-iterator :tree-map map
					 :has-upper-key? high-key-p
					 :upper-key high-key
					 :has-lower-key? low-key-p
					 :lower-key low-key)
	  (progn
		(make-instance 'tree-map-iterator :tree-map map))))

(defmethod-alias tm-it-hasnext map-it-hasnext ((it tree-map-iterator) &key &allow-other-keys)
  "Returns t if the map used to create the TREE-MAP-ITERATOR has another key value pair.
  This method should throw an error if it is not supported."
  (if (null (slot-value it 'current-node))
	  nil
	  t))

(defmethod-alias tm-it-next map-it-next ((it tree-map-iterator) &key &allow-other-keys)
  "Returns the next key value pair in the map used to create the TREE-MAP-ITERATOR. 
   If there no more values, nil is returned.
   This method should throw an error if it is not supported."
  (with-slots (current-node tree-map search-state) it
	(let ((return-val current-node))
	  ;; Progress the search forward. Called for effect.
	  (tm-it-increment-search! it)
	  (if (null return-val)
		  nil
		  (values (tree-map-node-value return-val) (tree-map-node-key return-val))))))

(defmethod-alias tm-bounded-it-hasnext map-it-hasnext ((it bounded-tree-map-iterator) &key &allow-other-keys)
  "Returns t if the map used to create the TREE-MAP-ITERATOR has another key value pair.
  This method should throw an error if it is not supported."
  (with-slots (current-node order-p has-upper-key? upper-key) it
	(if (null current-node)
		nil
		(if has-upper-key?
			(if (or (null current-node) (minusp (funcall order-p (tree-map-node-key current-node) upper-key)))
				nil
				t)
			T))))

(defmethod-alias tm-bounded-it-next map-it-next ((it bounded-tree-map-iterator) &key &allow-other-keys)
  "Returns the next key value pair in the map used to create the TREE-MAP-ITERATOR. 
   If there no more values, nil is returned.
   This method should throw an error if it is not supported."
  (with-slots (current-node tree-map search-state) it
	(if (map-it-hasnext it)
		(let ((return-val current-node))
		  ;; Progress the search forward. Called for effect.
		  (tm-it-increment-search! it)
		  (if (null return-val)
			  nil
			  (values (tree-map-node-value return-val) (tree-map-node-key return-val))))
		nil)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Make sure the implementation is complete                                 ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(check-implementation 'tree-map)
(check-implementation 'tree-map-iterator)
(check-implementation 'bounded-tree-map-iterator)