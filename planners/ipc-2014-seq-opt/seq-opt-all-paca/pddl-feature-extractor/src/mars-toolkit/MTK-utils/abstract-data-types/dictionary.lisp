;;;; Copyright (c) 2011 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   David C. Wang

(in-package #:abstract-data-types)

;; IMPORTANT - IMPLEMENTATION IS SIMPLE!!!
;; IMPORTANT - IMPLEMENTATION IS SIMPLE!!!
;; IMPORTANT - IMPLEMENTATION IS SIMPLE!!!
;; Note: The keys for each value must be unique, 
;; because each column stores its keys in a hashtable, not a multimap.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Class DICTIONARY COLUMN
;; This class allows you to specify some values as keys
;;

(defclass dictionary-column ()
  ((name
	:type t
	:accessor dictionary-column-name
	:initarg :name
	:initform nil
	:documentation "The name for this column in the dictionary.")
   (key-fn
	:type function
	:accessor dictionary-column-key-fn
	:initarg :key-fn
	:initform nil
	:documentation "The function used to find the key in an object for this column.")
   (hash-table 
	:type hash-table
	:accessor dictionary-column-hash-table
	:initarg :hash-table
	:initform nil
	:documentation "The hashtable for each column in the dictionary."
	)
   ))

(defun make1-dictionary-column (name key-fn test)
  (make-instance 'dictionary-column 
				 :name name 
				 :key-fn key-fn
				 :hash-table (make-hash-table :test test)))

(defun make2-dictionary-column (&key name key-fn test)
  (make-instance 'dictionary-column 
				 :name name
				 :key-fn key-fn
				 :hash-table (make-hash-table :test test)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Class DICTIONARY
;; This class allows you to save arbitrary objects, given a function that indexes that object as a key.
;;

(defclass dictionary ()
  ((col-hash-table
	:accessor dictionary-col-hash-table
	:initarg :col-hash-table
	:initform nil
	:documentation "Maps from a column symbol to a class consisting of a hashtable and a lookup function.")
   ))

(defun make-dictionary (column-defs &key (column-name-test #'eql))
  "(:name :key-fn :test)"
  (let ((col-hash-table (make-hash-table :test column-name-test))
		(column))
	(dolist (column-def column-defs)
	  (setf column (apply #'make2-dictionary-column column-def))
	  (setf (gethash (dictionary-column-name column) col-hash-table)
			column))
	(make-instance 'dictionary :col-hash-table col-hash-table)
	))

(defmethod dictionary-add ((dictionary dictionary) (value t) &key (column-names nil))
  (with-slots (col-hash-table) dictionary
	(flet ((column-add (column)
			 (with-slots (key-fn hash-table) column
			   (setf (gethash (funcall key-fn value) hash-table) value))))
	  ;; add to specific columns
	  (cond (;; add to all columns
			 (null column-names)
			 (loop for column being the hash-values in col-hash-table do
				  (column-add column)) )
			(;; add the value to each column named in COLUMN-NAMES
			 (listp column-names) 
			 (loop for column-name in column-names do
				  (let ((column (gethash column-name col-hash-table)))
					(when column (column-add column)))))
			(;; add the single column named by COLUMN-NAMES
			 :otherwise
			 (let ((column (gethash column-names col-hash-table)))
			   (when column (column-add column))) )
			))
	))

(defmethod dictionary-remove-value ((dictionary dictionary) (value t))
  (with-slots (col-hash-table) dictionary
	(let ((removed nil))
	  (loop for column being the hash-values in col-hash-table do
		   (with-slots (key-fn hash-table) column
			 (setf removed (or removed 
							   (remhash (funcall key-fn value) hash-table)))
			 ))
	  removed)
	))

(defmethod dictionary-get ((dictionary dictionary) (column-name t) (column-value t))  
  "Returns two values, the first is the value found, the second is whether the value exists."
  (with-slots (col-hash-table) dictionary
	(let ((column (gethash column-name col-hash-table)))
	  (unless column
		(error "Column ~a does not exist." column-name))
	  (gethash column-value (dictionary-column-hash-table column))
	  )))

(defmethod dictionary-remove ((dictionary dictionary) (column-name t) (column-value t))
  (with-slots (col-hash-table) dictionary
	(multiple-value-bind (value value-found) 
		(dictionary-get dictionary column-name column-value)
	  ;; checks if the value was found
	  (cond (value-found
			 (dictionary-remove-value dictionary value))
			(:otherwise nil))
	  )))

(defmethod dictionary-clear ((dictionary dictionary))
  (loop for column being the hash-values in (dictionary-col-hash-table dictionary) do
	 ;; delete the row entry from each column
	   (clrhash (dictionary-column-hash-table column)))
  )

(defun parse-keyed-arguments (arguments)
  "Given a list of arguments of the form (:SYMBOL1 value1 :SYMBOL2 value2 ...),
   returns a hashtable that maps the symbol to the value."
  (let* ((numargs (length arguments))
		 (argtable) )
	;; find the number of arguments (half the length of ARGUMENTS)
	(cond ((evenp numargs) 
		   (setf numargs (/ numargs 2)))
		  (:otherwise (error "There are an uneven number of arguments. Each argument must appear as a symbol followed by its value.")))
	;; make the hash table
	(setf argtable (make-hash-table :test #'eql :size numargs))
	;; set the arguments in the hashtable
	(loop for argpair on (list 10 20 30 40) by #'cddr do
		 (let ((symbol (first argpair))
			   (value (second argpair)))
		   (unless (symbolp symbol)
			 (error "The arguments must appear as a symbol followed by its value."))
		   (setf (gethash symbol argtable) value)
		   ))
	))