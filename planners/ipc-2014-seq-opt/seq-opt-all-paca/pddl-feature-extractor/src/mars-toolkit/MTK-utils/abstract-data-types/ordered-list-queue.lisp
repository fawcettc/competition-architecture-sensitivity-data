;;;; Copyright (c) 2013 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Eric Timmons

(in-package #:abstract-data-types)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ORDERED-LIST-QUEUE
;; This is a priority queue backed by a smart-list.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass ordered-list-queue (adt-priority-queue)
  ((head
	:initform nil
	:accessor ordered-list-queue-head)
   (size
	:initform 0
	:accessor ordered-list-queue-size)))

(defun make-ordered-list-queue (&key (key #'identity) (order #'<))
  (make-instance 'ordered-list-queue :key key :order order))

(defmethod coll-copy ((q ordered-list-queue) &key &allow-other-keys)
  "Returns a copy of this Q."
  (with-slots (key order) q
	(let ((copy (make-ordered-list-queue :key key 
										 :order order)))
	  (coll-add-all! copy q)
	  copy)))

(defun in-order? (e1 e2 q)
  "=> boolean

T iff `E1` comes before `E2` with respect ot the ordering specified by
`Q`."
  (declare (type adt-priority-queue q))
  (with-slots (key order) q
	(funcall order (funcall key e1) (funcall key e2))))

(defmethod coll-add! ((q ordered-list-queue)(element t) &key &allow-other-keys)
  "Adds ELEMENT to the collection COLL. 
   Returns t if the collection was modified, nil otherwise."
  (with-slots (head size key order) q
	;; Check to see if the head needs to be replaced.
	(if (or (null head) (in-order? element (car head) q))
		(progn
		  (push element head)
		  (incf size)
		  t)
		;; Otherwise, loop over all cons cells and insert in place.
		(progn
		  (loop for e on head
			 until (or (null (cdr e)) (in-order? element (cadr e) q))
			 finally
			   (setf (cdr e) (cons element (cdr e))))
		  (incf size)
		  t))))

(defmethod coll-add-all! ((coll ordered-list-queue)(elements list) &key &allow-other-keys)
  "Appends all of the ELEMENTS to the end of the COLL.
   Returns t if the collection was modified, nil otherwise. 
   Complexity: O(1)*<length of elements>"
  (let ((modified? nil))
 	(loop for element in elements do
 		 (setf modified? (or (coll-add! coll element) modified?)))
 	modified?))

(defmethod coll-add-all! ((coll ordered-list-queue)(elements adt-collection) &key &allow-other-keys)
  "Appends all of the ELEMENTS to the end of the COLL.
   Returns t if the collection was modified, nil otherwise. 
   Complexity: O(1)*<length of elements>"
  (let ((modified? nil))
	(coll-map elements
 			  (lambda (element) (setf modified? (or (coll-add! coll element) modified?))))
 	modified?))

(defmethod coll-remove! ((q ordered-list-queue)(element t) &key &allow-other-keys)
  "Removes ELEMENT from the collection Q. 
   Returns t if the collection was modified, nil otherwise."
  (with-slots (head size) q
	(let* ((sentinel (cons nil head))
		   (num-removed (olq-recursive-remove head sentinel element 0)))
	  (decf size num-removed)
	  (not (eql 0 num-removed)))))

(defun olq-recursive-remove (current-cell previous-cell element num-removed)
  (declare (type cons current-cell previous-cell)
		   (type fixnum num-removed))
  (if (eql (car current-cell) element)
	  ;; Remove this cell and update pointers
	  (progn
		(setf (cdr previous-cell) (cdr current-cell))
		(incf num-removed)
		(if (null (cdr current-cell))
			num-removed
			(olq-recursive-remove (cdr current-cell) previous-cell element num-removed)))
	  ;; Proceed to next cell unless this is the end.
	  (if (null (cdr current-cell))
		  num-removed
		  (olq-recursive-remove (cdr current-cell) current-cell element num-removed))))

(defmethod coll-clear! ((q ordered-list-queue) &key &allow-other-keys)
  "Clears the collection Q of all contents."
  (with-slots (head size) q
	(setf head nil)
	(setf size 0)))

(defmethod coll-contains? ((q ordered-list-queue)(element t) &key &allow-other-keys)
  "Returns t if collection Q contains ELEMENT
   and nil if it does not."
  (with-slots (head) q
	(if (member element head)
		t
		nil)))

(defmethod coll-size ((q ordered-list-queue) &key &allow-other-keys)
  "Returns the size of the collection Q."
  (slot-value q 'size))

(defmethod coll-empty? ((q ordered-list-queue) &key &allow-other-keys)
  "Returns t if the collection Q contains no elements, nil otherwise"
  (eql 0 (slot-value q 'size)))

(defmethod coll-map ((q ordered-list-queue)(f function) &key &allow-other-keys)
  "Applies a function F to each element in the collection Q.
   The function is required to take one argument - an element."
  (mapcar f (slot-value q 'head)))

(defmethod coll-tolist ((q ordered-list-queue) &key (copy t) &allow-other-keys)
  "Returns a list. Returns nil if the list is empty.
   When COPY is nil, the list returned is the one that backs `Q`. 
   Otherwise, when COPY is t (default), the list returned is a copy."
  (if copy
	  (copy-list (slot-value q 'head))
	  (slot-value q 'head)))

(defmethod coll-toarray ((q ordered-list-queue) &key (adjustable nil)(fill-pointer nil)(displaced-to nil)(displaced-index-offset 0) &allow-other-keys)
  "Returns an array of elements in the collection COLL."
  (make-array (coll-size q) 
			  :initial-contents (coll-tolist q :copy nil)
			  :adjustable adjustable
			  :fill-pointer fill-pointer
			  :displaced-to displaced-to
			  :displaced-index-offset displaced-index-offset))

(defmethod queue-remove! ((q ordered-list-queue) &key &allow-other-keys)
  "=> A values structure. The first is element at the head of `Q`.
 The second is `T` iff there was an element at the head, `NIL` else.

Removes the head of `Q`."
  (with-slots (head size) q
	(if (null head)
		(values nil nil)
		(progn
		  (decf size)
		  (values (pop head) t)))))

(defmethod queue-peek ((q ordered-list-queue) &key &allow-other-keys)
  "=> A values structure. The first is element at the head of `Q`.
 The second is `T` iff there was an element at the head, `NIL` else.

Does not modify `Q`."
  (with-slots (head) q
	(if (null head)
		(values nil nil)
		(progn
		  (values (car head) t)))))

(defmethod print-object ((list ordered-list-queue) stream)
  (if (ordered-list-queue-head list)
	  (format stream "#LIST-QUEUE~a" (ordered-list-queue-head list))
	  (format stream "#LIST-QUEUE()")))

(defmethod make-coll-iterator ((list ordered-list-queue) &key &allow-other-keys)
  "Creates an iterator object that maintains its state over the LIST.
  This method should throw an error if it is not supported."
  (error 'unsupported-function))

(check-implementation 'ordered-list-queue)
