;;;; Copyright (c) 2013 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   David C. Wang

(in-package #:clos-utils)

(defvar *abstract-classes* (make-hash-table)
  "A hashtable that maps abstract-class symbol names to a list of abstract methods.")

(defclass abstract-method ()
	((name
	  :initarg :name
	  :documentation "The symbol name of the method")
	 (qualifiers
	  :initarg :qualifiers
	  :documentation "A list of qualifiers for this method")
	 (spec-lambda-list
	  :initarg :spec-lambda-list
	  :documentation "The specialized lambda list of this method")
	 (documentation
	  :initarg :documentation
	  :documentation "The documentation for this abstract-method")
	 (has-forms?
	  :initarg :has-forms?
	  :documentation "A boolean value indicating whether this method had a body.")))

(defmethod documentation ((abstract-method abstract-method) dummy)
  (declare (ignore dummy))
  (with-slots (documentation) abstract-method documentation))

(defun is-keyword? (arg)
  "Returns t if the argument ARG is eq to a keyword symbol that can appear in a lambda list."
  (some (lambda (keyword) (eq arg keyword)) '(&allow-other-keys &key &rest &aux &optional)))

(defun get-specializers (spec-lambda-list)
  (let ((specializers nil))
	(loop for arg in spec-lambda-list do
		 (cond ((is-keyword? arg)
				(return nil))
			   ((listp arg)
				(setf specializers (nconc specializers `(,(cadr arg)))))
			   (:otherwise
				(setf specializers (nconc specializers `(,t))))
			   ))
	specializers))

(defmethod equal-methods ((method1 standard-method)(method2 standard-method))
  (and (eql (clos:method-generic-function method1) (clos:method-generic-function method1))
	   (equal (clos:method-specializers method1) (clos:method-specializers method2))) )

(defmethod equal-methods ((method1 abstract-method)(method2 abstract-method))
  (with-slots ((name1 name) (qual1 qualifiers) (lambda1 spec-lambda-list)) method1
	(with-slots ((name2 name) (qual2 qualifiers) (lambda2 spec-lambda-list)) method2
	  (and (equal name1 name2)
		   (equal qual1 qual2)
		   (equal (get-specializers lambda1) (get-specializers lambda2)))
	  )))

(defun parse-defmethod (defmethod-args &key (allow-doc-at-end nil))
  "Given a defmethod form or just the arguments to a defmethod form, returns the arguments in a categorized manner."
  (let ((mode 0)
		(num-args (length defmethod-args))
		(method-name nil)
		(qualifiers nil)
		(spec-lambda-list nil)
		(declarations nil)
		(documentation nil)
		(forms nil))
	(loop 
	   for arg in defmethod-args
	   for i from 0 to (1- num-args) do
		 (setf mode 
			   (cond ((and (eq mode 0) (eql arg 'defmethod))
					  1)
					 ((eq mode 0) ;; and (not (eql arg 'defmethod))
					  (setf method-name arg)
					  2)
					 ((eq mode 1)
					  (setf method-name arg)
					  2)
					 ((and (eq mode 2) (not (listp arg)))
					  (push arg qualifiers)
					  2)
					 ((and (eq mode 2) (listp arg))
					  (setf spec-lambda-list arg)
					  3)
					 ((and (eq mode 3) (stringp arg))
					  (if (and (eq i (1- num-args)) (not allow-doc-at-end))
						  (push arg forms)
						  (setf documentation arg))
					  4)
					 ((and (or (eq mode 3) (eq mode 4)) (listp arg) (eql (car arg) 'declare))
					  (push arg declarations)
					  mode)
					 (:otherwise
					  (push arg forms)
					  5)
					 )))
	(values method-name qualifiers spec-lambda-list declarations documentation forms)))

(defun make-lambda-list (specialized-lambda-list)
  "Returns a lambda-list from a SPECIALIZED-LAMBDA-LIST"
  (let ((keyword-detected nil))
	(mapcar (lambda (arg)
			  (unless keyword-detected
				(setf keyword-detected (is-keyword? arg)))
			  (cond (keyword-detected arg)
					((listp arg) (car arg))
					(:otherwise arg)))
			specialized-lambda-list)))

(defun make-call-list (specialized-lambda-list)
  "Returns a list of variables names from a SPECIALIZED-LAMBDA-LIST"
  (let ((keyword nil))
	(mapcan (lambda (arg)
			  (cond ((is-keyword? arg)
					 (setf keyword arg) nil)
					((eq keyword '&aux)
					 (if (listp arg) `(,(intern (car arg) 'keyword) ,(car arg)) `(:,arg ,arg)))
					((eq keyword '&allow-other-keys)
					 nil)
					((eq keyword '&key)
					 (if (listp arg) 
						 (if (listp (car arg))
							 (car arg)
							 `(,(intern (car arg) 'keyword) ,(car arg)))
						  `(:,arg ,arg)))
					((eq keyword '&rest)
					 `(,arg))
					((eq keyword '&optional)
					 (if (listp arg) `(,(car arg)) `(,arg)))
					(:otherwise
					 (if (listp arg) `(,(car arg)) `(,arg))) ))
			specialized-lambda-list)))


(defclass abstract-generic-function (standard-generic-function)
  ()
  #+allegro
  (:metaclass aclmop:funcallable-standard-class))

(defmethod no-applicable-method ((generic-function abstract-generic-function) &rest function-arguments)
  (error 'unsupported-function))

(defmethod no-next-method ((generic-function abstract-generic-function)(method standard-method) &rest args)
  (error 'unsupported-function))

(defmacro defmethod-abstract (&rest rest)
  "Creates a method that must be implemented by all decendants of this class.
   Behaves exactly like defmethod except for the following ways:
   1. It IS okay to have a documentation string be the last argument to defmethod.
   2. Interfaces without forms (a body of code) will automatically use the form
      (error 'unsupported-function).
   3. This macro 'registers' the existance of this method as an interface."
  (multiple-value-bind (method-name qualifiers spec-lambda-list declarations documentation forms)
	  (parse-defmethod rest :allow-doc-at-end t)
	(let ((list-documentation (if documentation (list documentation) nil))
		  (lambda-list (make-lambda-list spec-lambda-list))
		  (abstract-class-name (cadar spec-lambda-list))
		  (has-forms? (not (null forms)))
		  (method (gensym "METHOD-"))
		  (condition-var (gensym "CONDITION-VAR-")))
	  `(handler-case
		   (progn
			 (unless (fboundp ',method-name)
			   (defgeneric ,method-name ,lambda-list
				 #+allegro
				 (:generic-function-class abstract-generic-function)))
			 (let ((,method (defmethod ,method-name ,@qualifiers ,spec-lambda-list ,@declarations ,@list-documentation ,@forms)))
			   (pushnew (make-instance 'abstract-method
			   						   :name ',method-name
			   						   :qualifiers ',qualifiers
			   						   :spec-lambda-list ',spec-lambda-list
			   						   :documentation ,documentation
			   						   :has-forms? ,has-forms?)
			   			(gethash ',abstract-class-name *abstract-classes*)
			   			:test #'equal-methods)
			   ,(if has-forms?
			   		method
					`(progn (remove-method (symbol-function ',method-name) ,method) nil))
			   ))
		 (condition (,condition-var) (error ,condition-var)))
	  )))


(defun create-abstract-methods (generic-function)
  "Creates all of the abstract-methods for this generic function."
  (let ((created-methods nil))
	(loop for abstract-class being the hash-values in *abstract-classes* do
		 (loop for abstract-method in abstract-class do
			  (with-slots ((method-name name) qualifiers spec-lambda-list documentation has-forms?) abstract-method
				(when (and (eq generic-function (symbol-function method-name))
						   (not has-forms?))
				  ;; create the methods
				  (push
				   (eval `(defmethod ,method-name ,@qualifiers ,spec-lambda-list ,documentation (error 'unsupported-function)))
				   created-methods)  ))
			  )) ;; double loops
	created-methods))

(defun remove-methods (generic-function methods)
  "Removes the given methods from the generic-function."
  (loop for method in methods do
	   (remove-method generic-function method)))

(defun find-applicable-methods (generic-function qualifiers classes) 
  "Returns two values. The first is a list of methods of the 
   GENERIC-FUNCTION that can be called with the list of CLASSES.
   The second is the method that is an exact match, nil otherwise"
  (let ((specific-method (find-method generic-function qualifiers classes nil))
		(applicable-methods 
		 #+allegro
		  (reverse (clos:compute-applicable-methods-using-classes generic-function classes))
		  #-allegro
		  nil))
	(cond (applicable-methods 
		   (values applicable-methods specific-method))
		  (specific-method
		   (values `(,specific-method) specific-method))
		  (:otherwise
		   (values nil nil)))))


(defun check-implementation (this-class &optional (abstract-class this-class))
  "Checks that THIS-CLASS implements the ABSTRACT-CLASS.
   THIS-CLASS and ABSTRACT-CLASS must both be symbols."
  ;; Perform the recursive implementation checking.
  ;; Skip the recursion if this class is of type base-class standard-object
  (when (and (not (eql abstract-class 'standard-object)))
	(clos-utils-format *info* "Checking if class ~a implements abstract-class ~a ...~%" this-class abstract-class)
	(multiple-value-bind (methods) (gethash abstract-class *abstract-classes* nil)
	  ;; check this class method's
	  (loop for method in methods do
		   (with-slots (name qualifiers spec-lambda-list) method
			 (let* ((generic-function (symbol-function name))
					(specializers (get-specializers spec-lambda-list))
					;; replace the abstract class with the current class
					(spec-sym (cons this-class (cdr specializers)))
					;; replace specializer symbols with their instances
					(spec-class (mapcar (lambda (x) (find-class x)) spec-sym))
					;; the specific method for this-class
					(specific-method nil)
					;; applicable methods for this-class
					(applicable-methods nil)
					(applicable-method-class t))
			   (multiple-value-setq (applicable-methods specific-method)
				 (find-applicable-methods generic-function qualifiers spec-class))
			   (when applicable-methods
				 (setf applicable-method-class (class-name (car (clos:method-specializers (car applicable-methods))))))
			   (unless specific-method
				 ;; when this class does not have a specific method defined for it,
				 ;; check if there is another applicable method that has a form.
				 (if (not (eql applicable-method-class t))
					 (clos-utils-format *info* "~a inherits method ~a of abstract class ~a from class ~a.~%"
							 this-class name (car specializers) applicable-method-class)
					 #+allegro
					 (clos-utils-format *error* "~a does not implement method ~a of abstract class ~a.~%" 
							 this-class name (car specializers))
					 #-allegro
					 (clos-utils-format *info* "~a does not directly implement method ~a of abstract class ~a.~%" 
							 this-class name (car specializers)) 
					 )))
			 )))
	;; loop over the superclasses and check if this-class implements the superclasses
	(loop for superclass in (clos:class-direct-superclasses (find-class abstract-class)) do
		 (check-implementation this-class (class-name superclass))) ))

;; (defun check-implementation2 (this-class &optional (abstract-class this-class))
;;   (let ((abstract-method-table (compute-abstract-method-table))
;; 		(inheritance-list (compute-inheritance-list this-class)))
;; 	;; loop over each of the interface methods
;; 	(loop for abstract-method-name being the hash-keys in abstract-method-table 
;; 	   using (hash-value supporting-classes) do
;; 		 ;; check that this method is required for this class
;; 		 (when (some (lambda (x) (find x inheritance-list)) supporting-classes)
;; 		   ;; check if this method is implemented
		   
;; 		   (multiple-value-setq (applicable-methods specific-method)
;; 			 (find-applicable-methods generic-function qualifiers spec-class))

;; 			 )
;; 		 )
	
;; 	))


#+allegro
(defun compute-inheritance-table ()
  "Returns a hashtable, in which each key  is a symbol name of a class,
   and the values are a list of superclasses for that class."
  (let ((inheritance-table (make-hash-table)))
	(loop for abstract-class-name being the hash-keys in *abstract-classes* do
		 (setf (gethash abstract-class-name inheritance-table) 
			   (mapcar #'class-name (clos:class-direct-superclasses (find-class abstract-class-name)))))
	inheritance-table))

#+allegro
(defun compute-inheritance-list-helper (curr-class-name inheritance-list)
		   (pushnew curr-class-name inheritance-list)
		   (loop for next-class-name in
				(mapcar #'class-name (clos:class-direct-superclasses (find-class curr-class-name))) do
				(setf inheritance-list (compute-inheritance-list-helper next-class-name inheritance-list)))
		   inheritance-list)

#+allegro
(defun compute-inheritance-list (class-name)
  (reverse (compute-inheritance-list-helper class-name '())) )

(defun compute-abstract-method-table ()
  "Returns a hashtable, in which the keys are the symbol names of methods,
   and the values are a list of classes which have that abstract-method 
   in no particular order."
  (let ((method-table (make-hash-table)))
	(loop for abstract-class-name being the hash-keys in *abstract-classes* 
	   using (hash-value abstract-methods) do
		 (loop for abstract-method in abstract-methods do
			  (pushnew abstract-class-name
					   (gethash (with-slots (name) abstract-method name) method-table)  )))
	method-table))