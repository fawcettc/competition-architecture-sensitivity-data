;;;; Copyright (c) 2011 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   David Wang

(in-package #:abstract-data-types)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; SET
;; This class provides the interface specification for
;; data structures that store collections of elements 
;; in which each element of the collection is unique.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass adt-set (adt-collection)
  ())

(defmethod-abstract coll-add! ((set adt-set)(element t) &key (replace t) &allow-other-keys)
  "Adds ELEMENT to the SET.
   Returns t if a change was made to the set. i.e.: 
   If the SET did not originally contain the ELEMENT, t will be returned.
   If the SET contained ELEMENT and REPLACE is t, t will be returned.
   Otherwise, no change will be made to the set, and nil will be returned."
  (declare (ignore replace)))

(defmethod-abstract coll-remove! ((set adt-set)(element t) &key &allow-other-keys)
  "Removes ELEMENT from the SET.
   Returns t if the collection was modified, nil otherwise."
  (declare (ignore key-p)))

(defmethod-abstract coll-clear! ((set adt-set) &key &allow-other-keys)
  "Clears the SET of all contents.")

(defmethod-abstract coll-contains? ((set adt-set)(element t) &key &allow-other-keys)
  "Returns t if the SET contains ELEMENT and nil if it does not."
  (declare (ignore key-p)))

(defmethod-abstract coll-size ((set adt-set) &key &allow-other-keys) 
  "Returns the size of the SET.")

(defmethod-abstract coll-empty? ((set adt-set) &key &allow-other-keys)
  "Returns t if the SET contains no elements, nil otherwise.")

(defmethod-abstract coll-map ((set adt-set)(f function) &key &allow-other-keys)
  "Applies a function F to each element in the SET.
   The function is required to take one argument - a set element.")

(defmethod-abstract coll-tolist ((set adt-set) &key &allow-other-keys)
  "Returns a list of the elements in the SET.")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ITERATOR
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass adt-set-iterator (adt-collection)
  ())

(defmethod-abstract make-coll-iterator ((set adt-set) &key &allow-other-keys)
  "Creates an iterator object that maintains its state over the SET.
  This method should throw an error if it is not supported.")

(defmethod-abstract coll-it-next ((set-iterator adt-set-iterator) &key &allow-other-keys)
  "Returns the next element in the collection used to create the SET-ITERATOR. 
   If there are no more elements, nil is returned.
   This method should throw an error if it is not supported.")

(defmethod-abstract coll-it-hasnext ((set-iterator adt-set-iterator) &key &allow-other-keys)
  "Returns t if the collection used to create the SET-ITERATOR has another element.
  This method should throw an error if it is not supported.") 
