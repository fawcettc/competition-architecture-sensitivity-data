;;;; Copyright (c) 2011 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   David C. Wang

(in-package #:abstract-data-types)

(mtk-utils:deftest-suite :test-dictionary)

(defclass test-dictionary-element ()
  ((value1
	:type t
	:accessor test-dictionary-element-value1
	:initarg :value1
	:initform nil
	:documentation "")
   (value2
	:type t
	:accessor test-dictionary-element-value2
	:initarg :value2
	:initform nil
	:documentation "")
   (value3
	:type t
	:accessor test-dictionary-element-value3
	:initarg :value3
	:initform nil
	:documentation "")
   ))


(mtk-utils:deftest (test-dictionary-1 :test-dictionary) ()
  (let* ((column-defs `((:name :value1 :key-fn ,#'test-dictionary-element-value1 :test ,#'equal)
						(:name :value2 :key-fn ,#'test-dictionary-element-value2 :test ,#'equal)
						(:name :value3 :key-fn ,#'test-dictionary-element-value3 :test ,#'equal)
						))
		 (dictionary (make-dictionary column-defs)))
	;; Tests that there should be 3 columns in the hash table
	(mtk-utils:test (hash-table-count (dictionary-col-hash-table dictionary))
					3
					:test #'equal
					:fail-info "Failed column hash table count. There should be three columns in this dictionary.")
	;; Tests adding a few items to the hash table
	(dictionary-add dictionary (make-instance 'test-dictionary-element :value1 1 :value2 1 :value3 1))
	(dictionary-add dictionary (make-instance 'test-dictionary-element :value1 2 :value2 2 :value3 2))
	(dictionary-add dictionary (make-instance 'test-dictionary-element :value1 3 :value2 3 :value3 3))
	(dictionary-add dictionary (make-instance 'test-dictionary-element :value1 4 :value2 4 :value3 4))
	;; Tests removing an item from the hash table
	(mtk-utils:test (dictionary-remove dictionary :value1 2)
					t
					:test #'equal
					:fail-info "Failed to remove an item from the dictionary.")
	;; Tests removing an items from the hash table that does not exist.
	(mtk-utils:test (dictionary-remove dictionary :value3 10)
					nil
					:test #'equal
					:fail-info "Removed an item from the dictionary that does not exist.")
	))