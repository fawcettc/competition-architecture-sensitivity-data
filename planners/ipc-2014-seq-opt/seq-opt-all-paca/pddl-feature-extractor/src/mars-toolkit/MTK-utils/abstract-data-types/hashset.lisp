;;;; Copyright (c) 2011 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   David Wang

(in-package #:abstract-data-types)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; HASHSET
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass hashset (adt-set)
  ((contents
	:type hashmap
	:initform (make-hash-table)
	:initarg :contents
	:accessor hs-contents
	:documentation "Map to store the key and element.")
   (key-function
	:type function
	:initform (lambda (x) x)
	:initarg :key-function
	:accessor hs-key-function
	:documentation "Computes the key, given the element, that will be used to test for set membership.")))

(defun make-hashset (&key (initial-contents '())
					 (key-function (lambda (x) x)) 
					 (test 'eql) (size 67)
					 (rehash-size 1.2) (rehash-threshold 0.6407767)
					 )
  (let ((set (make-instance 
			  'hashset
			  :contents (make-hashmap :test test :size size 
									  :rehash-size rehash-size
									  :rehash-threshold rehash-threshold)
			  :key-function key-function)))
	(coll-add-all! set initial-contents)
	set))

(defmethod-alias hs-copy coll-copy ((set hashset) &key &allow-other-keys)
  "Returns a copy of this SET."
  (with-slots (key-function contents) set
	(let ((copy (make-hashset :key-function key-function 
							  :test (hash-table-test contents) 
							  :size (hash-table-size contents) 
							  :rehash-size (hash-table-rehash-size contents) 
							  :rehash-threshold (hash-table-rehash-threshold contents))))
	  (coll-add-all! copy set)
	  copy)))

(defmethod-alias hs-add! coll-add! ((set hashset)(element t) &key (replace t)(use-key-function t) &allow-other-keys)
  "Adds ELEMENT to the SET.
   Returns t if a change was made to the set. i.e.:
   If the SET did not originally contain the ELEMENT, t will be returned.
   If the SET contained ELEMENT and REPLACE is t, t will be returned.
   Otherwise, no change will be made to the set, and nil will be returned.
   If USE-KEY-FUNCTION is nil, the ELEMENT is used to test for set membership.
   If USE-KEY-FUNCTION is t (default), the key computed by applying the 
   key-function to the ELEMENT is used to test for set membership."
  (with-slots (contents key-function) set
	(let ((key (if use-key-function (funcall key-function element) element)))
	  (multiple-value-bind (success)
		  (hm-add! contents key element :replace replace)
		  success))))

(defmethod-alias hs-get (coll-get) ((set hashset)(element t) &key (use-key-function t)(default nil) &allow-other-keys)
  "Returns the element keyed by ELEMENT in the SET, or DEFAULT if no such key exists.
   Note that this function/method is not specified nor required by the set interface. 
   Implementations of coll-get on sets may return implementation specific values."
  (with-slots (contents key-function) set
	(let ((key (if use-key-function (funcall key-function element) element)))
	  (multiple-value-bind (orig-element)
			(hm-get contents key :default default)
		orig-element))))

(defmethod-alias hs-remove! coll-remove! ((set hashset)(element t) &key (use-key-function t)(default nil) &allow-other-keys)
  "Removes ELEMENT from the SET.
   Returns two values. The first value is t if the collection was modified, nil otherwise.
   The second value will be the element keyed by ELEMENT, or DEFAULT if no such key exists."
  (with-slots (contents key-function) set
	(let ((key (if use-key-function (funcall key-function element) element)))
	  (multiple-value-bind (orig-element success) 
		  (hm-remove! contents key :default default)
		(values success orig-element)))))

(defmethod-alias hs-clear! coll-clear! ((set hashset) &key &allow-other-keys)
  "Clears the SET of all contents."
  (hm-clear! (hs-contents set)))

(defmethod-alias hs-contains? coll-contains? ((set hashset)(element t) &key (use-key-function t) &allow-other-keys)
  "Returns t if the SET contains ELEMENT
   and nil if it does not.
   If USE-KEY-FUNCTION is nil, the ELEMENT is used to test for set membership.
   If USE-KEY-FUNCTION is t (default), the key computed by applying the 
   key-function to the ELEMENT is used to test for set membership."
  (with-slots (contents key-function) set
	(let ((key (if use-key-function (funcall key-function element) element)))
	  (hm-contains? contents key))))

(defmethod-alias hs-size coll-size ((set hashset) &key &allow-other-keys)
  "Returns the size of the SET."
  (hm-size (hs-contents set)))

(defmethod-alias hs-empty? coll-empty? ((set hashset) &key &allow-other-keys)
  "Returns t if the SET contains no elements, nil otherwise."
  (hm-empty? (hs-contents set)))

(defmethod-alias hs-map coll-map ((set hashset)(f function) &key &allow-other-keys)
  "Applies a function F to each element in the SET.
   The function is required to take one argument - a set element."
  (hm-map (hs-contents set) 
		  (lambda (key element) 
			(declare (ignore key))
			(funcall f element))))

(defmethod-alias hs-tolist coll-tolist ((set hashset) &key &allow-other-keys)
   "Returns a list of the elements in the SET."
  (let ((hslist (make-smart-list)))
	(hm-map (hs-contents set)
			(lambda (key element) 
			  (declare (ignore key)) 
			  (slist-push-end element hslist)))
	(slist-head hslist)))

(defmethod print-object ((set hashset) stream)
  (format stream "#HASHSET(")
  (let ((first-loop t))
	(hs-map set
			(lambda (element) 
			  (unless first-loop 
				(format stream ","))
			  (format stream "~a" element)
			  (setf first-loop nil)))
	)
  (format stream ")"))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ITERATOR
;; Not supported by hashset class.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmethod make-coll-iterator ((set hashset) &key &allow-other-keys)
  "Creates an iterator object that maintains its state over the SET.
  This method should throw an error if it is not supported."
  (error 'unsupported-function))





(check-implementation 'hashset)