;;;; Copyright (c) 2011 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   David Wang

(in-package #:abstract-data-types)

(defun test-tolist-and-size (list elements fail-msg)
  (mtk-utils:test (coll-tolist list)
				  elements
				  :test #'equal
				  :fail-info (format nil "~a Not all elements added." fail-msg))
  (mtk-utils:test (coll-size list)
				  (length elements)
				  :test #'equal
				  :fail-info (format nil "~a Size is not correct." fail-msg)))

(defun test-list-add (make-list-fn)
  "Tests the list interface coll-add!."
  (let ((list (funcall make-list-fn))
		(elements '(a b c d e f g h)))
	;; add a bunch of symbol pairs
	(loop for elt in elements do
		 (coll-add! list elt))
	;; Test all elements are present
	(test-tolist-and-size list elements "Failed add! on list collection.")
	))

(defun test-list-add-all-list (make-list-fn)
  "Tests the list interface coll-add-all! for lists."
  (let ((list (funcall make-list-fn))
		(elements '(a b c d e f g h)))
	;; Add list elements
	(coll-add-all! list elements)
	(test-tolist-and-size list elements "Failed add-all! for lists on list collection.")))

(defun test-list-add-all-array (make-list-fn)
  "Tests the list interface coll-add-all! for arrays."
  (let ((list (funcall make-list-fn))
		(elements '(a b c d e f g h)))
	;; Add array elements
	(coll-add-all! list (make-array (length list) :initial-contents elements))
	(test-tolist-and-size list elements "Failed add-all! for arrays on list collection.")))

(defun test-list-get (make-list-fn)
  "Tests the list interface test-coll-get."
  ;; Test the set for adding and getting a single element
  (flet ((make-info (msg) (format nil "Failed get on list collection. ~a" msg)))
	(let ((list (funcall make-list-fn)))
	  ;; add a single element
	  (coll-add! list 'z)
	  ;; test that the single element can be retrieved
	  (mtk-utils:test (coll-get list 0) 'z :test #'eql :fail-info (make-info "Did not get single element.")))
	;; Test the set for sequential adding of keys
	(let ((list (funcall make-list-fn))
		  (elements '(a b c d e f g h)))
	  ;; add a bunch of symbol pairs
	  (loop for elt in elements do (coll-add! list elt))
	  ;; Test that we can get all elements.
	  (loop 
		 for elt in elements
		 for i from 0 to (1- (length elements)) do
		   (mtk-utils:test (coll-get list i)
						   elt
						   :test #'equal
						   :fail-info (make-info "Some elements not gotten.")))
	  ;; Test indexing error
	  (mtk-utils:test-error (coll-get list -1)
							:condition-type 'index-out-of-bounds
							:fail-info (make-info "Index out of bounds error not received for -1 index."))
	  (loop for i from 0 to (1- (length elements)) do
		   (mtk-utils:test-no-error (coll-get list i)
									:fail-info (make-info "Index out of bounds error received when index were in bounds.")))
	  (mtk-utils:test-error (coll-get list 8)
							:condition-type 'index-out-of-bounds
							:fail-info (make-info "Index out of bounds error not received for index 8 for 8 element list."))
	  )
	(let ((list (funcall make-list-fn)))
	  ;; Test indexing error on empty list
	  (mtk-utils:test-error (coll-get list -1)
							:condition-type 'index-out-of-bounds
							:fail-info (make-info "Index out of bounds error not received for index -1 for empty list."))
	  (mtk-utils:test-error (coll-get list 0)
							:condition-type 'index-out-of-bounds
							:fail-info (make-info "Index out of bounds error not received for index 0 for empty list."))
	  )
	(let ((list (funcall make-list-fn)))
	  ;; Test ignore-index-error parameter.
	  (mtk-utils:test-no-error (coll-get list -1 :ignore-index-error t)
							   :fail-info (make-info "An index-out-of-bounds error was thrown despite being suppressed."))
	  (mtk-utils:test-no-error (coll-get list 0 :ignore-index-error t)
							   :fail-info (make-info "An index-out-of-bounds error was thrown despite being suppressed."))
	  ;; Test ignore-index-error and default return value.
	  (mtk-utils:test (coll-get list -1 :ignore-index-error t :default 'junk)
					  'junk
					  :test #'eql
					  :fail-info (make-info "The default argument was not returned when index error was ignored."))
	  )))


(defun test-list-insert (make-list-fn)
  "Tests the list interface coll-insert."
  ;; Test the set for sequential adding of keys
  (let ((list (funcall make-list-fn))
		(cases '((0 a (a)) 
				 (0 b (b a))
				 (1 c (b c a))
				 (3 d (b c a d))
				 (3 b (b c a b d))
				 )))
	;; Test element-by-element insertion
	(loop for case in cases do
		 (mtk-utils:test-no-error (coll-insert! list (first case) (second case))
								  :fail-info (format nil "Failed insert test. Add ~a at index ~a resulted in error.~%" (second case) (first case)))
		 (mtk-utils:test (coll-tolist list)
						 (third case)
						 :test #'equal
						 :fail-info (format nil "Failed insert test. Add ~a at index ~a.~%" (second case) (first case))))
	))

(defun test-list-remove-from (make-list-fn)
  "Tests the list interface coll-remove-from."
  ;; Test the set for sequential adding of keys
  (let ((list (funcall make-list-fn))
		(elements '(a b c d e f g h))
		(cases '((7 (t h) (a b c d e f g))
				 (0 (t a) (b c d e f g))
				 (0 (t b) (c d e f g))
				 (2 (t e) (c d f g))
				 (2 (t f) (c d g))
				 )))
	;; add a bunch of symbol pairs 
	(loop for elt in elements do
		 (coll-add! list elt))
	;; Test element-by-element removal
	(loop for case in cases do
		 ;;(format t "~a ~%" (multiple-value-list (coll-remove-from! list (first case))))
		 (mtk-utils:test (multiple-value-list (coll-remove-from! list (first case)))
		 				 (second case)
		 				 ;;:multiple-values t
		 				 :test #'equal
		 				 :fail-info (format nil "Failed removal test. Item removed from index ~a did not result in correct return values." (first case)))
		 (mtk-utils:test (coll-tolist list)
						 (third case)
						 :test #'equal
						 :fail-info (format nil "Failed removal test. Removal from index ~a did not result in list ~a." (first case) (third case))))
	))

(defun test-list-remove-first (make-list-fn)
  "Tests the list interface coll-remove-first."
  ;; Test the set for sequential adding of keys
  (let ((list (funcall make-list-fn))
		(elements '(a b c d e f g h)))
	;; add a bunch of symbol pairs 
	(loop for elt in elements do
		 (coll-add! list elt))
	;; Test element-by-element removal
	(loop for i from 0 to (- (length elements) 1) do
		 (mtk-utils:test (multiple-value-list (coll-remove-first! list))
		 				 `(t ,(nth i elements))
		 				 ;;:multiple-values t
		 				 :test #'equal
		 				 :fail-info (format nil "Failed remove first test. ~a removal did not result in correct return values." i))
		 ;;(format t "~a~%" (slist-head list))
		 (mtk-utils:test (coll-tolist list)
						 (nthcdr (+ i 1) elements)
						 :test #'equal
						 :fail-info (format nil "Failed remove first test. ~a removal did not result in list ~a." i (nthcdr (+ i 1) elements))))
	;; Test remove first with error.
	(mtk-utils:test-error (multiple-value-list (coll-remove-first! list))
					 :condition-type 'index-out-of-bounds
					 :fail-info "Failed remove first test. Remove first from empty list did not result in index error.")
	;; Test remove first with error suppressed.
	(mtk-utils:test-no-error (multiple-value-list (coll-remove-first! list :ignore-index-error t))
							 :fail-info "Failed remove first test. Remove first from empty resulted in index error despite error being suppressed.")
	))


(defun test-list-remove-last (make-list-fn)
  "Tests the list interface coll-remove-last."
  ;; Test the set for sequential adding of keys
  (let ((list (funcall make-list-fn))
		(elements '(a b c d e f g h)))
	;; add a bunch of symbol pairs 
	(loop for elt in elements do
		 (coll-add! list elt))
	;; Test element-by-element removal
	(let ((reverse-elements (reverse elements)))
	  (loop for i from 0 to (- (length elements) 1) do
		   (mtk-utils:test (multiple-value-list (coll-remove-last! list))
						   `(t ,(nth i reverse-elements))
						   ;;:multiple-values t
						   :test #'equal
						   :fail-info (format nil "Failed remove last test. ~a removal did not result in correct return values." i))
		 ;;(format t "~a ~a ~%" (slist-head list)  (reverse (nthcdr (+ i 1) (reverse elements))))
		   (mtk-utils:test (coll-tolist list)
						   (reverse (nthcdr (+ i 1) reverse-elements))
						   :test #'equal
						   :fail-info (format nil "Failed remove last test. ~a removal did not result in list ~a." i (reverse (nthcdr (+ i 1) reverse-elements)) ))))

	;; Test remove first with error.
	(mtk-utils:test-error (multiple-value-list (coll-remove-last! list))
					 :condition-type 'index-out-of-bounds
					 :fail-info "Failed remove last test. Remove last from empty list did not result in index error.")
	;; Test remove first with error suppressed.
	(mtk-utils:test-no-error (multiple-value-list (coll-remove-last! list :ignore-index-error t))
							 :fail-info "Failed remove last test. Remove last from empty resulted in index error despite error being suppressed.")
	))

(defun test-list-clear (make-list-fn)
  (let ((list (funcall make-list-fn))
		(elements '(1 2 3 4 5 6 7 8 9 10)))
	;; Add a single list element
	(coll-add! list 'a)
	;; Clear the list
	(coll-clear! list)
	(test-tolist-and-size list '() "Failed to clear list during add-all! for lists on list collection.")
	;; Add list elements
	(coll-add-all! list elements)
	(test-tolist-and-size list elements "Failed add-all! for lists on list collection.")
	;; Clear the list
	(coll-clear! list)
	(test-tolist-and-size list '() "Failed to clear list during add-all! for lists on list collection.")))