;;;; Copyright (c) 2013 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   David Wang

(in-package #:abstract-data-types)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; COLLECTION
;; This class provides the interface specification for
;; data structures that store collections of elements.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass adt-collection ()
  ())

(defmethod-abstract coll-copy ((coll adt-collection) &key &allow-other-keys)
  "Returns a copy of this collection")

(defmethod-abstract coll-add! ((coll adt-collection)(element t) &key &allow-other-keys)
  "Adds ELEMENT to the collection COLL. 
   Returns t if the collection was modified, nil otherwise.")

(defmethod-abstract coll-add-all! ((coll adt-collection)(elements list) &rest rest &key &allow-other-keys)
  "Appends all of the ELEMENTS to the end of the COLL.
   Returns t if the collection was modified, nil otherwise. 
   Complexity: O(1)*<length of elements>"
  (let ((modified? nil))
	(loop for element in elements do
		 (setf modified? (or (apply #'coll-add! coll element rest) modified?)))
	modified?))

(defmethod-abstract coll-add-all! ((coll adt-collection)(elements adt-collection) &rest rest &key &allow-other-keys)
  "Appends all of the ELEMENTS to the end of the COLL.
   Returns t if the collection was modified, nil otherwise. 
   Complexity: O(1)*<length of elements>"
  (let ((modified? nil))
	(coll-map elements 
			  (lambda (element) (setf modified? (or (apply #'coll-add! coll element rest) modified?))))
	modified?))

(defmethod-abstract coll-remove! ((coll adt-collection)(element t) &key &allow-other-keys)
  "Removes ELEMENT from the collection COLL. 
   Returns t if the collection was modified, nil otherwise.")

(defmethod-abstract coll-remove-all! ((coll adt-collection)(elements list) &rest rest &key (all t) &allow-other-keys)
  "Removes ELEMENTS from the COLL.
   If ALL is nil, COLL-REMOVE! will be called once on the collection for each element in ELEMENTS.
   If ALL is t, COLL-REMOVE! will be called until it returns nil for each element in ELEMENTS.
   Returns t if the collection was modified, nil otherwise. 
   Complexity: O(1)*<length of elements>"
  (let ((modified? nil))
	(loop for element in elements do
		 (if all
			 (loop while (apply #'coll-remove! coll element rest) do
				  (setf modified? t))
			 (setf modified? (or (apply #'coll-remove! coll element rest) modified?))
			 ))
	modified?))

(defmethod-abstract coll-remove-all! ((coll adt-collection)(elements adt-collection) &rest rest &key (all t) &allow-other-keys)
  "Removes ELEMENTS from the COLL.
   If ALL is nil, COLL-REMOVE! will be called once on the collection for each element in ELEMENTS.
   If ALL is t, COLL-REMOVE! will be called until it returns nil for each element in ELEMENTS.
   Returns t if the collection was modified, nil otherwise. 
   Complexity: O(1)*<length of elements>"
  (let ((modified? nil))
	(coll-map elements 
			  (lambda (element) 
				(if all
					(loop while (apply #'coll-remove! coll element rest) do
						 (setf modified? t))
					(setf modified? (or (apply #'coll-remove! coll element rest) modified?)))
				))
	modified?))

(defmethod-abstract coll-clear! ((coll adt-collection) &key &allow-other-keys)
  "Clears the collection COLL of all contents.")

(defmethod-abstract coll-contains? ((coll adt-collection)(element t) &key &allow-other-keys)
  "Returns t if collection COLL contains ELEMENT
   and nil if it does not.")

(defmethod-abstract coll-contains-all? ((coll adt-collection)(elements list) &rest rest &key &allow-other-keys)
  "Returns t if collection COLL contains all of the elements of ELEMENTS
   and nil if it does not."
  (block coll-contains-all-block
	(mapcar #'(lambda (element)
				(unless (apply #'coll-contains? coll element rest)
				  (return-from coll-contains-all-block nil))) 
			elements)
	t))

(defmethod-abstract coll-contains-all? ((coll adt-collection)(elements adt-collection) &rest rest &key &allow-other-keys)
  "Returns t if collection COLL contains all of the elements of ELEMENTS
   and nil if it does not."
  (block coll-contains-all-block
	(coll-map elements 
			  #'(lambda (element)
				  (unless (apply #'coll-contains? coll element rest)
					(return-from coll-contains-all-block nil))) )
	t))

(defmethod-abstract coll-size ((coll adt-collection) &key &allow-other-keys)
  "Returns the size of the collection COLL.")

(defmethod-abstract coll-empty? ((coll adt-collection) &key &allow-other-keys)
  "Returns t if the collection COLL contains no elements, nil otherwise")

(defmethod-abstract coll-map ((coll adt-collection)(f function) &key &allow-other-keys)
  "Applies a function F to each element in the collection COLL.
   The function is required to take one argument - an element.")

(defmacro docoll ((coll element-var &optional (index-var nil index-var-supplied-p)) &rest rest &key &allow-other-keys)
  "A convenience macro that operates identically to coll-map.
   Loops over COLL and binds each element in the collection, one-at-a-time, with ELEMENT-VAR. 
   INDEX-VAR starts at 0 for the first element and counts the elements. The INDEX-VAR does
   not necessarily imply the index of the element in COLL.
   The macro uses an implicit nil block, so (return) or (return-from nil) can be used to
   terminate traversal prematurely."
  (when (not (symbolp element-var))
	(error "Cannot bind ~a -- not a symbol" element-var))
  (when (and index-var-supplied-p (not (symbolp index-var)))
	(error "Cannot bind ~a -- not a symbol" index-var))
  `(block nil 
	 (if ,index-var-supplied-p
		 (let ((,index-var 0))
		   (coll-map ,coll #'(lambda (,element-var) ,@rest (incf ,index-var))))
		 (coll-map ,coll #'(lambda (,element-var) ,@rest))
		 )))

(defmethod-abstract coll-tolist ((coll adt-collection) &key &allow-other-keys)
  "Returns a list of elements in the collection COLL."
  (let* ((head '(junk))
		 (tail head))
	(coll-map coll (lambda (x) (setf (cdr tail) x)))
	(cdr head)))

(defmethod-abstract coll-toarray ((coll adt-collection) &key (adjustable nil)(fill-pointer nil)(displaced-to nil)(displaced-index-offset 0) &allow-other-keys)
  "Returns an array of elements in the collection COLL."
  (make-array (coll-size coll) 
			  :initial-contents (coll-tolist coll)
			  :adjustable adjustable
			  :fill-pointer fill-pointer
			  :displaced-to displaced-to
			  :displaced-index-offset displaced-index-offset))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ITERATOR
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass adt-collection-iterator ()
  ())

(defmethod-abstract make-coll-iterator ((coll adt-collection) &key &allow-other-keys)
  "Creates an iterator object that maintains its state over the collection.
  This method should throw an error if it is not supported.")

(defmethod-abstract coll-it-next ((coll-iterator adt-collection-iterator) &key &allow-other-keys)
  "Returns the next element in the collection used to create the COLL-ITERATOR. 
   If there are no more elements, nil is returned.
   This method should throw an error if it is not supported.")

(defmethod-abstract coll-it-hasnext ((coll-iterator adt-collection-iterator) &key &allow-other-keys)
  "Returns t if the collection used to create the COLL-ITERATOR has another element.
  This method should throw an error if it is not supported.")
