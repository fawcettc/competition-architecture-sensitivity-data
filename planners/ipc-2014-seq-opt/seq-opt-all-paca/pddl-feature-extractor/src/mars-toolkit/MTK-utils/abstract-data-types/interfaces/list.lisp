;;;; Copyright (c) 2011 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   David Wang

(in-package #:abstract-data-types)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ERRORS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define-condition index-out-of-bounds (type-error)
  ())

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; LIST
;; This class provides the interface specification for
;; data structures that store collections of elements 
;; as an indexed list.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass adt-list (adt-collection)
  ())

(defmethod-abstract coll-add! ((list adt-list)(element t) &key &allow-other-keys)
  "Appends the ELEMENT to the end of the LIST.
   Returns the modified LIST.")

(defmethod-abstract coll-add-all! ((list adt-list)(elements list) &rest rest &key &allow-other-keys)
  "Appends all of the ELEMENTS to the end of the LIST.
   Returns the modified LIST.
   Complexity: O(1)*<length of elements>"
  (loop for element in elements do
	   (apply #'coll-add! list element rest))
  list)

(defmethod-abstract coll-add-all! ((list adt-list)(elements adt-collection) &rest rest &key &allow-other-keys)
  "Appends all of the ELEMENTS to the end of the LIST.
   Returns the modified LIST.
   Complexity: O(1)*<length of elements>"
  (coll-map elements 
			(lambda (element) (apply #'coll-add! list element rest)))
  list)

(defmethod-abstract coll-get ((list adt-list)(index integer) &key &allow-other-keys)
  "Returns the element at the specified position in the LIST.
   Throws INDEX-OUT-OF-BOUNDS condition if (INDEX<0||INDEX>=size).")

(defmethod-abstract coll-get-first ((list adt-list) &rest rest &key &allow-other-keys)
  "Returns the first element in the LIST.
   Throws INDEX-OUT-OF-BOUNDS condition if there are no elements in the LIST."
  (apply #'coll-get list 0 rest))

(defmethod-abstract coll-get-last ((list adt-list) &rest rest &key &allow-other-keys)
  "Returns the last element in the LIST.
   Throws INDEX-OUT-OF-BOUNDS condition if there are no elements in the LIST."
  (apply #'coll-get list (- (coll-size list) 1) rest))

(defmethod-abstract coll-set! ((list adt-list)(index integer)(element t) &key &allow-other-keys)
  "Replaces the element at the specified position in the LIST with the specified ELEMENT.
   Returns the original element.
   Throws INDEX-OUT-OF-BOUNDS condition if (INDEX<0||INDEX>=size).")

(defmethod-abstract coll-insert! ((list adt-list)(index integer)(element t) &key &allow-other-keys)
  "Inserts the ELEMENT at the INDEX in the LIST. 
   Shifts elments currently at the index and any subsequent 
   elements to the right (adds one to their indices).
   Throws INDEX-OUT-OF-BOUNDS condition if (INDEX<0||INDEX>size).")

(defmethod-abstract coll-remove! ((list adt-list)(element t) &key &allow-other-keys)
  "Removes the first occurance of ELEMENT from the LIST.
   Returns t if the collection was modified, nil otherwise.")

(defmethod-abstract coll-remove-from! ((list adt-list)(index integer) &key &allow-other-keys)
  "Removes the element at INDEX from the LIST.
   Throws INDEX-OUT-OF-BOUNDS condition if (INDEX<0||INDEX>=size)")

(defmethod-abstract coll-remove-first! ((list adt-list) &rest rest &key &allow-other-keys)
  "Removes the first element from the LIST.
   Throws INDEX-OUT-OF-BOUNDS condition if there are no elements in the LIST."
  (apply #'coll-remove-from! list 0 rest))

(defmethod-abstract coll-remove-last! ((list adt-list) &rest rest &key &allow-other-keys)
  "Removes the last element from the LIST.
   Throws INDEX-OUT-OF-BOUNDS condition if there are no elements in the LIST."
  (apply #'coll-remove-from! list (- (coll-size list) 1) rest))

(defmethod-abstract coll-clear! ((list adt-list) &key &allow-other-keys)
  "Clears the LIST of all elements.")

(defmethod-abstract coll-contains? ((list adt-list)(element t) &key (test #'eql) &allow-other-keys)
  "Returns the index of the first element found, or nil if no element was found."
  (declare (ignore test)))

(defmethod-abstract coll-size ((list adt-list) &key &allow-other-keys) 
  "Returns the size of the LIST.")

(defmethod-abstract coll-empty? ((list adt-list) &rest rest &key &allow-other-keys)
  "Returns t if the LIST contains no elements, nil otherwise."
  (eql (apply #'coll-size list rest) 0))

(defmethod-abstract coll-map ((list adt-list)(f function) &key &allow-other-keys)
  "Applies a function F to each element in the list.
   The function is required to take one argument - a list element.")

(defmethod-abstract coll-tolist ((list adt-list) &key &allow-other-keys)
  "Returns a list. Returns nil if the list is empty.")

(defmethod-abstract coll-toarray ((list adt-list) &key &allow-other-keys)
  "Returns an array.")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ITERATOR
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass adt-list-iterator (adt-collection-iterator)
  ())

(defmethod-abstract make-coll-iterator ((list adt-list) &key &allow-other-keys)
  "Creates an iterator object that maintains its state over the LIST.
  This method should throw an error if it is not supported.")

(defmethod-abstract coll-it-next ((list-iterator adt-list-iterator) &key &allow-other-keys)
  "Returns the next element in the list used to create the LIST-ITERATOR. 
   If there are no more elements, nil is returned.
   This method should throw an error if it is not supported.")

(defmethod-abstract coll-it-hasnext ((list-iterator adt-list-iterator) &key &allow-other-keys)
  "Returns t if the list used to create the LIST-ITERATOR has another element.
  This method should throw an error if it is not supported.")

