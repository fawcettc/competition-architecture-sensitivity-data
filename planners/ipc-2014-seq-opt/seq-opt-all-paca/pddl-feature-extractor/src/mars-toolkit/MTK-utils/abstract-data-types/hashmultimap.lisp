;;;; Copyright (c) 2011 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   David Wang

(in-package #:abstract-data-types)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; HASHMULTIMAP
;; This class allows multiple values to be associated with
;; each key. Multiple values are stored as collections.
;; The type of collection can be specified by the user.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass hashmultimap (adt-map)
  ((table
	:type hash-table
	:initform (make-hash-table)
	:initarg :table
	:accessor hmm-table
	:documentation "The lisp hash table storing the hash map.")
   (make-coll
	:type list
	:initform nil
	:initarg :make-coll
	:accessor hmm-make-coll
	:documentation "The user provided call to create a collection."
	))
  )

(defun make-hashmultimap (&key (make-coll '(make-smart-list)) (test 'eql) (size 67)
							(rehash-size 1.2) (rehash-threshold 0.6407767))
  "make-coll are the arguments that will be used to make the collection. 
   i.e. through the call (apply #'make-instance make-coll).
   The remaining parameters are used to create the backing hashtable."
  (make-instance 
   'hashmultimap
   :table (make-hash-table :test test :size size 
						   :rehash-size rehash-size
						   :rehash-threshold rehash-threshold)
   :make-coll make-coll))


(defmethod-alias hmm-add! map-add! ((map hashmultimap)(key t)(value t) &key (replace t)(default nil) &allow-other-keys)
  "Adds the KEY VALUE pair to the MAP.
   Returns two values. The first value is boolean and represents if the KEY VALUE 
   pair was added. The second value is a collection of values associated with the KEY,
   DEFAULT otherwise.
   If REPLACE argument is passed to the coll-add! function, how it is honored depends on the collection being used."
  (with-slots (table make-coll) map
	(multiple-value-bind (old-coll key-there?)
		(gethash key table default)
	  ;; create the collection if it does not exist yet
	  (unless key-there?
		(setf old-coll (eval make-coll)))
	  ;; add the value to the collection
	  (cond ((coll-add! old-coll value :replace replace)
			 (setf (gethash key table) old-coll)
			 ;;return
			 (values t old-coll))
			(:otherwise 
			 (values nil old-coll)))
	   	  )))

(defmethod-alias hmm-remove! map-remove! ((map hashmultimap)(key t) &key (value nil value?)(default nil) &allow-other-keys)
  "Removes the KEY and all associated values from the MAP.
   If VALUE is specified, only the matching KEY-VALUE pair will be removed.
   Returns two values.
     If the KEY (and VALUE) is found, the first value will be the collection 
   associated with the key, the second value will be a boolean value,
   indicating something was removed.
     If the KEY is not found, the first value will be DEFAULT 
   the second value will be nil."
  (multiple-value-bind (old-coll there?)
	  (gethash key (hmm-table map) default)
	(cond ((not there?) (values default nil))
		  (value?
		   ;; the collection was found, now remove a value
		   (if (coll-remove! old-coll value)
			   (values old-coll t)
			   (values old-coll nil)))
		  (:otherwise
		   ;; delete it from the hash table
		   (remhash key (hmm-table map))
		   (values old-coll t))
		  )))

(defmethod-alias hmm-get map-get ((map hashmultimap)(key t) &key (default nil) &allow-other-keys)
  "Returns the collection associated with KEY in the MAP.
   Returns two values. 
     If the KEY is found, the first value will be the collection 
   associated with the key, the second value will be t, 
   indicating the key was found.
     If the KEY is not found, the first value will be DEFAULT 
   the second value will be nil."
  (gethash key (hmm-table map) default))

(defmethod-alias hmm-clear! map-clear! ((map hashmultimap) &key &allow-other-keys)
  "Clears the MAP of all contents."
  ;; clear the table
  (clrhash (hmm-table map)))

(defmethod-alias hmm-contains? map-contains? ((map hashmultimap)(key t) &rest rest &key &allow-other-keys)
  "Returns t if the MAP contains the KEY
   and nil if it does not."
  (apply #'hmm-contains-key? map key rest))

(defmethod-alias hmm-contains-key? map-contains-key? ((map hashmultimap)(key t) &key &allow-other-keys)
  "Returns t if the MAP contains the KEY
   and nil if it does not."
  (multiple-value-bind (coll there?)
	  (gethash key (hmm-table map))
	(declare (ignore coll))
	there?))

(defmethod-alias hmm-contains-value? map-contains-value? ((map hashmultimap)(value t) &key (default nil) &allow-other-keys)
  "Returns two values. 
   The first value is t if the MAP contains the VALUE, DEFAULT otherwise. 
   The second value is the key associated with the VALUE."
  (block stop-mapping
	(hmm-map map 
			(lambda (k coll)
			  (when (coll-contains? coll value)
				(return-from stop-mapping (values t k))
				(return-from stop-mapping (values nil default))))
			)))

(defmethod-alias hmm-size map-size ((map hashmultimap) &key (keys-only nil)  &allow-other-keys)
  "Returns the number of KEY VALUE pairs stored in the MAP."
  (if keys-only
	  ;; return only the number of keys in the table
	  (hash-table-count (hmm-table map))
	  ;; return all the key-value pairs
	  (let ((size 0))
		(maphash (lambda (key coll)
				   (declare (ignore key))
				   (incf size (coll-size coll)))
				 (hmm-table map))
		size)))

(defmethod-alias hmm-empty? map-empty? ((map hashmultimap) &key &allow-other-keys)
  "Returns t if the MAP contains no keys, nil otherwise."
  (= (hash-table-count (hmm-table map)) 0))

(defmethod-alias hmm-map map-map ((map hashmultimap) (f function) &key &allow-other-keys)
  "Applies a function F to each entry in the MAP.
   The function is required to take two arguments - the key and the value."
  (maphash (lambda (key coll) 
			 (coll-map coll (lambda (element) (funcall f key element)))) 
		   (hmm-table map)))

(defmethod print-object ((map hashmultimap) stream)
  (format stream "#HASHMULTIMAP(")
  (let ((first-loop t))
	(hmm-map map 
			 (lambda (key value) 
			   (unless first-loop 
				 (format stream " "))
			   (format stream "~a:~a" key value)
			   (setf first-loop nil)))
	)
  (format stream ")"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ITERATOR
;; Not supported by hashmap class.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmethod make-map-iterator ((map hashmultimap) &key &allow-other-keys)
  "Creates an iterator object that maintains its state over the MAP.
  This method should throw an error if it is not supported."
  (error 'unsupported-function))






(check-implementation 'hashmultimap)
