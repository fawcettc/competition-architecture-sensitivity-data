;;;; Copyright (c) 2013 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   David C. Wang

(in-package #:clos-utils)

(defun check-documentation (method-name)
  (flet ((print-documentation (method-name specializers documentation)  
		   (format t "Documentation for ~a ~a~%" method-name specializers)
		   (format t "   ~a~%" documentation)  ))
	;; print documentation for abstract classes
	(loop for abstract-class being the hash-values in *abstract-classes* do
		 (loop for abstract-method in abstract-class do
			  (with-slots (name spec-lambda-list documentation) abstract-method
				(when (eql name method-name)
				  (print-documentation method-name 
									   (get-specializers spec-lambda-list)
									   documentation)))))
	;; print documentation for implementations
	#+allegro
	(let ((method (symbol-function method-name)))
	  (loop for method in (reverse (clos:generic-function-methods method)) do
		   (print-documentation method-name 
								(mapcar #'class-name (mop:method-specializers method)) 
								(documentation method 'function)) ))
	#-allegro
	(format t "Function check-documentation is not fully supported for this lisp runtime - It does not support printing documentation for all of the methods of generic-function ~a~%." method-name))
  )