;;;; Copyright (c) 2009 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   David C. Wang


(asdf:defsystem #:abstract-data-types
  :name "abstract-data-types"
  :version "0.1"
  :description "MERS Toolkit: Abstract Data Types"
  :author "David Wang"
  :maintainer "MIT MERS Group"
  :components ((:file "package")
			   ;;; INTERFACES
			   (:file "interfaces/collection"
					  :depends-on ("package"))
			   (:file "interfaces/list"
					  :depends-on ("package" "interfaces/collection"))
			   (:file "interfaces/set"
					  :depends-on ("package" "interfaces/collection"))
			   (:file "interfaces/queue"
					  :depends-on ("package" "interfaces/collection"))
			   (:file "interfaces/map"
					  :depends-on ("package"))
			   (:file "interfaces/ordered-map"
					  :depends-on ("package" "interfaces/map"))

			   ;; IMPLEMENTATIONS
			   (:file "smart-list"
					  :depends-on ("package" "interfaces/list" "interfaces/queue"))
			   (:file "ordered-list-queue"
					  :depends-on ("package" "interfaces/queue"))
			   (:file "arraylist"
					  :depends-on ("package" "interfaces/list" "smart-list"))
			   (:file "hashmap"
			   		  :depends-on ("package" "interfaces/map"))
			   (:file "hashset"
			   		  :depends-on ("package" "interfaces/set" "hashmap"))
			   (:file "hashbag"
			   		  :depends-on ("package" "interfaces/set" "hashmap"))
			   (:file "linked-hashmap"
			   		  :depends-on ("package" "interfaces/map"))
			   (:file "linked-hashset"
			   		  :depends-on ("package" "interfaces/set" "linked-hashmap"))
			   (:file "bimap"
			   		  :depends-on ("package" "interfaces/map"))
			   (:file "hashmultimap"
					  :depends-on ("package" "interfaces/collection"))
			   (:file "tree-map"
					  :depends-on ("package" "interfaces/ordered-map"))
			   (:file "red-black-tree-map"
					  :depends-on ("package" "interfaces/ordered-map" "tree-map"))
			   ;; ;; (:file "linked-hashmultimap"
			   ;; ;; 		  :depends-on ("package" "interfaces/map"))
			   ;; Note: The dictionary class is not fully integrated into the naming conventions
			   (:file "dictionary"
			   		  :depends-on ("package"))

			   ;; TESTS
			   (:file "tests/test-list"
					  :depends-on ("package" "interfaces/list"))
			   (:file "tests/test-smart-list"
					  :depends-on ("package" "smart-list" "tests/test-list"))
			   (:file "tests/test-linked-hashmap"
			   		  :depends-on ("package" "linked-hashmap"))
			   (:file "tests/test-linked-hashset"
			   		  :depends-on ("package" "linked-hashset"))
			   (:file "tests/test-dictionary"
			   		  :depends-on ("package" "dictionary"))
			   )
  :depends-on (#:mtk-utils #:naming-utils #:clos-utils))

