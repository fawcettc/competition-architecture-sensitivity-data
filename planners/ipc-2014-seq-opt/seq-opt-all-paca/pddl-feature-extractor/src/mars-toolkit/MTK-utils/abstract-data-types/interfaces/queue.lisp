;;;; Copyright (c) 2013 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Eric Timmons

(in-package #:abstract-data-types)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; QUEUE
;; This class provides the interface specification for
;; data structures that store collections of elements in
;; order.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass adt-queue (adt-collection)
  ())

(defmethod-abstract queue-remove! ((q adt-queue) &key &allow-other-keys)
  "=> A values structure. The first is element at the head of `Q`.
 The second is `T` iff there was an element at the head, `NIL` else.

Removes the head of `Q`.")

(defmethod-abstract queue-peek ((q adt-queue) &key &allow-other-keys)
  "=> A values structure. The first is element at the head of `Q`.
 The second is `T` iff there was an element at the head, `NIL` else.

Does not modify `Q`.")

(defmethod-abstract queue-empty? ((q adt-queue) &rest rest)
  (apply #'coll-empty? q rest))

(defmethod-abstract queue-clear! ((q adt-queue) &rest rest)
  (apply #'coll-clear! q rest))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; LIFO-QUEUE
;; This class provides the interface specification for
;; data structures that store items in last-in-first-out
;; order.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass adt-lifo-queue (adt-queue)
  ())

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; FIFO-QUEUE
;; This class provides the interface specification for
;; data structures that store items in first-in-first-out
;; order.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass adt-fifo-queue (adt-queue)
  ())

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; PRIORITY-QUEUE
;; This class provides the interface specification for
;; data structures that store items in priority order.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass adt-priority-queue (adt-queue)
  ((key
	:initarg :key
	:initform #'identity
	:documentation "The key function. Defaults to `IDENTITY`.")
   (order
	:initarg :order
	:initform #'>
	:documentation "The ordering function to use. Defaults to `>`."))
  (:documentation "Interface specification for storing items in
  priority order as defined by `KEY` and `ORDER`."))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Convenience methods
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmethod enqueue! ((q adt-queue) object)
  (coll-add! q object))

(defmethod dequeue! ((q adt-queue))
  (queue-remove! q))
