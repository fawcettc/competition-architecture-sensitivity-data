;;;; Copyright (c) 2011 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   David Wang

(in-package #:abstract-data-types)

(mtk-utils:deftest-suite :test-smart-list)

(mtk-utils:deftest (test-slist-add :test-smart-list) ()
  (test-list-add #'make-smart-list)
  ;; Test the set for sequential adding of keys
  (let ((list (make-smart-list))
		(elements '(a b c d e f g h)))
	;; add a bunch of symbol pairs 
	(loop for elt in elements do (slist-add! list elt))
	;; Check the tail pointer
	(mtk-utils:test (car (slist-tail list))
					'h
					:test #'equal
					:fail-info "Failed add test. Tail poiner is not correct.")
	))

(mtk-utils:deftest (test-slist-get :test-smart-list) ()
  (test-list-get #'make-smart-list))

(mtk-utils:deftest (test-slist-insert :test-smart-list) ()
  (test-list-insert #'make-smart-list))

(mtk-utils:deftest (test-slist-remove-from :test-smart-list) ()
  (test-list-remove-from #'make-smart-list))

(mtk-utils:deftest (test-slist-remove-first :test-smart-list) ()
  (test-list-remove-first #'make-smart-list))

(mtk-utils:deftest (test-slist-remove-last :test-smart-list) ()
  (test-list-remove-last #'make-smart-list))

(mtk-utils:deftest (test-slist-clear :test-smart-list) ()
  (test-list-clear #'make-smart-list))