;;;; Copyright (c) 2011 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   David Wang

(in-package #:abstract-data-types)

(mtk-utils:deftest-suite :test-linked-hashmap)

(mtk-utils:deftest (test-lhm-1 :test-linked-hashmap) ()
  ;; Test the map for sequential adding of keys
  (let ((map (make-linked-hashmap))
		(keys   '(a b c d e f g h))
		(values '(aa bb cc dd ee ff gg hh)))
	;; add a bunch of symbol pairs 
	(loop for key in keys
		 for value in values do
		 (map-add! map key value))
	;; Test containment
	(loop for key in keys do
		 (mtk-utils:test (map-contains? map key)
						 t
						 :test #'eql
						 :fail-info "Failed containment test."))
	;; Test size
	(mtk-utils:test (map-size map)
					(length keys)
					:test #'eql
					:fail-info "Failed size test.")
	;; Test empty
	(mtk-utils:test (map-empty? map)
					nil
					:test #'eql
					:fail-info "Failed empty test.")
	;; Test mapping
	(let ((test-keys nil)
		  (test-values nil))
		(map-map map 
				 (lambda (key value)
				   (setf test-keys (nconc test-keys (list key)))
				   (setf test-values (nconc test-values (list value))))
				 :reverse-direction nil)
		(mtk-utils:test (list test-keys test-values)
						(list keys values)
						:test #'equal
						:fail-info "Failed mapping test."))
	;; Test reverse mapping
	(let ((test-keys nil)
		  (test-values nil))
	  (map-map map 
			   (lambda (key value)
				 (push key test-keys)
				 (push value test-values))
			   :reverse-direction t)
	  (mtk-utils:test (list test-keys test-values)
					  (list keys values)
					  :test #'equal
					  :fail-info "Failed reverse-mapping test."))
	
	))


(mtk-utils:deftest (test-lhm-2 :test-linked-hashmap) ()
  ;; Test the map for sequential adding of keys
  (let ((map (make-linked-hashmap))
		(keys   '(a b a c d g e f g h))
		(values '(aa bb aa cc dd gg ee ff gg hh))
		(unique-keys   '(b a c d e f g h))
		(unique-values '(bb aa cc dd ee ff gg hh)) )
	;; add a bunch of symbol pairs 
	(loop for key in keys
		 for value in values do
		 (map-add! map key value))
	;; Test containment
	(loop for key in unique-keys do
		 (mtk-utils:test (map-contains? map key)
						 t
						 :test #'equal
						 :fail-info "Failed containment test."))
	;; Test size
	(mtk-utils:test (map-size map)
					(length unique-keys)
					:test #'eql
					:fail-info "Failed size test.")
	;; Test empty
	(mtk-utils:test (map-empty? map)
					nil
					:test #'eql
					:fail-info "Failed empty test.")
	;; Test mapping
	(let ((test-keys nil)
		  (test-values nil))
		(map-map map 
				 (lambda (key value)
				   (setf test-keys (nconc test-keys (list key)))
				   (setf test-values (nconc test-values (list value))))
				 :reverse-direction nil)
		(mtk-utils:test (list test-keys test-values)
						(list unique-keys unique-values)
						:test #'equal
						:fail-info "Failed mapping test."))
	;; Test reverse mapping
	(let ((test-keys nil)
		  (test-values nil))
	  (map-map map 
			   (lambda (key value)
				 (push key test-keys)
				 (push value test-values))
			   :reverse-direction t)
	  (mtk-utils:test (list test-keys test-values)
					  (list unique-keys unique-values)
					  :test #'equal
					  :fail-info "Failed reverse mapping test."))
	
	))