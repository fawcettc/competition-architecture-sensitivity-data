;;;; Copyright (c) 2013 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Eric Timmons

;;;; This implementation of red black trees is based on the paper
;;;; "Left-leaning Red-Black Trees" by Robert Sedgewick

(in-package #:abstract-data-types)

(defstruct (red-black-tree-map-node (:include tree-map-node))
  "node for binary search trees."
  (red? T))

(defclass red-black-tree-map (tree-map)
  ((root
	:type red-black-search-tree-node
	:initform nil)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Utils                                                                    ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun rbtm-is-red? (node)
  "Returns T iff NODE != NIL and NODE is black."
  (declare (type red-black-tree-map-node node))
  (and node (red-black-tree-map-node-red? node)))

(defun rbtm-flip-colors! (node)
  (declare (type red-black-tree-map-node node))
  (setf (red-black-tree-map-node-red? node) (not (red-black-tree-map-node-red? node)))
  (let ((left-node (red-black-tree-map-node-left node))
		(right-node (red-black-tree-map-node-right node)))
	(setf (red-black-tree-map-node-red? left-node) (not (red-black-tree-map-node-red? left-node)))
	(setf (red-black-tree-map-node-red? right-node) (not (red-black-tree-map-node-red? right-node)))))

(defun rbtm-rotate-left! (node)
  (declare (type red-black-tree-map-node node))
  (let ((x (red-black-tree-map-node-right node)))
	(setf (red-black-tree-map-node-parent x) (red-black-tree-map-node-parent node))
	(setf (red-black-tree-map-node-parent node) x)
	(setf (red-black-tree-map-node-right node) (red-black-tree-map-node-left x))
	(when (red-black-tree-map-node-right node)
	  (setf (red-black-tree-map-node-parent (red-black-tree-map-node-right node)) node))
	(setf (red-black-tree-map-node-left x) node)
	(setf (red-black-tree-map-node-red? x) (red-black-tree-map-node-red? node))
	(setf (red-black-tree-map-node-red? node) t)
	x))

(defun rbtm-rotate-right! (node)
  (declare (type red-black-tree-map-node node))
  (let ((x (red-black-tree-map-node-left node)))
	(setf (red-black-tree-map-node-parent x) (red-black-tree-map-node-parent node))
	(setf (red-black-tree-map-node-parent node) x)
	(setf (red-black-tree-map-node-left node) (red-black-tree-map-node-right x))
	(when (red-black-tree-map-node-left node)
	  (setf (red-black-tree-map-node-parent (red-black-tree-map-node-left node)) node))
	(setf (red-black-tree-map-node-right x) node)
	(setf (red-black-tree-map-node-red? x) (red-black-tree-map-node-red? node))
	(setf (red-black-tree-map-node-red? node) t)
	x))

(defun rbtm-move-red-left! (node)
  (declare (type red-black-tree-map-node node))
  (rbtm-flip-colors! node)
  (when (and (tree-map-node-right node)
			 (rbtm-is-red? (tree-map-node-left (tree-map-node-right node))))
	(setf (tree-map-node-right node) (rbtm-rotate-right! (tree-map-node-right node)))
	(setf node (rbtm-rotate-left! node))
	(rbtm-flip-colors! node))
  node)

(defun rbtm-move-red-right! (node)
  (declare (type red-black-tree-map-node node))
  (rbtm-flip-colors! node)
  (when (and (tree-map-node-left node)
			 (rbtm-is-red? (tree-map-node-left (tree-map-node-left node))))
	(setf node (rbtm-rotate-right! node))
	(rbtm-flip-colors! node))
  node)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Add Elements                                                             ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmethod-alias rbtm-add! map-add! ((map red-black-tree-map) (key t) (value t) &key (replace t) (default nil) &allow-other-keys)
  "Adds the KEY VALUE pair to the MAP.
   Returns two values. The first value is boolean and represents if the KEY VALUE 
   pair was added. The second value is the original value associated with the KEY, 
   DEFAULT otherwise.
   If REPLACE is t (default), the new key-value pair will replace the old key-value pair."
  
  (with-slots (root order-predicate number-elements) map
	(multiple-value-bind (new-root added? e) (rbtm-add!-helper root nil value key order-predicate :replace replace :default default)
	  (setf root new-root)
	  (when added?
		(incf number-elements))
	  (setf (red-black-tree-map-node-red? root) nil)
	  (values added? e))))

(defun rbtm-add!-helper (node parent value key order-p &key replace default)
  (declare (type red-black-tree-map-node node)
   		   (type function order-p))
  (if (null node)
	  (values (make-red-black-tree-map-node :value value
											:key key
											:parent parent)
			  t
			  default)
	  (let ((added? nil)
			(old-value default))
		(let ((left-child (red-black-tree-map-node-left node))
			  (right-child (red-black-tree-map-node-right node)))
		  (when (and (rbtm-is-red? left-child) (rbtm-is-red? right-child))
			(rbtm-flip-colors! node))
		  (let ((comparison (funcall order-p
									 (tree-map-node-key node)
									 key)))
			(cond
			  ((zerop comparison)
			   ;; ELEMENT is equal to the KEY used in this search node.
			   (when replace
				 (setf old-value (tree-map-node-value node))
				 (setf (tree-map-node-value node) value)
				 (setf added? T)))
			  ((minusp comparison)
			   ;; Add ELEMENT to the left of this node.
			   (multiple-value-bind (new-left added?1 e) (rbtm-add!-helper left-child node value key order-p :replace replace :default default)
				 (setf (tree-map-node-left node) new-left)
				 (setf added? added?1)
				 (setf old-value e)))
			  ((plusp comparison)
			   ;; Add ELEMENT to the right of this node.
			   (multiple-value-bind (new-right added?1 e) (rbtm-add!-helper right-child node value key order-p :replace replace :default default)
				 (setf (tree-map-node-right node) new-right)
				 (setf added? added?1)
				 (setf old-value e)))
			  (T
			   ;; Signal an error
			   (error "Invalid value returned by ORDER-P")))))
		(let ((left-child (red-black-tree-map-node-left node))
			  (right-child (red-black-tree-map-node-right node)))
		  (when (and (rbtm-is-red? right-child) (not (rbtm-is-red? left-child)))
			(setf node (rbtm-rotate-left! node))))
		(let ((left-child (red-black-tree-map-node-left node)))
		  (when (and (rbtm-is-red? left-child) (rbtm-is-red? (tree-map-node-left left-child)))
			(setf node (rbtm-rotate-right! node))))
		(values node added? old-value))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Removing Elements                                                        ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmethod-alias rbtm-remove! map-remove! ((map red-black-tree-map) (key t) &key (default nil) &allow-other-keys)
  "Removes the KEY and associated value from the MAP.
   Returns two values. 
     If the KEY is found, the first value will be the value 
   associated with the key, the second value will be t, 
   indicating something was removed.
     If the KEY is not found, the first value will be DEFAULT 
   the second value will be nil."
  (if (map-contains? map key)
	  (with-slots (root order-predicate number-elements) map
		(multiple-value-bind (new-root item found?) (rbtm-remove!-helper root key order-predicate default)
		  (setf root new-root)
		  (setf (red-black-tree-map-node-red? new-root) nil)
		  (if found?
			  (decf number-elements))
		  (values item found?)))
	  (values default nil)))

(defun rbtm-fix-up! (node)
  (declare (type red-black-tree-map-node node))
  (when (and (rbtm-is-red? (tree-map-node-left node)) (rbtm-is-red? (tree-map-node-right node)))
	(rbtm-flip-colors! node))
  (let ((left-child (red-black-tree-map-node-left node))
		(right-child (red-black-tree-map-node-right node)))
	(when (and (rbtm-is-red? right-child) (not (rbtm-is-red? left-child)))
	  (setf node (rbtm-rotate-left! node))))
  (let ((left-child (red-black-tree-map-node-left node)))
	(when (and (rbtm-is-red? left-child) (rbtm-is-red? (tree-map-node-left left-child)))
	  (setf node (rbtm-rotate-right! node))))
  node)

(defun rbtm-delete-min! (node)
  (declare (type red-black-tree-map-node node))
  (if (null (tree-map-node-left node))
	  nil
	  (progn
		(when (and (not (rbtm-is-red? (tree-map-node-left node)))
				   (not (rbtm-is-red? (tree-map-node-left (tree-map-node-left node)))))
		  (setf node (rbtm-move-red-left! node)))
		(setf (tree-map-node-left node) (rbtm-delete-min! (tree-map-node-left node)))
		(rbtm-fix-up! node))))

(defun rbtm-remove!-helper (node key order-p default)
  (declare (type red-black-tree-map-node node)
		   (type function order-p))
  (let ((comp (funcall order-p (tree-map-node-key node) key))
		(item nil)
		(found? nil))
	(cond
	  ((minusp comp)
	   (when (and (tree-map-node-left node)
				  (not (rbtm-is-red? (tree-map-node-left node)))
				  (not (rbtm-is-red? (tree-map-node-left (tree-map-node-left node)))))
		 (setf node (rbtm-move-red-left! node)))
	   (multiple-value-bind (new-left item1 found?1) (rbtm-remove!-helper (tree-map-node-left node) key order-p default)
		 (setf (tree-map-node-left node) new-left)
		 (setf item item1)
		 (setf found? found?1)))
	  (T
	   (when (rbtm-is-red? (tree-map-node-left node))
		 (setf node (rbtm-rotate-right! node)))
	   (if (and (zerop (funcall order-p (tree-map-node-key node) key))
				(null (tree-map-node-right node)))
		   (return-from rbtm-remove!-helper (values nil (tree-map-node-value node) T))
		   (progn
			 (when (and (tree-map-node-right node)
						(not (rbtm-is-red? (tree-map-node-right node)))
						(not (rbtm-is-red? (tree-map-node-left (tree-map-node-right node)))))
			   (setf node (rbtm-move-red-right! node)))
			 (if (zerop (funcall order-p (tree-map-node-key node) key))
				 (let ((min-right (tm-find-min (tree-map-node-right node))))
				   (setf item (tree-map-node-value node))
				   (setf found? t)
				   (setf (tree-map-node-value node) (tree-map-node-value min-right))
				   (setf (tree-map-node-key node) (tree-map-node-key min-right))
				   (setf (tree-map-node-right node) (rbtm-delete-min! (tree-map-node-right node))))
				 (multiple-value-bind (new-right item1 found?1) (rbtm-remove!-helper (tree-map-node-right node) key order-p default)
				   (setf (tree-map-node-right node) new-right)
				   (setf item item1)
				   (setf found? found?1)))))))
	(values (rbtm-fix-up! node) item found?)))

(check-implementation 'red-black-tree-map)