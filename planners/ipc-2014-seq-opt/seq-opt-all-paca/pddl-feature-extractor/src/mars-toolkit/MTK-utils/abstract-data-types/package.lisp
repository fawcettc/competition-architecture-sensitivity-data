;;;; Copyright (c) 2009 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   David C. Wang

(in-package #:cl-user)

(defpackage #:abstract-data-types
  (:nicknames #:adt)
  (:use #:cl #:naming-utils #:clos-utils)
  (:export 

   ;; GENERIC INTERFACES
   #:coll-copy
   #:coll-add!
   #:coll-add-all!
   #:coll-get
   #:coll-get-first
   #:coll-get-last
   #:coll-set!
   #:coll-insert!
   #:coll-remove!
   #:coll-remove-from!
   #:coll-remove-first!
   #:coll-remove-last!
   #:coll-remove-all!
   #:coll-clear!
   #:coll-contains?
   #:coll-contains-all?
   #:coll-size
   #:coll-empty?
   #:coll-map
   #:docoll
   #:coll-tolist

   #:make-coll-iterator
   #:coll-it-next
   #:coll-it-hasnext

   #:map-add!
   #:map-add-all!
   #:map-remove!
   #:map-remove-first!
   #:map-remove-last!
   #:map-get
   #:map-get-first
   #:map-get-last
   #:map-clear!
   #:map-contains?
   #:map-size
   #:map-empty?
   #:map-map
   #:domap
   #:make-map-iterator
   #:map-it-next
   #:map-it-hasnext

   ;; error.lisp
   #:unsupported-function

   ;; collection.lisp
   #:adt-collection

   ;; list.lisp
   #:index-out-of-bounds
   #:adt-list
   #:adt-list-iterator

   ;; queue.lisp
   #:queue-remove!
   #:enqueue!
   #:dequeue!
   #:queue-clear!
   #:queue-empty?
   #:queue-peek

   ;; map.lisp
   #:adt-map
   #:adt-map-iterator

   ;; set.lisp
   #:adt-set
   #:adt-set-iterator

   ;; arraylist.lisp
   #:arraylist
   #:make-arraylist

   ;; smart-list.lisp
   #:smart-list
   #:make-smart-list
   #:smart-list-iterator

   ;; hashbag.lisp
   #:hashbag
   #:make-hashbag

   ;; hashmap.lisp
   #:hashmap
   #:make-hashmap

   ;; hashmultimap.lisp
   #:hashmultimap
   #:make-hashmultimap

   ;; hashset.lisp
   #:hashset
   #:make-hashset

   ;; linked-hashmap.lisp
   #:linked-hashmap
   #:make-linked-hashmap
   #:linked-hashmap-iterator
 
   ;; linked-hashset.lisp
   #:linked-hashset
   #:make-linked-hashset
   #:linked-hashset-iterator

   ;; bimap.lisp
   #:bimap
   #:make-bimap
   #:with-bimap-slots

   ;; dictionary.lisp
   #:dictionary 
   #:make-dictionary
   #:dictionary-add 
   #:dictionary-remove-value
   #:dictionary-get
   #:dictionary-remove 
   #:dictionary-clear 

   ;; ordered-list-queue.lisp
   #:make-ordered-list-queue
   )
)
