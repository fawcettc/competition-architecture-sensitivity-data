;;;; Copyright (c) 2013 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   David C. Wang

(in-package #:abstract-data-types)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; SMART-LIST
;; This class provides a very simple ordered append operation,
;; in which the tail and length of a list are tracked to 
;; prevent O(n) traversing the list everytime an append or 
;; length operation is required. 
;;
;; Note: This class is not included in utilities yet, because
;; there are still many useful methods which could be added
;; to complete this abstraction and make it operate like a
;; common-lisp 'list'.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass smart-list (adt-list adt-queue)
  ((head
	:accessor slist-head
	:initarg :head
	:initform nil
	:documentation "The traditional lisp list. Technically, this stores the pointer to the list's first element.")
   (tail
	:accessor slist-tail
	:initarg :tail
	:initform nil
	:documentation "This is the pointer to the last element in the list")
   (length
	:accessor slist-length
	:initarg :length
	:initform 0
	:documentation "This is the length of the list.")
   (fifo?
	:accessor slist-fifo
	:initarg :fifo?
	:initform T
	:documentation "When used as a queue determines the end to pop
	from.")))

(defun make-smart-list (&key (fifo-queue? T) (initial-contents '()) (copy t))
  "Make a smart-list.
   INITIAL-CONTENTS is a list with which to initialize the smart-list.
   If COPY is t, a copy of INITIAL-CONTENTS is used. 
   If COPY is nil, INITIAL-CONTENTS is used directly in the smart-list data structure
     and should not be modified without using the smart-list abstraction."
  (if initial-contents
	  ;; If there are initial contents, then initialize the smart-list
	  (let ((head (if copy (copy-list initial-contents) initial-contents))
			(tail nil)
			(length 0))
		(loop with curr = head 
		   while curr do
			 (incf length)
			 (setf tail curr)
			 (setf curr (cdr curr)))
		(make-instance 'smart-list :fifo? fifo-queue? :head head :tail tail :length length))
	  ;; Otherwise, save some time and just create it with the default values.
	  (make-instance 'smart-list :fifo? fifo-queue?)))

(defmethod-alias slist-copy coll-copy ((list smart-list) &key &allow-other-keys)
  "Returns a copy of this LIST."
  (with-slots (fifo?) list
	(let ((copy (make-smart-list :fifo-queue? fifo?)))
	  (coll-add-all! copy list)
	  copy)))

(defmethod-alias slist-add! coll-add! ((list smart-list)(element t) &key &allow-other-keys)
  "Appends the ELEMENT to the end of the LIST.
   Returns the modified LIST.
   Complexity: O(1)"
  (slist-push-end element list))

(defmethod-alias slist-add-list! coll-add-all! ((list smart-list)(elements list) &key &allow-other-keys)
  "Appends all of the ELEMENTS to the end of the LIST.
   Returns the modified LIST argument.
   Complexity: O(1)*<length of elements>"
  (loop for element in elements do
	   (slist-push-end element list))
  list)

(defmethod-alias slist-add-coll! coll-add-all! ((list smart-list)(elements adt-collection) &key &allow-other-keys)
  "Appends all of the ELEMENTS to the end of the LIST.
   Returns the modified LIST argument.
   Complexity: O(1)*<length of elements>"
  (coll-map elements 
			(lambda (element) 
			  (slist-push-end element list)))
  list)

(defmethod-alias slist-get coll-get ((list smart-list)(index integer) &key (ignore-index-error nil)(default nil) &allow-other-keys)
  "Returns the element at the specified position in the LIST.
   When (INDEX<0||INDEX>=size), if IGNORE-INDEX-ERROR is nil (default),
   an INDEX-OUT-OF-BOUNDS condition will be thrown. Otherwise, DEFAULT 
   will be returned.
   Complexity: O(n)"
  (handler-case
	  (elt (slist-head list) index)
	(type-error ()
	  (if ignore-index-error default (error 'index-out-of-bounds))
	  )))

(defmethod-alias slist-get-first coll-get-first ((list smart-list) &key (ignore-index-error nil)(default nil) &allow-other-keys)
  "Returns the first element in the LIST.
   When there are no elements in the LIST, if IGNORE-INDEX-ERROR is nil (default),
   an INDEX-OUT-OF-BOUNDS condition will be thrown. Otherwise, DEFAULT
   will be returned.
   Complexity: O(1)"
  (handler-case
	  (elt (slist-head list) 0)
	(type-error ()
	  (if ignore-index-error default (error 'index-out-of-bounds))
	  )))

(defmethod-alias slist-get-last coll-get-last ((list smart-list) &key (ignore-index-error nil)(default nil) &allow-other-keys)
  "Returns the last element in the LIST.
   When there are no elements in the LIST, if IGNORE-INDEX-ERROR is nil (default),
   an INDEX-OUT-OF-BOUNDS condition will be thrown. Otherwise, DEFAULT
   will be returned.
   Complexity: O(1)"
  (handler-case
	  (elt (slist-tail list) 0)
	(type-error ()
	  (if ignore-index-error default (error 'index-out-of-bounds))
	  )))

(defmethod-alias slist-set! coll-set! ((list smart-list)(index integer)(element t) &key (ignore-index-error nil)(default nil) &allow-other-keys)
  "Replaces the element at the specified position in the LIST with the specified ELEMENT.
   When (INDEX<0||INDEX>=size), if IGNORE-INDEX-ERROR is nil (default),
   an INDEX-OUT-OF-BOUNDS condition will be thrown. Otherwise, no replacement 
   will be done, and DEFAULT will be returned.
   Complexity: O(n)"
  (handler-case
	  (setf (elt (slist-head list) index) element)
	(type-error ()
	  (if ignore-index-error  default (error 'index-out-of-bounds))
	  )))

(defmethod-alias slist-insert! coll-insert! ((list smart-list)(index integer)(element t) &key (ignore-index-error nil) &allow-other-keys)
  "Inserts the ELEMENT at the INDEX in the LIST. 
   Shifts elments currently at the index and any subsequent 
   elements to the right (adds one to their indices).
   When (INDEX<0||INDEX>size), if IGNORE-INDEX-ERROR is nil (default),
   an INDEX-OUT-OF-BOUNDS condition will be thrown. Otherwise, no insertion
   will be done. Return value is undefined.
   Complexity: O(n)"
  (cond ((eql index 0)
		 (slist-push element list))
		((eql index (slist-length list))
		 (slist-push-end element list))
		(:otherwise
		 (handler-case
			 (progn
			   (push element (cdr (nthcdr (- index 1) (slist-head list))))
			   (incf (slist-length list)))
		   (type-error ()
			 (unless ignore-index-error 
			   (error 'index-out-of-bounds))) ))
		))

(defmethod coll-remove! ((list smart-list)(element t) &key (from-end nil)(test #'eql)(start 0)(end (slist-length list))(count 1)  &allow-other-keys)
  "Removes the first occurance of ELEMENT from the LIST.
   Returns t if the collection was modified, nil otherwise."
  (with-slots (head tail length) list
	;; TODO: runtime-efficient? implementation

	;; simple implementation
	(let ((orig-length length))
	  (setf head (delete element head :from-end from-end :test test :start start :end end :count count))
	  (setf length (length head))
	  (if (= orig-length length)
		  nil
		  (progn (setf tail (last head)) t))
	  )))

(defmethod-alias slist-remove-from! coll-remove-from! ((list smart-list)(index integer) &key (ignore-index-error nil)(default nil) &allow-other-keys)
  "Removes the element at INDEX from the LIST.
   Throws INDEX-OUT-OF-BOUNDS condition if (INDEX<0||INDEX>=size)
   Complexity: O(n)"
  (with-slots (head tail length) list
	(if (or (< index 0) (>= index length))
		(if ignore-index-error 
			(values nil default)
			(error 'index-out-of-bounds))
		(let ((temp nil))
		  (if (zerop index)
			  (progn
				(setf temp (car head))
				(setf head (cdr head))
				(if (null head) (setf tail nil)))
			  (let ((cons (nthcdr (1- index) head)))
				(setf temp (cadr cons))
				(setf (cdr cons) (cddr cons))
				;; update the tail
				(when (= index (1- length))
				  (setf tail cons))				
				))
			  (decf length)
			  (values t temp)))
	))

(defmethod coll-remove-first! ((list smart-list) &rest rest &key (ignore-index-error nil)(default nil) &allow-other-keys)
  "Removes the first element from the LIST.
   Throws INDEX-OUT-OF-BOUNDS condition if there are no elements in the LIST.
   Complexity: O(1)"
  (declare (ignore ignore-index-error default))
  (apply #'slist-remove-from! list 0 rest))

(defmethod coll-remove-last! ((list smart-list) &rest rest &key (ignore-index-error nil)(default nil) &allow-other-keys)
  "Removes the last element from the LIST.
   Throws INDEX-OUT-OF-BOUNDS condition if there are no elements in the LIST.
   Complexity: O(n)"
  (declare (ignore ignore-index-error default))
  (apply #'slist-remove-from! list (- (slist-length list) 1) rest))

(defmethod-alias slist-clear! coll-clear! ((list smart-list) &key &allow-other-keys)
  "Removes all items from the smart-list and returns the empty smart-list."
  (setf (slist-head list) nil)
  (setf (slist-tail list) nil)
  (setf (slist-length list) 0)
  list)

(defmethod-alias slist-contains? coll-contains? ((list smart-list)(element t) &key (test #'eql) (from-end nil) &allow-other-keys)
  "Returns the index of the first element found, or nil if no element was found."
  (find element (slist-head list) :test test :from-end from-end))

(defmethod coll-size ((list smart-list) &key &allow-other-keys) 
  "Returns the size of the LIST."
  (slist-length list))

(defmethod coll-empty? ((list smart-list) &key &allow-other-keys)
  "Returns t if the LIST contains no elements, nil otherwise."
  (eql (slist-length list) 0))

(defmethod coll-map ((list smart-list)(f function) &key &allow-other-keys)
  "Applies a function F to each element in the list.
   The function is required to take one argument - a list element."
  (mapcar f (slist-head list)))

(defmethod coll-tolist ((list smart-list) &key (copy t) &allow-other-keys)
  "Returns a list. Returns nil if the list is empty.
   When COPY is nil, the list returned is the one that backs the smart-list. 
   Otherwise, when COPY is t (default), the list returned is a copy."
  (if copy
	  (copy-list (slist-head list))
	  (slist-head list)))

(defmethod coll-toarray ((list smart-list) &key (adjustable nil)(fill-pointer nil)(displaced-to nil)(displaced-index-offset 0) &allow-other-keys)
  "Returns an array."
  (make-array (slist-length list) 
			  :initial-contents (slist-head list)
			  :adjustable adjustable
			  :fill-pointer fill-pointer
			  :displaced-to displaced-to
			  :displaced-index-offset displaced-index-offset))

(defmethod queue-remove! ((q smart-list) &key &allow-other-keys)
  "=> A values structure. The first is element at the head of `Q`.
 The second is `T` iff there was an element at the head, `NIL` else.

Removes the head of `Q`."
  (with-slots (fifo?) q
	(let ((idx (if fifo?
				   0
				   (- (coll-size q) 1))))
	  (multiple-value-bind (removed? value) (slist-remove-from! q idx
																:ignore-index-error t
																:default nil)
		(values value removed?)))))

(defmethod queue-peek ((q smart-list) &key &allow-other-keys)
  "=> A values structure. The first is element at the head of `Q`.
 The second is `T` iff there was an element at the head, `NIL` else.

Does not modify `Q`."
  (with-slots (length head tail fifo?) q
	(if (eql length 0)
		(values nil nil)
		(if fifo?
			(values (car head) t)
			(values (car tail) t)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; FUNCTIONS WITH LISPY NAMING CONVENTIONS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun slist-push (item list)
  "Adds ITEM to the front of the smart-list and returns the modified smart-list."
  (if (null (slist-head list))
	  ;; if the list is empty, set both the head and tail to the new one-element list.
	  (setf (slist-tail list) (push item (slist-head list)))
	  ;; if the list is NOT empty, push onto the HEAD.
	  (push item (slist-head list)))
  ;; increment the length
  (incf (slist-length list))
  list)

(defun slist-push-end (item list)
  "Adds ITEM to the tail of the smart-list and returns the modified smart-list."
  (if (null (slist-head list))
	  ;; if the list is empty, set both the head and tail to the new one-element list.
	  (setf (slist-tail list) (push item (slist-head list)))
	  ;; if the list is NOT empty, append onto the TAIL.
	  (progn 
		(setf (cdr (slist-tail list)) (list item))
		(setf (slist-tail list) (cdr (slist-tail list)))

		))
  ;; increment the length
  (incf (slist-length list))
  list)

(defmethod print-object ((list smart-list) stream)
  (if (slist-head list)
	  (format stream "#SMART-LIST~a" (slist-head list))
	  (format stream "#SMART-LIST()" (slist-head list))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ITERATOR
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass smart-list-iterator (adt-collection-iterator)
  ((current 
	:type list
	:initform nil
	:initarg :current
	:accessor slist-it-current
	:documentation "The current element.")
   (reverse-drection
	:type nil
	:initform nil
	:initarg :reverse-drection
	:accessor slist-it-reverse-drection
	:documentation "Traverse the list in the forward (nil) or reverse direction (t).")
   ))

(defmethod make-coll-iterator ((list smart-list) &key (reverse-direction nil) &allow-other-keys)
  "Creates an iterator object that maintains its state over the LIST.
  This method should throw an error if it is not supported."
  (make-instance 
   'linked-hashmap-iterator
   :current
   (if reverse-direction
	   (reverse (slist-head list))
	   (slist-head list))
   :reverse-direction reverse-direction))

(defmethod coll-it-next ((list-iterator smart-list-iterator) &key &allow-other-keys)
  "Returns the next element in the list used to create the LIST-ITERATOR. 
   If there are no more elements, nil is returned.
   This method should throw an error if it is not supported."
  (with-slots (current) list-iterator
	(let ((value (car current)))
	  (setf current (cdr current))
	  value)))

(defmethod coll-it-hasnext ((list-iterator smart-list-iterator) &key &allow-other-keys)
  "Returns t if the list used to create the LIST-ITERATOR has another element.
  This method should throw an error if it is not supported."
  (not (null (slist-it-current list-iterator))))




(check-implementation 'smart-list)
