;;;; Copyright (c) 2013 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   David C. Wang

(in-package #:clos-utils)

;; The current implementation of verbosity only permits two levels:
;; nil (default) - only errors are printed.
;; t - all messages printed.

(defconstant *verbosity-off* 100)
(defconstant *verbosity-errors* 90)
(defconstant *verbosity-all* 0)

(defvar *clos-utils-verbosity-level* *verbosity-errors*
  "Controls the verbosity of CLOS utils. This sets the minimum level of messages that will be formatted.")

(defun set-clos-utils-verbosity-level (level)
  (setf *clos-utils-verbosity-level* level))

(defconstant *error* 90)
(defconstant *info* 0)

(defun clos-utils-format (level format-string &rest rest)
  (if (>= level *clos-utils-verbosity-level*)
	  (apply #'format t format-string rest)))