;;;; Copyright (c) 2011 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   David Wang

(in-package #:abstract-data-types)

(mtk-utils:deftest-suite :test-linked-hashset)

(mtk-utils:deftest (test-lhs-1 :test-linked-hashset) ()
  ;; Test the set for sequential adding of keys
  (let ((set (make-linked-hashset))
		(keys '(a b c d e f g h)))
	;; add a bunch of symbol pairs 
	(loop for key in keys do
		 (coll-add! set key))
	;; Test containment
	(loop for key in keys do
		 (mtk-utils:test (coll-contains? set key
						  )
						 t
						 :test #'eql
						 :fail-info "Failed containment test."))
	;; Test size
	(mtk-utils:test (coll-size set)
					(length keys)
					:test #'eql
					:fail-info "Failed size test.")
	;; Test empty
	(mtk-utils:test (coll-empty? set)
					nil
					:test #'eql
					:fail-info "Failed empty test.")
	;; Test mapping
	(let ((test-keys nil))
		(coll-map set
				 (lambda (key)
				   (setf test-keys (nconc test-keys (list key))) )
				 :reverse-direction nil)
		(mtk-utils:test test-keys
						keys
						:test #'equal
						:fail-info "Failed mapping test."))
	;; Test reverse mapping
	(let ((test-keys nil))
	  (coll-map set 
			   (lambda (key)
				 (push key test-keys))
			   :reverse-direction t)
	  (mtk-utils:test test-keys
					  keys
					  :test #'equal
					  :fail-info "Failed reverse-mapping test."))
	
	))


(mtk-utils:deftest (test-lhs-2 :test-linked-hashset) ()
  ;; Test the set for sequential adding of keys
  (let ((set (make-linked-hashset))
		(keys   '(a b a c d g e f g h))
		(unique-keys   '(b a c d e f g h)) )
	;; add a bunch of symbol pairs 
	(loop for key in keys do
		 (coll-add! set key))
	;; Test containment
	(loop for key in unique-keys do
		 (mtk-utils:test (coll-contains? set key)
						 t
						 :test #'eql
						 :fail-info "Failed containment test."))
	;; Test size
	(mtk-utils:test (coll-size set)
					(length unique-keys)
					:test #'eql
					:fail-info "Failed size test.")
	;; Test empty
	(mtk-utils:test (coll-empty? set)
					nil
					:test #'eql
					:fail-info "Failed empty test.")
	;; Test mapping
	(let ((test-keys nil))
		(coll-map set
				 (lambda (key)
				   (setf test-keys (nconc test-keys (list key))) )
				 :reverse-direction nil)
		(mtk-utils:test test-keys
						unique-keys
						:test #'equal
						:fail-info "Failed mapping test."))
	;; Test reverse mapping
	(let ((test-keys nil))
	  (coll-map set 
			   (lambda (key)
				 (push key test-keys))
			   :reverse-direction t)
	  (mtk-utils:test test-keys
					  unique-keys
					  :test #'equal
					  :fail-info "Failed reverse-mapping test."))
	))