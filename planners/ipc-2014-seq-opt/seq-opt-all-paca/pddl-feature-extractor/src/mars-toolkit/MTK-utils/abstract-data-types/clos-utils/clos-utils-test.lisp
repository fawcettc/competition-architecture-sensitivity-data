;;;; Copyright (c) 2013 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   David C. Wang

(in-package #:clos-utils)

(defclass class1 ()
  ((slot1
	:initarg :slot1)
   (slot2
	:initarg :slot2)))

(defmethod-abstract clos-utils-method1 ((c class1)(key t)) "documentation" (format t "~a~%" key))

(defmethod-abstract clos-utils-method2 ((c class1)(key t)) "documentation" (format t "~a~%" key))

(defclass class2 (class1)
  ((slot3
	:initarg :slot3)
   (slot4
	:initarg :slot4)))

(defmethod clos-utils-method1 ((c class2)(key t)) "documentation" (format t "~a~%" key))

;; (defmethod method2 (name1 name2 &key dog cat) (declare (ignore dog)) "doc4")
;; (defmethod method3 (name1 name2 &key dog cat) (declare (ignore dog)) "doc4" (declare (ignore cat)))