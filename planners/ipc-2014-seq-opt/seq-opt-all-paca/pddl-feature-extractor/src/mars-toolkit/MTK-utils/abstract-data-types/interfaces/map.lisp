;;;; Copyright (c) 2011 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   David Wang

(in-package #:abstract-data-types)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; MAP
;; This class provides the interface specification for
;; data structures that store values indexed by a key.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass adt-map ()
  ())

(defmethod-abstract map-add! ((map adt-map)(key t)(value t) &key (replace t)(default nil) &allow-other-keys)
  "Adds the KEY VALUE pair to the MAP.
   Returns two values. The first value is boolean and represents if the KEY VALUE 
   pair was added. The second value is the original value associated with the KEY, 
   DEFAULT otherwise.
   If REPLACE is t (default), the new key-value pair will replace the old key-value pair."
  (declare (ignore replace default)))

(defmethod-abstract map-add-all! ((map adt-map)(elements adt-map) &rest rest &key &allow-other-keys)
  "Adds all of the ELEMENTS to MAP.
   Returns t if the collection was modified, nil otherwise. 
   Complexity: O(1)*<length of elements>"
  (let ((modified? nil))
	(map-map elements 
			  (lambda (key value) (setf modified? (or (apply #'map-add! map key value rest) modified?))))
	modified?))

(defmethod-abstract map-remove! ((map adt-map)(key t) &key (default nil) &allow-other-keys)
  "Removes the KEY and associated value from the MAP.
   Returns two values. 
     If the KEY is found, the first value will be the value 
   associated with the key, the second value will be t, 
   indicating something was removed.
     If the KEY is not found, the first value will be DEFAULT 
   the second value will be nil."
  (declare (ignore default)))

(defmethod-abstract map-get ((map adt-map)(key t) &key (default nil) &allow-other-keys)
  "Returns the value associated with KEY in the MAP.
   Returns two values. 
     If the KEY is found, the first value will be the value 
   associated with the key, the second value will be t, 
   indicating the key was found.
     If the KEY is not found, the first value will be DEFAULT 
   the second value will be nil."
  (declare (ignore default)))

(defmethod-abstract map-clear! ((map adt-map) &key &allow-other-keys)
  "Clears the MAP of all contents.")

(defmethod-abstract map-contains? ((map adt-map)(key t) &rest rest &key &allow-other-keys)
  "Returns t if the MAP contains the KEY
   and nil if it does not. Equilvanet to (map-contains-key?)"
  (apply #' map-contains-key? map key rest))

(defmethod-abstract map-contains-key? ((map adt-map)(key t) &key &allow-other-keys)
  "Returns t if the MAP contains the KEY
   and nil if it does not.")

(defmethod-abstract map-contains-value? ((map adt-map)(value t) &key &allow-other-keys)
  "Returns two values. 
   The first value is t if the MAP contains the VALUE, DEFAULT otherwise. 
   The second value is the key associated with the VALUE.")

(defmethod-abstract map-size ((map adt-map) &key &allow-other-keys)
  "Returns the number of KEY VALUE pairs stored in the MAP.")

(defmethod-abstract map-empty? ((map adt-map) &key &allow-other-keys)
  "Returns t if the MAP contains no keys, nil otherwise")

(defmethod-abstract map-map ((map adt-map)(f function) &key &allow-other-keys)
  "Applies a function F to each entry in the MAP.
   The function is required to take two arguments - the key and the value.")

(defmacro domap ((map key-var value-var &optional (index-var nil index-var-supplied-p)) &rest rest &key &allow-other-keys)
  "A convenience macro that operates identically to map-map.
   Loops over MAP and binds each key and value in the map, one-at-a-time, with KEY-VAR and VALUE_VAR, respectively. 
   INDEX-VAR starts at 0 for the first element and counts the elements. The INDEX-VAR does
   not necessarily imply the index of the element in MAP.
   The macro uses an implicit nil block, so (return) or (return-from nil) can be used to
   terminate traversal prematurely."
  (when (not (symbolp key-var))
	(error "Cannot bind ~a -- not a symbol" key-var))
  (when (not (symbolp value-var))
	(error "Cannot bind ~a -- not a symbol" value-var))
  (when (and index-var-supplied-p (not (symbolp index-var)))
	(error "Cannot bind ~a -- not a symbol" index-var))
  `(block nil 
	 (if ,index-var-supplied-p
		 (let ((,index-var 0))
		   (map-map ,map #'(lambda (,key-var ,value-var) ,@rest (incf ,index-var))))
		 (map-map ,map #'(lambda (,key-var ,value-var) ,@rest))
		 )))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ITERATOR
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass adt-map-iterator ()
  ())

(defmethod-abstract make-map-iterator ((map adt-map) &key &allow-other-keys)
  "Creates an iterator object that maintains its state over the MAP.
  This method should throw an error if it is not supported.")

(defmethod-abstract map-it-next ((map-iterator adt-map-iterator) &key &allow-other-keys)
  "Returns the next key value pair in the map used to create the MAP-ITERATOR. 
   If there no more values, nil is returned.
   This method should throw an error if it is not supported.")

(defmethod-abstract map-it-hasnext ((map-iterator adt-map-iterator) &key &allow-other-keys)
  "Returns t if the map used to create the MAP-ITERATOR has another key value pair.
  This method should throw an error if it is not supported.")
