;;;; Copyright (c) 2011 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   David Wang

(in-package #:abstract-data-types)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; LINKED-HASHSET
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass linked-hashset (adt-set)
  ((contents
	:type linked-hashmap
	:initform (make-linked-hashmap)
	:initarg :contents
	:accessor lhs-contents
	:documentation "Map to store the key computed from the element and the element.")
   (key-function
	:type function
	:initform (lambda (x) x)
	:initarg :key-function
	:accessor lhs-key-function
	:documentation "Computes the key, given the element, that will be used to test for set membership.")))

(defun make-linked-hashset (&key (initial-contents '())
							(key-function (lambda (x) x)) 
							(test 'eql) (size 67)
							(rehash-size 1.2) (rehash-threshold 0.6407767))
  (let ((set (make-instance 
			  'linked-hashset
			  :contents (make-linked-hashmap :test test :size size 
											 :rehash-size rehash-size
											 :rehash-threshold rehash-threshold)
			  :key-function key-function)))
	(coll-add-all! set initial-contents)
	set))

(defmethod-alias lhs-copy coll-copy ((set linked-hashset) &key &allow-other-keys)
  "Returns a copy of this SET."
  (with-slots (key-function contents) set
	(let ((copy (make-linked-hashset :key-function key-function 
									 :test (hash-table-test contents) 
									 :size (hash-table-size contents) 
									 :rehash-size (hash-table-rehash-size contents) 
									 :rehash-threshold (hash-table-rehash-threshold contents))))
	  (coll-add-all! copy set)
	  copy)))

(defmethod-alias lhs-add! coll-add! ((set linked-hashset)(element t) &key (replace t)(use-key-function t) &allow-other-keys)
  "Adds ELEMENT to the SET.
   Returns t if a change was made to the set. i.e.: 
   If the SET did not originally contain the ELEMENT, t will be returned.
   If the SET contained ELEMENT and REPLACE is t, t will be returned.
   Otherwise, no change will be made to the set, and nil will be returned.
   If USE-KEY-FUNCTION is nil, the ELEMENT is used to test for set membership.
   If USE-KEY-FUNCTION is t (default), the key computed by applying the 
   key-function to the ELEMENT is used to test for set membership."
  (with-slots (contents key-function) set
	(let ((key (if use-key-function (funcall key-function element) element)))
	  (multiple-value-bind (success)
		  (lhm-add! contents key element :replace replace)
		success))))

(defmethod-alias lhs-get coll-get ((set linked-hashset)(element t) &key (use-key-function t)(default nil) &allow-other-keys)
  "Returns the element keyed by ELEMENT in the SET, or DEFAULT if no such key exists.
   Note that this function/method is not specified nor required by the set interface. 
   Implementations of coll-get on sets may return implementation specific values."
  (with-slots (contents key-function) set
	(let ((key (if use-key-function (funcall key-function element) element)))
	  (multiple-value-bind (orig-element)
			(lhm-get contents key :default default)
		orig-element))))

(defmethod-alias lhs-get-first coll-get-first ((set linked-hashset) &key (default nil) &allow-other-keys)
  ""
  (multiple-value-bind (orig-element) 
	  (lhm-get-first (lhs-contents set) :default default)
	orig-element))

(defmethod-alias lhs-get-last coll-get-last ((set linked-hashset) &key (default nil) &allow-other-keys)
  ""
  (multiple-value-bind (orig-element)
	  (lhm-get-last (lhs-contents set) :default default)
	orig-element))

(defmethod-alias lhs-remove! coll-remove! ((set linked-hashset)(element t) &key (use-key-function t)(default nil) &allow-other-keys)
  "Removes ELEMENT from the SET.
   Returns two values. The first value is t if the collection was modified, nil otherwise.
   The second value will be the element keyed by ELEMENT, or DEFAULT if no such key exists."
  (with-slots (contents key-function) set
	(let ((key (if use-key-function (funcall key-function element) element)))
	  (multiple-value-bind (orig-element success)
		  (lhm-remove! contents key :default default)
		(values success orig-element)))))

(defmethod-alias lhs-remove-first! coll-remove-first! ((set linked-hashset) &key (default nil) &allow-other-keys)
  "" 
  (multiple-value-bind (orig-element success)
	  (lhm-remove-first! (lhs-contents set) :default default)
	(values success orig-element)))

(defmethod-alias lhs-remove-last! coll-remove-last! ((set linked-hashset) &key (default nil) &allow-other-keys)
  ""
  (multiple-value-bind (orig-element success) 
	  (lhm-remove-last! (lhs-contents set) :default default)
	(values success orig-element)))

(defmethod-alias lhs-clear! coll-clear! ((set linked-hashset) &key &allow-other-keys)
  "Clears the SET of all contents."
  (lhm-clear! (lhs-contents set)))

(defmethod-alias lhs-contains? coll-contains? ((set linked-hashset)(element t) &key (use-key-function t) &allow-other-keys)
  "Returns t if the SET contains ELEMENT
   and nil if it does not.
   If USE-KEY-FUNCTION is nil, the ELEMENT is used to test for set membership.
   If USE-KEY-FUNCTION is t (default), the key computed by applying the 
   key-function to the ELEMENT is used to test for set membership."
  (with-slots (contents key-function) set
	(let ((key (if use-key-function (funcall key-function element) element)))
	  (lhm-contains? contents key))))

(defmethod-alias lhs-size coll-size ((set linked-hashset) &key &allow-other-keys)
  "Returns the size of the SET."
  (lhm-size (lhs-contents set)))

(defmethod-alias lhs-empty? coll-empty? ((set linked-hashset) &key &allow-other-keys)
  "Returns t if the SET contains no elements, nil otherwise."
  (lhm-empty? (lhs-contents set)))

(defmethod-alias lhs-map coll-map ((set linked-hashset)(f function) &key (reverse-direction nil) &allow-other-keys)
  "Applies a function F to each element in the SET.
   The function is required to take one argument - a set element.
   When REVERSE-DIRECTION is nil (default), the map is iterated from oldest to newest insertion."
  (let ((lhsit (make-lhs-iterator
				set
				:reverse-direction reverse-direction)))
	(loop while (lhs-it-hasnext lhsit) do
		 (multiple-value-bind (element) (lhs-it-next lhsit)
		   (funcall f element)))
	nil))

(defmethod-alias lhs-tolist coll-tolist ((set linked-hashset) &key (reverse-direction nil) &allow-other-keys)
  "Returns a list of the elements in the SET.
   When REVERSE-DIRECTION is nil (default), the map is iterated from oldest to newest insertion."
  (let ((lhslist (make-smart-list)))
	(lhs-map set
			 (lambda (element) (slist-push-end element lhslist))
			 :reverse-direction reverse-direction)
	(slist-head lhslist)))

(defmethod print-object ((set linked-hashset) stream)
  (format stream "#LINKED-HASHSET(")
  (let ((first-loop t))
	(lhs-map set
			 (lambda (element) 
			   (unless first-loop 
				 (format stream " "))
			   (format stream "~a" element)
			   (setf first-loop nil))
			 :reverse-direction nil)
	)
  (format stream ")"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ITERATOR
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass linked-hashset-iterator (linked-hashmap-iterator adt-set-iterator)
  ((iterator
	:type linked-hashmap-iterator
	:initform nil
	:initarg :iterator
	:accessor lhs-it-iterator
	:documentation "The map iterator that backs the hashset iterator.")))

(defmethod-alias make-lhs-iterator make-coll-iterator ((set linked-hashset) &key (reverse-direction nil) &allow-other-keys)
  "Creates an iterator object that maintains its state over the SET.
  This method should throw an error if it is not supported.
  When REVERSE-DIRECTION is nil (default), the map is iterated from oldest to newest insertion."
  (make-instance 
   'linked-hashset-iterator
   :iterator (make-lhm-iterator (lhs-contents set) :reverse-direction reverse-direction)))

(defmethod-alias lhs-it-next coll-it-next ((set-iterator linked-hashset-iterator) &key &allow-other-keys)
  "Returns the next element in the collection used to create the SET-ITERATOR. 
   If there are no more elements, nil is returned.
   This method should throw an error if it is not supported."
  (multiple-value-bind (key element) (lhm-it-next (lhs-it-iterator set-iterator))
	(declare (ignore key))
	element))

(defmethod-alias lhs-it-hasnext coll-it-hasnext ((set-iterator linked-hashset-iterator) &key &allow-other-keys)
  "Returns t if the collection used to create the SET-ITERATOR has another element.
  This method should throw an error if it is not supported."
  (lhm-it-hasnext (lhs-it-iterator set-iterator)))





(check-implementation 'linked-hashset)