;;;; Copyright (c) 2011 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   David Wang

(in-package #:abstract-data-types)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ARRAYLIST
;; This class provides the interface specification for
;; data structures that store collections of elements 
;; in a resizable lisp array.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass arraylist (adt-list)
  ((array
	:accessor arraylist-array
	:initarg :array
	:initform nil
	:documentation "The lisp array that backs the arraylist.")
   (resize-function
	:accessor arraylist-resize-function
	:initarg :resize-function
	:initform nil
	:documentation "This function returns the new array size given the current and desired size.")))

(defun make-arraylist (&key 
					   (initial-capacity 10)
					   (element-type t)
					   (initial-element nil initial-element?)
					   (initial-contents nil initial-contents?)
					   (adjustable t)
					   (resize-function #'arraylist-resize-function-double-grow-only))
  (let* ((num-contents (length initial-contents))
		 (content-init-required nil)
		 (make-array-args (make-smart-list))
		 (array nil))
	;; initialize arguments
	(coll-add-all! make-array-args `(:element-type ,element-type :adjustable ,adjustable))
	(coll-add-all! make-array-args (cond (initial-contents?
									  (if (eql num-contents initial-capacity)
										  `(:initial-contents ,initial-contents :fill-pointer t)
										  (progn (setf content-init-required t)
												 `(:fill-pointer 0))))
									 (initial-element?
									  `(:initial-element ,initial-element :fill-pointer t))
									 (:otherwise `(:fill-pointer 0))))
	;; make the array
	(setf array (apply #'make-array (max initial-capacity num-contents) (coll-tolist make-array-args)))
	;; final content initialization
	(when content-init-required
	  (loop for element in initial-contents do
		   (vector-push element array)))
	(make-instance 
	 'arraylist
	 :array array
	 :resize-function resize-function)))

(defmethod-alias arraylist-copy coll-copy ((list arraylist) &key &allow-other-keys)
  "Returns a copy of this LIST."
  (with-slots (array resize-function) list
	(let* ((new-array (make-array (array-dimensions array)
								  :element-type (array-element-type array)
								  :adjustable (adjustable-array-p array)))
		   (copy (make-instance 'arraylist :array new-array :resize-function resize-function)))
	  (dotimes (i (array-total-size array))
		(setf (row-major-aref new-array i)
			  (row-major-aref array i)))
	  copy)))

(declaim (inline arraylist-resize-function-double-grow-only))
(defun arraylist-resize-function-double-grow-only (curr-cap desired-size)
  "Returns a new size at least large enough to hold desired-size number of elements.
   This function does NOT shrink the array as elements are removed."
  (if (> desired-size curr-cap)
	(let ((new-size (+ curr-cap (ash curr-cap 1))))
	  (max desired-size new-size))
	curr-cap))

;; (declaim (inline arraylist-resize-function-double))
;; (defun arraylist-resize-function-double (curr-cap desired-size)
;;   (cond ((or (equal curr-cap 0)
;; 			 (equal desired-size 0))
;; 		 10)
;; 		((equal curr-cap desired-size)
;; 		 desired-size)
;; 		(:otherwise
;; 		 (let ((temp (* curr-cap (expt 2 (ceiling (log (/ desired-size curr-cap) 2))))))
;; 		   temp
;; 		   ))
;; 		))

(declaim (inline arraylist-ensure-capacity))
(defun arraylist-ensure-capacity (arraylist desired-size)
  (with-slots (array size resize-function) arraylist
	(let* ((curr-cap (array-dimension array 0))
		   (new-size (funcall resize-function curr-cap desired-size)))
	  (when (not (equal new-size curr-cap))
		(setf array (adjust-array array new-size)))
	  )))

(defmethod-alias arraylist-add! coll-add! ((list arraylist)(element t) &key &allow-other-keys)
  "Appends the ELEMENT to the end of the LIST.
   Returns the modified LIST.
   Complexity: O(1)"
  (with-slots (array size) list
	(arraylist-ensure-capacity list (1+ (fill-pointer array)))
	(vector-push element array)
	list))

(defmethod coll-add-all! ((list arraylist)(elements list) &key &allow-other-keys)
  "Appends all of the ELEMENTS to the end of the COLL.
   Returns the modified LIST argument.
   Complexity: O(1)*<length of elements>"
  (loop for element in elements do
	   (arraylist-add! list element))
  list)

(defmethod coll-add-all! ((list arraylist)(elements adt-collection) &key &allow-other-keys)
  "Appends all of the ELEMENTS to the end of the COLL.
   Returns the modified LIST argument.
   Complexity: O(1)*<length of elements>"
  (coll-map elements
			(lambda (element) (arraylist-add! list element)))
  list)

(defmethod-alias arraylist-get coll-get ((list arraylist)(index integer) &key &allow-other-keys)
  "Returns the element at the specified position in the LIST.
   Throws INDEX-OUT-OF-BOUNDS condition if (INDEX<0||INDEX>=size)."
  (handler-case
	  (elt (arraylist-array list) index)
	(type-error ()
		(error 'index-out-of-bounds))))

(defmethod coll-get-first ((list arraylist) &rest rest &key &allow-other-keys)
  "Returns the first element in the LIST.
   Throws INDEX-OUT-OF-BOUNDS condition if there are no elements in the LIST."
  (apply #'arraylist-get list 0 rest))

(defmethod coll-get-last ((list arraylist) &rest rest &key &allow-other-keys)
  "Returns the last element in the LIST.
   Throws INDEX-OUT-OF-BOUNDS condition if there are no elements in the LIST."
  (apply #'arraylist-get list (- (coll-size list) 1) rest))

(defmethod coll-set! ((list arraylist)(index integer)(element t) &key &allow-other-keys)
  "Replaces the element at the specified position in the LIST with the specified ELEMENT.
   Returns the original element.
   Throws INDEX-OUT-OF-BOUNDS condition if (INDEX<0||INDEX>=size)."
  (handler-case
	  (setf (elt (arraylist-array list) index) element)
	(type-error ()
		(error 'index-out-of-bounds))))

(defmethod coll-insert! ((list arraylist)(index integer)(element t) &key &allow-other-keys)
  "Inserts the ELEMENT at the INDEX in the LIST. 
   Shifts elments currently at the index and any subsequent 
   elements to the right (adds one to their indices).
   Throws INDEX-OUT-OF-BOUNDS condition if (INDEX<0||INDEX>size)."
  (with-slots (array) list
	(let ((size (fill-pointer array)))
	  (when (or (< index 0) (> index size))
		(error 'index-out-of-bounds))
	  (arraylist-ensure-capacity list (1+ size))
	  (loop for i from (1- size) downto index do
		   (setf (aref array (1+ i)) (aref array i))))
	(setf (aref array index) element)
	(incf (fill-pointer array))))

(defmethod coll-remove! ((list arraylist)(element t) &key (from-end nil)(test #'eql)(start 0 start?)(end 0 end?)(count 1) &allow-other-keys)
  "Removes the first occurance of ELEMENT from the LIST.
   Returns t if the collection was modified, nil otherwise."
  (with-slots (array) list
	(let ((args (make-smart-list))
		  (last-index (1- (fill-pointer array))))
	  (coll-add! args `(:test ,test :from-end ,from-end :count ,count))
	  (when start?
		(coll-add! args `(:start ,(min start last-index))))
	  (when end?
		(coll-add! args `(:end ,(min (max start end) last-index))))
	  (format t "~a~%" (coll-tolist args))
	  (apply #'remove element array (coll-tolist args)))))

(defmethod-alias arraylist-remove-from! coll-remove-from! ((list arraylist)(index integer) &key &allow-other-keys)
  "Removes the element at INDEX from the LIST.
   Throws INDEX-OUT-OF-BOUNDS condition if (INDEX<0||INDEX>=size)"
  (with-slots (array) list
	(let ((size (fill-pointer array)))
	  (when (or (< index 0) (>= index size))
		(error 'index-out-of-bounds))
	  (arraylist-ensure-capacity list (1- size))
	  (loop for i from index to (- size 2) do
		   (setf (aref array i) (aref array (1+ i)))))
	(decf (fill-pointer array))))

(defmethod coll-remove-first! ((list arraylist) &rest rest &key &allow-other-keys)
  "Removes the first element from the LIST.
   Throws INDEX-OUT-OF-BOUNDS condition if there are no elements in the LIST."
  (apply #'arraylist-remove-from! list 0 rest))

(defmethod coll-remove-last! ((list arraylist) &rest rest &key &allow-other-keys)
  "Removes the last element from the LIST.
   Throws INDEX-OUT-OF-BOUNDS condition if there are no elements in the LIST."
  (apply #'arraylist-remove-from! list (- (coll-size list) 1) rest))

(defmethod coll-clear! ((list arraylist) &key &allow-other-keys)
  "Clears the LIST of all elements."
  (with-slots (array) list
	(arraylist-ensure-capacity list 0)
	(setf (fill-pointer array) 0)))

(defmethod coll-contains? ((list arraylist)(element t) &key (from-end nil)(test #'eql)(start 0 start?)(end 0 end?) &allow-other-keys)
  "Returns the index of the first element found, or nil if no element was found."
  (with-slots (array) list
	(let ((args (make-smart-list))
		  (last-index (1- (fill-pointer array))))
	  (coll-add! args `(:test ,test :from-end ,from-end))
	  (when start?
		(coll-add! args `(:start ,(min start last-index))))
	  (when end?
		(coll-add! args `(:end ,(min (max start end) last-index))))
	  (format t "~a~%" (coll-tolist args))
	  (apply #'position element array (coll-tolist args)))))

(defmethod coll-size ((list arraylist) &key &allow-other-keys) 
  "Returns the size of the LIST."
  (fill-pointer (arraylist-array list)))

(defmethod coll-empty? ((list arraylist) &rest rest &key &allow-other-keys)
  "Returns t if the LIST contains no elements, nil otherwise."
  (eql (apply #'coll-size list rest) 0))

(defmethod coll-map ((list arraylist)(f function) &key &allow-other-keys)
  "Applies a function F to each element in the list.
   The function is required to take one argument - a list element."
  (map nil f (arraylist-array list)))

(defmethod coll-tolist ((list arraylist) &key &allow-other-keys)
  "Returns a list. Returns nil if the list is empty."
  (map 'list #'identity (arraylist-array list)))

(defmethod coll-toarray ((list arraylist) &key (copy t) &allow-other-keys)
  "Returns an array.
   When COPY is nil, the array returned is the one that backs arraylist. 
   Otherwise, when COPY is t (default), the array returned is a copy."
  (if copy
	  (copy-seq (arraylist-array list))
	  (arraylist-array list)))

(defmethod print-object ((list arraylist) stream)
  (progn (format stream "#ARRAYLIST(")
		 (loop for e across (arraylist-array list) 
			with first-loop = t
			do
			  (if first-loop
				  (progn (setf first-loop nil) (format stream "~a" e))
				  (format stream " ~a" e)
				  ))
		 (format stream ")")))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ITERATOR
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass arraylist-iterator (adt-list-iterator)
  ((list
	 :type arraylist
	 :initform nil
	 :initarg :arraylist
	 :accessor arraylist-it-list
	 :documentation "The arraylist backing the iterator.")
   (index 
	:type integer
	:initform 0
	:initarg :index
	:accessor arraylist-it-index
	:documentation "The index of the element to be returned next.")
	(reverse-direction
	 :type nil
	 :initform nil
	 :initarg :reverse-direction
	 :accessor arraylist-it-reverse-direction
	 :documentation "Traverse the list in the forward (nil) or reverse direction (t).")))

(defmethod make-coll-iterator ((list arraylist) &key (reverse-direction nil) &allow-other-keys)
  "Creates an iterator object that maintains its state over the LIST.
  This method should throw an error if it is not supported.
  When REVERSE-DIRECTION is nil (default), the map is iterated from index 0."
  (make-instance
   'arraylist-iterator
   :list list
   :index (if reverse-direction
			  (1- (fill-pointer (arraylist-array list)))
			  0)
   :reverse-direction  reverse-direction
   ))

(defmethod coll-it-next ((list-iterator arraylist-iterator) &key &allow-other-keys)
  "Returns the next element in the list used to create the LIST-ITERATOR. 
   If there are no more elements, nil is returned.
   This method should throw an error if it is not supported."
  (with-slots (list index) list-iterator
	(let ((temp-index index))
	  (incf index)
	  (elt (arraylist-array list) temp-index))))

(defmethod coll-it-hasnext ((list-iterator arraylist-iterator) &key &allow-other-keys)
  "Returns t if the list used to create the LIST-ITERATOR has another element.
  This method should throw an error if it is not supported."
  (with-slots (list index) list-iterator
	(and (>= index 0) (< index (fill-pointer (arraylist-array list)))) ))

(check-implementation 'arraylist)
