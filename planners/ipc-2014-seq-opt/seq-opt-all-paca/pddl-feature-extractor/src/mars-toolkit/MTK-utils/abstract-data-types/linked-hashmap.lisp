;;;; Copyright (c) 2011 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   David Wang

(in-package #:abstract-data-types)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; LINKED-HASHMAP
;; A wrapper for a hash-table that maintains insertion order
;; using a linked list.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass linked-hashmap-node ()
  ((key
	:type t
	:initform nil
	:initarg :key
	:accessor lhm-node-key
	:documentation "The element key.")
   (value
	:type t
	:initform nil
	:initarg :value
	:accessor lhm-node-value
	:documentation "The element value.")
   (previous
	:type linked-hashmap-node
	:initform nil
	:initarg :previous
	:accessor lhm-node-previous
	:documentation "Pointer to the previous node.")
   (next
	:type linked-hashmap-node
	:initform nil
	:initarg :next
	:accessor lhm-node-next
	:documentation "Pointer to the next node.")
   ))

(defclass linked-hashmap (adt-map)
  ((table 
	:type hash-table
	:initform nil
	:initarg :table
	:accessor lhm-table
	:documentation "The lisp hash table storing the hash map.")
   (first
	:type linked-hashmap-node
	:initform nil
	:initarg :first
	:accessor lhm-first
	:documentation "Pointer to the first node in the linked list.")
   (last
	:type linked-hashmap-node
	:initform nil
	:initarg :last
	:accessor lhm-last
	:documentation "Pointer to the last node in the linked list.")
   ))

(defun make-linked-hashmap (&key (test 'eql) (size 67)
							(rehash-size 1.2) (rehash-threshold 0.6407767))
  (make-instance 
   'linked-hashmap
   :table (make-hash-table :test test :size size 
						   :rehash-size rehash-size
						   :rehash-threshold rehash-threshold)
   :first nil
   :last nil))


(defmethod-alias lhm-add! map-add! ((map linked-hashmap)(key t)(value t) &key (replace t)(default nil) &allow-other-keys)
  "Adds the KEY VALUE pair to the MAP.
   Returns two values. The first value represents if a change was made to the map. 
   The second value is the original value associated with the KEY, DEFAULT otherwise. 
   If the map did not originally contain the pair: The returned values will be (t nil).
   If REPLACE is t (default), the new key-value pair will replace the old key-value pair."
  (let ((old-value nil))
	(multiple-value-bind (old-node there?)
		(gethash key (lhm-table map) default)
	  (if there?
		  (setf old-value (lhm-node-value old-node)))
	  (cond 
		;; if the key already exists, do nothing.
		((and there? (not replace)) (values nil old-value))
		;; if the key does not exist, add it.
		(:otherwise
		   (when there? (lhm-remove! map key))
		   (let ((new-node (make-instance 'linked-hashmap-node
										  :key key
										  :value value
										  :previous (lhm-last map)
										  :next nil)))
			 (setf (gethash key (lhm-table map)) new-node)
			 (if (equal (map-size map) 1)
			   (setf (lhm-first map) new-node))
			 (if (not (null (lhm-last map)))
				 (setf (lhm-node-next (lhm-last map)) new-node))
			 (setf (lhm-last map) new-node)
			 (values t old-value)))
		))))

(defmethod-alias lhm-remove! map-remove! ((map linked-hashmap)(key t) &key (default nil) &allow-other-keys)
  "Removes the KEY and associated value from the MAP.
   Returns two values. 
     If the KEY is found, the first value will be the value 
   associated with the key, the second value will be t, 
   indicating something was removed.
     If the KEY is not found, the first value will be DEFAULT 
   the second value will be nil."
  (multiple-value-bind (node there?)
	  (gethash key (lhm-table map) nil)
	(cond ((not there?) (values default nil)) 
		  (:otherwise
		   (let ((value nil))
			 ;; save off the value
			 (setq value (lhm-node-value node))
			 ;; delete it from the list
			 (remhash key (lhm-table map))
			 ;; update the previous pointer
			 (cond ((null (lhm-node-previous node))
					;; it's the first node
					(setf (lhm-first map) 
						  (lhm-node-next node)))
				   (:otherwise
					;; update previous' next pointers
					(setf (lhm-node-next (lhm-node-previous node)) 
						  (lhm-node-next node))))
			 ;; update the next pointer
			 (cond ((null (lhm-node-next node))
					;; its the last node
					(setf (lhm-last map) 
						  (lhm-node-previous node)))
				   (:otherwise
					(setf (lhm-node-previous (lhm-node-next node)) 
						  (lhm-node-previous node))))
			 (values value t)))
		  )))

(defmethod-alias lhm-remove-first! map-remove-first! ((map linked-hashmap) &key (default nil) &allow-other-keys)
  (let ((first (lhm-first map)))
	(if (null first)
		(values nil nil)
		(lhm-remove! map (lhm-node-key first) :default default))
))

(defmethod-alias lhm-remove-last! map-remove-last! ((map linked-hashmap) &key (default nil) &allow-other-keys)
  (let ((last (lhm-last map)))
	(if (null last)
		(values nil nil)
		(lhm-remove! map (lhm-node-key last) :default default))
	))

(defmethod-alias lhm-get map-get ((map linked-hashmap)(key t) &key (default nil) &allow-other-keys)
  "Returns the value associated with KEY in the MAP.
   Returns two values. 
     If the KEY is found, the first value will be the value 
   associated with the key, the second value will be t, 
   indicating the key was found.
     If the KEY is not found, the first value will be DEFAULT 
   the second value will be nil."
  (multiple-value-bind (node there?)
	  (gethash key (lhm-table map) nil)
	(if there?
		(values (lhm-node-value node) t)
		(values default nil))
	))

(defmethod-alias lhm-get-first map-get-first ((map linked-hashmap) &key (default nil) &allow-other-keys)
  (let ((first (lhm-first map)))
	(if first
		(values (lhm-node-value first) t)
		(values default nil)
		)))

(defmethod-alias lhm-get-last map-get-last ((map linked-hashmap) &key (default nil) &allow-other-keys)
  (let ((last (lhm-last map)))
	(if last
		(values (lhm-node-value last) t)
		(values default nil)
		)))

(defmethod-alias lhm-clear! map-clear! ((map linked-hashmap) &key &allow-other-keys)
  "Clears the MAP of all contents."
  ;; clear the table
  (clrhash (lhm-table map))
  ;; reset the pointers
  (setf (lhm-first map) nil)
  (setf (lhm-last map) nil))

(defmethod-alias lhm-contains? map-contains? ((map linked-hashmap)(key t) &rest rest &key &allow-other-keys)
  "Returns t if the MAP contains the KEY
   and nil if it does not."
  (apply #'lhm-contains-key? map key rest))

(defmethod-alias lhm-contains-key? map-contains-key? ((map linked-hashmap)(key t) &key &allow-other-keys)
  "Returns t if the MAP contains the KEY
   and nil if it does not."
  (multiple-value-bind (orig-value there?)
	  (gethash key (lhm-table map))
	(declare (ignore orig-value))
	there?))

(defmethod-alias lhm-contains-value? map-contains-value? ((map linked-hashmap)(value t) &key (test #'eql)(default nil) &allow-other-keys)
  "Returns two values. 
   The first value is t if the MAP contains the VALUE, DEFAULT otherwise. 
   The second value is the key associated with the VALUE."
  (block stop-mapping
	(lhm-map map
			 (lambda (k v)
			   (when (funcall test v value)
				 (return-from stop-mapping (values t k))
				 (return-from stop-mapping (values nil default))))
			 )))

(defmethod-alias lhm-size map-size ((map linked-hashmap) &key &allow-other-keys)
  "Returns the number of KEY VALUE pairs stored in the MAP."
  (hash-table-count (lhm-table map)))

(defmethod-alias lhm-empty? map-empty? ((map linked-hashmap) &key &allow-other-keys)
  "Returns t if the MAP contains no keys, nil otherwise"
  (eql (lhm-size map) 0))

(defmethod-alias lhm-map map-map ((map linked-hashmap)(f function) &key (reverse-direction nil) &allow-other-keys)
  "Applies a function F to each entry in the MAP.
   The function is required to take two arguments - the key and the value.
   When REVERSE-DIRECTION is nil (default), the map is iterated from oldest to newest insertion."
  (let ((lhmit (make-lhm-iterator 
				map 
				:reverse-direction reverse-direction)))
	(loop while (lhm-it-hasnext lhmit) do
		 (multiple-value-bind (key value) (lhm-it-next lhmit)
		   (funcall f key value)))
	nil))

(defmethod print-object ((map linked-hashmap) stream)
  (format stream "#LINKED-HASHMAP(")
  (let ((first-loop t))
	(lhm-map map 
			 (lambda (key value) 
			   (unless first-loop 
				 (format stream " "))
			   (format stream "~a:~a" key value)
			   (setf first-loop nil))
			 :reverse-direction nil))
  (format stream ")"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ITERATOR
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass linked-hashmap-iterator (adt-map-iterator)
  ((current 
	:type linked-hashmap-node
	:initform nil
	:initarg :current
	:accessor lhm-it-current
	:documentation "The current element.")
   (reverse-direction
	:type nil
	:initform nil
	:initarg :reverse-direction
	:accessor lhm-it-reverse-direction
	:documentation "Traverse the linked list in the forward (nil) or reverse direction (t).")
   ))

(defmethod-alias make-lhm-iterator make-map-iterator ((map linked-hashmap) &key (reverse-direction nil) &allow-other-keys)
  "Creates an iterator object that maintains its state over the MAP.
   This method should throw an error if it is not supported.
   When REVERSE-DIRECTION is nil (default), the map is iterated from oldest to newest insertion."
  (make-instance 
   'linked-hashmap-iterator
   :current
   (if reverse-direction
	   (lhm-last map)
	   (lhm-first map))
   :reverse-direction
   reverse-direction
   ))

(defmethod-alias lhm-it-next map-it-next ((map-iterator linked-hashmap-iterator) &key &allow-other-keys)
  "Returns the next key value pair in the map used to create the MAP-ITERATOR. 
   If there no more values, nil is returned.
   This method should throw an error if it is not supported."
  (let* ((current (lhm-it-current map-iterator))
		 (key (lhm-node-key current))
		 (value (lhm-node-value current)))
	(setf (lhm-it-current map-iterator) 
		  (if (lhm-it-reverse-direction map-iterator)
			  (lhm-node-previous current)
			  (lhm-node-next current)))
	(values key value)))

(defmethod-alias lhm-it-hasnext map-it-hasnext ((map-iterator linked-hashmap-iterator) &key &allow-other-keys)
  "Returns t if the map used to create the MAP-ITERATOR has another key value pair.
  This method should throw an error if it is not supported."
  (not (null (lhm-it-current map-iterator))))



(check-implementation 'linked-hashmap)