;;; -*- Mode:Common-Lisp; Package:mtk-utils; Base:10 -*-

;;; Copyright (c) 1999 - 2008 Brian C. Williams, MIT.

;;; This software may not be redistributed, and can only be retained
;;; and used with the explicit written consent of the author,
;;; Brian C. Williams.

;;; This software is made available as is; no guarantee is provided
;;; with respect to performance or correct behavior.
;;; This software may only be used for non-commercial, non-profit,
;;; research activities.

(in-package :mtk-utils)

;;; ***********************************************************************
;;;
;;;                      ***  Things To Do ***
;;;
;;; ***********************************************************************

;;; o Make these functions inline, rather than using macros.

;;; ***********************************************************************
;;;
;;; ***  Constants for Guard Conditions ***
;;;
;;; ***********************************************************************

(defconstant *always-active* *true* 
  "symbol denoting a guard condition that is always true.")

;;; *********************************************************************
;;;
;;;               ***  Guard Conditions ***
;;;
;;; *********************************************************************

(defmacro always-active? (value)

  "Returns true if VALUE indicates that no value was specified,
   and returns nil otherwise. Applies to any variable type."
  
  `(eql ,value *always-active*))

(defmacro contains-always-active? (list)

  "Returns true if VALUE indicates that no value was specified,
   and returns nil otherwise. Applies to any variable type."
  
  `(member *always-active* ,list :test #'eql))
