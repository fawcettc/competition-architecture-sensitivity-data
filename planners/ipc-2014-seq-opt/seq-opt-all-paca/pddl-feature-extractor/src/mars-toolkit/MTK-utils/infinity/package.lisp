;;;; Copyright (c) 2009 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Seung H. Chung

(in-package :cl-user)

(defpackage #:mtk-utils-infinity
  (:nicknames #:mtk-infinity)
  (:use #:cl)
  (:export

   ;; infinity.lisp
   #:*inf*
   #:*-inf*
   #:infinity-to-string

   ;; binfinity.lisp
   #:*infinity*
   #:*-infinity*
   #:infinity?
   #:-infinity?
   #:bound?
   #:b=
   #:b/=
   #:b<
   #:b>
   #:b<=
   #:b>=
   #:b+
   #:bminus
   #:b-
   #:b*
   #:b/
   #:bmin
   #:bmax
   #:brational
   #:brationalize

   ;; convert-infinity.lisp
   #:to-pinf
   #:to-binf
   #:to-rinf

   ))

