;;;; Copyright (c) 2009 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Brian C. Williams, David Wang

(in-package #:mtk-utils-infinity)

;;; ***********************************************************************
;;;
;;;               Bounded Infinity Definitions
;;;
;;; ***********************************************************************
;;; The Constants, Variables, Objects and accessors defined in this file are:
;;;
;;; Constants:
;;;
;;;   *INFINITY* value 'infinity 
;;;                          Highest possible real value.
;;;   *-INFINITY* value '-infinity 
;;;                          Lowest possible real value.

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defconstant *-infinity* :-infinity
	"Lowest possible real value.")

  (defconstant *infinity* :infinity
	"Highest possible real value."))

;;; ***********************************************************************
;;;
;;;                Bound Arithmetic
;;;
;;; ***********************************************************************

(defmacro infinity? (x)
  "Returns True iff X denotes positive infinity."
  `(eql ,x ,*infinity*))

(defmacro -infinity? (x)
  "Returns True iff X denotes negative infinity."
  `(eql ,x ,*-infinity*))

(defmacro bound? (x)
  "Returns True iff X denotes a bound."
  `(or (infinity? ,x)
	   (-infinity? ,x)
	   (realp ,x)))

(defun b= (number &rest more-numbers)
  "The numbers must be of type bounds (realp or infinity).
   There must be at least one bound argument.
   Returns true if all are the same."
  (let ((numbers (list* number more-numbers))
		(prev-type nil)
		(real-count 0))
	(flet ((verify-type (type) (when (null prev-type) (setf prev-type type))
						(unless (eq prev-type type) (return-from b= nil))))
	  (mapc (lambda (n) (cond ((realp n) (incf real-count) (verify-type :real))
							  ((infinity? n) (verify-type :infinity))
							  ((-infinity? n) (verify-type :-infinity))
							  (:otherwise (error "The value ~s is not of type BOUND" n)))) 
			numbers))
	(if (eql real-count 0) 
		t
		(apply #'= numbers))))

(defun b/= (number &rest more-numbers)
  "The numbers must be of type bounds (realp or infinity).
   There must be at least one bound argument.
   Returns true if all are different."
  (let ((numbers (list* number more-numbers))
		(real-count 0)
		(real-list nil)
		(inf-count 0)
		(-inf-count 0))
	(mapc (lambda (n) (cond ((realp n) (incf real-count) 	
							 (setf real-list (nconc real-list `(,n))))
							((infinity? n) (incf inf-count) 
							 (when (> inf-count 1) (return-from b/= nil)))
							((-infinity? n) (incf -inf-count) 
							 (when (> -inf-count 1) (return-from b/= nil)))
							(:otherwise (error "The value ~s is not of type BOUND" n)))) 
		  numbers)
	(if (eql real-count 0)
		t
		(apply #'/= real-list))))

(defun b< (number &rest more-numbers)
  "The numbers must be of type bounds (realp or infinity).
   There must be at least one bound argument.
   Returns true if all are monotonically increasing."
  (let ((realx (realp number))
		(-infx (-infinity? number)))
	(unless (or realx (infinity? number) -infx)
	  (error "The value ~s is not of type BOUND" number))
	(reduce #'(lambda (x y) 
			  (let ((realy (realp y))
					(infy (infinity? y))
					(-infy (-infinity? y)))
				(unless (or realy infy -infy)
				  (error "The value ~s is not of type BOUND" y))
				(if (or (and realx infy)
						(and -infx realy)
						(and -infx infy)
						(and (and realx realy) (< x y)))
					(progn 
					  (setf realx realy)
					  (setf -infx -infy)
					  y)
					(return-from b< nil))))
			more-numbers :initial-value number)
	t))

(defun b> (number &rest more-numbers)
  "The numbers must be of type bounds (realp or infinity).
   There must be at least one bound argument.
   Returns true if all are monotonically decreasing."
  (let ((realx (realp number))
		(infx (infinity? number)))
	(unless (or realx infx (-infinity? number))
	  (error "The value ~s is not of type BOUND" number))
	(reduce #'(lambda (x y) 
			  (let ((realy (realp y))
					(infy (infinity? y))
					(-infy (-infinity? y)))
				(unless (or realy infy -infy)
				  (error "The value ~s is not of type BOUND" y))
				(if (or (and realx -infy)
						(and infx realy)
						(and infx -infy)
						(and (and realx realy) (> x y)))
					(progn 
					  (setf realx realy)
					  (setf infx infy)
					  y)
					(return-from b> nil))))
			more-numbers :initial-value number)
	t))

(defun b<= (number &rest more-numbers)
  "The numbers must be of type bounds (realp or infinity).
   There must be at least one bound argument.
   Returns true if all are monotonically nondecreasing."
  (let ((realx (realp number))
		(infx (infinity? number)))
	(unless (or realx infx (-infinity? number))
	  (error "The value ~s is not of type BOUND" number))
	(reduce #'(lambda (x y) 
			  (let ((realy (realp y))
					(infy (infinity? y))
					(-infy (-infinity? y)))
				(unless (or realy infy -infy)
				  (error "The value ~s is not of type BOUND" y))
				(if (not (or (and realx -infy)
							 (and infx realy)
							 (and infx -infy)
							 (and (and realx realy) (> x y))))
					(progn 
					  (setf realx realy)
					  (setf infx infy)
					  y)
					(return-from b<= nil))))
			more-numbers :initial-value number)
	t))

(defun b>= (number &rest more-numbers)
  "The numbers must be of type bounds (realp or infinity).
   There must be at least one bound argument.
   Returns true if all are monotonically nonincreasing."
  (let ((realx (realp number))
		(-infx (-infinity? number)))
	(unless (or realx (infinity? number) -infx)
	  (error "The value ~s is not of type BOUND" number))
	(reduce #'(lambda (x y) 
			  (let ((realy (realp y))
					(infy (infinity? y))
					(-infy (-infinity? y)))
				(unless (or realy infy -infy)
				  (error "The value ~s is not of type BOUND" y))
				(if (not (or (and realx infy)
							 (and -infx realy)
							 (and -infx infy)
							 (and (and realx realy) (< x y))))
					(progn 
					  (setf realx realy)
					  (setf -infx -infy)
					  y)
					(return-from b>= nil))))
			more-numbers :initial-value number)
	t))

(defun b+ (&rest numbers)
  "The numbers must be of type bounds (realp or infinity).
   Returns the sum."
   (reduce #'(lambda (x y)
			  (cond ((realp y) 
					 (if (realp x) (+ x y) x))
					((infinity? y) 
					 (if (-infinity? x) (error "The sum of ~s and ~s is undefined." x y) y))
					((-infinity? y) 
					 (if (infinity? x) (error "The sum of ~s and ~s is undefined." x y) y))
					(:otherwise (error "The value ~s is not of type BOUND" y))))
		  numbers :initial-value 0))

(defun b- (number &rest more-numbers)
  "The numbers must be of type bounds (realp or infinity).
   Returns the difference."
  (flet ((add-inv (x)
			(cond ((infinity? x) *-infinity*)
				  ((-infinity? x) *infinity*)
				  (:otherwise (- x)))))
	(if (null more-numbers)
		(add-inv number)
		(apply #'b+ number (mapcar #'add-inv more-numbers)))))

;;; This method is kept for backwards compatibility.
;;; The original meaning of b- was unary negation.
;;; The original meaning of bminus was binary subtraction.
;;; This function is now an alias for b-.
(defun bminus (number &rest more-numbers)
  "The numbers must be of type bounds (realp or infinity).
   Returns the difference."
  (apply #'b- number more-numbers))

(defun b* (&rest numbers)
  "The numbers must be of type bounds (realp or infinity).
   Returns the product."
  (let ((xinf nil)) ;; true denotes the value is either infinity or -infinity.
  	(reduce #'(lambda (x y)
				(setf xinf (not (realp x)))
  				(cond ((realp y)
					   (cond ((> y 0)
							  (if xinf x (* x y)))
							 ((< y 0)
							  (cond ((infinity? x) *-infinity*)
									((-infinity? x) *infinity*)
									(:otherwise (* x y))))
							 (t (if xinf
									(error "The product of ~s and ~s is undefined." x y)	 
									(* x y)))))
  					  ((or (infinity? y) (-infinity? y))
					   (cond ((b> x 0) y)
							 ((b< x 0) (b- y))
							 (:otherwise (error "The product of ~s and ~s is undefined." x y))))
					  (:otherwise (error "The value ~s is not of type BOUND" y))
  				))
  			numbers :initial-value 1)))

(defun b/ (number &rest more-numbers)
  "The numbers must be of type bounds (realp or infinity).
   Returns the difference."
  (flet ((mul-inv (x)
			(cond ((infinity? x) 0)
				  ((-infinity? x) -0)
				  (:otherwise (/ x)))))
	(if (null more-numbers)
		(mul-inv number)
		(apply #'b* number (mapcar #'mul-inv more-numbers)))))

(defun bmin (number &rest more-numbers)
  "The numbers must be of type bounds (realp or infinity).
   Returns the minimum bounds."
  (let ((numbers (list* number more-numbers))
		(val *infinity*))
	(mapc #'(lambda (x)
			   (if (-infinity? val)
				  (return-from bmin val))
			   (if (b< x val)
				   (setf val x)))
			numbers)
	val))

(defun bmax (number &rest more-numbers)
  "The numbers must be of type bounds (realp or infinity).
   Returns the maximum bounds."
  (let ((numbers (list* number more-numbers))
		(val *-infinity*))
	(mapc #'(lambda (x)
			   (if (infinity? val)
				  (return-from bmax val))
			   (if (b> x val)
				   (setf val x)))
			numbers)
	val))


;; For future reference, this is the lookup table for how to multiply infinity numbers.
;; ====-INF====
;; -INF = INF
;; -REAL = INF
;; 0 = error
;; +REAL = -INF
;; INF = -INF
;; ====-REAL=====
;; -INF = INF
;; -REAL = REAL
;; 0 = 0
;; +REAL = -REAL
;; INF = -INF
;; ====0====
;; -INF = error
;; -REAL = 0
;; 0 = 0
;; +REAL = 0
;; INF = error
;; ====+REAL====
;; -INF = -INF
;; -REAL = -REAL
;; 0 = 0
;; +REAL = REAL
;; INF = INF
;; ====INF====
;; -INF = -INF
;; -REAL = -INF
;; 0 = error
;; +REAL = INF
;; INF = INF

(defun brational (number)
  (cond ((infinity? number) number)
		((-infinity? number) number)
		(:otherwise (rational number))))

(defun brationalize (number)
  (cond ((infinity? number) number)
		((-infinity? number) number)
		(:otherwise (rationalize number))))