;;;; Copyright (c) 2009 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   Seung H. Chung

(in-package #:mtk-utils-infinity)

;;; The following defconstant *inf* and *-inf* were taken from 
;;; http://gbbopen.org/svn/GBBopen/trunk/source/tools/declared-numerics.lisp
;;; Written by: Dan Corkill
;;; Copyright (C) 2002-2011, Dan Corkill <corkill@GBBopen.org>
;;; Part of the GBBopen Project.
;;; Licensed under Apache License 2.0 (see LICENSE for license information).

(defconstant *inf*
  #+abcl ext:double-float-positive-infinity
  #+allegro excl::*infinity-double*
  #+clozure #.(unwind-protect
				   (progn
					 (ccl:set-fpu-mode :division-by-zero nil)
					 (/ 0d0))
				(ccl:set-fpu-mode :division-by-zero t))
  #+cmu ext:double-float-positive-infinity
  #+digitool-mcl #.(unwind-protect
						(progn
						  (ccl:set-fpu-mode :division-by-zero nil)
						  (/ 0d0))
					 (ccl:set-fpu-mode :division-by-zero t))
  #+(and ecl (not infinity-not-available)) si:double-float-positive-infinity
  #+lispworks #.(read-from-string "10E999")
  #+sbcl sb-ext:double-float-positive-infinity
  #+scl ext:double-float-positive-infinity
  ;; We have to fake infinity
  #+infinity-not-available most-positive-double-float
  #-(or abcl allegro clozure cmu digitool-mcl 
		(and ecl (not infinity-not-available)) 
		lispworks sbcl scl infinity-not-available)
  (need-to-port infinity$$))

(defconstant *-inf*
  #+abcl ext:double-float-negative-infinity
  #+allegro excl::*negative-infinity-double*
  #+clozure #.(unwind-protect
				   (progn
					 (ccl:set-fpu-mode :division-by-zero nil)
					 (/ -0d0))
				(ccl:set-fpu-mode :division-by-zero t))
  #+cmu ext:double-float-negative-infinity
  #+digitool-mcl #.(unwind-protect
						(progn
						  (ccl:set-fpu-mode :division-by-zero nil)
						  (/ -0d0))
					 (ccl:set-fpu-mode :division-by-zero t))
  #+(and ecl (not infinity-not-available)) si:double-float-negative-infinity
  #+lispworks #.(read-from-string "-10E999")
  #+sbcl sb-ext:double-float-negative-infinity
  #+scl ext:double-float-negative-infinity
  ;; We have to fake negative infinity
  #+infinity-not-available most-negative-double-float
  #-(or abcl allegro clozure cmu digitool-mcl 
		(and ecl (not infinity-not-available)) 
		lispworks sbcl scl infinity-not-available)
  (need-to-port -infinity$$))



;;-------------------------------------------------------------------------------
;; Print object
;;-------------------------------------------------------------------------------

(defmethod infinity-to-string (inf)
  (cond ((eql inf *inf*) "inf")
	((eql inf *-inf*) "-inf")
	(t inf)))
