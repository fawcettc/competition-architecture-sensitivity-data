;;;; Copyright (c) 2009 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   David Wang

(in-package #:mtk-utils-infinity)

(declaim (inline to-pinf))
(defun to-pinf (val &key (rational-inf nil) (-rational-inf nil))
  "Converts the given number, if an infinity value, to portable infinity (as defined in infinity.lisp).
   The key RATIONAL-INF defines a lowerbound, over which VAL is considered infinity.
   The key -RATIONAL-INF defines an upperbound, under which VAL is considered negative infinity."
  (cond ((infinity? val) *inf*)
		((-infinity? val) *-inf*)
		((and rational-inf (>= val rational-inf)) *inf*)
		((and -rational-inf (<= val -rational-inf)) *-inf*)
		(:otherwise val)))

(declaim (inline to-binf))
(defun to-binf (val &key (rational-inf nil) (-rational-inf nil))
  "Converts the given number, if an infinity value, to set bounded infinity (as defined in binfinity.lisp).
   The key RATIONAL-INF defines a lowerbound, over which VAL is considered infinity.
   The key -RATIONAL-INF defines an upperbound, under which VAL is considered negative infinity."
  (cond ((eql val *inf*) *infinity*)
		((eql val *-inf*) *-infinity*)
		((and rational-inf (>= val rational-inf)) *infinity*)
		((and -rational-inf (<= val -rational-inf)) *-infinity*)
		(:otherwise val)))

(declaim (inline to-rinf))
(defun to-rinf (val &key (rational-inf most-positive-double-float) (-rational-inf most-negative-double-float))
  "Converts the given number, if an infinity value, to the largest/smallest rational double float. 
   This is useful if the common lisp implementation calls the function (rational) on the value.
   Since infinity is not rational, this allows such computations to continue at the cost of accuracy."
  (cond ((eql val *inf*) rational-inf)
		((eql val *-inf*) -rational-inf)
		((infinity? val) rational-inf)
		((-infinity? val) -rational-inf)
		((>= val rational-inf) rational-inf)
		((<= val -rational-inf) -rational-inf)
		(:otherwise val))
  )

