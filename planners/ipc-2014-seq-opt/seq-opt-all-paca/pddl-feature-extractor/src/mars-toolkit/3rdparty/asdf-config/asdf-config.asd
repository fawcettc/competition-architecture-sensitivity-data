;;; -*- Mode:Common-Lisp; Package:cl-user; Base:10 -*-

;;; Copyright (c) 1999 - 2012 Brian C. Williams, MIT.

;;; This software may not be redistributed, and can only be retained
;;; and used with the explicit written consent of the author,
;;; Brian C. Williams.

;;; This software is made available as is; no guarantee is provided
;;; with respect to performance or correct behavior.

;;; This software may only be used for non-commercial, non-profit,
;;; research activities.

(in-package :cl-user)

(asdf:defsystem #:asdf-config
  :components ((:file "asdf-config"))
)