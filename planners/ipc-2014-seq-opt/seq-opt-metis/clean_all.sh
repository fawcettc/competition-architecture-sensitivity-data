#!/bin/sh

#Author: Andrea F. Bocchese

PLANNER_ROOT=$(pwd)
echo $PLANNER_ROOT
for line in $(find . -name 'Makefile'); do 
	cd $(dirname "${line}")
	make clean
	make distclean
	cd "$PLANNER_ROOT"
done
