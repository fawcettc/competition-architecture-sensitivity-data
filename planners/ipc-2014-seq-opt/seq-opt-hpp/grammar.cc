#define YY_PDDL_Parser_h_included

/*  A Bison++ parser, made from pddl2.y  */

 /* with Bison++ version bison++ Version 1.21-8, adapted from GNU bison by coetmeur@icdc.fr
  */


#line 1 "/usr/local/lib/bison.cc"
/* -*-C-*-  Note some compilers choke on comments on `#line' lines.  */
/* Skeleton output parser for bison,
   Copyright (C) 1984, 1989, 1990 Bob Corbett and Richard Stallman

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 1, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.  */

/* HEADER SECTION */
#if defined( _MSDOS ) || defined(MSDOS) || defined(__MSDOS__) 
#define __MSDOS_AND_ALIKE
#endif
#if defined(_WINDOWS) && defined(_MSC_VER)
#define __HAVE_NO_ALLOCA
#define __MSDOS_AND_ALIKE
#endif

#ifndef alloca
#if defined( __GNUC__)
#define alloca __builtin_alloca

#elif (!defined (__STDC__) && defined (sparc)) || defined (__sparc__) || defined (__sparc)  || defined (__sgi)
#include <alloca.h>

#elif defined (__MSDOS_AND_ALIKE)
#include <malloc.h>
#ifndef __TURBOC__
/* MS C runtime lib */
#define alloca _alloca
#endif

#elif defined(_AIX)
#include <malloc.h>
#pragma alloca

#elif defined(__hpux)
#ifdef __cplusplus
extern "C" {
void *alloca (unsigned int);
};
#else /* not __cplusplus */
void *alloca ();
#endif /* not __cplusplus */

#endif /* not _AIX  not MSDOS, or __TURBOC__ or _AIX, not sparc.  */
#endif /* alloca not defined.  */
#ifdef c_plusplus
#ifndef __cplusplus
#define __cplusplus
#endif
#endif
#ifdef __cplusplus
#ifndef YY_USE_CLASS
#define YY_USE_CLASS
#endif
#else
#ifndef __STDC__
#define const
#endif
#endif
#include <stdio.h>
#define YYBISON 1  

/* #line 73 "/usr/local/lib/bison.cc" */
#line 85 "grammar.cc"
#define YY_PDDL_Parser_ERROR  log_error
#define YY_PDDL_Parser_ERROR_BODY  = 0
#define YY_PDDL_Parser_ERROR_VERBOSE  1
#define YY_PDDL_Parser_LEX  next_token
#define YY_PDDL_Parser_LEX_BODY  = 0
#define YY_PDDL_Parser_DEBUG  1
#define YY_PDDL_Parser_INHERIT  : public HSPS::PDDL_Base
#define YY_PDDL_Parser_CONSTRUCTOR_PARAM  HSPS::StringTable& t
#define YY_PDDL_Parser_CONSTRUCTOR_INIT  : HSPS::PDDL_Base(t), error_flag(false), \
  current_param(0, 0), stored_n_param(0, 0), current_constant_decl(0, 0), \
  current_atom(0), current_atom_stack(0, 0), current_context(0), \
  stored_context(0, 0), current_item(0), current_goal(0, 0), \
  current_preference_name(0), current_entry(0), current_plan_file(0), \
  n_plans_in_current_file(0)
#define YY_PDDL_Parser_MEMBERS  \
public: \
virtual std::ostream& at_position(std::ostream& s) = 0; \
virtual const char*   current_file() = 0; \
bool                  error_flag; \
private: \
HSPS::PDDL_Base::variable_vec current_param; \
HSPS::index_vec               stored_n_param; \
HSPS::PDDL_Base::TypeSet      current_type_set; \
HSPS::PDDL_Base::symbol_vec   current_constant_decl; \
HSPS::index_type              last_n_functions; \
HSPS::PDDL_Base::AtomBase*    current_atom; \
HSPS::PDDL_Base::atom_base_vec current_atom_stack; \
HSPS::PDDL_Base::Context*     current_context; \
HSPS::lvector<Context*> stored_context; \
HSPS::PDDL_Base::DKEL_Item*   current_item; \
HSPS::lvector<ConjunctiveGoal*> current_goal; \
HSPS::PDDL_Base::Symbol*      current_preference_name; \
HSPS::PDDL_Base::HTableEntry* current_entry; \
const char* current_plan_file; \
HSPS::index_type n_plans_in_current_file;
#line 39 "pddl2.y"

#include <stdlib.h>
#include "base.h"
#include <sstream>

#line 45 "pddl2.y"
typedef union {
  HSPS::StringTable::Cell*         sym;
  HSPS::PDDL_Base::Expression*     exp;
  HSPS::PDDL_Base::ListExpression* lst;
  HSPS::PDDL_Base::Relation*       rel;
  HSPS::PDDL_Base::Goal*           goal;
  HSPS::PDDL_Base::Formula*        ff;
  HSPS::PDDL_Base::mode_keyword    tkw;
  HSPS::PDDL_Base::relation_type   rkw;
  HSPS::PDDL_Base::set_constraint_keyword sckw;
  NNTYPE rval;
  int    ival;
  char*  sval;
} yy_PDDL_Parser_stype;
#define YY_PDDL_Parser_STYPE yy_PDDL_Parser_stype

#line 73 "/usr/local/lib/bison.cc"
/* %{ and %header{ and %union, during decl */
#define YY_PDDL_Parser_BISON 1
#ifndef YY_PDDL_Parser_COMPATIBILITY
#ifndef YY_USE_CLASS
#define  YY_PDDL_Parser_COMPATIBILITY 1
#else
#define  YY_PDDL_Parser_COMPATIBILITY 0
#endif
#endif

#if YY_PDDL_Parser_COMPATIBILITY != 0
/* backward compatibility */
#ifdef YYLTYPE
#ifndef YY_PDDL_Parser_LTYPE
#define YY_PDDL_Parser_LTYPE YYLTYPE
#endif
#endif
#ifdef YYSTYPE
#ifndef YY_PDDL_Parser_STYPE 
#define YY_PDDL_Parser_STYPE YYSTYPE
#endif
#endif
#ifdef YYDEBUG
#ifndef YY_PDDL_Parser_DEBUG
#define  YY_PDDL_Parser_DEBUG YYDEBUG
#endif
#endif
#ifdef YY_PDDL_Parser_STYPE
#ifndef yystype
#define yystype YY_PDDL_Parser_STYPE
#endif
#endif
/* use goto to be compatible */
#ifndef YY_PDDL_Parser_USE_GOTO
#define YY_PDDL_Parser_USE_GOTO 1
#endif
#endif

/* use no goto to be clean in C++ */
#ifndef YY_PDDL_Parser_USE_GOTO
#define YY_PDDL_Parser_USE_GOTO 0
#endif

#ifndef YY_PDDL_Parser_PURE

/* #line 117 "/usr/local/lib/bison.cc" */
#line 191 "grammar.cc"

#line 117 "/usr/local/lib/bison.cc"
/*  YY_PDDL_Parser_PURE */
#endif

/* section apres lecture def, avant lecture grammaire S2 */

/* #line 121 "/usr/local/lib/bison.cc" */
#line 200 "grammar.cc"

#line 121 "/usr/local/lib/bison.cc"
/* prefix */
#ifndef YY_PDDL_Parser_DEBUG

/* #line 123 "/usr/local/lib/bison.cc" */
#line 207 "grammar.cc"

#line 123 "/usr/local/lib/bison.cc"
/* YY_PDDL_Parser_DEBUG */
#endif


#ifndef YY_PDDL_Parser_LSP_NEEDED

/* #line 128 "/usr/local/lib/bison.cc" */
#line 217 "grammar.cc"

#line 128 "/usr/local/lib/bison.cc"
 /* YY_PDDL_Parser_LSP_NEEDED*/
#endif



/* DEFAULT LTYPE*/
#ifdef YY_PDDL_Parser_LSP_NEEDED
#ifndef YY_PDDL_Parser_LTYPE
typedef
  struct yyltype
    {
      int timestamp;
      int first_line;
      int first_column;
      int last_line;
      int last_column;
      char *text;
   }
  yyltype;

#define YY_PDDL_Parser_LTYPE yyltype
#endif
#endif
/* DEFAULT STYPE*/
      /* We used to use `unsigned long' as YY_PDDL_Parser_STYPE on MSDOS,
	 but it seems better to be consistent.
	 Most programs should declare their own type anyway.  */

#ifndef YY_PDDL_Parser_STYPE
#define YY_PDDL_Parser_STYPE int
#endif
/* DEFAULT MISCELANEOUS */
#ifndef YY_PDDL_Parser_PARSE
#define YY_PDDL_Parser_PARSE yyparse
#endif
#ifndef YY_PDDL_Parser_LEX
#define YY_PDDL_Parser_LEX yylex
#endif
#ifndef YY_PDDL_Parser_LVAL
#define YY_PDDL_Parser_LVAL yylval
#endif
#ifndef YY_PDDL_Parser_LLOC
#define YY_PDDL_Parser_LLOC yylloc
#endif
#ifndef YY_PDDL_Parser_CHAR
#define YY_PDDL_Parser_CHAR yychar
#endif
#ifndef YY_PDDL_Parser_NERRS
#define YY_PDDL_Parser_NERRS yynerrs
#endif
#ifndef YY_PDDL_Parser_DEBUG_FLAG
#define YY_PDDL_Parser_DEBUG_FLAG yydebug
#endif
#ifndef YY_PDDL_Parser_ERROR
#define YY_PDDL_Parser_ERROR yyerror
#endif
#ifndef YY_PDDL_Parser_PARSE_PARAM
#ifndef __STDC__
#ifndef __cplusplus
#ifndef YY_USE_CLASS
#define YY_PDDL_Parser_PARSE_PARAM
#ifndef YY_PDDL_Parser_PARSE_PARAM_DEF
#define YY_PDDL_Parser_PARSE_PARAM_DEF
#endif
#endif
#endif
#endif
#ifndef YY_PDDL_Parser_PARSE_PARAM
#define YY_PDDL_Parser_PARSE_PARAM void
#endif
#endif
#if YY_PDDL_Parser_COMPATIBILITY != 0
/* backward compatibility */
#ifdef YY_PDDL_Parser_LTYPE
#ifndef YYLTYPE
#define YYLTYPE YY_PDDL_Parser_LTYPE
#else
/* WARNING obsolete !!! user defined YYLTYPE not reported into generated header */
#endif
#endif
#ifndef YYSTYPE
#define YYSTYPE YY_PDDL_Parser_STYPE
#else
/* WARNING obsolete !!! user defined YYSTYPE not reported into generated header */
#endif
#ifdef YY_PDDL_Parser_PURE
#ifndef YYPURE
#define YYPURE YY_PDDL_Parser_PURE
#endif
#endif
#ifdef YY_PDDL_Parser_DEBUG
#ifndef YYDEBUG
#define YYDEBUG YY_PDDL_Parser_DEBUG 
#endif
#endif
#ifndef YY_PDDL_Parser_ERROR_VERBOSE
#ifdef YYERROR_VERBOSE
#define YY_PDDL_Parser_ERROR_VERBOSE YYERROR_VERBOSE
#endif
#endif
#ifndef YY_PDDL_Parser_LSP_NEEDED
#ifdef YYLSP_NEEDED
#define YY_PDDL_Parser_LSP_NEEDED YYLSP_NEEDED
#endif
#endif
#endif
#ifndef YY_USE_CLASS
/* TOKEN C */

/* #line 236 "/usr/local/lib/bison.cc" */
#line 330 "grammar.cc"
#define	TK_OPEN	258
#define	TK_CLOSE	259
#define	TK_OPEN_SQ	260
#define	TK_CLOSE_SQ	261
#define	TK_GREATER	262
#define	TK_GREATEQ	263
#define	TK_LESS	264
#define	TK_LESSEQ	265
#define	TK_COLON	266
#define	TK_HASHT	267
#define	TK_EQ	268
#define	TK_HYPHEN	269
#define	TK_PLUS	270
#define	TK_MUL	271
#define	TK_DIV	272
#define	TK_UMINUS	273
#define	TK_NEW_SYMBOL	274
#define	TK_OBJ_SYMBOL	275
#define	TK_TYPE_SYMBOL	276
#define	TK_PRED_SYMBOL	277
#define	TK_OBJFUN_SYMBOL	278
#define	TK_FUN_SYMBOL	279
#define	TK_VAR_SYMBOL	280
#define	TK_ACTION_SYMBOL	281
#define	TK_MISC_SYMBOL	282
#define	TK_KEYWORD	283
#define	TK_NEW_VAR_SYMBOL	284
#define	TK_PREFERENCE_SYMBOL	285
#define	TK_SET_SYMBOL	286
#define	TK_FLOAT	287
#define	TK_INT	288
#define	TK_STRING	289
#define	KW_REQS	290
#define	KW_CONSTANTS	291
#define	KW_PREDS	292
#define	KW_FUNS	293
#define	KW_TYPES	294
#define	KW_DEFINE	295
#define	KW_DOMAIN	296
#define	KW_ACTION	297
#define	KW_PROCESS	298
#define	KW_EVENT	299
#define	KW_ARGS	300
#define	KW_PRE	301
#define	KW_COND	302
#define	KW_AT_START	303
#define	KW_AT_END	304
#define	KW_OVER_ALL	305
#define	KW_EFFECT	306
#define	KW_INVARIANT	307
#define	KW_DURATION	308
#define	KW_AND	309
#define	KW_OR	310
#define	KW_EXISTS	311
#define	KW_FORALL	312
#define	KW_IMPLY	313
#define	KW_NOT	314
#define	KW_WHEN	315
#define	KW_EITHER	316
#define	KW_PROBLEM	317
#define	KW_FORDOMAIN	318
#define	KW_OBJECTS	319
#define	KW_INIT	320
#define	KW_GOAL	321
#define	KW_LENGTH	322
#define	KW_SERIAL	323
#define	KW_PARALLEL	324
#define	KW_METRIC	325
#define	KW_MINIMIZE	326
#define	KW_MAXIMIZE	327
#define	KW_DURATION_VAR	328
#define	KW_TOTAL_TIME	329
#define	KW_INCREASE	330
#define	KW_DECREASE	331
#define	KW_SCALE_UP	332
#define	KW_SCALE_DOWN	333
#define	KW_ASSIGN	334
#define	KW_TAG	335
#define	KW_NAME	336
#define	KW_VARS	337
#define	KW_SET_CONSTRAINT	338
#define	KW_SETOF	339
#define	KW_AT_LEAST_N	340
#define	KW_AT_MOST_N	341
#define	KW_EXACTLY_N	342
#define	KW_CONTEXT	343
#define	KW_FORMULA	344
#define	KW_IRRELEVANT	345
#define	KW_PLAN	346
#define	KW_HEURISTIC	347
#define	KW_OPT	348
#define	KW_INF	349
#define	KW_FACT	350
#define	KW_SET	351
#define	KW_EXPANSION	352
#define	KW_TASKS	353
#define	KW_PREFERENCE	354
#define	KW_VIOLATED	355
#define	KW_WITHIN	356
#define	KW_ASSOC	357
#define	KW_CONSTRAINTS	358
#define	KW_ALWAYS	359
#define	KW_SOMETIME	360
#define	KW_AT_MOST_ONCE	361
#define	KW_SOMETIME_BEFORE	362
#define	KW_SOMETIME_AFTER	363
#define	KW_ALWAYS_WITHIN	364
#define	KW_IFF	365
#define	KW_FALSE	366
#define	KW_TRUE	367
#define	KW_NUMBER	368
#define	KW_UNDEFINED	369
#define	KW_MINOP	370
#define	KW_MAXOP	371


#line 236 "/usr/local/lib/bison.cc"
 /* #defines tokens */
#else
/* CLASS */
#ifndef YY_PDDL_Parser_CLASS
#define YY_PDDL_Parser_CLASS PDDL_Parser
#endif
#ifndef YY_PDDL_Parser_INHERIT
#define YY_PDDL_Parser_INHERIT
#endif
#ifndef YY_PDDL_Parser_MEMBERS
#define YY_PDDL_Parser_MEMBERS 
#endif
#ifndef YY_PDDL_Parser_LEX_BODY
#define YY_PDDL_Parser_LEX_BODY  
#endif
#ifndef YY_PDDL_Parser_ERROR_BODY
#define YY_PDDL_Parser_ERROR_BODY  
#endif
#ifndef YY_PDDL_Parser_CONSTRUCTOR_PARAM
#define YY_PDDL_Parser_CONSTRUCTOR_PARAM
#endif
#ifndef YY_PDDL_Parser_CONSTRUCTOR_CODE
#define YY_PDDL_Parser_CONSTRUCTOR_CODE
#endif
#ifndef YY_PDDL_Parser_CONSTRUCTOR_INIT
#define YY_PDDL_Parser_CONSTRUCTOR_INIT
#endif
/* choose between enum and const */
#ifndef YY_PDDL_Parser_USE_CONST_TOKEN
#define YY_PDDL_Parser_USE_CONST_TOKEN 0
/* yes enum is more compatible with flex,  */
/* so by default we use it */ 
#endif
#if YY_PDDL_Parser_USE_CONST_TOKEN != 0
#ifndef YY_PDDL_Parser_ENUM_TOKEN
#define YY_PDDL_Parser_ENUM_TOKEN yy_PDDL_Parser_enum_token
#endif
#endif

class YY_PDDL_Parser_CLASS YY_PDDL_Parser_INHERIT
{
public: 
#if YY_PDDL_Parser_USE_CONST_TOKEN != 0
/* static const int token ... */

/* #line 280 "/usr/local/lib/bison.cc" */
#line 494 "grammar.cc"
static const int TK_OPEN;
static const int TK_CLOSE;
static const int TK_OPEN_SQ;
static const int TK_CLOSE_SQ;
static const int TK_GREATER;
static const int TK_GREATEQ;
static const int TK_LESS;
static const int TK_LESSEQ;
static const int TK_COLON;
static const int TK_HASHT;
static const int TK_EQ;
static const int TK_HYPHEN;
static const int TK_PLUS;
static const int TK_MUL;
static const int TK_DIV;
static const int TK_UMINUS;
static const int TK_NEW_SYMBOL;
static const int TK_OBJ_SYMBOL;
static const int TK_TYPE_SYMBOL;
static const int TK_PRED_SYMBOL;
static const int TK_OBJFUN_SYMBOL;
static const int TK_FUN_SYMBOL;
static const int TK_VAR_SYMBOL;
static const int TK_ACTION_SYMBOL;
static const int TK_MISC_SYMBOL;
static const int TK_KEYWORD;
static const int TK_NEW_VAR_SYMBOL;
static const int TK_PREFERENCE_SYMBOL;
static const int TK_SET_SYMBOL;
static const int TK_FLOAT;
static const int TK_INT;
static const int TK_STRING;
static const int KW_REQS;
static const int KW_CONSTANTS;
static const int KW_PREDS;
static const int KW_FUNS;
static const int KW_TYPES;
static const int KW_DEFINE;
static const int KW_DOMAIN;
static const int KW_ACTION;
static const int KW_PROCESS;
static const int KW_EVENT;
static const int KW_ARGS;
static const int KW_PRE;
static const int KW_COND;
static const int KW_AT_START;
static const int KW_AT_END;
static const int KW_OVER_ALL;
static const int KW_EFFECT;
static const int KW_INVARIANT;
static const int KW_DURATION;
static const int KW_AND;
static const int KW_OR;
static const int KW_EXISTS;
static const int KW_FORALL;
static const int KW_IMPLY;
static const int KW_NOT;
static const int KW_WHEN;
static const int KW_EITHER;
static const int KW_PROBLEM;
static const int KW_FORDOMAIN;
static const int KW_OBJECTS;
static const int KW_INIT;
static const int KW_GOAL;
static const int KW_LENGTH;
static const int KW_SERIAL;
static const int KW_PARALLEL;
static const int KW_METRIC;
static const int KW_MINIMIZE;
static const int KW_MAXIMIZE;
static const int KW_DURATION_VAR;
static const int KW_TOTAL_TIME;
static const int KW_INCREASE;
static const int KW_DECREASE;
static const int KW_SCALE_UP;
static const int KW_SCALE_DOWN;
static const int KW_ASSIGN;
static const int KW_TAG;
static const int KW_NAME;
static const int KW_VARS;
static const int KW_SET_CONSTRAINT;
static const int KW_SETOF;
static const int KW_AT_LEAST_N;
static const int KW_AT_MOST_N;
static const int KW_EXACTLY_N;
static const int KW_CONTEXT;
static const int KW_FORMULA;
static const int KW_IRRELEVANT;
static const int KW_PLAN;
static const int KW_HEURISTIC;
static const int KW_OPT;
static const int KW_INF;
static const int KW_FACT;
static const int KW_SET;
static const int KW_EXPANSION;
static const int KW_TASKS;
static const int KW_PREFERENCE;
static const int KW_VIOLATED;
static const int KW_WITHIN;
static const int KW_ASSOC;
static const int KW_CONSTRAINTS;
static const int KW_ALWAYS;
static const int KW_SOMETIME;
static const int KW_AT_MOST_ONCE;
static const int KW_SOMETIME_BEFORE;
static const int KW_SOMETIME_AFTER;
static const int KW_ALWAYS_WITHIN;
static const int KW_IFF;
static const int KW_FALSE;
static const int KW_TRUE;
static const int KW_NUMBER;
static const int KW_UNDEFINED;
static const int KW_MINOP;
static const int KW_MAXOP;


#line 280 "/usr/local/lib/bison.cc"
 /* decl const */
#else
enum YY_PDDL_Parser_ENUM_TOKEN { YY_PDDL_Parser_NULL_TOKEN=0

/* #line 283 "/usr/local/lib/bison.cc" */
#line 617 "grammar.cc"
	,TK_OPEN=258
	,TK_CLOSE=259
	,TK_OPEN_SQ=260
	,TK_CLOSE_SQ=261
	,TK_GREATER=262
	,TK_GREATEQ=263
	,TK_LESS=264
	,TK_LESSEQ=265
	,TK_COLON=266
	,TK_HASHT=267
	,TK_EQ=268
	,TK_HYPHEN=269
	,TK_PLUS=270
	,TK_MUL=271
	,TK_DIV=272
	,TK_UMINUS=273
	,TK_NEW_SYMBOL=274
	,TK_OBJ_SYMBOL=275
	,TK_TYPE_SYMBOL=276
	,TK_PRED_SYMBOL=277
	,TK_OBJFUN_SYMBOL=278
	,TK_FUN_SYMBOL=279
	,TK_VAR_SYMBOL=280
	,TK_ACTION_SYMBOL=281
	,TK_MISC_SYMBOL=282
	,TK_KEYWORD=283
	,TK_NEW_VAR_SYMBOL=284
	,TK_PREFERENCE_SYMBOL=285
	,TK_SET_SYMBOL=286
	,TK_FLOAT=287
	,TK_INT=288
	,TK_STRING=289
	,KW_REQS=290
	,KW_CONSTANTS=291
	,KW_PREDS=292
	,KW_FUNS=293
	,KW_TYPES=294
	,KW_DEFINE=295
	,KW_DOMAIN=296
	,KW_ACTION=297
	,KW_PROCESS=298
	,KW_EVENT=299
	,KW_ARGS=300
	,KW_PRE=301
	,KW_COND=302
	,KW_AT_START=303
	,KW_AT_END=304
	,KW_OVER_ALL=305
	,KW_EFFECT=306
	,KW_INVARIANT=307
	,KW_DURATION=308
	,KW_AND=309
	,KW_OR=310
	,KW_EXISTS=311
	,KW_FORALL=312
	,KW_IMPLY=313
	,KW_NOT=314
	,KW_WHEN=315
	,KW_EITHER=316
	,KW_PROBLEM=317
	,KW_FORDOMAIN=318
	,KW_OBJECTS=319
	,KW_INIT=320
	,KW_GOAL=321
	,KW_LENGTH=322
	,KW_SERIAL=323
	,KW_PARALLEL=324
	,KW_METRIC=325
	,KW_MINIMIZE=326
	,KW_MAXIMIZE=327
	,KW_DURATION_VAR=328
	,KW_TOTAL_TIME=329
	,KW_INCREASE=330
	,KW_DECREASE=331
	,KW_SCALE_UP=332
	,KW_SCALE_DOWN=333
	,KW_ASSIGN=334
	,KW_TAG=335
	,KW_NAME=336
	,KW_VARS=337
	,KW_SET_CONSTRAINT=338
	,KW_SETOF=339
	,KW_AT_LEAST_N=340
	,KW_AT_MOST_N=341
	,KW_EXACTLY_N=342
	,KW_CONTEXT=343
	,KW_FORMULA=344
	,KW_IRRELEVANT=345
	,KW_PLAN=346
	,KW_HEURISTIC=347
	,KW_OPT=348
	,KW_INF=349
	,KW_FACT=350
	,KW_SET=351
	,KW_EXPANSION=352
	,KW_TASKS=353
	,KW_PREFERENCE=354
	,KW_VIOLATED=355
	,KW_WITHIN=356
	,KW_ASSOC=357
	,KW_CONSTRAINTS=358
	,KW_ALWAYS=359
	,KW_SOMETIME=360
	,KW_AT_MOST_ONCE=361
	,KW_SOMETIME_BEFORE=362
	,KW_SOMETIME_AFTER=363
	,KW_ALWAYS_WITHIN=364
	,KW_IFF=365
	,KW_FALSE=366
	,KW_TRUE=367
	,KW_NUMBER=368
	,KW_UNDEFINED=369
	,KW_MINOP=370
	,KW_MAXOP=371


#line 283 "/usr/local/lib/bison.cc"
 /* enum token */
     }; /* end of enum declaration */
#endif
public:
 int YY_PDDL_Parser_PARSE (YY_PDDL_Parser_PARSE_PARAM);
 virtual void YY_PDDL_Parser_ERROR(char *msg) YY_PDDL_Parser_ERROR_BODY;
#ifdef YY_PDDL_Parser_PURE
#ifdef YY_PDDL_Parser_LSP_NEEDED
 virtual int  YY_PDDL_Parser_LEX (YY_PDDL_Parser_STYPE *YY_PDDL_Parser_LVAL,YY_PDDL_Parser_LTYPE *YY_PDDL_Parser_LLOC) YY_PDDL_Parser_LEX_BODY;
#else
 virtual int  YY_PDDL_Parser_LEX (YY_PDDL_Parser_STYPE *YY_PDDL_Parser_LVAL) YY_PDDL_Parser_LEX_BODY;
#endif
#else
 virtual int YY_PDDL_Parser_LEX() YY_PDDL_Parser_LEX_BODY;
 YY_PDDL_Parser_STYPE YY_PDDL_Parser_LVAL;
#ifdef YY_PDDL_Parser_LSP_NEEDED
 YY_PDDL_Parser_LTYPE YY_PDDL_Parser_LLOC;
#endif
 int   YY_PDDL_Parser_NERRS;
 int    YY_PDDL_Parser_CHAR;
#endif
#if YY_PDDL_Parser_DEBUG != 0
 int YY_PDDL_Parser_DEBUG_FLAG;   /*  nonzero means print parse trace     */
#endif
public:
 YY_PDDL_Parser_CLASS(YY_PDDL_Parser_CONSTRUCTOR_PARAM);
public:
 YY_PDDL_Parser_MEMBERS 
};
/* other declare folow */
#if YY_PDDL_Parser_USE_CONST_TOKEN != 0

/* #line 314 "/usr/local/lib/bison.cc" */
#line 768 "grammar.cc"
const int YY_PDDL_Parser_CLASS::TK_OPEN=258;
const int YY_PDDL_Parser_CLASS::TK_CLOSE=259;
const int YY_PDDL_Parser_CLASS::TK_OPEN_SQ=260;
const int YY_PDDL_Parser_CLASS::TK_CLOSE_SQ=261;
const int YY_PDDL_Parser_CLASS::TK_GREATER=262;
const int YY_PDDL_Parser_CLASS::TK_GREATEQ=263;
const int YY_PDDL_Parser_CLASS::TK_LESS=264;
const int YY_PDDL_Parser_CLASS::TK_LESSEQ=265;
const int YY_PDDL_Parser_CLASS::TK_COLON=266;
const int YY_PDDL_Parser_CLASS::TK_HASHT=267;
const int YY_PDDL_Parser_CLASS::TK_EQ=268;
const int YY_PDDL_Parser_CLASS::TK_HYPHEN=269;
const int YY_PDDL_Parser_CLASS::TK_PLUS=270;
const int YY_PDDL_Parser_CLASS::TK_MUL=271;
const int YY_PDDL_Parser_CLASS::TK_DIV=272;
const int YY_PDDL_Parser_CLASS::TK_UMINUS=273;
const int YY_PDDL_Parser_CLASS::TK_NEW_SYMBOL=274;
const int YY_PDDL_Parser_CLASS::TK_OBJ_SYMBOL=275;
const int YY_PDDL_Parser_CLASS::TK_TYPE_SYMBOL=276;
const int YY_PDDL_Parser_CLASS::TK_PRED_SYMBOL=277;
const int YY_PDDL_Parser_CLASS::TK_OBJFUN_SYMBOL=278;
const int YY_PDDL_Parser_CLASS::TK_FUN_SYMBOL=279;
const int YY_PDDL_Parser_CLASS::TK_VAR_SYMBOL=280;
const int YY_PDDL_Parser_CLASS::TK_ACTION_SYMBOL=281;
const int YY_PDDL_Parser_CLASS::TK_MISC_SYMBOL=282;
const int YY_PDDL_Parser_CLASS::TK_KEYWORD=283;
const int YY_PDDL_Parser_CLASS::TK_NEW_VAR_SYMBOL=284;
const int YY_PDDL_Parser_CLASS::TK_PREFERENCE_SYMBOL=285;
const int YY_PDDL_Parser_CLASS::TK_SET_SYMBOL=286;
const int YY_PDDL_Parser_CLASS::TK_FLOAT=287;
const int YY_PDDL_Parser_CLASS::TK_INT=288;
const int YY_PDDL_Parser_CLASS::TK_STRING=289;
const int YY_PDDL_Parser_CLASS::KW_REQS=290;
const int YY_PDDL_Parser_CLASS::KW_CONSTANTS=291;
const int YY_PDDL_Parser_CLASS::KW_PREDS=292;
const int YY_PDDL_Parser_CLASS::KW_FUNS=293;
const int YY_PDDL_Parser_CLASS::KW_TYPES=294;
const int YY_PDDL_Parser_CLASS::KW_DEFINE=295;
const int YY_PDDL_Parser_CLASS::KW_DOMAIN=296;
const int YY_PDDL_Parser_CLASS::KW_ACTION=297;
const int YY_PDDL_Parser_CLASS::KW_PROCESS=298;
const int YY_PDDL_Parser_CLASS::KW_EVENT=299;
const int YY_PDDL_Parser_CLASS::KW_ARGS=300;
const int YY_PDDL_Parser_CLASS::KW_PRE=301;
const int YY_PDDL_Parser_CLASS::KW_COND=302;
const int YY_PDDL_Parser_CLASS::KW_AT_START=303;
const int YY_PDDL_Parser_CLASS::KW_AT_END=304;
const int YY_PDDL_Parser_CLASS::KW_OVER_ALL=305;
const int YY_PDDL_Parser_CLASS::KW_EFFECT=306;
const int YY_PDDL_Parser_CLASS::KW_INVARIANT=307;
const int YY_PDDL_Parser_CLASS::KW_DURATION=308;
const int YY_PDDL_Parser_CLASS::KW_AND=309;
const int YY_PDDL_Parser_CLASS::KW_OR=310;
const int YY_PDDL_Parser_CLASS::KW_EXISTS=311;
const int YY_PDDL_Parser_CLASS::KW_FORALL=312;
const int YY_PDDL_Parser_CLASS::KW_IMPLY=313;
const int YY_PDDL_Parser_CLASS::KW_NOT=314;
const int YY_PDDL_Parser_CLASS::KW_WHEN=315;
const int YY_PDDL_Parser_CLASS::KW_EITHER=316;
const int YY_PDDL_Parser_CLASS::KW_PROBLEM=317;
const int YY_PDDL_Parser_CLASS::KW_FORDOMAIN=318;
const int YY_PDDL_Parser_CLASS::KW_OBJECTS=319;
const int YY_PDDL_Parser_CLASS::KW_INIT=320;
const int YY_PDDL_Parser_CLASS::KW_GOAL=321;
const int YY_PDDL_Parser_CLASS::KW_LENGTH=322;
const int YY_PDDL_Parser_CLASS::KW_SERIAL=323;
const int YY_PDDL_Parser_CLASS::KW_PARALLEL=324;
const int YY_PDDL_Parser_CLASS::KW_METRIC=325;
const int YY_PDDL_Parser_CLASS::KW_MINIMIZE=326;
const int YY_PDDL_Parser_CLASS::KW_MAXIMIZE=327;
const int YY_PDDL_Parser_CLASS::KW_DURATION_VAR=328;
const int YY_PDDL_Parser_CLASS::KW_TOTAL_TIME=329;
const int YY_PDDL_Parser_CLASS::KW_INCREASE=330;
const int YY_PDDL_Parser_CLASS::KW_DECREASE=331;
const int YY_PDDL_Parser_CLASS::KW_SCALE_UP=332;
const int YY_PDDL_Parser_CLASS::KW_SCALE_DOWN=333;
const int YY_PDDL_Parser_CLASS::KW_ASSIGN=334;
const int YY_PDDL_Parser_CLASS::KW_TAG=335;
const int YY_PDDL_Parser_CLASS::KW_NAME=336;
const int YY_PDDL_Parser_CLASS::KW_VARS=337;
const int YY_PDDL_Parser_CLASS::KW_SET_CONSTRAINT=338;
const int YY_PDDL_Parser_CLASS::KW_SETOF=339;
const int YY_PDDL_Parser_CLASS::KW_AT_LEAST_N=340;
const int YY_PDDL_Parser_CLASS::KW_AT_MOST_N=341;
const int YY_PDDL_Parser_CLASS::KW_EXACTLY_N=342;
const int YY_PDDL_Parser_CLASS::KW_CONTEXT=343;
const int YY_PDDL_Parser_CLASS::KW_FORMULA=344;
const int YY_PDDL_Parser_CLASS::KW_IRRELEVANT=345;
const int YY_PDDL_Parser_CLASS::KW_PLAN=346;
const int YY_PDDL_Parser_CLASS::KW_HEURISTIC=347;
const int YY_PDDL_Parser_CLASS::KW_OPT=348;
const int YY_PDDL_Parser_CLASS::KW_INF=349;
const int YY_PDDL_Parser_CLASS::KW_FACT=350;
const int YY_PDDL_Parser_CLASS::KW_SET=351;
const int YY_PDDL_Parser_CLASS::KW_EXPANSION=352;
const int YY_PDDL_Parser_CLASS::KW_TASKS=353;
const int YY_PDDL_Parser_CLASS::KW_PREFERENCE=354;
const int YY_PDDL_Parser_CLASS::KW_VIOLATED=355;
const int YY_PDDL_Parser_CLASS::KW_WITHIN=356;
const int YY_PDDL_Parser_CLASS::KW_ASSOC=357;
const int YY_PDDL_Parser_CLASS::KW_CONSTRAINTS=358;
const int YY_PDDL_Parser_CLASS::KW_ALWAYS=359;
const int YY_PDDL_Parser_CLASS::KW_SOMETIME=360;
const int YY_PDDL_Parser_CLASS::KW_AT_MOST_ONCE=361;
const int YY_PDDL_Parser_CLASS::KW_SOMETIME_BEFORE=362;
const int YY_PDDL_Parser_CLASS::KW_SOMETIME_AFTER=363;
const int YY_PDDL_Parser_CLASS::KW_ALWAYS_WITHIN=364;
const int YY_PDDL_Parser_CLASS::KW_IFF=365;
const int YY_PDDL_Parser_CLASS::KW_FALSE=366;
const int YY_PDDL_Parser_CLASS::KW_TRUE=367;
const int YY_PDDL_Parser_CLASS::KW_NUMBER=368;
const int YY_PDDL_Parser_CLASS::KW_UNDEFINED=369;
const int YY_PDDL_Parser_CLASS::KW_MINOP=370;
const int YY_PDDL_Parser_CLASS::KW_MAXOP=371;


#line 314 "/usr/local/lib/bison.cc"
 /* const YY_PDDL_Parser_CLASS::token */
#endif
/*apres const  */
YY_PDDL_Parser_CLASS::YY_PDDL_Parser_CLASS(YY_PDDL_Parser_CONSTRUCTOR_PARAM) YY_PDDL_Parser_CONSTRUCTOR_INIT
{
#if YY_PDDL_Parser_DEBUG != 0
YY_PDDL_Parser_DEBUG_FLAG=0;
#endif
YY_PDDL_Parser_CONSTRUCTOR_CODE;
};
#endif

/* #line 325 "/usr/local/lib/bison.cc" */
#line 899 "grammar.cc"


#define	YYFINAL		1183
#define	YYFLAG		-32768
#define	YYNTBASE	117

#define YYTRANSLATE(x) ((unsigned)(x) <= 371 ? yytranslate[x] : 376)

static const char yytranslate[] = {     0,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     1,     2,     3,     4,     5,
     6,     7,     8,     9,    10,    11,    12,    13,    14,    15,
    16,    17,    18,    19,    20,    21,    22,    23,    24,    25,
    26,    27,    28,    29,    30,    31,    32,    33,    34,    35,
    36,    37,    38,    39,    40,    41,    42,    43,    44,    45,
    46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
    56,    57,    58,    59,    60,    61,    62,    63,    64,    65,
    66,    67,    68,    69,    70,    71,    72,    73,    74,    75,
    76,    77,    78,    79,    80,    81,    82,    83,    84,    85,
    86,    87,    88,    89,    90,    91,    92,    93,    94,    95,
    96,    97,    98,    99,   100,   101,   102,   103,   104,   105,
   106,   107,   108,   109,   110,   111,   112,   113,   114,   115,
   116
};

#if YY_PDDL_Parser_DEBUG != 0
static const short yyprhs[] = {     0,
     0,     3,     6,     9,    12,    15,    16,    22,    25,    28,
    31,    34,    37,    40,    43,    46,    47,    52,    54,    56,
    58,    60,    62,    64,    66,    68,    70,    72,    74,    79,
    82,    85,    88,    89,    94,    97,    98,    99,   105,   110,
   111,   120,   123,   124,   127,   130,   132,   134,   137,   139,
   140,   146,   147,   153,   154,   160,   161,   170,   172,   173,
   176,   178,   179,   185,   186,   192,   197,   202,   205,   206,
   209,   212,   213,   214,   220,   221,   227,   232,   235,   236,
   239,   241,   244,   246,   248,   250,   252,   253,   260,   261,
   268,   272,   276,   280,   284,   288,   291,   295,   296,   297,
   303,   304,   310,   312,   314,   319,   322,   325,   327,   329,
   331,   333,   335,   337,   339,   341,   343,   345,   346,   352,
   358,   359,   368,   377,   378,   387,   396,   397,   409,   421,
   424,   425,   428,   429,   431,   433,   435,   436,   442,   444,
   446,   452,   461,   463,   465,   467,   469,   471,   476,   482,
   488,   494,   500,   506,   512,   516,   518,   520,   524,   529,
   531,   533,   536,   538,   541,   546,   548,   551,   554,   555,
   556,   557,   569,   570,   571,   581,   582,   589,   590,   591,
   606,   607,   608,   621,   622,   632,   638,   640,   645,   647,
   650,   652,   657,   659,   660,   666,   667,   676,   682,   691,
   692,   698,   699,   700,   701,   711,   720,   725,   727,   730,
   732,   733,   739,   745,   746,   755,   764,   766,   771,   775,
   778,   779,   780,   781,   791,   792,   799,   800,   801,   814,
   815,   824,   826,   828,   834,   836,   841,   843,   845,   850,
   853,   855,   857,   859,   860,   866,   867,   877,   878,   887,
   889,   891,   892,   901,   902,   914,   915,   925,   926,   936,
   937,   947,   948,   961,   962,   975,   976,   989,   991,   996,
   998,  1001,  1003,  1005,  1007,  1013,  1015,  1021,  1023,  1025,
  1031,  1033,  1035,  1036,  1047,  1050,  1051,  1052,  1058,  1064,
  1069,  1075,  1078,  1081,  1084,  1087,  1090,  1093,  1096,  1099,
  1102,  1103,  1108,  1111,  1114,  1117,  1118,  1119,  1125,  1126,
  1136,  1137,  1150,  1151,  1161,  1162,  1172,  1178,  1183,  1191,
  1196,  1204,  1207,  1208,  1210,  1216,  1217,  1223,  1225,  1226,
  1232,  1233,  1239,  1242,  1243,  1244,  1250,  1251,  1260,  1266,
  1268,  1273,  1278,  1283,  1289,  1295,  1301,  1308,  1314,  1316,
  1318,  1320,  1326,  1332,  1334,  1338,  1340,  1342,  1343,  1349,
  1350,  1359,  1363,  1365,  1367,  1368,  1374,  1380,  1385,  1390,
  1395,  1401,  1407,  1408,  1417,  1418,  1427,  1430,  1432,  1433,
  1439,  1441,  1443,  1444,  1452,  1453,  1461,  1462,  1468,  1469,
  1478,  1482,  1485,  1488,  1491,  1494,  1495,  1498,  1501,  1504,
  1505,  1511,  1514,  1520,  1525,  1527,  1528,  1532,  1539,  1543,
  1550,  1554,  1561,  1562,  1565,  1571,  1572,  1575,  1581,  1584,
  1590,  1591,  1594,  1596,  1598,  1600,  1602,  1604,  1606,  1607,
  1613,  1614,  1623,  1624,  1633,  1639,  1640,  1649,  1650,  1662,
  1663,  1675,  1676,  1688,  1697,  1698,  1707,  1708,  1720,  1721,
  1733,  1734,  1749,  1754,  1759,  1761,  1763,  1765,  1768,  1771,
  1772,  1773,  1779,  1780,  1789,  1790,  1791,  1792,  1808,  1809,
  1815,  1818,  1821,  1824,  1825,  1828,  1829,  1837,  1839,  1841,
  1844,  1846,  1847,  1856,  1860,  1861,  1864,  1866,  1867,  1873,
  1878,  1881,  1882,  1883,  1889,  1892,  1894,  1897,  1899,  1901,
  1903,  1904,  1910,  1911,  1920,  1921,  1930,  1933,  1936,  1937,
  1940,  1943,  1944,  1945,  1951,  1953,  1954,  1962,  1963,  1969,
  1970
};

static const short yyrhs[] = {   117,
   118,     0,   117,   253,     0,   117,   340,     0,   117,   346,
     0,   117,   354,     0,     0,     3,    40,   120,   119,     4,
     0,   119,   123,     0,   119,   142,     0,   119,   146,     0,
   119,   125,     0,   119,   133,     0,   119,   151,     0,   119,
   365,     0,   119,   266,     0,     0,     3,    41,   121,     4,
     0,    19,     0,    20,     0,    21,     0,    22,     0,    24,
     0,    25,     0,    26,     0,    27,     0,    30,     0,    19,
     0,    26,     0,     3,    35,   124,     4,     0,   124,    39,
     0,   124,   103,     0,   124,    28,     0,     0,     3,    37,
   126,     4,     0,   126,   127,     0,     0,     0,     3,    19,
   128,   129,     4,     0,   129,   131,    14,    21,     0,     0,
   129,   131,    14,     3,    61,   130,   132,     4,     0,   129,
   131,     0,     0,   131,    29,     0,   131,    25,     0,    29,
     0,    25,     0,   132,    21,     0,    21,     0,     0,     3,
    38,   134,   135,     4,     0,     0,   139,    14,   113,   136,
   135,     0,     0,   139,    14,    21,   137,   135,     0,     0,
   139,    14,     3,    61,   132,     4,   138,   135,     0,   139,
     0,     0,   140,   139,     0,   140,     0,     0,     3,    19,
   141,   129,     4,     0,     0,     3,    39,   143,   144,     4,
     0,   144,   145,    14,    21,     0,   144,   145,    14,    19,
     0,   144,   145,     0,     0,   145,    21,     0,   145,    19,
     0,     0,     0,     3,    36,   147,   149,     4,     0,     0,
     3,    64,   148,   149,     4,     0,   149,   150,    14,    21,
     0,   149,   150,     0,     0,   150,    19,     0,    19,     0,
   150,    20,     0,    20,     0,   152,     0,   282,     0,   291,
     0,     0,     3,    42,   122,   153,   154,     4,     0,     0,
   154,    45,     3,   155,   129,     4,     0,   154,    96,   156,
     0,   154,    51,   211,     0,   154,    46,   159,     0,   154,
    47,   159,     0,   154,    53,   240,     0,   154,   248,     0,
   154,   102,    34,     0,     0,     0,     3,    19,   157,   170,
     4,     0,     0,     3,    31,   158,   170,     4,     0,   160,
     0,   162,     0,     3,    54,   161,     4,     0,     3,     4,
     0,   161,   162,     0,   162,     0,   164,     0,   167,     0,
   183,     0,   201,     0,   203,     0,   176,     0,    48,     0,
    49,     0,    50,     0,     0,     3,    22,   165,   171,     4,
     0,     3,    13,   175,   175,     4,     0,     0,     3,   163,
     3,    22,   166,   171,     4,     4,     0,     3,   163,     3,
    13,   175,   175,     4,     4,     0,     0,     3,    59,     3,
    22,   168,   171,     4,     4,     0,     3,    59,     3,    13,
   175,   175,     4,     4,     0,     0,     3,   163,     3,    59,
     3,    22,   169,   171,     4,     4,     4,     0,     3,   163,
     3,    59,     3,    13,   175,   175,     4,     4,     4,     0,
   170,   172,     0,     0,   171,   175,     0,     0,    25,     0,
    20,     0,    19,     0,     0,     3,    23,   174,   171,     4,
     0,   172,     0,   173,     0,     3,   177,   178,   178,     4,
     0,     3,   163,     3,   177,   178,   178,     4,     4,     0,
     7,     0,     8,     0,     9,     0,    10,     0,    13,     0,
     3,    14,   178,     4,     0,     3,    15,   178,   179,     4,
     0,     3,    14,   178,   178,     4,     0,     3,    16,   178,
   180,     4,     0,     3,    17,   178,   178,     4,     0,     3,
   115,   178,   178,     4,     0,     3,   116,   178,   178,     4,
     0,   178,    17,   178,     0,    33,     0,    32,     0,     3,
    74,     4,     0,     3,   100,    30,     4,     0,   181,     0,
   178,     0,   178,   179,     0,   178,     0,   178,   180,     0,
     3,    24,   182,     4,     0,    24,     0,    25,   182,     0,
    20,   182,     0,     0,     0,     0,     3,    84,   184,   308,
   311,     3,    22,   185,   171,     4,     4,     0,     0,     0,
     3,    57,     3,   186,   129,     4,   187,   194,     4,     0,
     0,     3,    58,   188,   197,   195,     4,     0,     0,     0,
     3,   163,     3,    84,   189,   308,   311,     3,    22,   190,
   171,     4,     4,     4,     0,     0,     0,     3,   163,     3,
    57,     3,   191,   129,     4,   192,   194,     4,     4,     0,
     0,     3,   163,     3,    58,   193,   197,   195,     4,     4,
     0,     3,    58,   197,   195,     4,     0,   195,     0,     3,
    54,   196,     4,     0,   198,     0,   198,   196,     0,   198,
     0,     3,    54,   313,     4,     0,   314,     0,     0,     3,
    22,   199,   171,     4,     0,     0,     3,    59,     3,    22,
   200,   171,     4,     4,     0,     3,    13,   175,   175,     4,
     0,     3,    59,     3,    13,   175,   175,     4,     4,     0,
     0,     3,    55,   202,   207,     4,     0,     0,     0,     0,
     3,    56,     3,   204,   129,     4,   205,   206,     4,     0,
     3,    54,   313,     3,    55,   207,     4,     4,     0,     3,
    55,   207,     4,     0,   208,     0,   208,   207,     0,   208,
     0,     0,     3,    22,   209,   171,     4,     0,     3,    13,
   175,   175,     4,     0,     0,     3,    59,     3,    22,   210,
   171,     4,     4,     0,     3,    59,     3,    13,   175,   175,
     4,     4,     0,   213,     0,     3,    54,   212,     4,     0,
     3,    54,     4,     0,   212,   213,     0,     0,     0,     0,
     3,    57,     3,   214,   129,     4,   215,   220,     4,     0,
     0,     3,    60,   216,   222,   221,     4,     0,     0,     0,
     3,   163,     3,    57,     3,   217,   129,     4,   218,   220,
     4,     4,     0,     0,     3,   163,     3,    60,   219,   222,
   221,     4,     0,   224,     0,   233,     0,     3,    60,   222,
   221,     4,     0,   221,     0,     3,    54,   223,     4,     0,
   224,     0,   314,     0,     3,    54,   313,     4,     0,   224,
   223,     0,   224,     0,   225,     0,   230,     0,     0,     3,
    22,   226,   171,     4,     0,     0,     3,    79,     3,    23,
   227,   171,     4,   229,     4,     0,     0,     3,   163,     3,
    22,   228,   171,     4,     4,     0,   175,     0,   114,     0,
     0,     3,    59,     3,    22,   231,   171,     4,     4,     0,
     0,     3,   163,     3,    59,     3,    22,   232,   171,     4,
     4,     4,     0,     0,     3,    75,     3,    24,   234,   171,
     4,   178,     4,     0,     0,     3,    76,     3,    24,   235,
   171,     4,   178,     4,     0,     0,     3,    79,     3,    24,
   236,   171,     4,   178,     4,     0,     0,     3,   163,     3,
    75,     3,    24,   237,   171,     4,   178,     4,     4,     0,
     0,     3,   163,     3,    76,     3,    24,   238,   171,     4,
   178,     4,     4,     0,     0,     3,   163,     3,    79,     3,
    24,   239,   171,     4,   178,     4,     4,     0,   242,     0,
     3,    54,   241,     4,     0,   242,     0,   242,   241,     0,
   243,     0,   244,     0,   246,     0,     3,    13,    73,   178,
     4,     0,   178,     0,     3,   245,    73,   178,     4,     0,
     9,     0,    10,     0,     3,   247,    73,   178,     4,     0,
     7,     0,     8,     0,     0,     3,    97,   249,   309,   310,
    98,     3,   250,     4,     4,     0,   251,   250,     0,     0,
     0,     3,    26,   252,   171,     4,     0,     3,    40,   254,
   255,     4,     0,     3,    62,   121,     4,     0,   255,     3,
    63,   121,     4,     0,   255,   123,     0,   255,   146,     0,
   255,   256,     0,   255,   266,     0,   255,   277,     0,   255,
   280,     0,   255,   298,     0,   255,   291,     0,   255,   365,
     0,     0,     3,    65,   257,     4,     0,   257,   264,     0,
   257,   262,     0,   257,   258,     0,     0,     0,     3,    22,
   259,   171,     4,     0,     0,     3,   121,   281,     3,    22,
   260,   171,     4,     4,     0,     0,     3,   121,   281,     3,
    59,     3,    22,   261,   171,     4,     4,     4,     0,     0,
     3,    13,     3,    23,   263,   171,     4,    20,     4,     0,
     0,     3,    13,     3,    24,   265,   171,     4,   281,     4,
     0,     3,    13,    24,   281,     4,     0,     3,    66,   268,
     4,     0,     3,    66,     3,    54,   267,     4,     4,     0,
     3,   103,   268,     4,     0,     3,   103,     3,    54,   267,
     4,     4,     0,   267,   268,     0,     0,   274,     0,     3,
    99,    19,   270,     4,     0,     0,     3,    55,   269,   273,
     4,     0,   274,     0,     0,     3,    54,   271,   273,     4,
     0,     0,     3,    55,   272,   273,     4,     0,   274,   273,
     0,     0,     0,     3,    22,   275,   171,     4,     0,     0,
     3,    59,     3,    22,   276,   171,     4,     4,     0,     3,
    13,   175,   175,     4,     0,   176,     0,     3,   104,   270,
     4,     0,     3,   105,   270,     4,     0,     3,   106,   270,
     4,     0,     3,   107,   270,   270,     4,     0,     3,   108,
   270,   270,     4,     0,     3,   101,   281,   270,     4,     0,
     3,   109,   281,   270,   270,     4,     0,     3,    70,   278,
   279,     4,     0,    71,     0,    72,     0,   178,     0,     3,
    67,    68,    33,     4,     0,     3,    67,    69,    33,     4,
     0,    33,     0,    33,    17,    33,     0,    32,     0,    94,
     0,     0,     3,    52,   283,   302,   284,     0,     0,    83,
   285,     3,   331,    33,   332,     4,     4,     0,    89,   286,
     4,     0,   111,     0,   112,     0,     0,     3,    22,   287,
   170,     4,     0,     3,    13,   172,   172,     4,     0,     3,
    59,   286,     4,     0,     3,    54,   290,     4,     0,     3,
    55,   290,     4,     0,     3,    58,   286,   286,     4,     0,
     3,   110,   286,   286,     4,     0,     0,     3,    57,     3,
   288,   129,     4,   286,     4,     0,     0,     3,    56,     3,
   289,   129,     4,   286,     4,     0,   290,   286,     0,   286,
     0,     0,     3,    90,   292,   302,   293,     0,   294,     0,
   296,     0,     0,    42,     3,    26,   295,   171,     4,     4,
     0,     0,    95,     3,    22,   297,   171,     4,     4,     0,
     0,     3,    52,   299,   302,   300,     0,     0,    83,   301,
     3,   331,    33,   332,     4,     4,     0,    89,   286,     4,
     0,   302,   303,     0,   302,   304,     0,   302,   305,     0,
   302,   307,     0,     0,    80,   121,     0,    81,    19,     0,
    81,    27,     0,     0,    82,     3,   306,   129,     4,     0,
    88,   314,     0,    88,     3,    54,   313,     4,     0,    82,
     3,   129,     4,     0,   308,     0,     0,    88,   314,   312,
     0,    88,     3,    54,   313,     4,   312,     0,    46,   314,
   311,     0,    46,     3,    54,   313,     4,   311,     0,    47,
   314,   311,     0,    47,     3,    54,   313,     4,   311,     0,
     0,    88,   314,     0,    88,     3,    54,   313,     4,     0,
     0,    46,   314,     0,    46,     3,    54,   313,     4,     0,
    47,   314,     0,    47,     3,    54,   313,     4,     0,     0,
   313,   314,     0,   314,     0,   315,     0,   319,     0,   324,
     0,   327,     0,   330,     0,     0,     3,    22,   316,   171,
     4,     0,     0,     3,   163,     3,    22,   317,   171,     4,
     4,     0,     0,     3,    65,     3,    22,   318,   171,     4,
     4,     0,     3,    13,   175,   175,     4,     0,     0,     3,
    59,     3,    22,   320,   171,     4,     4,     0,     0,     3,
   163,     3,    59,     3,    22,   321,   171,     4,     4,     4,
     0,     0,     3,    65,     3,    59,     3,    22,   322,   171,
     4,     4,     4,     0,     0,     3,    59,     3,    65,     3,
    22,   323,   171,     4,     4,     4,     0,     3,    59,     3,
    13,   175,   175,     4,     4,     0,     0,     3,    66,     3,
    22,   325,   171,     4,     4,     0,     0,     3,    66,     3,
    59,     3,    22,   326,   171,     4,     4,     4,     0,     0,
     3,    59,     3,    66,     3,    22,   328,   171,     4,     4,
     4,     0,     0,     3,    59,     3,    66,     3,    59,     3,
    22,   329,   171,     4,     4,     4,     4,     0,     3,    21,
    25,     4,     0,     3,    21,   173,     4,     0,    85,     0,
    86,     0,    87,     0,   332,   333,     0,   332,   336,     0,
     0,     0,     3,    22,   334,   171,     4,     0,     0,     3,
    59,     3,    22,   335,   171,     4,     4,     0,     0,     0,
     0,     3,    84,    82,     3,   337,   129,     4,   338,   311,
     3,    22,   339,   171,     4,     4,     0,     0,     3,    91,
   341,   342,     4,     0,   343,   342,     0,    93,   342,     0,
   344,   342,     0,     0,    81,   121,     0,     0,   281,    11,
     3,    26,   345,   171,     4,     0,   347,     0,   351,     0,
   348,   347,     0,   348,     0,     0,   281,    11,     3,    26,
   349,   171,     4,   350,     0,     5,   281,     6,     0,     0,
   352,   351,     0,   352,     0,     0,     3,    26,   353,   171,
     4,     0,     3,    92,   355,     4,     0,   355,   356,     0,
     0,     0,     3,   357,   359,     4,   358,     0,    93,   281,
     0,   281,     0,   359,   360,     0,   360,     0,   361,     0,
   363,     0,     0,     3,    22,   362,   171,     4,     0,     0,
     3,    59,     3,    22,   364,   171,     4,     4,     0,     0,
     3,    96,   366,   367,   309,   311,   368,     4,     0,    81,
    19,     0,    81,    27,     0,     0,   368,   371,     0,   368,
   369,     0,     0,     0,     3,   121,   370,   171,     4,     0,
   121,     0,     0,     3,    84,   372,   308,   311,   373,     4,
     0,     0,     3,   121,   374,   171,     4,     0,     0,     3,
    59,     3,   121,   375,   171,     4,     4,     0
};

#endif

#if YY_PDDL_Parser_DEBUG != 0
static const short yyrline[] = { 0,
   105,   107,   108,   109,   110,   111,   114,   118,   120,   121,
   122,   123,   124,   125,   126,   127,   130,   138,   140,   141,
   142,   143,   144,   145,   146,   147,   150,   152,   167,   171,
   173,   174,   175,   180,   184,   186,   189,   194,   204,   209,
   213,   217,   221,   224,   237,   244,   256,   265,   270,   277,
   282,   285,   291,   291,   321,   321,   351,   351,   355,   358,
   360,   363,   368,   380,   385,   388,   402,   415,   423,   426,
   432,   438,   443,   448,   449,   453,   456,   477,   488,   491,
   501,   510,   517,   528,   530,   531,   536,   541,   550,   555,
   559,   560,   561,   562,   563,   564,   565,   570,   573,   582,
   586,   591,   597,   599,   600,   603,   607,   609,   612,   614,
   619,   620,   621,   622,   628,   633,   637,   643,   648,   654,
   662,   666,   672,   683,   688,   694,   702,   706,   712,   767,
   774,   777,   784,   787,   795,   799,   807,   813,   833,   838,
   843,   850,   859,   864,   868,   872,   876,   882,   887,   891,
   895,   899,   903,   907,   911,   915,   919,   923,   927,   931,
   937,   942,   948,   953,   959,   964,   970,   975,   979,   985,
   991,   995,  1010,  1015,  1024,  1034,  1038,  1044,  1049,  1053,
  1069,  1074,  1083,  1095,  1099,  1108,  1110,  1113,  1115,  1118,
  1120,  1123,  1125,  1128,  1133,  1140,  1144,  1151,  1160,  1197,
  1202,  1210,  1211,  1216,  1225,  1237,  1239,  1240,  1243,  1245,
  1248,  1253,  1259,  1267,  1271,  1277,  1287,  1289,  1290,  1300,
  1302,  1305,  1311,  1319,  1330,  1334,  1339,  1344,  1352,  1365,
  1369,  1376,  1377,  1380,  1382,  1385,  1387,  1390,  1392,  1395,
  1397,  1400,  1402,  1405,  1411,  1421,  1425,  1452,  1457,  1469,
  1474,  1480,  1486,  1496,  1501,  1513,  1518,  1525,  1529,  1536,
  1540,  1547,  1551,  1558,  1562,  1569,  1573,  1582,  1584,  1587,
  1589,  1592,  1594,  1595,  1598,  1604,  1611,  1618,  1620,  1623,
  1630,  1632,  1635,  1645,  1668,  1670,  1673,  1678,  1691,  1695,
  1703,  1705,  1706,  1707,  1708,  1709,  1710,  1711,  1712,  1713,
  1714,  1717,  1721,  1723,  1724,  1725,  1728,  1733,  1744,  1748,
  1761,  1765,  1780,  1785,  1797,  1802,  1815,  1831,  1833,  1834,
  1835,  1838,  1840,  1843,  1848,  1854,  1858,  1866,  1871,  1875,
  1881,  1885,  1893,  1901,  1904,  1909,  1916,  1920,  1927,  1935,
  1939,  1943,  1947,  1951,  1955,  1959,  1963,  1999,  2003,  2013,
  2024,  2039,  2044,  2057,  2059,  2060,  2061,  2066,  2072,  2075,
  2080,  2087,  2095,  2100,  2104,  2108,  2113,  2117,  2121,  2126,
  2131,  2135,  2139,  2143,  2158,  2162,  2179,  2184,  2190,  2197,
  2200,  2202,  2205,  2210,  2222,  2227,  2239,  2246,  2249,  2254,
  2261,  2269,  2271,  2272,  2273,  2274,  2277,  2284,  2291,  2298,
  2303,  2309,  2311,  2314,  2324,  2326,  2329,  2331,  2332,  2333,
  2334,  2335,  2336,  2339,  2341,  2342,  2345,  2347,  2348,  2349,
  2350,  2353,  2355,  2358,  2360,  2361,  2362,  2363,  2366,  2371,
  2378,  2382,  2388,  2392,  2398,  2408,  2413,  2420,  2424,  2430,
  2434,  2440,  2444,  2450,  2460,  2465,  2471,  2475,  2483,  2488,
  2494,  2498,  2506,  2513,  2520,  2525,  2529,  2535,  2537,  2538,
  2540,  2545,  2550,  2554,  2561,  2567,  2575,  2579,  2595,  2605,
  2618,  2620,  2625,  2626,  2629,  2637,  2642,  2654,  2660,  2667,
  2669,  2672,  2677,  2708,  2710,  2713,  2715,  2718,  2723,  2755,
  2759,  2761,  2764,  2769,  2776,  2782,  2788,  2790,  2793,  2795,
  2798,  2804,  2813,  2819,  2828,  2837,  2844,  2850,  2854,  2857,
  2859,  2860,  2863,  2873,  2880,  2895,  2902,  2916,  2926,  2934,
  2943
};

static const char * const yytname[] = {   "$","error","$illegal.","TK_OPEN",
"TK_CLOSE","TK_OPEN_SQ","TK_CLOSE_SQ","TK_GREATER","TK_GREATEQ","TK_LESS","TK_LESSEQ",
"TK_COLON","TK_HASHT","TK_EQ","TK_HYPHEN","TK_PLUS","TK_MUL","TK_DIV","TK_UMINUS",
"TK_NEW_SYMBOL","TK_OBJ_SYMBOL","TK_TYPE_SYMBOL","TK_PRED_SYMBOL","TK_OBJFUN_SYMBOL",
"TK_FUN_SYMBOL","TK_VAR_SYMBOL","TK_ACTION_SYMBOL","TK_MISC_SYMBOL","TK_KEYWORD",
"TK_NEW_VAR_SYMBOL","TK_PREFERENCE_SYMBOL","TK_SET_SYMBOL","TK_FLOAT","TK_INT",
"TK_STRING","KW_REQS","KW_CONSTANTS","KW_PREDS","KW_FUNS","KW_TYPES","KW_DEFINE",
"KW_DOMAIN","KW_ACTION","KW_PROCESS","KW_EVENT","KW_ARGS","KW_PRE","KW_COND",
"KW_AT_START","KW_AT_END","KW_OVER_ALL","KW_EFFECT","KW_INVARIANT","KW_DURATION",
"KW_AND","KW_OR","KW_EXISTS","KW_FORALL","KW_IMPLY","KW_NOT","KW_WHEN","KW_EITHER",
"KW_PROBLEM","KW_FORDOMAIN","KW_OBJECTS","KW_INIT","KW_GOAL","KW_LENGTH","KW_SERIAL",
"KW_PARALLEL","KW_METRIC","KW_MINIMIZE","KW_MAXIMIZE","KW_DURATION_VAR","KW_TOTAL_TIME",
"KW_INCREASE","KW_DECREASE","KW_SCALE_UP","KW_SCALE_DOWN","KW_ASSIGN","KW_TAG",
"KW_NAME","KW_VARS","KW_SET_CONSTRAINT","KW_SETOF","KW_AT_LEAST_N","KW_AT_MOST_N",
"KW_EXACTLY_N","KW_CONTEXT","KW_FORMULA","KW_IRRELEVANT","KW_PLAN","KW_HEURISTIC",
"KW_OPT","KW_INF","KW_FACT","KW_SET","KW_EXPANSION","KW_TASKS","KW_PREFERENCE",
"KW_VIOLATED","KW_WITHIN","KW_ASSOC","KW_CONSTRAINTS","KW_ALWAYS","KW_SOMETIME",
"KW_AT_MOST_ONCE","KW_SOMETIME_BEFORE","KW_SOMETIME_AFTER","KW_ALWAYS_WITHIN",
"KW_IFF","KW_FALSE","KW_TRUE","KW_NUMBER","KW_UNDEFINED","KW_MINOP","KW_MAXOP",
"pddl_declarations","pddl_domain","domain_elements","domain_name","any_symbol",
"action_symbol","domain_requires","require_list","domain_predicates","predicate_list",
"predicate_decl","@1","typed_param_list","@2","typed_param_sym_list","non_empty_type_name_list",
"domain_functions","@3","function_list","@4","@5","@6","function_decl_list",
"function_decl","@7","domain_types","@8","typed_type_list","primitive_type_list",
"domain_constants","@9","@10","typed_constant_list","ne_constant_sym_list","domain_structure",
"action_declaration","@11","action_elements","@12","action_set_name","@13","@14",
"action_condition","empty_action_condition","action_condition_list","single_action_condition",
"timing_keyword","positive_atom_condition","@15","@16","negative_atom_condition",
"@17","@18","flat_atom_argument_list","atom_argument_list","flat_atom_argument",
"functional_atom_argument","@19","atom_argument","numeric_condition","relation_keyword",
"d_expression","d_sum","d_product","d_function","d_argument_list","set_condition",
"@20","@21","@22","@23","@24","@25","@26","@27","@28","@29","universal_condition_body",
"one_or_more_condition_atoms","quantified_condition_atom_list","one_or_more_context_atoms",
"quantified_condition_atom","@30","@31","disjunctive_condition","@32","disjunctive_set_condition",
"@33","@34","existential_condition_body","disjunction_list","disjunction_atom",
"@35","@36","action_effect","action_effect_list","single_action_effect","@37",
"@38","@39","@40","@41","@42","quantified_effect_body","one_or_more_atomic_effects",
"effect_conditions","atomic_effect_list","atomic_effect","positive_atom_effect",
"@43","@44","@45","object_assignment_value","negative_atom_effect","@46","@47",
"numeric_effect","@48","@49","@50","@51","@52","@53","action_duration","action_duration_list",
"action_duration_exp","action_exact_duration","action_min_duration","less_or_lesseq",
"action_max_duration","greater_or_greatereq","action_expansion","@54","task_list",
"task","@55","pddl_problem","problem_name","problem_elements","initial_state",
"init_elements","init_atom","@56","@57","@58","init_object_function","@59","init_function",
"@60","goal_spec","goal_spec_list","single_goal_spec","@61","new_goal","@62",
"@63","new_goal_list","new_single_goal","@64","@65","metric_spec","metric_keyword",
"metric_expression","length_spec","numeric_value","domain_invariant","@66","domain_invariant_body",
"@67","fol_formula","@68","@69","@70","fol_formula_list","irrelevant_item","@71",
"irrelevant_item_content","irrelevant_action","@72","irrelevant_atom","@73",
"problem_invariant","@74","problem_invariant_body","@75","dkel_element_list",
"dkel_tag_spec","dkel_name_spec","dkel_vars_spec","@76","dkel_context_spec",
"req_vars_spec","opt_vars_spec","opt_context_and_precondition_spec","opt_context_spec",
"opt_precondition_spec","context_list","context_atom","positive_context_atom",
"@77","@78","@79","negative_context_atom","@80","@81","@82","@83","positive_context_goal_atom",
"@84","@85","negative_context_goal_atom","@86","@87","type_constraint_atom",
"set_constraint_type","invariant_set","invariant_atom","@88","@89","invariant_set_of_atoms",
"@90","@91","@92","pddl_plan","@93","plan_elements","plan_name","plan_step",
"@94","ipc_plan","ipc_plan_step_list","ipc_plan_step","@95","opt_step_duration",
"ipc_plan_step_seq","simple_plan_step","@96","heuristic_table","table_entry_list",
"table_entry","@97","entry_value","ne_entry_atom_list","entry_atom","pos_entry_atom",
"@98","neg_entry_atom","@99","reference_set","@100","opt_set_name","reference_list",
"reference","@101","simple_reference_set","@102","simple_reference_set_body",
"@103","@104",""
};
#endif

static const short yyr1[] = {     0,
   117,   117,   117,   117,   117,   117,   118,   119,   119,   119,
   119,   119,   119,   119,   119,   119,   120,   121,   121,   121,
   121,   121,   121,   121,   121,   121,   122,   122,   123,   124,
   124,   124,   124,   125,   126,   126,   128,   127,   129,   130,
   129,   129,   129,   131,   131,   131,   131,   132,   132,   134,
   133,   136,   135,   137,   135,   138,   135,   135,   135,   139,
   139,   141,   140,   143,   142,   144,   144,   144,   144,   145,
   145,   145,   147,   146,   148,   146,   149,   149,   149,   150,
   150,   150,   150,   151,   151,   151,   153,   152,   155,   154,
   154,   154,   154,   154,   154,   154,   154,   154,   157,   156,
   158,   156,   159,   159,   159,   160,   161,   161,   162,   162,
   162,   162,   162,   162,   163,   163,   163,   165,   164,   164,
   166,   164,   164,   168,   167,   167,   169,   167,   167,   170,
   170,   171,   171,   172,   172,   172,   174,   173,   175,   175,
   176,   176,   177,   177,   177,   177,   177,   178,   178,   178,
   178,   178,   178,   178,   178,   178,   178,   178,   178,   178,
   179,   179,   180,   180,   181,   181,   182,   182,   182,   184,
   185,   183,   186,   187,   183,   188,   183,   189,   190,   183,
   191,   192,   183,   193,   183,   194,   194,   195,   195,   196,
   196,   197,   197,   199,   198,   200,   198,   198,   198,   202,
   201,   203,   204,   205,   203,   206,   206,   206,   207,   207,
   209,   208,   208,   210,   208,   208,   211,   211,   211,   212,
   212,   214,   215,   213,   216,   213,   217,   218,   213,   219,
   213,   213,   213,   220,   220,   221,   221,   222,   222,   223,
   223,   224,   224,   226,   225,   227,   225,   228,   225,   229,
   229,   231,   230,   232,   230,   234,   233,   235,   233,   236,
   233,   237,   233,   238,   233,   239,   233,   240,   240,   241,
   241,   242,   242,   242,   243,   243,   244,   245,   245,   246,
   247,   247,   249,   248,   250,   250,   252,   251,   253,   254,
   255,   255,   255,   255,   255,   255,   255,   255,   255,   255,
   255,   256,   257,   257,   257,   257,   259,   258,   260,   258,
   261,   258,   263,   262,   265,   264,   264,   266,   266,   266,
   266,   267,   267,   268,   268,   269,   268,   270,   271,   270,
   272,   270,   273,   273,   275,   274,   276,   274,   274,   274,
   274,   274,   274,   274,   274,   274,   274,   277,   278,   278,
   279,   280,   280,   281,   281,   281,   281,   283,   282,   285,
   284,   284,   286,   286,   287,   286,   286,   286,   286,   286,
   286,   286,   288,   286,   289,   286,   290,   290,   292,   291,
   293,   293,   295,   294,   297,   296,   299,   298,   301,   300,
   300,   302,   302,   302,   302,   302,   303,   304,   304,   306,
   305,   307,   307,   308,   309,   309,   310,   310,   310,   310,
   310,   310,   310,   311,   311,   311,   312,   312,   312,   312,
   312,   313,   313,   314,   314,   314,   314,   314,   316,   315,
   317,   315,   318,   315,   315,   320,   319,   321,   319,   322,
   319,   323,   319,   319,   325,   324,   326,   324,   328,   327,
   329,   327,   330,   330,   331,   331,   331,   332,   332,   332,
   334,   333,   335,   333,   337,   338,   339,   336,   341,   340,
   342,   342,   342,   342,   343,   345,   344,   346,   346,   347,
   347,   349,   348,   350,   350,   351,   351,   353,   352,   354,
   355,   355,   357,   356,   358,   358,   359,   359,   360,   360,
   362,   361,   364,   363,   366,   365,   367,   367,   367,   368,
   368,   368,   370,   369,   369,   372,   371,   374,   373,   375,
   373
};

static const short yyr2[] = {     0,
     2,     2,     2,     2,     2,     0,     5,     2,     2,     2,
     2,     2,     2,     2,     2,     0,     4,     1,     1,     1,
     1,     1,     1,     1,     1,     1,     1,     1,     4,     2,
     2,     2,     0,     4,     2,     0,     0,     5,     4,     0,
     8,     2,     0,     2,     2,     1,     1,     2,     1,     0,
     5,     0,     5,     0,     5,     0,     8,     1,     0,     2,
     1,     0,     5,     0,     5,     4,     4,     2,     0,     2,
     2,     0,     0,     5,     0,     5,     4,     2,     0,     2,
     1,     2,     1,     1,     1,     1,     0,     6,     0,     6,
     3,     3,     3,     3,     3,     2,     3,     0,     0,     5,
     0,     5,     1,     1,     4,     2,     2,     1,     1,     1,
     1,     1,     1,     1,     1,     1,     1,     0,     5,     5,
     0,     8,     8,     0,     8,     8,     0,    11,    11,     2,
     0,     2,     0,     1,     1,     1,     0,     5,     1,     1,
     5,     8,     1,     1,     1,     1,     1,     4,     5,     5,
     5,     5,     5,     5,     3,     1,     1,     3,     4,     1,
     1,     2,     1,     2,     4,     1,     2,     2,     0,     0,
     0,    11,     0,     0,     9,     0,     6,     0,     0,    14,
     0,     0,    12,     0,     9,     5,     1,     4,     1,     2,
     1,     4,     1,     0,     5,     0,     8,     5,     8,     0,
     5,     0,     0,     0,     9,     8,     4,     1,     2,     1,
     0,     5,     5,     0,     8,     8,     1,     4,     3,     2,
     0,     0,     0,     9,     0,     6,     0,     0,    12,     0,
     8,     1,     1,     5,     1,     4,     1,     1,     4,     2,
     1,     1,     1,     0,     5,     0,     9,     0,     8,     1,
     1,     0,     8,     0,    11,     0,     9,     0,     9,     0,
     9,     0,    12,     0,    12,     0,    12,     1,     4,     1,
     2,     1,     1,     1,     5,     1,     5,     1,     1,     5,
     1,     1,     0,    10,     2,     0,     0,     5,     5,     4,
     5,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     0,     4,     2,     2,     2,     0,     0,     5,     0,     9,
     0,    12,     0,     9,     0,     9,     5,     4,     7,     4,
     7,     2,     0,     1,     5,     0,     5,     1,     0,     5,
     0,     5,     2,     0,     0,     5,     0,     8,     5,     1,
     4,     4,     4,     5,     5,     5,     6,     5,     1,     1,
     1,     5,     5,     1,     3,     1,     1,     0,     5,     0,
     8,     3,     1,     1,     0,     5,     5,     4,     4,     4,
     5,     5,     0,     8,     0,     8,     2,     1,     0,     5,
     1,     1,     0,     7,     0,     7,     0,     5,     0,     8,
     3,     2,     2,     2,     2,     0,     2,     2,     2,     0,
     5,     2,     5,     4,     1,     0,     3,     6,     3,     6,
     3,     6,     0,     2,     5,     0,     2,     5,     2,     5,
     0,     2,     1,     1,     1,     1,     1,     1,     0,     5,
     0,     8,     0,     8,     5,     0,     8,     0,    11,     0,
    11,     0,    11,     8,     0,     8,     0,    11,     0,    11,
     0,    14,     4,     4,     1,     1,     1,     2,     2,     0,
     0,     5,     0,     8,     0,     0,     0,    15,     0,     5,
     2,     2,     2,     0,     2,     0,     7,     1,     1,     2,
     1,     0,     8,     3,     0,     2,     1,     0,     5,     4,
     2,     0,     0,     5,     2,     1,     2,     1,     1,     1,
     0,     5,     0,     8,     0,     8,     2,     2,     0,     2,
     2,     0,     0,     5,     1,     0,     7,     0,     5,     0,
     8
};

static const short yydefact[] = {     6,
     0,     0,   356,   354,   357,     1,     2,     0,     3,     4,
   478,   481,   479,   487,     5,   488,     0,   469,   492,     0,
     0,   480,     0,   486,   133,     0,    16,   301,   474,     0,
   355,     0,     0,     0,     0,     0,     0,     0,   474,     0,
     0,   474,   474,   493,   490,   491,   482,     0,   489,   136,
   135,   134,   139,   140,   132,    18,    19,    20,    21,    22,
    23,    24,    25,    26,     0,     0,     0,     7,     8,    11,
    12,     9,    10,    13,    84,    15,    85,    86,    14,     0,
   289,   292,   293,   294,   295,   296,   297,   299,   298,   300,
   475,   472,     0,   470,   471,   473,     0,   133,   137,    17,
   290,    33,    73,    36,    50,    64,     0,   358,    75,     0,
   379,   505,     0,   387,     0,   306,     0,     0,     0,     0,
     0,   498,   499,   500,     0,   133,     0,    79,     0,    59,
    69,    27,    28,    87,   396,    79,     0,   340,     0,   324,
   396,   509,     0,     0,   396,     0,     0,     0,     0,   349,
   350,     0,   476,   501,     0,     0,   497,   485,     0,    29,
    32,    30,    31,     0,     0,    34,    35,     0,     0,    58,
    61,    72,    98,     0,     0,   143,   144,   145,   146,   147,
   335,   115,   116,   117,   323,   326,     0,     0,     0,     0,
     0,     0,     0,     0,     0,     0,     0,   318,     0,     0,
   406,   323,   320,     0,   291,     0,   302,   305,   304,   303,
     0,     0,     0,   166,   157,   156,   351,   160,     0,   133,
   133,     0,     0,   496,   494,     0,   483,   138,    74,    81,
    83,    78,    37,    62,    51,     0,    60,    65,    68,     0,
     0,     0,     0,   360,     0,     0,   359,   392,   393,   394,
   395,    76,     0,   133,     0,   334,     0,     0,     0,     0,
     0,   328,     0,     0,     0,     0,     0,     0,     0,     0,
     0,   380,   381,   382,   507,   508,     0,   405,   416,     0,
   389,     0,   388,     0,   307,     0,   352,   353,     0,     0,
     0,     0,   169,     0,     0,     0,     0,     0,   348,     0,
     0,   503,   495,     0,     0,    80,    82,    43,    43,     0,
    54,    52,     0,    71,    70,     0,    88,     0,   202,   202,
     0,     0,     0,     0,    96,   397,   398,   399,   400,     0,
     0,   402,   424,   425,   426,   427,   428,     0,   363,   364,
     0,     0,     0,     0,     0,   322,     0,     0,   334,   337,
     0,     0,   329,   331,   341,   342,   343,     0,     0,     0,
   147,     0,     0,     0,     0,    43,     0,   512,     0,     0,
     0,     0,     0,   133,     0,     0,     0,     0,     0,   169,
   169,     0,   158,     0,     0,     0,   155,   477,   502,   133,
   484,    77,     0,     0,     0,    59,    59,    67,    66,   283,
    89,     0,    93,   103,   104,   109,   110,   114,   111,   112,
   113,    94,     0,    92,   217,   232,   242,   243,   233,     0,
   276,    95,   268,   272,   273,   274,     0,    91,    97,    43,
     0,     0,     0,   429,     0,     0,     0,     0,     0,     0,
   365,     0,     0,     0,     0,     0,     0,     0,   362,   339,
   336,   319,   327,   333,   133,   325,   346,   334,   334,   344,
   345,     0,     0,   141,   383,   385,     0,     0,   414,     0,
   321,     0,   391,   313,   315,     0,     0,     0,   148,     0,
   161,     0,   163,     0,     0,   168,   167,   165,   159,     0,
     0,     0,    38,    47,    46,    42,    63,    49,     0,    55,
    53,   406,    43,   106,   147,   118,   202,   200,     0,     0,
   176,     0,   170,     0,   244,   221,     0,     0,   225,     0,
     0,     0,     0,   281,   282,   278,   279,     0,     0,     0,
     0,    99,   101,     0,   455,   456,   457,     0,     0,     0,
     0,   133,     0,     0,   423,     0,     0,     0,     0,     0,
   131,   378,     0,     0,   375,   373,     0,     0,     0,     0,
     0,     0,   347,     0,   133,   133,   404,     0,     0,   506,
   515,   511,   510,     0,   133,   133,   317,   308,   309,     0,
   150,   162,   149,   164,   151,   152,   153,   154,     0,     0,
    45,    44,    56,    48,   413,     0,     0,   133,     0,     0,
   108,     0,   203,   173,     0,     0,     0,     0,   133,   219,
     0,   222,     0,     0,     0,     0,     0,     0,     0,     0,
     0,   270,     0,     0,   131,   131,   401,   460,     0,   453,
   454,     0,   403,   422,     0,   436,     0,     0,   433,     0,
   445,     0,   431,     0,     0,     0,   369,   377,   370,    43,
    43,     0,   368,     0,     0,   330,   332,     0,     0,     0,
     0,   516,   513,   460,     0,     0,   133,     0,   504,     0,
    39,    59,     0,     0,     0,     0,    90,     0,     0,   105,
   107,     0,     0,   210,    43,    43,     0,     0,   193,     0,
   124,   416,   147,   121,     0,   184,     0,   178,     0,     0,
   218,   220,    43,   252,     0,     0,   238,   256,   258,   246,
   260,   248,     0,     0,   230,     0,     0,     0,     0,   269,
   271,     0,     0,     0,     0,     0,   435,   430,     0,   133,
     0,     0,   133,     0,   133,     0,   133,     0,   367,   366,
   130,     0,     0,   371,   372,   338,   142,     0,     0,   415,
     0,   133,     0,     0,     0,     0,   311,    40,    57,     0,
   416,     0,   416,     0,   421,     0,   120,   119,     0,   211,
     0,   201,   209,     0,     0,     0,     0,     0,   189,     0,
   133,     0,     0,   133,   181,     0,     0,     0,   245,     0,
   133,     0,     0,     0,   237,   133,   133,   133,   133,   133,
   227,     0,     0,     0,     0,     0,   275,   277,   280,   100,
   102,     0,     0,   458,   459,     0,     0,   442,   449,     0,
     0,   440,     0,   447,     0,   438,     0,     0,   384,   386,
   416,     0,     0,     0,     0,     0,   133,     0,     0,   409,
     0,   411,     0,     0,     0,   407,   286,     0,   133,     0,
   204,   174,     0,     0,   194,     0,     0,   177,     0,     0,
     0,     0,     0,    43,     0,     0,   127,   416,   223,     0,
     0,     0,     0,     0,   226,     0,     0,     0,     0,     0,
    43,   254,     0,   262,   264,   266,   461,     0,     0,   361,
     0,     0,   133,   133,     0,     0,   133,     0,   133,     0,
   133,     0,     0,     0,   514,   390,   314,   316,   310,     0,
     0,     0,     0,     0,     0,   417,     0,   419,     0,     0,
   286,     0,     0,     0,   214,     0,     0,   192,     0,   133,
     0,     0,   191,     0,     0,     0,   171,     0,     0,     0,
     0,     0,   133,     0,     0,     0,   239,     0,     0,   241,
     0,     0,     0,     0,     0,     0,     0,     0,   133,     0,
   133,   133,   133,   133,     0,     0,   444,   437,     0,     0,
   451,   434,     0,   446,     0,   432,     0,   376,   374,     0,
     0,     0,    41,   416,   416,   421,     0,     0,   287,     0,
   285,   213,   212,     0,   133,     0,     0,   208,     0,     0,
   187,     0,     0,   188,   190,     0,   196,   126,   125,   133,
   123,   122,   182,     0,     0,     0,     0,     0,     0,   235,
   253,   236,   240,     0,     0,   251,   250,     0,     0,   249,
   228,     0,   231,     0,     0,     0,     0,   463,   465,     0,
     0,   133,     0,     0,     0,     0,   518,   517,     0,   410,
   412,   408,     0,     0,   133,   284,     0,     0,     0,     0,
   205,     0,   175,   198,   195,     0,   133,     0,     0,   185,
     0,     0,   179,     0,   224,   257,   259,   247,   261,     0,
     0,     0,     0,     0,   462,   133,    43,     0,     0,     0,
     0,     0,     0,     0,   133,   312,   418,   420,     0,     0,
     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
   133,     0,     0,     0,     0,     0,     0,     0,     0,   443,
   450,     0,   441,   448,   439,   520,     0,   288,   216,   215,
     0,   207,     0,     0,     0,   172,     0,   129,   128,     0,
     0,     0,   255,     0,     0,     0,     0,   466,     0,   133,
   519,     0,   186,   199,   197,   183,     0,   234,   229,   263,
   265,   267,   464,   416,     0,     0,     0,     0,     0,   452,
     0,     0,   180,     0,   521,   206,   467,   133,     0,     0,
   468,     0,     0
};

static const short yydefgoto[] = {     1,
     6,    36,    27,    65,   134,    69,   127,    70,   129,   167,
   308,   393,   838,   496,   499,    71,   130,   169,   397,   396,
   672,   170,   171,   309,    72,   131,   172,   239,    73,   128,
   136,   164,   232,    74,    75,   173,   240,   503,   428,   625,
   626,   403,   404,   600,   405,   439,   406,   598,   784,   407,
   781,   943,   646,    33,    53,    54,   126,    55,   138,   197,
   421,   482,   484,   218,   382,   409,   607,  1010,   686,   927,
   605,   788,  1111,   864,  1069,   786,  1000,  1001,   932,   688,
   779,   930,  1067,   410,   602,   411,   685,   926,   997,   683,
   684,   849,   995,   414,   611,   415,   703,   945,   614,   881,
  1080,   803,  1019,  1020,   706,   949,   795,   417,   609,   798,
   800,  1028,   418,   791,   959,   419,   796,   797,   799,   961,
   962,   963,   422,   621,   622,   424,   425,   530,   426,   531,
   325,   502,   920,   921,  1055,     7,    28,    37,    84,   147,
   208,   374,   667,   837,   209,   575,   210,   576,    76,   255,
   346,   256,   261,   458,   459,   348,   262,   254,   455,    86,
   152,   219,    87,    40,    77,   135,   247,   330,   552,   551,
   651,   650,   553,    78,   141,   272,   273,   565,   274,   566,
    89,   145,   283,   370,   174,   248,   249,   250,   430,   251,
   278,   279,   676,   368,   846,   544,   545,   333,   542,   737,
   733,   334,   730,   901,   897,   893,   335,   735,   899,   336,
   894,  1042,   337,   538,   726,   814,   964,  1086,   815,  1087,
  1164,  1178,     9,    29,    41,    42,    43,   220,    10,    11,
    12,    98,   227,    13,    14,    25,    15,    30,    46,    97,
   225,   121,   122,   123,   221,   124,   390,    79,   142,   201,
   470,   572,   752,   573,   751,   981,  1095,  1150
};

static const short yypact[] = {-32768,
    38,    19,-32768,    20,-32768,-32768,-32768,   106,-32768,-32768,
-32768,    32,-32768,   141,-32768,-32768,   180,-32768,-32768,   214,
   270,-32768,   259,-32768,-32768,    68,-32768,-32768,   145,    87,
-32768,   327,   637,  1137,  1137,   201,   641,  1137,   145,   320,
   385,   145,   145,-32768,-32768,-32768,-32768,   386,-32768,-32768,
-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,
-32768,-32768,-32768,-32768,   409,   415,   484,-32768,-32768,-32768,
-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,   896,
-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,
-32768,-32768,   476,-32768,-32768,-32768,   486,-32768,-32768,-32768,
-32768,-32768,-32768,-32768,-32768,-32768,   281,-32768,-32768,   506,
-32768,-32768,   522,-32768,  1137,-32768,   592,   625,   398,    80,
   664,-32768,-32768,-32768,   789,-32768,    73,-32768,   710,   524,
-32768,-32768,-32768,-32768,-32768,-32768,   433,-32768,   541,-32768,
-32768,   466,   616,   549,-32768,   559,   778,   548,   569,-32768,
-32768,   255,-32768,-32768,   611,    25,-32768,   612,   819,-32768,
-32768,-32768,-32768,   414,   614,-32768,-32768,   627,   651,   617,
   524,   659,-32768,   901,   446,-32768,-32768,-32768,-32768,   298,
-32768,-32768,-32768,-32768,-32768,-32768,   669,   662,    32,   689,
   689,   689,   689,   689,    32,   691,   255,-32768,   381,   143,
   634,-32768,-32768,   961,-32768,  1454,-32768,-32768,-32768,-32768,
   702,   714,   235,-32768,-32768,-32768,   709,-32768,   736,-32768,
-32768,   722,    32,-32768,-32768,    32,-32768,-32768,-32768,-32768,
-32768,   596,-32768,-32768,-32768,    23,-32768,-32768,   578,   217,
  1137,   171,   745,-32768,   750,    46,-32768,-32768,-32768,-32768,
-32768,-32768,   298,-32768,   785,   752,   735,   689,   689,   720,
   761,-32768,   768,   774,   689,   689,   689,  1478,   574,   780,
   787,-32768,-32768,-32768,-32768,-32768,   791,-32768,   718,   794,
-32768,    46,-32768,   113,   102,    32,-32768,-32768,   255,   255,
   255,   255,   308,   796,   772,   255,   255,   255,-32768,   873,
   903,-32768,-32768,   824,   829,-32768,-32768,-32768,-32768,   776,
-32768,-32768,   408,-32768,-32768,   743,-32768,   830,   859,   859,
   864,   305,   871,   856,-32768,-32768,-32768,-32768,-32768,   908,
   821,-32768,-32768,-32768,-32768,-32768,-32768,   186,-32768,-32768,
   895,   900,   917,   846,   909,-32768,   389,   920,   752,-32768,
   923,   939,-32768,-32768,-32768,-32768,-32768,   945,   954,   689,
-32768,   255,   167,   930,   942,-32768,   964,-32768,   968,   972,
   976,   708,    32,-32768,   982,   527,   574,   574,   574,   308,
   308,   987,-32768,   989,   574,   574,-32768,-32768,-32768,-32768,
-32768,-32768,   348,   350,   973,   524,   524,-32768,-32768,-32768,
-32768,   336,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,
-32768,-32768,   628,-32768,-32768,-32768,-32768,-32768,-32768,    59,
   709,-32768,-32768,-32768,-32768,-32768,    66,-32768,-32768,-32768,
   421,   298,    37,-32768,   995,  1004,  1005,  1007,  1009,   432,
-32768,    46,    46,  1012,  1022,    46,    46,    46,-32768,-32768,
-32768,-32768,-32768,-32768,-32768,-32768,-32768,   752,   752,-32768,
-32768,  1015,   574,-32768,-32768,-32768,   351,   860,-32768,  1150,
-32768,   421,-32768,-32768,-32768,  1023,  1088,   151,-32768,   229,
   574,  1026,   574,  1027,   295,-32768,-32768,-32768,-32768,   307,
   322,  1112,-32768,-32768,-32768,   252,-32768,-32768,    94,-32768,
-32768,   634,-32768,-32768,   298,-32768,  1029,-32768,  1033,  1042,
-32768,  1043,-32768,  1044,-32768,  1047,  1050,  1051,-32768,  1052,
  1056,  1066,  1068,-32768,-32768,-32768,-32768,   943,   481,   985,
  1000,-32768,-32768,   403,-32768,-32768,-32768,  1041,   298,  1082,
  1084,-32768,  1077,   807,-32768,   116,   163,   164,   166,   432,
-32768,-32768,    43,    49,-32768,-32768,    46,  1089,    46,  1162,
  1093,  1098,-32768,   355,-32768,-32768,-32768,   995,   341,-32768,
-32768,-32768,-32768,  1073,-32768,-32768,-32768,-32768,-32768,  1107,
-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,  1108,    39,
-32768,-32768,-32768,-32768,    33,   487,   298,-32768,   563,   812,
-32768,  1114,-32768,-32768,  1115,   267,   634,   728,-32768,-32768,
   814,-32768,  1097,  1127,  1087,  1104,   823,   196,   255,    79,
  1130,   481,   255,   255,-32768,-32768,-32768,-32768,  1131,-32768,
-32768,  1175,-32768,-32768,   298,-32768,  1135,  1138,-32768,  1145,
-32768,  1157,-32768,  1165,  1169,   670,-32768,-32768,-32768,-32768,
-32768,  1179,-32768,  1186,  1187,-32768,-32768,  1194,  1182,  1185,
   845,-32768,-32768,-32768,  1189,  1193,-32768,  1177,-32768,  1142,
-32768,   524,  1203,  1208,  1214,  1123,-32768,  1220,  1212,-32768,
-32768,    92,  1221,  1114,-32768,-32768,   952,  1225,-32768,   298,
-32768,   718,   298,-32768,  1237,-32768,  1244,-32768,  1216,   507,
-32768,-32768,-32768,-32768,   955,  1251,-32768,-32768,-32768,-32768,
-32768,-32768,  1255,  1258,-32768,  1259,  1262,  1274,   370,-32768,
-32768,   397,   441,   739,   816,   854,-32768,-32768,   298,-32768,
  1269,   210,-32768,  1273,-32768,  1276,-32768,  1277,-32768,-32768,
-32768,   499,   564,-32768,-32768,-32768,-32768,  1280,  1298,-32768,
   634,-32768,   875,  1294,    32,  1219,-32768,-32768,-32768,   974,
   718,  1013,   718,  1016,   838,  1318,-32768,-32768,   298,-32768,
  1325,-32768,-32768,   575,   605,   995,   282,  1328,-32768,   298,
-32768,  1332,   298,-32768,-32768,  1115,   434,   634,-32768,   623,
-32768,   995,   712,  1335,-32768,-32768,-32768,-32768,-32768,-32768,
-32768,  1314,  1127,  1345,  1348,  1349,-32768,-32768,-32768,-32768,
-32768,   109,  1347,-32768,-32768,  1354,  1223,-32768,-32768,  1362,
  1226,-32768,  1230,-32768,  1249,-32768,    46,    46,-32768,-32768,
   718,  1253,  1372,  1384,  1391,  1398,-32768,   973,   995,-32768,
   995,-32768,   995,  1403,  1406,-32768,  1407,   298,-32768,   488,
-32768,-32768,   885,   298,-32768,  1410,  1422,-32768,  1428,  1256,
  1417,  1439,  1260,-32768,  1225,   298,-32768,   718,-32768,  1263,
   899,  1443,  1444,  1447,-32768,  1267,  1286,  1290,  1293,  1297,
-32768,-32768,  1251,-32768,-32768,-32768,-32768,  1448,  1370,-32768,
  1451,  1452,-32768,-32768,  1440,  1464,-32768,  1465,-32768,  1467,
-32768,  1468,  1473,  1479,-32768,-32768,-32768,-32768,-32768,  1300,
   159,   912,   914,   926,  1035,-32768,  1055,-32768,  1457,  1485,
  1407,  1486,  1304,   298,-32768,  1489,  1490,-32768,   298,-32768,
   189,  1491,  1410,   530,  1492,  1493,-32768,  1494,  1495,   624,
  1496,   298,-32768,  1498,  1499,  1500,-32768,   697,  1501,  1443,
  1471,   232,   255,   255,    36,   255,  1502,   655,-32768,  1503,
-32768,-32768,-32768,-32768,  1481,  1505,-32768,-32768,  1323,  1327,
-32768,-32768,  1330,-32768,  1334,-32768,  1337,-32768,-32768,  1125,
  1506,  1507,-32768,   718,   718,   838,   995,   995,-32768,  1508,
-32768,-32768,-32768,   298,-32768,   261,  1509,-32768,   395,  1510,
-32768,  1511,  1341,-32768,-32768,   298,-32768,-32768,-32768,-32768,
-32768,-32768,-32768,  1512,  1513,  1360,  1487,   356,  1514,-32768,
-32768,-32768,-32768,   482,   500,-32768,-32768,  1515,   529,-32768,
-32768,  1364,-32768,  1367,  1371,  1374,  1378,-32768,-32768,  1516,
  1517,-32768,  1518,  1519,  1520,  1522,-32768,-32768,  1523,-32768,
-32768,-32768,   935,   937,-32768,-32768,  1524,  1397,   995,  1114,
-32768,  1115,-32768,-32768,-32768,   298,-32768,  1401,  1490,-32768,
  1525,  1526,-32768,  1127,-32768,-32768,-32768,-32768,-32768,  1499,
  1527,   255,   255,   255,-32768,-32768,-32768,  1528,  1529,  1404,
  1530,  1531,  1532,  1137,-32768,-32768,-32768,-32768,  1408,  1533,
  1534,  1536,  1537,  1225,  1538,  1411,  1539,  1540,  1541,  1542,
-32768,  1251,  1543,  1544,   545,   571,   586,  1415,   748,-32768,
-32768,  1545,-32768,-32768,-32768,-32768,  1434,-32768,-32768,-32768,
  1074,-32768,  1546,  1547,  1548,-32768,  1549,-32768,-32768,  1438,
  1550,  1551,-32768,  1552,  1553,  1554,  1555,-32768,  1556,-32768,
-32768,  1114,-32768,-32768,-32768,-32768,  1557,-32768,-32768,-32768,
-32768,-32768,-32768,   718,  1558,  1441,  1559,  1560,  1562,-32768,
  1563,  1564,-32768,  1504,-32768,-32768,-32768,-32768,  1445,  1565,
-32768,  1566,-32768
};

static const short yypgoto[] = {-32768,
-32768,-32768,-32768,   -34,-32768,  1535,-32768,-32768,-32768,-32768,
-32768,  -303,-32768,-32768,   732,-32768,-32768,  -374,-32768,-32768,
-32768,  1369,-32768,-32768,-32768,-32768,-32768,-32768,  1561,-32768,
-32768,  1435,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,
-32768,  1254,-32768,-32768,  -476,  -135,-32768,-32768,-32768,-32768,
-32768,-32768,   345,   -98,  -422,  1140,-32768,  -175,  -290,  -252,
  -149,  1094,  1095,-32768,   598,-32768,-32768,-32768,-32768,-32768,
-32768,-32768,-32768,-32768,-32768,-32768,   508,  -678,   643,  -769,
  -829,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,  -672,
   653,-32768,-32768,-32768,-32768,   969,-32768,-32768,-32768,-32768,
-32768,-32768,   501,  -691,  -784,   632,  -314,-32768,-32768,-32768,
-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,
-32768,-32768,-32768,   962,  1261,-32768,-32768,-32768,-32768,-32768,
-32768,-32768,   665,-32768,-32768,-32768,-32768,-32768,-32768,-32768,
-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,  1567,  1383,
   -59,-32768,  -159,-32768,-32768,  -313,   -89,-32768,-32768,-32768,
-32768,-32768,-32768,     8,-32768,-32768,-32768,-32768,  -232,-32768,
-32768,-32768,  1144,  1568,-32768,-32768,-32768,-32768,-32768,-32768,
-32768,-32768,-32768,-32768,   120,-32768,-32768,-32768,-32768,-32768,
  -582,  1086,-32768,  -679,   603,  -557,  -202,-32768,-32768,-32768,
-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,
-32768,-32768,-32768,  1118,   927,-32768,-32768,-32768,-32768,-32768,
-32768,-32768,-32768,-32768,   666,-32768,-32768,-32768,-32768,  1580,
-32768,-32768,-32768,  1579,-32768,-32768,-32768,-32768,-32768,-32768,
-32768,-32768,  1474,-32768,-32768,-32768,-32768,  1569,-32768,-32768,
-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768
};


#define	YYLAST		1606


static const short yytable[] = {   125,
    66,   196,   217,    91,   253,   394,   416,   196,     8,   778,
   661,   773,   782,   341,   794,   362,   865,   550,   883,     8,
   140,   500,   501,   140,   692,   310,   933,   159,   408,   408,
   601,   263,   264,   265,   266,   454,    20,  1182,    48,    48,
     2,   670,   332,   311,    16,   338,   647,   269,   338,   371,
   139,   338,   649,   144,    50,    51,     3,     4,    17,   671,
    52,   540,   467,     3,     4,   524,   525,   526,   527,     3,
     4,   528,   289,   290,   291,   292,   160,   342,   673,   674,
   146,   840,   293,   842,   532,   524,   525,   526,   527,    44,
    45,   528,   289,   290,   291,   292,   533,   593,   351,   352,
   161,   154,   293,   933,   769,   358,   359,   360,    34,    18,
    19,   162,   529,   770,   594,   372,    21,   223,     5,   363,
   675,   300,   301,   681,   196,     5,   534,   645,   635,    35,
   887,     5,   294,   -21,   -21,   312,   373,   636,   155,   376,
   377,   378,   379,    23,   561,   562,   385,   386,   387,  1026,
   771,   904,   294,   339,   340,   343,   339,   340,   295,   339,
   340,   275,   983,   224,   469,   140,   349,   888,   831,   276,
   464,   286,   579,   296,   297,   163,     3,     4,   295,   594,
   637,   638,    26,   298,   639,   641,   941,   643,   944,   327,
   140,   960,   889,   296,   297,   -21,   259,   328,   440,   596,
   462,   854,   267,    67,    68,   868,   326,   441,   196,   580,
   855,   196,   463,   557,   558,   559,   408,   712,   853,   316,
   317,   640,   642,   741,   644,    38,   480,   481,   483,   485,
   303,   819,   581,   304,   871,   490,   491,    39,     5,   442,
   443,   444,   445,   446,   447,   298,    31,   857,   289,   290,
   291,   292,   713,   712,   714,   715,   539,   213,   293,   349,
   199,   318,   319,   320,   204,   590,   514,   321,   820,   322,
   716,   717,    32,   769,   718,   477,   591,   523,   214,   690,
   592,   912,   770,   913,    16,   914,   215,   216,   691,  1112,
   714,   492,  1104,   375,   854,   448,   416,   759,   586,   132,
    48,   741,   741,   855,  1050,  1051,   133,   420,   294,   408,
   587,   298,   323,   564,  1059,  1060,    50,    51,   324,   771,
   648,   648,    52,   298,   652,   588,   654,   380,   214,   597,
    93,   481,   381,   483,   295,   856,   215,   216,   298,   504,
   857,   634,   176,   177,   178,   179,   742,   743,   505,   296,
   297,   493,    47,   497,   567,   362,   560,   506,   658,    56,
    57,    58,    59,   629,    60,    61,    62,    63,   349,   349,
    64,   298,   494,   807,   494,   494,   495,   515,   495,   495,
   476,   774,   775,   182,   183,   184,   298,  1103,    94,   507,
   508,   509,   510,   511,   512,   176,   177,   178,   179,   790,
   808,   180,   689,   182,   183,   184,   627,   854,    99,   872,
   181,   707,   100,   298,   518,  1074,   855,   229,   101,   513,
  1141,   678,   270,   153,   662,  1133,   398,   494,   399,  1053,
  1054,   495,   230,   231,   873,   571,   182,   183,   184,   176,
   177,   178,   179,   632,   809,   180,   866,   187,   856,   252,
    50,    51,  1062,   857,   181,   867,    52,   298,   634,   729,
   241,   242,   243,   514,   230,   231,   659,   660,   245,   719,
   761,   763,   765,   722,   723,   271,   665,   666,   119,  1167,
   182,   183,   184,   620,  1169,  1076,   185,   186,   120,   189,
   677,   187,   190,   191,   192,   193,   194,   195,   298,   679,
   924,  1102,   827,  1077,   214,   535,   536,   537,   137,   925,
   699,   494,   215,   216,   780,   495,   298,   783,   102,   103,
   104,   105,   106,   494,   143,   107,   168,   495,   515,   213,
   479,   188,  1079,   189,   663,   108,   190,   191,   192,   193,
   194,   195,  1006,   298,   198,   298,   200,   109,  1144,   110,
   214,  1007,   203,   816,   182,   183,   184,   950,   215,   216,
   940,   298,   205,   517,   523,   518,   519,   828,   756,   176,
   177,   178,   179,   111,  1145,   505,   213,   958,   851,   112,
   211,   520,   521,   689,   506,   522,   113,   298,   494,  1146,
   298,   313,   495,   848,   902,   903,   314,   214,   315,   494,
   707,   212,   298,   495,   859,   215,   216,   862,   852,   305,
   182,   183,   184,   222,   306,   307,   226,   508,   509,   510,
   511,   512,   176,   177,   178,   179,   869,  1013,   180,   494,
   236,   817,   233,   495,   821,   950,   823,   181,   825,    48,
    49,   916,   918,    80,    81,   234,   513,   494,   494,   515,
   634,   495,   495,   832,   235,    50,    51,   874,  1031,   148,
   149,    52,   238,   182,   183,   184,   120,   156,   634,   202,
   186,   257,   922,   740,   187,   182,   183,   184,   929,   494,
   258,   516,   860,   495,   517,   863,   518,   519,    50,    51,
   942,   260,   870,   268,    52,   150,   151,   876,   877,   878,
   879,   880,   520,   521,    92,   287,   522,    95,    96,   634,
   634,   634,   165,   166,   188,   277,   189,   288,   515,   190,
   191,   192,   193,   194,   195,   298,   176,   177,   178,   179,
   474,   475,   180,   515,   176,   177,   178,   179,   910,   299,
   693,   181,   810,   302,   182,   183,   184,   329,   994,   694,
   923,  1148,   331,  1002,   347,   518,   350,    50,    51,   182,
   183,   184,   835,    52,   355,   872,  1015,   182,   183,   184,
   518,   356,   494,   353,   354,   873,   495,   357,   187,  1027,
   206,   207,   364,  1119,   695,   696,   697,   344,   345,   365,
   873,    48,   158,   366,   969,   970,   344,   369,   973,   383,
   975,   384,   977,  1024,  1025,   367,  1029,    50,    51,   543,
   633,   698,   874,    52,   599,   680,   700,   701,  1057,   811,
   189,    48,   228,   190,   191,   192,   193,   194,   195,   391,
  1066,  1003,   401,   432,    50,    51,   395,    50,    51,   400,
    52,   433,   434,    52,  1016,   710,   711,   543,   750,   392,
   634,   634,   176,   177,   178,   179,   812,   813,   180,   689,
  1032,   402,  1034,  1035,  1036,  1037,   413,   181,   182,   183,
   184,   707,   432,   427,   435,    48,   388,   812,   833,   436,
   433,   434,   874,   844,   845,   437,   438,   543,   928,   429,
  1105,    50,    51,   182,   183,   184,  1058,    52,   449,   634,
   186,   543,   947,   450,   187,    48,   389,   182,   183,   184,
   431,  1068,   452,   568,   543,   984,   543,   985,   436,    48,
   451,    50,    51,   453,   437,   438,   456,    52,   543,   986,
   102,   103,  1115,  1116,  1117,    50,    51,   543,  1097,   543,
  1098,    52,   457,  1090,   188,  1047,   189,   114,   460,   190,
   191,   192,   193,   194,   195,   465,  1099,   461,   115,   109,
   116,   110,   117,   466,   432,   118,   468,   432,  1106,   724,
   725,   471,   433,   434,   472,   433,   434,   486,   487,   473,
   241,   242,   243,   244,   478,   111,   432,  1118,   245,   246,
   488,   112,   489,   498,   433,   434,  1127,   543,   113,   182,
   183,   184,   182,   183,   184,   776,   546,   547,   792,   548,
   436,   549,  1140,   436,   555,   619,   437,   438,   563,   437,
   438,   182,   183,   184,   556,   432,   577,   839,   432,   583,
   585,   599,   436,   433,   434,   603,   433,   434,   437,   438,
   241,   242,   243,   281,   604,   606,   608,   432,   245,   282,
   610,  1166,   612,   613,   615,   433,   434,   623,   616,  1126,
   182,   183,   184,   182,   183,   184,   841,   432,   617,   843,
   618,   436,   624,   628,   436,   433,   434,   437,   438,  1179,
   437,   438,   182,   183,   184,   630,   432,   631,   987,   432,
    48,   578,   653,   436,   433,   434,   656,   433,   434,   437,
   438,   657,   182,   183,   184,   664,    50,    51,   988,   668,
   708,   669,    52,   436,    48,   589,   682,   687,   704,   437,
   438,   182,   183,   184,   182,   183,   184,   709,  1152,   705,
    50,    51,   436,   720,   727,   436,    52,   731,   437,   438,
   732,   437,   438,    56,    57,    58,    59,   734,    60,    61,
    62,    63,   569,   570,    64,    56,    57,    58,    59,   736,
    60,    61,    62,    63,    48,   655,    64,   738,    56,    57,
    58,    59,   739,    60,    61,    62,    63,    48,   728,    64,
    50,    51,   744,  1046,    48,   748,    52,    48,   749,   745,
   746,    48,   754,    50,    51,    48,   755,   747,   757,    52,
    50,    51,   758,    50,    51,   760,    52,    50,    51,    52,
   762,    50,    51,    52,    48,   768,   764,    52,    48,   789,
   766,    48,   836,   767,   772,    48,   892,   777,    48,   896,
    50,    51,    48,   898,    50,    51,    52,    50,    51,   785,
    52,    50,    51,    52,    50,    51,   787,    52,    50,    51,
    52,    48,   900,   793,    52,    48,   905,   801,    48,   936,
   802,   804,    48,   939,   805,    48,   946,    50,    51,    48,
   953,    50,    51,    52,    50,    51,   806,    52,    50,    51,
    52,    50,    51,   829,    52,    50,    51,    52,    48,   954,
   818,    52,    48,   955,   822,    48,   956,   824,   826,    48,
   957,   830,    48,   982,    50,    51,    48,   993,    50,    51,
    52,    50,    51,   834,    52,    50,    51,    52,    50,    51,
   847,    52,    50,    51,    52,    48,  1040,   850,    52,    48,
  1041,   858,    48,  1043,   861,   882,    48,  1044,   875,    48,
  1045,    50,    51,    48,  1065,    50,    51,    52,    50,    51,
   890,    52,    50,    51,    52,    50,    51,   891,    52,    50,
    51,    52,    48,  1072,   895,    52,    48,  1081,   884,    48,
  1082,   885,   886,    48,  1083,   906,    48,  1084,    50,    51,
    48,  1085,    50,    51,    52,    50,    51,   907,    52,    50,
    51,    52,    50,    51,   908,    52,    50,    51,    52,    48,
  1101,   909,    52,    48,  1107,   915,    48,  1122,   917,   919,
    48,  1128,   931,    48,  1135,    50,    51,    48,  1147,    50,
    51,    52,    50,    51,   934,    52,    50,    51,    52,    50,
    51,   935,    52,    50,    51,    52,    48,  1151,   937,    52,
    48,  1157,   938,    48,  1171,   948,   951,    48,  1180,   952,
   965,   966,    50,    51,   967,   968,    50,    51,    52,    50,
    51,   971,    52,    50,    51,    52,   284,   972,   974,    52,
   976,   978,    56,    57,    58,   285,   979,    60,    61,    62,
    63,   980,   989,    64,   176,   177,   178,   179,   990,   992,
   361,   996,   999,   710,  1004,  1008,  1009,  1011,  1012,  1014,
  1017,  1018,  1038,  1021,  1022,  1030,  1033,  1039,  1073,  1048,
  1049,  1056,  1061,  1063,  1064,  1070,  1071,  1075,  1078,  1088,
  1089,  1091,  1092,  1093,  1094,  1177,  1096,  1100,  1109,  1110,
  1114,  1120,  1121,  1123,  1124,  1125,  1129,  1130,  1131,   237,
  1132,  1134,  1136,  1137,  1138,  1139,  1142,  1143,  1149,  1153,
  1154,  1155,  1156,  1158,  1159,  1160,  1161,  1162,  1163,  1165,
  1168,  1170,  1172,  1173,  1174,  1183,  1175,  1176,  1181,   911,
   175,    82,   541,   412,   582,  1005,  1108,   584,   998,   702,
  1113,  1023,   423,   721,   280,   991,   554,   595,  1052,   574,
   753,    22,    24,     0,   157,     0,     0,    83,     0,     0,
     0,     0,     0,    85,    88,    90
};

static const short yycheck[] = {    98,
    35,   137,   152,    38,   180,   309,   321,   143,     1,   688,
   568,   684,   692,   246,   706,   268,   786,   440,   803,    12,
   110,   396,   397,   113,   607,     3,   856,   126,   319,   320,
   507,   191,   192,   193,   194,   349,    17,     0,     3,     3,
     3,     3,   245,    21,    26,     3,     4,   197,     3,   282,
   110,     3,     4,   113,    19,    20,    32,    33,    40,    21,
    25,    25,   366,    32,    33,     7,     8,     9,    10,    32,
    33,    13,    14,    15,    16,    17,     4,   253,    46,    47,
   115,   761,    24,   763,    19,     7,     8,     9,    10,     3,
     4,    13,    14,    15,    16,    17,    31,     4,   258,   259,
    28,    22,    24,   933,    13,   265,   266,   267,    41,    91,
    92,    39,    54,    22,    21,     3,    11,    93,    94,   269,
    88,   220,   221,   600,   260,    94,   430,   550,    13,    62,
    22,    94,    74,    32,    33,   113,    24,    22,    59,   289,
   290,   291,   292,     3,   458,   459,   296,   297,   298,   114,
    59,   831,    74,   111,   112,   254,   111,   112,   100,   111,
   112,    19,     4,   156,   367,   255,   256,    59,   751,    27,
     4,   206,    22,   115,   116,   103,    32,    33,   100,    21,
    65,    66,     3,    17,    22,    22,   865,    22,   868,    19,
   280,   883,    84,   115,   116,    94,   189,    27,    13,   503,
   360,    13,   195,     3,     4,   788,   241,    22,   344,    59,
    22,   347,   362,   446,   447,   448,   507,    22,   776,     3,
     4,    59,    59,   646,    59,    81,   376,   377,   378,   379,
   223,    22,     4,   226,   792,   385,   386,    93,    94,    54,
    55,    56,    57,    58,    59,    17,    33,    59,    14,    15,
    16,    17,    57,    22,    59,    60,   432,     3,    24,   349,
   141,    45,    46,    47,   145,    14,   402,    51,    59,    53,
    75,    76,     3,    13,    79,   374,    25,   413,    24,    13,
    29,   839,    22,   841,    26,   843,    32,    33,    22,  1074,
    59,   390,  1062,   286,    13,   110,   611,   672,     4,    19,
     3,   724,   725,    22,   984,   985,    26,     3,    74,   600,
     4,    17,    96,   463,    54,    55,    19,    20,   102,    59,
   553,   554,    25,    17,   557,     4,   559,    20,    24,   505,
    11,   481,    25,   483,   100,    54,    32,    33,    17,     4,
    59,   544,     7,     8,     9,    10,   650,   651,    13,   115,
   116,     4,    26,     4,     4,   608,   455,    22,     4,    19,
    20,    21,    22,   539,    24,    25,    26,    27,   458,   459,
    30,    17,    25,     4,    25,    25,    29,    22,    29,    29,
   373,   685,   686,    48,    49,    50,    17,  1060,     4,    54,
    55,    56,    57,    58,    59,     7,     8,     9,    10,   703,
     4,    13,   605,    48,    49,    50,     4,    13,    23,    54,
    22,   614,     4,    17,    59,    60,    22,     4,     4,    84,
  1112,   597,    42,    26,    84,  1104,    19,    25,    21,   987,
   988,    29,    19,    20,    79,   470,    48,    49,    50,     7,
     8,     9,    10,   542,     4,    13,    13,    59,    54,     4,
    19,    20,    58,    59,    22,    22,    25,    17,   661,   635,
    80,    81,    82,   599,    19,    20,   565,   566,    88,   619,
   673,   674,   675,   623,   624,    95,   575,   576,     3,  1152,
    48,    49,    50,     3,  1164,     4,    54,    55,     3,   101,
     4,    59,   104,   105,   106,   107,   108,   109,    17,   598,
    13,  1059,     4,     4,    24,    85,    86,    87,     3,    22,
   609,    25,    32,    33,   690,    29,    17,   693,    35,    36,
    37,    38,    39,    25,     3,    42,     3,    29,    22,     3,
     4,    99,     4,   101,   569,    52,   104,   105,   106,   107,
   108,   109,    13,    17,     4,    17,    81,    64,     4,    66,
    24,    22,     4,   729,    48,    49,    50,   872,    32,    33,
   864,    17,     4,    57,   700,    59,    60,     4,   667,     7,
     8,     9,    10,    90,     4,    13,     3,   881,     4,    96,
    33,    75,    76,   786,    22,    79,   103,    17,    25,     4,
    17,    14,    29,   769,   827,   828,    19,    24,    21,    25,
   803,    33,    17,    29,   780,    32,    33,   783,     4,    14,
    48,    49,    50,     3,    19,    20,     5,    55,    56,    57,
    58,    59,     7,     8,     9,    10,     4,     4,    13,    25,
    14,   730,    19,    29,   733,   950,   735,    22,   737,     3,
     4,   844,   845,     3,     4,    19,    84,    25,    25,    22,
   853,    29,    29,   752,     4,    19,    20,   793,     4,    68,
    69,    25,     4,    48,    49,    50,     3,     4,   871,    54,
    55,     3,   848,     4,    59,    48,    49,    50,   854,    25,
    19,    54,   781,    29,    57,   784,    59,    60,    19,    20,
   866,     3,   791,     3,    25,    71,    72,   796,   797,   798,
   799,   800,    75,    76,    39,     4,    79,    42,    43,   912,
   913,   914,     3,     4,    99,    82,   101,     4,    22,   104,
   105,   106,   107,   108,   109,    17,     7,     8,     9,    10,
    23,    24,    13,    22,     7,     8,     9,    10,   837,     4,
    13,    22,     4,    22,    48,    49,    50,     3,   924,    22,
   849,     4,     3,   929,     3,    59,    22,    19,    20,    48,
    49,    50,   755,    25,     4,    54,   942,    48,    49,    50,
    59,     4,    25,    54,    55,    79,    29,     4,    59,   955,
     3,     4,     3,  1087,    57,    58,    59,     3,     4,     3,
    79,     3,     4,     3,   893,   894,     3,     4,   897,     4,
   899,    30,   901,   953,   954,    88,   956,    19,    20,     3,
     4,    84,   948,    25,     3,     4,     3,     4,   994,     4,
   101,     3,     4,   104,   105,   106,   107,   108,   109,     6,
  1006,   930,     3,    13,    19,    20,    61,    19,    20,    97,
    25,    21,    22,    25,   943,    23,    24,     3,     4,    21,
  1053,  1054,     7,     8,     9,    10,     3,     4,    13,  1062,
   959,     3,   961,   962,   963,   964,     3,    22,    48,    49,
    50,  1074,    13,     3,    54,     3,     4,     3,     4,    59,
    21,    22,  1018,    46,    47,    65,    66,     3,     4,    34,
  1066,    19,    20,    48,    49,    50,   995,    25,     4,  1102,
    55,     3,     4,     4,    59,     3,     4,    48,    49,    50,
     3,  1010,     4,    54,     3,     4,     3,     4,    59,     3,
     4,    19,    20,     4,    65,    66,     4,    25,     3,     4,
    35,    36,  1082,  1083,  1084,    19,    20,     3,     4,     3,
     4,    25,     4,  1042,    99,   980,   101,    52,     4,   104,
   105,   106,   107,   108,   109,    26,  1055,     4,    63,    64,
    65,    66,    67,    22,    13,    70,     3,    13,  1067,   625,
   626,     4,    21,    22,     3,    21,    22,   380,   381,     4,
    80,    81,    82,    83,     3,    90,    13,  1086,    88,    89,
     4,    96,     4,    21,    21,    22,  1095,     3,   103,    48,
    49,    50,    48,    49,    50,    54,     3,     3,    54,     3,
    59,     3,  1111,    59,     3,    73,    65,    66,     4,    65,
    66,    48,    49,    50,     3,    13,     4,    54,    13,     4,
     4,     3,    59,    21,    22,     3,    21,    22,    65,    66,
    80,    81,    82,    83,     3,     3,     3,    13,    88,    89,
     4,  1150,     3,     3,     3,    21,    22,    73,     3,  1094,
    48,    49,    50,    48,    49,    50,    54,    13,     3,    54,
     3,    59,    73,    33,    59,    21,    22,    65,    66,  1178,
    65,    66,    48,    49,    50,     4,    13,     4,    54,    13,
     3,     4,     4,    59,    21,    22,     4,    21,    22,    65,
    66,     4,    48,    49,    50,    33,    19,    20,    54,     3,
    24,     4,    25,    59,     3,     4,     3,     3,    22,    65,
    66,    48,    49,    50,    48,    49,    50,    24,    55,     3,
    19,    20,    59,     4,     4,    59,    25,     3,    65,    66,
     3,    65,    66,    19,    20,    21,    22,     3,    24,    25,
    26,    27,     3,     4,    30,    19,    20,    21,    22,     3,
    24,    25,    26,    27,     3,     4,    30,     3,    19,    20,
    21,    22,     4,    24,    25,    26,    27,     3,     4,    30,
    19,    20,     4,    59,     3,     4,    25,     3,     4,     4,
     4,     3,     4,    19,    20,     3,     4,     4,    22,    25,
    19,    20,    61,    19,    20,     3,    25,    19,    20,    25,
     3,    19,    20,    25,     3,     4,     3,    25,     3,     4,
    98,     3,     4,     4,     4,     3,     4,     3,     3,     4,
    19,    20,     3,     4,    19,    20,    25,    19,    20,     3,
    25,    19,    20,    25,    19,    20,     3,    25,    19,    20,
    25,     3,     4,     3,    25,     3,     4,     3,     3,     4,
     3,     3,     3,     4,     3,     3,     4,    19,    20,     3,
     4,    19,    20,    25,    19,    20,     3,    25,    19,    20,
    25,    19,    20,     4,    25,    19,    20,    25,     3,     4,
    22,    25,     3,     4,    22,     3,     4,    22,    22,     3,
     4,     4,     3,     4,    19,    20,     3,     4,    19,    20,
    25,    19,    20,    20,    25,    19,    20,    25,    19,    20,
     3,    25,    19,    20,    25,     3,     4,     3,    25,     3,
     4,     4,     3,     4,     3,    22,     3,     4,     4,     3,
     4,    19,    20,     3,     4,    19,    20,    25,    19,    20,
     4,    25,    19,    20,    25,    19,    20,     4,    25,    19,
    20,    25,     3,     4,     3,    25,     3,     4,    24,     3,
     4,    24,    24,     3,     4,     4,     3,     4,    19,    20,
     3,     4,    19,    20,    25,    19,    20,     4,    25,    19,
    20,    25,    19,    20,     4,    25,    19,    20,    25,     3,
     4,     4,    25,     3,     4,     3,     3,     4,     3,     3,
     3,     4,     3,     3,     4,    19,    20,     3,     4,    19,
    20,    25,    19,    20,     3,    25,    19,    20,    25,    19,
    20,     4,    25,    19,    20,    25,     3,     4,    22,    25,
     3,     4,     4,     3,     4,     3,     3,     3,     4,     3,
     3,    82,    19,    20,     4,     4,    19,    20,    25,    19,
    20,    22,    25,    19,    20,    25,    13,     4,     4,    25,
     4,     4,    19,    20,    21,    22,     4,    24,    25,    26,
    27,     3,    26,    30,     7,     8,     9,    10,     4,     4,
    13,     3,     3,    23,     4,     4,     4,     4,     4,     4,
     3,     3,    22,     4,     4,     4,     4,     3,    22,     4,
     4,     4,     4,     4,     4,     4,     4,     4,     4,     4,
     4,     4,     4,     4,     3,    22,     4,     4,     4,     4,
     4,     4,     4,     4,     4,     4,     4,     4,     3,   171,
     4,     4,     4,     4,     4,     4,     4,     4,     4,     4,
     4,     4,     4,     4,     4,     4,     4,     4,     4,     4,
     4,     4,     4,     4,     3,     0,     4,     4,     4,   838,
   136,    37,   433,   320,   481,   933,  1069,   483,   926,   611,
  1080,   950,   322,   622,   202,   921,   443,   502,   986,   472,
   664,    12,    14,    -1,   121,    -1,    -1,    37,    -1,    -1,
    -1,    -1,    -1,    37,    37,    37
};

#line 325 "/usr/local/lib/bison.cc"
 /* fattrs + tables */

/* parser code folow  */


/* This is the parser code that is written into each bison parser
  when the %semantic_parser declaration is not specified in the grammar.
  It was written by Richard Stallman by simplifying the hairy parser
  used when %semantic_parser is specified.  */

/* Note: dollar marks section change
   the next  is replaced by the list of actions, each action
   as one case of the switch.  */ 

#if YY_PDDL_Parser_USE_GOTO != 0
/* 
 SUPRESSION OF GOTO : on some C++ compiler (sun c++)
  the goto is strictly forbidden if any constructor/destructor
  is used in the whole function (very stupid isn't it ?)
 so goto are to be replaced with a 'while/switch/case construct'
 here are the macro to keep some apparent compatibility
*/
#define YYGOTO(lb) {yy_gotostate=lb;continue;}
#define YYBEGINGOTO  enum yy_labels yy_gotostate=yygotostart; \
                     for(;;) switch(yy_gotostate) { case yygotostart: {
#define YYLABEL(lb) } case lb: {
#define YYENDGOTO } } 
#define YYBEGINDECLARELABEL enum yy_labels {yygotostart
#define YYDECLARELABEL(lb) ,lb
#define YYENDDECLARELABEL  };
#else
/* macro to keep goto */
#define YYGOTO(lb) goto lb
#define YYBEGINGOTO 
#define YYLABEL(lb) lb:
#define YYENDGOTO
#define YYBEGINDECLARELABEL 
#define YYDECLARELABEL(lb)
#define YYENDDECLARELABEL 
#endif
/* LABEL DECLARATION */
YYBEGINDECLARELABEL
  YYDECLARELABEL(yynewstate)
  YYDECLARELABEL(yybackup)
/* YYDECLARELABEL(yyresume) */
  YYDECLARELABEL(yydefault)
  YYDECLARELABEL(yyreduce)
  YYDECLARELABEL(yyerrlab)   /* here on detecting error */
  YYDECLARELABEL(yyerrlab1)   /* here on error raised explicitly by an action */
  YYDECLARELABEL(yyerrdefault)  /* current state does not do anything special for the error token. */
  YYDECLARELABEL(yyerrpop)   /* pop the current state because it cannot handle the error token */
  YYDECLARELABEL(yyerrhandle)  
YYENDDECLARELABEL
/* ALLOCA SIMULATION */
/* __HAVE_NO_ALLOCA */
#ifdef __HAVE_NO_ALLOCA
int __alloca_free_ptr(char *ptr,char *ref)
{if(ptr!=ref) free(ptr);
 return 0;}

#define __ALLOCA_alloca(size) malloc(size)
#define __ALLOCA_free(ptr,ref) __alloca_free_ptr((char *)ptr,(char *)ref)

#ifdef YY_PDDL_Parser_LSP_NEEDED
#define __ALLOCA_return(num) \
            return( __ALLOCA_free(yyss,yyssa)+\
		    __ALLOCA_free(yyvs,yyvsa)+\
		    __ALLOCA_free(yyls,yylsa)+\
		   (num))
#else
#define __ALLOCA_return(num) \
            return( __ALLOCA_free(yyss,yyssa)+\
		    __ALLOCA_free(yyvs,yyvsa)+\
		   (num))
#endif
#else
#define __ALLOCA_return(num) return(num)
#define __ALLOCA_alloca(size) alloca(size)
#define __ALLOCA_free(ptr,ref) 
#endif

/* ENDALLOCA SIMULATION */

#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (YY_PDDL_Parser_CHAR = YYEMPTY)
#define YYEMPTY         -2
#define YYEOF           0
#define YYACCEPT        __ALLOCA_return(0)
#define YYABORT         __ALLOCA_return(1)
#define YYERROR         YYGOTO(yyerrlab1)
/* Like YYERROR except do call yyerror.
   This remains here temporarily to ease the
   transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */
#define YYFAIL          YYGOTO(yyerrlab)
#define YYRECOVERING()  (!!yyerrstatus)
#define YYBACKUP(token, value) \
do                                                              \
  if (YY_PDDL_Parser_CHAR == YYEMPTY && yylen == 1)                               \
    { YY_PDDL_Parser_CHAR = (token), YY_PDDL_Parser_LVAL = (value);                 \
      yychar1 = YYTRANSLATE (YY_PDDL_Parser_CHAR);                                \
      YYPOPSTACK;                                               \
      YYGOTO(yybackup);                                            \
    }                                                           \
  else                                                          \
    { YY_PDDL_Parser_ERROR ("syntax error: cannot back up"); YYERROR; }   \
while (0)

#define YYTERROR        1
#define YYERRCODE       256

#ifndef YY_PDDL_Parser_PURE
/* UNPURE */
#define YYLEX           YY_PDDL_Parser_LEX()
#ifndef YY_USE_CLASS
/* If nonreentrant, and not class , generate the variables here */
int     YY_PDDL_Parser_CHAR;                      /*  the lookahead symbol        */
YY_PDDL_Parser_STYPE      YY_PDDL_Parser_LVAL;              /*  the semantic value of the */
				/*  lookahead symbol    */
int YY_PDDL_Parser_NERRS;                 /*  number of parse errors so far */
#ifdef YY_PDDL_Parser_LSP_NEEDED
YY_PDDL_Parser_LTYPE YY_PDDL_Parser_LLOC;   /*  location data for the lookahead     */
			/*  symbol                              */
#endif
#endif


#else
/* PURE */
#ifdef YY_PDDL_Parser_LSP_NEEDED
#define YYLEX           YY_PDDL_Parser_LEX(&YY_PDDL_Parser_LVAL, &YY_PDDL_Parser_LLOC)
#else
#define YYLEX           YY_PDDL_Parser_LEX(&YY_PDDL_Parser_LVAL)
#endif
#endif
#ifndef YY_USE_CLASS
#if YY_PDDL_Parser_DEBUG != 0
int YY_PDDL_Parser_DEBUG_FLAG;                    /*  nonzero means print parse trace     */
/* Since this is uninitialized, it does not stop multiple parsers
   from coexisting.  */
#endif
#endif



/*  YYINITDEPTH indicates the initial size of the parser's stacks       */

#ifndef YYINITDEPTH
#define YYINITDEPTH 200
#endif

/*  YYMAXDEPTH is the maximum size the stacks can grow to
    (effective only if the built-in stack extension method is used).  */

#if YYMAXDEPTH == 0
#undef YYMAXDEPTH
#endif

#ifndef YYMAXDEPTH
#define YYMAXDEPTH 10000
#endif


#if __GNUC__ > 1                /* GNU C and GNU C++ define this.  */
#define __yy_bcopy(FROM,TO,COUNT)       __builtin_memcpy(TO,FROM,COUNT)
#else                           /* not GNU C or C++ */

/* This is the most reliable way to avoid incompatibilities
   in available built-in functions on various systems.  */

#ifdef __cplusplus
static void __yy_bcopy (char *from, char *to, int count)
#else
#ifdef __STDC__
static void __yy_bcopy (char *from, char *to, int count)
#else
static void __yy_bcopy (from, to, count)
     char *from;
     char *to;
     int count;
#endif
#endif
{
  register char *f = from;
  register char *t = to;
  register int i = count;

  while (i-- > 0)
    *t++ = *f++;
}
#endif

int
#ifdef YY_USE_CLASS
 YY_PDDL_Parser_CLASS::
#endif
     YY_PDDL_Parser_PARSE(YY_PDDL_Parser_PARSE_PARAM)
#ifndef __STDC__
#ifndef __cplusplus
#ifndef YY_USE_CLASS
/* parameter definition without protypes */
YY_PDDL_Parser_PARSE_PARAM_DEF
#endif
#endif
#endif
{
  register int yystate;
  register int yyn;
  register short *yyssp;
  register YY_PDDL_Parser_STYPE *yyvsp;
  int yyerrstatus;      /*  number of tokens to shift before error messages enabled */
  int yychar1=0;          /*  lookahead token as an internal (translated) token number */

  short yyssa[YYINITDEPTH];     /*  the state stack                     */
  YY_PDDL_Parser_STYPE yyvsa[YYINITDEPTH];        /*  the semantic value stack            */

  short *yyss = yyssa;          /*  refer to the stacks thru separate pointers */
  YY_PDDL_Parser_STYPE *yyvs = yyvsa;     /*  to allow yyoverflow to reallocate them elsewhere */

#ifdef YY_PDDL_Parser_LSP_NEEDED
  YY_PDDL_Parser_LTYPE yylsa[YYINITDEPTH];        /*  the location stack                  */
  YY_PDDL_Parser_LTYPE *yyls = yylsa;
  YY_PDDL_Parser_LTYPE *yylsp;

#define YYPOPSTACK   (yyvsp--, yyssp--, yylsp--)
#else
#define YYPOPSTACK   (yyvsp--, yyssp--)
#endif

  int yystacksize = YYINITDEPTH;

#ifdef YY_PDDL_Parser_PURE
  int YY_PDDL_Parser_CHAR;
  YY_PDDL_Parser_STYPE YY_PDDL_Parser_LVAL;
  int YY_PDDL_Parser_NERRS;
#ifdef YY_PDDL_Parser_LSP_NEEDED
  YY_PDDL_Parser_LTYPE YY_PDDL_Parser_LLOC;
#endif
#endif

  YY_PDDL_Parser_STYPE yyval;             /*  the variable used to return         */
				/*  semantic values from the action     */
				/*  routines                            */

  int yylen;
/* start loop, in which YYGOTO may be used. */
YYBEGINGOTO

#if YY_PDDL_Parser_DEBUG != 0
  if (YY_PDDL_Parser_DEBUG_FLAG)
    fprintf(stderr, "Starting parse\n");
#endif
  yystate = 0;
  yyerrstatus = 0;
  YY_PDDL_Parser_NERRS = 0;
  YY_PDDL_Parser_CHAR = YYEMPTY;          /* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */

  yyssp = yyss - 1;
  yyvsp = yyvs;
#ifdef YY_PDDL_Parser_LSP_NEEDED
  yylsp = yyls;
#endif

/* Push a new state, which is found in  yystate  .  */
/* In all cases, when you get here, the value and location stacks
   have just been pushed. so pushing a state here evens the stacks.  */
YYLABEL(yynewstate)

  *++yyssp = yystate;

  if (yyssp >= yyss + yystacksize - 1)
    {
      /* Give user a chance to reallocate the stack */
      /* Use copies of these so that the &'s don't force the real ones into memory. */
      YY_PDDL_Parser_STYPE *yyvs1 = yyvs;
      short *yyss1 = yyss;
#ifdef YY_PDDL_Parser_LSP_NEEDED
      YY_PDDL_Parser_LTYPE *yyls1 = yyls;
#endif

      /* Get the current used size of the three stacks, in elements.  */
      int size = yyssp - yyss + 1;

#ifdef yyoverflow
      /* Each stack pointer address is followed by the size of
	 the data in use in that stack, in bytes.  */
#ifdef YY_PDDL_Parser_LSP_NEEDED
      /* This used to be a conditional around just the two extra args,
	 but that might be undefined if yyoverflow is a macro.  */
      yyoverflow("parser stack overflow",
		 &yyss1, size * sizeof (*yyssp),
		 &yyvs1, size * sizeof (*yyvsp),
		 &yyls1, size * sizeof (*yylsp),
		 &yystacksize);
#else
      yyoverflow("parser stack overflow",
		 &yyss1, size * sizeof (*yyssp),
		 &yyvs1, size * sizeof (*yyvsp),
		 &yystacksize);
#endif

      yyss = yyss1; yyvs = yyvs1;
#ifdef YY_PDDL_Parser_LSP_NEEDED
      yyls = yyls1;
#endif
#else /* no yyoverflow */
      /* Extend the stack our own way.  */
      if (yystacksize >= YYMAXDEPTH)
	{
	  YY_PDDL_Parser_ERROR("parser stack overflow");
	  __ALLOCA_return(2);
	}
      yystacksize *= 2;
      if (yystacksize > YYMAXDEPTH)
	yystacksize = YYMAXDEPTH;
      yyss = (short *) __ALLOCA_alloca (yystacksize * sizeof (*yyssp));
      __yy_bcopy ((char *)yyss1, (char *)yyss, size * sizeof (*yyssp));
      __ALLOCA_free(yyss1,yyssa);
      yyvs = (YY_PDDL_Parser_STYPE *) __ALLOCA_alloca (yystacksize * sizeof (*yyvsp));
      __yy_bcopy ((char *)yyvs1, (char *)yyvs, size * sizeof (*yyvsp));
      __ALLOCA_free(yyvs1,yyvsa);
#ifdef YY_PDDL_Parser_LSP_NEEDED
      yyls = (YY_PDDL_Parser_LTYPE *) __ALLOCA_alloca (yystacksize * sizeof (*yylsp));
      __yy_bcopy ((char *)yyls1, (char *)yyls, size * sizeof (*yylsp));
      __ALLOCA_free(yyls1,yylsa);
#endif
#endif /* no yyoverflow */

      yyssp = yyss + size - 1;
      yyvsp = yyvs + size - 1;
#ifdef YY_PDDL_Parser_LSP_NEEDED
      yylsp = yyls + size - 1;
#endif

#if YY_PDDL_Parser_DEBUG != 0
      if (YY_PDDL_Parser_DEBUG_FLAG)
	fprintf(stderr, "Stack size increased to %d\n", yystacksize);
#endif

      if (yyssp >= yyss + yystacksize - 1)
	YYABORT;
    }

#if YY_PDDL_Parser_DEBUG != 0
  if (YY_PDDL_Parser_DEBUG_FLAG)
    fprintf(stderr, "Entering state %d\n", yystate);
#endif

  YYGOTO(yybackup);
YYLABEL(yybackup)

/* Do appropriate processing given the current state.  */
/* Read a lookahead token if we need one and don't already have one.  */
/* YYLABEL(yyresume) */

  /* First try to decide what to do without reference to lookahead token.  */

  yyn = yypact[yystate];
  if (yyn == YYFLAG)
    YYGOTO(yydefault);

  /* Not known => get a lookahead token if don't already have one.  */

  /* yychar is either YYEMPTY or YYEOF
     or a valid token in external form.  */

  if (YY_PDDL_Parser_CHAR == YYEMPTY)
    {
#if YY_PDDL_Parser_DEBUG != 0
      if (YY_PDDL_Parser_DEBUG_FLAG)
	fprintf(stderr, "Reading a token: ");
#endif
      YY_PDDL_Parser_CHAR = YYLEX;
    }

  /* Convert token to internal form (in yychar1) for indexing tables with */

  if (YY_PDDL_Parser_CHAR <= 0)           /* This means end of input. */
    {
      yychar1 = 0;
      YY_PDDL_Parser_CHAR = YYEOF;                /* Don't call YYLEX any more */

#if YY_PDDL_Parser_DEBUG != 0
      if (YY_PDDL_Parser_DEBUG_FLAG)
	fprintf(stderr, "Now at end of input.\n");
#endif
    }
  else
    {
      yychar1 = YYTRANSLATE(YY_PDDL_Parser_CHAR);

#if YY_PDDL_Parser_DEBUG != 0
      if (YY_PDDL_Parser_DEBUG_FLAG)
	{
	  fprintf (stderr, "Next token is %d (%s", YY_PDDL_Parser_CHAR, yytname[yychar1]);
	  /* Give the individual parser a way to print the precise meaning
	     of a token, for further debugging info.  */
#ifdef YYPRINT
	  YYPRINT (stderr, YY_PDDL_Parser_CHAR, YY_PDDL_Parser_LVAL);
#endif
	  fprintf (stderr, ")\n");
	}
#endif
    }

  yyn += yychar1;
  if (yyn < 0 || yyn > YYLAST || yycheck[yyn] != yychar1)
    YYGOTO(yydefault);

  yyn = yytable[yyn];

  /* yyn is what to do for this token type in this state.
     Negative => reduce, -yyn is rule number.
     Positive => shift, yyn is new state.
       New state is final state => don't bother to shift,
       just return success.
     0, or most negative number => error.  */

  if (yyn < 0)
    {
      if (yyn == YYFLAG)
	YYGOTO(yyerrlab);
      yyn = -yyn;
      YYGOTO(yyreduce);
    }
  else if (yyn == 0)
    YYGOTO(yyerrlab);

  if (yyn == YYFINAL)
    YYACCEPT;

  /* Shift the lookahead token.  */

#if YY_PDDL_Parser_DEBUG != 0
  if (YY_PDDL_Parser_DEBUG_FLAG)
    fprintf(stderr, "Shifting token %d (%s), ", YY_PDDL_Parser_CHAR, yytname[yychar1]);
#endif

  /* Discard the token being shifted unless it is eof.  */
  if (YY_PDDL_Parser_CHAR != YYEOF)
    YY_PDDL_Parser_CHAR = YYEMPTY;

  *++yyvsp = YY_PDDL_Parser_LVAL;
#ifdef YY_PDDL_Parser_LSP_NEEDED
  *++yylsp = YY_PDDL_Parser_LLOC;
#endif

  /* count tokens shifted since error; after three, turn off error status.  */
  if (yyerrstatus) yyerrstatus--;

  yystate = yyn;
  YYGOTO(yynewstate);

/* Do the default action for the current state.  */
YYLABEL(yydefault)

  yyn = yydefact[yystate];
  if (yyn == 0)
    YYGOTO(yyerrlab);

/* Do a reduction.  yyn is the number of a rule to reduce with.  */
YYLABEL(yyreduce)
  yylen = yyr2[yyn];
  if (yylen > 0)
    yyval = yyvsp[1-yylen]; /* implement default value of the action */

#if YY_PDDL_Parser_DEBUG != 0
  if (YY_PDDL_Parser_DEBUG_FLAG)
    {
      int i;

      fprintf (stderr, "Reducing via rule %d (line %d), ",
	       yyn, yyrline[yyn]);

      /* Print the symbols being reduced, and their result.  */
      for (i = yyprhs[yyn]; yyrhs[i] > 0; i++)
	fprintf (stderr, "%s ", yytname[yyrhs[i]]);
      fprintf (stderr, " -> %s\n", yytname[yyr1[yyn]]);
    }
#endif


/* #line 811 "/usr/local/lib/bison.cc" */
#line 2567 "grammar.cc"

  switch (yyn) {

case 17:
#line 132 "pddl2.y"
{
  domain_name = yyvsp[-1].sym->text;
  if (current_file()) domain_file = strdup(current_file());
;
    break;}
case 18:
#line 139 "pddl2.y"
{ yyval.sym = yyvsp[0].sym; ;
    break;}
case 19:
#line 140 "pddl2.y"
{ yyval.sym = yyvsp[0].sym; ;
    break;}
case 20:
#line 141 "pddl2.y"
{ yyval.sym = yyvsp[0].sym; ;
    break;}
case 21:
#line 142 "pddl2.y"
{ yyval.sym = yyvsp[0].sym; ;
    break;}
case 22:
#line 143 "pddl2.y"
{ yyval.sym = yyvsp[0].sym; ;
    break;}
case 23:
#line 144 "pddl2.y"
{ yyval.sym = yyvsp[0].sym; ;
    break;}
case 24:
#line 145 "pddl2.y"
{ yyval.sym = yyvsp[0].sym; ;
    break;}
case 25:
#line 146 "pddl2.y"
{ yyval.sym = yyvsp[0].sym; ;
    break;}
case 26:
#line 147 "pddl2.y"
{ yyval.sym = yyvsp[0].sym; ;
    break;}
case 27:
#line 151 "pddl2.y"
{ yyval.sym = yyvsp[0].sym; ;
    break;}
case 28:
#line 153 "pddl2.y"
{
  if (warning_level > 0) {
    std::cerr << "warning: redeclaration of action " << yyvsp[0].sym->text << std::endl;
    if (!best_effort) exit(1);
    yyval.sym = tab.gensym(yyvsp[0].sym->text);
  }
  else {
    yyval.sym = yyvsp[0].sym;
  }
;
    break;}
case 37:
#line 191 "pddl2.y"
{
  current_param.clear()
;
    break;}
case 38:
#line 195 "pddl2.y"
{
  PredicateSymbol* p = new PredicateSymbol(yyvsp[-3].sym->text);
  p->param = current_param;
  dom_predicates.append(p);
  clear_context(current_param);
  yyvsp[-3].sym->val = p;
;
    break;}
case 39:
#line 206 "pddl2.y"
{
  set_variable_type(current_param, (TypeSymbol*)yyvsp[0].sym->val);
;
    break;}
case 40:
#line 210 "pddl2.y"
{
  current_type_set.clear();
;
    break;}
case 41:
#line 214 "pddl2.y"
{
  set_variable_type(current_param, current_type_set);
;
    break;}
case 42:
#line 218 "pddl2.y"
{
  set_variable_type(current_param, dom_top_type);
;
    break;}
case 44:
#line 226 "pddl2.y"
{
  yyvsp[0].sym->val = new VariableSymbol(yyvsp[0].sym->text);
  current_param.append((VariableSymbol*)yyvsp[0].sym->val);
  if (trace_print_context) {
    std::cerr << "variable ";
    current_param[current_param.length() - 1]->print(std::cerr);
    std::cerr << " added to context (now "
	      << current_param.length() << " variables)"
	      << std::endl;
  }
;
    break;}
case 45:
#line 238 "pddl2.y"
{
  std::cerr << "error: variable ";
  ((VariableSymbol*)yyvsp[0].sym->val)->print(std::cerr);
  std::cerr << " redeclared" << std::endl;
  exit(255);
;
    break;}
case 46:
#line 245 "pddl2.y"
{
  yyvsp[0].sym->val = new VariableSymbol(yyvsp[0].sym->text);
  current_param.append((VariableSymbol*)yyvsp[0].sym->val);
  if (trace_print_context) {
    std::cerr << "variable ";
    current_param[current_param.length() - 1]->print(std::cerr);
    std::cerr << " added to context (now "
	      << current_param.length() << " variables)"
	      << std::endl;
  }
;
    break;}
case 47:
#line 257 "pddl2.y"
{
  std::cerr << "error: variable ";
  ((VariableSymbol*)yyvsp[0].sym->val)->print(std::cerr);
  std::cerr << " redeclared" << std::endl;
  exit(255);
;
    break;}
case 48:
#line 267 "pddl2.y"
{
  current_type_set.append((TypeSymbol*)yyvsp[0].sym->val);
;
    break;}
case 49:
#line 271 "pddl2.y"
{
  current_type_set.append((TypeSymbol*)yyvsp[0].sym->val);
;
    break;}
case 50:
#line 279 "pddl2.y"
{
  last_n_functions = dom_functions.length();
;
    break;}
case 52:
#line 287 "pddl2.y"
{
  last_n_functions = dom_functions.length();
;
    break;}
case 54:
#line 292 "pddl2.y"
{
  TypeSymbol* t = (TypeSymbol*)yyvsp[0].sym->val;
  for (HSPS::index_type k = last_n_functions; k < dom_functions.length(); k++){
    if (write_info) {
      std::cerr << "info: converting ";
      dom_functions[k]->print(std::cerr);
      std::cerr << " to object function with type " << t->print_name
		<< std::endl;
    }
    HSPS::PDDL_Base::ObjectFunctionSymbol* f =
      new HSPS::PDDL_Base::ObjectFunctionSymbol(dom_functions[k]->print_name);
    f->param = dom_functions[k]->param;
    f->sym_types.assign_value(t, 1);
    dom_object_functions.append(f);
    HSPS::StringTable::Cell* c =
      (HSPS::StringTable::Cell*)tab.find(f->print_name);
    if (c == 0) {
      std::cerr << "very bad error: function "
		<< dom_functions[k]->print_name
		<< " declared but not found in string table!"
		<< std::endl;
      exit(255);
    }
    c->val = f;
  }
  dom_functions.set_length(last_n_functions);
  // last_n_functions = dom_functions.length();
;
    break;}
case 56:
#line 322 "pddl2.y"
{
  for (HSPS::index_type k = last_n_functions; k < dom_functions.length(); k++){
    if (write_info) {
      std::cerr << "info: converting ";
      dom_functions[k]->print(std::cerr);
      std::cerr << " to object function with type ";
      current_type_set.write_type(std::cerr);
      std::cerr << std::endl;
    }
    HSPS::PDDL_Base::ObjectFunctionSymbol* f =
      new HSPS::PDDL_Base::ObjectFunctionSymbol(dom_functions[k]->print_name);
    f->param = dom_functions[k]->param;
    f->sym_types.assign_copy(current_type_set);
    dom_object_functions.append(f);
    HSPS::StringTable::Cell* c =
      (HSPS::StringTable::Cell*)tab.find(f->print_name);
    if (c == 0) {
      std::cerr << "very bad error: function "
		<< dom_functions[k]->print_name
		<< " declared but not found in string table!"
		<< std::endl;
      exit(255);
    }
    c->val = f;
  }
  dom_functions.set_length(last_n_functions);
  // last_n_functions = dom_functions.length();
;
    break;}
case 58:
#line 352 "pddl2.y"
{
  last_n_functions = dom_functions.length();
;
    break;}
case 62:
#line 365 "pddl2.y"
{
  current_param.clear();
;
    break;}
case 63:
#line 369 "pddl2.y"
{
  FunctionSymbol* f = new FunctionSymbol(yyvsp[-3].sym->text);
  f->param = current_param;
  dom_functions.append(f);
  clear_context(current_param);
  yyvsp[-3].sym->val = f;
;
    break;}
case 64:
#line 382 "pddl2.y"
{
  current_type_set.clear();
;
    break;}
case 66:
#line 390 "pddl2.y"
{
  // set_type_type(dom_types, (TypeSymbol*)$4->val);
  for (HSPS::index_type k = 0; k < current_type_set.length(); k++) {
    if (current_type_set[k] == (TypeSymbol*)yyvsp[0].sym->val) {
      log_error("cyclic type declaration");
    }
    else {
      current_type_set[k]->sym_types.assign_value((TypeSymbol*)yyvsp[0].sym->val, 1);
    }
  }
  current_type_set.clear();
;
    break;}
case 67:
#line 403 "pddl2.y"
{
  yyvsp[0].sym->val = new TypeSymbol(yyvsp[0].sym->text);
  ((TypeSymbol*)yyvsp[0].sym->val)->sym_types.assign_value(dom_top_type, 1);
  // if (warning_level > 0)
  // std::cerr << "warning: assuming " << $4->text << " - object" << std::endl;
  // ((TypeSymbol*)$4->val)->sym_types.assign_value(dom_top_type, 1);
  // set_type_type(dom_types, (TypeSymbol*)$4->val);
  dom_types.append((TypeSymbol*)yyvsp[0].sym->val);
  for (HSPS::index_type k = 0; k < current_type_set.length(); k++)
    current_type_set[k]->sym_types.assign_value((TypeSymbol*)yyvsp[0].sym->val, 1);
  current_type_set.clear();
;
    break;}
case 68:
#line 416 "pddl2.y"
{
  // set_type_type(dom_types, dom_top_type);
  for (HSPS::index_type k = 0; k < current_type_set.length(); k++)
    if (current_type_set[k] != dom_top_type)
      current_type_set[k]->sym_types.assign_value(dom_top_type, 1);
  current_type_set.clear();
;
    break;}
case 70:
#line 428 "pddl2.y"
{
  /* the type is already (implicitly) declared */
  current_type_set.append((TypeSymbol*)yyvsp[0].sym->val);
;
    break;}
case 71:
#line 433 "pddl2.y"
{
  yyvsp[0].sym->val = new TypeSymbol(yyvsp[0].sym->text);
  dom_types.append((TypeSymbol*)yyvsp[0].sym->val);
  current_type_set.append((TypeSymbol*)yyvsp[0].sym->val);
;
    break;}
case 73:
#line 445 "pddl2.y"
{
  current_constant_decl.clear();
;
    break;}
case 75:
#line 450 "pddl2.y"
{
  current_constant_decl.clear();
;
    break;}
case 77:
#line 458 "pddl2.y"
{
  TypeSymbol* t = (TypeSymbol*)yyvsp[0].sym->val;
  for (HSPS::index_type k = 0; k < current_constant_decl.size(); k++) {
    HSPS::bool_vec d(false, current_constant_decl[k]->sym_types.length());
    bool is_d = false;
    for (HSPS::index_type i = 0; i < current_constant_decl[k]->sym_types.length(); i++)
      if (current_constant_decl[k]->sym_types[i]->subtype_or_equal(t))
	is_d = true;
      else if (t->subtype_or_equal(current_constant_decl[k]->sym_types[i]))
	d[i] = true;
    if (!is_d) {
      current_constant_decl[k]->sym_types.remove(d);
      current_constant_decl[k]->sym_types.append(t);
      t->add_element(current_constant_decl[k]);
    }
  }
  current_constant_decl.clear();
  // set_constant_type(dom_constants, (TypeSymbol*)$4->val);
;
    break;}
case 78:
#line 478 "pddl2.y"
{
  for (HSPS::index_type k = 0; k < current_constant_decl.size(); k++) {
    if (current_constant_decl[k]->sym_types.empty()) {
      current_constant_decl[k]->sym_types.append(dom_top_type);
      dom_top_type->add_element(current_constant_decl[k]);
    }
  }
  current_constant_decl.clear();
  // set_constant_type(dom_constants, dom_top_type);
;
    break;}
case 80:
#line 493 "pddl2.y"
{
  yyvsp[0].sym->val = new Symbol(yyvsp[0].sym->text);
  if (problem_name) {
    ((Symbol*)yyvsp[0].sym->val)->defined_in_problem = true;
  }
  dom_constants.append((Symbol*)yyvsp[0].sym->val);
  current_constant_decl.append((Symbol*)yyvsp[0].sym->val);
;
    break;}
case 81:
#line 502 "pddl2.y"
{
  yyvsp[0].sym->val = new Symbol(yyvsp[0].sym->text);
  if (problem_name) {
    ((Symbol*)yyvsp[0].sym->val)->defined_in_problem = true;
  }
  dom_constants.append((Symbol*)yyvsp[0].sym->val);
  current_constant_decl.append((Symbol*)yyvsp[0].sym->val);
;
    break;}
case 82:
#line 511 "pddl2.y"
{
  if (warning_level > 0) {
    std::cerr << "warning: redeclaration of constant " << yyvsp[0].sym->text << std::endl;
  }
  current_constant_decl.append((Symbol*)yyvsp[0].sym->val);
;
    break;}
case 83:
#line 518 "pddl2.y"
{
  if (warning_level > 0) {
    std::cerr << "warning: redeclaration of constant " << yyvsp[0].sym->text << std::endl;
  }
  current_constant_decl.append((Symbol*)yyvsp[0].sym->val);
;
    break;}
case 87:
#line 538 "pddl2.y"
{
  dom_actions.append(new ActionSymbol(yyvsp[0].sym->text));
;
    break;}
case 88:
#line 542 "pddl2.y"
{
  // post-processing should be done on all actions after the complete
  // domain and problem have been read (calling PDDL_Base::post_process())
  clear_context(current_param);
  yyvsp[-3].sym->val = dom_actions[dom_actions.length() - 1];
;
    break;}
case 89:
#line 552 "pddl2.y"
{
  current_param.clear();
;
    break;}
case 90:
#line 556 "pddl2.y"
{
  dom_actions[dom_actions.length() - 1]->param = current_param;
;
    break;}
case 97:
#line 566 "pddl2.y"
{
  //std::cerr << "read assoc string: [" << $3 << "]" << std::endl;
  dom_actions[dom_actions.length() - 1]->assoc = yyvsp[0].sval;
;
    break;}
case 99:
#line 575 "pddl2.y"
{
  SetSymbol* ssym = new SetSymbol(yyvsp[0].sym->text);
  yyvsp[0].sym->val = ssym;
  partitions.append(ssym);
  SetName* s = new SetName(ssym);
  current_atom = s;
;
    break;}
case 100:
#line 583 "pddl2.y"
{
  dom_actions[dom_actions.length() - 1]->part = (SetName*)current_atom;
;
    break;}
case 101:
#line 587 "pddl2.y"
{
  SetName* s = new SetName((SetSymbol*)yyvsp[0].sym->val);
  current_atom = s;
;
    break;}
case 102:
#line 592 "pddl2.y"
{
  dom_actions[dom_actions.length() - 1]->part = (SetName*)current_atom;
;
    break;}
case 114:
#line 623 "pddl2.y"
{
  dom_actions[dom_actions.length() - 1]->num_pre.append(yyvsp[0].rel);
;
    break;}
case 115:
#line 630 "pddl2.y"
{
  yyval.tkw = PDDL_Base::md_start;
;
    break;}
case 116:
#line 634 "pddl2.y"
{
  yyval.tkw = PDDL_Base::md_end;
;
    break;}
case 117:
#line 638 "pddl2.y"
{
  yyval.tkw = PDDL_Base::md_all;
;
    break;}
case 118:
#line 645 "pddl2.y"
{
  current_atom = new Atom((PredicateSymbol*)yyvsp[0].sym->val);
;
    break;}
case 119:
#line 649 "pddl2.y"
{
  ((Atom*)current_atom)->check();
  dom_actions[dom_actions.length() - 1]->pos_pre.append((Atom*)current_atom);
  ((Atom*)current_atom)->pred->pos_pre = true;
;
    break;}
case 120:
#line 655 "pddl2.y"
{
  Atom* eq_atom = new Atom(dom_eq_pred);
  eq_atom->param.set_length(2);
  eq_atom->param[0] = (Symbol*)yyvsp[-2].sym->val;
  eq_atom->param[1] = (Symbol*)yyvsp[-1].sym->val;
  dom_actions[dom_actions.length() - 1]->pos_pre.append(eq_atom);
;
    break;}
case 121:
#line 663 "pddl2.y"
{
  current_atom = new Atom((PredicateSymbol*)yyvsp[0].sym->val, yyvsp[-2].tkw);
;
    break;}
case 122:
#line 667 "pddl2.y"
{
  ((Atom*)current_atom)->check();
  dom_actions[dom_actions.length() - 1]->pos_pre.append((Atom*)current_atom);
  ((Atom*)current_atom)->pred->pos_pre = true;
;
    break;}
case 123:
#line 674 "pddl2.y"
{
  Atom* eq_atom = new Atom(dom_eq_pred, yyvsp[-6].tkw);
  eq_atom->param.set_length(2);
  eq_atom->param[0] = (Symbol*)yyvsp[-3].sym->val;
  eq_atom->param[1] = (Symbol*)yyvsp[-2].sym->val;
  dom_actions[dom_actions.length() - 1]->pos_pre.append(eq_atom);
;
    break;}
case 124:
#line 685 "pddl2.y"
{
  current_atom = new Atom((PredicateSymbol*)yyvsp[0].sym->val);
;
    break;}
case 125:
#line 689 "pddl2.y"
{
  ((Atom*)current_atom)->check();
  dom_actions[dom_actions.length() - 1]->neg_pre.append((Atom*)current_atom);
  ((Atom*)current_atom)->pred->neg_pre = true;
;
    break;}
case 126:
#line 695 "pddl2.y"
{
  Atom* eq_atom = new Atom(dom_eq_pred);
  eq_atom->param.set_length(2);
  eq_atom->param[0] = (Symbol*)yyvsp[-3].sym->val;
  eq_atom->param[1] = (Symbol*)yyvsp[-2].sym->val;
  dom_actions[dom_actions.length() - 1]->neg_pre.append(eq_atom);
;
    break;}
case 127:
#line 703 "pddl2.y"
{
  current_atom = new Atom((PredicateSymbol*)yyvsp[0].sym->val, yyvsp[-4].tkw);
;
    break;}
case 128:
#line 707 "pddl2.y"
{
  ((Atom*)current_atom)->check();
  dom_actions[dom_actions.length() - 1]->neg_pre.append((Atom*)current_atom);
  ((Atom*)current_atom)->pred->neg_pre = true;
;
    break;}
case 129:
#line 714 "pddl2.y"
{
  Atom* eq_atom = new Atom(dom_eq_pred, yyvsp[-9].tkw);
  eq_atom->param.set_length(2);
  eq_atom->param[0] = (Symbol*)yyvsp[-4].sym->val;
  eq_atom->param[1] = (Symbol*)yyvsp[-3].sym->val;
  dom_actions[dom_actions.length() - 1]->neg_pre.append(eq_atom);
;
    break;}
case 130:
#line 769 "pddl2.y"
{
  if (current_atom != 0) {
    current_atom->param.append((Symbol*)yyvsp[0].sym->val);
  }
;
    break;}
case 132:
#line 779 "pddl2.y"
{
  if (current_atom != 0) {
    current_atom->param.append((Symbol*)yyvsp[0].sym->val);
  }
;
    break;}
case 134:
#line 789 "pddl2.y"
{
  if (yyvsp[0].sym->val == 0) {
    log_error("undeclared variable in atom argument");
  }
  yyval.sym = yyvsp[0].sym;
;
    break;}
case 135:
#line 796 "pddl2.y"
{
  yyval.sym = yyvsp[0].sym;
;
    break;}
case 136:
#line 800 "pddl2.y"
{
  yyvsp[0].sym->val = new Symbol(yyvsp[0].sym->text);
  dom_constants.append((Symbol*)yyvsp[0].sym->val);
  yyval.sym = yyvsp[0].sym;
;
    break;}
case 137:
#line 809 "pddl2.y"
{
  current_atom_stack.append(current_atom);
  current_atom = new FTerm((ObjectFunctionSymbol*)yyvsp[0].sym->val);
;
    break;}
case 138:
#line 814 "pddl2.y"
{
  ObjectFunctionSymbol* f = (ObjectFunctionSymbol*)yyvsp[-3].sym->val;
  VariableSymbol* v =
    (VariableSymbol*)gensym(sym_variable, "?omsk", f->sym_types);
  v->binding = (FTerm*)current_atom;
  assert(current_atom_stack.length() > 0);
  current_atom = current_atom_stack[current_atom_stack.length() - 1];
  current_atom_stack.dec_length();
  HSPS::StringTable::Cell* c =
    (HSPS::StringTable::Cell*)tab.find(v->print_name);
  if (c == 0) {
    std::cerr << "very bad error: omsk symbol " << v->print_name
	      << " generated but not found in string table!"
	      << std::endl;
    exit(255);
  }
  yyval.sym = c;
;
    break;}
case 139:
#line 835 "pddl2.y"
{
  yyval.sym = yyvsp[0].sym;
;
    break;}
case 140:
#line 839 "pddl2.y"
{
  yyval.sym = yyvsp[0].sym;
;
    break;}
case 141:
#line 845 "pddl2.y"
{
  yyval.rel = new Relation(yyvsp[-3].rkw, yyvsp[-2].exp, yyvsp[-1].exp);
  yyvsp[-2].exp->mark_functions_in_condition();
  yyvsp[-1].exp->mark_functions_in_condition();
;
    break;}
case 142:
#line 852 "pddl2.y"
{
  yyval.rel = new Relation(yyvsp[-4].rkw, yyvsp[-6].tkw, yyvsp[-3].exp, yyvsp[-2].exp);
  yyvsp[-3].exp->mark_functions_in_condition();
  yyvsp[-2].exp->mark_functions_in_condition();
;
    break;}
case 143:
#line 861 "pddl2.y"
{
  yyval.rkw = rel_greater;
;
    break;}
case 144:
#line 865 "pddl2.y"
{
  yyval.rkw = rel_greater_equal;
;
    break;}
case 145:
#line 869 "pddl2.y"
{
  yyval.rkw = rel_less;
;
    break;}
case 146:
#line 873 "pddl2.y"
{
  yyval.rkw = rel_less_equal;
;
    break;}
case 147:
#line 877 "pddl2.y"
{
  yyval.rkw = rel_equal;
;
    break;}
case 148:
#line 884 "pddl2.y"
{
  yyval.exp = new BinaryExpression(exp_sub, new ConstantExpression(0), yyvsp[-1].exp);
;
    break;}
case 149:
#line 888 "pddl2.y"
{
  yyval.exp = new BinaryExpression(exp_add, yyvsp[-2].exp, yyvsp[-1].exp);
;
    break;}
case 150:
#line 892 "pddl2.y"
{
  yyval.exp = new BinaryExpression(exp_sub, yyvsp[-2].exp, yyvsp[-1].exp);
;
    break;}
case 151:
#line 896 "pddl2.y"
{
  yyval.exp = new BinaryExpression(exp_mul, yyvsp[-2].exp, yyvsp[-1].exp);
;
    break;}
case 152:
#line 900 "pddl2.y"
{
  yyval.exp = new BinaryExpression(exp_div, yyvsp[-2].exp, yyvsp[-1].exp);
;
    break;}
case 153:
#line 904 "pddl2.y"
{
  yyval.exp = new BinaryExpression(exp_min, yyvsp[-2].exp, yyvsp[-1].exp);
;
    break;}
case 154:
#line 908 "pddl2.y"
{
  yyval.exp = new BinaryExpression(exp_max, yyvsp[-2].exp, yyvsp[-1].exp);
;
    break;}
case 155:
#line 912 "pddl2.y"
{
  yyval.exp = new BinaryExpression(exp_div, yyvsp[-2].exp, yyvsp[0].exp);
;
    break;}
case 156:
#line 916 "pddl2.y"
{
  yyval.exp = new ConstantExpression(yyvsp[0].ival);
;
    break;}
case 157:
#line 920 "pddl2.y"
{
  yyval.exp = new ConstantExpression(NN_TO_N(yyvsp[0].rval));
;
    break;}
case 158:
#line 924 "pddl2.y"
{
  yyval.exp = new TimeExpression();
;
    break;}
case 159:
#line 928 "pddl2.y"
{
  yyval.exp = new PreferenceExpression((Symbol*)yyvsp[-1].sym->val);
;
    break;}
case 160:
#line 932 "pddl2.y"
{
  yyval.exp = yyvsp[0].exp;
;
    break;}
case 161:
#line 939 "pddl2.y"
{
  yyval.exp = yyvsp[0].exp;
;
    break;}
case 162:
#line 943 "pddl2.y"
{
  yyval.exp = new BinaryExpression(exp_add, yyvsp[-1].exp, yyvsp[0].exp);
;
    break;}
case 163:
#line 950 "pddl2.y"
{
  yyval.exp = yyvsp[0].exp;
;
    break;}
case 164:
#line 954 "pddl2.y"
{
  yyval.exp = new BinaryExpression(exp_mul, yyvsp[-1].exp, yyvsp[0].exp);
;
    break;}
case 165:
#line 961 "pddl2.y"
{
  yyval.exp = new FunctionExpression((FunctionSymbol*)yyvsp[-2].sym->val, yyvsp[-1].lst);
;
    break;}
case 166:
#line 965 "pddl2.y"
{
  yyval.exp = new FunctionExpression((FunctionSymbol*)yyvsp[0].sym->val, 0);
;
    break;}
case 167:
#line 972 "pddl2.y"
{
  yyval.lst = new ListExpression((VariableSymbol*)yyvsp[-1].sym->val, yyvsp[0].lst);
;
    break;}
case 168:
#line 976 "pddl2.y"
{
  yyval.lst = new ListExpression((Symbol*)yyvsp[-1].sym->val, yyvsp[0].lst);
;
    break;}
case 169:
#line 980 "pddl2.y"
{
  yyval.lst = 0;
;
    break;}
case 170:
#line 987 "pddl2.y"
{
  stored_n_param.append(current_param.length());
  current_context = new SetOf();
;
    break;}
case 171:
#line 992 "pddl2.y"
{
  current_atom = new Atom((PredicateSymbol*)yyvsp[0].sym->val);
;
    break;}
case 172:
#line 996 "pddl2.y"
{
  ((Atom*)current_atom)->check();
  SetOf* s = (SetOf*)current_context;
  s->pos_atoms.append((Atom*)current_atom);
  dom_actions[dom_actions.length() - 1]->set_pre.append(s);
  ((Atom*)current_atom)->pred->pos_pre = true;
  assert(stored_n_param.length() > 0);
  clear_context(current_param,
		stored_n_param[stored_n_param.length() - 1],
		current_param.length());
  current_param.set_length(stored_n_param[stored_n_param.length() - 1]);
  stored_n_param.dec_length();
  current_context = 0;
;
    break;}
case 173:
#line 1011 "pddl2.y"
{
  stored_n_param.append(current_param.length());
  current_context = new SetOf();
;
    break;}
case 174:
#line 1016 "pddl2.y"
{
  SetOf* s = (SetOf*)current_context;
  for (HSPS::index_type k = stored_n_param[stored_n_param.length() - 1];
       k < current_param.length(); k++) {
    s->param.append(current_param[k]);
  }
  dom_actions[dom_actions.length() - 1]->set_pre.append(s);
;
    break;}
case 175:
#line 1025 "pddl2.y"
{
  assert(stored_n_param.length() > 0);
  clear_context(current_param,
		stored_n_param[stored_n_param.length() - 1],
		current_param.length());
  current_param.set_length(stored_n_param[stored_n_param.length() - 1]);
  stored_n_param.dec_length();
  current_context = 0;
;
    break;}
case 176:
#line 1035 "pddl2.y"
{
  current_context = new SetOf();
;
    break;}
case 177:
#line 1039 "pddl2.y"
{
  SetOf* s = (SetOf*)current_context;
  dom_actions[dom_actions.length() - 1]->set_pre.append(s);
  current_context = 0;
;
    break;}
case 178:
#line 1045 "pddl2.y"
{
  stored_n_param.append(current_param.length());
  current_context = new SetOf();
;
    break;}
case 179:
#line 1050 "pddl2.y"
{
  current_atom = new Atom((PredicateSymbol*)yyvsp[0].sym->val);
;
    break;}
case 180:
#line 1054 "pddl2.y"
{
  ((Atom*)current_atom)->check();
  SetOf* s = (SetOf*)current_context;
  s->pos_atoms.append((Atom*)current_atom);
  s->set_mode(yyvsp[-12].tkw);
  dom_actions[dom_actions.length() - 1]->set_pre.append(s);
  ((Atom*)current_atom)->pred->pos_pre = true;
  assert(stored_n_param.length() > 0);
  clear_context(current_param,
		stored_n_param[stored_n_param.length() - 1],
		current_param.length());
  current_param.set_length(stored_n_param[stored_n_param.length() - 1]);
  stored_n_param.dec_length();
  current_context = 0;
;
    break;}
case 181:
#line 1070 "pddl2.y"
{
  stored_n_param.append(current_param.length());
  current_context = new SetOf();
;
    break;}
case 182:
#line 1075 "pddl2.y"
{
  SetOf* s = (SetOf*)current_context;
  for (HSPS::index_type k = stored_n_param[stored_n_param.length() - 1];
       k < current_param.length(); k++) {
    s->param.append(current_param[k]);
  }
  dom_actions[dom_actions.length() - 1]->set_pre.append(s);
;
    break;}
case 183:
#line 1084 "pddl2.y"
{
  SetOf* s = (SetOf*)current_context;
  s->set_mode(yyvsp[-10].tkw);
  assert(stored_n_param.length() > 0);
  clear_context(current_param,
		stored_n_param[stored_n_param.length() - 1],
		current_param.length());
  current_param.set_length(stored_n_param[stored_n_param.length() - 1]);
  stored_n_param.dec_length();
  current_context = 0;
;
    break;}
case 184:
#line 1096 "pddl2.y"
{
  current_context = new SetOf();
;
    break;}
case 185:
#line 1100 "pddl2.y"
{
  SetOf* s = (SetOf*)current_context;
  s->set_mode(yyvsp[-7].tkw);
  dom_actions[dom_actions.length() - 1]->set_pre.append(s);
  current_context = 0;
;
    break;}
case 194:
#line 1130 "pddl2.y"
{
  current_atom = new Atom((PredicateSymbol*)yyvsp[0].sym->val);
;
    break;}
case 195:
#line 1134 "pddl2.y"
{
  ((Atom*)current_atom)->check();
  SetOf* s = (SetOf*)current_context;
  s->pos_atoms.append((Atom*)current_atom);
  ((Atom*)current_atom)->pred->pos_pre = true;
;
    break;}
case 196:
#line 1141 "pddl2.y"
{
  current_atom = new Atom((PredicateSymbol*)yyvsp[0].sym->val);
;
    break;}
case 197:
#line 1145 "pddl2.y"
{
  ((Atom*)current_atom)->check();
  SetOf* s = (SetOf*)current_context;
  s->neg_atoms.append((Atom*)current_atom);
  ((Atom*)current_atom)->pred->neg_pre = true;
;
    break;}
case 198:
#line 1152 "pddl2.y"
{
  Atom* eq_atom = new Atom(dom_eq_pred);
  eq_atom->param.set_length(2);
  eq_atom->param[0] = (Symbol*)yyvsp[-2].sym->val;
  eq_atom->param[1] = (Symbol*)yyvsp[-1].sym->val;
  SetOf* s = (SetOf*)current_context;
  s->pos_atoms.append(eq_atom);
;
    break;}
case 199:
#line 1161 "pddl2.y"
{
  Atom* eq_atom = new Atom(dom_eq_pred);
  eq_atom->param.set_length(2);
  eq_atom->param[0] = (Symbol*)yyvsp[-3].sym->val;
  eq_atom->param[1] = (Symbol*)yyvsp[-2].sym->val;
  SetOf* s = (SetOf*)current_context;
  s->neg_atoms.append(eq_atom);
;
    break;}
case 200:
#line 1199 "pddl2.y"
{
  current_context = new SetOf();
;
    break;}
case 201:
#line 1203 "pddl2.y"
{
  SetOf* s = (SetOf*)current_context;
  dom_actions[dom_actions.length() - 1]->dis_pre.append(s);
  current_context = 0;
;
    break;}
case 203:
#line 1212 "pddl2.y"
{
  stored_n_param.append(current_param.length());
  current_context = new SetOf();
;
    break;}
case 204:
#line 1217 "pddl2.y"
{
  SetOf* s = (SetOf*)current_context;
  for (HSPS::index_type k = stored_n_param[stored_n_param.length() - 1];
       k < current_param.length(); k++) {
    s->param.append(current_param[k]);
  }
  dom_actions[dom_actions.length() - 1]->dis_pre.append(s);
;
    break;}
case 205:
#line 1226 "pddl2.y"
{
  assert(stored_n_param.length() > 0);
  clear_context(current_param,
		stored_n_param[stored_n_param.length() - 1],
		current_param.length());
  current_param.set_length(stored_n_param[stored_n_param.length() - 1]);
  stored_n_param.dec_length();
  current_context = 0;
;
    break;}
case 211:
#line 1250 "pddl2.y"
{
  current_atom = new Atom((PredicateSymbol*)yyvsp[0].sym->val);
;
    break;}
case 212:
#line 1254 "pddl2.y"
{
  ((Atom*)current_atom)->check();
  ((SetOf*)current_context)->pos_atoms.append((Atom*)current_atom);
  ((Atom*)current_atom)->pred->pos_pre = true;
;
    break;}
case 213:
#line 1260 "pddl2.y"
{
  Atom* eq_atom = new Atom(dom_eq_pred);
  eq_atom->param.set_length(2);
  eq_atom->param[0] = (Symbol*)yyvsp[-2].sym->val;
  eq_atom->param[1] = (Symbol*)yyvsp[-1].sym->val;
  ((SetOf*)current_context)->pos_atoms.append(eq_atom);
;
    break;}
case 214:
#line 1268 "pddl2.y"
{
  current_atom = new Atom((PredicateSymbol*)yyvsp[0].sym->val);
;
    break;}
case 215:
#line 1272 "pddl2.y"
{
  ((Atom*)current_atom)->check();
  ((SetOf*)current_context)->neg_atoms.append((Atom*)current_atom);
  ((Atom*)current_atom)->pred->neg_pre = true;
;
    break;}
case 216:
#line 1278 "pddl2.y"
{
  Atom* eq_atom = new Atom(dom_eq_pred);
  eq_atom->param.set_length(2);
  eq_atom->param[0] = (Symbol*)yyvsp[-3].sym->val;
  eq_atom->param[1] = (Symbol*)yyvsp[-2].sym->val;
  ((SetOf*)current_context)->neg_atoms.append(eq_atom);
;
    break;}
case 222:
#line 1307 "pddl2.y"
{
  stored_n_param.append(current_param.length());
  current_context = new SetOf();
;
    break;}
case 223:
#line 1312 "pddl2.y"
{
  SetOf* s = (SetOf*)current_context;
  for (HSPS::index_type k = stored_n_param[stored_n_param.length() - 1];
       k < current_param.length(); k++) {
    s->param.append(current_param[k]);
  }
;
    break;}
case 224:
#line 1320 "pddl2.y"
{
  dom_actions[dom_actions.length() - 1]->set_eff.append((SetOf*)current_context);
  assert(stored_n_param.length() > 0);
  clear_context(current_param,
		stored_n_param[stored_n_param.length() - 1],
		current_param.length());
  current_param.set_length(stored_n_param[stored_n_param.length() - 1]);
  stored_n_param.dec_length();
  current_context = 0;
;
    break;}
case 225:
#line 1331 "pddl2.y"
{
  current_context = new SetOf();
;
    break;}
case 226:
#line 1335 "pddl2.y"
{
  dom_actions[dom_actions.length() - 1]->set_eff.append((SetOf*)current_context);
  current_context = 0;
;
    break;}
case 227:
#line 1340 "pddl2.y"
{
  stored_n_param.append(current_param.length());
  current_context = new SetOf();
;
    break;}
case 228:
#line 1345 "pddl2.y"
{
  SetOf* s = (SetOf*)current_context;
  for (HSPS::index_type k = stored_n_param[stored_n_param.length() - 1];
       k < current_param.length(); k++) {
    s->param.append(current_param[k]);
  }
;
    break;}
case 229:
#line 1353 "pddl2.y"
{
  SetOf* s = (SetOf*)current_context;
  s->set_mode(yyvsp[-10].tkw);
  dom_actions[dom_actions.length() - 1]->set_eff.append(s);
  assert(stored_n_param.length() > 0);
  clear_context(current_param,
		stored_n_param[stored_n_param.length() - 1],
		current_param.length());
  current_param.set_length(stored_n_param[stored_n_param.length() - 1]);
  stored_n_param.dec_length();
  current_context = 0;
;
    break;}
case 230:
#line 1366 "pddl2.y"
{
  current_context = new SetOf();
;
    break;}
case 231:
#line 1370 "pddl2.y"
{
  SetOf* s = (SetOf*)current_context;
  s->set_mode(yyvsp[-6].tkw);
  dom_actions[dom_actions.length() - 1]->set_eff.append(s);
  current_context = 0;
;
    break;}
case 244:
#line 1407 "pddl2.y"
{
  current_atom = new Atom((PredicateSymbol*)yyvsp[0].sym->val);
  ((PredicateSymbol*)yyvsp[0].sym->val)->modded = true;
;
    break;}
case 245:
#line 1412 "pddl2.y"
{
  ((Atom*)current_atom)->check();
  if (current_context != 0) {
    ((SetOf*)current_context)->pos_atoms.append((Atom*)current_atom);
  }
  else {
    dom_actions[dom_actions.length() - 1]->adds.append((Atom*)current_atom);
  }
;
    break;}
case 246:
#line 1422 "pddl2.y"
{
  current_atom = new FTerm((ObjectFunctionSymbol*)yyvsp[0].sym->val);
;
    break;}
case 247:
#line 1426 "pddl2.y"
{
  FTerm* ft = (FTerm*)current_atom;
  ft->fun->modded = true;
  VariableSymbol* v =
    (VariableSymbol*)gensym(sym_variable,"?omsk",ft->fun->sym_types);
  v->binding = ft;
  Atom* a = new Atom(dom_assign_pred);
  a->param.set_length(2);
  a->param[0] = v;
  a->param[1] = (Symbol*)yyvsp[-1].sym->val;
  if (current_context != 0) {
    if ((warning_level > 0) || !best_effort) {
      std::cerr << "warning: object function assignment ";
      a->print(std::cerr);
      std::cerr << " in quantified/conditional effect ignored"
		<< std::endl;
      std::cerr << "context: ";
      current_context->print(std::cerr);
      std::cerr << std::endl;
    }
    if (!best_effort) exit(1);
  }
  else {
    dom_actions[dom_actions.length() - 1]->adds.append(a);
  }
;
    break;}
case 248:
#line 1453 "pddl2.y"
{
  current_atom = new Atom((PredicateSymbol*)yyvsp[0].sym->val, yyvsp[-2].tkw);
  ((PredicateSymbol*)yyvsp[0].sym->val)->modded = true;
;
    break;}
case 249:
#line 1458 "pddl2.y"
{
  ((Atom*)current_atom)->check();
  if (current_context != 0) {
    ((SetOf*)current_context)->pos_atoms.append((Atom*)current_atom);
  }
  else {
    dom_actions[dom_actions.length() - 1]->adds.append((Atom*)current_atom);
  }
;
    break;}
case 250:
#line 1471 "pddl2.y"
{
  yyval.sym = yyvsp[0].sym;
;
    break;}
case 251:
#line 1475 "pddl2.y"
{
  yyval.sym = (HSPS::StringTable::Cell*)tab.find("undefined");
  assert(yyval.sym != 0);
;
    break;}
case 252:
#line 1482 "pddl2.y"
{
  current_atom = new Atom((PredicateSymbol*)yyvsp[0].sym->val);
  ((PredicateSymbol*)yyvsp[0].sym->val)->modded = true;
;
    break;}
case 253:
#line 1487 "pddl2.y"
{
  ((Atom*)current_atom)->check();
  if (current_context != 0) {
    ((SetOf*)current_context)->neg_atoms.append((Atom*)current_atom);
  }
  else {
    dom_actions[dom_actions.length() - 1]->dels.append((Atom*)current_atom);
  }
;
    break;}
case 254:
#line 1497 "pddl2.y"
{
  current_atom = new Atom((PredicateSymbol*)yyvsp[0].sym->val, yyvsp[-4].tkw);
  ((PredicateSymbol*)yyvsp[0].sym->val)->modded = true;
;
    break;}
case 255:
#line 1502 "pddl2.y"
{
  ((Atom*)current_atom)->check();
  if (current_context != 0) {
    ((SetOf*)current_context)->neg_atoms.append((Atom*)current_atom);
  }
  else {
    dom_actions[dom_actions.length() - 1]->dels.append((Atom*)current_atom);
  }
;
    break;}
case 256:
#line 1515 "pddl2.y"
{
  current_atom = new FChangeAtom((FunctionSymbol*)yyvsp[0].sym->val);
;
    break;}
case 257:
#line 1519 "pddl2.y"
{
  ((FChangeAtom*)current_atom)->val = yyvsp[-1].exp;
  ((FChangeAtom*)current_atom)->fun->modified = true;
  if (!yyvsp[-1].exp->is_integral()) ((FChangeAtom*)current_atom)->fun->integral = false;
  dom_actions[dom_actions.length() - 1]->incs.append((FChangeAtom*)current_atom);
;
    break;}
case 258:
#line 1526 "pddl2.y"
{
  current_atom = new FChangeAtom((FunctionSymbol*)yyvsp[0].sym->val);
;
    break;}
case 259:
#line 1530 "pddl2.y"
{
  ((FChangeAtom*)current_atom)->val = yyvsp[-1].exp;
  ((FChangeAtom*)current_atom)->fun->modified = true;
  if (!yyvsp[-1].exp->is_integral()) ((FChangeAtom*)current_atom)->fun->integral = false;
  dom_actions[dom_actions.length() - 1]->decs.append((FChangeAtom*)current_atom);
;
    break;}
case 260:
#line 1537 "pddl2.y"
{
  current_atom = new FChangeAtom((FunctionSymbol*)yyvsp[0].sym->val);
;
    break;}
case 261:
#line 1541 "pddl2.y"
{
  ((FChangeAtom*)current_atom)->val = yyvsp[-1].exp;
  ((FChangeAtom*)current_atom)->fun->modified = true;
  if (!yyvsp[-1].exp->is_integral()) ((FChangeAtom*)current_atom)->fun->integral = false;
  dom_actions[dom_actions.length() - 1]->fass.append((FChangeAtom*)current_atom);
;
    break;}
case 262:
#line 1548 "pddl2.y"
{
  current_atom = new FChangeAtom((FunctionSymbol*)yyvsp[0].sym->val, yyvsp[-4].tkw);
;
    break;}
case 263:
#line 1552 "pddl2.y"
{
  ((FChangeAtom*)current_atom)->val = yyvsp[-2].exp;
  ((FChangeAtom*)current_atom)->fun->modified = true;
  if (!yyvsp[-2].exp->is_integral()) ((FChangeAtom*)current_atom)->fun->integral = false;
  dom_actions[dom_actions.length() - 1]->incs.append((FChangeAtom*)current_atom);
;
    break;}
case 264:
#line 1559 "pddl2.y"
{
  current_atom = new FChangeAtom((FunctionSymbol*)yyvsp[0].sym->val, yyvsp[-4].tkw);
;
    break;}
case 265:
#line 1563 "pddl2.y"
{
  ((FChangeAtom*)current_atom)->val = yyvsp[-2].exp;
  ((FChangeAtom*)current_atom)->fun->modified = true;
  if (!yyvsp[-2].exp->is_integral()) ((FChangeAtom*)current_atom)->fun->integral = false;
  dom_actions[dom_actions.length() - 1]->decs.append((FChangeAtom*)current_atom);
;
    break;}
case 266:
#line 1570 "pddl2.y"
{
  current_atom = new FChangeAtom((FunctionSymbol*)yyvsp[0].sym->val, yyvsp[-4].tkw);
;
    break;}
case 267:
#line 1574 "pddl2.y"
{
  ((FChangeAtom*)current_atom)->val = yyvsp[-2].exp;
  ((FChangeAtom*)current_atom)->fun->modified = true;
  if (!yyvsp[-2].exp->is_integral()) ((FChangeAtom*)current_atom)->fun->integral = false;
  dom_actions[dom_actions.length() - 1]->fass.append((FChangeAtom*)current_atom);
;
    break;}
case 275:
#line 1600 "pddl2.y"
{
  dom_actions[dom_actions.length() - 1]->dmin = yyvsp[-1].exp;
  dom_actions[dom_actions.length() - 1]->dmax = yyvsp[-1].exp;
;
    break;}
case 276:
#line 1605 "pddl2.y"
{
  dom_actions[dom_actions.length() - 1]->dmin = yyvsp[0].exp;
  dom_actions[dom_actions.length() - 1]->dmax = yyvsp[0].exp;
;
    break;}
case 277:
#line 1613 "pddl2.y"
{
  dom_actions[dom_actions.length() - 1]->dmax = yyvsp[-1].exp;
;
    break;}
case 280:
#line 1625 "pddl2.y"
{
  dom_actions[dom_actions.length() - 1]->dmin = yyvsp[-1].exp;
;
    break;}
case 283:
#line 1637 "pddl2.y"
{
  current_context = new SequentialTaskNet();
  stored_n_param.append(current_param.length());
  if (trace_print_context) {
    std::cerr << "pushed context (" << current_param.length() << " variables)"
	      << std::endl;
  }
;
    break;}
case 284:
#line 1647 "pddl2.y"
{
  SequentialTaskNet* n = (SequentialTaskNet*)current_context;
  n->abs_act = dom_actions[dom_actions.length() - 1];
  dom_actions[dom_actions.length() - 1]->exps.append(n);
  if (trace_print_context) {
    std::cerr << "poping context from "
	      << current_param.length()
	      << " to "
	      << stored_n_param[stored_n_param.length() - 1]
	      << " variables..." << std::endl;
  }
  assert(stored_n_param.length() > 0);
  clear_context(current_param,
		stored_n_param[stored_n_param.length() - 1],
		current_param.length());
  current_param.set_length(stored_n_param[stored_n_param.length() - 1]);
  stored_n_param.dec_length();
  current_context = 0;
;
    break;}
case 287:
#line 1675 "pddl2.y"
{
  current_atom = new Reference((ActionSymbol*)yyvsp[0].sym->val);
;
    break;}
case 288:
#line 1679 "pddl2.y"
{
  Reference* ref = (Reference*)current_atom;
  // ActionSymbol* act = (ActionSymbol*)$2->val;
  // act->refs[act->n_refs] = ref;
  // act->n_refs += 1;
  SequentialTaskNet* task_net = (SequentialTaskNet*)current_context;
  task_net->tasks.append(ref);
;
    break;}
case 290:
#line 1697 "pddl2.y"
{
  problem_name = yyvsp[-1].sym->text;
  if (current_file()) problem_file = strdup(current_file());
;
    break;}
case 307:
#line 1730 "pddl2.y"
{
  current_atom = new Atom((PredicateSymbol*)yyvsp[0].sym->val, md_init);
;
    break;}
case 308:
#line 1734 "pddl2.y"
{
  ((Atom*)current_atom)->check();
  PredicateSymbol* p = (PredicateSymbol*)yyvsp[-3].sym->val;
  if (p->param.length() != current_atom->param.length()) {
    log_error("wrong number of arguments for predicate in (:init ...");
  }
  ((Atom*)current_atom)->insert(p->init);
  current_atom->at_time = 0;
  dom_init.append((Atom*)current_atom);
;
    break;}
case 309:
#line 1745 "pddl2.y"
{
  current_atom = new Atom((PredicateSymbol*)yyvsp[0].sym->val, md_init);
;
    break;}
case 310:
#line 1749 "pddl2.y"
{
  ((Atom*)current_atom)->check();
  PredicateSymbol* p = (PredicateSymbol*)yyvsp[-4].sym->val;
  if (p->param.length() != current_atom->param.length()) {
    log_error("wrong number of arguments for predicate in (:init ...");
  }
  ((Atom*)current_atom)->insert(p->init);
  current_atom->at_time = yyvsp[-6].rval;
  p->added = true;
  p->modded = true;
  dom_pos_til.append((Atom*)current_atom);
;
    break;}
case 311:
#line 1762 "pddl2.y"
{
  current_atom = new Atom((PredicateSymbol*)yyvsp[0].sym->val, md_init);
;
    break;}
case 312:
#line 1766 "pddl2.y"
{
  ((Atom*)current_atom)->check();
  PredicateSymbol* p = (PredicateSymbol*)yyvsp[-5].sym->val;
  if (p->param.length() != current_atom->param.length()) {
    log_error("wrong number of arguments for predicate in (:init ...");
  }
  ((Atom*)current_atom)->insert(p->init);
  current_atom->at_time = yyvsp[-9].rval;
  p->deleted = true;
  p->modded = true;
  dom_neg_til.append((Atom*)current_atom);
;
    break;}
case 313:
#line 1782 "pddl2.y"
{
  current_atom = new OInitAtom((ObjectFunctionSymbol*)yyvsp[0].sym->val);
;
    break;}
case 314:
#line 1786 "pddl2.y"
{
  ObjectFunctionSymbol* f = (ObjectFunctionSymbol*)yyvsp[-5].sym->val;
  if (f->param.length() != current_atom->param.length()) {
    log_error("wrong number of arguments for object function in (:init ...");
  }
  ((OInitAtom*)current_atom)->val = (Symbol*)yyvsp[-1].sym->val;
  current_atom->at_time = 0;
  ((OInitAtom*)current_atom)->insert(f->init);
  dom_obj_init.append((OInitAtom*)current_atom);
;
    break;}
case 315:
#line 1799 "pddl2.y"
{
  current_atom = new FInitAtom((FunctionSymbol*)yyvsp[0].sym->val);
;
    break;}
case 316:
#line 1803 "pddl2.y"
{
  FunctionSymbol* f = (FunctionSymbol*)yyvsp[-5].sym->val;
  if (f->param.length() != current_atom->param.length()) {
    log_error("wrong number of arguments for function in (:init ...");
  }
  ((FInitAtom*)current_atom)->val = NN_TO_N(yyvsp[-1].rval);
  if (!INTEGRAL(((FInitAtom*)current_atom)->val))
    ((FInitAtom*)current_atom)->fun->integral = false;
  ((FInitAtom*)current_atom)->insert(f->init);
  current_atom->at_time = 0;
  dom_fun_init.append((FInitAtom*)current_atom);
;
    break;}
case 317:
#line 1816 "pddl2.y"
{
  FunctionSymbol* f = (FunctionSymbol*)yyvsp[-2].sym->val;
  current_atom = new FInitAtom((FunctionSymbol*)yyvsp[-2].sym->val);
  if (f->param.length() != 0) {
    log_error("wrong number of arguments for function in (:init ...");
  }
  ((FInitAtom*)current_atom)->val = NN_TO_N(yyvsp[-1].rval);
  if (!INTEGRAL(((FInitAtom*)current_atom)->val))
    ((FInitAtom*)current_atom)->fun->integral = false;
  current_atom->at_time = 0;
  ((FInitAtom*)current_atom)->insert(f->init);
  dom_fun_init.append((FInitAtom*)current_atom);
;
    break;}
case 324:
#line 1845 "pddl2.y"
{
  dom_goals.append(yyvsp[0].goal);
;
    break;}
case 325:
#line 1849 "pddl2.y"
{
  Symbol* name = new Symbol(sym_preference, yyvsp[-2].sym->text);
  yyvsp[-2].sym->val = name;
  dom_preferences.append(new Preference(name, yyvsp[-1].goal));
;
    break;}
case 326:
#line 1855 "pddl2.y"
{
  current_goal.append(new DisjunctiveGoal());
;
    break;}
case 327:
#line 1859 "pddl2.y"
{
  assert(current_goal.length() > 0);
  dom_goals.append(current_goal[current_goal.length() - 1]);
  current_goal.dec_length();
;
    break;}
case 328:
#line 1868 "pddl2.y"
{
  yyval.goal = yyvsp[0].goal;
;
    break;}
case 329:
#line 1872 "pddl2.y"
{
  current_goal.append(new ConjunctiveGoal());
;
    break;}
case 330:
#line 1876 "pddl2.y"
{
  assert(current_goal.length() > 0);
  yyval.goal = current_goal[current_goal.length() - 1];
  current_goal.dec_length();
;
    break;}
case 331:
#line 1882 "pddl2.y"
{
  current_goal.append(new DisjunctiveGoal());
;
    break;}
case 332:
#line 1886 "pddl2.y"
{
  assert(current_goal.length() > 0);
  yyval.goal = current_goal[current_goal.length() - 1];
  current_goal.dec_length();
;
    break;}
case 333:
#line 1895 "pddl2.y"
{
  assert(current_goal.length() > 0);
  ConjunctiveGoal* cg = current_goal[current_goal.length() - 1];
  assert(cg != 0);
  cg->goals.append(yyvsp[-1].goal);
;
    break;}
case 335:
#line 1906 "pddl2.y"
{
  current_atom = new Atom((PredicateSymbol*)yyvsp[0].sym->val);
;
    break;}
case 336:
#line 1910 "pddl2.y"
{
  ((Atom*)current_atom)->check();
  yyval.goal = new AtomicGoal((Atom*)current_atom, false);
  ((Atom*)current_atom)->pred->pos_pre = true;
  ((Atom*)current_atom)->insert(((Atom*)current_atom)->pred->pos_goal);
;
    break;}
case 337:
#line 1917 "pddl2.y"
{
  current_atom = new Atom((PredicateSymbol*)yyvsp[0].sym->val);
;
    break;}
case 338:
#line 1921 "pddl2.y"
{
  ((Atom*)current_atom)->check();
  yyval.goal = new AtomicGoal((Atom*)current_atom, true);
  ((Atom*)current_atom)->pred->neg_pre = true;
  ((Atom*)current_atom)->insert(((Atom*)current_atom)->pred->neg_goal);
;
    break;}
case 339:
#line 1928 "pddl2.y"
{
  Atom* eq_atom = new Atom(dom_eq_pred);
  eq_atom->param.set_length(2);
  eq_atom->param[0] = (Symbol*)yyvsp[-2].sym->val;
  eq_atom->param[1] = (Symbol*)yyvsp[-1].sym->val;
  yyval.goal = new AtomicGoal(eq_atom, false);
;
    break;}
case 340:
#line 1936 "pddl2.y"
{
  yyval.goal = new NumericGoal(yyvsp[0].rel);
;
    break;}
case 341:
#line 1940 "pddl2.y"
{
  yyval.goal = new SimpleSequenceGoal(goal_always, yyvsp[-1].goal);
;
    break;}
case 342:
#line 1944 "pddl2.y"
{
  yyval.goal = new SimpleSequenceGoal(goal_sometime, yyvsp[-1].goal);
;
    break;}
case 343:
#line 1948 "pddl2.y"
{
  yyval.goal = new SimpleSequenceGoal(goal_at_most_once, yyvsp[-1].goal);
;
    break;}
case 344:
#line 1952 "pddl2.y"
{
  yyval.goal = new TriggeredSequenceGoal(goal_sometime_before, yyvsp[-2].goal, yyvsp[-1].goal);
;
    break;}
case 345:
#line 1956 "pddl2.y"
{
  yyval.goal = new TriggeredSequenceGoal(goal_sometime_after, yyvsp[-2].goal, yyvsp[-1].goal);
;
    break;}
case 346:
#line 1960 "pddl2.y"
{
  yyval.goal = new DeadlineGoal(yyvsp[-2].rval, yyvsp[-1].goal);
;
    break;}
case 347:
#line 1964 "pddl2.y"
{
  yyval.goal = new TriggeredDeadlineGoal(yyvsp[-2].goal, yyvsp[-3].rval, yyvsp[-1].goal);
;
    break;}
case 349:
#line 2005 "pddl2.y"
{
  if (metric_type != metric_none) {
    if (warning_level > 0) {
      std::cerr << "warning: multiple :metric expressions - overwriting previous definition" << std::endl;
    }
  }
  metric_type = metric_minimize;
;
    break;}
case 350:
#line 2014 "pddl2.y"
{
  if (metric_type != metric_none) {
    if (warning_level > 0) {
      std::cerr << "warning: multiple :metric expressions - overwriting previous definition" << std::endl;
    }
  }
  metric_type = metric_maximize;
;
    break;}
case 351:
#line 2026 "pddl2.y"
{
  if (yyvsp[0].exp->exp_class == exp_time) {
    metric = 0;
    metric_type = metric_makespan;
    yyval.exp = 0;
  }
  else {
    metric = yyvsp[0].exp;
    yyval.exp = yyvsp[0].exp;
  }
;
    break;}
case 352:
#line 2041 "pddl2.y"
{
  serial_length = I_TO_N(yyvsp[-1].ival);
;
    break;}
case 353:
#line 2045 "pddl2.y"
{
  parallel_length = I_TO_N(yyvsp[-1].ival);
;
    break;}
case 354:
#line 2058 "pddl2.y"
{ yyval.rval = N_TO_NN(yyvsp[0].ival); ;
    break;}
case 355:
#line 2059 "pddl2.y"
{ yyval.rval = N_TO_NN(R_TO_N(yyvsp[-2].ival,yyvsp[0].ival)); ;
    break;}
case 356:
#line 2060 "pddl2.y"
{ yyval.rval = yyvsp[0].rval; ;
    break;}
case 357:
#line 2061 "pddl2.y"
{ yyval.rval = POS_INF; ;
    break;}
case 358:
#line 2068 "pddl2.y"
{
  current_item = new DKEL_Item(":invariant");
  current_context = current_item;
;
    break;}
case 360:
#line 2077 "pddl2.y"
{
  dom_sc_invariants.append(new SetConstraint(current_item));
;
    break;}
case 361:
#line 2081 "pddl2.y"
{
  dom_sc_invariants[dom_sc_invariants.length() - 1]->sc_type = yyvsp[-4].sckw;
  dom_sc_invariants[dom_sc_invariants.length() - 1]->sc_count = yyvsp[-3].ival;
  clear_context(current_param);
  current_context = 0;
;
    break;}
case 362:
#line 2088 "pddl2.y"
{
  dom_f_invariants.append(new InvariantFormula(current_item, yyvsp[-1].ff));
  clear_context(current_param);
  current_context = 0;
;
    break;}
case 363:
#line 2097 "pddl2.y"
{
  yyval.ff = new Formula(fc_false);
;
    break;}
case 364:
#line 2101 "pddl2.y"
{
  yyval.ff = new Formula(fc_true);
;
    break;}
case 365:
#line 2105 "pddl2.y"
{
  current_atom = new Atom((PredicateSymbol*)yyvsp[0].sym->val);
;
    break;}
case 366:
#line 2109 "pddl2.y"
{
  ((Atom*)current_atom)->check();
  yyval.ff = new AFormula((Atom*)current_atom);
;
    break;}
case 367:
#line 2114 "pddl2.y"
{
  yyval.ff = new EqFormula((Symbol*)yyvsp[-2].sym->val, (Symbol*)yyvsp[-1].sym->val);
;
    break;}
case 368:
#line 2118 "pddl2.y"
{
  yyval.ff = new NFormula(yyvsp[-1].ff);
;
    break;}
case 369:
#line 2122 "pddl2.y"
{
  yyval.ff = yyvsp[-1].ff;
  yyval.ff->fc = fc_conjunction;
;
    break;}
case 370:
#line 2127 "pddl2.y"
{
  yyval.ff = yyvsp[-1].ff;
  yyval.ff->fc = fc_conjunction;
;
    break;}
case 371:
#line 2132 "pddl2.y"
{
  yyval.ff = new BFormula(fc_implication, yyvsp[-2].ff, yyvsp[-1].ff);
;
    break;}
case 372:
#line 2136 "pddl2.y"
{
  yyval.ff = new BFormula(fc_equivalence, yyvsp[-2].ff, yyvsp[-1].ff);
;
    break;}
case 373:
#line 2140 "pddl2.y"
{
  stored_n_param.append(current_param.length());
;
    break;}
case 374:
#line 2144 "pddl2.y"
{
  QFormula* qf = new QFormula(fc_universal, yyvsp[-1].ff);
  assert(stored_n_param.length() > 0);
  for (HSPS::index_type k = stored_n_param[stored_n_param.length() - 1];
       k < current_param.length(); k++) {
    qf->vars.append(current_param[k]);
  }
  clear_context(current_param,
		stored_n_param[stored_n_param.length() - 1],
		current_param.length());
  current_param.set_length(stored_n_param[stored_n_param.length() - 1]);
  stored_n_param.dec_length();
  yyval.ff = qf;
;
    break;}
case 375:
#line 2159 "pddl2.y"
{
  stored_n_param.append(current_param.length());
;
    break;}
case 376:
#line 2163 "pddl2.y"
{
  QFormula* qf = new QFormula(fc_existential, yyvsp[-1].ff);
  assert(stored_n_param.length() > 0);
  for (HSPS::index_type k = stored_n_param[stored_n_param.length() - 1];
       k < current_param.length(); k++) {
    qf->vars.append(current_param[k]);
  }
  clear_context(current_param,
		stored_n_param[stored_n_param.length() - 1],
		current_param.length());
  current_param.set_length(stored_n_param[stored_n_param.length() - 1]);
  stored_n_param.dec_length();
  yyval.ff = qf;
;
    break;}
case 377:
#line 2181 "pddl2.y"
{
  ((CFormula*)yyval.ff)->add(yyvsp[0].ff);
;
    break;}
case 378:
#line 2185 "pddl2.y"
{
  yyval.ff = new CFormula(fc_list);
;
    break;}
case 379:
#line 2192 "pddl2.y"
{
  current_item = new IrrelevantItem();
  if (problem_name) current_item->defined_in_problem = true;
  current_context = current_item;
;
    break;}
case 383:
#line 2207 "pddl2.y"
{
  current_atom = new Reference((ActionSymbol*)yyvsp[0].sym->val);
;
    break;}
case 384:
#line 2211 "pddl2.y"
{
  IrrelevantItem* item = (IrrelevantItem*)current_item;
  item->entity = (Reference*)current_atom;
  dom_irrelevant.append(item);
  ActionSymbol* act = (ActionSymbol*)yyvsp[-4].sym->val;
  act->irr_ins.append(item);
  clear_context(current_param);
  current_context = 0;
;
    break;}
case 385:
#line 2224 "pddl2.y"
{
  current_atom = new Reference((PredicateSymbol*)yyvsp[0].sym->val);
;
    break;}
case 386:
#line 2228 "pddl2.y"
{
  IrrelevantItem* item = (IrrelevantItem*)current_item;
  item->entity = (Reference*)current_atom;
  dom_irrelevant.append(item);
  PredicateSymbol* pred = (PredicateSymbol*)yyvsp[-4].sym->val;
  pred->irr_ins.append(item);
  clear_context(current_param);
  current_context = 0;
;
    break;}
case 387:
#line 2241 "pddl2.y"
{
  current_item = new DKEL_Item(":invariant");
  current_item->defined_in_problem = true;
  current_context = current_item;
;
    break;}
case 389:
#line 2251 "pddl2.y"
{
  dom_sc_invariants.append(new SetConstraint(current_item));
;
    break;}
case 390:
#line 2255 "pddl2.y"
{
  dom_sc_invariants[dom_sc_invariants.length() - 1]->sc_type = yyvsp[-4].sckw;
  dom_sc_invariants[dom_sc_invariants.length() - 1]->sc_count = yyvsp[-3].ival;
  clear_context(current_param);
  current_context = 0;
;
    break;}
case 391:
#line 2262 "pddl2.y"
{
  dom_f_invariants.append(new InvariantFormula(current_item, yyvsp[-1].ff));
  clear_context(current_param);
  current_context = 0;
;
    break;}
case 397:
#line 2279 "pddl2.y"
{
  current_item->item_tags.insert(yyvsp[0].sym->text);
;
    break;}
case 398:
#line 2286 "pddl2.y"
{
  yyvsp[0].sym->val = new Symbol(sym_misc, yyvsp[0].sym->text);
  current_item->name = (Symbol*)yyvsp[0].sym->val;
  current_item->item_tags.insert(yyvsp[0].sym->text);
;
    break;}
case 399:
#line 2292 "pddl2.y"
{
  current_item->name = (Symbol*)yyvsp[0].sym->val;
  current_item->item_tags.insert(yyvsp[0].sym->text);
;
    break;}
case 400:
#line 2300 "pddl2.y"
{
  current_param.clear();
;
    break;}
case 401:
#line 2304 "pddl2.y"
{
  current_context->param = current_param;
;
    break;}
case 404:
#line 2316 "pddl2.y"
{
  for (HSPS::index_type k = stored_n_param[stored_n_param.length() - 1];
       k < current_param.length(); k++) {
    current_context->param.append(current_param[k]);
  }
;
    break;}
case 429:
#line 2368 "pddl2.y"
{
  current_atom = new Atom((PredicateSymbol*)yyvsp[0].sym->val);
;
    break;}
case 430:
#line 2372 "pddl2.y"
{
  ((Atom*)current_atom)->check();
  current_context->pos_con.append((Atom*)current_atom);
  ((Atom*)current_atom)->pred->pos_pre = true;
;
    break;}
case 431:
#line 2379 "pddl2.y"
{
  current_atom = new Atom((PredicateSymbol*)yyvsp[0].sym->val);
;
    break;}
case 432:
#line 2383 "pddl2.y"
{
  ((Atom*)current_atom)->check();
  current_context->pos_con.append((Atom*)current_atom);
  ((Atom*)current_atom)->pred->pos_pre = true;
;
    break;}
case 433:
#line 2389 "pddl2.y"
{
  current_atom = new Atom((PredicateSymbol*)yyvsp[0].sym->val, md_init);
;
    break;}
case 434:
#line 2393 "pddl2.y"
{
  ((Atom*)current_atom)->check();
  current_context->pos_con.append((Atom*)current_atom);
  ((Atom*)current_atom)->pred->pos_pre = true;
;
    break;}
case 435:
#line 2399 "pddl2.y"
{
  Atom* eq_atom = new Atom(dom_eq_pred);
  eq_atom->param.set_length(2);
  eq_atom->param[0] = (Symbol*)yyvsp[-2].sym->val;
  eq_atom->param[1] = (Symbol*)yyvsp[-1].sym->val;
  current_context->pos_con.append(eq_atom);
;
    break;}
case 436:
#line 2410 "pddl2.y"
{
  current_atom = new Atom((PredicateSymbol*)yyvsp[0].sym->val);
;
    break;}
case 437:
#line 2414 "pddl2.y"
{
  ((Atom*)current_atom)->check();
  current_context->neg_con.append((Atom*)current_atom);
  ((Atom*)current_atom)->pred->neg_pre = true;
;
    break;}
case 438:
#line 2421 "pddl2.y"
{
  current_atom = new Atom((PredicateSymbol*)yyvsp[0].sym->val);
;
    break;}
case 439:
#line 2425 "pddl2.y"
{
  ((Atom*)current_atom)->check();
  current_context->neg_con.append((Atom*)current_atom);
  ((Atom*)current_atom)->pred->neg_pre = true;
;
    break;}
case 440:
#line 2431 "pddl2.y"
{
  current_atom = new Atom((PredicateSymbol*)yyvsp[0].sym->val, md_init);
;
    break;}
case 441:
#line 2435 "pddl2.y"
{
  ((Atom*)current_atom)->check();
  current_context->neg_con.append((Atom*)current_atom);
  ((Atom*)current_atom)->pred->neg_pre = true;
;
    break;}
case 442:
#line 2441 "pddl2.y"
{
  current_atom = new Atom((PredicateSymbol*)yyvsp[0].sym->val, md_init);
;
    break;}
case 443:
#line 2445 "pddl2.y"
{
  ((Atom*)current_atom)->check();
  current_context->neg_con.append((Atom*)current_atom);
  ((Atom*)current_atom)->pred->neg_pre = true;
;
    break;}
case 444:
#line 2451 "pddl2.y"
{
  Atom* eq_atom = new Atom(dom_eq_pred);
  eq_atom->param.set_length(2);
  eq_atom->param[0] = (Symbol*)yyvsp[-3].sym->val;
  eq_atom->param[1] = (Symbol*)yyvsp[-2].sym->val;
  current_context->neg_con.append(eq_atom);
;
    break;}
case 445:
#line 2462 "pddl2.y"
{
  current_atom = new Atom((PredicateSymbol*)yyvsp[0].sym->val, md_pos_goal);
;
    break;}
case 446:
#line 2466 "pddl2.y"
{
  ((Atom*)current_atom)->check();
  current_context->pos_con.append((Atom*)current_atom);
  ((Atom*)current_atom)->pred->pos_pre = true;
;
    break;}
case 447:
#line 2472 "pddl2.y"
{
  current_atom = new Atom((PredicateSymbol*)yyvsp[0].sym->val, md_neg_goal);
;
    break;}
case 448:
#line 2476 "pddl2.y"
{
  ((Atom*)current_atom)->check();
  current_context->pos_con.append((Atom*)current_atom);
  ((Atom*)current_atom)->pred->neg_pre = true;
;
    break;}
case 449:
#line 2485 "pddl2.y"
{
  current_atom = new Atom((PredicateSymbol*)yyvsp[0].sym->val, md_pos_goal);
;
    break;}
case 450:
#line 2489 "pddl2.y"
{
  ((Atom*)current_atom)->check();
  current_context->neg_con.append((Atom*)current_atom);
  ((Atom*)current_atom)->pred->pos_pre = true;
;
    break;}
case 451:
#line 2495 "pddl2.y"
{
  current_atom = new Atom((PredicateSymbol*)yyvsp[0].sym->val, md_neg_goal);
;
    break;}
case 452:
#line 2499 "pddl2.y"
{
  ((Atom*)current_atom)->check();
  current_context->neg_con.append((Atom*)current_atom);
  ((Atom*)current_atom)->pred->neg_pre = true;
;
    break;}
case 453:
#line 2508 "pddl2.y"
{
  TypeConstraint* c =
    new TypeConstraint((VariableSymbol*)yyvsp[-1].sym->val, (TypeSymbol*)yyvsp[-2].sym->val);
  current_context->type_con.append(c);
;
    break;}
case 454:
#line 2514 "pddl2.y"
{
  TypeConstraint* c =
    new TypeConstraint((VariableSymbol*)yyvsp[-1].sym->val, (TypeSymbol*)yyvsp[-2].sym->val);
  current_context->type_con.append(c);
;
    break;}
case 455:
#line 2522 "pddl2.y"
{
  yyval.sckw = sc_at_least;
;
    break;}
case 456:
#line 2526 "pddl2.y"
{
  yyval.sckw = sc_at_most;
;
    break;}
case 457:
#line 2530 "pddl2.y"
{
  yyval.sckw = sc_exactly;
;
    break;}
case 461:
#line 2542 "pddl2.y"
{
  current_atom = new Atom((PredicateSymbol*)yyvsp[0].sym->val);
;
    break;}
case 462:
#line 2546 "pddl2.y"
{
  ((Atom*)current_atom)->check();
  dom_sc_invariants[dom_sc_invariants.length()-1]->pos_atoms.append((Atom*)current_atom);
;
    break;}
case 463:
#line 2551 "pddl2.y"
{
  current_atom = new Atom((PredicateSymbol*)yyvsp[0].sym->val);
;
    break;}
case 464:
#line 2555 "pddl2.y"
{
  ((Atom*)current_atom)->check();
  dom_sc_invariants[dom_sc_invariants.length()-1]->neg_atoms.append((Atom*)current_atom);
;
    break;}
case 465:
#line 2563 "pddl2.y"
{
  stored_n_param.append(current_param.length());
  current_context = new SetOf();
;
    break;}
case 466:
#line 2568 "pddl2.y"
{
  SetOf* s = (SetOf*)current_context;
  for (HSPS::index_type k = stored_n_param[stored_n_param.length() - 1];
       k < current_param.length(); k++) {
    s->param.append(current_param[k]);
  }
;
    break;}
case 467:
#line 2576 "pddl2.y"
{
  current_atom = new Atom((PredicateSymbol*)yyvsp[0].sym->val);
;
    break;}
case 468:
#line 2580 "pddl2.y"
{
  ((Atom*)current_atom)->check();
  SetOf* s = (SetOf*)current_context;
  s->pos_atoms.append((Atom*)current_atom);
  dom_sc_invariants[dom_sc_invariants.length()-1]->atom_sets.append(s);
  assert(stored_n_param.length() > 0);
  clear_context(current_param,
		stored_n_param[stored_n_param.length() - 1],
		current_param.length());
  current_param.set_length(stored_n_param[stored_n_param.length() - 1]);
  stored_n_param.dec_length();
  current_context = 0;
;
    break;}
case 469:
#line 2597 "pddl2.y"
{
  input_plans.append(new InputPlan());
  input_plans[input_plans.length() - 1]->src = current_file();
  if (current_plan_file != current_file()) {
    n_plans_in_current_file = 0;
    current_plan_file = current_file();
  }
;
    break;}
case 470:
#line 2606 "pddl2.y"
{
  if (input_plans[input_plans.length() - 1]->name == 0)
    if (current_plan_file) {
      std::ostringstream pn;
      pn << current_plan_file << ":" << n_plans_in_current_file;
      Symbol* plan_file_name = new Symbol(sym_misc, strdup(pn.str().c_str()));
      input_plans[input_plans.length() - 1]->name = plan_file_name;
    }
  n_plans_in_current_file += 1;
;
    break;}
case 472:
#line 2621 "pddl2.y"
{
  assert(input_plans.length() > 0);
  input_plans[input_plans.length() - 1]->is_opt = true;
;
    break;}
case 475:
#line 2631 "pddl2.y"
{
  assert(input_plans.length() > 0);
  input_plans[input_plans.length() - 1]->name = new Symbol(yyvsp[0].sym->text);
;
    break;}
case 476:
#line 2639 "pddl2.y"
{
  current_atom = new Reference((ActionSymbol*)yyvsp[0].sym->val);
;
    break;}
case 477:
#line 2643 "pddl2.y"
{
  Reference* ref = (Reference*)current_atom;
  ActionSymbol* act = (ActionSymbol*)yyvsp[-3].sym->val;
  act->refs.append(ref);
  assert(input_plans.length() > 0);
  input_plans[input_plans.length() - 1]->
    steps.append(new InputPlanStep(ref, yyvsp[-6].rval));
  clear_context(current_param);
;
    break;}
case 478:
#line 2656 "pddl2.y"
{
  // input_plans.append(0);
  current_plan_file = 0;
;
    break;}
case 479:
#line 2661 "pddl2.y"
{
  // input_plans.append(0);
  current_plan_file = 0;
;
    break;}
case 482:
#line 2674 "pddl2.y"
{
  current_atom = new Reference((ActionSymbol*)yyvsp[0].sym->val);
;
    break;}
case 483:
#line 2678 "pddl2.y"
{
  Reference* ref = (Reference*)current_atom;
  ActionSymbol* act = (ActionSymbol*)yyvsp[-4].sym->val;
  act->refs.append(ref);
  if (input_plans.length() == 0) {
    at_position(std::cerr) << "beginning of new plan" << std::endl;
    input_plans.append(new InputPlan());
    input_plans[input_plans.length() - 1]->src = current_file();
    if (current_file()) {
      Symbol* plan_file_name = new Symbol(sym_misc, current_file());
      input_plans[input_plans.length() - 1]->name = plan_file_name;
    }
    current_plan_file = current_file();
  }
  else if (current_file() != current_plan_file) {
    at_position(std::cerr) << "beginning of new plan (new file)" << std::endl;
    input_plans.append(new InputPlan());
    input_plans[input_plans.length() - 1]->src = current_file();
    if (current_file()) {
      Symbol* plan_file_name = new Symbol(sym_misc, current_file());
      input_plans[input_plans.length() - 1]->name = plan_file_name;
    }
    current_plan_file = current_file();
  }
  input_plans[input_plans.length() - 1]->
    steps.append(new InputPlanStep(ref, yyvsp[-7].rval));
  clear_context(current_param);
;
    break;}
case 488:
#line 2720 "pddl2.y"
{
  current_atom = new Reference((ActionSymbol*)yyvsp[0].sym->val);
;
    break;}
case 489:
#line 2724 "pddl2.y"
{
  Reference* ref = (Reference*)current_atom;
  ActionSymbol* act = (ActionSymbol*)yyvsp[-3].sym->val;
  act->refs.append(ref);
  if (input_plans.length() == 0) {
    at_position(std::cerr) << "beginning of new plan" << std::endl;
    input_plans.append(new InputPlan());
    input_plans[input_plans.length() - 1]->src = current_file();
    if (current_file()) {
      Symbol* plan_file_name = new Symbol(sym_misc, current_file());
      input_plans[input_plans.length() - 1]->name = plan_file_name;
    }
    current_plan_file = current_file();
  }
  else if (current_file() != current_plan_file) {
    at_position(std::cerr) << "beginning of new plan (new file)" << std::endl;
    input_plans.append(new InputPlan());
    input_plans[input_plans.length() - 1]->src = current_file();
    if (current_file()) {
      Symbol* plan_file_name = new Symbol(sym_misc, current_file());
      input_plans[input_plans.length() - 1]->name = plan_file_name;
    }
    current_plan_file = current_file();
  }
  HSPS::index_type n = input_plans[input_plans.length() - 1]->steps.length();
  input_plans[input_plans.length() - 1]->
    steps.append(new InputPlanStep(ref, n));
  clear_context(current_param);
;
    break;}
case 493:
#line 2766 "pddl2.y"
{
  current_entry = new HTableEntry();
;
    break;}
case 494:
#line 2770 "pddl2.y"
{
  h_table.append(current_entry);
  current_entry = 0;
;
    break;}
case 495:
#line 2778 "pddl2.y"
{
  current_entry->cost = yyvsp[0].rval;
  current_entry->opt = true;
;
    break;}
case 496:
#line 2783 "pddl2.y"
{
  current_entry->cost = yyvsp[0].rval;
;
    break;}
case 501:
#line 2800 "pddl2.y"
{
  assert(current_entry);
  current_atom = new Atom((PredicateSymbol*)yyvsp[0].sym->val);
;
    break;}
case 502:
#line 2805 "pddl2.y"
{
  ((Atom*)current_atom)->check();
  current_entry->atoms.append((Atom*)current_atom);
  current_entry->neg.append(false);
  assert(current_entry->atoms.length() == current_entry->neg.length());
;
    break;}
case 503:
#line 2815 "pddl2.y"
{
  assert(current_entry);
  current_atom = new Atom((PredicateSymbol*)yyvsp[0].sym->val);
;
    break;}
case 504:
#line 2820 "pddl2.y"
{
  ((Atom*)current_atom)->check();
  current_entry->atoms.append((Atom*)current_atom);
  current_entry->neg.append(true);
  assert(current_entry->atoms.length() == current_entry->neg.length());
;
    break;}
case 505:
#line 2830 "pddl2.y"
{
  ReferenceSet* set = new ReferenceSet();
  current_context = set;
  stored_n_param.assign_value(0, 1);
  current_param.clear();
  input_sets.append(set);
;
    break;}
case 506:
#line 2838 "pddl2.y"
{
  clear_context(current_param);
  current_context = 0;
;
    break;}
case 507:
#line 2846 "pddl2.y"
{
  yyvsp[0].sym->val = new Symbol(sym_misc, yyvsp[0].sym->text);
  ((ReferenceSet*)current_context)->name = (Symbol*)yyvsp[0].sym->val;
;
    break;}
case 508:
#line 2851 "pddl2.y"
{
  ((ReferenceSet*)current_context)->name = (Symbol*)yyvsp[0].sym->val;
;
    break;}
case 513:
#line 2865 "pddl2.y"
{
  if (yyvsp[0].sym->val) {
    current_atom = new Reference((Symbol*)yyvsp[0].sym->val, false, true);
  }
  else {
    current_atom = new Reference(new Symbol(sym_misc, yyvsp[0].sym->text), false, true);
  }
;
    break;}
case 514:
#line 2874 "pddl2.y"
{
  assert(input_sets.length() > 0);
  assert(input_sets[input_sets.length() - 1] != 0);
  input_sets[input_sets.length() - 1]->
    add(new SimpleReferenceSet((Reference*)current_atom));
;
    break;}
case 515:
#line 2881 "pddl2.y"
{
  assert(input_sets.length() > 0);
  assert(input_sets[input_sets.length() - 1] != 0);
  if (yyvsp[0].sym->val) {
    input_sets[input_sets.length() - 1]->add
      (new SimpleReferenceSet(new Reference((Symbol*)yyvsp[0].sym->val, false, false)));
  }
  else {
    input_sets[input_sets.length() - 1]->add
      (new SimpleReferenceSet(new Reference(new Symbol(sym_misc, yyvsp[0].sym->text), false, false)));
  }
;
    break;}
case 516:
#line 2897 "pddl2.y"
{
  stored_n_param.append(current_param.length());
  stored_context.append(current_context);
  current_context = new SimpleReferenceSet(0);
;
    break;}
case 517:
#line 2903 "pddl2.y"
{
  assert(stored_n_param.length() > 0);
  assert(stored_context.length() > 0);
  clear_context(current_param,
		stored_n_param[stored_n_param.length() - 1],
		current_param.length());
  current_param.set_length(stored_n_param[stored_n_param.length() - 1]);
  stored_n_param.dec_length();
  current_context = stored_context[stored_context.length() - 1];
  stored_context.dec_length();
;
    break;}
case 518:
#line 2918 "pddl2.y"
{
  if (yyvsp[0].sym->val) {
    current_atom = new Reference((Symbol*)yyvsp[0].sym->val, false, true);
  }
  else {
    current_atom = new Reference(new Symbol(sym_misc, yyvsp[0].sym->text), false, true);
  }
;
    break;}
case 519:
#line 2927 "pddl2.y"
{
  SimpleReferenceSet* s = (SimpleReferenceSet*)current_context;
  s->ref = (Reference*)current_atom;
  assert(input_sets.length() > 0);
  assert(input_sets[input_sets.length() - 1] != 0);
  input_sets[input_sets.length() - 1]->add(s);
;
    break;}
case 520:
#line 2935 "pddl2.y"
{
  if (yyvsp[0].sym->val) {
    current_atom = new Reference((Symbol*)yyvsp[0].sym->val, true, true);
  }
  else {
    current_atom = new Reference(new Symbol(sym_misc, yyvsp[0].sym->text), true, true);
  }
;
    break;}
case 521:
#line 2944 "pddl2.y"
{
  SimpleReferenceSet* s = (SimpleReferenceSet*)current_context;
  s->ref = (Reference*)current_atom;
  assert(input_sets.length() > 0);
  assert(input_sets[input_sets.length() - 1] != 0);
  input_sets[input_sets.length() - 1]->add(s);
;
    break;}
}

#line 811 "/usr/local/lib/bison.cc"
   /* the action file gets copied in in place of this dollarsign  */
  yyvsp -= yylen;
  yyssp -= yylen;
#ifdef YY_PDDL_Parser_LSP_NEEDED
  yylsp -= yylen;
#endif

#if YY_PDDL_Parser_DEBUG != 0
  if (YY_PDDL_Parser_DEBUG_FLAG)
    {
      short *ssp1 = yyss - 1;
      fprintf (stderr, "state stack now");
      while (ssp1 != yyssp)
	fprintf (stderr, " %d", *++ssp1);
      fprintf (stderr, "\n");
    }
#endif

  *++yyvsp = yyval;

#ifdef YY_PDDL_Parser_LSP_NEEDED
  yylsp++;
  if (yylen == 0)
    {
      yylsp->first_line = YY_PDDL_Parser_LLOC.first_line;
      yylsp->first_column = YY_PDDL_Parser_LLOC.first_column;
      yylsp->last_line = (yylsp-1)->last_line;
      yylsp->last_column = (yylsp-1)->last_column;
      yylsp->text = 0;
    }
  else
    {
      yylsp->last_line = (yylsp+yylen-1)->last_line;
      yylsp->last_column = (yylsp+yylen-1)->last_column;
    }
#endif

  /* Now "shift" the result of the reduction.
     Determine what state that goes to,
     based on the state we popped back to
     and the rule number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTBASE] + *yyssp;
  if (yystate >= 0 && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTBASE];

  YYGOTO(yynewstate);

YYLABEL(yyerrlab)   /* here on detecting error */

  if (! yyerrstatus)
    /* If not already recovering from an error, report this error.  */
    {
      ++YY_PDDL_Parser_NERRS;

#ifdef YY_PDDL_Parser_ERROR_VERBOSE
      yyn = yypact[yystate];

      if (yyn > YYFLAG && yyn < YYLAST)
	{
	  int size = 0;
	  char *msg;
	  int x, count;

	  count = 0;
	  /* Start X at -yyn if nec to avoid negative indexes in yycheck.  */
	  for (x = (yyn < 0 ? -yyn : 0);
	       x < (sizeof(yytname) / sizeof(char *)); x++)
	    if (yycheck[x + yyn] == x)
	      size += strlen(yytname[x]) + 15, count++;
	  msg = (char *) malloc(size + 15);
	  if (msg != 0)
	    {
	      strcpy(msg, "parse error");

	      if (count < 5)
		{
		  count = 0;
		  for (x = (yyn < 0 ? -yyn : 0);
		       x < (sizeof(yytname) / sizeof(char *)); x++)
		    if (yycheck[x + yyn] == x)
		      {
			strcat(msg, count == 0 ? ", expecting `" : " or `");
			strcat(msg, yytname[x]);
			strcat(msg, "'");
			count++;
		      }
		}
	      YY_PDDL_Parser_ERROR(msg);
	      free(msg);
	    }
	  else
	    YY_PDDL_Parser_ERROR ("parse error; also virtual memory exceeded");
	}
      else
#endif /* YY_PDDL_Parser_ERROR_VERBOSE */
	YY_PDDL_Parser_ERROR("parse error");
    }

  YYGOTO(yyerrlab1);
YYLABEL(yyerrlab1)   /* here on error raised explicitly by an action */

  if (yyerrstatus == 3)
    {
      /* if just tried and failed to reuse lookahead token after an error, discard it.  */

      /* return failure if at end of input */
      if (YY_PDDL_Parser_CHAR == YYEOF)
	YYABORT;

#if YY_PDDL_Parser_DEBUG != 0
      if (YY_PDDL_Parser_DEBUG_FLAG)
	fprintf(stderr, "Discarding token %d (%s).\n", YY_PDDL_Parser_CHAR, yytname[yychar1]);
#endif

      YY_PDDL_Parser_CHAR = YYEMPTY;
    }

  /* Else will try to reuse lookahead token
     after shifting the error token.  */

  yyerrstatus = 3;              /* Each real token shifted decrements this */

  YYGOTO(yyerrhandle);

YYLABEL(yyerrdefault)  /* current state does not do anything special for the error token. */

#if 0
  /* This is wrong; only states that explicitly want error tokens
     should shift them.  */
  yyn = yydefact[yystate];  /* If its default is to accept any token, ok.  Otherwise pop it.*/
  if (yyn) YYGOTO(yydefault);
#endif

YYLABEL(yyerrpop)   /* pop the current state because it cannot handle the error token */

  if (yyssp == yyss) YYABORT;
  yyvsp--;
  yystate = *--yyssp;
#ifdef YY_PDDL_Parser_LSP_NEEDED
  yylsp--;
#endif

#if YY_PDDL_Parser_DEBUG != 0
  if (YY_PDDL_Parser_DEBUG_FLAG)
    {
      short *ssp1 = yyss - 1;
      fprintf (stderr, "Error: state stack now");
      while (ssp1 != yyssp)
	fprintf (stderr, " %d", *++ssp1);
      fprintf (stderr, "\n");
    }
#endif

YYLABEL(yyerrhandle)

  yyn = yypact[yystate];
  if (yyn == YYFLAG)
    YYGOTO(yyerrdefault);

  yyn += YYTERROR;
  if (yyn < 0 || yyn > YYLAST || yycheck[yyn] != YYTERROR)
    YYGOTO(yyerrdefault);

  yyn = yytable[yyn];
  if (yyn < 0)
    {
      if (yyn == YYFLAG)
	YYGOTO(yyerrpop);
      yyn = -yyn;
      YYGOTO(yyreduce);
    }
  else if (yyn == 0)
    YYGOTO(yyerrpop);

  if (yyn == YYFINAL)
    YYACCEPT;

#if YY_PDDL_Parser_DEBUG != 0
  if (YY_PDDL_Parser_DEBUG_FLAG)
    fprintf(stderr, "Shifting error token, ");
#endif

  *++yyvsp = YY_PDDL_Parser_LVAL;
#ifdef YY_PDDL_Parser_LSP_NEEDED
  *++yylsp = YY_PDDL_Parser_LLOC;
#endif

  yystate = yyn;
  YYGOTO(yynewstate);
/* end loop, in which YYGOTO may be used. */
  YYENDGOTO
}

/* END */

/* #line 1010 "/usr/local/lib/bison.cc" */
#line 5513 "grammar.cc"
#line 2953 "pddl2.y"

