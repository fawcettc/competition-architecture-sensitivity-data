package pddl2bdd.parser.logic;

import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Vector;

import net.sf.javabdd.BDD;
import net.sf.javabdd.BDDFactory;

public class AndOrTerm extends Expression {
    @Override
    public BDD createBDD(BDDFactory factory, LinkedList<String> nAryVariables,
            LinkedList<BDD> nAryVariablesBDDs, boolean[] unusedVarIndices) {
        if (terms.size() == 0) {
            if (isAndTerm)
                return factory.one();
            else
                return factory.zero();
        }
        if (terms.size() == 1)
            return terms.firstElement().createBDD(factory, nAryVariables,
                    nAryVariablesBDDs, unusedVarIndices);
        BDD tmp1;
        BDD tmp2;
        Vector<BDD> termBDDs = new Vector<BDD>();
        ListIterator<Expression> termsIt = terms.listIterator();
        while (termsIt.hasNext()) {
            tmp1 = termsIt.next().createBDD(factory, nAryVariables,
                    nAryVariablesBDDs, unusedVarIndices);
            if (tmp1 != null)
                termBDDs.add(tmp1);
        }
        if (termBDDs.size() == 0)
            return null;
        if (termBDDs.size() == 1)
            return termBDDs.firstElement();

        for (int i = 0; i < termBDDs.size(); i++) {
            tmp1 = termBDDs.get(i);
            i++;
            if (i < termBDDs.size()) {
                tmp2 = termBDDs.get(i);
                if (isAndTerm)
                    termBDDs.add(tmp1.and(tmp2));
                else
                    termBDDs.add(tmp1.or(tmp2));
                tmp1.free();
                tmp2.free();
            }
        }
        return termBDDs.lastElement();
    }

    @Override
    public void classifyEffects(Vector<String> addEffects,
            Vector<String> deleteEffects, boolean isNegated) {
        ListIterator<Expression> termsIt = terms.listIterator();
        while (termsIt.hasNext()) {
            termsIt.next()
                    .classifyEffects(addEffects, deleteEffects, isNegated);
        }
    }

    @Override
    public void getAllPredicates(Vector<Predicate> allPreds) {
        ListIterator<Expression> termIt = terms.listIterator();
        while (termIt.hasNext()) {
            termIt.next().getAllPredicates(allPreds);
        }
    }

    @Override
    public void findDelAdds(Vector<String> delEffects,
            Vector<String> addEffects, boolean isPositive) {
        ListIterator<Expression> termIt = terms.listIterator();
        while (termIt.hasNext()) {
            termIt.next().findDelAdds(delEffects, addEffects, isPositive);
        }
    }

    @Override
    public boolean eliminateDelEffects(Vector<String> delEffectsToEliminate,
            boolean isPositive) {
        ListIterator<Expression> termIt = terms.listIterator();
        while (termIt.hasNext()) {
            if (termIt.next().eliminateDelEffects(delEffectsToEliminate,
                    isPositive)) {
                termIt.remove();
            }
        }
        return false;
    }

    @Override
    public String toString() {
        String ret;
        if (isAndTerm)
            ret = "(and\n";
        else
            ret = "(or\n";
        ListIterator<Expression> termsIt = terms.listIterator();
        while (termsIt.hasNext()) {
            ret += termsIt.next().toString() + "\n";
        }
        ret += ")";
        return ret;
    }

    public void setIsAndTerm(boolean isAndTerm) {
        this.isAndTerm = isAndTerm;
    }

    public boolean getIsAndTerm() {
        return isAndTerm;
    }

    public void setTerms(Vector<Expression> terms) {
        this.terms = terms;
    }

    public Vector<Expression> getTerms() {
        return terms;
    }

    private boolean isAndTerm;
    private Vector<Expression> terms;
}
