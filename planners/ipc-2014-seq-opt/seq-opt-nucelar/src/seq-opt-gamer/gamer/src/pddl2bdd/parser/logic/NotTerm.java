package pddl2bdd.parser.logic;

import java.util.LinkedList;
import java.util.Vector;

import net.sf.javabdd.BDD;
import net.sf.javabdd.BDDFactory;

public class NotTerm extends Expression {
    @Override
    public BDD createBDD(BDDFactory factory, LinkedList<String> nAryVariables,
            LinkedList<BDD> nAryVariablesBDDs, boolean[] unusedVarIndices) {
        BDD tmp = term.createBDD(factory, nAryVariables, nAryVariablesBDDs,
                unusedVarIndices);
        if (tmp == null)
            return null;
        BDD ret = tmp.not();
        tmp.free();
        return ret;
    }

    @Override
    public void classifyEffects(Vector<String> addEffects,
            Vector<String> deleteEffects, boolean isNegated) {
        term.classifyEffects(addEffects, deleteEffects, !isNegated);
    }

    @Override
    public void getAllPredicates(Vector<Predicate> allPreds) {
        term.getAllPredicates(allPreds);
    }

    @Override
    public void findDelAdds(Vector<String> delEffects,
            Vector<String> addEffects, boolean isPositive) {
        term.findDelAdds(delEffects, addEffects, !isPositive);
    }

    @Override
    public boolean eliminateDelEffects(Vector<String> delEffectsToEliminate,
            boolean isPositive) {
        return term.eliminateDelEffects(delEffectsToEliminate, !isPositive);
    }

    @Override
    public String toString() {
        String ret = "(not\n" + term.toString() + "\n)";
        return ret;
    }

    public void setTerm(Expression term) {
        this.term = term;
    }

    public Expression getTerm() {
        return term;
    }

    private Expression term;
}
