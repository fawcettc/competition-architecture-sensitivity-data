package pddl2bdd.parser.logic;

import java.util.LinkedList;
import java.util.Vector;

import net.sf.javabdd.*;

public abstract class Expression {
    public abstract BDD createBDD(BDDFactory factory,
            LinkedList<String> nAryVariables,
            LinkedList<BDD> nAryVariablesBDDs, boolean[] unusedVarIndices);

    public abstract void classifyEffects(Vector<String> addEffects,
            Vector<String> deleteEffects, boolean isNegated);

    public abstract void getAllPredicates(Vector<Predicate> allPreds);

    public abstract void findDelAdds(Vector<String> delEffects,
            Vector<String> addEffects, boolean isPositive);

    public abstract boolean eliminateDelEffects(
            Vector<String> delEffectsToEliminate, boolean isPositive);

    public abstract String toString();
}
