package pddl2bdd.parser.logic;

import java.util.LinkedList;
import java.util.Vector;

import net.sf.javabdd.BDD;
import net.sf.javabdd.BDDFactory;

public class Predicate extends Expression {
    @Override
    public BDD createBDD(BDDFactory factory, LinkedList<String> nAryVariables,
            LinkedList<BDD> nAryVariablesBDDs, boolean[] unusedVarIndices) {
        if (name.equalsIgnoreCase("foo"))
            return factory.one();
        int index = nAryVariables.indexOf(name);
        if (unusedVarIndices[index])
            return null;
        return nAryVariablesBDDs.get(nAryVariables.indexOf(name)).id();
    }

    @Override
    public void classifyEffects(Vector<String> addEffects,
            Vector<String> deleteEffects, boolean isNegated) {
        if (!addEffects.contains(name)) {
            if (isNegated && !deleteEffects.contains(name))
                deleteEffects.add(name);
            else
                addEffects.add(name);
        }
    }

    @Override
    public void getAllPredicates(Vector<Predicate> allPreds) {
        if (!allPreds.contains(this))
            allPreds.add(this);
    }

    @Override
    public void findDelAdds(Vector<String> delEffects,
            Vector<String> addEffects, boolean isPositive) {
        if (isPositive)
            addEffects.add(name);
        else
            delEffects.add(name);
    }

    @Override
    public boolean eliminateDelEffects(Vector<String> delEffectsToEliminate,
            boolean isPositive) {
        if (!isPositive && delEffectsToEliminate.contains(name))
            return true;
        return false;
    }

    @Override
    public String toString() {
        String ret = "(" + name + ")";
        return ret;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    private String name;
}
