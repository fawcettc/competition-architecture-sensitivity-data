## Competition Architecture Sensitivity

Solver competitions have been used in many areas of AI to assess the current state of the art and guide future research
and development. AI planning is no exception, and the International Planning Competition (IPC) has been frequently run
for nearly two decades. Due to the organizational and computational burden involved in running these competitions,
solvers are generally compared using a single homogeneous hardware and software environment for all competitors.

To what extent does the specific choice of hardware and software environment have an effect on solver performance, and
is that effect distributed equally across the competing solvers?

This repository contains the benchmark problem instances, planners and script wrappers
used in a large experimental analysis of the above questions for the Agile and Optimal
tracks of the 2014 IPC. This study used two hardware configurations and eight software configurations:
the Compute-Calcul Canada Westgrid *Orcinus* and Italian CINECA *Galileo* compute clusters,
and two versions each of GCC, Java and Python dependencies.

We also include the detailed results of our experiments, performance of each (problem instance, planner)
pair over repeated runs in each configuration.


### Organisation

The repository is organised roughly as follows:

#### <root>/problem-instances

Problem instance and domain PDDL for the instances used in the IPC 2014 Agile and Optimal tracks.

#### <root>/planners

Planner source code and Python wrapper scripts used for planner execution with a single command-line format.
These wrappers are designed for use with runsolver (see below), and use the generic solver wrapper
[GenericWrapper4AC](https://github.com/automl/GenericWrapper4AC) developed by members of the ML4AAD group at the University of Freiburg
and the BETA/algorithms lab at the University of British Columbia.

#### <root>/misc/runsolver-3.3.4

The runsolver tool is useful for measuring resource usage and enforcing resource limits
when performing solver runs. We include the version used for our experiments.

Runsolver is maintained by Olivier Roussel and the official
project webpage can be found [here](http://www.cril.univ-artois.fr/~roussel/runsolver/).

#### <root>/experimental-results

For each hardware and software configuration, and for each of the two considered IPC tracks,
we include a CSV for each planner with the performance of that planner on each benchmark instance in that IPC track.

For many of our experiments, repeated runs were performed for each (planner, instance) pair
in an attempt to control for sources of stochasticity that were not considered in our study.
Results for these runs appear in separate `runX` directories.

Results are organized in the following subdirectory structure:

    <root>/<competition track>/<hardware configuration>/<GCC version>-<Python version>-<Java version>-<memory
    limit>/<repeated run number>/<planner>.csv

Each of the results CSV have rows with the following format:

    <instance filename>,<run result>:<CPU time>:<validated plan quality>:<CPU time limit>:<unused>


### Contributors

 * Andrea F. Bocchese ( [email](mailto:boccheseandrea@gmail.com) ), Università degli Studi di Brescia
 * Chris Fawcett ( [webpage](https://www.cs.ubc.ca/~fawcettc/) | [email](mailto:fawcettc@cs.ubc.ca) ), University of British Columbia
 * Mauro Vallati ( [webpage](https://pure.hud.ac.uk/en/persons/mauro-vallati) | [email](mailto:m.vallati@hud.ac.uk) ), University of Huddersfield
 * Holger H. Hoos ( [webpage](https://www.cs.ubc.ca/~hoos/) | [email](mailto:hoos@cs.ubc.ca) ), University of British Columbia
 * Alfonso E. Gerevini ( [webpage](http://artificial-intelligence.unibs.it/gerevini/) | [email](mailto:alfonso.gerevini@unibs.it) ), Università degli Studi di Brescia


### Papers
 * Coming soon, please get in touch with us for a preprint.
